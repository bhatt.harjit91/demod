//
//  ICJsonParser.swift
//  iCanRefer
//
//  Created by TalentMicro on 18/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

public class ICResponseMain: Codable {
    
    var success: String?
    var message: String?
    var count: String?
    var nextPage: String?
    
    var isNextPage: Bool {
        return (nextPage != nil && nextPage! == "1")
    }
    
    var isSuccess: Bool {
        return (success != nil && success! != "0")
    }
    
    
    
    private enum CodingKeys: String, CodingKey {
        case success = "success"
        case message = "message"
        case count = "count"
        case nextPage = "next_page"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(success, forKey: .success)
        try container.encode(message, forKey: .message)
        try container.encode(count, forKey: .count)
        try container.encode(nextPage, forKey: .nextPage)
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try? values.decode(String.self, forKey: .success)
        message = try? values.decode(String.self, forKey: .message)
        count = try? values.decode(String.self, forKey: .count)
        nextPage = try? values.decode(String.self, forKey: .nextPage)
    }
}
public class WSResponseError<T: ICResponseData> : Codable {
    
    var message: String?
    
    private enum CodingKeys: String, CodingKey {
        case message = "Message"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(message, forKey: .message)
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try? values.decode(String.self, forKey: .message)
    }
}

public class WSResponse<T: ICResponseData> : Codable {
    
    var setting: ICResponseMain?
    var success: String?
    var message: String?
    var arrayData: [T]?
    var dictData: T?
    var errorMsg: String?
    var status: Bool?
    
    var isSuccess: Bool {
        return (success != nil && success?.lowercased() == "success")
    }
    
    private enum CodingKeys: String, CodingKey {
        case setting = "settings"
        case success = "success"
        case message = "message"
        case status = "status"
        case Message = "Message"
        case data = "data"
        case Data = "Data"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(setting, forKey: .setting)
        try container.encode(success, forKey: .success)
        if success == nil {
            try container.encode(success, forKey: .status)
        }
        try container.encode(message, forKey: .message)
        try container.encode(message, forKey: .Message)
        try container.encode(arrayData, forKey: .data)
        try container.encode(dictData, forKey: .data)
        try container.encode(errorMsg, forKey: .data)
    }
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        setting = try? values.decode(ICResponseMain.self, forKey: .setting)
        success = try? values.decode(String.self, forKey: .success)
        status = try? values.decode(Bool.self, forKey: .status)
        message = try? values.decode(String.self, forKey: .message)
        if message == nil {
            message = try? values.decode(String.self, forKey: .Message)
        }
        arrayData = try? values.decode([T].self, forKey: .data)
        if arrayData == nil {
            arrayData = try? values.decode([T].self, forKey: .Data)
        }
        dictData = try? values.decode(T.self, forKey: .data)
        errorMsg = try? values.decode(String.self, forKey: .data)
    }
}

public class ICResponseData: Codable {
    
    init() {
        
    }
    public func encode(to encoder: Encoder) throws {
        
    }
    
    required public init(from decoder: Decoder) throws {
        
    }
}
