//
//  BannerView.swift
//  NearKart
//
//  Created by RaviKiran B on 19/09/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit

class BannerView: UIView {
    
    //var titleLabel:UILabel = UILabel.init()
    var messageLabel:NKLabel = NKLabel.init()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        //titleLabel.frame = CGRect.init(x: 5, y: 5, width: rect.size.width-10, height: 24)
        messageLabel.frame = CGRect.init(x: 5, y: 5, width: rect.size.width-10, height: rect.size.height-5)
        messageLabel.numberOfLines = 0
        messageLabel.textType = LabelType.normalText.rawValue
        messageLabel.adjustsFontSizeToFitWidth = true
        messageLabel.textAlignment = .center
        //        if titleLabel.superview == nil{
        //            addSubview(titleLabel)
        //        }
        
        if messageLabel.superview == nil{
            addSubview(messageLabel)
        }
        layoutIfNeeded()
    }
}
