//
//  RCView.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

@IBDesignable

class RCView: UIView {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupUI()
    }
    
    @IBInspectable var cornerRadius:CGFloat = 0.0 {
        didSet {
            setupUI()
        }
    }
    @IBInspectable var borderWidth:CGFloat = 0.0 {
        didSet {
            setupUI()
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.colorWithHexString(hexString: "#EEEEEE")! {
        didSet {
            setupUI()
        }
    }
    func setupUI() {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
          
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
      get {
        return layer.shadowRadius
      }
      set {
        layer.shadowRadius = newValue
      }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
      get {
        return layer.shadowOpacity
      }
      set {
        layer.shadowOpacity = newValue
      }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
      get {
        return layer.shadowOffset
      }
      set {
        layer.shadowOffset = newValue
      }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
     get {
        if let color = layer.shadowColor {
          return UIColor(cgColor: color)
        }
        return nil
      }
      set {
        if let color = newValue {
          layer.shadowColor = color.cgColor
            layer.masksToBounds = false
            self.clipsToBounds = false
        } else {
          layer.shadowColor = nil
        }
      }
    }
    
}

