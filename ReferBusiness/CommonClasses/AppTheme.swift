//
//  AppTheme.swift
//  WhatMate
//
//  Created by RaviKiran B on 23/01/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//


import UIKit


@objc enum LabelType:Int{
    case normalText = 0
    case largeTitle = 1
    case titleText = 2
    case smallTitle = 3
    case subTitleText = 4
    case largeSubtitle = 5
    case smallSubtitle = 6
    case smallFont = 7
    case italicFont = 8
    case primaryButtonFont = 9
    case tinyFont = 10
    case italicSmallFont = 11
}

//MARK: -
extension UIColor{
    
    //Mark - Color
    class var primaryColor:UIColor{
        //return UIColor.rgb(fromHex: 0x000000)
        return UIColor.rgb(fromHex: 0xbe2025)
        
    }
    
    class var statusBarColor:UIColor{
        
       // return UIColor.rgb(fromHex:  0xf0424b)
        //return UIColor.rgb(fromHex: 0xFFFFFF)
         return UIColor.rgb(fromHex: 0xbe2025)
        
    }
    
    class var navbarTint:UIColor{
        
        //return UIColor.rgb(fromHex: 0xe6f7ff)
        return UIColor.rgb(fromHex: 0x000000)
        
    }
    
    class var btnLinkColor:UIColor{
        
        //return UIColor.rgb(fromHex: 0xe6f7ff)
        return UIColor.rgb(fromHex: 0x0172ff)
        
    }
    
    
    class var secondaryColor:UIColor{
        
        return UIColor.rgb(fromHex: 0xE46242)
        //  return UIColor.rgb(fromHex: 0xC12C32)
        
    }
    
    class var bgColor: UIColor {
        return UIColor.rgb(fromHex: 0xD5D5D5)
    }
    
    class var pTextColor:UIColor{
        return UIColor.white
    }
    
    class var sTextColor:UIColor{
        return UIColor.white
    }
    
    // Creates a UIColor from a Hex string.
    class func colorWithHexString (hexString:String) -> UIColor? {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            
            if hexColor.count == 6 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt32 = 0
                
                if scanner.scanHexInt32(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                    g = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
                    //b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    b = CGFloat(hexNumber & 0x0000ff) / 255
                    a = 1.0
                    return  self.init(red: r, green: g, blue: b, alpha: a)
                    
                }
            }
        }
        
        return nil
    }
    
    class func rgb(fromHex: Int) -> UIColor {
        
        let red =   CGFloat((fromHex & 0xFF0000) >> 16) / 0xFF
        let green = CGFloat((fromHex & 0x00FF00) >> 8) / 0xFF
        let blue =  CGFloat(fromHex & 0x0000FF) / 0xFF
        let alpha = CGFloat(1.0)
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    class func getGradientColor(index:Int) -> [UIColor]{
        let newIndex = index%11
        switch newIndex {
        case 0://Dark blue
           // return [UIColor.rgb(fromHex: 0x2B98C1),UIColor.rgb(fromHex: 0x3EB1D6)]
        return [UIColor.rgb(fromHex: 0x052060),UIColor.rgb(fromHex: 0x052060)]
        case 1://Primary
            return [UIColor.rgb(fromHex: 0xbe2025),UIColor.rgb(fromHex: 0xbe2025)]
        case 2://Blue
            return [UIColor.rgb(fromHex: 0x0470FB),UIColor.rgb(fromHex: 0x0470FB)]
        case 3://Blue(Btn Link)
            return [UIColor.rgb(fromHex: 0x0172ff),UIColor.rgb(fromHex: 0x0172ff)]
        case 4://Pink-Purple
            return [UIColor.rgb(fromHex: 0xE259B1),UIColor.rgb(fromHex: 0x9466E2)]
        case 5://Green - Yellow
            return [UIColor.rgb(fromHex: 0x7DAD40),UIColor.rgb(fromHex: 0xB9CE4D)]
        case 6://REd
            return [UIColor.rgb(fromHex: 0xCC4240),UIColor.rgb(fromHex: 0xE46F63)]
        case 7://dark ash blue
            return [UIColor.rgb(fromHex: 0x2D4B63),UIColor.rgb(fromHex: 0x4B6D79)]
        case 8://SKy blue
            return [UIColor.rgb(fromHex: 0x2EACD0),UIColor.rgb(fromHex: 0x6CD3E6)]
        case 9://clear
            return [UIColor.clear]
        case 10://Light Gray
            return [UIColor.rgb(fromHex: 0xD3D3D3),UIColor.rgb(fromHex: 0xE9E9E9)]
        default:
            return [UIColor.white]
            
        }
        
    }
    
    
}

//MARK: -
extension UIFont{
    /*
     
     
     
     Roboto
     == Roboto-Regular
     == Roboto-Thin
     == Roboto-MediumItalic
     == Roboto-Medium
     == Roboto-Bold
     == Roboto-BlackItalic
     == Roboto-Italic
     == Roboto-ThinItalic
     */
    //Mark: - Font
    
    class func textWidthForButton(s: String) -> CGFloat
    {
        return s.size(withAttributes: [NSAttributedString.Key.font: UIFont.primaryButtonFont]).width
    }
    
    class func fontForType(textType:Int) -> UIFont {
        var font:UIFont =  UIFont.bodyFont
        switch textType {
        case LabelType.normalText.rawValue:
            font = UIFont.bodyFont
        case LabelType.titleText.rawValue:
            font = UIFont.titleFont
        case LabelType.largeTitle.rawValue:
            font = UIFont.largeTitleFont
        case LabelType.smallTitle.rawValue:
            font = UIFont.smallTitleFont
        case LabelType.subTitleText.rawValue:
            font = UIFont.subTitleFont
        case LabelType.largeSubtitle.rawValue:
            font = UIFont.largeSubTitleFont
        case LabelType.smallSubtitle.rawValue:
            font = UIFont.smallSubTitleFont
        case LabelType.smallFont.rawValue:
            font = UIFont.smallFont
        case LabelType.italicFont.rawValue:
            font = UIFont.italicFont
        case LabelType.primaryButtonFont.rawValue:
            font = UIFont.primaryButtonFont
        case LabelType.tinyFont.rawValue:
            font = UIFont.tinyFont
        case LabelType.italicSmallFont.rawValue:
            font = UIFont.italicSmallFont
        default:
            font = UIFont.bodyFont
        }
        return font
    }
    
    class var defaultFont:UIFont {
        return UIFont.systemFont(ofSize: 15.0)
    }
    class var bodyFont:UIFont{
        
        return UIFont.init(name: "Roboto-Regular", size: 15.0) ?? UIFont.defaultFont
        
    }
    
    class var textErrorFont:UIFont{
        
        return UIFont.init(name: "Roboto-Regular", size: 12.0) ?? UIFont.defaultFont
        
        
    }
    
    class var titleFont:UIFont{
        
        return UIFont.init(name: "Roboto-Bold", size: 17.0) ?? UIFont.defaultFont
        
        
    }
    
    class var OTPFont:UIFont{
        
        return UIFont.init(name: "Roboto-Bold", size: 30.0) ?? UIFont.defaultFont
        
        
    }
    
    class var HomeTitleFont:UIFont{
        
        return UIFont.init(name: "Roboto-Bold", size: 25.0) ?? UIFont.defaultFont
        
        
    }
    
    class var HomeSubTitleFont:UIFont{
        
        return UIFont.init(name: "Roboto-Bold", size: 22.0) ?? UIFont.defaultFont
        
        
    }
    
    class var largeTitleFont:UIFont{
        
        return UIFont.init(name: "Roboto-Bold", size: 20.0) ?? UIFont.defaultFont
        
        
    }
    
    class var smallTitleFont:UIFont{
        
        return UIFont.init(name: "Roboto-Medium", size: 14.0) ?? UIFont.defaultFont
        
        
    }
    
    class var subTitleFont:UIFont{
        
        return UIFont.init(name: "Roboto-Medium", size: 17.0) ?? UIFont.defaultFont
        
        
    }
    
    class var largeSubTitleFont:UIFont{
        
        return UIFont.init(name: "Roboto-Medium", size: 20.0) ?? UIFont.defaultFont
        
        
    }
    
    class var smallSubTitleFont:UIFont{
        
        return UIFont.init(name: "Roboto-Medium", size: 14.0) ?? UIFont.defaultFont
        
        
    }
    
    class var smallFont:UIFont{
        
        return UIFont.init(name: "Roboto-Regular", size: 12.0) ?? UIFont.defaultFont
        
        
    }
    
    class var tinyFont:UIFont{
        
        return UIFont.init(name: "Roboto-Medium", size: 9.0) ?? UIFont.defaultFont
        
        
    }
    
    
    class var primaryButtonFont:UIFont {
        
        return UIFont.init(name: "Roboto-Bold", size: 15.0) ?? UIFont.defaultFont
    }
    
    class var secondaryButtonFont:UIFont {
        
        return UIFont.init(name: "Roboto-Bold", size: 17.0) ?? UIFont.defaultFont
    }
    
    class var planeButtonFont:UIFont {
        
        return  UIFont.init(name: "Roboto-Medium", size: 15.0) ?? UIFont.defaultFont
    }
    
    
    class var italicFont:UIFont {
        
        return  UIFont.init(name: "Roboto-Italic", size: 15.0) ?? UIFont.defaultFont
    }
    
    class var italicSmallFont:UIFont {
        
        return  UIFont.init(name: "Roboto-Italic", size: 12.0) ?? UIFont.defaultFont
    }
    
    class var newtitleFont:UIFont{
        
        return UIFont.init(name: "Roboto-Bold", size: 25.0) ?? UIFont.defaultFont
        
        
    }
}


