//
//  NKLabel.swift
//  NearKart
//
//  Created by RaviKiran B on 05/10/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit

@IBDesignable


class NKLabel: UILabel {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    @IBInspectable var textType:Int = 0{
        didSet{
            updateTextType()
        }
    }
    
    @IBInspectable var textAllignType:Int = 0{
        didSet{
            updateTextType()
        }
    }
    
    func updateTextType() {
        font = UIFont.fontForType(textType: textType)
        
        if textAllignType == 1 {
            //Reverse Allign
            textAlignment = Localize.isArabic() ? .left : .right
        }
        else if textAllignType == 2 {
            //Center Allign
            textAlignment = .center
        }
        else {
            textAlignment = Localize.isArabic() ? .right : .left
        }
        
    }
    
}
