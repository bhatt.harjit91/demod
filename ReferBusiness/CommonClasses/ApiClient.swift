//
//  APIClient.swift
//  WhatMate
//
//  Created by RaviKiran B on 22/01/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import Foundation
import Alamofire

class APIClient: NSObject {
    
    typealias CompletionHandler = (HTTPURLResponse?, NSDictionary?) -> Void
    let headers: HTTPHeaders = Alamofire.SessionManager.defaultHTTPHeaders
    //    [
    //        //AppDeviceAgentKey: AppDeviceAgentValue,
    //        "Accept": "application/json"
    //    ]
    
    func handleResponse(response:DataResponse<Any>, completionHandler: @escaping CompletionHandler) {
        print("======================================================================================")
        print("======================================================================================\n")
        print("Request: \(String(describing: response.request))")   // original url request
        print("Response: \(String(describing: response.response))") // http url response
        print("Result: \(response.result)")                         // Result serialization result
        print("Status Code: \(response.response?.statusCode ?? 0)")
        print("======================================================================================")
        print("======================================================================================\n")
        
        if let result = response.result.value {
            let JSON = result as! NSDictionary
            print(JSON)
            completionHandler(response.response,JSON)
        }
        else{
            completionHandler(response.response, nil)
        }
    }
    func GetRequest(urlString:String,params:NSDictionary?, completionHandler: @escaping CompletionHandler) {
        
        if let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            
            Alamofire.request(urlStr, method: .get, parameters: params as? Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                self.handleResponse(response: response, completionHandler: completionHandler)
            }
        }
    }
    
    func PostRequest(urlString:String,params:NSDictionary?, completionHandler: @escaping CompletionHandler) {
        
        if let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            
            Alamofire.request(urlStr, method: .post, parameters: params as? Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                self.handleResponse(response: response, completionHandler: completionHandler)
            }
        }
    }
    
    
    func uploadFile(endUrl: String, imageData: Data?, withName:String,fileName:String,mimeType:String, parameters: [String : Any]?, completionHandler: @escaping CompletionHandler){
        
        let url = endUrl /* your API url */
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if parameters != nil{
                for (key, value) in parameters ?? [:] {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }
            
            
            if let data = imageData{
                multipartFormData.append(data, withName: withName, fileName: fileName, mimeType: mimeType)
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    self.handleResponse(response: response, completionHandler: completionHandler)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionHandler(nil, nil)
            }
        }
    }
}
////

// change

///
