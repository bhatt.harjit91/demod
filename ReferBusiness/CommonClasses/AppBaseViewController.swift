//
//  AppBaseViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//
    
    import UIKit
    import CoreLocation
    import MapKit
    import WebKit
    let failedToConnectMessage = "Failed to connect, please try again later".localized()
    
    class AppBaseViewController: UIViewController ,WKNavigationDelegate, WKUIDelegate {
        
        var loaderView:LoadingView!
        
        var images = [String]()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Do any additional setup after loading the view.
            
          //  navigationController?.navigationBar.barTintColor = UIColor.primaryColor
           // navigationController?.navigationBar.tintColor = UIColor.navbarTint
            //         navigationController?.navigationBar.isTranslucent = false
            //         navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
            //        if #available(iOS 11.0, *) {
            //             navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            //        }
            //        let attributes = [
            //            NSForegroundColorAttributeName : UIColor.white,
            //            NSFontAttributeName : UIFont.systemFont(ofSize: 17)
            //        ]
            //        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
            //   navigationController?.navigationBar.titleTextAttributes = [.font:UIFont.titleFont]
            
            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            
        }
        
        func addPlaceHolderWithText(placeholder:String) -> NSMutableAttributedString
        {
            let starAttributes: [NSAttributedString.Key: Any] = [
                .font: UIFont.fontForType(textType: 6),
                .foregroundColor: UIColor.getGradientColor(index: 1).first ?? 0]
            
            let star = NSAttributedString(string:"*" , attributes: starAttributes)
            
            let starTexr = NSMutableAttributedString(string: placeholder)
            
            starTexr.append(star)
            
            return starTexr
        }
        
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        func startAnimating() {
            if loaderView == nil {
                loaderView = LoadingView()
            }
            view.addSubview(loaderView)
            loaderView.translatesAutoresizingMaskIntoConstraints = false
            let top = NSLayoutConstraint.init(item: loaderView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint.init(item: loaderView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint.init(item: loaderView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint.init(item: loaderView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
            view.addConstraints([top,bottom,leading,trailing])
            loaderView.startAnimating()
        }
        
        func startAnimatingAfterSubmit() {
            if loaderView == nil {
                loaderView = LoadingView()
            }
            view.addSubview(loaderView)
            loaderView.translatesAutoresizingMaskIntoConstraints = false
            let top = NSLayoutConstraint.init(item: loaderView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint.init(item: loaderView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint.init(item: loaderView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint.init(item: loaderView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
            view.addConstraints([top,bottom,leading,trailing])
            loaderView.startAnimatingAfterSubmit()
        }
        
        func stopAnimating() {
            if loaderView != nil {
                if loaderView.superview != nil{
                    loaderView.removeFromSuperview()
                    loaderView.stopAnimating()
                    loaderView = nil
                }
            }
        }
        
        func textToSalesSupportPriorityValue(intValue : Int) -> String
        {
            if intValue == 0
            {
                return "Low"
            }
            else if intValue == 1
            {
                return "Normal"
            }
            else if intValue == 2
            {
                return "High"
            }
            else
            {
                return "Low"
            }
        }
        
        func showBanners(attachmentArray:[NSDictionary]) {
            
            if attachmentArray.count > 0 {
                
                var str:String = ""
                
                for item in attachmentArray {
                    
                    str = item.object(forKey: "cdnPath") as! String
                    images.append(str)
                    
                }
                
                
                
            }
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeBannerViewController") as! HomeBannerViewController
            vc.images = images
            
            present(vc, animated: true, completion: nil)
            
            
        }
        
    }
    
    //MARK: Validation
    extension UIViewController{
        
        func isValidEmail(Str:String) -> Bool {
            // print("validate calendar: \(testStr)")
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: Str)
        }
    }
    
    //MARK: CountryCodeList
    extension UIViewController{
        
        func getCountryList ()-> [CountryCodeModel]{
            if let settingsURL = Bundle.main.path(forResource: "countryList".localized(), ofType: "plist".localized()) {
                
                do {
                    var settings: CountryCodeList?
                    let data = try Data(contentsOf: URL(fileURLWithPath: settingsURL))
                    let decoder = PropertyListDecoder()
                    settings = try decoder.decode(CountryCodeList.self, from: data)
                    //print("array  is \(settings?.countryList ?? [])")//prints array  is ["Good morning", "Good afternoon"]
                    return settings?.countryList ?? []
                    
                } catch {
                    print(error)
                }
            }
            return []
        }
        
        func getLocalCountryCode() -> CountryCodeModel? {
            //        let locale = Locale.current
            //      //  print(locale.regionCode ?? "No Region found")
            //
            //        let defaults = UserDefaults.standard
            //
            //        let isoCountryCode = defaults.object(forKey: "isoCountryCode")
            //
            //        if let countryCode = isoCountryCode as? String {
            //
            //                let countryList:[CountryCodeModel] =  getCountryList()
            //                let searchResult:[CountryCodeModel] = countryList.filter({( country : CountryCodeModel) -> Bool in
            //
            //                    return country.code == countryCode
            //                })
            //                if searchResult.count>0 {
            //                    return searchResult.first
            //                }
            //
            //
            //        }
            
            let locale = Locale.current
            print(locale.regionCode ?? "No Region found")
            
            if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
                
                let countryList:[CountryCodeModel] =  getCountryList()
                let searchResult:[CountryCodeModel] = countryList.filter({( country : CountryCodeModel) -> Bool in
                    return country.code == countryCode
                })
                if searchResult.count>0 {
                    return searchResult.first
                }
                
                
            }
            return nil
        }
        
        func getLocalCurrencySymbol() -> String? {
            
            let locale = Locale.current
            print(locale.regionCode ?? "No Region found")
            
            if let currencyCode = (Locale.current as NSLocale).object(forKey: .currencySymbol) as? String {
                
                    return currencyCode
                }
            
            return nil
            
        }
        func getLocalCurrencyCode() -> String? {
            
            let locale = Locale.current
            print(locale.regionCode ?? "No Region found")
            
            if let currencyCode = (Locale.current as NSLocale).object(forKey: .currencyCode) as? String {
                
                    return currencyCode
                }
            
            return nil
        }
        
        
        func getCountryCodeModel(dialCode:String) -> CountryCodeModel? {
            let countryList:[CountryCodeModel] =  getCountryList()
            let searchResult:[CountryCodeModel] = countryList.filter({( country : CountryCodeModel) -> Bool in
                return country.dial_code == dialCode
            })
            if searchResult.count>0 {
                return searchResult.first
            }
            return nil
        }
        
        func getCountryCodeModelByISO(isoCode:String) -> CountryCodeModel? {
            let countryList:[CountryCodeModel] =  getCountryList()
            let searchResult:[CountryCodeModel] = countryList.filter({( country : CountryCodeModel) -> Bool in
                return country.code == isoCode
            })
            if searchResult.count>0 {
                return searchResult.first
            }
            return nil
        }
        
        
    }
    
    
    //MARK: Notification error
    
    extension UIViewController{
        
        //    func showDismisableAlert(title:String,message:String) {
        //        let alert:UIAlertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        //         present(alert, animated: true, completion: {
        //            let deadlineTime = DispatchTime.now() + .seconds(3)
        //            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
        //
        //                alert.dismiss(animated: true, completion: nil)
        //            }
        //        })
        //    }
        
        func showErrorMessage(message:String) {
            let theHeight = view.frame.size.height
            let banner : BannerView = BannerView()
            banner.backgroundColor = UIColor.rgb(fromHex: 0xD85730)
            banner.frame = CGRect.init(x: 0, y: theHeight - 60, width: view.frame.size.width, height:60)
            banner.isHidden = true
            //UIApplication.shared.windows.last?.addSubview(banner)
            view.addSubview(banner)
            banner.messageLabel.textColor = UIColor.white
            banner.messageLabel.text = message
            
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                banner.isHidden = false
            })
            let deadlineTime = DispatchTime.now() + .seconds(3)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
                
                if let selfObj = self {
                    UIView.transition(with: selfObj.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        banner.isHidden = true
                    }, completion: {status in
                        banner.removeFromSuperview()
                    })
                }
            }
            
        }
        
        //
        
        
        func showNotificationMessage(message:String) {
            let theHeight = view.frame.size.height
            let banner : BannerView = BannerView()
            banner.backgroundColor = UIColor.rgb(fromHex: 0xD85730)
            banner.frame = CGRect.init(x: 0, y: 0, width: view.frame.size.width, height:60)
            banner.isHidden = true
            //UIApplication.shared.windows.last?.addSubview(banner)
            view.addSubview(banner)
            banner.messageLabel.textColor = UIColor.white
            banner.messageLabel.text = message
            
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                banner.isHidden = false
            })
            let deadlineTime = DispatchTime.now() + .seconds(3)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
                
                if let selfObj = self {
                    UIView.transition(with: selfObj.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        banner.isHidden = true
                    }, completion: {status in
                        banner.removeFromSuperview()
                    })
                }
            }
            
        }
        //
        
        func showCustomMessage(message:String) {
           
            let banner : BannerView = BannerView()
            banner.backgroundColor = UIColor.rgb(fromHex: 0xD85730)
            banner.frame = CGRect.init(x: 0, y: 0, width: view.frame.size.width, height:60)
            banner.isHidden = true
            //UIApplication.shared.windows.last?.addSubview(banner)
            view.addSubview(banner)
            banner.messageLabel.textColor = UIColor.white
            banner.messageLabel.text = message
            
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                banner.isHidden = false
            })
            let deadlineTime = DispatchTime.now() + .seconds(3)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
                
                if let selfObj = self {
                    UIView.transition(with: selfObj.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        banner.isHidden = true
                    }, completion: {status in
                        banner.removeFromSuperview()
                    })
                }
            }
            
        }
        func showOTPErrorMessage(message:String) {
            let theHeight = view.frame.size.height
            let banner : BannerView = BannerView()
            banner.backgroundColor = UIColor.rgb(fromHex: 0xD85730)
            banner.frame = CGRect.init(x: 0, y: 0, width: view.frame.size.width, height:60)
            banner.isHidden = true
            //UIApplication.shared.windows.last?.addSubview(banner)
            view.addSubview(banner)
            banner.messageLabel.textColor = UIColor.white
            banner.messageLabel.text = message
            
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                banner.isHidden = false
            })
            let deadlineTime = DispatchTime.now() + .seconds(3)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
                
                if let selfObj = self {
                    UIView.transition(with: selfObj.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        banner.isHidden = true
                    }, completion: {status in
                        banner.removeFromSuperview()
                    })
                }
            }
            
        }
        
        func showWarningMessage(message:String) {
            let theHeight = view.frame.size.height
            let banner : BannerView = BannerView()
            banner.backgroundColor = UIColor.rgb(fromHex: 0xF4C377)
            banner.frame = CGRect.init(x: 0, y: theHeight-60, width: view.frame.size.width, height:60)
            banner.isHidden = true
            view.addSubview(banner)
            banner.messageLabel.textColor = UIColor.black
            banner.messageLabel.text = message
            
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                banner.isHidden = false
            })
            let deadlineTime = DispatchTime.now() + .seconds(3)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
                
                if let selfObj = self {
                    UIView.transition(with: selfObj.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        banner.isHidden = true
                    }, completion: {status in
                        banner.removeFromSuperview()
                    })
                }
            }
            
        }
        
        func showInfoMessage(message:String) {
            let theHeight = view.frame.size.height
            let banner : BannerView = BannerView()
            banner.backgroundColor = UIColor.rgb(fromHex: 0xBFE0F0)
            banner.frame = CGRect.init(x: 0, y: theHeight-60, width: view.frame.size.width, height:60)
            banner.isHidden = true
            view.addSubview(banner)
            banner.messageLabel.textColor = UIColor.black
            banner.messageLabel.text = message
            
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                banner.isHidden = false
            })
            let deadlineTime = DispatchTime.now() + .seconds(3)
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
                if let selfObj = self {
                    UIView.transition(with: selfObj.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        banner.isHidden = true
                    }, completion: {status in
                        banner.removeFromSuperview()
                    })
                }
                
            }
            
        }
        
        func validateStatus(response:HTTPURLResponse?) -> Bool {
            if let statusCode = response?.statusCode{
                if statusCode == 200{
                    return true
                }
                else if statusCode == 401{
                    
                    clearUserDetails()
                    
                    sessionExpiredMessage()
                }
            }
            return false
        }
        
        func sessionExpiredMessage(){
            weak var weakSelf = self
            let alert = UIAlertController.init(title: "Session Expired".localized(), message: "Your session has been expired, please relogin to proceed.".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Cancel".localized(), style: .default, handler: {action in
                weakSelf?.navigationController?.popToRootViewController(animated: true)
            }))
            alert.addAction(UIAlertAction.init(title: "Login".localized(), style: .default, handler: {action in
                weakSelf?.openSignInPage()
                
            }))
            navigationController?.present(alert, animated: true, completion: nil)
        }
        
        func openSignInPage() {
            let storyBoardL = UIStoryboard.init(name: MainSB, bundle: nil)
            navigationController?.present(storyBoardL.instantiateInitialViewController()!, animated: true, completion: nil)
            
        }
        
        func openNaviagationMap(latitude:Double,longitude:Double,name:String){
            
            let appDomain: String = "comgooglemaps://"
            let browserDomen: String = "https://www.google.co.in/maps/dir/"
            let directionBody: String = "/\(latitude),\(longitude)"
            let domainMap = "http://maps.google.com/?daddr=\(latitude),\(longitude)&directionsmode=Driving"
            // Make route with google maps application
            if let appUrl = URL(string: appDomain), UIApplication.shared.canOpenURL(appUrl) {
                guard let appFullPathUrl = URL(string: domainMap) else { return }
                UIApplication.shared.openURL(appFullPathUrl)
                
                // If user don't have an application make route in browser
                
                if let browserUrl = URL(string: browserDomen), UIApplication.shared.canOpenURL(browserUrl) {
                    guard let browserPathUrl = URL(string: browserDomen + directionBody) else { return }
                    UIApplication.shared.canOpenURL(browserPathUrl)
                }
                
            } else {
                //let latitude:CLLocationDegrees =  lat1.doubleValue
                //let longitude:CLLocationDegrees =  lng1.doubleValue
                
                let regionDistance:CLLocationDistance = 10000
                let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
                ]
                let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.name = name
                mapItem.openInMaps(launchOptions: options)
                //guard let browserFullPathUrl = URL(string: browserDomen + directionBody) else { return }
                //UIApplication.shared.openURL(browserFullPathUrl)
            }
            
        }
        
        func openWebView(urlString:String, title:String, subTitle:String){
            let webViewer:WebViewViewController = WebViewViewController(nibName:"WebViewViewController",bundle:nil)
            webViewer.urlString = urlString
            webViewer.navTitle = title
             navigationController?.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
            navigationController?.pushViewController(webViewer, animated: true)
        }
        
        
        func initiatePhoneCall(phoneNumber:String){
            if let url = URL(string: "tel://\(phoneNumber)"),
                UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            } else {
                // add error message here
                showErrorMessage(message: "Failed to initiae call".localized())
            }
        }
        
        func initiateEmail(mailStr:String) {
            let email = mailStr
            if let url = URL(string: "mailto:\(email)".localized()) {
              if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
              } else {
                UIApplication.shared.openURL(url)
              }
            }else {
                // add error message here
                showErrorMessage(message: "Failed to initiae email".localized())
            }
            
        }
        
        
        func initiatePrintOrder(orderNo:Int, BranchCode:Int){
            //        let printer:PrintingViewController = PrintingViewController(nibName:"PrintingViewController",bundle:nil)
            //        printer.orderNo = orderNo
            //        printer.branchCode = BranchCode
            //        printer.definesPresentationContext = true
            //        printer.modalPresentationStyle = .overCurrentContext
            //        navigationController?.present(printer, animated: true, completion: nil)
        }
    }

extension NSLocale {
    class func locales1(countryName1 : String) -> String {
        let locales : String = ""
        for localeCode in NSLocale.isoCountryCodes {
            let countryName = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: localeCode)
            if countryName1.lowercased() == countryName?.lowercased() {
                return localeCode
            }
        }
        return locales
    }
    
}

extension UIView {

    func aspectRation(_ ratio: CGFloat) -> NSLayoutConstraint {

        return NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: self, attribute: .width, multiplier: ratio, constant: 0)
    }
}
