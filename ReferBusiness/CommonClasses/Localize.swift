//
//  Localize.swift
//  NearKart
//
//  Created by RaviKiran B on 05/09/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import Foundation
import UIKit
/// Internal current language key
let NKCurrentLaunguageKey = "LCLCurrentLanguageKey"

/// Default language. English. If English is unavailable defaults to base localization.
let NKDefaultLanguage = "en"

/// Base bundle as fallback.
let NKBaseBundle = "Base"

class Localize:NSObject{
    /**
     Current language ID
     - Returns: The current language id. Int
     */
    class var currentLanguageID:Int{
        switch (currentLanguage){
        case "en":
            return 1
        case "ar":
            return 2
        case "es":
            return 3
        case "th":
            return 4
        case "fr":
            return 5
        case "pt-PT":
             return 6
        default:
            return 1
        }
        
    }
    /**
     List available languages
     - Returns: Array of available languages.
     */
    open class func availableLanguages(_ excludeBase: Bool = false) -> [String] {
        var availableLanguages = Bundle.main.localizations
        // If excludeBase = true, don't include "Base" in available languages
        if let indexOfBase = availableLanguages.index(of: "Base") , excludeBase == true {
            availableLanguages.remove(at: indexOfBase)
        }
        return availableLanguages
    }
    /**
     Current language
     - Returns: The current language. String.
     */
    class var currentLanguage:String{
        //return UserDefaults.standard.object(forKey: NKCurrentLaunguageKey) as! String
        
        if let currentLanguage = UserDefaults.standard.object(forKey: NKCurrentLaunguageKey) as? String {
            return currentLanguage
        }
        return ""
    }
    
    /**
     TextAlignMent
     - Returns: The current language. String.
     */
    class func textAlignmentForLanguage() -> NSTextAlignment {
        if Localize.currentLanguage == "ar"{
            return .right
        }
        
        return .left
    }
    /**
     Change the current language
     - Parameter language: Desired language.
     */
    class func setCurrentLanguage(selectedLanguage:String) {
        UserDefaults.standard.set(selectedLanguage, forKey: NKCurrentLaunguageKey)
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set([selectedLanguage], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        UIApplication.shared.delegate?.setSemantics()
    }
    
    /**
     Get the current language's display name for a language.
     - Parameter language: Desired language.
     - Returns: The localized string.
     */
    open class func displayNameForLanguage(_ language: String) -> String {
        let locale : NSLocale = NSLocale(localeIdentifier: Localize.currentLanguage)
        if let displayName = locale.displayName(forKey: NSLocale.Key.identifier, value: language) {
            return displayName
        }
        return String()
    }
    
    class func isArabic() -> Bool{
        if Localize.currentLanguage == "ar"{
            return true
        }
        
        return false
    }
    
    class func DoTheMagic() {
        
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(_:value:table:)))
        MethodSwizzleGivenClassName(cls: UIApplication.self, originalSelector: #selector(getter: UIApplication.userInterfaceLayoutDirection), overrideSelector: #selector(getter: UIApplication.cstm_userInterfaceLayoutDirection))
        //        MethodSwizzleGivenClassName(cls: UITextField.self, originalSelector: #selector(UITextField.layoutSubviews), overrideSelector: #selector(UITextField.cstmlayoutSubviews))
        //        MethodSwizzleGivenClassName(cls: UILabel.self, originalSelector: #selector(UILabel.layoutSubviews), overrideSelector: #selector(UILabel.cstmlayoutSubviews))
        
        
    }
    
}

extension Bundle {
    @objc func specialLocalizedStringForKey(_ key: String, value: String?, table tableName: String?) -> String {
        if self == Bundle.main {
            //let currentLanguage = L102Language.currentAppleLanguage()
            var currentLanguage = Localize.currentLanguage
            if currentLanguage.count==0 {
                currentLanguage = Locale.preferredLanguages.first!
            }
            
            var bundle = Bundle();
            if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
                bundle = Bundle(path: _path)!
            }else
                if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
                    bundle = Bundle(path: _path)!
                } else {
                    let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
                    bundle = Bundle(path: _path)!
            }
            return (bundle.specialLocalizedStringForKey(key, value: value, table: tableName))
        } else {
            return ( specialLocalizedStringForKey(key, value: value, table: tableName))
        }
    }
}

extension UIApplication {
    @objc var cstm_userInterfaceLayoutDirection : UIUserInterfaceLayoutDirection {
        get {
            var direction = UIUserInterfaceLayoutDirection.leftToRight
            if Localize.isArabic() {
                direction = .rightToLeft
            }
            return direction
        }
    }
}

/// Exchange the implementation of two methods of the same Class
func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
    if let origMethod: Method = class_getInstanceMethod(cls, originalSelector){
        if let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector){
            if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
                class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
            } else {
                method_exchangeImplementations(origMethod, overrideMethod);
            }
        }
        
    }
    
}

public extension String {
    
    /**
     Swift 2 friendly localization syntax, replaces NSLocalizedString.
     
     - parameter tableName: The receiver’s string table to search. If tableName is `nil`
     or is an empty string, the method attempts to use `Localizable.strings`.
     
     - parameter bundle: The receiver’s bundle to search. If bundle is `nil`,
     the method attempts to use main bundle.
     
     - returns: The localized string.
     */
    func localized() -> String {
        ////print("Localization for : \(self)")
        let bundle: Bundle = Bundle.main
        var currentLanguage = Localize.currentLanguage
        if currentLanguage.count==0 {
            currentLanguage = Locale.preferredLanguages.first!
        }
        if let path = bundle.path(forResource: currentLanguage, ofType: "lproj"),
            let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: "")
            
        }
        else if let path = bundle.path(forResource: NKBaseBundle, ofType: "lproj"),
            let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: "")
        }
        return self
    }
    
    
}

public extension UIViewController{
    func textAlignmentForLanguage() -> NSTextAlignment {
        if Localize.currentLanguage == "ar"{
            return .right
        }
        
        return .natural
    }
}

extension RCTextField{
    func textAlignmentForLanguage() -> NSTextAlignment {
        if Localize.currentLanguage == "ar"{
            return .right
        }
        
        return .natural
    }
}
