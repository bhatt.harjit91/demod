//
//  NKTextView.swift
//  ICanRefer
//
//  Created by TalentMicro on 09/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

@IBDesignable
class NKTextView: UITextView {
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        super.draw(rect)
        
        messageLable.frame = CGRect.init(x: 15, y: rect.size.height-15, width: rect.size.width-30, height: 13)
        messageLable.font = UIFont.textErrorFont
        if messageLable.superview == nil{
            addSubview(messageLable)
        }
        
        placeHolderLabel.frame = CGRect.init(x: 10, y: 10, width: rect.size.width-20, height: 21)
        placeHolderLabel.font = UIFont.bodyFont
        placeHolderLabel.textColor = UIColor.lightGray
        if placeHolderLabel.superview == nil{
            addSubview(placeHolderLabel)
        }
        updatePlaceHolder()
        NotificationCenter.default.removeObserver(self, name: UITextView.textDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(textChanged), name: UITextView.textDidChangeNotification, object: nil)
    }
    
    
    @objc func textChanged(notification:NSNotification){
        updatePlaceHolder()
    }
    
    private func updatePlaceHolder() {
        if text.count == 0 {
            
            if changePlaceHolderColor == true {
                
                let starAttributes: [NSAttributedString.Key: Any] = [
                    .font: UIFont.fontForType(textType: 6),
                    .foregroundColor: UIColor.getGradientColor(index: 1).first ?? 0]
                
                let textAttributes: [NSAttributedString.Key: Any] = [
                    .font: UIFont.fontForType(textType: 6),
                    .foregroundColor: UIColor.lightGray]
                
                
                let star = NSAttributedString(string:"*" , attributes: starAttributes)
                
                let starTexr = NSMutableAttributedString(string: placeholder , attributes: textAttributes)
                
                starTexr.append(star)
                
                if placeholder.isEmpty
                {
                    placeHolderLabel.text = placeholder
                }
                else
                {
                    placeHolderLabel.attributedText = starTexr
                }
            }
            else
            {
                placeHolderLabel.text = placeholder
            }
        }
        else {
            placeHolderLabel.text = ""
            text = text.removeWhitespaceWithNextLine()
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    let placeHolderLabel:UILabel = UILabel()
    var changePlaceHolderColor:Bool = false
    var messageLable:UILabel = UILabel.init()
    @IBInspectable var placeholder:String = "" {
        didSet {
            updatePlaceHolder()
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0{
        didSet{
            updateTextType()
        }
    }
    
    @IBInspectable var textType:Int = 0{
        didSet{
            updateTextType()
        }
    }
    
    @IBInspectable var textAllignType:Int = 0{
        didSet{
            updateTextType()
        }
    }
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    func updateTextType() {
        layer.borderWidth = borderWidth
        layer.borderColor = UIColor.lightGray.cgColor
        font = UIFont.fontForType(textType: textType)
        layer.cornerRadius = (frame.size.height > 20) ? 5 : frame.size.height/10
        layer.masksToBounds = true
        if textAllignType == 1 {
            //Reverse Allign
            textAlignment = Localize.isArabic() ? .left : .right
            messageLable.textAlignment = Localize.isArabic() ? .right : .left
        }
        else if textAllignType == 2 {
            //Center Allign
            textAlignment = .center
            messageLable.textAlignment = .center
        }
            
        else {
            textAlignment = Localize.isArabic() ? .right : .left
            messageLable.textAlignment = Localize.isArabic() ? .left : .right
        }
        placeHolderLabel.textAlignment = textAlignment
    }
    
    
    func showError(message:String) {
        messageLable.text = message
        messageLable.textColor = UIColor.red
    }
}

