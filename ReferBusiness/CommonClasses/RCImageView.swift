//
//  RCImageView.swift
//  ICanRefer
//
//  Created by TalentMicro on 24/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation
@IBDesignable
class RCImageView: UIImageView {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupUI()
    }
    
    @IBInspectable var cornerRadius:CGFloat = 0.0 {
        didSet {
            setupUI()
        }
    }
    
    func setupUI() {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        clipsToBounds = true
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.lightGray.cgColor
    }
    
}
