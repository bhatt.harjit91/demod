//
//  YouTubePlayerViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 06/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit
import WebKit

class YouTubePlayerViewController: AppBaseViewController {
    
    
    
    let webView = WKWebView()
    var videoLink:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        startAnimating()
        self.view.backgroundColor = .white
        webView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height/2.2)
        self.view.addSubview(webView)
        
        webView.navigationDelegate = self
        webView.uiDelegate = self
 
           if videoLink != nil {
              guard let youtubeURL = URL(string: "https://www.youtube.com/embed/\(videoLink?.youtubeID ?? "")") else { return }
                  
                  webView.navigationDelegate = self
                  webView.uiDelegate = self
                  webView.load(URLRequest(url: youtubeURL))
         }
        
    }
    
 
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
       stopAnimating()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
        stopAnimating()
    }
    
    
}
