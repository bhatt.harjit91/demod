//
//  PlayerView.swift
//  iCanRefer
//
//  Created by TalentMicro on 04/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PlayerView: UIView {
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    var player: AVPlayer? {
        get {
            return playerLayer.player;
        }
        set {
            playerLayer.player = newValue
        }
    }
}
