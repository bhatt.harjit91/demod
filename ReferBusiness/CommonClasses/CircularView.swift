//
//  CircularView.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

@IBDesignable

class CircularView: UIView {
    
    private var shadowLayer: CAShapeLayer!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupUI()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    
    @IBInspectable var borderWidth:CGFloat = 0{
        didSet{
            setupUI()
        }
    }
    
    func setupUI() {
        layer.cornerRadius = (frame.size.height > 20) ? 5 : frame.size.height/10
        layer.masksToBounds = true
        layer.borderWidth = borderWidth
        layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func updateShadow() {
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
            shadowLayer.fillColor = backgroundColor?.cgColor
            
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            shadowLayer.shadowOpacity = 0.2
            shadowLayer.shadowRadius = 3
            
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
}
