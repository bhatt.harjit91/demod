//
//  UIViewController+Encryption.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CommonCrypto


extension UIViewController {
     
    func encryptParams(params:NSDictionary) -> String {
        if let key = getSecretKey(){
            let iv      = "1234567891234567"

            let crypt = DataCrypto.init()
            let result = crypt.encryptedString(params as! [AnyHashable : Any], key: key, iv: iv)

            return result
        }
        return ""
    }
    
    func decryptData(encryptedString:String) -> NSDictionary? {
        if let key = getSecretKey(){
            let iv      = "1234567891234567"
            
            let crypt = DataCrypto.init()
            let result = crypt.decryptData(encryptedString, key: key, iv: iv)
            
            return result as NSDictionary
        }
        
        return nil
    }
    
    
}
 


