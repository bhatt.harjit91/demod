//
//  LoadingView.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    
    var imageView : UIImageView!
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        super.draw(rect)
        
    }
    
    func startAnimating() {
        let color = UIColor.init(red: 154.0 / 255.0, green: 154.0 / 255.0, blue: 154.0 / 255.0, alpha: 1)
        backgroundColor = color
        
        let imageName = "Loader"
        let image = UIImage(named: imageName)
        imageView = UIImageView(image: image!)
        self.addSubview(imageView)
        //Imageview on Top of View
        self.bringSubviewToFront(imageView)
        imageView.center = self.center
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let xConstraint = NSLayoutConstraint.init(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let yConstraint = NSLayoutConstraint.init(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0)
        addConstraints([xConstraint,yConstraint])
        
        let rotationAnimation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: .pi * 2.0)
        rotationAnimation.duration = 1;
        rotationAnimation.isCumulative = true;
        rotationAnimation.repeatCount = .infinity;
        self.imageView?.layer.add(rotationAnimation, forKey: "rotationAnimation")
        
    }
    
    func startAnimatingAfterSubmit() {
        let color = UIColor.init(red: 154.0 / 255.0, green: 154.0 / 255.0, blue: 154.0 / 255.0, alpha: 0.5)
        backgroundColor = color
        
        let imageName = "Loader"
        let image = UIImage(named: imageName)
        imageView = UIImageView(image: image!)
        self.addSubview(imageView)
        //Imageview on Top of View
        self.bringSubviewToFront(imageView)
        imageView.center = self.center
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let xConstraint = NSLayoutConstraint.init(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let yConstraint = NSLayoutConstraint.init(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0)
        addConstraints([xConstraint,yConstraint])
        
        let rotationAnimation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: .pi * 2.0)
        rotationAnimation.duration = 1;
        rotationAnimation.isCumulative = true;
        rotationAnimation.repeatCount = .infinity;
        self.imageView?.layer.add(rotationAnimation, forKey: "rotationAnimation")
        
    }
    
    func stopAnimating() {
        
        self.imageView?.layer.removeAnimation(forKey: "rotationAnimation")
        
        self.imageView.removeFromSuperview()
    }
    
}
