//
//  NKButton.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

enum btnTypeEnum:Int {
    case planeButton = 0
    case applyTagColor
    case applyColor
    case planeWhiteWithShadow
    case square
    case noshadow
}

@IBDesignable



class NKButton: UIButton {
    
    
    var gradient = CAGradientLayer()
    private var shadowLayer: CAShapeLayer = CAShapeLayer()
    let containerView = UIView()
    var colorsToApply:[UIColor] = [UIColor.white,UIColor.white]
    //    {
    //        didSet {
    //            setUpButtonUI()
    //        }
    //    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        //setUpButtonUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpButtonUI()
    }
    @IBInspectable var BtnType:Int = 0 {
        didSet {
            setUpButtonUI()
        }
    }
    
    @IBInspectable var colorIndex:Int = 0 {
        didSet {
            setUpButtonUI()
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var NKButtonAllignment:Int = 0 {
        didSet {
            updateAllginMent()
        }
    }
    /*
     file:///Users/ravikiranb/Desktop/ProjectsRepo/NearKartGitRepo/NearKart/NearKart/ViewViewModel/CommonVC/QRCodeScanner/QRScannerViewController.xib: error: IB Designables: Failed to render and update auto layout status for UIView (i5M-Pr-FkT): The bundle “NearKart.app” couldn’t be loaded because its executable couldn’t be located.
     
     */
    //allignType
    func updateAllginMent() {
        if NKButtonAllignment == 1 {
            //Natural Allign
            contentHorizontalAlignment = Localize.isArabic() ? .right : .left
        }
        else if NKButtonAllignment == 2 {
            //Reverse Allign
            contentHorizontalAlignment = Localize.isArabic() ? .left : .right
        }
        else {
            contentHorizontalAlignment = .center
        }
    }
    func setUpButtonUI() {
        updateAllginMent()
        
        if BtnType == btnTypeEnum.applyTagColor.rawValue{
            setUpButtonWithColors(colors: UIColor.getGradientColor(index: colorIndex), isWhite: false)
        }
        else if BtnType == btnTypeEnum.applyColor.rawValue{
            setUpButtonWithColors(colors: colorsToApply,isWhite: false)
            //setUpButtonWithColors(colors: UIColor.getGradientColor(index: tag))
            
        }
        else if BtnType == btnTypeEnum.planeWhiteWithShadow.rawValue {
            setUpButtonWithColors(colors: [UIColor.white,UIColor.white],isWhite: true)
        }
        else if BtnType == btnTypeEnum.square.rawValue {
            setupSquare(colors: UIColor.getGradientColor(index: colorIndex), isWhite: false)
        }
        else if BtnType == btnTypeEnum.noshadow.rawValue {
           setUpNewButtonWithColors(colors: [UIColor.white,UIColor.white],isWhite: true)
        }
            
        else {
            setupDefault()
        }
        
    }
    
    func setupDefault() {
        titleLabel?.font = UIFont.planeButtonFont
        gradient.removeFromSuperlayer()
        shadowLayer.removeFromSuperlayer()
        
        let bgColor = UIColor.clear
        //        if let spView = superview {
        //            bgColor = spView.backgroundColor ?? UIColor.white
        //        }
        let colors = UIColor.getGradientColor(index: colorIndex)
        let color:UIColor = colors.first ?? UIColor.primaryColor
        backgroundColor = bgColor
        tintColor = color
        layer.borderColor = color.cgColor
        setTitleColor(color, for: .normal)
        setTitleColor(color, for: .selected)
        layer.cornerRadius = (frame.size.height > 20) ? 5 : frame.size.height/10
        
        
        layer.masksToBounds = true;
    }
    
    func setupSquare(colors:[UIColor], isWhite:Bool) {
        gradient.removeFromSuperlayer()
        
        layer.cornerRadius = (frame.size.height > 20) ? 30 : frame.size.height/2
        
        //layer.masksToBounds = true;
        // gradient colors in order which they will visually appear
        gradient.colors = colors.map { $0.cgColor }
        
        // Gradient from left to right
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        // set the gradient layer to the same size as the view
        gradient.frame = bounds
        // add the gradient layer to the views layer for rendering
        gradient.cornerRadius = layer.cornerRadius
        layer.insertSublayer(gradient, at: 1)
        titleLabel?.font = UIFont.planeButtonFont
        
        let color = isWhite ? UIColor.primaryColor : UIColor.white
        tintColor = color
        setTitleColor(color, for: .normal)
        layer.borderColor = color.cgColor
        
        layer.masksToBounds = true;
        setUpShadow()
    }
    
    func setUpButtonWithColors(colors:[UIColor], isWhite:Bool) {
        gradient.removeFromSuperlayer()
        
        layer.cornerRadius = (frame.size.height > 20) ? 5 : frame.size.height/10
        
        //layer.masksToBounds = true;
        // gradient colors in order which they will visually appear
        gradient.colors = colors.map { $0.cgColor }
        
        // Gradient from left to right
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        // set the gradient layer to the same size as the view
        gradient.frame = bounds
        // add the gradient layer to the views layer for rendering
        gradient.cornerRadius = layer.cornerRadius
        layer.insertSublayer(gradient, at: 1)
        
        titleLabel?.font = UIFont.primaryButtonFont
        let color = isWhite ? UIColor.getGradientColor(index: 0).first! : UIColor.white
        tintColor = color
        setTitleColor(color, for: .normal)
        layer.borderColor = color.cgColor
        
        setUpShadow()
        
    }
    
    func setUpShadow()  {
        shadowLayer.removeFromSuperlayer()
        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius).cgPath
        let bgColor = UIColor.clear
        //        if let spView = superview {
        //            bgColor = spView.backgroundColor ?? UIColor.clear
        //        }
        
        backgroundColor = bgColor
        shadowLayer.fillColor = bgColor.cgColor
        
        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = 1
        
        layer.insertSublayer(shadowLayer, at: 0)
        layer.masksToBounds = false
    }
    
    func setUpNewButtonWithColors(colors:[UIColor], isWhite:Bool) {
       
        gradient.removeFromSuperlayer()
        
        layer.cornerRadius = (frame.size.height > 20) ? 5 : frame.size.height/10
        
        //layer.masksToBounds = true;
        // gradient colors in order which they will visually appear
        gradient.colors = colors.map { $0.cgColor }
        
        // Gradient from left to right
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        // set the gradient layer to the same size as the view
        gradient.frame = bounds
        // add the gradient layer to the views layer for rendering
        gradient.cornerRadius = layer.cornerRadius
        layer.insertSublayer(gradient, at: 1)
        
        titleLabel?.font = UIFont.primaryButtonFont
        let color = isWhite ? UIColor.getGradientColor(index: 1).first : UIColor.white
        tintColor = color
        setTitleColor(color, for: .normal)
        layer.borderColor = color?.cgColor
        
        setUpShadow()
        
    }
}


