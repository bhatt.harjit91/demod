//
//  MotionView.swift
//  iCanRefer
//
//  Created by Hirecraft on 22/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class MotionView: UIImageView {

    override func didMoveToSuperview() {
               let min = CGFloat(-7)
               let max = CGFloat(7)
                     
               let xMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .tiltAlongHorizontalAxis)
               xMotion.minimumRelativeValue = min
               xMotion.maximumRelativeValue = max
                     
               let yMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .tiltAlongVerticalAxis)
               yMotion.minimumRelativeValue = min
               yMotion.maximumRelativeValue = max
                     
               let motionEffectGroup = UIMotionEffectGroup()
               motionEffectGroup.motionEffects = [xMotion,yMotion]

               self.addMotionEffect(motionEffectGroup)
    }
   
    override func draw(_ rect: CGRect) {
        super.draw(rect)
       
        
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
    }
    

}
 
protocol DataTypeGen {
    
}

class MotionGenClass <T:DataTypeGen> {
    
}

