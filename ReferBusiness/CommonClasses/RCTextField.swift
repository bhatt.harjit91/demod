//
//  RCTextField.swift
//  NearKart
//
//  Created by RaviKiran B on 11/09/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//
private var kAssociationKeyMaxLength: Int = 0
import UIKit

@objc public enum txtAlignType: Int {
    case natural = 0 //Language specific
    case leftAlign = 1 //Force leftAlign
    case rightAlign = 2 //Force rightAlign
    case centerAlign = 3 //Force centerAlign
}

@objc public enum txtFieldType: Int {
    case normal = 0
    case dropDown = 1
    case datePicker = 2
    case dateTimePicker = 3
    case timePicker = 4
    case monthYearPicker = 5
}

protocol RCTextFieldDelegate:UITextFieldDelegate {
    func didChangeDate(textField:RCTextField)
    func didChangeOption(textField:RCTextField)
}
extension RCTextFieldDelegate {
    
    func didChangeDate(textField:RCTextField){
        
    }
    func didChangeOption(textField:RCTextField) {
      
    }
}

@IBDesignable

class RCTextField: UITextField {
    
    
    var messageLable:UILabel = UILabel.init()
    var isMandotry:Bool = false
    var maximumCharacterAllowed = 999
    var didEditOnce:Bool = false
    var pickerOptions:[String]?
    var dropDownPicker:UIPickerView?
    let dateTimePicker:UIDatePicker = UIDatePicker.init()
    var RCDelegate:RCTextFieldDelegate?
    var textTag:Int = 0
    
    var selectedDate:Date = Date.init() {
        didSet {
            updateDateString()
        }
    }
    var selectedOption:String = "" {
        didSet {
            updateSelectedOption()
        }
    }
    @IBInspectable var RCTextAlignment:Int = 0{
        didSet{
            updateAlignment()
        }
    }
    
    @IBInspectable var RCTextFieldType:Int = 0{
        didSet{
            setTextFieldUI()
        }
    }
    
    @IBInspectable var borderWidth:CGFloat = 0{
        didSet{
            setTextFieldUI()
        }
    }
    
    func updateAlignment() {
        switch RCTextAlignment {
        case txtAlignType.leftAlign.rawValue:
            textAlignment = .left
            break
        case txtAlignType.rightAlign.rawValue:
            textAlignment = .right
            break
        case txtAlignType.centerAlign.rawValue:
            textAlignment = .center
            break
        default:
            textAlignment = Localize.isArabic() ? .right : .left
            break
        }
        if textAlignment == .right {
            messageLable.textAlignment = .left
        }
        else{
            messageLable.textAlignment = .right
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setTextFieldUI()
        textColor = isEnabled ? UIColor.black:UIColor.lightGray
        
        messageLable.frame = CGRect.init(x: 15, y: rect.size.height-15, width: rect.size.width-30, height: 13)
        messageLable.font = UIFont.textErrorFont
        if messageLable.superview == nil{
            addSubview(messageLable)
        }
        updateAlignment()
        addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    
    func setTextFieldUI() {
        
        
        font = UIFont.bodyFont
        
        layer.cornerRadius = (frame.size.height > 20) ? 5 : frame.size.height/10
        layer.masksToBounds = true
        layer.borderWidth = borderWidth
        layer.borderColor = UIColor.lightGray.cgColor
        
        if RCTextFieldType > 0 {
            
            
            switch RCTextFieldType {
            case txtFieldType.datePicker.rawValue:
                setupDatePicker(mode: UIDatePicker.Mode.date)
                break
            case txtFieldType.dateTimePicker.rawValue:
                setupDatePicker(mode: UIDatePicker.Mode.dateAndTime)
                break
            case txtFieldType.timePicker.rawValue:
                setupDatePicker(mode: UIDatePicker.Mode.time)
                break
            case txtFieldType.monthYearPicker.rawValue:
                setupDatePicker(mode: UIDatePicker.Mode.date)
                break
            default:
                dropDownPicker = UIPickerView.init()
                dropDownPicker?.delegate = self
                inputView = dropDownPicker
                
                updateSelectedOption()
                break
            }
            
            
        }
        else {
            inputView = nil
        }
    }
    
    func setupDatePicker(mode:UIDatePicker.Mode){
        //let picker = UIDatePicker.init()
        dateTimePicker.datePickerMode = mode //UIDatePicker.Mode.date
        dateTimePicker.addTarget(self, action: #selector(didChangeDate(datePicker:)), for: .valueChanged)
        dateTimePicker.date = selectedDate
        inputView = dateTimePicker
        updateDateString()
    }
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    func showError(message:String) {
        messageLable.text = message
        messageLable.textColor = UIColor.red
    }
    
    func removeError() {
           messageLable.text = ""
           messageLable.textColor = UIColor.clear
       }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
    }
    
    @objc func didChangeDate(datePicker:UIDatePicker){
        selectedDate = datePicker.date
        updateDateString()
        RCDelegate?.didChangeDate(textField: self)
    }
    
    func updateSelectedOption() {
        text = selectedOption
        if !(selectedOption.count > 0) {
            dropDownPicker?.selectRow(0, inComponent: 0, animated: false)
        }

    }
    
    func updateDateString () {
        switch RCTextFieldType {
        case txtFieldType.datePicker.rawValue:
            text = selectedDate.dateString()
            break
        case txtFieldType.dateTimePicker.rawValue:
            text = selectedDate.dateTimeString()
            break
        case txtFieldType.timePicker.rawValue:
            text = selectedDate.timeString()
            break
        case txtFieldType.monthYearPicker.rawValue:
            text = selectedDate.yearString()
            break
        default:
            text = ""
            break
        }
    }
    
   override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
         
       if tag == 100 {
        
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
                   return false
               }
          }
    
        return super.canPerformAction(action, withSender: sender)
    }
    
    @IBInspectable var maxLength: Int {
          get {
              if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                  return length
              } else {
                  return Int.max
              }
          }
          set {
              objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
              addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
          }
      }

      @objc func checkMaxLength(textField: UITextField) {
          guard let prospectiveText = self.text,
              prospectiveText.count > maxLength
              else {
                  return
          }

          let selection = selectedTextRange

          let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
          let substring = prospectiveText[..<indexEndOfText]
          text = String(substring)

          selectedTextRange = selection
      }
    
    
}

extension UITextField{
    override open var isEnabled: Bool{
        didSet{
            textColor = isEnabled ? UIColor.black:UIColor.lightGray
        }
    }
}

extension RCTextField:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerOptions?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerOptions?[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedOption = pickerOptions?[row] ?? ""
        text = selectedOption
        
         RCDelegate?.didChangeOption(textField: self)
    }
}

@IBDesignable
extension RCTextField {

    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }

    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
          //  rightViewMode = .always
        }
    }
}
