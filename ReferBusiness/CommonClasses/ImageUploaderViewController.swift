//
//  ImageUploaderViewController.swift
//  NearKart
//
//  Created by RaviKiran B on 10/10/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit
import IGRPhotoTweaks
import MobileCoreServices

protocol ImageUploaderViewControllerDelegate {
    func didFinishSelectingImage(image:UIImage)
}

class ImageUploaderViewController: AppBaseViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate,IGRPhotoTweakViewControllerDelegate  {
    
    typealias didFinishPickingImage = (UIImage) -> Void
    typealias didFinishUploadingImage = (Bool,String) -> Void
    typealias didFinishUploadingLogoImage = (Bool,String) -> Void
    typealias didFinishUploadingHelpDeskImage = (Bool,String,String) -> Void
    
    var imagePicker: UIImagePickerController!
    var isUploadingProceess = false
    var profilePicUrl:String = ""
    var isAttachment:Bool = false
    var isBanner:Bool = false
    var isDocument:Bool = false
    var isLogo:Bool = false
    var isThumbnail:Bool = false
    
    var didPickImage:didFinishPickingImage?
    var didUploadImage:didFinishUploadingImage?
    var didUploadLogoImage:didFinishUploadingLogoImage?
    var didUploadHelpDeskImage:didFinishUploadingHelpDeskImage?
    @IBOutlet weak var imageSpinner: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    func checkForCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            openCamera()
        }
        else
        {
            let actionController: UIAlertController = UIAlertController(title: "Camera is not available".localized(),message: "", preferredStyle: .alert)
            actionController.view.tintColor = UIColor.btnLinkColor
            let cancelAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .cancel) { action -> Void  in
            }
            actionController.addAction(cancelAction)
            self.present(actionController, animated: true, completion: nil)
        }
    }
    func checkForPhotos(){
        if(UIImagePickerController.isSourceTypeAvailable(.photoLibrary))
        {
            openPhotLibrary()
        }
        else
        {
            let actionController: UIAlertController = UIAlertController(title: "Photos are not available".localized(),message: "", preferredStyle: .alert)
            actionController.view.tintColor = UIColor.btnLinkColor
            let cancelAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .cancel) { action -> Void  in
            }
            actionController.addAction(cancelAction)
            self.present(actionController, animated: true, completion: nil)
        }
    }
    
    func openImagePicker() {
        weak var weakSelf = self
        let actionSheet:UIAlertController = UIAlertController.init(title: "Select".localized(), message: "", preferredStyle: .actionSheet)
        actionSheet.view.tintColor = UIColor.btnLinkColor
        actionSheet.addAction(UIAlertAction.init(title: "Camera".localized(), style: .default, handler: {action in
            weakSelf?.checkForCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Photo Library".localized(), style: .default, handler: {action in
            weakSelf?.checkForPhotos()
        }))
        
        if isDocument{
            actionSheet.addAction(UIAlertAction.init(title: "Document".localized(), style: .default, handler: {action in
                weakSelf?.openImportDocumentPicker()
            }))
        }
        
        actionSheet.addAction(UIAlertAction.init(title: "Cancel".localized(), style: .default, handler: {action in
            if let closure =  weakSelf?.didUploadImage {
                closure(false,"")
            }
            if let closure =  weakSelf?.didUploadLogoImage {
                closure(false,"")
             }
            if let closure =  weakSelf?.didUploadHelpDeskImage {
                closure(false,"", "")
            }
        }))
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            present(actionSheet, animated: true, completion: nil)
            break
            
        case .pad:
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            present(actionSheet, animated: true, completion: nil)
            break
            
        case .unspecified:
            break
        case .tv:
            break
        case .carPlay:
            break
        }
    }
    
    func  openCamera() {
        weak var weakSelf = self
        checkCameraPermisson(completionHander: {status in
            if status{
                weakSelf?.openImagePIcker(type: 0)
            }
        })
    }
    
    func openPhotLibrary() {
        weak var weakSelf = self
        checkPhotoLibraryPermission(completionHander: {status in
            if status{
                weakSelf?.openImagePIcker(type: 1)
            }
        })
    }
    
    func openImagePIcker(type:Int) {
        imagePicker = UIImagePickerController.init()
        imagePicker.sourceType = type == 1 ? .photoLibrary : .camera
        //imagePicker.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
        imagePicker.delegate = self
        if isBanner {
            imagePicker.allowsEditing = false
        }
        else
        {
            imagePicker.allowsEditing = true
        }
        imagePicker.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor : UIColor.white]
        imagePicker.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
        if let closure =  didUploadImage {
            closure(false,"")
        }
        if let closure =  didUploadLogoImage {
           closure(false,"")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if isBanner {
            
            if let tempImage:UIImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
                if let closure =  didPickImage {
                    closure(tempImage)
                }
                
                
                imagePicker.dismiss(animated: true) {
                    self.edit(image: tempImage)
                }
            }
        }
        else
        {
            if let tempImage:UIImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
                
                if let closure =  didPickImage {
                    closure(tempImage)
                }
                
                if isLogo == true {
                   
                  generatePhotoThumbnail(tempImage)
                    
                }
                
                uploadImage(image: tempImage)
                
                imagePicker.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func edit(image: UIImage) {
        let storyBoard = UIStoryboard.init(name: "CropImage", bundle: nil)
        if let newSellerPage:ImageCropViewController = storyBoard.instantiateViewController(withIdentifier: "ImageCropViewController") as? ImageCropViewController  {

            newSellerPage.image = image
            newSellerPage.delegate = self

            self.navigationController?.pushViewController(newSellerPage, animated: true)
        }

    }
    
    func photoTweaksController(_ controller: IGRPhotoTweakViewController, didFinishWithCroppedImage croppedImage: UIImage) {
        uploadImage(image: croppedImage)
       // self.imageView?.image = croppedImage
        _ = controller.navigationController?.popViewController(animated: true)
    }
    
    func photoTweaksControllerDidCancel(_ controller: IGRPhotoTweakViewController) {
        _ = controller.navigationController?.popViewController(animated: true)
    }
    
    //////////-----vivek
    
    func uploadImage(image:UIImage) {
        isUploadingProceess = true
        // updateNextButton()
        imageSpinner?.startAnimating()
        let compressedImage = image.jpeg(.lowest)
       // print("Image:::::::::::::::::\(compressedImage?.count ?? 0)")
        let bytes = compressedImage?.count ?? 0
        let KB = Double(bytes) / 1024.0
        print("Image:::::::::::::::::\(KB)")
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.uploadFile(endUrl: ("\(ApiBaseUrl)service_attachment"), imageData: compressedImage, withName: "attachment", fileName: "image.JPG", mimeType: "image/JPG", parameters: nil, completionHandler: {response, jsonObj in
            
            if let selfObj = weakSelf {
                selfObj.isUploadingProceess = false
                // updateNextButton()
                selfObj.imageSpinner?.stopAnimating()
                var message:String = failedToConnectMessage.localized()
                if let serverMessage = jsonObj?.message(){
                    message = (serverMessage.count>0) ? serverMessage : message
                }
                if (jsonObj?.status() ?? false) {
                    if let dataObj = jsonObj?.dataObj(){
                        if let urlString:String = dataObj.object(forKey: "a_url") as? String{
                            selfObj.profilePicUrl = urlString
                            
                            if !selfObj.isAttachment {
                                
                            if let closure =  selfObj.didUploadImage {
                                closure(true,urlString)
                              }
                           if selfObj.isLogo {
                             if let closure =  selfObj.didUploadLogoImage {
                                  closure(true,urlString)
                                 }
                            }
                          }
                        if selfObj.isAttachment {
                            if let closure = selfObj.didUploadHelpDeskImage
                            {
                                if let imageName:String = dataObj.object(forKey: "a_fn") as? String
                                {
                                    closure(true,urlString,imageName)
                                }
                                
                            }
                          }
                            return
                        }
                    }
                }
                // imgProfilePic.image = UIImage.init(named: "profilePicIcon")
                if let closure =  selfObj.didUploadImage {
                    closure(false,"")
                }
                if let closure = selfObj.didUploadHelpDeskImage
                {
                   
                    closure(false,"","")
                    
                }
                selfObj.showErrorMessage(message: message)
            }
            
        })
    }
    
    func uploadThumbnailImage(image:UIImage) {
        isUploadingProceess = true
        // updateNextButton()
        imageSpinner?.startAnimating()
        let compressedImage = image.jpeg(.lowest)
      //  print("Image:::::::::::::::::\(compressedImage?.count ?? 0)")
        let bytes = compressedImage?.count ?? 0
               let KB = Double(bytes) / 1024.0
               print("Image:::::::::::::::::\(KB)")
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.uploadFile(endUrl: ("\(ApiBaseUrl)service_attachment"), imageData: compressedImage, withName: "attachment", fileName: "image.JPG", mimeType: "image/JPG", parameters: nil, completionHandler: {response, jsonObj in
            
            if let selfObj = weakSelf {
                selfObj.isUploadingProceess = false
                // updateNextButton()
                selfObj.imageSpinner?.stopAnimating()
                var message:String = failedToConnectMessage.localized()
                if let serverMessage = jsonObj?.message(){
                    message = (serverMessage.count>0) ? serverMessage : message
                }
                if (jsonObj?.status() ?? false) {
                    if let dataObj = jsonObj?.dataObj(){
                        if let urlString:String = dataObj.object(forKey: "a_url") as? String{
                           // selfObj.profilePicUrl = urlString
                            
                             if let closure =  selfObj.didUploadLogoImage {
                                  closure(true,urlString)
                                 }
                           
                            return
                        }
                    }
                }
                // imgProfilePic.image = UIImage.init(named: "profilePicIcon")
                if let closure =  selfObj.didUploadImage {
                    closure(false,"")
                }
                if let closure = selfObj.didUploadHelpDeskImage
                {
                   
                    closure(false,"","")
                    
                }
                selfObj.showErrorMessage(message: message)
            }
            
        })
    }
    
    func uploadAttachmentImage(image:UIImage) {
        
        let temp:UIImage = imageWithImage(sourceImage: image, scaledToWidth: 4.5)
        
        isUploadingProceess = true
        // updateNextButton()
        imageSpinner?.startAnimating()
        let compressedImage = temp.jpeg(.lowest)
        print("Image:::::::::::::::::\(compressedImage?.count ?? 0)")
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.uploadFile(endUrl: ("\(ApiBaseUrl)service_attachment"), imageData: compressedImage, withName: "attachment", fileName: "image.JPG", mimeType: "image/JPG", parameters: nil, completionHandler: {response, jsonObj in
            
            if let selfObj = weakSelf {
                selfObj.isUploadingProceess = false
                // updateNextButton()
                selfObj.imageSpinner?.stopAnimating()
                var message:String = failedToConnectMessage.localized()
                if let serverMessage = jsonObj?.message(){
                    message = (serverMessage.count>0) ? serverMessage : message
                }
                if (jsonObj?.status() ?? false) {
                    if let dataObj = jsonObj?.dataObj(){
                        if let urlString:String = dataObj.object(forKey: "a_url") as? String{
                            selfObj.profilePicUrl = urlString
                            if let closure =  selfObj.didUploadImage {
                                closure(true,urlString)
                            }
                            if let closure =  selfObj.didUploadLogoImage {
                                closure(true,urlString)
                              }
                            if let closure = selfObj.didUploadHelpDeskImage
                            {
                                if let imageName:String = dataObj.object(forKey: "a_fn") as? String
                                {
                                    closure(true,urlString,imageName)
                                }
                            }
                            return
                        }
                    }
                }
                // imgProfilePic.image = UIImage.init(named: "profilePicIcon")
                if let closure =  selfObj.didUploadImage {
                    closure(false,"")
                }
                selfObj.showErrorMessage(message: message)
            }
            
        })
    }
    
    func imageWithImage (sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    
    func generatePhotoThumbnail(_ image: UIImage?) {
      
        let size = image?.size
        var croppedSize:CGSize
        let ratio: CGFloat = 64.0
        var offsetX: CGFloat = 0.0
        var offsetY: CGFloat = 0.0

      
        if (size?.width ?? 0.0) > (size?.height ?? 0.0) {
            offsetX = ((size?.height ?? 0.0) - (size?.width ?? 0.0)) / 2
            croppedSize = CGSize(width: size?.height ?? 0.0, height: size?.height ?? 0.0)
        } else {
            offsetY = ((size?.width ?? 0.0) - (size?.height ?? 0.0)) / 2
            croppedSize = CGSize(width: size?.width ?? 0.0, height: size?.width ?? 0.0)
        }

        let clippedRect = CGRect(x: offsetX * -1, y: offsetY * -1, width: croppedSize.width, height: croppedSize.height)
        
        let imageRef = image?.cgImage?.cropping(to: clippedRect)
     
        let rect = CGRect(x: 0.0, y: 0.0, width: ratio, height: ratio)

        UIGraphicsBeginImageContext(rect.size)
        
        if let imageRef = imageRef {
            UIImage(cgImage: imageRef).draw(in: rect)
        }
        
        guard let thumbnail = UIGraphicsGetImageFromCurrentImageContext() else { return }
        UIGraphicsEndImageContext()
        
        uploadThumbnailImage(image: thumbnail)

        return
    }
    
}


extension ImageUploaderViewController:UIDocumentPickerDelegate
{
    func openImportDocumentPicker(){
    
          let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)

          documentPicker.delegate = self
        
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance(whenContainedInInstancesOf: [UIDocumentBrowserViewController.self]).tintColor = nil
        }
        
          present(documentPicker, animated: true, completion: nil)
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {

     let cico = url as URL
     print(cico)
     print(url)

     print("url.lastPathComponent \(url.lastPathComponent)")

        print("url.pathExtension \(url.pathExtension)")
        
    // print(url.pathExtension)
        
        do {
            // inUrl is the document's URL
               let data = try Data(contentsOf: url) // Getting file data here
            
            uploadDocument(docData: data, mimeType: mimeTypeForPath(url: url), fileName: url.lastPathComponent)

            print(data)
           } catch {
               print("document loading error")
           }

    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {

        print(" cancelled by user")

        dismiss(animated: true, completion: nil)

    }
    
    func mimeTypeForPath(url: URL) -> String {
        //let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension

        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    
    
    func uploadDocument(docData:Data, mimeType:String, fileName:String) {
        
        print("mimeType ::: \(mimeType)")
        print("fileName ::: \(fileName)")
        
        isUploadingProceess = true
        imageSpinner?.startAnimating()

        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.uploadFile(endUrl: ("\(ApiBaseUrl)service_attachment"), imageData: docData, withName: "attachment", fileName: fileName, mimeType: mimeType, parameters: nil, completionHandler: {response, jsonObj in
            
            if let selfObj = weakSelf {
                selfObj.isUploadingProceess = false
                // updateNextButton()
                selfObj.imageSpinner?.stopAnimating()
                var message:String = failedToConnectMessage.localized()
                if let serverMessage = jsonObj?.message(){
                    message = (serverMessage.count>0) ? serverMessage : message
                }
                if (jsonObj?.status() ?? false) {
                    if let dataObj = jsonObj?.dataObj(){
                        if let urlString:String = dataObj.object(forKey: "a_url") as? String{
                            selfObj.profilePicUrl = urlString
                            
                            if !selfObj.isAttachment {
                                
                            if let closure =  selfObj.didUploadImage {
                                closure(true,urlString)
                            }
                            }
                        if selfObj.isAttachment {
                            if let closure = selfObj.didUploadHelpDeskImage
                            {
                                if let imageName:String = dataObj.object(forKey: "a_fn") as? String
                                {
                                    closure(true,urlString,imageName)
                                }
                                
                            }
                          }
                            return
                        }
                    }
                }
                // imgProfilePic.image = UIImage.init(named: "profilePicIcon")
                if let closure =  selfObj.didUploadImage {
                    closure(false,"")
                }
                if let closure = selfObj.didUploadHelpDeskImage
                {
                   
                    closure(false,"","")
                    
                }
                selfObj.showErrorMessage(message: message)
            }
            
        })
    }
 
}
