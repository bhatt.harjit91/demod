//
//  DateUtils.swift
//  NearKart
//
//  Created by RaviKiran B on 20/11/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import Foundation


extension Date {
    
    
    func dateTimeStringWithConversion() -> String {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = DisplayDateTimeFormatString
        dateFormatter.locale = Locale.init(identifier: Localize.currentLanguage)
        dateFormatter.timeZone = TimeZone.init(identifier: "UTC")
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: self)
    }
    
    func dateTimeString() -> String {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = DisplayDateTimeFormatString
        dateFormatter.locale = Locale.init(identifier: Localize.currentLanguage)
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: self)
    }
   
    func yearString() -> String {
          let dateFormatter = DateFormatter()
          //dateFormatter.dateFormat = DisplayDateTimeFormatString
          dateFormatter.locale = Locale.init(identifier: Localize.currentLanguage)
          dateFormatter.dateStyle = .medium
          dateFormatter.timeStyle = .none
          dateFormatter.dateFormat = "yyyy"
          return dateFormatter.string(from: self)
      }
    
    
    func dateString() -> String {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = DisplayDateTimeFormatString
        dateFormatter.locale = Locale.init(identifier: Localize.currentLanguage)
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        return dateFormatter.string(from: self)
    }
    
    func timeString() -> String {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = DisplayDateTimeFormatString
        dateFormatter.locale = Locale.init(identifier: Localize.currentLanguage)
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: self)
    }
    
    //MARK:- Server String
    func serverDateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    
    func serverDateTimeString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: self)
    }
    
    func serverTimeString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: self)
    }
}
