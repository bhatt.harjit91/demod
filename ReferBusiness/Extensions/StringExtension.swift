//
//  StringExtension.swift
//  iCanRefer
//
//  Created by TalentMicro on 16/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

extension String {

    func fileName() -> String {
        return NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent ?? ""
    }

    func fileExtension() -> String {
        return NSURL(fileURLWithPath: self).pathExtension ?? ""
    }

//   var youtubeID: String? {
//
//    //https://youtu.be/xwwAVRyNmgQ
//
//        //let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)|(?<=(\\?|\\&)youtu.be/)"
//        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
//        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
//        let range = NSRange(location: 0, length: count)
//        guard let result = regex?.firstMatch(in: self, range: range) else {
//            return nil
//        }
//
//        return (self as NSString).substring(with: result.range)
//
//    }
}
extension String {
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
    
}
