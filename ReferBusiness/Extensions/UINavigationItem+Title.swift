//
//  UINavigationItem+Title.swift
//  NearKart
//
//  Created by RaviKiran B on 30/10/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit


extension UINavigationItem {
    
    
    
    func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.text = title
        one.font = UIFont.titleFont
        one.textAlignment = .center
        one.textColor = .white
        one.sizeToFit()
        
        
        let two = UILabel()
        two.text = subtitle
        two.font = UIFont.bodyFont
        two.textAlignment = .center
        two.textColor = .white
        two.sizeToFit()
        
        var stackView = UIStackView(arrangedSubviews: [one])
        if subtitle.count > 0 {
            stackView = UIStackView(arrangedSubviews: [one, two])
        }
        
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        stackView.frame = CGRect.init(x: 0, y: 0, width: 200, height: 35) //init(frame: CGRect.init(x: 0, y: 0, width: 1000, height: 30))
        //let width = max(one.frame.size.width, two.frame.size.width)
        //stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        one.sizeToFit()
        two.sizeToFit()
        
        
        
        self.titleView = stackView
    }
}
