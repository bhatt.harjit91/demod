 extension UIViewController {

     /**
      *  Height of status bar + navigation bar (if navigation bar exist)
      */

     var topbarHeight: CGFloat {
       
        if #available(iOS 13.0, *) {
            return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
                (self.navigationController?.navigationBar.frame.height ?? 0.0)
        } else {
            return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        }
     }
 }

 public protocol ViewDesigning{ }
 
 public extension ViewDesigning where Self : UIViewController{
    func setupShadow(_ vw:UIView){
        vw.layer.shadowColor = UIColor.black.cgColor
        vw.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        vw.layer.shadowRadius = 1
        vw.layer.shadowOpacity = 0.5
        vw.layer.masksToBounds = false
        vw.layer.shadowPath = UIBezierPath(roundedRect: vw.bounds, cornerRadius: vw.layer.cornerRadius).cgPath
    }
 }

 
