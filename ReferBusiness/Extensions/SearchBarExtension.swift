//
//  SearchBarExtension.swift
//  NearKart
//
//  Created by RaviKiran B on 07/09/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

extension UISearchBar{
    
    func setTextFieldColor(color:UIColor) {
        if let textfield =  value(forKey: "searchField") as? UITextField {
            textfield.tintColor = UIColor.primaryColor
            if let backgroundview = textfield.subviews.first {
                
                // Background color
                backgroundview.backgroundColor = UIColor.pTextColor
                
                // Rounded corner
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;
            }
        }
    }
    
    func localizeSearchBar() {
        
       // let textField:UITextField =  value(forKey: "_searchField") as! UITextField
        
        if #available(iOS 13.0, *) {
            let textField:UITextField = searchTextField
            textField.font = UIFont.bodyFont
                   textField.textAlignment = Localize.textAlignmentForLanguage()
             UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Cancel".localized()
        } else {
            let textField:UITextField =  value(forKey: "_searchField") as! UITextField
            textField.font = UIFont.bodyFont
                   textField.textAlignment = Localize.textAlignmentForLanguage()
            setValue("Cancel".localized(), forKey:"_cancelButtonText")
        }
       // let textField = searchTextField
       
        
        
        
       // setValue("Cancel".localized(), forKey:"_cancelButtonText")
        
        
    }
}
