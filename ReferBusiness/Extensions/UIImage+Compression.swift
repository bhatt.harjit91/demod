//
//  UIImage+Compression.swift
//  NearKart
//
//  Created by RaviKiran B on 19/09/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: quality.rawValue)
    }
    class func getImageWithIndex(index:Int) -> UIImage{
        let newIndex = index%12
        switch newIndex {
        case 0:
            return  UIImage.init(imageLiteralResourceName: "spicy_1")
        case 1:
            return  UIImage.init(imageLiteralResourceName: "spicy_1")
        case 2:
            return  UIImage.init(imageLiteralResourceName: "spicy_2")
        case 3:
            return UIImage.init(imageLiteralResourceName: "spicy_3")
        case 4:
            return UIImage.init(imageLiteralResourceName: "spicy_4")
        case 5:
            return UIImage.init(imageLiteralResourceName: "glutenFree")
        case 6:
            return UIImage.init(imageLiteralResourceName: "dailyFree")
        case 7:
            return UIImage.init(imageLiteralResourceName: "vegIcon")
        case 8:
            return UIImage.init(imageLiteralResourceName: "non_Veg")
        case 9:
            return UIImage.init(imageLiteralResourceName: "beefIncluded")
        case 10:
            return UIImage.init(imageLiteralResourceName: "porkIncluded")
        case 11:
            return UIImage.init(imageLiteralResourceName: "nutfree")
        case 12:
            return UIImage.init(imageLiteralResourceName: "noSugar")
        default:
            return UIImage.init(imageLiteralResourceName: "spicy_1")
            
        }
        
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
