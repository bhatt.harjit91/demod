//
//  TableViewExtension.swift
//  iCanRefer
//
//  Created by TalentMicro on 05/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

extension UITableView {
    
    func reloadDataWithoutScroll() {
        let offset = contentOffset
        reloadData()
        layoutIfNeeded()
        setContentOffset(offset, animated: false)
    }
}
