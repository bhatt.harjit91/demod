//
//  UIViewController+Permissions.swift
//  NearKart
//
//  Created by RaviKiran B on 19/09/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

extension UIViewController{
    
    //MARK: - Camer Permisson
    /// Checks camera permission
    ///
    /// - Parameter completionHander:
    func checkCameraPermisson(completionHander:@escaping(Bool)->Void) {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            completionHander(true)
        }
        else if AVCaptureDevice.authorizationStatus(for: .video) ==  .denied {
            //already denied
            completionHander(false)
            showCameraPermisson()
        }
        else if AVCaptureDevice.authorizationStatus(for: .video) ==  .restricted {
            //already authorized
            completionHander(false)
            showCameraPermisson()
        }
        else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    completionHander(true)
                } else {
                    //access denied
                    completionHander(false)
                    // showCameraPermisson()
                }
            })
        }
    }
    
    func showCameraPermisson() {
        weak var weakSelf = self
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable camera access to take picture.".localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction.init(title: "Settings", style: .default, handler: {action in
            weakSelf?.openAppSettings()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Library Permisson
    func checkPhotoLibraryPermission(completionHander:@escaping(Bool)->Void) {
        let status = PHPhotoLibrary.authorizationStatus()
        if status ==  .authorized {
            //already authorized
            completionHander(true)
        }
        else if status ==  .denied {
            //already denied
            completionHander(false)
            showLibraryPermissonAlert()
        }
        else if status ==  .restricted {
            //already authorized
            completionHander(false)
            showLibraryPermissonAlert()
        }
        else {
            PHPhotoLibrary.requestAuthorization() { (status) -> Void in
                if status == .authorized {
                    //access allowed
                    completionHander(true)
                } else {
                    //access denied
                    completionHander(false)
                    // showCameraPermisson()
                }
            }
        }
    }
    
    func showLibraryPermissonAlert() {
        weak var weakSelf = self
        
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable library access to select picture.".localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction.init(title: "Settings", style: .default, handler: {action in
            weakSelf?.openAppSettings()
            
        }))
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Location Permisson
    func checkLocationPermission(completionHander:@escaping(Bool)->Void) {
        let status = CLLocationManager.authorizationStatus()
        if status ==   .authorizedWhenInUse || status == .authorizedAlways {
            //already authorized
            completionHander(true)
        }
        else if status ==  .denied {
            //already denied
            completionHander(false)
            showLocationPermissonAlert()
        }
        else if status ==  .restricted {
            //already authorized
            completionHander(false)
            showLocationPermissonAlert()
        }
        else {
            completionHander(false)
        }
    }
    
    
    
    func showLocationPermissonAlert() {
        weak var weakSelf = self
        
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable location access to provide you near by search.".localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction.init(title: "Settings", style: .default, handler: {action in
            
            weakSelf?.openAppSettings()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func openAppSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.openURL(settingsUrl)
        }
    }
}

