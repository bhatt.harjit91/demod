//
//  DataCrypto.m
//  NearKart
//
//  Created by RaviKiran B on 10/10/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

#import "DataCrypto.h"
#import "NSData+Base64.h"
#import "GZIP.h"
#import "AESCrypt.h"

@implementation DataCrypto


- (NSDictionary *)decryptData:(NSString *)encryptedString Key:(NSString *)key IV:(NSString *)iv{
    NSData *encryptedData = [NSData base64DataFromString:encryptedString];
    
    NSData *decryptedData = [AESCrypt decryptData:encryptedData password:key iv:iv];
    NSData *deCompData = [decryptedData gunzippedData];
    
    NSError *errorJson=nil;
    //NSString* newStr = [[NSString alloc] initWithData:deCompData encoding:NSUTF8StringEncoding];
    NSDictionary* responseData = [NSJSONSerialization JSONObjectWithData:deCompData options:kNilOptions error:&errorJson];
    return responseData;
}


- (NSString *)encryptedString:(NSDictionary *)params Key:(NSString *)key IV:(NSString *)iv{
    //NSData *paramData = [NSKeyedArchiver archivedDataWithRootObject:params];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSData *compData = [jsonData gzippedData];
    NSData *encryptedData = [AESCrypt encryptData:compData password:key iv:iv];
    
    NSString *encryptedBase64String = [encryptedData base64EncodedStringWithOptions:0];
    
    return encryptedBase64String;
}
@end
