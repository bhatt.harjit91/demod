//
//  DataCrypto.h
//  NearKart
//
//  Created by RaviKiran B on 10/10/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataCrypto : NSObject

- (NSDictionary *)decryptData:(NSString *)encryptedString Key:(NSString *)key IV:(NSString *)iv;
- (NSString *)encryptedString:(NSDictionary *)params Key:(NSString *)key IV:(NSString *)iv;
@end

NS_ASSUME_NONNULL_END
