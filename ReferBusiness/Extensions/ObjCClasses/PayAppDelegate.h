//
//  PayAppDelegate.h
//  iCanRefer
//
//  Created by TalentMicro on 17/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayAppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;

@end
