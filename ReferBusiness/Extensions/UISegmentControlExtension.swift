//
//  UISegmentControlExtension.swift
//  iCanRefer
//
//  Created by TalentMicro on 14/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

extension UISegmentedControl{
    
   func setSegmentStyle() {

    let segmentTitleAttributes: [NSAttributedString.Key: Any] = [
       .font: UIFont.fontForType(textType: 3),
       .foregroundColor: UIColor.white,]
    
    if #available(iOS 13.0, *) {
        
        selectedSegmentTintColor = UIColor.getGradientColor(index: 0).first!
                                         
        setTitleTextAttributes(segmentTitleAttributes, for: .selected)
            } else {
                tintColor = UIColor.getGradientColor(index: 0).first!
    }
    
   }
}
