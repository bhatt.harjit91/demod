//
//  ViewController+UserData.swift
//  NearKart
//
//  Created by RaviKiran B on 11/09/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit

struct billingUserModel: Codable {
    var billing_name:String?
    var billing_email:String?
    var billing_address:String?
    var billing_city:String?
    var billing_state:String?
    var billing_zip:String?
    var billing_country:String?
    var billing_tel:String?
    var billing_tel_isd:String?
}
struct userModel: Codable {
    let userId:Int?
    let displayName: String?
    let firstName:String?
    let lastName:String?
    let mobileIsd: String?
    let mobileNumber: String?
    let emailId: String?
    let profilePicture: String?
    var jobTitle:String?
    var organization:String?
    var city:String?
    var about:String?
    var latitude:Double?
    var longitude:Double?
    var isCharity:Int?
    var userCurrency:String?
    var userLoyaltyAmount:Double?
    var billingDetails:billingUserModel?
    var isHome:Int?
    var isSearch:Int?
    var isMessages:Int?
    var address:String?
    var isBuyer:Int?
    var isReferralMember:Int?
    var isSeller:Int?
    var membershipId:String?
    

    var dictionary: [String: Any] {
        
        return ["displayName": displayName ?? "",
                "mobileIsd": mobileIsd ?? "",
                "mobileNumber": mobileNumber ?? "",
                "emailId": emailId ?? "",
                "profilePicture": profilePicture ?? "",
                "userId":userId ?? 0,
                "jobTitle":jobTitle ?? "",
                "organization":organization ?? "",
                "city":city ?? "",
                "about":about ?? "",
                "latitude":latitude ?? 0.0,
                "longitude":longitude ?? 0.0,
                "isCharity": isCharity ?? false,
                "billingDetails":billingDetails ?? "",
                "userCurrency":userCurrency ?? "",
                "userLoyaltyAmount":userLoyaltyAmount ?? 0.0,
                "lastName":lastName ?? "",
                "firstName":firstName ?? "",
                 "isHome":isHome ?? 0,
                 "isSearch":isSearch ?? 0,
                 "isMessages":isMessages ?? 0,
                 "address":address ?? "",
                 "isBuyer":isBuyer ?? 0,
                 "isReferralMember":isReferralMember ?? 0,
                  "isSeller":isSeller ?? 0,
                  "membershipId":membershipId ?? ""
        ]
    }
    var nsDictionary: NSDictionary {
        return dictionary as NSDictionary
    }
}
class UserAPIModel: Codable {
    var userDetails:userModel?
}
let SecretKeyUserName = "SecretKeyUserName"
let ApnsDeviceTokenKey = "ApnsDeviceTokenKey"
let UUIDStringKey = "UUIDStringKey"

extension UIViewController{
    
    func checkLogin() -> Bool {
        if let userDetails:userModel = getUserDetail(){
            if let token = getToken(userName: userDetails.mobileNumber ?? ""){
                if token.count>0{
                    return true
                }
            }
        }
        return false
    }
    func checkLoginAndCharity() -> Bool {
        if let userDetails:userModel = getUserDetail(){
            if let token = getToken(userName: userDetails.mobileNumber ?? ""){
                if token.count>0 && userDetails.isCharity == 1{
                    return true
                }
            }
        }
        return false
    }
    
    func getCurrentUserID() -> Int {
        if let userDetails:userModel = getUserDetail(){
            return userDetails.userId ?? 0
        }
        return 0
    }
    
    func getCurrentUserToken()->String{
        if let userDetails:userModel = getUserDetail(){
            if let token = getToken(userName: userDetails.mobileNumber ?? ""){
                if token.count>0{
                    return token
                }
            }
        }
        return ""
    }
    func getToken(userName:String) -> String? {
        do {
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: userName,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            let keychainPassword = try passwordItem.readPassword()
            return keychainPassword
        } catch {
            //fatalError("Error reading password from keychain - \(error)")
            print("Exception in \(#file) Function:\(#function) LINE:\(#line)")
            print(error)
            print("===========================================================")
            return nil
        }
        //return nil
    }
    
    func deleteIfExists(userName:String) {
        do {
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: userName,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            try passwordItem.deleteItem()
            
        }
        catch{
            print("Exception in \(#file) Function:\(#function) LINE:\(#line)")
            print(error)
            print("===========================================================")
        }
    }
    
    func saveToken(userName:String,token:String) -> Bool{
        do {
            deleteIfExists(userName: userName)
            // This is a new account, create a new keychain item with the account name.
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: userName,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            
            // Save the password for the new item.
            try passwordItem.savePassword(token)
        } catch {
            //fatalError("Error updating keychain - \(error)")
            //            print("Exception in \(#file) Function:\(#function) LINE:\(#line)")
            //            print(error)
            //            print("===========================================================")
            return false
        }
        return true;
    }
    
    func saveUserDetails(userDetail:NSDictionary)->Bool {
        do {
            let data = try JSONSerialization.data(withJSONObject: userDetail as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
            let decoder = JSONDecoder()
            _ = try! decoder.decode(userModel.self, from: data)
            UserDefaults.standard.set(data, forKey: "UserDetails")
            UserDefaults.standard.synchronize()
            return true
            
        } catch {
            print("Exception in \(#file) Function:\(#function) LINE:\(#line)")
            print(error)
            print("===========================================================")
            return false
        }
        
    }
    
    
    
    func getUserDetail() -> userModel? {
        
        if let data:Data = UserDefaults.standard.object(forKey: "UserDetails") as? Data{
            let decoder = JSONDecoder()
            let userDetail = try! decoder.decode(userModel.self, from: data)
            return userDetail
        }
        return nil
    }
    
    func clearUserDetails(){
        if let userDetails:userModel = getUserDetail(){
            deleteIfExists(userName: userDetails.mobileNumber ?? "")
        }
        UserDefaults.standard.removeObject(forKey: "UserDetails")
        UserDefaults.standard.removeObject(forKey: "EnableTextSpeech")
        UserDefaults.standard.removeObject(forKey: "isoCountryCode")
        UserDefaults.standard.removeObject(forKey: "configVersionNo")
        UserDefaults.standard.removeObject(forKey: "config")
        UserDefaults.standard.removeObject(forKey: "bannerList")
        UserDefaults.standard.removeObject(forKey: "alertNo")
        UserDefaults.standard.removeObject(forKey: "firstTime")
        UserDefaults.standard.removeObject(forKey: "selectedTab")
        UserDefaults.standard.synchronize()
        //CartDataPersistance.clearCart()
    }
    
    func clearUserDetailsForce(){
        if let userDetails:userModel = getUserDetail(){
            deleteIfExists(userName: userDetails.mobileNumber ?? "")
        }
        UserDefaults.standard.removeObject(forKey: "UserDetails")
        UserDefaults.standard.removeObject(forKey: "configVersionNo")
        UserDefaults.standard.removeObject(forKey: "config")
        UserDefaults.standard.removeObject(forKey: "bannerList")
        UserDefaults.standard.removeObject(forKey: "alertNo")
        UserDefaults.standard.removeObject(forKey: "firstTime")
        UserDefaults.standard.removeObject(forKey: "selectedTab")
        UserDefaults.standard.synchronize()
        //CartDataPersistance.clearCart()
    }
    
    
    func getSecretKey() -> String? {
        if let key:String = getToken(userName: SecretKeyUserName){
            return key
        }
        return nil
    }
    
    func generateSecretKey() -> Bool {
        let rnString:String = randomString(length: 32)
        if saveToken(userName: SecretKeyUserName, token: rnString) {
            return true
        }
        
        return false
    }
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func getAPNSDeviceToken() -> String {
        if let apnsId:String = UserDefaults.standard.object(forKey: ApnsDeviceTokenKey) as? String{
            return apnsId
        }
        return ""
    }
    
    func getUUIDString() -> String {
        if let apnsId:String = UserDefaults.standard.object(forKey: UUIDStringKey) as? String{
            
            print("\("APNS::::::::")\(apnsId)")
            
            return apnsId
        }
        return ""
    }
    
    
    
}
