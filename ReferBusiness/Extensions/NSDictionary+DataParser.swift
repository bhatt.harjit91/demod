//
//  NSDictionary+DataParser.swift
//  WhatMate
//
//  Created by RaviKiran B on 18/05/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import Foundation

extension NSDictionary{
    
    func dataObj()->NSDictionary?{
        return  object(forKey: "data") as? NSDictionary
    }
    
    func dataString()->String?{
        return  object(forKey: "data") as? String
    }
    
    func status() -> Bool {
        if let status:Int =  object(forKey: "status") as? Int{
            if status == 1 {
                return true
            }
        }
        return false
    }
    
    func message() -> String {
        if let message:String =  object(forKey: "message") as? String{
            return message
        }
        return ""
    }
}
