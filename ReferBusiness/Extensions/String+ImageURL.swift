//
//  String+ImageURL.swift
//  NearKart
//
//  Created by RaviKiran B on 04/12/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

//import Foundation
import UIKit

extension String {
    func ImageURL() -> URL? {
        if self.count > 0 {
            return URL.init(string: "\(StorageUrl)\(self)")
        }
        return nil
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: "\\n", replacement: "")
    }
    
    func removeWhitespaceSpecial() -> String {
        return self.replace(string: "\\", replacement: "")
    }
    
    func removeWhitespaceWithNextLine() -> String {
        return self.replace(string: "\\n", replacement: " ")
    }
}







