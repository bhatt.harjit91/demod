//
//  WebViewViewController.swift
//  NearKart
//
//  Created by RaviKiran B on 27/12/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: AppBaseViewController {

    
    @IBOutlet weak var viewWebView: UIView!
    
    var urlString:String = ""
    var navTitle:String = ""
    var subTitle:String = ""
    var webView = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationItem.setTitle(title: navTitle, subtitle: subTitle)
        
      //  self.navigationController?.navigationBar.titleTextAttributes =
      //      [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        //startAnimating()
//        webViewer.delegate = self
//        webViewer.scalesPageToFit = true
        
      
        let configuration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: configuration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        view.addSubview(webView)
        
        [webView.topAnchor.constraint(equalTo: viewWebView.topAnchor),
         webView.bottomAnchor.constraint(equalTo: viewWebView.bottomAnchor),
         webView.leftAnchor.constraint(equalTo: viewWebView.leftAnchor),
         webView.rightAnchor.constraint(equalTo: viewWebView.rightAnchor)].forEach  { anchor in
            anchor.isActive = true
        }
        
      //  webView.navigationDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startAnimating()

        if urlString.count > 0 {
            if let urlToLoad:URL = urlString.ImageURL(){
                let urlRequest:URLRequest = URLRequest.init(url:urlToLoad)
                webView.load(urlRequest)
            }
            
        }
    }

    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        stopAnimating()
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
        stopAnimating()
    }
    
    
    // MARK: - WEBView
    

    
    @IBAction func btnClose(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

