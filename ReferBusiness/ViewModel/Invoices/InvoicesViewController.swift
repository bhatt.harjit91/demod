//
//  InvoicesViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 17/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import Razorpay

class InvoicesViewController: AppBaseViewController,TransferPaymentVCDelegate {
    
    var sellerCode:Int = 0
    var invoiceList:[InvoiceListModel]?
    var count:Int?
    var invoiceStatusMaster:[InvoiceStatusMasterModel]?
    var statusSegmentSelection:Int = 1
    var isFirst:Bool = true
    var paymentDetails:PaymentModel?
    var billingModel:billingUserModel?
    var paymentController = InitialViewController()
    var currency:[CurrencyModel]?
    var paymentTypeList:[PaymentTypeListModel]?
    
    @IBOutlet weak var lblOutStandingAmount: NKLabel!
    @IBOutlet weak var lblTotalOutStanding: NKLabel!
    @IBOutlet weak var btnTransferPayment: NKButton!
     @IBOutlet weak var lblNoData: NKLabel!
    @IBOutlet weak var btnCreditCard: NKButton!
    
    
    //    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tblInvoiceList: UITableView!
    
    @IBOutlet weak var heightOfStackView: NSLayoutConstraint!
    @IBOutlet weak var stackViewPayment: UIStackView!
    //    var selectedIndex:Int = 0
    //
    private var razorpay:Razorpay?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        lblNoData.text = "No Data Found".localized()
        navigationItem.title = "Payout Tracker".localized()
        
        btnCreditCard.setTitle("Pay by Credit Card/Online".localized(), for: .normal)
        btnTransferPayment.setTitle("Declare Cheque/Transfer Payment".localized(), for: .normal)
        
        lblTotalOutStanding.text = "Total Outstanding".localized()
        fetchInvoiceList(status: statusSegmentSelection)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
         super.viewWillAppear(animated)
        
    }
    
    func openPayPal() {

        
        guard let url = URL(string: paymentDetails?.paymentUrl ?? "") else { return }
        UIApplication.shared.open(url)
               
    }
    
    
    func fetchInvoiceList(status:Int) {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        
        if isFirst == true
        {
            isFirst = false
            startAnimating()
        }
        else
        {
            startAnimatingAfterSubmit()
        }
        
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/sellerInvoices?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&status=\(status)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(InvoiceListCalss.self, from: data)
                                
                                if let apiResponse:Float = jsonResponse.netAmount {

                                    weakSelf?.lblOutStandingAmount.textColor = UIColor.primaryColor
                                    weakSelf?.lblOutStandingAmount.text = "\(jsonResponse.currencySymbol ?? "")\(" ")\(apiResponse)"
                                    
                                    if apiResponse <= 0 {

                                        weakSelf?.lblOutStandingAmount.textColor = UIColor.rgb(fromHex: 0x5DB67B)
                                        weakSelf?.heightOfStackView.constant = 50
                                        weakSelf?.stackViewPayment.arrangedSubviews[1].isHidden = true
                                        weakSelf?.btnCreditCard.isUserInteractionEnabled = false
                                    }
                                }
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currency {
                                    
                                    weakSelf?.currency = apiResponse
                                    
                                }
                                
                                if let apiResponse:[PaymentTypeListModel] = jsonResponse.paymentTypeList {
                                    
                                    weakSelf?.paymentTypeList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[InvoiceListModel] = jsonResponse.invoiceList {
                                    
                                    weakSelf?.invoiceList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[InvoiceStatusMasterModel] = jsonResponse.invoiceStatusMaster {

                                    weakSelf?.invoiceStatusMaster = apiResponse
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.count = apiResponse
                                }
                                
                                DispatchQueue.main.async
                                    {
                                        weakSelf?.stopAnimating()
                                        weakSelf?.tblInvoiceList.reloadData()
                                }
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            DispatchQueue.main.async
                {
                    weakSelf?.stopAnimating()
            }
            
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    @IBAction func btnTransferMoney(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: "Invoices", bundle: nil)
        let transferPaymentPage:TransferPaymentViewController = storyBoard.instantiateViewController(withIdentifier: "TransferPaymentViewController") as! TransferPaymentViewController
        
        transferPaymentPage.currency = currency
        transferPaymentPage.delegate = self
        transferPaymentPage.paymentTypeList = paymentTypeList ?? []
        transferPaymentPage.sellerCode = sellerCode
        
        navigationController?.pushViewController(transferPaymentPage, animated: true)
    }
    
    func refreshInvoicePage(message: String) {
        
        DispatchQueue.main.async {
            
         //  self.showErrorMessage(message: message)
        }
        
        fetchInvoiceList(status: statusSegmentSelection)
    }
    
    @IBAction func btnCreditcard(_ sender: Any) {
        
        getGcDetails()
    }
    
    @objc func btnOpenDocument(sender:UIButton)
    {
        print("btnOpenDocument")
        
        if let model:InvoiceListModel = invoiceList?[sender.tag]
        {
            openWebView(urlString:model.invoiceCdnPath ?? "", title: model.invoiceTitle ?? "", subTitle: "")
        }
    }
    
    
    
    func getGcDetails() {
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        
        if isFirst == true
        {
            isFirst = false
            startAnimating()
        }
        else
        {
            startAnimatingAfterSubmit()
        }
        
        let params:NSDictionary = ["sellerCode":sellerCode]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/gcdetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            DispatchQueue.main.async
                                {
                                    weakSelf?.stopAnimating()
                            }
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(PaymentAPIModel.self, from: data)
                                                                
                                if let paymentResponse:PaymentModel = jsonResponse.details {
                                    weakSelf?.paymentDetails = paymentResponse
                                    weakSelf?.razorpay = Razorpay.initWithKey(weakSelf?.paymentDetails?.accessCode ?? "", andDelegate: self)
                                    weakSelf?.billingModel = weakSelf?.paymentDetails?.billingDetails
                                    
                                    weakSelf?.openPaymentPage()
                                }
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            //Show error
            DispatchQueue.main.async
                {
                    weakSelf?.stopAnimating()
            }
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    func openPaymentPage()
    {
        if paymentDetails != nil {
            
            if billingModel?.billing_address?.count ?? 0 > 0{
                
                if paymentDetails?.billingDetails != nil{
                    
                if (paymentDetails?.currencyTitle?.caseInsensitiveCompare("AED") == ComparisonResult.orderedSame) {
                        
                        openCCAvenuePayment()
                    }
                if (paymentDetails?.currencyTitle?.caseInsensitiveCompare("INR") == ComparisonResult.orderedSame) {
                    
                     openRazorpay()
                    
                  }
                    else
                    {
                          openPayPal()
                        // self.paymentContext?.presentPaymentOptionsViewController()
                    }
                }
                else{
                    print("billing_address is empty")
                    
                }
            }
        }
    }
    
    func openRazorpay()
    {
        let amount = paymentDetails?.amount ?? 0.0
        let options: [String:Any] = [
            "amount" : amount * 100, //mandatory in paise like:- 1000 paise ==  10 rs
            "currency" : paymentDetails?.currencyTitle ?? "",
            "order_id":paymentDetails?.razorpay_order_id ?? "",
            "receipt": paymentDetails?.order_id ?? "",
            "prefill": [
                "contact": billingModel?.billing_tel ?? "",
                "email": billingModel?.billing_email ?? ""
            ]
        ]
        print("options::\(options)")
        razorpay?.open(options)
    }
    
    func openCCAvenuePayment()
    {
        
        
//        let storyBoard = UIStoryboard.init(name: "Invoices", bundle: nil)
//        let sellerPage:CCWebViewController = storyBoard.instantiateViewController(withIdentifier: "CCWebViewController") as! CCWebViewController
//
//        sellerPage.paymentDetails = paymentDetails
//
//        navigationController?.pushViewController(sellerPage, animated: true)
        
        
        //print("\("PayemetModel::::::::::::::")\(String(describing: paymentDetails?.billingDetails))\(String(describing: paymentDetails))")
        paymentController = InitialViewController.init(orderId: String(paymentDetails?.order_id ?? ""), merchantId: paymentDetails?.merchantId!, accessCode: paymentDetails?.accessCode!, custId: String(paymentDetails?.customerId ?? 0), amount: String(paymentDetails?.amount ?? 0.0), currency: paymentDetails?.currencyTitle!, rsaKeyUrl: paymentDetails?.rsaKeyUrl!, redirectUrl: paymentDetails?.redirectUrl!, cancelUrl: paymentDetails?.cancelUrl!, showAddress: "N", billingName: billingModel?.billing_name!, billingAddress: billingModel?.billing_address!, billingCity: billingModel?.billing_city!, billingState: billingModel?.billing_state!, billingCountry: billingModel?.billing_country!, billingTel: billingModel?.billing_tel!, billingEmail: billingModel?.billing_email!, deliveryName: paymentDetails?.delivery_name!, deliveryAddress: paymentDetails?.delivery_address!, deliveryCity: paymentDetails?.delivery_city!, deliveryState: paymentDetails?.delivery_state!, deliveryCountry: paymentDetails?.delivery_country!, deliveryTel: paymentDetails?.delivery_tel!, promoCode: "", merchant_param1: "Param 1", merchant_param2: "Param 2", merchant_param3: "Param 3", merchant_param4: "Param 4", merchant_param5: "Param 5", useCCPromo: "N")
        
        
        paymentController.delegate = self
        
        present(paymentController, animated: true, completion: nil)
        
    }
    
    
}


extension InvoicesViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let count = invoiceList?.count ?? 0
        
        if count > 0 {

            tblInvoiceList.alpha = 1
            return count
        }
        else{
            
            tblInvoiceList.alpha = 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:InvoiceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InvoiceTableViewCell") as? InvoiceTableViewCell
        {
            return getCellForInvoiceList(cell: cell, indexPath: indexPath)
        }
        
        return UITableViewCell.init()
    }
    
    func getCellForInvoiceList(cell:InvoiceTableViewCell,indexPath:IndexPath) -> InvoiceTableViewCell {
        
        cell.selectionStyle = .none
        
        if let model:InvoiceListModel = invoiceList?[indexPath.row]
        {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = .current
            
            var InvoiceDate = ""
            
            var paidDate = ""
            
            if let date = dateFormatterGet.date(from: model.invoiceDate ?? "") {
                
                InvoiceDate = dateFormatterPrint.string(from: date)
                
            }
            if let date = dateFormatterGet.date(from: model.invoiceDueDate ?? "") {
                
                paidDate = dateFormatterPrint.string(from: date)
            }
            
            let starAttributes: [NSAttributedString.Key: Any] = [
                .font: UIFont.fontForType(textType: 4),
                .underlineStyle : NSUnderlineStyle.single.rawValue,
                .foregroundColor: UIColor.btnLinkColor]
            
            cell.btnDocument.tag = indexPath.row
            cell.btnDocument.addTarget(self, action: #selector(btnOpenDocument), for: .touchUpInside)
            
            if model.totalAmount ?? 0 > 0 {
                
                cell.lblTotalAmount.text = "\(model.currencyTitle ?? "")\(" ")\(model.totalAmount ?? 0)"
            }
            else
            {
                cell.lblTotalAmount.text = ""
            }
            
            if model.isPaid == 1 {
                
                let star = NSAttributedString(string:"\("Receipt: ".localized())\(model.invoiceNumber ?? "")" , attributes: starAttributes)
                cell.btnDocument.setAttributedTitle(star, for: .normal)
                cell.lblInvoiceDate.text = "\("Paid on ".localized())\(paidDate)\(" by ".localized())\(model.paymentType ?? "")\(", Verification Status : ".localized())\(model.verificationType ?? "")"
                
            }
            else
            {
                let star = NSAttributedString(string:"\("Invoice: ".localized())\(model.invoiceNumber ?? "")" , attributes: starAttributes)
                cell.btnDocument.setAttributedTitle(star, for: .normal)
                
                cell.lblInvoiceDate.text = "\("Invoice dated ".localized())\(InvoiceDate)\(", Status : ")\(model.statusTitle ?? "")"
                
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
}


class InvoiceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTotalAmount: NKLabel!
    @IBOutlet weak var btnDocument: UIButton!
    @IBOutlet weak var lblInvoiceDate: NKLabel!
    
}


extension InvoicesViewController:RazorpayPaymentCompletionProtocol
{
    func onPaymentSuccess(_ payment_id: String) {
        
        saveRazorPaymentGatewayResponse(payment_id: payment_id)
        
        let alert = UIAlertController(title: "Paid".localized(), message: "Your payment was successful. The payment ID is \(payment_id)".localized(), preferredStyle: .alert)
        let action = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        let alert = UIAlertController(title: "Error".localized(), message: "Your payment failed due to an error.\nCode: \(code)\nDescription: \(str)".localized(), preferredStyle: .alert)
        let action = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func saveRazorPaymentGatewayResponse(payment_id: String)
    {
        
        weak var weakSelf = self

        DispatchQueue.main.async {
            
           weakSelf?.startAnimatingAfterSubmit()
        }

        let params:NSDictionary = ["order_id":paymentDetails?.order_id ?? "",
                                   "razorpay_order_id" : paymentDetails?.razorpay_order_id ?? "",
                                   "razorpay_payment_id" : payment_id
        ]
        
        print("params::\(params)")
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        let apiClient:APIClient = APIClient()

        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveGatewayPaymentResponse?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                weakSelf?.stopAnimating()
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    
                    print("response :: \(response)")
                    print("jsonObj:: \(String(describing: jsonObj))")
                    //............
                }
            }
            
            //Show error
             weakSelf?.stopAnimating()
              weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
}

extension InvoicesViewController:InitialViewControllerDelegate
{
    func getResponse(_ responseDict_: NSMutableDictionary!) {
        
        let orderNum = "\(responseDict_["order_id"] ?? "")"
        let orderStatus = "\(responseDict_["order_status"] ?? "")"
        let orderTrackId = "\(responseDict_["tracking_id"] ?? "")"
        
        let msg = "Your order # \(orderNum) is \(orderStatus)\n\nPayment Reference Number : \(orderTrackId)\n\nYou may use this number for any future communications.";
        
        let alert = UIAlertController(title: "Payment Status".localized(), message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Done".localized(), style: UIAlertAction.Style.default, handler: nil));
        
        self.present(alert, animated: true, completion: nil);
    }
}

