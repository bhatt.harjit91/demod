//
//  TransferPaymentViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 19/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol TransferPaymentVCDelegate: class {
    func refreshInvoicePage(message:String)
    
}

class TransferPaymentViewController: AppBaseViewController {
    
    @IBOutlet weak var heightOfCollectionViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightOfTransferTypeConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblTransferTypeText: NKLabel!
    @IBOutlet weak var lblReferenceNumbertext: NKLabel!
    @IBOutlet weak var tfReferenceNumber: RCTextField!
    @IBOutlet weak var lblAmountPaidText: NKLabel!
    @IBOutlet weak var tfCCAmountPaid: RCTextField!
    @IBOutlet weak var tfAmountValue: RCTextField!
    @IBOutlet weak var lblDateOfPaymentText: NKLabel!
    @IBOutlet weak var tfDateOfPayment: RCTextField!
    @IBOutlet weak var lblNotes: NKLabel!
    @IBOutlet weak var tvNotes: NKTextView!
    weak var delegate:TransferPaymentVCDelegate?
    var selectedCurrencyCode : CurrencyModel?    
    var currency:[CurrencyModel]?
    var paymentTypeList:[PaymentTypeListModel]?
    var selectedIndex:Int = 0
    var sellerCode:Int = 0
    
    var transactionId:Int = 0
    var isFromLeads:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if paymentTypeList?.count ?? 0 <= 2 {
            
            heightOfCollectionViewConstraint.constant = 30
            heightOfTransferTypeConstraint.constant = 30 + 50
        }
        
        if paymentTypeList?.count ?? 0 <= 4 {
            
            heightOfCollectionViewConstraint.constant = 30 * 2
            heightOfTransferTypeConstraint.constant = (30 * 2) + 50
        }
        
        if paymentTypeList?.count ?? 0 <= 6 {
            
            heightOfCollectionViewConstraint.constant = 30 * 3
            heightOfTransferTypeConstraint.constant = (30 * 3) + 50
        }
        
        navigationItem.title = "Declare Cheque/Transfer Payment".localized()
        
        tfDateOfPayment.delegate = self
        let date = Date()
        tfDateOfPayment.dateTimePicker.maximumDate =  date
        
        tfAmountValue.delegate = self
        tfCCAmountPaid.delegate = self
        tfReferenceNumber.delegate = self
        
        tfAmountValue.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        tfCCAmountPaid.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
      //  tfReferenceNumber.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Reference Number/Cheque Number".localized())
        
        tfReferenceNumber.placeholder = "Reference Number/Cheque Number".localized()
    }
    
    
    @IBAction func btnSubmit(_ sender: Any) {
        
         var paymentTypeCode:Int = 0
        
        if let model:PaymentTypeListModel = paymentTypeList?[selectedIndex]
        {
            paymentTypeCode = model.paymentTypeCode ?? 0
         }
        
        if validation() {
            
            weak var weakSelf = self

            DispatchQueue.main.async {
                
               weakSelf?.startAnimatingAfterSubmit()
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "MMM dd,yyyy hh:mm a"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = .current
            var dateText = ""
            
            var dateServer = ""
            
            if let myString:String = dateFormatterGet.string(from: tfDateOfPayment.selectedDate ) {
                
                dateServer = myString
            }
            
            if let date = dateFormatterGet.date(from:dateServer) {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            var params:NSDictionary = [:]
            
            if isFromLeads {
               
                params = ["transactionId":transactionId,
                                                      "paymentType" : "\(paymentTypeCode)",
                                                      "referenceNumber" : tfReferenceNumber.text ?? "",
                                                      "currencyId": selectedCurrencyCode?.currencyId ?? "0",
                                                      "amount": tfAmountValue.text ?? "0",
                                                      "paidDate": dateText,
                                                      "notes": tvNotes.text ?? ""

                           ]
            }
            else{
                params = ["sellerCode":sellerCode,
                                                      "paymentType" : "\(paymentTypeCode)",
                                                      "referenceNumber" : tfReferenceNumber.text ?? "",
                                                      "currencyId": selectedCurrencyCode?.currencyId ?? "0",
                                                      "amount": tfAmountValue.text ?? "0",
                                                      "paidDate": dateText,
                                                      "notes": tvNotes.text ?? ""

                           ]
            }
            
            
            let encryptedParams = ["data": encryptParams(params: params)]
            
            let apiClient:APIClient = APIClient()

            apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/declarePayment?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
                
                var message:String = failedToConnectMessage.localized()
                if let serverMessage = jsonObj?.message(){
                    message = (serverMessage.count>0) ? serverMessage : message
                }
                
                if let response = response {
                    
                    weakSelf?.stopAnimating()
                    
                    if  weakSelf?.validateStatus(response: response) ?? false {
                        
                        weakSelf?.delegate?.refreshInvoicePage(message: message)
                        weakSelf?.navigationController?.popViewController(animated: true)
                    }
                }
                
                //Show error
                 weakSelf?.stopAnimating()
                  weakSelf?.showErrorMessage(message: message)
                
            })
        }
    }
    
    func validation() -> Bool
    {
        
        if tfCCAmountPaid.text?.count ?? 0 == 0 {
            
            tfCCAmountPaid.showError(message: "Required".localized())
            
            return false
        }
        
        if tfAmountValue.text?.count ?? 0 == 0 {
            
            tfAmountValue.showError(message: "Required".localized())
            
            return false
        }
        
        return true
    }
}

extension TransferPaymentViewController:UITextFieldDelegate,CurrencySelectorVCDelegate
{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if textField ==  tfCCAmountPaid {
            
            tfCCAmountPaid.showError(message: "")
            openCurrencySelector()
            return false;
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField ==  tfReferenceNumber {

            tfReferenceNumber.showError(message: "")
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
        }
        
        if textField ==  tfCCAmountPaid {

            tfCCAmountPaid.showError(message: "")
        }
        
        if textField ==  tfAmountValue {
            
            tfAmountValue.showError(message: "")
            
        }
        return true
    }
    
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
        tfCCAmountPaid.text = selectedCurrencyCode?.currencySymbol ?? ""
    }
}



extension TransferPaymentViewController:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource
{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return paymentTypeList?.count  ?? 0 > 0 ? paymentTypeList?.count ?? 0 : 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let cell:PaymentTypeList = collectionView.dequeueReusableCell(withReuseIdentifier: "PaymentTypeList".localized(), for: indexPath) as? PaymentTypeList
        {
            
            return getCellForPaymentTypeList(cell: cell, indexPath: indexPath)
        }

        return UICollectionViewCell.init()
    }



    func getCellForPaymentTypeList(cell:PaymentTypeList,indexPath:IndexPath) -> PaymentTypeList {

        if let model:PaymentTypeListModel = paymentTypeList?[indexPath.row]
        {
            cell.lblPaymenttypeName.text = model.paymentType
            
            if selectedIndex == indexPath.row {
                
                cell.imgRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled".localized())
            }
            else
            {
                cell.imgRadio.image = UIImage.init(imageLiteralResourceName: "radio".localized())
            }
    
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        selectedIndex = indexPath.row
        collectionView.reloadData()
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 130, height: 40)
    }

}

class PaymentTypeList: UICollectionViewCell {
    
    @IBOutlet weak var imgRadio: UIImageView!
    
    @IBOutlet weak var lblPaymenttypeName: NKLabel!
}
