//
//  InvoiceListCalss.swift
//  iCanRefer
//
//  Created by Hirecraft on 18/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class InvoiceListCalss: Codable {
    var currency:[CurrencyModel]?
    var invoiceList:[InvoiceListModel]?
    var count:Int?
    var currencyId:Int?
    var netAmount:Float?
    var currencySymbol:String?
    var invoiceStatusMaster:[InvoiceStatusMasterModel]?
    var paymentTypeList:[PaymentTypeListModel]?
    
    private enum CodingKeys:String,CodingKey{
        case currency
        case invoiceList
        case count
        case currencyId
        case netAmount
        case currencySymbol
        case invoiceStatusMaster
        case paymentTypeList
    }
}

class InvoiceStatusMasterModel: Codable {
    
    var status:Int?
    var statusTitle:String?
    var colorCode:String?
    
    private enum CodingKeys:String,CodingKey
    {
        case status
        case statusTitle
        case colorCode
    }
}

class PaymentTypeListModel: Codable {
    
    var paymentType:String?
    var paymentTypeCode:Int?
   
    private enum CodingKeys:String,CodingKey
    {
        case paymentType
        case paymentTypeCode
    }
}

class InvoiceListModel: Codable {
    
    var invoiceNumber:String?
    var invoiceDate:String?
    var invoiceTitle:String?
    var invoiceDueDate:String?
    var status:Int?
    var statusTitle:String?
    var crDate:String?
    var luDate:String?
    var totalAmount:Int?
    var currencyId:Int?
    var currencyTitle:String?
    var branchCode:Int?
    var invoiceCdnPath:String?
    var branchName:String?
    var enablePay:Int?
    var isPaid:Int?
    var paidDate:String?
    var paymentType:String?
    var paymentTypeCode:Int?
    var verificationType:String?
    var verificationTypeCode:Int?
    
    private enum CodingKeys:String,CodingKey{
        case invoiceNumber
        case invoiceDate
        case invoiceTitle
        case invoiceDueDate
        case status
        case statusTitle
        case crDate
        case luDate
        case totalAmount
        case currencyId
        case currencyTitle
        case branchCode
        case invoiceCdnPath
        case branchName
        case enablePay
        case isPaid
        case paidDate
        case paymentType
        case paymentTypeCode
        case verificationType
        case verificationTypeCode
    }

}
