//
//  PartnerPostMessageVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 16/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class PartnerPostMessageVC: AppBaseViewController {

    @IBOutlet weak var txtViewPostMessage: NKTextView!
    
     @IBOutlet weak var lblPartnerCount: NKLabel!
    
     var unSelectedPartnerList = [Int]()
     var selectedPartnerList = [Int]()
     var isAll:Int = 0
     var countTotal:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblPartnerCount.textColor = UIColor.getGradientColor(index: 0).first!
        
        if unSelectedPartnerList.count == 1 && selectedPartnerList.count == 1 {
            
            lblPartnerCount.text = "All Partners selected".localized()
            
        }
            
        else{
            
            lblPartnerCount.text = "\(countTotal) Partners selected".localized()
            
        }

       
        navigationItem.title = "Post a Message".localized()
        
        txtViewPostMessage.delegate = self
        
        updateNavBar()
    }
    
    func updateNavBar() {
       
                  let btnName = UIButton()
       
                  btnName.setTitle("Submit -->".localized(), for: .normal)
                  btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                  btnName.setTitleColor(UIColor.white, for: .normal)
       
               btnName.addTarget(self, action: #selector(btnSubmitAction(_:)), for: .touchUpInside)
       
                  let rightBarButton = UIBarButtonItem()
                  rightBarButton.customView = btnName
                  self.navigationItem.rightBarButtonItem = rightBarButton
       
              }
    
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if txtViewPostMessage.text.count > 0 {
        
        submitMessage(search: txtViewPostMessage.text ?? "",selectedList: selectedPartnerList, unSelectedList: unSelectedPartnerList)
            
        }
        
        else{
            
            txtViewPostMessage.showError(message: "Required".localized())
        }
        
    }
    
    func submitMessage(search:String,selectedList:[Int],unSelectedList:[Int]){
        
            startAnimatingAfterSubmit()
            
        let params:NSDictionary = [
                                   "message" : search,
                                   "selectedUsersList":selectedList,
                                   "unSelectedUsersList":unSelectedList
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/submitMsgToUsers?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                                               
                                                               
                                if let isRequestForTrainingValue:Int = dataObj.object(forKey: "transactionId") as? Int {
                                                                   
                                                                   
                                   // weakSelf?.isRequestForTraining = isRequestForTrainingValue
                                    
                                  }
                                
                    weakSelf?.navigationController?.popToRootViewController(animated: true)
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }

}

extension PartnerPostMessageVC:UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        txtViewPostMessage.showError(message: "")
        
        return textView.text.count + (text.count - range.length) <= 4000
        
       // return true
    }

}
