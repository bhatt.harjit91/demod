//
//  PartnerListVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 16/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation

class PartnerListVC: AppBaseViewController {
    
    
    @IBOutlet weak var searchBarPartner: UISearchBar!
    
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var viewPartnerCount: UIView!
    
    @IBOutlet weak var collectionViewList: UICollectionView!
    
    @IBOutlet weak var heightPartnerCount: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewTopPartnerList: NSLayoutConstraint!
    
    @IBOutlet weak var viewTopPartnerList: UIView!
    
    @IBOutlet weak var lblNoData: NKLabel!
    
    @IBOutlet weak var lblPartnerCount: NKLabel!
    
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var heightViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var btnPostMessage: NKButton!
    
    @IBOutlet weak var lblSelectedPartnerCount: NKLabel!
    
    var partnerList:[PartnerModel]?
    var filterList = [PartnerModel]()
    var unSelectedPartnerList = [PartnerModel]()
    var selectedPartnerList = [PartnerModel]()
    var count:Int?
    var pageNo:Int = 1
    var limit:Int = 20
    var keywords:String = ""
    var currentLoc:CLLocationCoordinate2D?
    var locationManager = CLLocationManager()
    
    var isFirst:Int?
    var isDataLoading:Bool=false
    
    var btnName = UIButton()
    
    var isSelectAll:Int = 0
    
    var isAllPartnerSelected:Int = 0
    
    var isSearch:Int?
    
    var isCancelSearch:Int?
    
    var partnerCount:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateTopView()
        
        getCurrentLocation()
        
        searchBarPartner.placeholder = "Search Partners".localized()
        
        lblNoData.text = "No Data Found".localized()
        
        navigationItem.title = "Manage Partners".localized()
        
        setUpNavBar()
        
        
        
        searchPartner(search: searchBarPartner.text ?? "" , startPage:1,limit: limit, isFirst: isFirst ?? 0)
        
    }
    
    func setUpNavBar(){
        
        btnName = UIButton()
        
        btnName.setTitle("Select All".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(btnSelectAllAction), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    
    @objc func btnSelectAllAction(){
        
        
        updateSelectAll()
        
    }
    
    func updateSelectAll(){
        
        if isSelectAll == 0 {
            
            isSelectAll = 1
            
            isAllPartnerSelected = 1
            
            btnName.setTitle("Unselect All".localized(), for: .normal)
            
            if partnerList?.count ?? 0 > 0 {
                
                for model in partnerList ?? [] {
                    
                    model.isSelected = 1
                    
                    updateModel(modelMain:model)
                }
                
                selectedPartnerList = partnerList ?? []
                
            }
            
        }
        else{
            if partnerList?.count ?? 0 > 0 {
                
                isSelectAll = 0
                
                isAllPartnerSelected = 0
                
                for model in partnerList ?? [] {
                    
                    model.isSelected = 0
                    
                    updateModel(modelMain:model)
                    
                }
                
                selectedPartnerList = []
                
                unSelectedPartnerList = partnerList ?? []
                
                btnName.setTitle("Select All".localized(), for: .normal)
                
            }
            
            
        }
        
        updateTopView()
        tableViewList.reloadData()
    }
    
    @objc func btnDeleteAction(_sender:UIButton){
        
        if selectedPartnerList.count > 0 {
            
            if let modelMain:PartnerModel = selectedPartnerList[_sender.tag] {
                
                for model in partnerList ?? [] {
                    
                    if model.displayName == modelMain.displayName {
                        
                        model.isSelected = 0
                        
                        //                        if isAllPartnerSelected == 1  {
                        //
                        //                          updateModel(modelMain: model)
                        //
                        //                        }
                    }
                }
                selectedPartnerList.remove(at: _sender.tag)
                
                updateTopView()
                
                btnName.setTitle("Select All".localized(), for: .normal)
                
                //  updateSelectAll()
                
                tableViewList.reloadData()
                
            }
            
        }
    }
    
    @IBAction func btnPostMessageAction(_ sender: Any) {
        
        var selectedList = [Int]()
        var unSelectedList = [Int]()
        var countTotal = 0
        
        
        if isAllPartnerSelected == 1 {
            
            for model in partnerList ?? []{
                
                if model.isSelected == 0 {
                    
                    unSelectedList.append(model.userId ?? 0)
                    
                }
                
            }
            
            countTotal = (count ?? 0) - unSelectedList.count
            
        }
        else{
            
            for model in selectedPartnerList{
                
                if model.isSelected == 1 {
                    
                    selectedList.append(model.userId ?? 0)
                    
                }
            }
            
            countTotal = selectedList.count
            
        }
        
        openPostMessage(unSelectedUsers: unSelectedList, selectedUsers: selectedList,totalCount:countTotal)
        
    }
    
    func openPostMessage(unSelectedUsers:[Int],selectedUsers:[Int],totalCount:Int) {
        
//        var totalCount = count ?? 0
//
//        if isSearch ?? 0 == 1 {
//
//         totalCount = filterList.count
//
//        }
//
//        let finalCount  = totalCount - unSelectedPartnerList.count
//
       partnerCount = totalCount
        
        let storyBoard = UIStoryboard.init(name: PartnerSB, bundle: nil)
        if let postMessage:PartnerPostMessageVC = storyBoard.instantiateViewController(withIdentifier: "PartnerPostMessageVC") as? PartnerPostMessageVC {
            
            postMessage.unSelectedPartnerList = unSelectedUsers
            postMessage.selectedPartnerList = selectedUsers
            postMessage.isAll = isAllPartnerSelected
            postMessage.countTotal = partnerCount ?? 0
            navigationController?.pushViewController(postMessage, animated: true)
            
        }
        
        
    }
    
    
    
    func updateTopView() {
        
        viewTopPartnerList.alpha = 0
        
        heightViewTopPartnerList.constant = 0
        
        heightViewBottom.constant = 0
        
        viewBottom.alpha = 0
        
        heightPartnerCount.constant = 40
                   
        viewPartnerCount.alpha = 1
        
        lblPartnerCount.text = "\(count ?? 0) Partners"
        
        lblSelectedPartnerCount.alpha = 0
        
        viewTopPartnerList.layer.cornerRadius = 5
        viewTopPartnerList.layer.borderWidth = 1.0
        viewTopPartnerList.layer.borderColor = UIColor.lightGray.cgColor
        viewTopPartnerList.layer.shadowColor = UIColor.black.cgColor
        viewTopPartnerList.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewTopPartnerList.layer.shadowOpacity = 0.3
        viewTopPartnerList.layer.shadowRadius = 4.0
        
        if selectedPartnerList.count > 0 && selectedPartnerList.count < 20 {
            
            var finalCount = 0
            
            viewTopPartnerList.alpha = 1
            
             lblSelectedPartnerCount.alpha = 1
            
//            heightPartnerCount.constant = 0
//
//            viewPartnerCount.alpha = 0
            
            heightViewTopPartnerList.constant = 90
            
            collectionViewList.alpha = 1
            
            collectionViewList.reloadData()
            
            var totalCount = count ?? 0
                      
                      if isSearch ?? 0 == 1 {
                          
                          totalCount = filterList.count
                          
                      }
                      else{
                        
                        totalCount = count ?? 0
                        
                       }
            
                     if isAllPartnerSelected == 1 && isSearch ?? 0 == 0 {
                      
                       finalCount  = totalCount - unSelectedPartnerList.count
                        
                      }
                     else{
                             finalCount = selectedPartnerList.count
                        }
                      
                    //  lblPartnerCount.text = "\(totalCount) Partners"
                      
                    //  partnerCount = finalCount
                      
                      lblSelectedPartnerCount.alpha = 1
                      
                      if totalCount == finalCount {
                          
                         // lblPartnerCount.text = "\(totalCount) Partners".localized()
                          
                          lblSelectedPartnerCount.text = "\(totalCount) Selected".localized()
                      }
                      else{
                          
                         // lblPartnerCount.text = "\(finalCount) Partners"
                          
                          lblSelectedPartnerCount.text = "\(finalCount) Selected".localized()
                          
                      }
            
        }
        
        
        if selectedPartnerList.count > 20 && isAllPartnerSelected == 1{
            
           // heightPartnerCount.constant = 50
            
           // viewPartnerCount.alpha = 1
            
            viewTopPartnerList.alpha = 0
            
            collectionViewList.alpha = 0
            
            var totalCount = count ?? 0
            
            if isSearch ?? 0 == 1 {
                
                totalCount = filterList.count
                
            }
            
            let finalCount  = totalCount - unSelectedPartnerList.count
            
            lblPartnerCount.text = "\(totalCount) Partners"
            
          //  partnerCount = finalCount
            
            lblSelectedPartnerCount.alpha = 1
            
            if totalCount == finalCount {
                
                lblPartnerCount.text = "\(totalCount) Partners".localized()
                
                lblSelectedPartnerCount.text = "\(totalCount) Selected".localized()
            }
            else{
                
                lblPartnerCount.text = "\(totalCount) Partners"
                
                lblSelectedPartnerCount.text = "\(finalCount) Selected".localized()
                
            }
            
            heightViewTopPartnerList.constant = 0
            
        }
        
        for model in partnerList ?? []{
            
            if model.isSelected == 1 {
                
                heightViewBottom.constant = 65
                viewBottom.alpha = 1
                //btnPostMessage.alpha = 1
            }
        }
        
        
    }
    
    func getCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch locationAuthorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.startUpdatingLocation()
                currentLoc = locationManager.location?.coordinate
                
            }
        case .restricted, .denied:
            
            //  showLocationRequiredPage()
            
            print("Access Denied")
            
        }
    }
    
    //MARK: Location manager
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if  let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
            
            
            currentLoc = locValue
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        stopAnimating()
    }
    
    
    
    func searchPartner(search:String ,startPage:Int,limit:Int, isFirst: Int){
        
        
        if isFirst == 1 {
            
            startAnimatingAfterSubmit()
            
        }
        else{
            
            startAnimating()
        }
        
        let params:NSDictionary = ["startPage": startPage,
                                   "limit":limit,
                                   "keywords" : search
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/myreferredPartners?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                let jsonResponse = try decoder.decode(PartnerListApiModel.self, from: data)
                                
                                if let apiResponse:[PartnerModel] = jsonResponse.partnerList {
                                    
                                    if weakSelf?.partnerList?.count ?? 0 > 0 {
                                        
                                        for dict in apiResponse{
                                            
                                            // weakSelf?.partnerList?.append(dict)
                                            
                                            if !(weakSelf?.partnerList?.contains(dict))! {
                                                
                                                if weakSelf?.isSelectAll == 1{
                                                    dict.isSelected = 1
                                                }
                                                
                                                weakSelf?.selectedPartnerList.append(dict)
                                                
                                                weakSelf?.partnerList?.append(dict)
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    else{
                                        
                                        weakSelf?.partnerList = apiResponse
                                    }
                                    
                                }
                                
                                if self.isSearch == 1 {
                                    
                                    for modelMain in self.selectedPartnerList {
                                        
                                        for model in self.partnerList ?? [] {
                                            
                                            if modelMain.userId == model.userId {
                                                
                                                model.isSelected = 1
                                                
                                            }
                                            
                                        }
                                    }
                                }
                                
                                if let apiRespose:Int = jsonResponse.count {
                                    
                                    weakSelf?.count = apiRespose
                                    
                                }
                                
                                weakSelf?.isFirst = 1
                                
                                weakSelf?.updateTopView()
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    
}

extension PartnerListVC:UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if partnerList?.count ?? 0 > 0 {
            
            tableViewList.alpha = 1
        
          return partnerList?.count ?? 0
            
        }
        
        tableViewList.alpha = 0
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:PartnerListCell = tableView.dequeueReusableCell(withIdentifier: "PartnerListCell", for: indexPath) as? PartnerListCell{
            
            
            return getCellForPartnerList(cell: cell, indexPath: indexPath)
        }
        
        
        return UITableViewCell.init()
        
    }
    
    func getCellForPartnerList(cell:PartnerListCell,indexPath:IndexPath) -> PartnerListCell {
        
        cell.selectionStyle = .none
        
        if let model:PartnerModel = partnerList?[indexPath.row]{
            
            cell.lblPartnerName.text = model.displayName ?? ""
            
            cell.lblDate.text = "Partner ID \(model.partnerId ?? "")"
            
            cell.imgViewPartner.image = nil
            
            if model.profilePicture?.count ?? 0 > 0 {
                cell.imgViewPartner.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.profilePicture ?? "")")), completed: nil)
            }
            else{
                cell.imgViewPartner.image = UIImage.init(named: "profilePicIcon")
            }
            
            
            if model.isSelected == 1 {
                
                cell.imgViewCheckBox.alpha = 1
                
                cell.imgViewCheckBox.image = UIImage.init(named: "tickBlue")
            }
            else{
                  cell.imgViewCheckBox.alpha = 0
                    // cell.imgViewCheckBox.image = UIImage.init(named: "checkBox")
                
            }
            
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:PartnerModel = partnerList?[indexPath.row]{
            
            if model.isSelected == 1 {
                
                model.isSelected = 0
                
                if let index = selectedPartnerList.index(where: {$0.userId ?? 0 == model.userId ?? 0}) {
                    selectedPartnerList.remove(at: index)
                }
                
                updateView()
                
            }
            else if model.isSelected == 0 {
                
                model.isSelected = 1
                
                selectedPartnerList.append(model)
                
                updateView()
                
            }
            
            if isAllPartnerSelected == 1 {
                
                updateModel(modelMain: model)
                
            }
            
            
            updateTopView()
        }
        
        tableViewList.reloadData()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
           
           isDataLoading = false
           
       }
       
       
       //Pagination
    
       func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
           
           
           if ((tableViewList.contentOffset.y + tableViewList.frame.size.height) >= tableViewList.contentSize.height)
           {
               if partnerList?.count ?? 0 < count ?? 0 {
                   if !isDataLoading{
                       isDataLoading = true
                       pageNo = pageNo+1
                       
                       searchPartner(search: searchBarPartner.text ?? "", startPage: self.pageNo, limit: limit, isFirst: isFirst ?? 0)
                       
                   }
                   
               }
               
           }
           
       }
    
    func updateModel(modelMain:PartnerModel){
        
        if modelMain.isSelected == 0{
            
            unSelectedPartnerList.append(modelMain)
            isSelectAll = 0
            
        }
        
        if modelMain.isSelected == 1{
            
            
            if let index = unSelectedPartnerList.index(where: {$0.userId ?? 0 == modelMain.userId ?? 0}) {
                unSelectedPartnerList.remove(at: index)
                isSelectAll = 1
            }
            
        }
        
    }
    
    func updateView() {
        
        if isSearch == 0 {
            
            if partnerList?.count ?? 0 > 0 {
                
                for model in partnerList ?? [] {
                    
                    if model.isSelected == 0 {
                        
                        btnName.setTitle("Select All".localized(), for: .normal)
                        
                        return
                        
                    }
                    else{
                        
                        btnName.setTitle("Unselect All".localized(), for: .normal)
                        
                    }
                }
                
            }
        }
        else{
            
            isSearch = 0
            
            if selectedPartnerList.count > 0 && isSelectAll == 1 {
                
                for model in selectedPartnerList {
                    
                    if model.isSelected == 0 {
                        
                        btnName.setTitle("Select All".localized(), for: .normal)
                        
                        return
                        
                    }
                    else{
                        
                        btnName.setTitle("Unselect All".localized(), for: .normal)
        
                   }
                }
            }
        }
    }
}


extension PartnerListVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize.init(width: 80, height: 80)

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if selectedPartnerList.count > 0 {
            
            return selectedPartnerList.count
            
        }
        
        return 0
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if let cell:PartnerCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PartnerCollectionViewCell", for: indexPath) as? PartnerCollectionViewCell{
            
            
            return getCellForPartnerList(cell: cell, indexPath: indexPath)
            
            
        }
        
        
        return UICollectionViewCell.init()
    }
    
    func getCellForPartnerList(cell:PartnerCollectionViewCell,indexPath:IndexPath) -> PartnerCollectionViewCell {
        
        if let model:PartnerModel = selectedPartnerList[indexPath.row]{
            
            cell.imgViewPartner.image = nil
            
            if model.profilePicture?.count ?? 0 > 0 {
                cell.imgViewPartner.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.profilePicture ?? "")")), completed: nil)
            }
            else{
                cell.imgViewPartner.image = UIImage.init(named: "profilePicIcon")
            }
            
            cell.lblName.text = model.displayName ?? ""
            
            cell.btnCancel.tag = indexPath.row
            
            cell.btnCancel.addTarget(self, action: #selector(btnDeleteAction(_sender:)), for: .touchUpInside)
            
            
        }
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedPartnerList.count > 0 {
            
            if let modelMain:PartnerModel = selectedPartnerList[indexPath.row] {
                
                for model in partnerList ?? [] {
                    
                    if model.displayName == modelMain.displayName {
                        
                        model.isSelected = 0
                        
                        //                        if isAllPartnerSelected == 1  {
                        //
                        //                          updateModel(modelMain: model)
                        //
                        //                        }
                    }
                }
                
                selectedPartnerList.remove(at:indexPath.row)
                
                collectionViewList.reloadData()
                
                updateTopView()
                
                btnName.setTitle("Select All".localized(), for: .normal)
                
                //  updateSelectAll()
                
                tableViewList.reloadData()
                
            }
            
        }
    }
    
}
extension PartnerListVC: UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) -> Void {
        
        searchBarPartner.showsCancelButton = true
        
        btnName.alpha = 0
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        
        cancelSearch()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        isSearch = 1
        
        partnerList?.removeAll()
        
        pageNo = 1
        
        btnName.alpha = 1
        
        searchPartner(search: searchBar.text ?? "",startPage:pageNo,limit: limit, isFirst: isFirst ?? 0)
        
        cancelSearch()
        
    }
    
    @objc func cancelSearch(){
        
        searchBarPartner.showsCancelButton = false
        searchBarPartner.resignFirstResponder()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        isSearch = 1
        
        partnerList?.removeAll()
        
        pageNo = 1
        
        btnName.alpha = 0
        
        searchPartner(search: searchBar.text ?? "",startPage:pageNo,limit: limit, isFirst: isFirst ?? 0)
        
        cancelSearch()
        
        // tableViewList.scrollsToTop = true
        
        
    }
    
    
}


class PartnerListCell:UITableViewCell{
    
    @IBOutlet weak var imgViewPartner: RCImageView!
    
    @IBOutlet weak var lblPartnerName: NKLabel!
    
    @IBOutlet weak var lblDate: NKLabel!
    
    @IBOutlet weak var imgViewCheckBox: UIImageView!
}

class PartnerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgViewPartner: RCImageView!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var lblName: NKLabel!
}
