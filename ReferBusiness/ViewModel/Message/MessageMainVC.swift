//
//  MessageMainVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 23/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class MessageMainVC: AppBaseViewController,UpdateArchiveCount {

    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblNoData: NKLabel!
    
    @IBOutlet weak var lblArchiveCount: NKLabel!
    
    @IBOutlet weak var ViewArchiveCountHeightConstraint: NSLayoutConstraint!
    
    var messageList:[MessageListModel]?
    
    var transactionId:Int = 0
    var count:Int?
    var isDataLoading:Bool=false
    var pageNo:Int=1
    var limit:Int=10
    var offset:Int=0
    
    var isFirst:Int = 0
    
    
    var cnt = 0
    
     var messageId = Set<Int>()
    
    let attributedTextOne:[NSAttributedString.Key: Any] = [
           .font: UIFont.fontForType(textType: 6),
           .foregroundColor: UIColor.btnLinkColor]
    
    let attributedTextTwo:[NSAttributedString.Key: Any] = [
              .font: UIFont.fontForType(textType: 6),
              .foregroundColor: UIColor.primaryColor]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !checkLogin(){
                                 
            openSignInPage()
                          
        }

         navigationItem.title = "iCanRefer".localized()
        
         lblNoData.text = "No Messages".localized()
        
          isFirst = 1
        
          NotificationCenter.default.addObserver(self, selector: #selector(callMessageApi(notification:)), name: NSNotification.Name(rawValue: "callMessage"), object: nil)
        
    }
    
    deinit {
              
             NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "callMessage"), object: nil)
        }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if !checkLogin(){
                                 
            openSignInPage()
                          
        }
        
        if UserDefaults.standard.integer(forKey: "selectedTab") == 1
        {
            messageList?.removeAll()
            
            UserDefaults.standard.set(0, forKey: "selectedTab")
            
            pageNo = 1
            
            fetchMessageList(startPage: pageNo, limit: limit, isFirst: 1)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
           stopAnimating()
       }
    
    @IBAction func btnArchiveCountClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: MessageSB, bundle: nil)
        
        let referralStatusPage:ArchiveList = storyBoard.instantiateViewController(withIdentifier: "ArchiveList") as! ArchiveList
        referralStatusPage.delegate = self
        navigationController?.pushViewController(referralStatusPage, animated: true)
        
    }
    
    
    func updateRestoreCountToNext(archiveCount: Int) {
        
        pageNo = 1
        
        fetchMessageList(startPage: pageNo, limit: limit, isFirst: 1)
        
//        weak var weakSelf = self
//
//        if archiveCount > 0
//        {
//            let archiveCountText = "\("Archived Items".localized())\(" (")\(String(describing: archiveCount))\(")")"
//
//            let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
//
//            let underlineAttributedString = NSAttributedString(string: archiveCountText, attributes: underlineAttribute)
//
//            weakSelf?.lblArchiveCount.attributedText = underlineAttributedString
//
//            weakSelf?.ViewArchiveCountHeightConstraint.constant = 45
//            weakSelf?.lblArchiveCount.isHidden = false
//        }
//        else
//        {
//            weakSelf?.ViewArchiveCountHeightConstraint.constant = 0
//            weakSelf?.lblArchiveCount.isHidden = true
//        }
    }
    
    ////
    
    //Mark: - ArchiveBtn Clicked.....
    
    @objc func respondAction(sender: UIButton){
        
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewList)
        let indexPath = self.tableViewList.indexPathForRow(at: buttonPosition)
        if indexPath != nil {
            
            if let model:MessageListModel = self.messageList?[indexPath?.row ?? 0]
            {
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let referralStatusPage:ReferralPostMessageVC = storyBoard.instantiateViewController(withIdentifier: "ReferralPostMessageVC") as! ReferralPostMessageVC
                referralStatusPage.transactionId = model.transactionId ?? 0
                navigationController?.pushViewController(referralStatusPage, animated: true)
                
            }
        }
        
        
    }
    
    //////
    
    @objc func archiveBtnClicked(sender: UIButton){
        
        let alert = UIAlertController(title: nil, message: "Are you sure to Archive?".localized(), preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.btnLinkColor
        
        alert.addAction(UIAlertAction(title: "No".localized(),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewList)
            let indexPath = self.tableViewList.indexPathForRow(at: buttonPosition)
            if indexPath != nil {
                
                if self.messageList?.count ?? 0 > 0
                {
                    if let model:MessageListModel = self.messageList?[indexPath?.row ?? 0]
                    {
                        self.getArchiveCount(transactionId: model.transactionId ?? 0, tag: indexPath?.row ?? 0)
                    }
                }
            }

            self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getArchiveCount(transactionId:Int,tag:NSInteger){
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        
        startAnimatingAfterSubmit()

        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/archiveTransaction?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&transactionId=\(transactionId)&isArchive=1"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()

            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                
                                ////////////////
                                
                                if let archiveCount = dataObj["archiveCount"] as? Int {
                                    
                                    let archiveCountText = "\("Archived Items".localized())\(" (")\(String(describing: archiveCount))\(")")"
                                    
                                    let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
                                    
                                    let underlineAttributedString = NSAttributedString(string: archiveCountText, attributes: underlineAttribute)
                                    
                                    weakSelf?.lblArchiveCount.attributedText = underlineAttributedString
                                    
                                    weakSelf?.ViewArchiveCountHeightConstraint.constant = 45
                                    weakSelf?.lblArchiveCount.isHidden = false
                                }

                                self.messageList?.remove(at: tag)
                                
                                                                        
                                self.messageId.remove(transactionId)
                                                                          
                                                                
                                
                                let indexPath = IndexPath(item: tag, section: 0)

                                
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)

                                let jsonResponse = try decoder.decode(NextTransactionListApiModel.self, from: data)

                                if let apiResponse:MessageListModel = jsonResponse.nextTransaction {

                                     DispatchQueue.main.async {
                                        
                                    weakSelf?.messageList?.append(apiResponse)

                                        
                                        self.tableViewList.reloadData()
                                        
                                       // self.tableViewList.scrollToRow(at: IndexPath(row: tag, section: 0), at: .top, animated: true)
                                    
                                }

                                }
                                else
                                {
                                    self.tableViewList.beginUpdates()
                                    
                                    self.tableViewList.deleteRows(at: [indexPath], with: .fade)
                                    
                                    self.tableViewList.endUpdates()
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {

                                    weakSelf?.count = apiResponse

                                }

                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    @objc func resposeAction(sender: UIButton){
        
        
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewList)
        let indexPath = self.tableViewList.indexPathForRow(at: buttonPosition)
        if indexPath != nil {
            
            if let model:MessageListModel = self.messageList?[indexPath?.row ?? 0]
            {
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let referralStatusPage:ReferralPostMessageVC = storyBoard.instantiateViewController(withIdentifier: "ReferralPostMessageVC") as! ReferralPostMessageVC
                referralStatusPage.transactionId = model.transactionId ?? 0
                navigationController?.pushViewController(referralStatusPage, animated: true)
            }
        }
        
    }
  
    func fetchMessageList(startPage:Int,limit:Int,isFirst:Int) {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        
        if isFirst == 1 {
          startAnimating()
        }
        else{
            startAnimatingAfterSubmit()
        }

        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getTransactionMessages?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&startPage=\(startPage)&limit=\(limit)&isArchive=0"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(MessageListApiModel.self, from: data)
                                
                                if let apiResponse:[MessageListModel] = jsonResponse.transactionList {
                                    
                                    
                                    if weakSelf?.messageList?.count ?? 0 > 0
                                    {
                                        
                                        
                                        for dataDict in apiResponse{
                    
                                            
                                            if !self.messageId.contains(dataDict.transactionId ?? 0) {
                                                
                                                weakSelf?.messageList?.append(dataDict)
                                                
                                                self.messageId.insert(dataDict.transactionId ?? 0)
                                            
                                            print(self.messageList?.count ?? 0)
                                        }
                                        }
                                    }
                                    else
                                    {
                                        
                                        weakSelf?.messageList = apiResponse

                                        for dict in apiResponse{
                                          
                                            self.messageId.insert(dict.transactionId ?? 0)
                                            
                                        }
                                    }
                                    
                                }
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.count = apiResponse
                                    
                                }
                                
                                ////////////////
                                
                                if let archiveCount = dataObj["archiveCount"] as? Int {
                                    
                                    if archiveCount > 0
                                    {
                                        let archiveCountText = "\("Archived Items".localized())\(" (")\(String(describing: archiveCount))\(")")"
                                        
                                        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
                                        
                                        let underlineAttributedString = NSAttributedString(string: archiveCountText, attributes: underlineAttribute)
                                        
                                        weakSelf?.lblArchiveCount.attributedText = underlineAttributedString
                                        
                                        weakSelf?.ViewArchiveCountHeightConstraint.constant = 45
                                        weakSelf?.lblArchiveCount.isHidden = false
                                    }
                                    else
                                    {
                                        weakSelf?.ViewArchiveCountHeightConstraint.constant = 0
                                        weakSelf?.lblArchiveCount.isHidden = true
                                    }
                                }
                                
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    @objc func callMessageApi(notification: NSNotification) {
     if let isFlag = notification.userInfo?["update"] as? Int {
         
         if isFlag == 1 {
            
            pageNo = 1
             
             fetchMessageList(startPage: pageNo, limit: 10, isFirst: 1)
             
         }
     
       }
    }
    
}

extension MessageMainVC:UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate{
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if messageList?.count ?? 0 > 0{
            
          tableViewList.alpha = 1
            
          lblNoData.alpha = 0
        
          return messageList?.count ?? 0
            
        }
        else{
            
             tableViewList.alpha = 0
            
             lblNoData.alpha = 1
            
            return 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:MessageListCell = tableView.dequeueReusableCell(withIdentifier: "MessageListCell", for: indexPath) as? MessageListCell
        {
            
            return getCellForMessageList(cell: cell, indexPath: indexPath)
            
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model:MessageListModel = messageList?[indexPath.row]
        {
            if model.isBuyer == 1 {
               
                let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
                let productDetail:HomeBuyerProductDetailVC = storyBoard.instantiateViewController(withIdentifier: "HomeBuyerProductDetailVC") as! HomeBuyerProductDetailVC
                productDetail.transactionId = model.transactionId ?? 0
                navigationController?.pushViewController(productDetail, animated: true)
                
            }

           else if model.isReferralMember == 1 || model.isListingPartner == 1 {
                
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let newProductPage:ReferalManagerVC = storyBoard.instantiateViewController(withIdentifier: "ReferalManagerVC") as! ReferalManagerVC
                newProductPage.transactionId = model.transactionId ?? 0
                newProductPage.isEditable = true
                 newProductPage.isSeller = model.isSeller ?? 0
                navigationController?.pushViewController(newProductPage, animated: true)
                
            }
                
          else if model.isSeller == 1 {
                
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let newProductPage:ReferalManagerVC = storyBoard.instantiateViewController(withIdentifier: "ReferalManagerVC") as! ReferalManagerVC
                newProductPage.transactionId = model.transactionId ?? 0
                 newProductPage.isEditable = false
                newProductPage.isSeller = model.isSeller ?? 0
                navigationController?.pushViewController(newProductPage, animated: true)
                
            }
            
            else{
                
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let referralStatusPage:ReferralPostMessageVC = storyBoard.instantiateViewController(withIdentifier: "ReferralPostMessageVC") as! ReferralPostMessageVC
                referralStatusPage.transactionId = model.transactionId ?? 0
                navigationController?.pushViewController(referralStatusPage, animated: true)
            }
            
            
        }
    }
    
    func getCellForMessageList(cell:MessageListCell,indexPath:IndexPath) -> MessageListCell {
        
        var responseText = ""
        
        cell.selectionStyle = .none
        
        if messageList?.count ?? 0 > 0 {
        
        if let model:MessageListModel = messageList?[indexPath.row]
        {
            cell.lblTitle.text = model.title ?? ""
            
            if model.isBuyer == 0 && model.isSeller == 0 && model.isListingPartner == 0 && model.isReferralMember == 0 && model.actualMessage?.count ?? 0 > 0 {
                
               cell.lblDes.text = model.actualMessage ?? ""
                
            }
            
            responseText = "\(model.responseCount ?? 0)\(" ")\("Responses".localized())"
            
            cell.lblResponse.text = responseText
            cell.lblRespond.text = "\("Respond".localized())"
            cell.lblResponse.font = UIFont.textErrorFont
        
            cell.lblRef.text = "\("Ref : ")\(model.transactionId ?? 0)"
            
            cell.stackViewResponse.arrangedSubviews[1].isHidden = true
            
            cell.heightViewVideo.constant = 0
            
            cell.viewVideo.alpha = 1
            
            if model.isVideo != 1 {
                                    
            if model.productLogo?.count ?? 0 > 0 {
                
                cell.imgViewMessage.aspectRation(9.0 / 16.0).isActive = true
                
                cell.imgViewMessage.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
                
                
            }
            else
            {
                cell.heightConstraintOfImgView.constant = 0
            }
           
            }
            else{
                
                cell.heightViewVideo.constant = 150
                           
                cell.viewVideo.alpha = 1
            }
    
            if model.responseCount ?? 0 > 0 {
                
             //  cell.stackViewResponse.arrangedSubviews[1].isHidden = false
                responseText = "\("Respond(".localized())\(model.responseCount ?? 0))"
                 cell.lblRespond.text = responseText
                
            }
            
            if model.unreadResponseCount ?? 0 > 0 {
            // create an NSMutableAttributedString that we'll append everything to
            let fullString =  NSMutableAttributedString(string:"\(responseText) ",
                       attributes: attributedTextOne)

            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            
            image1Attachment.image = UIImage(named: "unReadMsg")
                
                let iconsSize = CGRect(x: 0, y: -5, width: 15, height: 15)
                               
                image1Attachment.bounds = iconsSize
            
            // wrap the attachment in its own attributed string so we can append it
                
            let image1String = NSAttributedString(attachment: image1Attachment)
                
            // add the NSTextAttachment wrapper to our full string, then add some more text.
            
            let finalStr = NSAttributedString(string:" \(model.unreadResponseCount ?? 0)", attributes: attributedTextTwo)
            
            fullString.append(image1String)
            fullString.append(finalStr)
            
             cell.lblRespond.attributedText = fullString
            
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.referredDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblDate.text = dateText
            
            cell.btnRespond.tag = indexPath.row
            
            cell.imgViewrespond.tintColor = UIColor.getGradientColor(index: 0).first
            
            cell.btnResponse.tag = indexPath.row
            
            cell.btnRespond.addTarget(self, action: #selector(respondAction(sender:)), for: .touchUpInside)
            
            cell.btnResponse.addTarget(self, action:  #selector(resposeAction(sender:)), for: .touchUpInside)
            
            /// Archive///
            
            let color = UIColor.getGradientColor(index: 0).first!
            cell.imgViewArchive.setImageColor(color: color)
            cell.lblArchive.text = "Archive".localized()
            cell.btnArchive.tag = indexPath.row
            cell.btnArchive.addTarget(self, action: #selector(archiveBtnClicked(sender:)), for: .touchUpInside)

            ////////////
        }
        
      }
        return cell
    }
    
    ////////
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {

        isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((tableViewList.contentOffset.y + tableViewList.frame.size.height) >= tableViewList.contentSize.height)
        {
            if messageId.count < count ?? 0 {
                
                if !isDataLoading{
                    isDataLoading = true
                    self.pageNo=self.pageNo+1
                    self.offset=self.limit * self.pageNo
                    fetchMessageList(startPage: pageNo, limit: limit, isFirst: 0)
                }
            }
        }
    }
}

class MessageListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var lblDes: NKLabel!
    
    @IBOutlet weak var imgViewMessage: UIImageView!
    
    @IBOutlet weak var lblResponse: NKLabel!
    
    @IBOutlet weak var lblRespond: NKLabel!
    
    @IBOutlet weak var viewResponse: UIView!
    
    @IBOutlet weak var viewRespond: UIView!
    
    @IBOutlet weak var stackViewResponse: UIStackView!
    
    @IBOutlet weak var btnRespond: UIButton!
    
    @IBOutlet weak var imgViewrespond: UIImageView!
    
    @IBOutlet weak var btnResponse: UIButton!
        
    @IBOutlet weak var imgViewArchive: UIImageView!
    
    @IBOutlet weak var lblRef: NKLabel!
    
    @IBOutlet weak var lblDate: NKLabel!
    
    @IBOutlet weak var lblArchive: NKLabel!
    
    @IBOutlet weak var heightConstraintOfImgView: NSLayoutConstraint!
    @IBOutlet weak var btnArchive: UIButton!
    
    @IBOutlet weak var viewVideo: UIView!
    
    @IBOutlet weak var heightViewVideo: NSLayoutConstraint!
    
}
