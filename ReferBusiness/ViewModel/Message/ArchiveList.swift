//
//  ArchiveList.swift
//  ICanRefer
//
//  Created by Vivek on 26/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol UpdateArchiveCount: AnyObject {
    
    func updateRestoreCountToNext(archiveCount:Int)
}

class ArchiveList: AppBaseViewController {

    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblNoData: NKLabel!
    
    var messageList:[MessageListModel]?
    
    var transactionId:Int = 0
    var count:Int?
    var isDataLoading:Bool=false
    var pageNo:Int=1
    var limit:Int=10
    var offset:Int=0
    
    weak var delegate: UpdateArchiveCount?

    
    override func viewDidLoad() {
        super.viewDidLoad()

         navigationItem.title = "iCanRefer".localized()
        
         lblNoData.text = "No Data Found".localized()
        
        fetchMessageList(startPage: pageNo, limit: limit)
    }
    
    ////
    
    //Mark: - ArchiveBtn Clicked.....

    
    @objc func restoreArchiveBtnClicked(sender: UIButton){
        
        /////
        
        let alert = UIAlertController(title: nil, message: "Are you sure to Restore?".localized(), preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.btnLinkColor
        
        alert.addAction(UIAlertAction(title: "No".localized(),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableViewList)
            let indexPath = self.tableViewList.indexPathForRow(at: buttonPosition)
            if indexPath != nil {
                
                if self.messageList?.count ?? 0 > 0
                {
                    if let model:MessageListModel = self.messageList?[indexPath?.row ?? 0]
                    {
                        self.updateRestoreCount(transactionId: model.transactionId ?? 0, tag: indexPath?.row ?? 0)
                    }
                }
            }
            
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func updateRestoreCount(transactionId:Int,tag:NSInteger){
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        startAnimating()
        
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/archiveTransaction?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&transactionId=\(transactionId)&isArchive=0"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                
                                ////////////////
                                
                                if let archiveCount = dataObj["archiveCount"] as? Int {
                                    
                                    weakSelf?.delegate?.updateRestoreCountToNext(archiveCount: archiveCount)

                                }

                                
                                self.messageList?.remove(at: tag)
                                
                                let indexPath = IndexPath(item: tag, section: 0)

                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(NextTransactionListApiModel.self, from: data)
                                
                                if let apiResponse:MessageListModel = jsonResponse.nextTransaction {
                                    
                                    DispatchQueue.main.async {
                                        
                                        weakSelf?.messageList?.append(apiResponse)
                                        
                                        self.tableViewList.reloadData()
                                        
                                    }
                                    
                                }
                                else
                                {
                                    self.tableViewList.beginUpdates()
                                    
                                    self.tableViewList.deleteRows(at: [indexPath], with: .fade)
                                    
                                    self.tableViewList.endUpdates()
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.count = apiResponse
                                    
                                }
                                
                                let isUpdated:[String: Int] = ["update": 1]

                                                                                                           
                                                                                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callMessage"), object: nil, userInfo: isUpdated)

                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    
    @objc func resposeAction(sender: UIButton){

        let buttonPosition = sender.convert(CGPoint.zero, to: tableViewList)
        let indexPath = tableViewList.indexPathForRow(at: buttonPosition)
        if indexPath != nil {
            
            if self.messageList?.count ?? 0 > 0
            {
                if let model:MessageListModel = self.messageList?[indexPath?.row ?? 0]
                {
                    let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                    let referralStatusPage:ReferralPostMessageVC = storyBoard.instantiateViewController(withIdentifier: "ReferralPostMessageVC") as! ReferralPostMessageVC
                    referralStatusPage.transactionId = model.transactionId ?? 0
                    referralStatusPage.comingFromArchive = true
                    navigationController?.pushViewController(referralStatusPage, animated: true)
                }
            }
        }
    }
  
    func fetchMessageList(startPage:Int,limit:Int) {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getTransactionMessages?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&isArchive=1"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(MessageListApiModel.self, from: data)
                                
                                if let apiResponse:[MessageListModel] = jsonResponse.transactionList {
                                    
                                    
                                    if weakSelf?.messageList?.count ?? 0 > 0
                                    {
                                        for dataDict in apiResponse{
                                            
                                            weakSelf?.messageList?.append(dataDict)
                                            
                                            print(self.messageList?.count ?? 0)
                                        }
                                    }
                                    else
                                    {
                                        weakSelf?.messageList = apiResponse
                                        
                                    }
                                    
                                }
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.count = apiResponse
                                    
                                }
                                
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }

}

extension ArchiveList:UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if messageList?.count ?? 0 > 0{
            
          tableViewList.alpha = 1
            
          lblNoData.alpha = 0
        
          return messageList?.count ?? 0
            
        }
        else{
            
             tableViewList.alpha = 0
            
             lblNoData.alpha = 1
            
            return 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:ArchiveListCell = tableView.dequeueReusableCell(withIdentifier: "ArchiveListCell", for: indexPath) as? ArchiveListCell
        {
            
            return getCellForMessageList(cell: cell, indexPath: indexPath)
            
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:MessageListModel = messageList?[indexPath.row]
        {
            if model.isBuyer == 1 {
                
                let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
                let productDetail:HomeBuyerProductDetailVC = storyBoard.instantiateViewController(withIdentifier: "HomeBuyerProductDetailVC") as! HomeBuyerProductDetailVC
                productDetail.transactionId = model.transactionId ?? 0
                productDetail.comingFromArchive = true
                navigationController?.pushViewController(productDetail, animated: true)
                
            }

            else if model.isReferralMember == 1 || model.isListingPartner == 1 {
                
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let newProductPage:ReferalManagerVC = storyBoard.instantiateViewController(withIdentifier: "ReferalManagerVC") as! ReferalManagerVC
                newProductPage.transactionId = model.transactionId ?? 0
                newProductPage.isEditable = false
                newProductPage.comingFromArchive = true
                navigationController?.pushViewController(newProductPage, animated: true)
                
            }
                
           else if model.isSeller == 1 {
                
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let newProductPage:ReferalManagerVC = storyBoard.instantiateViewController(withIdentifier: "ReferalManagerVC") as! ReferalManagerVC
                newProductPage.transactionId = model.transactionId ?? 0
                 newProductPage.isEditable = true
                newProductPage.comingFromArchive = true
                navigationController?.pushViewController(newProductPage, animated: true)
                
            }
            
            else{
                ///need
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let referralStatusPage:ReferralPostMessageVC = storyBoard.instantiateViewController(withIdentifier: "ReferralPostMessageVC") as! ReferralPostMessageVC
                referralStatusPage.transactionId = model.transactionId ?? 0
                referralStatusPage.comingFromArchive = true
                navigationController?.pushViewController(referralStatusPage, animated: true)
                
            }
            
        }
    }
    
    func getCellForMessageList(cell:ArchiveListCell,indexPath:IndexPath) -> ArchiveListCell {
        
        cell.selectionStyle = .none
        
        if let model:MessageListModel = messageList?[indexPath.row]
        {
            cell.lblTitle.text = model.title ?? ""
            
            cell.lblDes.text = model.actualMessage ?? ""
            
            cell.lblResponse.text = "\("Responses".localized())\(" (")\(model.responseCount ?? 0)\(")")"
            
            cell.lblRef.text = "\("Ref : ")\(model.transactionId ?? 0)"
            
            cell.stackViewResponse.arrangedSubviews[1].isHidden = true

            if model.productLogo?.count ?? 0 > 0 {
                
                cell.imgViewMessage.aspectRation(9.0 / 16.0).isActive = true
                
                cell.imgViewMessage.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
            }
            else
            {
                cell.heightImageView.constant = 0
            }
    
            if model.responseCount ?? 0 > 0 {
                
               cell.stackViewResponse.arrangedSubviews[1].isHidden = false
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterGet.timeZone = .current
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.referredDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblDate.text = dateText
            
            cell.imgViewrespond.tintColor = UIColor.getGradientColor(index: 0).first
            
            cell.btnResponse.tag = indexPath.row
            
            cell.btnResponse.addTarget(self, action:  #selector(resposeAction(sender:)), for: .touchUpInside)
            
            /// Archive///
            
            print("indexPath.rowindexPath.rowindexPath.rowindexPath.rowindexPath.rowindexPath.row")
            
            print(indexPath.row)
            
            let color = UIColor.getGradientColor(index: 0).first!
            cell.imgViewRestore.setImageColor(color: color)
            cell.lblArchive.text = "Restore".localized()
            cell.btnArchive.tag = indexPath.row
            cell.btnArchive.addTarget(self, action: #selector(restoreArchiveBtnClicked(sender:)), for: .touchUpInside)

            ////////////
        }
        
        
        return cell
    }
    
    
    ////////
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((tableViewList.contentOffset.y + tableViewList.frame.size.height) >= tableViewList.contentSize.height)
        {
            if messageList?.count ?? 0 < count ?? 0 {
                
                if !isDataLoading{
                    isDataLoading = true
                    self.pageNo=self.pageNo+1
                    self.offset=self.limit * self.pageNo
                    fetchMessageList(startPage: pageNo, limit: limit)
                }
            }
        }
    }

    
}

class ArchiveListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var lblDes: NKLabel!
    
    @IBOutlet weak var imgViewMessage: UIImageView!
    
    @IBOutlet weak var lblResponse: NKLabel!///
    
    @IBOutlet weak var imgViewRestore: UIImageView!
    @IBOutlet weak var viewResponse: UIView!
    
    @IBOutlet weak var stackViewResponse: UIStackView!
    
    @IBOutlet weak var imgViewrespond: UIImageView!
    
    @IBOutlet weak var btnResponse: UIButton!///
    
    @IBOutlet weak var heightImageView: NSLayoutConstraint!
    
    @IBOutlet weak var lblRef: NKLabel!
    
    @IBOutlet weak var lblDate: NKLabel!
    
    @IBOutlet weak var lblArchive: NKLabel!
    
    @IBOutlet weak var btnArchive: UIButton!
}
