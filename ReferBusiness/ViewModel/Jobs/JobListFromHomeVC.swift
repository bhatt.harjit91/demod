//
//  JobListFromHomeVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 19/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class JobListFromHomeVC: AppBaseViewController {

    @IBOutlet weak var tableViewList: UITableView!
       
       @IBOutlet weak var lblProductCount: NKLabel!
       
       @IBOutlet weak var lblNoFound: NKLabel!
       
       var sellerCode:Int = 0
       
       var productCount:Int = 0
       
       var statusList:[SellerStatusModel]?
       
       var productList:[ProductModel]?
       
       var colorCodes:ColorModel?
       
       var responseTypes:[ResponseTypeModel]?
       
       var currency:[CurrencyModel]?
       
       var branchList:[ProductBranchModel]?
       
       var attachmentTypes:[AttachmentTypeModel]?
       
       weak var delegate:JobListVCDelegate?
       
       var isAdmin:Int?
       
       var isPartnerForCompany:Int?
       
       var companyType:Int = 1
       
       var jobTypeList:[JobTypeFilter]?
    
       var productCode:Int = 0
    
      var filterMasterList:[FilterReferralModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchJobList()
        
        navigationItem.title = "Jobs".localized()
        
    }
    

  func fetchJobList() {
      
      let apiClient:APIClient = APIClient()
      //lblCartEmpty.text = "Loading...".localized()
      
      weak var weakSelf = self
      startAnimating()
      apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/productList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&productType=\(1)"), params: nil, completionHandler:{response, jsonObj in
          
          var message:String = failedToConnectMessage.localized()
          if let serverMessage = jsonObj?.message(){
              message = (serverMessage.count>0) ? serverMessage : message
          }
          weakSelf?.stopAnimating()
          //weakSelf?.lblCartEmpty.text = ""
          if let response = response {
              if  weakSelf?.validateStatus(response: response) ?? false{
                  if let dataStr:String =  jsonObj?.dataString(){
                      if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                          
                          do {
                              let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                              let decoder = JSONDecoder()
                              print(dataObj)
                              
                              let jsonResponse = try decoder.decode(ProductApiModel.self, from: data)
                              
                              /// vivek
                              
                              if let apiResponse:[AttachmentTypeModel] = jsonResponse.attachmentTypes {
                                  
                                  weakSelf?.attachmentTypes = apiResponse
                                  
                              }
                              
                              if let apiResponse:[ProductModel] = jsonResponse.productList {
                                  
                                  weakSelf?.productList = apiResponse
                                  
                              }
                              if let apiResponse:Int = jsonResponse.count {
                                  
                                  weakSelf?.productCount = apiResponse
                                  
                                  weakSelf?.lblProductCount.text = ("Jobs (\(apiResponse))")
                              }
                              
                              if let apiResponse:[ResponseTypeModel] = jsonResponse.responseTypes {
                                  
                                  weakSelf?.responseTypes = apiResponse
                                  
                              }
                              
                              if let apiResponse:[CurrencyModel] = jsonResponse.currency {
                                  
                                  weakSelf?.currency = apiResponse
                                  
                              }
                              
                              
                              
                              if let apiResponse:[JobTypeFilter] = jsonResponse.jobTypeList {
                                  
                                  weakSelf?.jobTypeList = apiResponse
                                  
                              }
                              
                              if let apiResponse:[ProductBranchModel] = jsonResponse.branchList {
                                  
                                  weakSelf?.branchList = apiResponse
                                  
                              }
                              
                            
                              weakSelf?.tableViewList.reloadData()
                              
                              return
                          } catch {
                              print(error)
                              message = failedToConnectMessage.localized()
                              
                          }
                      }
                  }
              }
          }
          
          weakSelf?.showErrorMessage(message: message)
          
      })
      
  }

}

extension JobListFromHomeVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if productList?.count ?? 0 > 0{
            
            tableViewList.alpha = 1
            
            lblNoFound.alpha = 0
            
            return productList?.count ?? 0
        }
        else{
            
            tableViewList.alpha = 0
            
            lblNoFound.alpha = 1
            
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:ProductListCell = tableView.dequeueReusableCell(withIdentifier: "ProductListCell", for: indexPath) as? ProductListCell {
            
            return getCellForProductList(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:ProductModel = productList?[indexPath.row]
        {
            
            
            let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
            let referalLeadsPage:ReferalLeadsVC = storyBoard.instantiateViewController(withIdentifier: "ReferalLeadsVC") as!   ReferalLeadsVC
            //
            referalLeadsPage.sellerCode = model.sellerCode ?? 0
            referalLeadsPage.progressValue = "-1"
            referalLeadsPage.isFromHomeNew = true
            referalLeadsPage.isFromHome = true
            referalLeadsPage.strTitle = "Referrals Leads".localized()
            referalLeadsPage.filterMasterList = filterMasterList ?? []
            referalLeadsPage.productCode = model.productCode ?? 0
            navigationController?.pushViewController(referalLeadsPage, animated: true)
            
        }
    }
    
    func getCellForProductList(cell:ProductListCell,indexPath:IndexPath) -> ProductListCell {
        
        cell.selectionStyle = .none
        
        if let model:ProductModel = productList?[indexPath.row]
        {
            cell.lblProductNameText.text = model.productName ?? ""
            cell.lblStatusText.text = "\(model.transactionCount ?? 0) Resumes"
            
            cell.imageView?.image = nil
            
            if model.productLogo?.count ?? 0 > 0 {
                cell.imgViewProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
            }
            else{
                cell.imgViewProduct.image = UIImage.init(named: "jobDefault")
            }
            
//            if model.status == 1 {
//                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.activeColor ?? "")
//            }
//
//            else if model.status == 2 {
//                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.inactiveColor ?? "")
//            }
//
//            else if model.status == 3 {
//                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.blacklistedColor ?? "")
//            }
            
        }
        
        return cell
    }
    
    
    
}

//productCode
