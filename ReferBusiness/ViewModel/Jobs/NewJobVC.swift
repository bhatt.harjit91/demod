//
//  NewJobVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 25/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol NewJobVCDelegate: class {
    func didAddedJob()
}

class NewJobVC: SendOTPViewController ,CurrencySelectorVCDelegate{
    
    @IBOutlet weak var txtProductName: RCTextField!
    
    @IBOutlet weak var lblBranchestext: NKLabel!
    
    @IBOutlet weak var txtViewProductDes: NKTextView!
    
    @IBOutlet weak var collectionViewAttachment: UICollectionView!
    
    @IBOutlet weak var lblAttachmentTitle: NKLabel!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var lblStatusTitle: NKLabel!
    
    @IBOutlet weak var txtReferralCurrency: RCTextField!
    
    @IBOutlet weak var txtReferralPrice: RCTextField!
    
    @IBOutlet weak var btnSumit: NKButton!
    
    @IBOutlet weak var tableViewBranchList: UITableView!
    
    @IBOutlet weak var heightViewBranchList: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewAttachment: UIImageView!
    
    @IBOutlet weak var txtTagLine: RCTextField!
    
    @IBOutlet weak var txtKeywords: RCTextField!
    
    @IBOutlet weak var lblSpecialMsgPartner: NKLabel!
    
    @IBOutlet weak var txtViewMsgPartner: NKTextView!
    
    @IBOutlet weak var lblSpecialMsgUser: NKLabel!
    
    @IBOutlet weak var txtViewMsgUser: NKTextView!
    
    @IBOutlet weak var lblProductDes: NKLabel!
    
    @IBOutlet weak var btnProductDes: UIButton!
    
    @IBOutlet weak var heightHtmlProductDes: NSLayoutConstraint!
    
    @IBOutlet weak var viewHtmlProductDes: RCView!
    
    @IBOutlet weak var topSpaceForHtmlDes: NSLayoutConstraint!
    
    @IBOutlet weak var txtExpFrom: RCTextField!
    
    
    @IBOutlet weak var txtExpTo: RCTextField!
    
    @IBOutlet weak var txtJobType: RCTextField!
    
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    var branchCode:[Int] = []
    
    var selectedCurrencyCode : CurrencyModel?
    
    var sellerCode:Int = 0
    
    var productCode:Int = 0
    
    var attachmentsProduct:[AttachmentBannerTypeModel] = []
    
    var productCount:Int = 0
    
    var selectedIndex:Int = 1
    
    var statusList:[SellerStatusModel]?
    
    var productList:[ProductModel]?
    
    var colorCodes:ColorModel?
    
    var responseTypes:[ResponseTypeModel]?
    
    var currency:[CurrencyModel]?
    
    var branchList:[ProductBranchModel]?
    
    var attachmentTypes:[AttachmentTypeModel]?
    
    var referralValue = 0.0
    
    var referralFeeType:Int = 0
    
    var referralCurrencyId:Int?
    
    var productDetail:ProductDetailModel?
    
    var isDetail:Bool = false
    
    var btnName = UIButton()
    
    weak var delegate:NewJobVCDelegate?
    
    var htmlDes:String = ""
    
    var companyType:Int = 1
    
    let countAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 8),
        .foregroundColor: UIColor.btnLinkColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var jobTypeList:[JobTypeFilter]?
    
    var jobOptions:[String] = []
    
    var jobType:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewLoad()
        
    }
    
    func viewLoad() {
        
        lblBranchestext.attributedText = addPlaceHolderWithText(placeholder: "Branches".localized())
        txtReferralCurrency.delegate = self
        txtReferralPrice.delegate = self
        
        navigationItem.title = "Job".localized()
        
        btnName.setTitle("Submit -->", for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(btnSubmitAction(_:)), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        let productDesTitle = NSMutableAttributedString(string: "Click here to add job description".localized(),
                                                        attributes: countAttributes)
        
        lblProductDes.attributedText = productDesTitle
        
        //  btnProductDes.setAttributedTitle(productDesTitle, for: .normal)
        
        btnProductDes.addTarget(self, action: #selector(openHtmlView), for: .touchUpInside)
        
        if isDetail == true{
            
            btnName.setTitle("Update -->".localized(), for: .normal)
            btnSumit.setTitle("Update".localized(), for: .normal)
            
            btnName.alpha = 0
            btnSumit.alpha = 0
            
            if isAdmin == 1 || isPartnerForCompany == 1 {
                
                btnName.alpha = 1
                btnSumit.alpha = 1
            }
            
            fetchProductDetail()
        }
        
        if companyType != 1 {
            
            topSpaceForHtmlDes.constant = 10
            heightHtmlProductDes.constant = 40
            viewHtmlProductDes.alpha = 1
            
            
        }
        else{
            topSpaceForHtmlDes.constant = 0
            heightHtmlProductDes.constant = 0
            viewHtmlProductDes.alpha = 0
            
        }
        
        if jobTypeList?.count ?? 0 > 0 {
            
            for model in jobTypeList ?? []{
                
                jobOptions.append(model.title ?? "")
                
            }
            
            txtJobType.pickerOptions = jobOptions
            
            txtJobType.dropDownPicker?.reloadAllComponents()
            
            txtJobType.selectedOption = jobOptions.first ?? ""
            
        }
        
        
        
        txtProductName.delegate = self
        txtViewProductDes.delegate = self
        txtReferralCurrency.delegate = self
        txtReferralPrice.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let heightBranch = branchList?.count ?? 0
        
        heightViewBranchList.constant = CGFloat((heightBranch * 44) + 40)
        
        tableViewBranchList.reloadData()
    }
    
    func updateUI() {
        
        if let model:ProductDetailModel = productDetail {
            
            txtProductName.text = model.productName ?? ""
            txtViewProductDes.text = model.description ?? ""
            attachmentsProduct = model.attachmentList ?? []
            
            let shortDes = model.shortDescription ?? ""
            let des = model.description ?? ""
            
            if companyType != 1 {
                
                if shortDes.count ?? 0 > 0 {
                    
                    txtViewProductDes.placeholder = ""
                    txtViewProductDes.text = shortDes.removeWhitespaceSpecial()
                    
                }
                htmlDes = des.removeWhitespaceSpecial()
                
            }
            else{
                
                if model.description?.count ?? 0 > 0 {
                    
                    txtViewProductDes.placeholder = ""
                    txtViewProductDes.text = des.removeWhitespaceSpecial()
                    
                }
            }
            selectedIndex = model.status ?? 0
            
            referralCurrencyId = model.referralCurrencyId ?? 0
            
            
            
            if model.referralCurrencySymbol?.count ?? 0 > 0 {
                
                txtReferralCurrency.text = "\( model.referralCurrencySymbol ?? "")"
            }
            
            if model.keywords?.count ?? 0 > 0 {
                
                txtKeywords.text = model.keywords ?? ""
                
            }
            
            if model.referralFormMsg?.count ?? 0 > 0 {
                
                txtViewMsgPartner.placeholder = ""
                
                txtViewMsgPartner.text = model.referralFormMsg ?? ""
                
            }
            
            if model.enquiryOrApplyJobFormMsg?.count ?? 0 > 0 {
                
                txtViewMsgUser.placeholder = ""
                
                txtViewMsgUser.text = model.enquiryOrApplyJobFormMsg ?? ""
                
            }
            
            
            if model.tagLine?.count ?? 0 > 0 {
                
                txtTagLine.text = model.tagLine ?? ""
                
            }
            
            if model.referralFee ?? 0 > 0 {
                
                txtReferralPrice.text = "\( model.referralFee ?? 0)"
            }
            
            
            branchCode = model.branchCode ?? []
            
            
            referralCurrencyId = model.referralCurrencyId ?? 0
            
            for index in 0..<branchCode.count{
                
                for modelMain in branchList ?? []  {
                    
                    if modelMain.branchCode == branchCode[index]{
                        
                        modelMain.isSelected = 1
                        
                    }
                }
            }
            
            jobType = model.jobType ?? 0
            
            if jobTypeList?.count ?? 0 > 0 {
                
                for model in jobTypeList ?? []{
                    
                    if model.jobType == jobType{
                        
                        txtJobType.selectedOption = model.title ?? ""
                        
                    }
                    
                }
                
            }
            
            txtExpFrom.text = "\( model.expFrom ?? 0)"
            txtExpTo.text = "\(model.expTo ?? 0)"
            
            txtExpTo.delegate = self
            txtExpFrom.delegate = self
            
            
            tableViewBranchList.reloadData()
            
            collectionViewStatus.reloadData()
            
            collectionViewAttachment.reloadData()
            
            //  updateSegmentUI()
            
        }
    }
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachmentsProduct.count > sender.tag {
            attachmentsProduct.remove(at: sender.tag)
            collectionViewAttachment.reloadData()
        }
    }
    
    @IBAction func btnAttachmentProductAction(_ sender: Any) {
        
        isAttachment = true
        isDocument = true
        openImagePicker()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            
            if status {
                let arrayEachImage:AttachmentBannerTypeModel = AttachmentBannerTypeModel()
                arrayEachImage.cdnPath = urlString
                arrayEachImage.fileName = imageName
                arrayEachImage.attachmentType = 0
                weakSelf?.attachmentsProduct.append(arrayEachImage)
                weakSelf?.collectionViewAttachment.reloadData()
                weakSelf?.isAttachment = false
            }
        }
        
    }
    
    @objc func openHtmlView() {
        
        let storyBoard = UIStoryboard.init(name: ProductSB, bundle: nil)
        
        if let htmlSelector:HtmlEditorVC = storyBoard.instantiateViewController(withIdentifier: "HtmlEditorVC") as? HtmlEditorVC {
            
            htmlSelector.htmlText = htmlDes
            htmlSelector.delegate = self
            
            navigationController?.pushViewController(htmlSelector, animated: true)
            
        }
    }
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
        
        txtReferralCurrency.text = selectedCurrencyCode?.currencySymbol ?? ""
        txtReferralCurrency.removeError()
        referralCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        
        
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if txtProductName.text?.count == 0 {
            
            txtProductName.showError(message: "Required".localized())
            
            return
        }
        
        if txtViewProductDes.text?.count == 0 {
            
            txtViewProductDes.showError(message: "Required".localized())
            
            return
            
        }
        
        if let cost = Double(txtReferralPrice.text ?? "0.0") {
            
            referralValue = cost
        }
        
        if txtReferralCurrency.text?.count == 0 {
            
            txtReferralCurrency.showError(message: "Required".localized())
    
        }
        
        if referralValue <= 0.0 {
            
            txtReferralPrice.showError(message: "Required".localized())
            
            return
        }
        
        
        
        branchCode = []
        
        if let intArray:[ProductBranchModel] = branchList {
            
            for model in intArray  {
                
                if model.isSelected == 1 {
                    
                    branchCode.append(model.branchCode ?? 0)
                    
                }
            }
        }
        
        if jobTypeList?.count ?? 0 > 0 {
            
            for model in jobTypeList ?? []{
                
                if model.title == txtJobType.selectedOption {
                    
                    jobType = model.jobType ?? 0
                    
                }
                
            }
            
        }
        
        if branchCode.count == 0 {
            
            showErrorMessage(message: "Please select a Branch".localized())
            
            return
        }
        
        
        submitProductDetails()
    }
    
    func submitProductDetails(){
        
        var shortDescription = ""
        var description = ""
        var expTo = 0
        var expFrom = 0
        
        if let exp = Int(txtExpTo.text ?? "0") {
           expTo = exp
        }
        
        if let exp = Int(txtExpFrom.text ?? "0") {
            expFrom = exp
        }
        if expTo < expFrom {
           showErrorMessage(message: "'Experience From' should be lesser than 'Experience TO'".localized())
           return
        }
        
        if companyType != 1{
            
            shortDescription = txtViewProductDes.text ?? ""
            description = htmlDes
            
        }
        else{
            
            description = txtViewProductDes.text ?? ""
            shortDescription = ""
        }
        
        if jobTypeList?.count ?? 0 > 0 {
            
            for model in jobTypeList ?? []{
                
                if model.title == txtJobType.selectedOption {
                    
                    jobType = model.jobType ?? 0
                    
                }
                
            }
            
        }
        
        var array : [Any] = []
        
        for dict in attachmentsProduct
        {
            var model:[String : Any] = [:]
            model["cdnPath"] = dict.cdnPath
            model["attachmentType"] = dict.attachmentType
            model["fileName"] = dict.fileName
            array.append(model)
        }
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["sellerCode": sellerCode,
                                   "productCode": productCode,
                                   "productName" : txtProductName.text ?? "",
                                   "description" : description,
                                   "shortDescription":shortDescription,
                                   
                                   "attachmentList":array,
                                   
                                   
                                   "referralFeeType": 2,
                                   "referralCurrencySymbol":txtReferralCurrency.text ?? "",
                                   
                                   "referralCurrencyId":referralCurrencyId ?? 0,
                                   
                                   "referralFee":referralValue,
                                   
                                   "status" : selectedIndex,
                                   
                                   "branchCode": branchCode,
                                   
                                   "keywords":txtKeywords.text ?? "",
                                   
                                   "tagLine":txtTagLine.text ?? "",
                                   
                                   "productType":1,
                                   
                                   "isReferralFeeType":1,
                                   
                                   "referralFormMsg":txtViewMsgPartner.text ?? "",
                                   
                                   "enquiryOrApplyJobFormMsg":txtViewMsgUser.text ?? "",
                                   
                                   "expTo":expTo,
                                   
                                   "expFrom":expFrom,
                                   
                                   "jobType":jobType ?? 0
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerProducts?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                //                            let apiResponse = try decoder.decode(CategoryListModel.self, from: data)
                                //                            weakSelf?.categoryNewList = apiResponse.categoriesList ?? []
                                //                            weakSelf?.searchCategoryList = apiResponse.categoriesList ?? []
                                //
                                //                            DispatchQueue.main.async {
                                //
                                //                                weakSelf?.collectionViewCategory.reloadData()
                                //                            }
                                
                                weakSelf?.delegate?.didAddedJob()
                                
                                
                                weakSelf?.showInfoMessage(message: message.localized())
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    func fetchProductDetail() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/productDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&productCode=\(productCode)&sellerCode=\(sellerCode)&productType=\(1)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ProductDetailApiModel.self, from: data)
                                
                                if let apiResponse:ProductDetailModel = jsonResponse.productDetails {
                                    
                                    weakSelf?.productDetail = apiResponse
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
}


extension NewJobVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return branchList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:NewProductBranchCell = tableView.dequeueReusableCell(withIdentifier: "NewProductBranchCell", for: indexPath) as? NewProductBranchCell {
            
            return getCellForProductBranch(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:ProductBranchModel = branchList?[indexPath.row]{
            
            if model.isSelected == 0 {
                
                model.isSelected = 1
                
            }
            else{
                model.isSelected = 0
            }
            
            tableViewBranchList.reloadData()
        }
        
        
    }
    
    func getCellForProductBranch(cell:NewProductBranchCell,indexPath:IndexPath) -> NewProductBranchCell {
        
        cell.selectionStyle = .none
        
        if let model:ProductBranchModel = branchList?[indexPath.row]{
            
            cell.lblTitle.text = model.branchName ?? ""
            
            if model.isSelected == 0 {
                
                cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBox")
                
            }
            else{
                cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
            }
        }
        
        return cell
    }
    
}


extension NewJobVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            return CGSize.init(width: collectionViewStatus.frame.width / 4, height: 40.0)
            
        }
        
        if collectionView == collectionViewAttachment
            
        {
            return CGSize.init(width: 100, height: 100)
            
        }
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewAttachment
        {
            if attachmentsProduct.count > 0 {
                
                return attachmentsProduct.count
            }
                
            else
            {
                return 0
            }
            
        }
        
        if collectionView == collectionViewStatus{
            
            if statusList?.count ?? 0 > 0 {
                
                return statusList?.count ?? 0
            }
            
        }
        
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewAttachment
        {
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachment(cell: cell, indexPath: indexPath)
                
            }
        }
        else{
            
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForStatus(cell: cell, indexPath: indexPath)
                
            }
            
        }
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionViewStatus
            
        {
            selectedIndex = indexPath.row
            
            collectionViewStatus.reloadData()
            
        }
        
        //        if collectionView == collectionViewAttachment {
        //                    if let model:AttachmentBannerTypeModel = attachmentsProduct[indexPath.row] {
        //                
        //              if let imagePath = model.cdnPath {
        //                  
        //                  let strFileType = model.fileName?.fileExtension()
        //
        //                  if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
        //                {
        //                               
        //                       
        //               let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
        //                  let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
        //                  imageViewer.strImg = imagePath
        //                  navigationController?.pushViewController(imageViewer, animated: true)
        //                    
        //                }
        //            else{
        //              
        //              
        //              openWebView(urlString: model.cdnPath ?? "", title: model.fileName ?? "", subTitle: "")
        //                  }
        //                
        //            }
        //                
        //          }
        //              
        //        }
        
    }
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete .addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        if let model:AttachmentBannerTypeModel = attachmentsProduct[indexPath.row]
        {
            if let imagePath = model.cdnPath
            {
                if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                    
                    let strFileType = model.fileName?.fileExtension()
                    
                    if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                    {
                        
                        cell.imgView.sd_setImage(with: url, completed: nil)
                        
                    }
                    else{
                        
                        cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                    }
                }
                
                for modelMain in attachmentTypes ?? [] {
                    
                    if modelMain.attachmentType == model.attachmentType {
                        
                        cell.lblTitle.text = modelMain.title ?? ""
                        
                    }
                    
                }
                
            }
            
        }
        return cell
    }
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:SellerStatusModel = statusList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.statusTitle ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
}


extension NewJobVC:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtReferralCurrency {
            
            openCurrencySelector()
            
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let txtField:RCTextField = textField as? RCTextField{
            txtField.showError(message: "")
        }
        
        if textField ==  txtExpFrom ||  textField ==  txtExpTo{
            
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        return true
    }
    
}

extension NewJobVC:HtmlEditorVCDelegate{
    
    func didAddedString(htmlStr:String){
        
        htmlDes = htmlStr
        
        if htmlDes.count > 0 {
            
            let productDesTitle = NSMutableAttributedString(string: "Click here to update job description".localized(), attributes: countAttributes)
            
            lblProductDes.attributedText = productDesTitle
            
        }
        
    }
    
}


extension NewJobVC:UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if companyType != 1 {
            
            if textView == txtViewProductDes {
                txtViewProductDes.showError(message: "")
                
                return textView.text.count + (text.count - range.length) <= 150
            }
        }
        
        return true
    }
    
}
