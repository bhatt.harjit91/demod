//
//  CurrencySelectorVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 24/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol CurrencySelectorVCDelegate: class {
    func didFinishSelectingCurrency(selectedCurrency:CurrencyModel)
}

class CurrencySelectorVC: AppBaseViewController,UISearchResultsUpdating,UISearchControllerDelegate,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    
     var searchController: UISearchController!
     var currencyList:[CurrencyModel] = []
     var searchResult:[CurrencyModel] = []
     var selectedCurrency:CurrencyModel?
    
     weak var delegate : CurrencySelectorVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Select Currency".localized()
        
        searchResult = currencyList
        setupNavBar()
       
    }
    
    func setupNavBar(){
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.searchBar.tintColor = UIColor.primaryColor
        searchController.searchBar.sizeToFit()
        searchController.searchBar.autocapitalizationType = .none
        //searchController.searchBar.barTintColor = UIColor.primaryColor
        searchController.searchBar.placeholder = "Search".localized()
        if #available(iOS 11.0, *) {
            searchController.hidesNavigationBarDuringPresentation = false
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
            
            searchController.searchBar.setTextFieldColor(color: UIColor.primaryColor)
        } else {
            // Fallback on earlier versions
            
            searchController.searchBar.delegate = self
            tblView.tableHeaderView = searchController.searchBar
            
        }
        
        
        searchController.searchBar.localizeSearchBar()
        searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = false // default is YES
        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */
        definesPresentationContext = true
        
    }
   
    //MARK: SearchUpdater
    func updateSearchResults(for searchController: UISearchController) {
        let searchText:String = searchController.searchBar.text!
        filterCurrency(searchText: searchText)
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterCurrency(searchText: searchText)
    }
    func filterCurrency(searchText:String) {
        if searchText.count>0{
            searchResult = currencyList.filter({( currency : CurrencyModel) -> Bool in
                let countryName = currency.currencySymbol ?? ""
                return (countryName.lowercased().contains(searchText.lowercased()))
            })
        }
        else{
            searchResult = currencyList
        }
        
        tblView.reloadData()
    }
    //MARK: TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell")
        let currencyModel:CurrencyModel = searchResult[indexPath.row]
        
        let currencyName = currencyModel.currencySymbol ?? ""
        cell?.textLabel?.text = currencyName
        cell?.accessoryType = .none
        if currencyModel.currencyId == selectedCurrency?.currencyId {
            cell?.accessoryType = .checkmark
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currencyModel:CurrencyModel = searchResult[indexPath.row]
        delegate?.didFinishSelectingCurrency(selectedCurrency: currencyModel)
        navigationController?.popViewController(animated: true)
    }
}
