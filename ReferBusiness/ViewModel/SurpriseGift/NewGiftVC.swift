//
//  NewGiftVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 11/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol NewGiftVCCDelegate: class {
    
    func didAddedGift()
    
}
class NewGiftVC: SendOTPViewController {
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var btnLogo: NKButton!
    
    @IBOutlet weak var txtTitle: RCTextField!
    
    @IBOutlet weak var txtViewDes: NKTextView!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var lblDisplayFor: NKLabel!
    
    
    @IBOutlet weak var lblBuyer: NKLabel!
    
    @IBOutlet weak var lblPartner: NKLabel!
    
    @IBOutlet weak var lblBoth: NKLabel!
    
    @IBOutlet weak var imgRadioBuyer: UIImageView!
    
    @IBOutlet weak var imgRadioPartner: UIImageView!
    
    @IBOutlet weak var imgRadioBoth: UIImageView!
    
    @IBOutlet weak var txtStartDate: RCTextField!
    
    @IBOutlet weak var txtEndDate: RCTextField!
    
    @IBOutlet weak var txtScheduleText: NKTextView!
    
    @IBOutlet weak var lblStartDate: NKLabel!
    
    @IBOutlet weak var lblEndDate: NKLabel!
    
    weak var delegate:NewGiftVCCDelegate?
    
    var selectedIndex:Int = 0
    
    var statusList:[SellerStatusModel]?
    
    var sellerCode:Int = 0
    
    var isDetail:Bool = false
    
    var giftCode:Int = 0
    
    var strLogo:String?
    
    var strLogoName:String?
    
    var activeTextField = UITextField()
    
    var selectedDisplayType:Int = 3
    
    var dateArray:[Any] = []
    
    var timeArray:[Any] = []
    
    var sellerGiftDetails:GiftModel?
    
    var isAdmin:Int?
      
    var isPartnerForCompany:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Gift".localized()
        
        txtTitle.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Title (not visible to others)".localized())
        
        txtViewDes.changePlaceHolderColor = true
        txtViewDes.placeholder = "Description".localized()
        
        txtScheduleText.changePlaceHolderColor = true
        txtScheduleText.placeholder = "Special Notes".localized()

        lblStartDate.text = "Start Date & Time".localized()
        
        lblEndDate.text = "End Date & Time".localized()
        
        txtScheduleText.textColor = .primaryColor
        
        btnLogo.addTarget(self, action: #selector(btnLogoAction), for: .touchUpInside)
        
        btnSubmit.addTarget(self, action: #selector(btnSubmitAction), for: .touchUpInside)
        
        updateNavBar()
        
        txtStartDate.tag = 11
        txtEndDate.tag = 12
        
        txtStartDate.RCDelegate = self
        txtEndDate.RCDelegate = self
        
        updateDisplayType(type:selectedDisplayType)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isDetail {
            
            fetchGiftDetail()
            
        }
    }
    
    func updateNavBar(){
        let btnName = UIButton()
        
        if isDetail {
            btnName.setTitle("Update -->".localized(), for: .normal)
            btnSubmit.setTitle("Update".localized(), for: .normal)
            
            btnName.alpha = 0
            btnSubmit.alpha = 0
            
            if isAdmin == 1 || isPartnerForCompany == 1 {
                                    
                    btnName.alpha = 1
                    btnSubmit.alpha = 1
            }
        }
        else{
            btnName.setTitle("Submit -->".localized(), for: .normal)
            btnSubmit.setTitle("Submit".localized(), for: .normal)
        }
        
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: txtStartDate.selectedDate)
        
        txtEndDate.selectedDate = nextMonth!
        
        txtEndDate.dateTimePicker.minimumDate = Date.init()
        txtStartDate.dateTimePicker.minimumDate = Date.init()
        
        
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(btnSubmitAction), for:.touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    func adjustTextViewHeight(arg : NKTextView)
      {
        
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        //arg.isScrollEnabled = false
      }
    
    func updateUI(){
        
        if sellerGiftDetails != nil {
            
            txtTitle.text = sellerGiftDetails?.title ?? ""
            
            txtViewDes.text = sellerGiftDetails?.description ?? ""
            
            txtViewDes.placeholder = ""
            
            txtScheduleText.placeholder = ""
            
            txtScheduleText.text = sellerGiftDetails?.scheduleText ?? ""
            
            //adjustUITextViewHeight(arg: txtScheduleText)
            
            if txtViewDes.text.count > 100 {
                adjustTextViewHeight(arg: txtViewDes)
            }
            
            if txtScheduleText.text.count > 100 {
              adjustTextViewHeight(arg: txtScheduleText)
            }
            
           // adjustTextViewHeight(arg: txtViewDes)
            
            strLogo = sellerGiftDetails?.imagePath ?? ""
            
            strLogoName = sellerGiftDetails?.imageFileName ?? ""
            
            
            if sellerGiftDetails?.status == 1 {
                
                selectedIndex = 0
                
            }
            if sellerGiftDetails?.status == 2 {
                
                selectedIndex = 1
                
            }
            
            if sellerGiftDetails?.status == 3 {
                
                selectedIndex = 2
                
            }
            
            if sellerGiftDetails?.imagePath?.count ?? 0 > 0 {
                imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(sellerGiftDetails?.imagePath ?? "")")), completed: nil)
            }
            
            selectedDisplayType = sellerGiftDetails?.displayFor ?? 0
            
            updateDisplayType(type:selectedDisplayType)
            
            
            
            collectionViewStatus.reloadData()
            
            var startDateTime = ""
            
            var endDateTime = ""
            
            if let dateDict = sellerGiftDetails?.giftSchedule?.first
            {
                if let startDate = dateDict["startDate"]
                {
                   // txtStartDate.selectedDate = getDate(stringDate: startDate)
                    
                    startDateTime = startDate
                    
                }
                if let endDate = dateDict["endDate"]
                {
                   // txtEndDate.selectedDate = getDate(stringDate: endDate)
                    
                    endDateTime = endDate
                    
                }
                
              //  dateArray = sellerGiftDetails?.giftSchedule ?? [[:]]
            }
            
            if let timeDict = sellerGiftDetails?.giftTimings?.first
            {
                if let startTime = timeDict["startTime"]
                {
                    
                   // txtStartTime.selectedDate = getTime(stringDate: startTime)
                    
                    startDateTime = "\(startDateTime)\(" ")\(startTime)"
                    
                }
                
                if let endTime = timeDict["endTime"]
                {
                   
                      //  txtEndTime.selectedDate = getTime(stringDate: endTime)
                    
                     endDateTime = "\(endDateTime)\(" ")\(endTime)"
                
                }
                
                
              //  timeArray = sellerGiftDetails?.giftTimings ?? [[:]]
                
            }
            
            txtStartDate.selectedDate = getDateTime(stringDateTime: startDateTime)
            
            txtEndDate.selectedDate = getDateTime(stringDateTime: endDateTime)
        }
        
        
    }
    
    func updateDisplayType(type:Int) {
        
        if type == 1 {
            
            imgRadioBuyer.image = UIImage.init(named: "radioFilled")
            imgRadioPartner.image = UIImage.init(named: "radio")
            imgRadioBoth.image = UIImage.init(named: "radio")
            
        }
        if type == 2 {
            
            imgRadioPartner.image = UIImage.init(named: "radioFilled")
            imgRadioBuyer.image = UIImage.init(named: "radio")
            imgRadioBoth.image = UIImage.init(named: "radio")
            
        }
        if type == 3 {
            
            imgRadioBoth.image = UIImage.init(named: "radioFilled")
            imgRadioBuyer.image = UIImage.init(named: "radio")
            imgRadioPartner.image = UIImage.init(named: "radio")
            
        }
        
    }
    
    
    @IBAction func btnBuyerAction(_ sender: Any) {
        
        selectedDisplayType = 1
        
        updateDisplayType(type: selectedDisplayType)
        
    }
    
    @IBAction func btnPartnerAction(_ sender: Any) {
        
        selectedDisplayType = 2
        
        updateDisplayType(type: selectedDisplayType)
        
    }
    
    
    @IBAction func btnBothAction(_ sender: Any) {
        
        selectedDisplayType = 3
        
        updateDisplayType(type: selectedDisplayType)
        
    }
    
    @objc func btnLogoAction() {
        
        isAttachment = true
        
        openImagePicker()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            //     weakSelf?.stopAnimating()
            if status {
                print(urlString, imageName)
                weakSelf?.strLogo = urlString
                weakSelf?.strLogoName = imageName
                if weakSelf?.strLogo?.count ?? 0 > 0 {
                    self.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(weakSelf?.strLogo ?? "")")), completed: nil)
                    
                    self.isAttachment = false
                    
                }
                
            }
            
        }
    }
    
    
    @objc func btnSubmitAction() {
        
        if txtTitle.text?.count ?? 0 == 0 {
            
            txtTitle.showError(message: "Required".localized())
            
            return
        }
        
        if txtViewDes.text?.count ?? 0 == 0 {
            
            txtViewDes.showError(message: "Required".localized())
            
            return
            
        }
        
        if txtScheduleText.text?.count ?? 0 == 0 {
            
            txtScheduleText.showError(message: "Required".localized())
            
            return
            
        }
        
        submitGiftDetails()
        
    }
    
    func getDateString(stringDate:Date) -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        //dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: stringDate)
    }
    
    func getTimeString(stringDate:Date) -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
       // dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: stringDate)
    }
    
    func getDateTime(stringDateTime:String) -> Date! {
        
        var dateStartText = ""
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
                  
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterPrint.timeZone = .current
        
        if let date = dateFormatterGet.date(from: stringDateTime) {
                      
            dateStartText = dateFormatterPrint.string(from: date)
                      
        }

           return getDateTimeWithFormat(stringDate:dateStartText)
       }
    
    func getDateTimeWithFormat(stringDate:String) -> Date! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: stringDate)
    }
    
    func fetchGiftDetail() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getSellerGiftDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&giftCode=\(giftCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(GiftDetailApiModel.self, from: data)
                                
                                if let apiResponse:GiftModel = jsonResponse.sellerGiftDetails {
                                    
                                    weakSelf?.sellerGiftDetails = apiResponse
                                    
                                }
                                
                                
                                
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func submitGiftDetails(){
        
        var arraydate : [String : String] = [:]
        arraydate["startDate"] = getDateString(stringDate: txtStartDate.selectedDate)
        arraydate["endDate"] = getDateString(stringDate: txtEndDate.selectedDate)
        dateArray.append(arraydate)
        
        var arrayTime: [String : String] = [:]
        arrayTime["startTime"] = getTimeString(stringDate: txtStartDate.selectedDate)
        arrayTime["endTime"] = getTimeString(stringDate: txtEndDate.selectedDate)
        
        timeArray.append(arrayTime)
        
        let status:Int  = statusList?[selectedIndex].status ?? 0
        
        startAnimating()
        let params:NSDictionary = ["giftCode":giftCode,
                                   "sellerCode": sellerCode,
                                   "title":txtTitle.text ?? "",
                                   "description":txtViewDes.text ?? "",
                                   "imagePath":strLogo ?? "",
                                   "imageFileName":strLogoName ?? "",
                                   "status": status,
                                   "scheduleText":txtScheduleText.text ?? "",
                                   "displayFor":selectedDisplayType,
                                   "giftSchedule":dateArray,
                                   "giftTimings":timeArray
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerGift?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                            weakSelf?.delegate?.didAddedGift()
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                weakSelf?.showErrorMessage(message: message)
                weakSelf?.dismiss(animated: true, completion: nil)
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    
    
}
extension NewGiftVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            return CGSize.init(width: collectionViewStatus.frame.width / 3, height: 40.0)
            
        }
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if statusList?.count ?? 0 > 0 {
            
            return statusList?.count ?? 0
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
            
            return getCellForStatus(cell: cell, indexPath: indexPath)
            
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        selectedIndex = indexPath.row
        
        collectionViewStatus.reloadData()
        
    }
    
    
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:SellerStatusModel = statusList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
            cell.lblTitle.text = model.statusTitle ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
}

extension NewGiftVC:RCTextFieldDelegate{
    
    func didChangeDate(textField: RCTextField) {
        
        if textField.tag == 11 {
            
            let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: txtStartDate.selectedDate)
            
            txtEndDate.selectedDate = nextMonth!
            
            txtEndDate.dateTimePicker.minimumDate = txtStartDate.selectedDate
        }
        
    }
    
}
