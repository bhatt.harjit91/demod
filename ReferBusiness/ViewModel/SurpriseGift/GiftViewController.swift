//
//  SurpriseGiftViewController.swift
//  iCanRefer
//
//  Created by Vivek Mishra on 09/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol GiftViewControllerDelegate: class {
    func didCalledGift()
    
}

class GiftViewController: AppBaseViewController {
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblCount: NKLabel!
    
    @IBOutlet weak var lblNoData: NKLabel!
    
    var sellerGiftlList:[GiftModel]?
    
    var sellerCode:Int = 0
    
    var statusList:[SellerStatusModel]?
    
    var colorCodes:ColorModel?
    
    var userCount:Int?
    
    weak var delegate: GiftViewControllerDelegate?
    
    var isAdmin:Int?
      
    var isPartnerForCompany:Int?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
         lblNoData.text = "No Data Found".localized()
        
       // navigationItem.title = "Gifts".localized()
        
        updateNavBar()
        
        fetchGiftList()
        

    }
    
    func updateNavBar() {
        let btnName = UIButton()
        
        btnName.setTitle("New -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(newGiftAction(sender:)), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        btnName.alpha = 0
                      
        if isAdmin == 1 || isPartnerForCompany == 1 {
                          
          btnName.alpha = 1
            
         }
    }
    
    @objc func newGiftAction(sender: UIButton){
        
       openGifts()
        
    }
    
    func openGifts(){
        let storyBoard = UIStoryboard.init(name: GiftSB, bundle: nil)
               if let newGiftPage:NewGiftVC = storyBoard.instantiateViewController(withIdentifier: "NewGiftVC") as? NewGiftVC {
                   
                   newGiftPage.sellerCode = sellerCode
                   newGiftPage.statusList = statusList ?? []
                   newGiftPage.isDetail = false
                   newGiftPage.delegate = self
                   
                   navigationController?.pushViewController(newGiftPage, animated: true)
               }
    }
    
    func fetchGiftList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getSellerGiftList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(GiftApiModel.self, from: data)
                                
                                if let apiResponse:[GiftModel] = jsonResponse.sellerGiftlList {
                                    
                                    weakSelf?.sellerGiftlList = apiResponse
                                    
                                }
                                if let apiResponse:[SellerStatusModel] = jsonResponse.statusList {
                                    
                                    weakSelf?.statusList = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.userCount = apiResponse
                                    
                                    self.navigationItem.title = "Gifts".localized()
                                    
                                    if weakSelf?.userCount ?? 0 > 0 {
                                        
                                        self.navigationItem.title = "\("Gifts (".localized())\(weakSelf?.userCount ?? 0)\(")")".localized()
                                    }
                                    
//                                    weakSelf?.lblCount.text = ("\("This seller has ".localized())\(weakSelf?.userCount ?? 0)\(" gifts".localized())")
                                }
                                
                                if weakSelf?.sellerGiftlList?.count ?? 0 == 0 {
                                    
                                   if weakSelf?.isAdmin == 1 || weakSelf?.isPartnerForCompany == 1 {
                                    
                                        weakSelf?.openGifts()
                                    
                                    }
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
}

extension GiftViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let count = sellerGiftlList?.count ?? 0
               
               if count > 0 {
                   
                   tableViewList.alpha = 1
                   
                   return sellerGiftlList?.count ?? 0
                   
               }
               else{
                   
                   tableViewList.alpha = 0
               }
               
               return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:GiftViewCell = tableView.dequeueReusableCell(withIdentifier: "GiftViewCell", for: indexPath) as? GiftViewCell
        {
            
            return getCellForGiftList(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:GiftModel = sellerGiftlList?[indexPath.row] {
            
            let storyBoard = UIStoryboard.init(name: GiftSB, bundle: nil)
            if let newGiftPage:NewGiftVC = storyBoard.instantiateViewController(withIdentifier: "NewGiftVC") as? NewGiftVC {
                
                newGiftPage.sellerCode = model.sellerCode ?? 0
                newGiftPage.giftCode = model.giftCode ?? 0
                newGiftPage.statusList = statusList ?? []
                newGiftPage.isDetail = true
                newGiftPage.delegate = self
                newGiftPage.isAdmin = isAdmin
                newGiftPage.isPartnerForCompany = isPartnerForCompany
                navigationController?.pushViewController(newGiftPage, animated: true)
                
            }
        }
    }
    
    func getCellForGiftList(cell:GiftViewCell,indexPath:IndexPath) -> GiftViewCell {
        
        cell.selectionStyle = .none
        
        if let model:GiftModel = sellerGiftlList?[indexPath.row]
        {
            cell.lblStatus.text = model.statusTitle ?? ""
            
            if model.imagePath?.count ?? 0 > 0 {
                
                cell.imgViewGift.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.imagePath ?? "")")), completed: nil)
                
            }
            
            var startDateTime = ""
            
            var endDateTime = ""
            
            if let dateDict = model.giftSchedule?.first
            {
                if let startDate = dateDict["startDate"]
                {
                    startDateTime = startDate
                    
                }
                if let endDate = dateDict["endDate"]
                {
                    endDateTime = endDate
                    
                }
                
            }
            
            if let timeDict = model.giftTimings?.first
            {
                if let startTime = timeDict["startTime"]
                {
                    startDateTime = "\(startDateTime)\(" ")\(startTime)"
                    
                }
                if let endTime = timeDict["endTime"]
                {
                    endDateTime = "\(endDateTime)\(" ")\(endTime)"
                    
                }
                
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = .current
            
            var dateStartText = ""
            
            var dateEndText = ""
            
            
            
            if let date = dateFormatterGet.date(from: startDateTime) {
                
                dateStartText = dateFormatterPrint.string(from: date)
                
            }
            if let date = dateFormatterGet.date(from: endDateTime) {
                
                dateEndText = dateFormatterPrint.string(from: date)
                
            }
            
            cell.lblDes.text = "\(dateStartText)\(" to ")\(dateEndText)"
            
            
            
            if model.status == 1 {
                cell.viewStatus.backgroundColor =   UIColor.colorWithHexString(hexString: colorCodes?.activeColor ?? "")
            }
                
            else if model.status == 2 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.inactiveColor ?? "")
            }
                
            else if model.status == 3 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.blacklistedColor ?? "")
            }
                
            else {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString:"#42A5F5")
            }
            
        }
        
        
        return cell
    }
    
}

extension GiftViewController:NewGiftVCCDelegate{
    
    func didAddedGift() {
        
         fetchGiftList()
        
        delegate?.didCalledGift()
        
    }
    
}

class GiftViewCell:UITableViewCell{
    
    @IBOutlet weak var viewStatus: RCView!
    
    @IBOutlet weak var lblStatus: NKLabel!
    
    @IBOutlet weak var viewImgBackGround: RCView!
    
    @IBOutlet weak var imgViewGift: UIImageView!
    
    @IBOutlet weak var lblDes: NKLabel!
}
