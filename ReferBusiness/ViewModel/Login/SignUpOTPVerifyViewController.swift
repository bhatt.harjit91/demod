//
//  SignUpOTPVerifyViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation

protocol SignUpOTPVerifyViewControllerDelegate {
    func verifiedOTP(isd:String,mobileNo:String,OTP:String)
}

class SignUpOTPVerifyViewController: SendOTPViewController,OTPViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var txtOtp: OTPView!
    
    @IBOutlet weak var lblOTpText: UILabel!
    @IBOutlet weak var btnResentOTP: NKButton!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var lblOtpSentTextTitle: NKLabel!
    
    var isdCode:String = ""
    var mobileNo:String = ""
    var firstName:String = ""
    var lastName:String = ""
    var password:String = ""
    var emailId:String = ""
    var urlProfilePic:String = ""
    var navigationTitle:String = ""
    var isForgotPassword:Bool = false
    var isUpdatePhoneNumber:Bool = false
    var isBecomePartner:Bool = false
    
    @IBOutlet weak var lblMobileNumber: NKLabel!
    
    var delegate:SignUpOTPVerifyViewControllerDelegate?
    
    var params:[String:Any] = [:]
    
    var currentLoc:CLLocationCoordinate2D?
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txtOtp.becomeFirstResponder()
        txtOtp.delegate = self
        
        lblMobileNumber.text = ("\(isdCode) \(mobileNo)")
        
        navigationItem.title = navigationTitle
        updateString()
        
            btnResentOTP.isEnabled = false
            btnResentOTP.colorIndex = 9
        
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 200.0
        locationManager.requestLocation()
        
            Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(myTimerTick), userInfo: nil, repeats: false)
        
        lblOtpSentTextTitle.text = "One Time Password(OTP) was sent to your mobile phone and email. In case you do not recieve mobile OTP, please check your email inbox or spam folder".localized()
        
        lblOtpSentTextTitle.textColor = .primaryColor
        
        navigationItem.title = "Verify Mobile Number".localized()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func myTimerTick() {
        btnResentOTP.isEnabled = true
        btnResentOTP.colorIndex = 0
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        locationManager.stopUpdatingLocation()
        currentLoc = locValue
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        
    }
    
    func openHomePage() {
        
        let defaults = UserDefaults.standard
        
        if  defaults.bool(forKey: "isShowBanner")
        {
            let HomeStoryBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
            let homePage:UINavigationController = HomeStoryBoard.instantiateInitialViewController() as! UINavigationController
            homePage.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
            view.window?.rootViewController = homePage
        }
        else
        {
            
            if let arrayBanner:[NSDictionary] = UserDefaults.standard.value(forKey: "bannerList") as? [NSDictionary] {
                
                if arrayBanner.count > 0 {
                    
                    showBanners(attachmentArray:arrayBanner)
                    
                }
                else{
                    let HomeStoryBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
                    let homePage:UINavigationController = HomeStoryBoard.instantiateInitialViewController() as! UINavigationController
                    homePage.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
                    
                    view.window?.rootViewController = homePage
                }
                
            }
            
            
        }
        
    }
    
    func updateString() {
        lblOTpText.text = "Verification code has been sent to above mobile number, please enter it to verify.".localized()
        btnResentOTP.setTitle("Resend OTP".localized(), for: .normal)
    }
    
    func didChangeOTP(otpText: String) {
        if otpText.count == txtOtp.digits {
            //Call verification
            verifyOTP()
        }
    }
    
    func callSignUp() {
        
        spinner.startAnimating()
        var secretKey:String = ""
        if generateSecretKey() {
            if let key = getSecretKey(){
                secretKey = key
            }
        }
        
        var locationLat:Double = 0.0
        var locationLon:Double = 0.0
        var isReferralMember:Int = 0
        
        if let locationDictionary = UserDefaults.standard.object(forKey: "locationFromSignUp") as? Dictionary<String,NSNumber> {
            locationLat = locationDictionary["lat"]!.doubleValue
            locationLon = locationDictionary["lon"]!.doubleValue
        }

        isReferralMember = UserDefaults.standard.integer(forKey: "referredUserId")
        
        
        params["secretKey"] = secretKey
        params["firstName"] = firstName
        params["displayName"] = firstName
        params["lastName"] = lastName
        params["mobileIsd"] = isdCode
        params["mobileNumber"] = mobileNo
        params["password"] = password
        params["emailId"] = emailId
        params["latitude"] = locationLat
        params["longitude"] = locationLon
        params["otp"] = txtOtp.otpText
        params["profilePicture"] = urlProfilePic
        params["referredByUserId"] = isReferralMember
        params["isBuyer"] = UserDefaults.standard.integer(forKey: "isBuyer")
        params["address"] = UserDefaults.standard.object(forKey: "addressSignUp")
        params["existingUser"] = UserDefaults.standard.object(forKey: "existingUser")
        print(params)
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/signUp?lngId=\(Localize.currentLanguageID)"), params: params as NSDictionary, completionHandler:{response, jsonObj in
            //let statusCode = response?.statusCode
            
            
            var message:String = failedToConnectMessage.localized()
            if (jsonObj?.status() ?? false) {
                
                //Save userdata
                if let dataObj:NSDictionary = jsonObj?.dataObj() {

                    if let token:String = dataObj.object(forKey: "token") as? String {
                        let status =  weakSelf?.saveToken(userName:  weakSelf?.mobileNo ?? "", token: token)
                        
                        if status ?? false {
                            
                            let configVersionNo:Int =  dataObj.object(forKey: "configVersionNo") as? Int ?? 0
                            let alertNo:Int =  dataObj.object(forKey: "alertNo") as? Int ?? 0
                            
                            UserDefaults.standard.removeObject(forKey: "locationFromSignUp")
                            UserDefaults.standard.removeObject(forKey: "addressSignUp")
                            UserDefaults.standard.removeObject(forKey: "isBuyer")
                            UserDefaults.standard.removeObject(forKey: "referredUserId")

                            
                            print("configVersionNoconfigVersionNoconfigVersionNoconfigVersionNo")
                            print(configVersionNo)
                            UserDefaults.standard.set(configVersionNo, forKey: "configVersionNo")
                            UserDefaults.standard.set(alertNo, forKey: "alertNo")
                            
                            self.getUserDetailsNew(token: token, alertNo: alertNo, configVersionNo: configVersionNo)
                        }
                    }
                    ///
//                    if let userDetails:NSDictionary = dataObj.object(forKey: "userDetails") as? NSDictionary{
//                        if let token:String = userDetails.object(forKey: "token") as? String {
//                            let status =  weakSelf?.saveToken(userName:  weakSelf?.mobileNo ?? "", token: token)
//                            if status ?? false {
//                                //Sign up success
//
//                                UserDefaults.standard.removeObject(forKey: "locationFromSignUp")
//                                UserDefaults.standard.removeObject(forKey: "addressSignUp")
//                                UserDefaults.standard.removeObject(forKey: "isBuyer")
//                                UserDefaults.standard.removeObject(forKey: "referredUserId")
//
//                                if let mutableData:NSMutableDictionary = userDetails.mutableCopy() as? NSMutableDictionary{
//                                    mutableData.removeObject(forKey: "token")
//
//                                    if  weakSelf?.saveUserDetails(userDetail: mutableData.copy() as! NSDictionary) ?? false{
//                                        weakSelf?.navigationController?.dismiss(animated: true, completion: nil)
//
//                                        weakSelf?.openHomePage()
//
//                                        return
//                                    }
//                                }
//                            }
//                        }
//                    }
                }
            }
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.showCustomMessage(message: message)
            
             weakSelf?.txtOtp.isEnabled = true
            
            weakSelf?.spinner.stopAnimating()
        })
    }
        
    
    func getUserDetailsNew(token:String,alertNo:Int,configVersionNo:Int) {

        UserDefaults.standard.set(1, forKey: "firstTime")
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getUserDetailsNew?token=\(token)&lngId=\(Localize.currentLanguageID)&configVersionNo=\(configVersionNo)&alertNo=\(alertNo)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                print(dataObj)
                                
                                let isFetch:Int =  dataObj.object(forKey: "isFetch") as? Int ?? 0
                                
                                let configVersionNo:Int =  dataObj.object(forKey: "configVersionNo") as? Int ?? 0
                                let alertNo:Int =  dataObj.object(forKey: "alertNo") as? Int ?? 0
                                
                                print("configVersionNoconfigVersionNoconfigVersionNoconfigVersionNo")
                                print(configVersionNo)
                                print(dataObj.object(forKey: "config") ?? [:])
                                UserDefaults.standard.set(configVersionNo, forKey: "configVersionNo")
                                UserDefaults.standard.set(alertNo, forKey: "alertNo")
                                
                                if isFetch == 1
                                {
                                    UserDefaults.standard.set(dataObj.object(forKey: "config"), forKey: "config")
                                    
                                    UserDefaults.standard.set(dataObj.object(forKey: "bannerList"), forKey: "bannerList")

                                    if let userDetails:NSDictionary = dataObj.object(forKey: "userDetails") as? NSDictionary{
                                        
                                        if let mutableData:NSMutableDictionary = userDetails.mutableCopy() as? NSMutableDictionary{
                                            
                                            mutableData.removeObject(forKey: "token")
                                            
                                            if  weakSelf?.saveUserDetails(userDetail: mutableData.copy() as! NSDictionary) ?? false{
                                                weakSelf?.navigationController?.dismiss(animated: true, completion: nil)
                                                
                                                weakSelf?.openHomePage()
                                                
                                                return
                                            }
                                        }
                                    }
                                }
                            
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
    func verifyOTP() {
        
        //         performSegue(withIdentifier: "SignUpSegue", sender: nil)
        //        return
        txtOtp.isEnabled = false
        btnResentOTP.isHidden = true
        spinner.startAnimating()
        let params:NSDictionary = [
            "mobileIsd":isdCode,
            "mobileNumber":mobileNo,
            "otp":txtOtp.otpText
        ]
        let apiClient:APIClient = APIClient()
        
        var urlString = String("\(ApiBaseUrl)icr/verifyOtp?lngId=\(Localize.currentLanguageID)")
        if  isUpdatePhoneNumber {
            urlString = String("\(ApiBaseUrl)icr/verifyOtp?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())")
        }
        weak var weakSelf = self
        apiClient.PostRequest(urlString: urlString, params: params as NSDictionary, completionHandler:{response, jsonObj in
            //let statusCode = response?.statusCode
            var message:String = failedToConnectMessage.localized()
            if (jsonObj?.status() ?? false) {
                if  weakSelf?.isForgotPassword ?? false{
                    weakSelf?.performSegue(withIdentifier: "SetNewPasswordSegue", sender: nil)
                }
                else if weakSelf?.isUpdatePhoneNumber ?? false {
                    if let delegate =  weakSelf?.delegate {
                        delegate.verifiedOTP(isd:  weakSelf?.isdCode ?? "", mobileNo:  weakSelf?.mobileNo ?? "", OTP:  weakSelf?.txtOtp.otpText ?? "")
                    }
                    weakSelf?.navigationController?.popViewController(animated: true)
                }
                    
                    else if weakSelf?.isBecomePartner ?? false {
                        if let delegate =  weakSelf?.delegate {
                            delegate.verifiedOTP(isd:  weakSelf?.isdCode ?? "", mobileNo:  weakSelf?.mobileNo ?? "", OTP:  weakSelf?.txtOtp.otpText ?? "")
                        }
                        weakSelf?.navigationController?.popViewController(animated: true)
                    }
                    
                else{
                    weakSelf?.callSignUp()
                }
                
            }
            else{
                if let serverMessage = jsonObj?.message(){
                    message = (serverMessage.count>0) ? serverMessage : message
                }
                
                weakSelf?.showOTPErrorMessage(message: message)
            }
            
            weakSelf?.txtOtp.otpText = ""
             weakSelf?.txtOtp.isEnabled = true
            weakSelf?.btnResentOTP.isHidden = false
            weakSelf?.spinner.stopAnimating()
        })
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SignUpSegue" {
            let dvc:SignUpInfoTableViewController = segue.destination as! SignUpInfoTableViewController
            dvc.mobileNo = mobileNo
            dvc.isdCode = isdCode
            dvc.otp = txtOtp.otpText
        }
        else if segue.identifier == "SetNewPasswordSegue" {
            let dvc:SignUpPasswordTableViewController = segue.destination as! SignUpPasswordTableViewController
            dvc.mobileNo = mobileNo
            dvc.isdCode = isdCode
            dvc.otp = txtOtp.otpText
            dvc.isForgotPassword = isForgotPassword
        }
        
        
    }
    
    
    
    @IBAction func resentOtp(_ sender: Any) {
        btnResentOTP.isHidden = true
        spinner.startAnimating()
        
        weak var weakSelf = self
        if isForgotPassword {
            sendForgotPasswordOTP(isdCode: isdCode, mobileNo: mobileNo, completionHandler: { status, message in
                
                weakSelf?.btnResentOTP.isHidden = false
                weakSelf?.spinner.stopAnimating()
                weakSelf?.btnResentOTP.isEnabled = false
                weakSelf?.btnResentOTP.colorIndex = 9
                DispatchQueue.main.asyncAfter(deadline: .now() + 15.0 ) {
                    weakSelf?.btnResentOTP.isEnabled = true
                    weakSelf?.btnResentOTP.colorIndex = 0
                }
            })
        }
        else if isUpdatePhoneNumber {
            sendUpdateProfileOTP(isdCode: isdCode, mobileNo: mobileNo, completionHandler: { status, message in
                
                weakSelf?.btnResentOTP.isHidden = false
                weakSelf?.spinner.stopAnimating()
                weakSelf?.btnResentOTP.isEnabled = false
                 weakSelf?.btnResentOTP.colorIndex = 9
                DispatchQueue.main.asyncAfter(deadline: .now() + 8.0 ) {
                    weakSelf?.btnResentOTP.isEnabled = true
                    weakSelf?.btnResentOTP.colorIndex = 0
                }
            })
        }
        else{
            sendOTP(isdCode: isdCode, mobileNo: mobileNo, completionHandler: { status, message in
                
                weakSelf?.btnResentOTP.isHidden = false
                weakSelf?.spinner.stopAnimating()
                weakSelf?.btnResentOTP.isEnabled = false
                weakSelf?.btnResentOTP.colorIndex = 9
                DispatchQueue.main.asyncAfter(deadline: .now() + 8.0 ) {
                    weakSelf?.btnResentOTP.isEnabled = true
                    weakSelf?.btnResentOTP.colorIndex = 0
                }
            })
        }
        
    }
    
}

