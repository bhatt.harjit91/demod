//
//  SignUpProfessionInfoViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation

class SignUpProfessionInfoViewController: UITableViewController,LocationPermissionVCNewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var txtJobTitle: RCTextField!
    @IBOutlet weak var txtOrganization: RCTextField!
    @IBOutlet weak var txtCity: RCTextField!
    @IBOutlet weak var txtAbout: RCTextField!
    @IBOutlet weak var btnFinish: NKButton!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var currentLoc:CLLocationCoordinate2D?
    var locationManager = CLLocationManager()
    var mobileNo:String?
    var params:[String:Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 200.0
        locationManager.requestLocation()
        
        //locationManager.startUpdatingLocation()
    }
    @IBAction func changeLocation(_ sender: Any) {
        //        let locationSB = UIStoryboard.init(name: LocationPermissionSB, bundle: nil)
        //        if let locationPage:LocationPermissionVC = locationSB.instantiateInitialViewController() as? LocationPermissionVC{
        //            locationPage.isCancelable = true
        //            locationPage.delegate = self
        //            locationPage.currentLocation = currentLoc
        //            navigationController?.present(locationPage, animated: true, completion: nil)
        //        }
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        locationPage.modalPresentationStyle = .fullScreen
        navigationController?.present(locationPage, animated: true)
    }
    
    func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        currentLoc = coordinate
        updateLocationName()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        locationManager.stopUpdatingLocation()
        currentLoc = locValue
        updateLocationName()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        
    }
    
    func updateLocationName() {
        let geocoder = CLGeocoder()
        let location:CLLocation = CLLocation.init(latitude: currentLoc?.latitude ?? 0.0, longitude: currentLoc?.longitude ?? 0.0)
        // Look up the location and pass it to the completion handler
        weak var weakSelf = self
        geocoder.reverseGeocodeLocation(location,
                                        completionHandler: { (placemarks, error) in
                                            if error == nil {
                                                if let firstLocation:CLPlacemark = placemarks?[0]{
                                                    ////print("Location: \(firstLocation.name ?? "")")
                                                    let cityCountry:String = ("\(firstLocation.subAdministrativeArea ?? "") \(firstLocation.country ?? "")")
                                                    weakSelf?.txtCity.text =  cityCountry
                                                }
                                            }
                                            else {
                                                // An error occurred during geocoding.
                                                
                                            }
        })
    }
    
    @IBAction func finishSignUp(_ sender: Any) {
        btnFinish.isHidden = true
        spinner.startAnimating()
        var secretKey:String = ""
        if generateSecretKey() {
            if let key = getSecretKey(){
                secretKey = key
            }
        }
        params["secretKey"] = secretKey
        params["jobTitle"] = txtJobTitle.text ?? ""
        params["organization"] = txtOrganization.text ?? ""
        params["city"] = txtCity.text ?? ""
        params["about"] = txtAbout.text ?? ""
        params["latitude"] = currentLoc?.latitude ?? 0.0
        params["longitude"] = currentLoc?.longitude ?? 0.0
      

        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/signUp?lngId=\(Localize.currentLanguageID)"), params: params as NSDictionary, completionHandler:{response, jsonObj in
            //let statusCode = response?.statusCode
            
            
            var message:String = failedToConnectMessage.localized()
            if (jsonObj?.status() ?? false) {
                
                //Save userdata
                if let dataObj:NSDictionary = jsonObj?.dataObj() {
                    if let userDetails:NSDictionary = dataObj.object(forKey: "userDetails") as? NSDictionary{
                        if let token:String = userDetails.object(forKey: "token") as? String {
                            let status =  weakSelf?.saveToken(userName:  weakSelf?.mobileNo ?? "", token: token)
                            if status ?? false {
                                //Sign up success
                                
                               
                                if let mutableData:NSMutableDictionary = userDetails.mutableCopy() as? NSMutableDictionary{
                                    mutableData.removeObject(forKey: "token")
                                    
                                    if  weakSelf?.saveUserDetails(userDetail: mutableData.copy() as! NSDictionary) ?? false{
                                        weakSelf?.navigationController?.dismiss(animated: true, completion: nil)
                                        return
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.showErrorMessage(message: message)
            weakSelf?.btnFinish.isHidden = false
            weakSelf?.spinner.stopAnimating()
        })
    }
    
}

