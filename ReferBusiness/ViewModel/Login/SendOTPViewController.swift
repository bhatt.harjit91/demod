//
//  SendOTPViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class SendOTPViewController: ImageUploaderViewController {
    
    typealias CompletionHandler = (Bool, String) -> Void
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func sendOTP(isdCode:String, mobileNo:String,completionHandler: @escaping CompletionHandler) {
        let params:NSDictionary = [
            "mobileIsd":isdCode,
            "mobileNumber":mobileNo
        ]
        let apiClient:APIClient = APIClient()
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/sendOtp?lngId=\(Localize.currentLanguageID)"), params: params as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                completionHandler(true,message)
            }
            else{
                
                completionHandler(false,message)
                
            }
            
        })
    }
    
    func sendForgotPasswordOTP(isdCode:String, mobileNo:String,completionHandler: @escaping CompletionHandler) {
        let params:NSDictionary = [
            "mobileIsd":isdCode,
            "mobileNumber":mobileNo
        ]
        let apiClient:APIClient = APIClient()
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/pwdResetOtp?lngId=\(Localize.currentLanguageID)&"), params: params as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                completionHandler(true,message)
            }
            else{
                
                completionHandler(false,message)
                
            }
            
        })
    }
    
    func sendUpdateProfileOTP(isdCode:String, mobileNo:String,completionHandler: @escaping CompletionHandler) {
        let params:NSDictionary = [
            "mobileIsd":isdCode,
            "mobileNumber":mobileNo
        ]
        let apiClient:APIClient = APIClient()
        let enctyptedParam = ["data": encryptParams(params: params)]
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/updateProfileSendOtp?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: enctyptedParam as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                completionHandler(true,message)
            }
            else{
                
                completionHandler(false,message)
                
            }
            
        })
    }
    
}

