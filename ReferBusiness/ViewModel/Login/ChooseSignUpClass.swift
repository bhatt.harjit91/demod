//
//  ChooseSignUpClass.swift
//  iCanRefer
//
//  Created by Vivek Mishra on 28/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class ChooseSignUpClass: Codable {
    
    var partnerFeatures:[BuyerFeatures]?
    var buyerFeatures:[BuyerFeatures]?
}


class BuyerFeatures: Codable {
    
    var textCode:Int?
    var title:String?
    var description:String?

    private enum CodingKeys:String,CodingKey{
        
        case textCode = "textCode"
        case title = "title"
        case description = "description"
    }
    
}

