//
//  ConfirmViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ConfirmViewController: AppBaseViewController {
    
    @IBOutlet weak var lblTitleText: NKLabel!
    
    @IBOutlet weak var txtCountry: RCTextField!
    
    @IBOutlet weak var txtMobileNo: RCTextField!
    
    @IBOutlet weak var btnNext: NKButton!
    var mobileNo:String?
    var countryCode:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtCountry.text = countryCode ?? ""
        lblTitleText.text = "Please confirm your country code and enter your mobile number".localized()
        lblTitleText.textAllignType = 2
        
        txtCountry.isUserInteractionEnabled = false
        
         navigationController?.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ConfirmNoSegue" {
            let dvc:SignUpInfoTableViewController = segue.destination as! SignUpInfoTableViewController
            dvc.mobileNo = txtMobileNo.text!
            dvc.isdCode = txtCountry.text!
            //dvc.otp = txtOtp.otpText
        }
    }
    @IBAction func btnNext(_ sender: Any) {
        if mobileNo == txtMobileNo.text  && countryCode == txtCountry.text {
            performSegue(withIdentifier: "ConfirmNoSegue", sender: nil)
        }
        else{
            showErrorMessage(message: "Mobile number doesn't match. Please try again".localized())
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

