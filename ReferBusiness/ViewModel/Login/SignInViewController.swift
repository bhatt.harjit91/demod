//
//  SignInViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation
import GoogleMaps



class SignInViewController: AppBaseViewController,CountryCodeSelectViewControllerDelegate,UITextFieldDelegate,CLLocationManagerDelegate {
    @IBOutlet weak var btnShowHidePwd: UIButton!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignIn: NKButton!
    
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtMobileNo: RCTextField!
    @IBOutlet weak var txtPassword: RCTextField!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var lblNewUser: UILabel!
    
    @IBOutlet weak var btnTermsOfUse: UIButton!
    
    @IBOutlet weak var stackViewSocialMedia: UIStackView!
    
    @IBOutlet weak var viewSocialMedia: UIView!
    
    @IBOutlet weak var heightViewSocialMedia: NSLayoutConstraint!
    
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    
    @IBOutlet weak var lblORTitle: UILabel!
    
    var selectedCountryCode : CountryCodeModel?
    
    var socialMediaList:[SocialMediaModel] = []
    
    var iconClick = true

    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 4),
        .foregroundColor: UIColor.btnLinkColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
  let attributesWithItalicFont: [NSAttributedString.Key: Any] = [
           .font: UIFont.fontForType(textType: 8),
           .foregroundColor: UIColor.btnLinkColor,
           .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        UserDefaults.standard.set(1, forKey: "firstTime")
        
        lblORTitle.text = " OR ".localized()
        
        // Do any additional setup after loading the view.
        // updateUI()
        // Do any additional setup after loading the view.
        if let currentCountry =  getLocalCountryCode(){
            selectedCountryCode = currentCountry
            // txtCountryCode.setTitle(currentCountry.dial_code, for: .normal)
            txtCountryCode.text = currentCountry.dial_code
        }
        updateStrings()
        txtMobileNo.delegate = self
        txtPassword.delegate = self
        
        let attributeString = NSMutableAttributedString(string: "Terms of Use".localized(),
                                                        attributes: yourAttributes)
        
        let attributePrivacyString = NSMutableAttributedString(string: "Privacy Policy".localized(),
                                                              attributes: yourAttributes)
        
        btnTermsOfUse.setAttributedTitle(attributeString, for: .normal)
        
        btnPrivacyPolicy.setAttributedTitle(attributePrivacyString, for: .normal)
        
        heightViewSocialMedia.constant = 210
        viewSocialMedia.alpha = 1
        
//        if socialMediaList.count > 0 {
//            heightViewSocialMedia.constant = 190
//             viewSocialMedia.alpha = 1
//        }
        
        btnSignUp.setTitleColor(UIColor.btnLinkColor, for: .normal)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       
    }
    
    
    func updateStrings() {
        navigationItem.title = "Sign In".localized()
        btnSignIn.setTitle("Sign In".localized(), for: UIControl.State.normal)
        btnSignUp.setTitle("Sign Up".localized(), for: UIControl.State.normal)
        
    let attributeString = NSMutableAttributedString(string:"Forgot Password?".localized(),
                                                               attributes: yourAttributes)
        
        btnForgotPassword.setAttributedTitle(attributeString, for: .normal)
        
        lblNewUser.text = "New to iCanRefer?".localized()
        txtMobileNo.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Mobile Number".localized())
        txtPassword.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Password".localized())
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField ==  txtCountryCode {
            openCountrySelector()
            return false;
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let txtField:RCTextField = textField as? RCTextField{
            txtField.showError(message: "")
        }
        
        if textField ==  txtMobileNo {
            
            let maxLength = 14
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        return true
    }
    
    func updateUI() {
        
        btnSignIn.backgroundColor = UIColor.primaryColor
        btnSignIn.setTitleColor(UIColor.pTextColor, for: .normal)
        
        btnSignUp.backgroundColor = UIColor.secondaryColor
        btnSignUp.setTitleColor(UIColor.btnLinkColor, for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    @IBAction func fogotPasswordClicked(_ sender: Any) {
        let forgotVC:SignUpViewController = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
         forgotVC.isForgotPassword = true
         forgotVC.countryCode = selectedCountryCode
         forgotVC.strMobileNumber = txtMobileNo.text ?? ""
         navigationController?.pushViewController(forgotVC, animated: true)
    }
    @IBAction func cancelSignIn(_ sender: Any) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func facebookButtonPressed(_ sender: Any) {
       
    }
    
    @IBAction func linkedinButtonPressed(_ sender: Any) {
     
        
    }
    
    @IBAction func googleButtonClicked(_ sender: Any) {
       
    }
    
    @IBAction func instagramButtonClicked(_ sender: Any) {
        loginWithInstagram()
    }
    
    @IBAction func twitterButtonClicked(_ sender: Any) {
       
    }
    
    
    @IBAction func btnTermsOfUse(_ sender: Any) {
        
        openWebView(urlString: "\("terms_")\(Localize.currentLanguage)\(".html")", title: "Terms of Use".localized(), subTitle: "")
    }
    
    @IBAction func btnPrivacyPolicy(_ sender: Any) {
        
         openWebView(urlString: "\("privacy_")\(Localize.currentLanguage)\(".html")", title: "Privacy Policy".localized(), subTitle: "")
        
    }
    
    //MARK:Instagram Login
    
    func loginWithInstagram() {
       
    }
    
    
    
    func openCountrySelector() {
        let storyBoard = UIStoryboard.init(name: CountryCodeSB, bundle: nil)
        let countrySelector:CountryCodeSelectViewController = storyBoard.instantiateViewController(withIdentifier: "CountryCodeSelectViewController") as! CountryCodeSelectViewController
        countrySelector.selectedCountryCode = selectedCountryCode
        countrySelector.delegate = self
       navigationController?.pushViewController(countrySelector, animated: true)
    }
    
    func openProfile() {
        let storyBoard = UIStoryboard.init(name: ProfilePageSB, bundle: nil)
        let profile:ProfileNewViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileNewViewController") as! ProfileNewViewController
        navigationController?.pushViewController(profile, animated: true)
    }
    
    func openHome() {
            
            let defaults = UserDefaults.standard
            
            if  defaults.bool(forKey: "isShowBanner")
            {
                let HomeStoryBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
                let homePage:UINavigationController = HomeStoryBoard.instantiateInitialViewController() as! UINavigationController
                homePage.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
                view.window?.rootViewController = homePage
            }
            else
            {
                
                if let arrayBanner:[NSDictionary] = UserDefaults.standard.value(forKey: "bannerList") as? [NSDictionary] {
                    
                    if arrayBanner.count > 0 {
                        
                        showBanners(attachmentArray:arrayBanner)
                        
                    }
                    
                    else{
                        let HomeStoryBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
                        let homePage:UINavigationController = HomeStoryBoard.instantiateInitialViewController() as! UINavigationController
                        homePage.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
                        
                        view.window?.rootViewController = homePage
                    }
                    
                }
                
                
            }
            
        }
    
    func didFinishSelectingCountryCode(selectedCountry: CountryCodeModel) {
        selectedCountryCode = selectedCountry
        txtCountryCode.text = selectedCountry.dial_code
    }
    
    @IBAction func signUpClicked(_ sender: Any) {
        
    }
    
    @IBAction func signInClicked(_ sender: Any) {
        if txtMobileNo.text?.count == 0 {
            txtMobileNo.showError(message: "Required".localized())
            return
        }
        if txtMobileNo.text?.count ?? 0 < 5 {
            
            txtMobileNo.showError(message: "Should be at least 5 characters".localized())
            
            return
        }
        
        if txtPassword.text?.count == 0 {
            txtPassword.showError(message: "Required".localized())
            return
        }
        var secretKey:String = ""
        if generateSecretKey() {
            if let key = getSecretKey(){
                secretKey = key
            }
        }
        let params:NSDictionary = [
            "mobileIsd":txtCountryCode.text ?? "",
            "mobileNumber":txtMobileNo.text ?? "",
            "password":txtPassword.text ?? "",
            "secretKey":secretKey,
            "apnsId":getAPNSDeviceToken()
        ]
        
        spinner.startAnimating()
        btnSignIn.isHidden = true;
        btnSignUp.isEnabled = false
        txtMobileNo.isEnabled = false
        txtPassword.isEnabled = false
        view.endEditing(true)
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/login?lngId=\(Localize.currentLanguageID)"), params: params as NSDictionary, completionHandler:{response, jsonObj in
            //let statusCode = response?.statusCode
            var message:String = failedToConnectMessage.localized()
            
            if let selfObj = weakSelf {
                selfObj.spinner.stopAnimating()
                selfObj.btnSignIn.isHidden = false
                selfObj.btnSignUp.isEnabled = true
                selfObj.txtMobileNo.isEnabled = true
                selfObj.txtPassword.isEnabled = true
                selfObj.view.endEditing(true)
                
                if (jsonObj?.status() ?? false) {
                    
                    //Save userdata
                    if let dataObj:NSDictionary = jsonObj?.dataObj() {
                        
                        
                        if let token:String = dataObj.object(forKey: "token") as? String {
                            
                            let status =  selfObj.saveToken(userName:  selfObj.txtMobileNo.text ?? "", token: token)
                            
                            if status {
                                
                                let configVersionNo:Int =  dataObj.object(forKey: "configVersionNo") as? Int ?? 0
                                let alertNo:Int =  dataObj.object(forKey: "alertNo") as? Int ?? 0
                                print("configVersionNoconfigVersionNoconfigVersionNoconfigVersionNo")
                                print(configVersionNo)
                                UserDefaults.standard.set(configVersionNo, forKey: "configVersionNo")
                                UserDefaults.standard.set(alertNo, forKey: "alertNo")
                                
                                self.getUserDetailsNew(token: token, alertNo: alertNo, configVersionNo: configVersionNo)
                            }
                        }
                        
//                        /////
//                        if let userDetails:NSDictionary = dataObj.object(forKey: "userDetails") as? NSDictionary{
//                            if let token:String = userDetails.object(forKey: "token") as? String {
//                                let status =  selfObj.saveToken(userName:  selfObj.txtMobileNo.text ?? "", token: token)
//                                if status {
//                                    //Sign up success
//                                    let userLoyaltyAmount = userDetails.object(forKey: "userLoyaltyAmount")
//                                    UserDefaults.standard.set(userLoyaltyAmount, forKey: "userLoyaltyAmount")
//                                    if let mutableData:NSMutableDictionary = userDetails.mutableCopy() as? NSMutableDictionary{
//                                        mutableData.removeObject(forKey: "token")
//
//                                        if  selfObj.saveUserDetails(userDetail: mutableData.copy() as! NSDictionary){
//
////                                            selfObj.navigationController?.dismiss(animated: true, completion: nil)
//
//                                            selfObj.openHome()
//                                            // selfObj.openProfile()
//
//                                            return
//                                        }
//
//                                    }
//                                }
//                            }
//                        }
                       ////////
                        
                        
                    }
                }
                if let serverMessage = jsonObj?.message(){
                    message = (serverMessage.count>0) ? serverMessage : message
                }
                selfObj.showErrorMessage(message: message)
            }
            
        })
        
    }
    
    func getUserDetailsNew(token:String,alertNo:Int,configVersionNo:Int) {
        
        UserDefaults.standard.set(1, forKey: "firstTime")
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getUserDetailsNew?token=\(token)&lngId=\(Localize.currentLanguageID)&configVersionNo=\(configVersionNo)&alertNo=\(alertNo)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                print(dataObj)
                                
                                let isFetch:Int =  dataObj.object(forKey: "isFetch") as? Int ?? 0
                                
                                let configVersionNo:Int =  dataObj.object(forKey: "configVersionNo") as? Int ?? 0
                                
                                print("configVersionNoconfigVersionNoconfigVersionNoconfigVersionNo")
                                print(configVersionNo)
                                print(dataObj.object(forKey: "config") ?? [:])
                                
                                let alertNo:Int =  dataObj.object(forKey: "alertNo") as? Int ?? 0
                                
                                UserDefaults.standard.set(configVersionNo, forKey: "configVersionNo")
                                UserDefaults.standard.set(alertNo, forKey: "alertNo")
                                
                                if isFetch == 1
                                {
                                    UserDefaults.standard.set(dataObj.object(forKey: "config"), forKey: "config")
                                    
                                    
                                    UserDefaults.standard.set(dataObj.object(forKey: "bannerList"), forKey: "bannerList")
                                    
                                    if let userDetails:NSDictionary = dataObj.object(forKey: "userDetails") as? NSDictionary{
                                        
                                        if let mutableData:NSMutableDictionary = userDetails.mutableCopy() as? NSMutableDictionary{
                                            
                                            mutableData.removeObject(forKey: "token")
                                            
                                            if  weakSelf?.saveUserDetails(userDetail: mutableData.copy() as! NSDictionary) ?? false{
                                                weakSelf?.navigationController?.dismiss(animated: true, completion: nil)

                                                weakSelf?.openHome()
                                                
                                                return
                                            }
                                        }
                                    }
                                }
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
    
    @IBAction func btnShowPassword(_ sender: Any) {
        
        if iconClick == true
        {
            btnShowHidePwd.setImage(UIImage(named: "showpwd"), for: .normal)
            txtPassword.isSecureTextEntry = false
        }
        else
        {
            btnShowHidePwd.setImage(UIImage(named: "hidepwd"), for: .normal)
            txtPassword.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
    }
    
}


