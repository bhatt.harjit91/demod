//
//  ChoseSignupSelectionViewController.swift
//  iCanRefer
//
//  Created by Vivek Mishra on 27/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class ChoseSignupSelectionViewController: AppBaseViewController {
    
    @IBOutlet weak var viewBgYesNoBtn: UIView!
    @IBOutlet weak var viewBgReferral: UIView!
    @IBOutlet weak var heightOfBtnSearchConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightOfPartnerCodeBgConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblSignUpAsText: NKLabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tblSelectedUser: UITableView!
    @IBOutlet weak var btnNext: NKButton!
    @IBOutlet weak var viewBottomBG: UIView!
    @IBOutlet weak var imgYesCheckbox: UIImageView!
    @IBOutlet weak var imgNoCheckbox: UIImageView!
    @IBOutlet weak var lblCheckBoxLabelText: NKLabel!
    @IBOutlet weak var btnSearch: NKButton!
    @IBOutlet weak var tfMobileNo: RCTextField!
    // @IBOutlet weak var heightOfReferralCodeViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgViewSuccess: UIImageView!
    @IBOutlet weak var tblreferral: UITableView!
    @IBOutlet weak var viewReferralBG: UIView!
    @IBOutlet weak var heightOfReferralBgConstraint: NSLayoutConstraint!
    var selectedIndexPath: NSInteger = -1
    var isSelected:Int = -1
    var isSelectCheckBox:Int = 0
    var buyerFeatures:[BuyerFeatures]?
    var partnerFeatures:[BuyerFeatures]?
    var heighOfCheckboox:Float = 35
    var arrayOfReferralUser = [AnyObject]()
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var currentLoc:CLLocationCoordinate2D?
    
    var locationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        referralCodeUI()
        
        navigationItem.title = "Sign Up".localized()
        
        lblSignUpAsText.text = "Sign up as".localized()
        
        fetchSelectedUserDetails()
        
        lblCheckBoxLabelText.text = "Are you referred by a Partner?".localized()
        
        imgViewSuccess.isHidden = true
        
        tfMobileNo.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Partner Code / Mobile Number".localized())
        
        let rect = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        actInd.frame = rect
        actInd.center = tfMobileNo.center
        actInd.hidesWhenStopped = true
        actInd.style = UIActivityIndicatorView.Style.gray
        tfMobileNo.rightView = actInd
        tfMobileNo.rightViewMode = .always
        
        UserDefaults.standard.removeObject(forKey: "locationFromSignUp")
        UserDefaults.standard.removeObject(forKey: "addressSignUp")
        UserDefaults.standard.removeObject(forKey: "isBuyer")
        UserDefaults.standard.removeObject(forKey: "referredUserId")
        
        getCurrentLocation()
        
        updateNavBar()
    }
    
    func updateNavBar() {
        let btnName = UIButton()
        
        btnName.setTitle("Next -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(btnNext(_:)), for: .touchUpInside)
                    
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func getCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch locationAuthorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.startUpdatingLocation()
                currentLoc = locationManager.location?.coordinate
                
            }
        case .restricted, .denied:
            
            print("Access Denied")
            
            // showLocationRequiredPage()
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
            
            currentLoc = locValue
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        // stopAnimating()
    }
    
    func showLocationRequiredPage(){
        weak var weakSelf = self
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable location access to provide you near by search.".localized(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Enable Location Service".localized(), style: .default, handler: {action in
            
            weakSelf?.openAppSettings()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    
    
    func referralCodeUI()//........
    {
        heightOfReferralBgConstraint.constant = 0
        viewReferralBG.isHidden = true
        tblreferral.isHidden = true
        selectedIndexPath = -1
        viewBgReferral.isHidden = true
        viewBgYesNoBtn.isHidden = false
        heightOfPartnerCodeBgConstraint.constant = 125
        heightOfBtnSearchConstraint.constant = 50
        viewBgReferral.isHidden = false
        tfMobileNo.showError(message: "")
        imgYesCheckbox.image = UIImage.init(imageLiteralResourceName: "radioFilled")
        imgNoCheckbox.image = UIImage.init(imageLiteralResourceName: "radio")
        isSelectCheckBox = 1
        arrayOfReferralUser = []
        imgViewSuccess.isHidden = true
        btnSearch.isHidden = false
        btnSearch.isUserInteractionEnabled = true
        UserDefaults.standard.removeObject(forKey: "referredUserId")
        tfMobileNo.text = ""
    }
    
    func referralCodeUIUpdate(height:CGFloat)
    {
        heightOfReferralBgConstraint.constant = height
        viewReferralBG.isHidden = false
        tblreferral.isHidden = false
        
    }
    
    @IBAction func btnYesCheckBox(_ sender: Any) {
        
        referralCodeUI()
    }
    
    @IBAction func btnNoCheckBox(_ sender: Any) {
        
        heightOfPartnerCodeBgConstraint.constant = 125 - 50
        heightOfBtnSearchConstraint.constant = 0
        viewBgReferral.isHidden = true
        tfMobileNo.showError(message: "")
        imgNoCheckbox.image = UIImage.init(imageLiteralResourceName: "radioFilled")
        imgYesCheckbox.image = UIImage.init(imageLiteralResourceName: "radio")
        isSelectCheckBox = 0
        
        heightOfReferralBgConstraint.constant = 0
        viewReferralBG.isHidden = true
        tblreferral.isHidden = true
        
    }
    
    
    @IBAction func btnSearchReferralCode(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if tfMobileNo.text?.count ?? 0 == 0 {
            
            tfMobileNo.showError(message: "Required".localized())
            
            return
        }
            
            //        else if tfMobileNo.text?.count ?? 0 < 13 {
            //
            //            tfMobileNo.showError(message: "Should be at least 13 characters".localized())
            //
            //            return
            //        }
            
        else
        {
            checkReferralCode()
        }
        
        
    }
    
    func checkReferralCode()
    {
        actInd.startAnimating()
        
        tfMobileNo.isUserInteractionEnabled = false
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/referredBy?lngId=\(Localize.currentLanguageID)&referralCode=\(tfMobileNo.text ?? "")"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.actInd.stopAnimating()
            
            weakSelf?.tfMobileNo.isUserInteractionEnabled = true
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false{
                    
                    if let dataObj:NSDictionary = jsonObj?.dataObj() {
                        do {
                            
                            weakSelf?.arrayOfReferralUser = dataObj["referredUserList"] as! [AnyObject]
                            
                            if weakSelf?.arrayOfReferralUser.count ?? 0 > 0
                            {
                                if weakSelf?.arrayOfReferralUser.count ?? 0 > 3
                                {
                                    weakSelf?.referralCodeUIUpdate(height: 3 * 62.0)
                                }
                                else
                                {
                                    if weakSelf?.arrayOfReferralUser.count ?? 0 == 1
                                    {
                                        weakSelf?.selectedIndexPath = 0
                                    }
                                    
                                    let height = weakSelf?.arrayOfReferralUser.count ?? 0
                                    
                                    weakSelf?.referralCodeUIUpdate(height: CGFloat(height * 62))
                                }
                                
                                weakSelf?.imgViewSuccess.isHidden = false
                                weakSelf?.btnSearch.isHidden = true
                                weakSelf?.btnSearch.isUserInteractionEnabled = false
                                
                                weakSelf?.tblreferral.reloadData()
                            }
                            else
                            {
                                weakSelf?.showErrorMessage(message: message)
                            }
                            return
                            
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                        }
                    }
                    
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    func fetchSelectedUserDetails() {
        
        startAnimating()
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/signUpAsDescription?lngId=\(Localize.currentLanguageID)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataObj:NSDictionary = jsonObj?.dataObj() {
                        do {
                            
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
                            
                            print(dataObj)
                            
                            if let isBuyer = dataObj["isBuyer"] as? Int {
                                
                                if isBuyer == 1
                                {
                                    weakSelf?.isSelected = 1////////vivekchange new
                                }
                                else
                                {
                                    weakSelf?.isSelected = 0////////vivekchange new
                                }
                            }
                            
                            let apiResponse = try decoder.decode(ChooseSignUpClass.self, from: data)
                            
                            if let apiResponse:[BuyerFeatures] = apiResponse.partnerFeatures {
                                weakSelf?.partnerFeatures = apiResponse
                                
                            }
                            if let apiResponse:[BuyerFeatures] = apiResponse.buyerFeatures {
                                weakSelf?.buyerFeatures = apiResponse
                                
                            }
                            
                            DispatchQueue.main.async {
                                weakSelf?.isSelectCheckBox = 0
                                weakSelf?.collectionView.reloadData()
                                weakSelf?.tblSelectedUser.reloadData()
                            }
                            
                            return
                            
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                        }
                    }
                    
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
 //       if isSelectCheckBox == 1
   //     {
//            if tfMobileNo.text?.count ?? 0 == 0 {
//
//                tfMobileNo.showError(message: "Required".localized())
//
//                return
//            }
//            else
//            {
        //        if imgViewSuccess.isHidden == false
//                {
//                    if selectedIndexPath >= 0
//                    {
//
//                        if let dictReferral:NSDictionary = arrayOfReferralUser[selectedIndexPath] as? NSDictionary {
//
//                            if let referredUserId = dictReferral["referredUserId"] as? Int {
//
//                                UserDefaults.standard.set(referredUserId, forKey: "referredUserId")
//
//                                print("referredUserIdreferredUserIdreferredUserIdreferredUserIdreferredUserId")
//
//                            }
//
//                        }
//                        navigateToNextScreen()
//                    }
//                    else
//                    {
//                        showErrorMessage(message: "Please select a Partner".localized())
//                    }
//                }
//                else
//                {
//                    tfMobileNo.showError(message: "Please enter valid referral code".localized())
//
//                    return
//                }
                
//            }
//        }
//        else
//        {
           // UserDefaults.standard.removeObject(forKey: "referredUserId")
            
            navigateToNextScreen()
            
//          }
    }
    
    func navigateToNextScreen()
    {
       
        
        if isSelected == 1////////vivekchange new
        {
           let storyBoard = UIStoryboard.init(name: MainSB, bundle: nil)
            let signUpInfoTableVC:SignUpInfoTableViewController = storyBoard.instantiateViewController(withIdentifier: "SignUpInfoTableViewController") as! SignUpInfoTableViewController
             signUpInfoTableVC.navigationTitle = "Public".localized()
             UserDefaults.standard.set(1, forKey: "isBuyer")
             navigationController?.pushViewController(signUpInfoTableVC, animated: true)
        }
        else
        {
            UserDefaults.standard.set(0, forKey: "isBuyer")
             let storyBoard = UIStoryboard.init(name: MainSB, bundle: nil)
                    let partnerPage:PartnerSignUpVC = storyBoard.instantiateViewController(withIdentifier: "PartnerSignUpVC") as! PartnerSignUpVC
                  //  partnerPage.navigationTitle = "User".localized()
                    
                     navigationController?.pushViewController(partnerPage, animated: true)
        }
        
    }
    
}


extension ChoseSignupSelectionViewController:UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 25
        
        tfMobileNo.showError(message: "")
        
        heightOfReferralBgConstraint.constant = 0
        viewReferralBG.isHidden = true
        tblreferral.isHidden = true
        
//        referralCodeUI()
        arrayOfReferralUser = []
        
        imgViewSuccess.isHidden = true
        btnSearch.isHidden = false
        btnSearch.isUserInteractionEnabled = true
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= maxLength
    }
}

extension ChoseSignupSelectionViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 2 {
            
            if isSelected == 1 {////////vivekchange new
                
                return buyerFeatures?.count ?? 0
            }
            
            if isSelected == 0 {////////vivekchange new
                
                return partnerFeatures?.count ?? 0
            }
        }
        
        if tableView.tag == 1 {
            
            return arrayOfReferralUser.count > 0 ? arrayOfReferralUser.count : 0
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView.tag == 2 {
            
            return UITableView.automaticDimension
        }
        if tableView.tag == 1 {
            
            return 62
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 1 {
            
            if let tableViewCell:ReferralCodeCell = tableView.dequeueReusableCell(withIdentifier: "ReferralCodeCell", for: indexPath) as? ReferralCodeCell
            {
                return getCellForReferralCode(cell: tableViewCell, indexPath: indexPath)
            }
        }
        if tableView.tag == 2 {
            
            if let tableViewCell:SelectedUserInfoCell = tableView.dequeueReusableCell(withIdentifier: "SelectedUserInfoCell", for: indexPath) as? SelectedUserInfoCell
            {
                return getCellForSelectedUserInfo(cell: tableViewCell, indexPath: indexPath)
            }
        }
        
        return UITableViewCell.init()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.tag == 1 {
            
            tableView.deselectRow(at: indexPath, animated: true)
            
            selectedIndexPath = indexPath.row
            
            tblreferral.reloadData()
            
        }
    }
    
    
    
    func getCellForReferralCode(cell:ReferralCodeCell,indexPath: IndexPath) -> ReferralCodeCell {
        
        cell.selectionStyle = .none
        
        if indexPath.row == selectedIndexPath {
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
        
        if let dictReferral:NSDictionary = arrayOfReferralUser[indexPath.row] as? NSDictionary {
            
            if let profilePicture = dictReferral["profilePicture"] as? String {
                
                if profilePicture.count > 0{
                    
                    if let url:URL = URL.init(string: "\(StorageUrl)\(profilePicture)"){
                        cell.imgViewReferralConatct.sd_setImage(with: url, completed: nil)
                    }
                }
            }
            
            if let displayName = dictReferral["displayName"] as? String {
                
                cell.lblDisplayName.text = displayName
                
            }
            
        }
        
        return cell
    }
    
    func getCellForSelectedUserInfo(cell:SelectedUserInfoCell,indexPath: IndexPath) -> SelectedUserInfoCell {
        
        cell.selectionStyle = .none
        
        if isSelected == 1 {////////vivekchange new
            
            if let model:BuyerFeatures = buyerFeatures?[indexPath.row] {
                
                cell.lblTitleText.text = model.title
                
                cell.lblDescription.text = model.description
                
                let count = indexPath.row + 1
                
                cell.lblCount.text = "\(count)"
            }
        }
        
        if isSelected == 0 {
            
            if let model:BuyerFeatures = partnerFeatures?[indexPath.row] {
                
                cell.lblTitleText.text = model.title
                
                cell.lblDescription.text = model.description
                
                let count = indexPath.row + 1
                
                cell.lblCount.text = "\(count)"
            }
        }
        
        return cell
    }
    
}


extension ChoseSignupSelectionViewController:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell:ChoseSignupCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChoseSignupCollectionCell", for: indexPath) as? ChoseSignupCollectionCell
        {
            return getCellForChoseSignupCollection(cell: cell, indexPath: indexPath)
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isSelected != indexPath.row {
            
            isSelected = indexPath.row
            
            UserDefaults.standard.removeObject(forKey: "referredUserId")
            tfMobileNo.showError(message: "")
            
            imgNoCheckbox.image = UIImage.init(imageLiteralResourceName: "radio")
            imgYesCheckbox.image = UIImage.init(imageLiteralResourceName: "radio")
            
            isSelectCheckBox = 0
            
            collectionView.reloadData()
            
            weak var weakSelf = self
            
            DispatchQueue.main.async {
                
                weakSelf?.tblSelectedUser.reloadData()
            }
        }
    }
    
    
    func getCellForChoseSignupCollection(cell:ChoseSignupCollectionCell,indexPath:IndexPath) -> ChoseSignupCollectionCell {
        
        if indexPath.row == 0
        {
            cell.lblRadioText.text = "Partner".localized()
        }
        else
        {
            cell.lblRadioText.text = "Public".localized()
        }
        
        if isSelected == indexPath.row {
            
            cell.imageViewRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled")
        }
        else
        {
            cell.imageViewRadio.image = UIImage.init(imageLiteralResourceName: "radio")
        }
        
//        if isSelected == 0 {
//
//            referralCodeUI()
//        }
//        else
//        {
            heightOfReferralBgConstraint.constant = 0
            viewReferralBG.isHidden = true
            tblreferral.isHidden = true
            arrayOfReferralUser = []
            imgViewSuccess.isHidden = true
            btnSearch.isHidden = false
            btnSearch.isUserInteractionEnabled = true
            tfMobileNo.text = ""
            heightOfPartnerCodeBgConstraint.constant = 0
            viewBgYesNoBtn.isHidden = true
            
        //}
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 115, height: collectionView.frame.size.height)
    }
    
}


class ReferralCodeCell: UITableViewCell {
    
    @IBOutlet weak var lblDisplayName: NKLabel!
    
    @IBOutlet weak var imgViewReferralConatct: RCImageView!
    
}


class SelectedUserInfoCell: UITableViewCell {
    
    @IBOutlet weak var lblTitleText: NKLabel!
    
    @IBOutlet weak var lblDescription: NKLabel!
    
    @IBOutlet weak var lblCount: NKLabel!
    
}


class ChoseSignupCollectionCell:UICollectionViewCell{
    
    
    @IBOutlet weak var viewBgCollectionView: UIView!
    @IBOutlet weak var imageViewRadio: UIImageView!
    @IBOutlet weak var lblRadioText: NKLabel!
    
}
