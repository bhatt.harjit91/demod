//
//  SignUpTermsViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import WebKit

class SignUpTermsViewController: AppBaseViewController {
    
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var heightViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var webViewer: UIView!
    
     var webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let configuration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: configuration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        view.addSubview(webView)
        
        [webView.topAnchor.constraint(equalTo: webViewer.topAnchor),
         webView.bottomAnchor.constraint(equalTo: webViewer.bottomAnchor),
         webView.leftAnchor.constraint(equalTo: webViewer.leftAnchor),
         webView.rightAnchor.constraint(equalTo: webViewer.rightAnchor)].forEach  { anchor in
            anchor.isActive = true
        }
        viewBottom.alpha = 0
        
        navigationController?.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
        
        navigationItem.title = "Sign Up".localized()
        
       // updateNavBar()
    }
    
//    func updateNavBar() {
//
//           let btnName = UIButton()
//
//           btnName.setTitle("Next -->".localized(), for: .normal)
//           btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//           btnName.setTitleColor(UIColor.white, for: .normal)
//
//          // btnName.addTarget(self, action: #selector(btnNext(_:)), for: .touchUpInside)
//
//           let rightBarButton = UIBarButtonItem()
//           rightBarButton.customView = btnName
//           self.navigationItem.rightBarButtonItem = rightBarButton
//
//       }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startAnimating()
        
        let urlStr = "\("privacy_")\(Localize.currentLanguage)\(".html")"
        
        // Do any additional setup after loading the view.
        if let urlToLoad:URL = urlStr.ImageURL(){
            let urlRequest:URLRequest = URLRequest.init(url:urlToLoad)
            webView.load(urlRequest)
        }
    }
    
    @IBAction func declineTerms(_ sender: Any) {
        
        let alert = UIAlertController.init(title: "Are you sure to cancel the sign up ?".localized(), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "No".localized(), style: .cancel, handler: { (action) in
            
        }))
        alert.addAction(UIAlertAction.init(title: "Yes".localized(), style: .destructive, handler: { (action) in
            
            self.navigationController?.popViewController(animated: true)
            
        }))
        present(alert, animated: true, completion: nil)
    
    }
    
    // MARK: - WEBView
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        stopAnimating()
        viewBottom.alpha = 1
        
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
         stopAnimating()
    }
    
    
}

