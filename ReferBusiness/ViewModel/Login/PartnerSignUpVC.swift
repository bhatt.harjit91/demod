//
//  PartnerSignUpVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 17/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class PartnerSignUpVC: AppBaseViewController {

    @IBOutlet weak var viewBgYesNoBtn: UIView!
    @IBOutlet weak var viewBgReferral: UIView!
    @IBOutlet weak var heightOfBtnSearchConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightOfPartnerCodeBgConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnNext: NKButton!
    @IBOutlet weak var viewBottomBG: UIView!
    @IBOutlet weak var imgYesCheckbox: UIImageView!
    @IBOutlet weak var imgNoCheckbox: UIImageView!
    @IBOutlet weak var lblCheckBoxLabelText: NKLabel!
    @IBOutlet weak var btnSearch: NKButton!
    @IBOutlet weak var tfMobileNo: RCTextField!
    @IBOutlet weak var imgViewSuccess: UIImageView!
    @IBOutlet weak var tblreferral: UITableView!
    @IBOutlet weak var viewReferralBG: UIView!
    @IBOutlet weak var heightOfReferralBgConstraint: NSLayoutConstraint!
    
    var selectedIndexPath: NSInteger = -1
    
    var arrayOfReferralUser = [AnyObject]()
    
    var isSelectCheckBox:Int = 0
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var isSelected:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
         navigationItem.title = "Partner Sign Up".localized()
        
         lblCheckBoxLabelText.text = "Are you referred by a Partner?".localized()
        
        imgViewSuccess.isHidden = true
        
        referralCodeUI()
               
        tfMobileNo.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Partner Code / Mobile Number".localized())
               
               let rect = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
               actInd.frame = rect
               actInd.center = tfMobileNo.center
               actInd.hidesWhenStopped = true
               actInd.style = UIActivityIndicatorView.Style.gray
               tfMobileNo.rightView = actInd
               tfMobileNo.rightViewMode = .always
        
                updateNavBar()
    }
    
    func updateNavBar() {
      
                 let btnName = UIButton()
      
                 btnName.setTitle("Next -->".localized(), for: .normal)
                 btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                 btnName.setTitleColor(UIColor.white, for: .normal)
      
              btnName.addTarget(self, action: #selector(btnNext(_:)), for: .touchUpInside)
      
                 let rightBarButton = UIBarButtonItem()
                 rightBarButton.customView = btnName
                 self.navigationItem.rightBarButtonItem = rightBarButton
      
             }
      
    
    func referralCodeUI()//........
       {
           heightOfReferralBgConstraint.constant = 0
           viewReferralBG.isHidden = true
           tblreferral.isHidden = true
           selectedIndexPath = -1
           viewBgReferral.isHidden = true
           viewBgYesNoBtn.isHidden = false
           heightOfPartnerCodeBgConstraint.constant = 180
           heightOfBtnSearchConstraint.constant = 100
           viewBgReferral.isHidden = false
           tfMobileNo.showError(message: "")
           imgYesCheckbox.image = UIImage.init(imageLiteralResourceName: "radioFilled")
           imgNoCheckbox.image = UIImage.init(imageLiteralResourceName: "radio")
           isSelectCheckBox = 1
           arrayOfReferralUser = []
           imgViewSuccess.isHidden = true
           btnSearch.isHidden = false
           btnSearch.isUserInteractionEnabled = true
           UserDefaults.standard.removeObject(forKey: "referredUserId")
           tfMobileNo.text = ""
       }
   
    @IBAction func btnYesCheckBox(_ sender: Any) {
        
        referralCodeUI()
    }
    
    @IBAction func btnNoCheckBox(_ sender: Any) {
        
        heightOfPartnerCodeBgConstraint.constant = 180 - 100
        heightOfBtnSearchConstraint.constant = 0
        viewBgReferral.isHidden = true
        tfMobileNo.showError(message: "")
        imgNoCheckbox.image = UIImage.init(imageLiteralResourceName: "radioFilled")
        imgYesCheckbox.image = UIImage.init(imageLiteralResourceName: "radio")
        isSelectCheckBox = 0
        
        heightOfReferralBgConstraint.constant = 0
        viewReferralBG.isHidden = true
        tblreferral.isHidden = true
        
    }
    
    @IBAction func btnSearchReferralCode(_ sender: Any) {
         
         self.view.endEditing(true)
         
         if tfMobileNo.text?.count ?? 0 == 0 {
             
             tfMobileNo.showError(message: "Required".localized())
             
             return
         }
             
             //        else if tfMobileNo.text?.count ?? 0 < 13 {
             //
             //            tfMobileNo.showError(message: "Should be at least 13 characters".localized())
             //
             //            return
             //        }
             
         else
         {
             checkReferralCode()
         }
         
         
     }
    
    @IBAction func btnNext(_ sender: Any) {
        
        if isSelectCheckBox == 1
        {
            if tfMobileNo.text?.count ?? 0 == 0 {
                
                tfMobileNo.showError(message: "Required".localized())
                
                return
            }
            else
            {
                if imgViewSuccess.isHidden == false
                {
                    if selectedIndexPath >= 0
                    {
                        
                        if let dictReferral:NSDictionary = arrayOfReferralUser[selectedIndexPath] as? NSDictionary {
                            
                            if let referredUserId = dictReferral["referredUserId"] as? Int {
                                
                                UserDefaults.standard.set(referredUserId, forKey: "referredUserId")
                                
                                print("referredUserIdreferredUserIdreferredUserIdreferredUserIdreferredUserId")
                                
                            }
                            
                        }
                        navigateToNextScreen()
                    }
                    else
                    {
                        showErrorMessage(message: "Please select a Partner".localized())
                    }
                }
                else
                {
                    tfMobileNo.showError(message: "Please enter valid referral code".localized())
                    
                    return
                }
                
            }
        }
        else
        {
            UserDefaults.standard.removeObject(forKey: "referredUserId")
            
            navigateToNextScreen()
        }
    }
    
    func referralCodeUIUpdate(height:CGFloat){
        
           heightOfReferralBgConstraint.constant = height
           viewReferralBG.isHidden = false
           tblreferral.isHidden = false
           
       }
    
    
   func checkReferralCode()
    {
        actInd.startAnimating()
        
        tfMobileNo.isUserInteractionEnabled = false
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/referredBy?lngId=\(Localize.currentLanguageID)&referralCode=\(tfMobileNo.text ?? "")"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.actInd.stopAnimating()
            
            weakSelf?.tfMobileNo.isUserInteractionEnabled = true
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false{
                    
                    if let dataObj:NSDictionary = jsonObj?.dataObj() {
                        do {
                            
                            weakSelf?.arrayOfReferralUser = dataObj["referredUserList"] as! [AnyObject]
                            
                            if weakSelf?.arrayOfReferralUser.count ?? 0 > 0
                            {
                                if weakSelf?.arrayOfReferralUser.count ?? 0 > 3
                                {
                                    weakSelf?.referralCodeUIUpdate(height: 3 * 62.0)
                                }
                                else
                                {
                                    if weakSelf?.arrayOfReferralUser.count ?? 0 == 1
                                    {
                                        weakSelf?.selectedIndexPath = 0
                                    }
                                    
                                    let height = weakSelf?.arrayOfReferralUser.count ?? 0
                                    
                                    weakSelf?.referralCodeUIUpdate(height: CGFloat(height * 62))
                                }
                                
                                weakSelf?.imgViewSuccess.isHidden = false
                                weakSelf?.btnSearch.isHidden = true
                                weakSelf?.btnSearch.isUserInteractionEnabled = false
                                
                                weakSelf?.tblreferral.reloadData()
                            }
                            else
                            {
                                weakSelf?.showErrorMessage(message: message)
                            }
                            return
                            
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                        }
                    }
                    
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    func navigateToNextScreen()
       {
           let storyBoard = UIStoryboard.init(name: MainSB, bundle: nil)
           let signUpInfoTableVC:SignUpInfoTableViewController = storyBoard.instantiateViewController(withIdentifier: "SignUpInfoTableViewController") as! SignUpInfoTableViewController
           
           if isSelected == 1////////vivekchange new
           {
               UserDefaults.standard.set(1, forKey: "isBuyer")
               signUpInfoTableVC.navigationTitle = "User".localized()
           }
           else
           {
               UserDefaults.standard.set(0, forKey: "isBuyer")
               signUpInfoTableVC.navigationTitle = "Partner".localized()
           }
           
           navigationController?.pushViewController(signUpInfoTableVC, animated: true)
       }

}

extension PartnerSignUpVC:UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 25
        
        tfMobileNo.showError(message: "")
        
        heightOfReferralBgConstraint.constant = 0
        viewReferralBG.isHidden = true
        tblreferral.isHidden = true
        
//        referralCodeUI()
        arrayOfReferralUser = []
        
        imgViewSuccess.isHidden = true
        btnSearch.isHidden = false
        btnSearch.isUserInteractionEnabled = true
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= maxLength
    }
}

extension PartnerSignUpVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
          return arrayOfReferralUser.count > 0 ? arrayOfReferralUser.count : 0
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 62
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            if let tableViewCell:ReferralCodeCell = tableView.dequeueReusableCell(withIdentifier: "ReferralCodeCell", for: indexPath) as? ReferralCodeCell
            {
                           return getCellForReferralCode(cell: tableViewCell, indexPath: indexPath)
            }
        
        return UITableViewCell.init()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            tableView.deselectRow(at: indexPath, animated: true)
            
            selectedIndexPath = indexPath.row
            
            tblreferral.reloadData()
            
    }
    
    
    
    func getCellForReferralCode(cell:ReferralCodeCell,indexPath: IndexPath) -> ReferralCodeCell {
        
        cell.selectionStyle = .none
        
        if indexPath.row == selectedIndexPath {
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
        
        if let dictReferral:NSDictionary = arrayOfReferralUser[indexPath.row] as? NSDictionary {
            
            if let profilePicture = dictReferral["profilePicture"] as? String {
                
                if profilePicture.count > 0{
                    
                    if let url:URL = URL.init(string: "\(StorageUrl)\(profilePicture)"){
                        cell.imgViewReferralConatct.sd_setImage(with: url, completed: nil)
                    }
                }
            }
            
            if let displayName = dictReferral["displayName"] as? String {
                
                cell.lblDisplayName.text = displayName
                
            }
            
        }
        
        return cell
    }
    
}
