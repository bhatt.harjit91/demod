//
//  SignUpPasswordTableViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class SignUpPasswordTableViewController: UITableViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtPassword: RCTextField!
    @IBOutlet weak var txtConfirmPassword: RCTextField!
    @IBOutlet weak var btnFinish: NKButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var btnTermsOfUse: NKButton!
    @IBOutlet weak var btnPrivacyPolicy: NKButton!
    @IBOutlet weak var btnAggrement: NKButton!
    
    ///
    
    @IBOutlet weak var btnConfirmPwd: UIButton!
    
    @IBOutlet weak var btnPwd: UIButton!
    
    var iconPwdClick = true
    
    var iconConfPwdClick = true
    ///
    
    var isdCode:String = ""
    var mobileNo:String = ""
    var otp:String = ""
    var displayName:String = ""
    var email:String = ""
    var profilePicUrl:String = ""
    var isForgotPassword:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Forgot Password".localized()
        txtPassword.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Password".localized())
        txtConfirmPassword.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Confirm Password".localized())
        
        
        txtPassword.delegate = self;
        txtConfirmPassword.delegate = self
        
        if isForgotPassword{
            btnFinish.setTitle("Finish".localized(), for: .normal)
            if let viewcontrollers =  navigationController?.viewControllers{
                
                for viewController in viewcontrollers{
                    if viewController.isKind(of: SignUpOTPVerifyViewController.self) {
                        if let index = viewcontrollers.index(of:viewController){
                            navigationController?.viewControllers.remove(at: index)
                            break
                        }
                        
                    }
                }
            }
        }
        else {
            btnFinish.setTitle("Next".localized(), for: .normal)
        }
    }
    
    func addPlaceHolderWithText(placeholder:String) -> NSMutableAttributedString
    {
        let starAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.fontForType(textType: 6),
            .foregroundColor: UIColor.getGradientColor(index: 1).first ?? 0]
        
        let star = NSAttributedString(string:"*" , attributes: starAttributes)
        
        let starTexr = NSMutableAttributedString(string: placeholder)
        
        starTexr.append(star)
        
        return starTexr
    }
    
    @IBAction func btnShowHidePwd(_ sender: Any) {
        
        if iconPwdClick == true
        {
            btnPwd.setImage(UIImage(named: "showpwd"), for: .normal)
            txtPassword.isSecureTextEntry = false
        }
        else
        {
            btnPwd.setImage(UIImage(named: "hidepwd"), for: .normal)
            txtPassword.isSecureTextEntry = true
        }
        
        iconPwdClick = !iconPwdClick
    }
    
    
    @IBAction func btnConfirmShowHidePwd(_ sender: Any) {
        
        if iconConfPwdClick == true
        {
            btnConfirmPwd.setImage(UIImage(named: "showpwd"), for: .normal)
            txtConfirmPassword.isSecureTextEntry = false
        }
        else
        {
            btnConfirmPwd.setImage(UIImage(named: "hidepwd"), for: .normal)
            txtConfirmPassword.isSecureTextEntry = true
        }
        
        iconConfPwdClick = !iconConfPwdClick
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtPassword{
            txtPassword.showError(message: "")
        }
        else if textField == txtConfirmPassword{
            txtConfirmPassword.showError(message: "")
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtPassword{
            txtPassword.showError(message: "")
        }
        if textField == txtConfirmPassword{
            txtConfirmPassword.showError(message: "")
        }
        
        return true
    }
    func validate() -> Bool {
        
        if txtPassword.text?.count ?? 0 == 0 {
            txtPassword.showError(message: "Required".localized())
            return false
        }
        
        if txtPassword.text?.count ?? 0 < 6 {
            txtPassword.showError(message: "6 to 24 characters and at least one numeric digit".localized())
            return false
        }
        if  txtPassword.text?.count ?? 0 > 24 {
            txtPassword.showError(message:"6 to 24 characters and at least one numeric digit".localized())
            return false
        }
        if txtConfirmPassword.text?.count == 0 {
            txtConfirmPassword.showError(message: "Required".localized())
            return false
        }
        
        if txtPassword.text != txtConfirmPassword.text {
            txtConfirmPassword.showError(message: "Passwords do not match.".localized())
            showWarningMessage(message: "Please enter same password in both the fields.".localized())
            return false
        }
        return true
    }
    
    
    @IBAction func openTerms(_ sender: Any) {
        openWebView(urlString: "\("terms_")\(Localize.currentLanguage)\(".html")", title: "Terms".localized(), subTitle: "")
    }
    
    @IBAction func openPrivacy(_ sender: Any) {
        openWebView(urlString:  "\("privacy_")\(Localize.currentLanguage)\(".html")", title: "Privacy".localized(), subTitle: "")
    }
    @IBAction func agressToTerms(_ sender: NKButton) {
        btnAggrement.isSelected = !btnAggrement.isSelected
    }
    @IBAction func callFinishSignUp(_ sender: Any) {
        let valid = validate()
        if !valid {
            return
        }
        if isForgotPassword {
            setPassword()
        }
        else{
            signUP()
        }
    }
    
    func setPassword() {
        btnFinish.isHidden = true
        
        txtPassword.resignFirstResponder()
        txtConfirmPassword.resignFirstResponder()
        
        spinner.startAnimating()
        
        let params:NSDictionary = [
            "mobileIsd":isdCode,
            "mobileNumber":mobileNo,
            "otp":otp,
            "newPassword":txtPassword.text ?? "",
        ]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/resetPassword?lngId=\(Localize.currentLanguageID)"), params: params as NSDictionary, completionHandler:{response, jsonObj in
            //let statusCode = response?.statusCode
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                
                //Save userdata
                weakSelf?.showInfoMessage(message: message)
                weakSelf?.spinner.stopAnimating()
                let deadlineTime = DispatchTime.now() + .seconds(1)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    weakSelf?.navigationController?.popToRootViewController(animated: true)
                }
                return
            }
            
            weakSelf?.showErrorMessage(message: message)
            weakSelf?.btnFinish.isHidden = false
            weakSelf?.spinner.stopAnimating()
        })
    }
    
    func signUP() {
        
        txtPassword.resignFirstResponder()
        txtConfirmPassword.resignFirstResponder()
        
        var secretKey:String = ""
        if generateSecretKey() {
            if let key = getSecretKey(){
                secretKey = key
            }
        }
        let params:NSDictionary = [
            "mobileIsd":isdCode,
            "mobileNumber":mobileNo,
            "otp":otp,
            "displayName":displayName,
            "emailId":email,
            "profilePicture":profilePicUrl,
            "password":txtPassword.text ?? "",
            "secretKey":secretKey,
            "apnsId":getAPNSDeviceToken()
        ]
        
        let finalPage:SignUpProfessionInfoViewController = storyboard?.instantiateViewController(withIdentifier: "SignUpProfessionInfoViewController") as! SignUpProfessionInfoViewController
        finalPage.params = params as! [String : Any]
        finalPage.mobileNo = mobileNo
        navigationController?.pushViewController(finalPage, animated: true)
        
    }
    
}
