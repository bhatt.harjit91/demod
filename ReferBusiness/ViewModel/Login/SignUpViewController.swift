//
//  SignUpViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
class SignUpViewController: SendOTPViewController,UITextFieldDelegate,CountryCodeSelectViewControllerDelegate {
    
    
    // @IBOutlet weak var lblEnterPhoneNo: UILabel!
    
    @IBOutlet weak var lblEnterPhoneNo: NKLabel!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var btnNext: NKButton!
    @IBOutlet weak var txtCountryCode: RCTextField!
    @IBOutlet weak var txtPhoneNum: RCTextField!
    var isForgotPassword:Bool = false
    var yframe:CGFloat = 0
    var selectedCountryCode : CountryCodeModel?
    
    var countryCode:CountryCodeModel?
    var strMobileNumber:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
//        if let currentCountry =  getLocalCountryCode(){
//            selectedCountryCode = currentCountry
//            // txtCountryCode.setTitle(currentCountry.dial_code, for: .normal)
//            txtCountryCode.text = currentCountry.dial_code
//        }
        
        if countryCode?.dial_code.count ?? 0 > 0 {
            
            txtCountryCode.text = countryCode?.dial_code ?? ""
            
            selectedCountryCode = countryCode
            
        }
        else{
            if let currentCountry =  getLocalCountryCode()
            {
                selectedCountryCode = currentCountry
                
                txtCountryCode.text = currentCountry.dial_code
            }
        }
        
        if strMobileNumber?.count ?? 0 > 0 {
            
            txtPhoneNum.text = strMobileNumber ?? ""
        }
        
        lblEnterPhoneNo.textAllignType = 2
        txtPhoneNum.textAlignment = Localize.isArabic() ? .right : .left
        //txtPhoneNum.becomeFirstResponder()
        updateStrings()
        
        txtCountryCode.delegate = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6, execute: {
            self.txtPhoneNum.becomeFirstResponder()
        })
        
        btnNext.isEnabled = true
        
         navigationController?.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
    }
    
    func updateStrings() {
        lblEnterPhoneNo.text = "Please confirm your country code and enter your mobile number".localized()
        btnNext.setTitle("Next".localized(), for: .normal)
        navigationItem.title = "Forgot Password".localized()
        
        txtPhoneNum.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Mobile Number".localized())
    }
    
    func updateUI() {
        btnNext.backgroundColor = UIColor.primaryColor
        btnNext.setTitleColor(UIColor.pTextColor, for: .normal)
        btnNext.setTitleColor(UIColor.lightGray, for: .disabled)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField ==  txtCountryCode {
            openCountrySelector()
            return false;
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField ==  txtPhoneNum {
//            let nsString = textField.text as NSString?
//            let numbString = nsString?.replacingCharacters(in: range, with: string)
//            let Formatter = NumberFormatter()
//            Formatter.locale = NSLocale(localeIdentifier: "en") as Locale?
//            let final = Formatter.number(from: numbString!)
//            if final != 0 {
//                ////print("\(final!)")
//                textField.text = final?.stringValue
//                if (textField.text?.count)!>0{
//
//                    navigationItem.title = ("\(txtCountryCode.text ?? "") \(txtPhoneNum.text ?? "")")
//                }
//                else{
//
//                    navigationItem.title = "Mobile Number".localized()
//                }
//                return false
//            }
//        }
        
        if textField ==  txtPhoneNum {
            
            txtPhoneNum.showError(message: "")
            
            let maxLength = 14
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }

        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func openCountrySelector() {
        let storyBoard = UIStoryboard.init(name: CountryCodeSB, bundle: nil)
        let countrySelector:CountryCodeSelectViewController = storyBoard.instantiateViewController(withIdentifier: "CountryCodeSelectViewController") as! CountryCodeSelectViewController
        countrySelector.selectedCountryCode = selectedCountryCode
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
    }
    
    func didFinishSelectingCountryCode(selectedCountry: CountryCodeModel) {
        selectedCountryCode = selectedCountry
        txtCountryCode.text = selectedCountry.dial_code
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        txtPhoneNum.resignFirstResponder()
        
        if selectedCountryCode == nil {
            showErrorMessage(message: "Please select country code".localized())
          //  showCustomMessage(message: "Please select country code".localized(), yPos: yframe )
            
            return
        }
        if txtCountryCode.text?.count ?? 0 == 0 {
            
            txtPhoneNum.showError(message: "Required")
         
            
            return
        }
        if txtPhoneNum.text?.count ?? 0 == 0 {
            
            txtPhoneNum.showError(message: "Required".localized())
            
            
            
            return
        }
        if txtPhoneNum.text?.count ?? 0 < 5 {
            
            txtPhoneNum.showError(message: "Should be at least 5 characters".localized())
            
            
            return
        }
       
    
        spinner.startAnimating()
//        txtPhoneNum.isEnabled = false
//        txtCountryCode.isEnabled = false
        txtPhoneNum.resignFirstResponder()
        weak var weakSelf = self
        if isForgotPassword {
            sendForgotPasswordOTP(isdCode: txtCountryCode.text ?? "", mobileNo: txtPhoneNum.text ?? "", completionHandler: { status, message in
                if status {
                    weakSelf?.openOtpScreen()
                }
                else{
                    
                     weakSelf?.showErrorMessage(message: message)
                 
                }
                weakSelf?.btnNext.isHidden = false
                weakSelf?.spinner.stopAnimating()
                weakSelf?.txtPhoneNum.isEnabled = true
                weakSelf?.txtCountryCode.isEnabled = true
            })
        }
        else{
            requestOTP(isdCode: txtCountryCode.text ?? "", mobileNo: txtPhoneNum.text ?? "")
        }
    }
    func openConfirmMobileNumber() {
        let storyBoard = UIStoryboard.init(name: MainSB, bundle: nil)
        let confirmMobileNumber:ConfirmViewController = storyBoard.instantiateViewController(withIdentifier: "ConfirmViewController") as! ConfirmViewController
        confirmMobileNumber.countryCode = txtCountryCode.text
        confirmMobileNumber.mobileNo = txtPhoneNum.text
        navigationController?.pushViewController(confirmMobileNumber, animated: true)
    }
    func profileSignUp() {
        let storyBoard = UIStoryboard.init(name: MainSB, bundle: nil)
        let profileSignUp:SignUpInfoTableViewController = storyBoard.instantiateViewController(withIdentifier: "SignUpInfoTableViewController") as! SignUpInfoTableViewController
        profileSignUp.isdCode = txtCountryCode.text ?? ""
        profileSignUp.mobileNo = txtPhoneNum.text ?? ""

        navigationController?.pushViewController(profileSignUp, animated: true)
    }
    
    func openOtpScreen() {
        //OTPSegue
        performSegue(withIdentifier: "OTPSegue", sender:nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OTPSegue" {
            let otpScreen:SignUpOTPVerifyViewController = segue.destination as! SignUpOTPVerifyViewController
            otpScreen.isdCode = txtCountryCode.text!
            otpScreen.mobileNo = txtPhoneNum.text!
            otpScreen.navigationTitle = "Forgot Password".localized()
            otpScreen.isForgotPassword = isForgotPassword
        }
    }
    
    func requestOTP(isdCode:String, mobileNo:String) {
        let params:NSDictionary = [
            "mobileIsd":isdCode,
            "mobileNumber":mobileNo,
            "isSignUp":1
        ]
        var isOTPRequired:Int?
        weak var weakSelf = self
        let apiClient:APIClient = APIClient()
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/sendOtp?lngId=\(Localize.currentLanguageID)"), params: params as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                if let dataObj:NSDictionary = jsonObj?.dataObj(){
                    isOTPRequired = dataObj.value(forKey: "isOtpRequired") as? Int
                    if isOTPRequired == 1{
                        weakSelf?.openOtpScreen()
                    }
                    else if isOTPRequired == 0{
                        weakSelf?.openConfirmMobileNumber()
                    }
                    else if isOTPRequired == 2{
                        weakSelf?.profileSignUp()
                    }
                    weakSelf?.spinner.stopAnimating()
                }
                
            }
            else{
                weakSelf?.spinner.stopAnimating()
                
                weakSelf?.showErrorMessage(message: message)
                
            }
            weakSelf?.btnNext.isHidden = false
            weakSelf?.spinner.stopAnimating()
            weakSelf?.txtPhoneNum.isEnabled = true
            weakSelf?.txtCountryCode.isEnabled = true
            
        })
    }
    
}

