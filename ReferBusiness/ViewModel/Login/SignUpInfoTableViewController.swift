//
//  SignUpInfoTableViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

enum SignUpSection:Int {
    case ProfilePicture = 0
    case FirstName
    case EmailId
    case MobileNumber
    case Address
    case Password
    case ConfirmPassword
}


class SignUpInfoTableViewController: UITableViewController, UITextFieldDelegate, UINavigationControllerDelegate,CountryCodeSelectViewControllerDelegate,UIImagePickerControllerDelegate,LocationPermissionVCNewDelegate,CLLocationManagerDelegate {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            switch indexPath.row{
            case SignUpSection.ProfilePicture.rawValue:
                return 140
                
            case SignUpSection.FirstName.rawValue:
                return  60
            case SignUpSection.EmailId.rawValue:
                return  60
            case SignUpSection.MobileNumber.rawValue:
                return 60
            case SignUpSection.Address.rawValue:
                
                if navigationTitle == "User".localized()
                {
                    return 0
                }
                return 97
            case SignUpSection.Password.rawValue:
                return 100
            case SignUpSection.ConfirmPassword.rawValue:
                return 60
            default:
                return 0
            }
        }
        else if indexPath.section == 1 {
            
            return 60
        }
        return 0
    }
    
    
    @IBOutlet weak var imgViewCamera: UIImageView!
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    @IBOutlet weak var imgViewProfilePic: UIImageView!
    @IBOutlet weak var txtFirstName: RCTextField!
    @IBOutlet weak var txtLastName: RCTextField!
    @IBOutlet weak var txtEmail: RCTextField!
    
    @IBOutlet weak var btnNext: NKButton!
    
    @IBOutlet weak var imgViewLocation: UIImageView!
    @IBOutlet weak var lblAddress: NKLabel!
    @IBOutlet weak var txtCountryCode: RCTextField!
    
    @IBOutlet weak var txtMobileNo: RCTextField!
    
    @IBOutlet weak var txtPassword: RCTextField!
    
    @IBOutlet weak var lblMinCharacter: NKLabel!
    
    @IBOutlet weak var txtConfirmPassword: RCTextField!
    
    @IBOutlet weak var btnShowHidePwd: UIButton!
    
    @IBOutlet weak var btnConfirmShowHidePwd: UIButton!
    
    var iconShowHidePwdClick = true
    
    var iconConfirmShowHidePwdClick = true
    
    
    var imagePicker: UIImagePickerController!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var isUploadingProceess = false
    var isdCode:String = ""
    var mobileNo:String = ""
    var navigationTitle:String = ""
    var otp:String = ""
    var profilePicUrl:String = ""
    var titleName:String = ""
    
    var isForgotPassword:Bool = false
    var selectedCountryCode : CountryCodeModel?
    
    var currentLoc:CLLocationCoordinate2D?
    
    var locationManager = CLLocationManager()
    
    var isExistingUser:Int?
    
    var msg:String = ""
    
    var isNoTapped:Bool = false
    
    
    override func viewDidAppear(_ animated: Bool) {
        updateLocationName()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgViewLocation.setImageColor(color: .white)
        
        
        getCurrentLocation()
        
        lblAddress.text = "Fetching Location...".localized()
        
        spinner.isHidden = true
        
        imgViewCamera.setImageColor(color: UIColor.white)
        
        txtFirstName.delegate = self
        txtPassword.delegate = self
        txtConfirmPassword.delegate = self
        txtMobileNo.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        
        if let currentCountry =  getLocalCountryCode(){
            selectedCountryCode = currentCountry
            // txtCountryCode.setTitle(currentCountry.dial_code, for: .normal)
            txtCountryCode.text = currentCountry.dial_code
        }
        
        titleName = "\(navigationTitle)\(" ")\("Sign Up".localized())"
        navigationItem.title = titleName
        
        txtFirstName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "First Name".localized())
        
        txtPassword.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Password".localized())
        
        txtConfirmPassword.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Confirm Password".localized())
        
        txtMobileNo.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Mobile Number".localized())
        
        txtEmail.placeholder = "Email ID".localized()
        btnNext.setTitle("Next".localized(), for: .normal)
        
        txtCountryCode.delegate = self
        
        
        //        if let viewcontrollers =  navigationController?.viewControllers{
        //
        //            for viewController in viewcontrollers{
        //                if viewController.isKind(of: SignUpOTPVerifyViewController.self) {
        //                    if let index = viewcontrollers.index(of:viewController){
        //                        navigationController?.viewControllers.remove(at: index)
        //                        break
        //                    }
        //
        //                }
        //            }
        //        }
        
        updateNavBar()
    }
    
    func updateNavBar() {
        
        let btnName = UIButton()
        
        btnName.setTitle("Next -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        
        btnName.addTarget(self, action: #selector(callNextScreen(_:)), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    
    func addPlaceHolderWithText(placeholder:String) -> NSMutableAttributedString
    {
        let starAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.fontForType(textType: 6),
            .foregroundColor: UIColor.getGradientColor(index: 1).first ?? 0]
        
        let star = NSAttributedString(string:"*" , attributes: starAttributes)
        
        let starTexr = NSMutableAttributedString(string: placeholder)
        
        starTexr.append(star)
        
        return starTexr
    }
    
    
    @IBAction func btnShowHidePwd(_ sender: Any) {
        
        if iconShowHidePwdClick == true
        {
            btnShowHidePwd.setImage(UIImage(named: "showpwd"), for: .normal)
            txtPassword.isSecureTextEntry = false
        }
        else
        {
            btnShowHidePwd.setImage(UIImage(named: "hidepwd"), for: .normal)
            txtPassword.isSecureTextEntry = true
        }
        
        iconShowHidePwdClick = !iconShowHidePwdClick
    }
    
    @IBAction func btnConfirmShowHidePwd(_ sender: Any) {
        
        if iconConfirmShowHidePwdClick == true
        {
            btnConfirmShowHidePwd.setImage(UIImage(named: "showpwd"), for: .normal)
            txtConfirmPassword.isSecureTextEntry = false
        }
        else
        {
            btnConfirmShowHidePwd.setImage(UIImage(named: "hidepwd"), for: .normal)
            txtConfirmPassword.isSecureTextEntry = true
        }
        
        iconConfirmShowHidePwdClick = !iconConfirmShowHidePwdClick
    }
    
    @IBAction func btnLocation(_ sender: Any) {
        
        openMap()
    }
    
    func openMap() {
        
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        locationPage.lat = currentLoc?.latitude ?? 0.0
        locationPage.lng = currentLoc?.longitude ?? 0.0
        locationPage.modalPresentationStyle = .fullScreen
        navigationController?.present(locationPage, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField ==  txtFirstName {
            let nsString = textField.text as NSString?
            let numbString = nsString?.replacingCharacters(in: range, with: string) ?? ""
            if numbString.count>0{
                txtFirstName.showError(message: "")
            }
            
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
            
        else if textField ==  txtLastName {
            
            let maxLength = 50
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else if textField ==  txtEmail {
            
            txtEmail.showError(message: "")
            
            let nsString = textField.text as NSString?
            let numbString = nsString?.replacingCharacters(in: range, with: string) ?? ""
            if numbString.count>0{
                if  isValidEmail(Str: numbString){
                    txtEmail.showError(message: "")
                }
            }
        }
        else if textField ==  txtMobileNo {
            let nsString = textField.text as NSString?
            let numbString = nsString?.replacingCharacters(in: range, with: string) ?? ""
            if numbString.count>0{
                txtMobileNo.showError(message: "")
            }
            
            let maxLength = 14
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
            
        else if textField ==  txtPassword {
            let nsString = textField.text as NSString?
            let numbString = nsString?.replacingCharacters(in: range, with: string) ?? ""
            if numbString.count>0{
                txtPassword.showError(message: "")
            }
        }
        else if textField ==  txtConfirmPassword {
            let nsString = textField.text as NSString?
            let numbString = nsString?.replacingCharacters(in: range, with: string) ?? ""
            if numbString.count>0{
                txtConfirmPassword.showError(message: "")
            }
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField ==  txtCountryCode {
            openCountrySelector()
            return false;
        }
        return true
    }
    
    func isValidated(_ password: String) -> Bool {
        
        var digit: Bool = false
        
        
        if password.count  >= 6 {
            for char in password.unicodeScalars {
                
                if !digit {
                    digit = CharacterSet.decimalDigits.contains(char)
                }
                
            }
            if  digit  {
                
                return true
            }
            else {
                
                txtPassword.showError(message: "Minimum 6 characters,atleast 1 digit required".localized())
                
                return false
            }
        }
        return false
    }
    
    func openCountrySelector() {
        let storyBoard = UIStoryboard.init(name: CountryCodeSB, bundle: nil)
        let countrySelector:CountryCodeSelectViewController = storyBoard.instantiateViewController(withIdentifier: "CountryCodeSelectViewController") as! CountryCodeSelectViewController
        countrySelector.selectedCountryCode = selectedCountryCode
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
    }
    
    func didFinishSelectingCountryCode(selectedCountry: CountryCodeModel) {
        selectedCountryCode = selectedCountry
        txtCountryCode.text = selectedCountry.dial_code
    }
    
    func openOtpScreen() {
        //OTPSegue
        performSegue(withIdentifier: "OTPSegue", sender:nil)
    }
    
    func openBecomePartnerOrUser(withMessage:String) {
        
        let alert = UIAlertController(title: nil, message:withMessage, preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.btnLinkColor
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            //  self.submitClaimRequest()
            
            self.requestOTP(isdCode: self.txtCountryCode.text ?? "", mobileNo: self.txtMobileNo.text ?? "", mailId: self.txtEmail.text ?? "")
            
            // self.alert.
            
            self.isNoTapped = false
            
        }))
        
        alert.addAction(UIAlertAction(title: "No".localized(),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        //   self.openOtpScreen()
                                        
                                        self.isNoTapped = true
                                        
                                        // self.dismiss(animated: true, completion: nil)
                                        
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func getCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch locationAuthorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.startUpdatingLocation()
                currentLoc = locationManager.location?.coordinate
                
                updateLocationName()
            }
        case .restricted, .denied:
            
            // showLocationRequiredPage()
            
            print("Access Denied")
            
        }
    }
    
    func showLocationRequiredPage(){
        weak var weakSelf = self
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable location access to provide you near by search.".localized(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Enable Location Service".localized(), style: .default, handler: {action in
            
            weakSelf?.openAppSettings()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: Location manager
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
            
            currentLoc = locValue
            
            updateLocationName()
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        // stopAnimating()
    }
    
    func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        currentLoc = coordinate
        updateLocationName()
    }
    
    func updateLocationName() {
        let geoCoder = GMSGeocoder()
        
        weak var weakSelf = self
        
        geoCoder.reverseGeocodeCoordinate(currentLoc ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), completionHandler: { response, error in
            
            if response != nil {
                let address = response?.results()?.first
                if address != nil {
                    
                    weakSelf?.lblAddress.text = address?.lines?.joined(separator: ",")
                    let locationLat = NSNumber(value:address?.coordinate.latitude ?? 0.0)
                    let locationLon = NSNumber(value:address?.coordinate.longitude ?? 0.0)
                    UserDefaults.standard.set(["lat": locationLat, "lon": locationLon], forKey:"locationFromSignUp")
                    UserDefaults.standard.set(weakSelf?.lblAddress.text, forKey: "addressSignUp")
                    
                }
            }
            
        })
    }
    
    
    @IBAction func btnCamera(_ sender: Any) {
        
        weak var weakSelf = self
        let actionSheet:UIAlertController = UIAlertController.init(title: "Select".localized(), message: "", preferredStyle: .actionSheet)
        actionSheet.view.tintColor = UIColor.btnLinkColor
        
        actionSheet.addAction(UIAlertAction.init(title: "Camera".localized(), style: .default, handler: {action in
            weakSelf?.checkForCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Photo Library".localized(), style: .default, handler: {action in
            weakSelf?.checkForPhotos()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel".localized(), style: .default, handler: {action in
            
        }))
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            present(actionSheet, animated: true, completion: nil)
            break
            
        case .pad:
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            present(actionSheet, animated: true, completion: nil)
            break
            
        case .unspecified:
            break
        case .tv:
            break
        case .carPlay:
            break
        }
        //present(actionSheet, animated: true, completion: nil)
    }
    
    
    func checkForCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            openCamera()
        }
        else
        {
            let actionController: UIAlertController = UIAlertController(title: "Camera is not available".localized(),message: "", preferredStyle: .alert)
            actionController.view.tintColor = UIColor.btnLinkColor
            let cancelAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .cancel) { action -> Void  in
            }
            actionController.addAction(cancelAction)
            self.present(actionController, animated: true, completion: nil)
        }
    }
    func checkForPhotos(){
        if(UIImagePickerController.isSourceTypeAvailable(.photoLibrary))
        {
            openPhotLibrary()
        }
        else
        {
            let actionController: UIAlertController = UIAlertController(title: "Photos are not available".localized(),message: "", preferredStyle: .alert)
            
            actionController.view.tintColor = UIColor.btnLinkColor
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .cancel) { action -> Void  in
            }
            actionController.addAction(cancelAction)
            self.present(actionController, animated: true, completion: nil)
        }
    }
    
    
    func  openCamera() {
        weak var weakSelf = self
        checkCameraPermisson(completionHander: {status in
            if status{
                weakSelf?.openImagePIcker(type: 0)
            }
        })
    }
    
    func openPhotLibrary() {
        weak var weakSelf = self
        checkPhotoLibraryPermission(completionHander: {status in
            if status{
                weakSelf?.openImagePIcker(type: 1)
            }
        })
    }
    
    
    func openImagePIcker(type:Int) {
        imagePicker = UIImagePickerController.init()
        imagePicker.sourceType = type == 1 ? .photoLibrary : .camera
        //imagePicker.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        if navigationController is UIImagePickerController {
            navigationController.navigationBar.barTintColor = UIColor.primaryColor
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let tempImage:UIImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            imgViewProfilePic.image = tempImage
            uploadImage()
        }
        imagePicker.dismiss(animated: true, completion: nil)
        
    }
    
    
    func uploadImage() {
        
        isUploadingProceess = true
        // updateNextButton()
        spinnerView.startAnimating()
        let compressedImage = imgViewProfilePic.image?.jpeg(.lowest)
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.uploadFile(endUrl: ("\(ApiBaseUrl)service_attachment?lngId=\(Localize.currentLanguageID)"), imageData: compressedImage, withName: "attachment", fileName: "image.JPG", mimeType: "image/JPG", parameters: nil, completionHandler: {response, jsonObj in
            weakSelf?.isUploadingProceess = false
            // updateNextButton()
            weakSelf?.spinnerView.stopAnimating()
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                if let dataObj = jsonObj?.dataObj(){
                    if let urlString:String = dataObj.object(forKey: "a_url") as? String{
                        weakSelf?.profilePicUrl = urlString
                        
                        return
                    }
                }
            }
            weakSelf?.imgViewProfilePic.image = UIImage.init(named: "profilePicIcon")
            weakSelf?.showErrorMessage(message: message)
        })
    }
    
    
    @IBAction func callNextScreen(_ sender: Any) {
        
        let count =  txtPassword.text?.count ?? 0
        
        if txtFirstName.text?.count==0 {
            txtFirstName.showError(message: "Required".localized())
            //showWarningMessage(message: "Please enter name.".localized())
            return
        }
        
        if txtEmail.text?.count ?? 0 > 0 && !isValidEmail(Str: txtEmail.text ?? ""){
            txtEmail.showError(message: "Please enter a valid email address.".localized())
            return
        }
        
        if txtMobileNo.text?.count ?? 0 == 0 {
            txtMobileNo.showError(message: "Required".localized())
            //showWarningMessage(message: "Please enter name.".localized())
            return
        }
        if txtMobileNo.text?.count ?? 0 < 5 {
            
            txtMobileNo.showError(message: "Should be at least 5 characters".localized())
            
            return
        }
        
        if navigationTitle == "Partner" && lblAddress.text == "Fetching Location...".localized()
        {
            showWarningMessage(message: "Pleaes Select Address".localized())
            
            return
        }
        
        //        if (selectedCountryCode?.digits ?? 0 > 0 && txtMobileNo.text?.count ?? 0 != selectedCountryCode?.digits ) {
        //
        //            showWarningMessage(message: "Pleaes enter a valid mobile number.".localized())
        //
        //            return
        //        }
        
        if count < 6  {
            
            txtPassword.showError(message:"6 to 24 characters and at least one numeric digit".localized())
            
            return
        }
        if count > 24  {
            
            txtPassword.showError(message:"6 to 24 characters and at least one numeric digit".localized())
            
            return
        }
        if ( (count >= 6 && count <= 24) && !isValidated(txtPassword.text ?? "") ) {
            
            txtPassword.showError(message:"6 to 24 characters and at least one numeric digit".localized())
            
            return
        }
        if txtConfirmPassword.text?.count ?? 0 == 0 {
            
            txtConfirmPassword.showError(message: "Required".localized())
            
            return
        }
        
        if txtPassword.text != txtConfirmPassword.text {
            
            showErrorMessage(message: "Passwords do not match.".localized())
            
            return
        }
        
        requestOTP(isdCode: txtCountryCode.text ?? "", mobileNo: txtMobileNo.text ?? "", mailId: txtEmail.text ?? "")
        
        btnNext.isHidden = true
        spinner.isHidden = false
        spinner.startAnimating()
        //        txtMobileNo.isEnabled = false
        //        txtCountryCode.isEnabled = false
        txtMobileNo.resignFirstResponder()
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtPassword.resignFirstResponder()
        txtConfirmPassword.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtEmail.resignFirstResponder()
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "OTPSegue" {
            let otpScreen:SignUpOTPVerifyViewController = segue.destination as! SignUpOTPVerifyViewController
            otpScreen.isdCode = txtCountryCode.text!
            otpScreen.mobileNo = txtMobileNo.text!
            otpScreen.isForgotPassword = isForgotPassword
            otpScreen.firstName = txtFirstName.text ?? ""
            otpScreen.lastName = txtLastName.text ?? ""
            otpScreen.password = txtPassword.text ?? ""
            otpScreen.emailId = txtEmail.text ?? ""
            otpScreen.urlProfilePic = profilePicUrl
            otpScreen.navigationTitle = titleName
        }
        
    }
    
    
    func requestOTP(isdCode:String, mobileNo:String, mailId:String) {
        let params:NSDictionary = [
            "mobileIsd":isdCode,
            "mobileNumber":mobileNo,
            "isSignUp":1,
            "emailId":mailId,
            "existingUser":isExistingUser ?? 0,
            "isBuyer":UserDefaults.standard.integer(forKey: "isBuyer")
        ]
        //  var isOTPRequired:Int?
        UserDefaults.standard.set(isExistingUser, forKey: "existingUser")
        weak var weakSelf = self
        let apiClient:APIClient = APIClient()
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/sendOtp?lngId=\(Localize.currentLanguageID)"), params: params as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                if let dataObj:NSDictionary = jsonObj?.dataObj(){
                    
                    if let alertMessageData:NSDictionary = dataObj.value(forKey: "alertMessageData") as? NSDictionary {
                        
                        weakSelf?.isExistingUser = alertMessageData.value(forKey: "existingUser") as? Int
                        
                        UserDefaults.standard.set(weakSelf?.isExistingUser, forKey: "existingUser")
                        
                        if  weakSelf?.isExistingUser == 1{
                            
                            self.msg = alertMessageData.value(forKey: "displayText") as? String ?? ""
                            
                            weakSelf?.openBecomePartnerOrUser(withMessage: self.msg )
                            
                            weakSelf?.spinner.stopAnimating()
                            weakSelf?.spinner.isHidden = true
                            
                            weakSelf?.btnNext.isHidden = false
                            return
                        }
                    }
                    
                    if (self.isNoTapped) {
                    
                        weakSelf?.openBecomePartnerOrUser(withMessage: self.msg )
                        
                        weakSelf?.spinner.stopAnimating()
                        weakSelf?.spinner.isHidden = true
                        
                        weakSelf?.btnNext.isHidden = false
                        
                        return
                    }
                    
                    weakSelf?.openOtpScreen()
                    
                    //      isOTPRequired = dataObj.value(forKey: "isOtpRequired") as? Int
                    //                   if isOTPRequired == 1{
                    // weakSelf?.openOtpScreen()
                    //       }
                    
                    weakSelf?.spinner.stopAnimating()
                    weakSelf?.spinner.isHidden = true
                }
                
            }
            else{
                weakSelf?.spinner.stopAnimating()
                weakSelf?.spinner.isHidden = true
                
                //weakSelf?.showErrorMessage(message: message)
                weakSelf?.showErrorMessage(message: message)
                
            }
            weakSelf?.btnNext.isHidden = false
            weakSelf?.spinner.isHidden = true
            //           weakSelf?.spinner.stopAnimating()
            weakSelf?.txtMobileNo.isEnabled = true
            weakSelf?.txtCountryCode.isEnabled = true
            
        })
    }
    
}
