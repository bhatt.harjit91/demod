//
//  ProfileViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//


import UIKit
import SDWebImage

class ProfileViewController: AppBaseViewController {
    
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationItem.title = "Settings".localized()
        
        // set the shadow properties
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowRadius = 4.0

    }
    
    
    
}

///FooterCell
//HeaderCell


enum ProfileSection:Int {
    case ProfileViewIndex = 0
    case SignInViewIndex = 1
}

enum MenuSection:Int {
    case TopHeaderViewIndex = 0
    case EditProfileIndex
//    case HelpIndex
    case ChangePasswordIndex
    case ChangeLanguageIndex
    case AboutUsIndex
    case SignOutIndex
    case FooterViewIndex
}
class ProfileTableViewController: UITableViewController {
    
   
    @IBOutlet weak var languageCell: UITableViewCell!
    @IBOutlet weak var signOutCell: UITableViewCell!
    
    
    @IBOutlet weak var lblEditProfile: NKLabel!
   
    @IBOutlet weak var lblChangePassword: NKLabel!
   
    
   
    @IBOutlet weak var lblChangeLanguage: NKLabel!
    @IBOutlet weak var lblSignOut: NKLabel!
    
   
    
//    @IBOutlet weak var lblHelpAndSupport: NKLabel!
    
    
    @IBOutlet weak var lblAboutUs: NKLabel!
    
    
    @IBOutlet weak var btnTermsOfUse: NKButton!
    
    @IBOutlet weak var btnPrivacyPolicy: NKButton!
    
    var memberShipStatusObj:MemberShipStatusObj?
    
    var membershipId:String?
    
   
    @IBOutlet weak var lblMembershipId: NKLabel!
    
    
    @IBOutlet weak var imgViewMembership: UIImageView!
    
    @IBOutlet weak var lblMemberName: NKLabel!
    
    @IBOutlet weak var lblBillingDate: NKLabel!
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.darkGray]
    
    let mainAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.black]
    
     var loaderView:LoadingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view.
        
       
        
        lblEditProfile.text = "Edit Profile".localized()
        lblChangeLanguage.text = "Change Language".localized()
        lblChangePassword.text = "Change Password".localized()
        lblSignOut.text = "Sign Out".localized()
        
//        lblHelpAndSupport.text = "Help & Support".localized()
        lblAboutUs.text = "About_Us".localized()
        
        btnTermsOfUse.setTitle("Terms of use".localized(), for: .normal)
        btnPrivacyPolicy.setTitle("Privacy Policy".localized(), for: .normal)
        
        
        tableView.reloadData()
        
         navigationItem.title = "Settings".localized()
        
        if checkLogin() {
            if let userDetails:userModel = getUserDetail(){
                lblMemberName.text = userDetails.displayName ?? ""
            }
        }
        if memberShipStatusObj != nil {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy"
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: memberShipStatusObj?.expiryDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            let datelblText = NSMutableAttributedString(string:"\("Next Billing Date  ")",
                attributes: yourAttributes)
            
            let finaldateLblText = NSMutableAttributedString(string:"\(dateText)",
                attributes: mainAttributes)
            
            let combination = NSMutableAttributedString()
            
            combination.append(datelblText)
            combination.append(finaldateLblText)
            
            lblBillingDate.attributedText = combination
            
            if memberShipStatusObj?.memberShipType == 1 {
                
                imgViewMembership.image = UIImage.init(imageLiteralResourceName: "basicMedalIcon")
            }
            if memberShipStatusObj?.memberShipType == 2 {
                
                imgViewMembership.image = UIImage.init(imageLiteralResourceName: "silverMedalIcon")
            }
            if memberShipStatusObj?.memberShipType == 3 {
                
                imgViewMembership.image = UIImage.init(imageLiteralResourceName: "goldMedalIcon")
            }
        }
        
        if membershipId?.count ?? 0 > 0 {
            
            lblMembershipId.text = membershipId ?? ""
        }
        
        //isServiceManager = UserDefaults.standard.integer(forKey: "isServiceManager")
        ////print("\("Service Manager:::::::")\(isServiceManager)")
    }
    
    @IBAction func openTerms(_ sender: Any) {
        
        openWebView(urlString: "\("terms_")\(Localize.currentLanguage)\(".html")", title: "Terms of use".localized(), subTitle: "")
        
    }
    
    @IBAction func openPrivacy(_ sender: Any) {
        
         openWebView(urlString: "\("privacy_")\(Localize.currentLanguage)\(".html")", title: "Privacy Policy".localized(), subTitle: "")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        if !checkLogin(){
                                 
            openSignInPage()
                          
        }
    
        
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            switch indexPath.row {
            case ProfileSection.ProfileViewIndex.rawValue:
                if checkLogin(){
                    
                    if membershipId?.count ?? 0 > 0 {
                    
                    return tableView.frame.size.height/3 > 250 ? tableView.frame.size.height/3 : 265
                    
                   }
                  else {
                    
                       return 0
                    
                   }
                }
                else {
                    return 0
                }
            default:
                 return 0
            }
        }
        
        if indexPath.section == 1 {
            switch indexPath.row{
            case MenuSection.EditProfileIndex.rawValue:
                return checkLogin() ? 60 : 0
//            case MenuSection.HelpIndex.rawValue:
//                return 60
            case MenuSection.ChangePasswordIndex.rawValue:
                return checkLogin() ? 60 : 0
            case MenuSection.ChangeLanguageIndex.rawValue:
                return 60
            case MenuSection.AboutUsIndex.rawValue:
                return 60
            case MenuSection.SignOutIndex.rawValue:
                return checkLogin() ? 60 : 0
            case MenuSection.FooterViewIndex.rawValue:
                return 20
            default:
                return 0
            }
        }
        else if indexPath.section == 2 {
            
            return checkLogin() ? 30 : 0
        }
        return 0
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == MenuSection.ChangeLanguageIndex.rawValue && indexPath.section == 1 {
            openLanguageSelector()
        }
        else if indexPath.row == MenuSection.SignOutIndex.rawValue && indexPath.section == 1 {
            let alert = UIAlertController.init(title: "Are you sure you want to logout?".localized(), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "No".localized(), style: .cancel, handler: { (action) in
                
            }))
            alert.addAction(UIAlertAction.init(title: "Yes".localized(), style: .destructive, handler: { (action) in
                self.signOut()
            }))
            present(alert, animated: true, completion: nil)
            
        }
        else if indexPath.row == MenuSection.ChangePasswordIndex.rawValue && indexPath.section == 1 {
            performSegue(withIdentifier: "ChangePasswordSegue", sender: nil)
        }
        else if indexPath.row == MenuSection.EditProfileIndex.rawValue && indexPath.section == 1 {
            performSegue(withIdentifier: "EditProfileSegue", sender: nil)
        }
        else if indexPath.row == MenuSection.AboutUsIndex.rawValue && indexPath.section == 1 {
            openWebView(urlString: AboutusUrl, title: "About Us".localized(), subTitle: "")
        }
//        else if indexPath.row == MenuSection.HelpIndex.rawValue && indexPath.section == 1 {
//            openWebView(urlString: HelpUrl, title: "Help & Support".localized(), subTitle: "")
//        }
        
    }
    
    
    func openLanguageSelector() {
        let sBoard = UIStoryboard.init(name: LanguageSelectorSB, bundle: nil)
        
        present(sBoard.instantiateInitialViewController()!, animated: true, completion: nil)
    }
    
    
    @IBAction func callSignIn(_ sender: Any) {
        
        if checkLogin(){
            
        }
        else{
            
            openSignInPage()
        }
        
        
    }
    
    func openHomePage() {
        let HomeStoryBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
        let homePage:UINavigationController = HomeStoryBoard.instantiateInitialViewController() as! UINavigationController
      
           view.window?.rootViewController = homePage
    }

    
    func signOut() {
       
//        tableView.reloadData()
      
       // let param:NSDictionary  = [:]
        
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/logout?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: nil, completionHandler:{response, jsonObj in
            if let selfObj = weakSelf {
                selfObj.clearUserDetails()
               selfObj.openSignInPage() 
            }
            
        })
        
        self.tableView.reloadData()
    }
}
