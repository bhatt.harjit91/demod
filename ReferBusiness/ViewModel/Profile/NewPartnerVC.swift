//
//  NewPartnerVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class NewPartnerVC: AppBaseViewController {

   @IBOutlet weak var segmentStatus: UISegmentedControl!
    
   @IBOutlet weak var tableViewList: UITableView!
    
   var sellerRequestList:[NewSellerUserModel]?
    
   var count:Int?
    
    var isFirstTime:Bool = true
    
    var status:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        segmentStatus.setSegmentStyle()
        
       navigationItem.title = "New Business Oppurtunity".localized()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
         segmentStatus.setSegmentStyle()
        
        fetchNewSellerList(withStatus: status)
    }
    

    @IBAction func segmentStatusAction(_ sender: UISegmentedControl) {
        
        segmentStatus.setSegmentStyle()
        
        switch sender.selectedSegmentIndex {
            
        case 0:
            status = 0
            break
        case 1:
            status = 1
             break
        case 2:
            status = 2
               break
            
        default:
            print("No Data")
        }
        
        
         fetchNewSellerList(withStatus: status)
    }
    
    
    func fetchNewSellerList(withStatus:Int) {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        
        if isFirstTime {
           startAnimating()
           isFirstTime = false
        }
            
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getNewSellerRequests?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&status=\(withStatus)&requestId=\(0)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(NewSellerUserApiModel.self, from: data)
                                
                                if let apiResponse:[NewSellerUserModel] = jsonResponse.sellerRequestList {
                                    
                                    weakSelf?.sellerRequestList = apiResponse
                                    
                                }
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.count = apiResponse
                                    
                                }
                                
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    

}


extension NewPartnerVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = sellerRequestList?.count ?? 0
        
        if count > 0 {
            
            tableViewList.alpha = 1
            
            return sellerRequestList?.count ?? 0
            
        }
            
        else{
            
            tableViewList.alpha = 0
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:NewPartnerListCell = tableView.dequeueReusableCell(withIdentifier: "NewPartnerListCell", for: indexPath) as? NewPartnerListCell
        {
            
            return getCellForNewUserList(cell: cell, indexPath: indexPath)
            
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:NewSellerUserModel = sellerRequestList?[indexPath.row]
        {
            let storyBoard = UIStoryboard.init(name: ProfilePageSB, bundle: nil)
            if let newSellerPage:NewPartnerDetailVC = storyBoard.instantiateViewController(withIdentifier: "NewPartnerDetailVC") as? NewPartnerDetailVC  {
                
                newSellerPage.requestId = model.requestId ?? 0
                newSellerPage.status = model.status ?? 0
                
                self.navigationController?.pushViewController(newSellerPage, animated: true)
            }
        }
        
        
    }
    
    func getCellForNewUserList(cell:NewPartnerListCell,indexPath:IndexPath) -> NewPartnerListCell {
        
        cell.selectionStyle = .none
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
        dateFormatterPrint.timeZone = .current
        
        
        
        if let model:NewSellerUserModel = sellerRequestList?[indexPath.row]
        {
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.crDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblTitle.text = "\("Request from".localized())\(" ")\(model.createdBy ?? "")"

          //  cell.lblStatus.text = model.status

            if model.status == 0 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString:"#FFC000")
                
                cell.lblStatus.text = "Pending".localized()
            }

            if model.status == 1 {
                
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString:"#92D050")
                
                cell.lblStatus.text = "Closed".localized()
            }

          
            cell.lblDes.text = model.description ?? ""
            
            cell.lblDate.text = dateText
            
        }
        
        
        return cell
    }
    
}

class NewPartnerListCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var viewStatus: RCView!
    
    @IBOutlet weak var lblStatus: NKLabel!
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var lblDes: NKLabel!
    
    @IBOutlet weak var lblDate: NKLabel!
    
}
