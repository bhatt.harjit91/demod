//
//  PartnerListViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 12/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class PartnerListViewController: AppBaseViewController {

    var partnerFeatures:[BuyerFeatures]?
    
    @IBOutlet weak var btnNext: NKButton!
    @IBOutlet weak var tblPartnerList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Become a Partner".localized()
        
        let btnName = UIButton()
        //  btnName.setImage(UIImage(named: "walletIcon"), for: .normal)
        btnName.setTitle("Next -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(btnNext(_:)), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        btnNext.setTitle("Next".localized(), for: .normal)
        
        fetchSelectedUserDetails()
    }
    
    

    func fetchSelectedUserDetails() {
        
        startAnimating()
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/signUpAsDescription?lngId=\(Localize.currentLanguageID)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataObj:NSDictionary = jsonObj?.dataObj() {
                        do {
                            
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
                                                        
                            let apiResponse = try decoder.decode(ChooseSignUpClass.self, from: data)
                            
                            if let apiResponse:[BuyerFeatures] = apiResponse.partnerFeatures {
                                weakSelf?.partnerFeatures = apiResponse
                                
                            }
                            
                            DispatchQueue.main.async {
                                weakSelf?.tblPartnerList.reloadData()
                            }
                            
                            return
                            
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                        }
                    }
                    
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    @IBAction func btnNext(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: ProfilePageSB, bundle: nil)
        let profileSignUp:BecomePartner = storyBoard.instantiateViewController(withIdentifier: "BecomePartner") as! BecomePartner

        navigationController?.pushViewController(profileSignUp, animated: true)
    }
    
}

class PartnerCell: UITableViewCell {
    
    @IBOutlet weak var lblTitleText: NKLabel!
    
    @IBOutlet weak var lblDescription: NKLabel!
    
    @IBOutlet weak var lblCount: NKLabel!
    
}

extension PartnerListViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return partnerFeatures?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let tableViewCell:PartnerCell = tableView.dequeueReusableCell(withIdentifier: "PartnerCell", for: indexPath) as? PartnerCell
        {
            return getCellForSelectedUserInfo(cell: tableViewCell, indexPath: indexPath)
        }
        return UITableViewCell.init()
    }
    
    func getCellForSelectedUserInfo(cell:PartnerCell,indexPath: IndexPath) -> PartnerCell {
        
        cell.selectionStyle = .none

        if let model:BuyerFeatures = partnerFeatures?[indexPath.row] {
            
            cell.lblTitleText.text = model.title
            
            cell.lblDescription.text = model.description
            
            let count = indexPath.row + 1
            
            cell.lblCount.text = "\(count)"
        }
        
        return cell
    }
    
}
