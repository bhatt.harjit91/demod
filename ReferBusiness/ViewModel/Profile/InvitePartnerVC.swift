//
//  InvitePartnerVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 24/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import LinkedinSwift


private let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81awlk7k324wi3", clientSecret: "Lu1wQu7MijC5gaSs", state: "DJF46ikMMDLKDJF46ikMMZADfdfds", permissions: ["r_liteprofile", "r_emailaddress","w_member_social"], redirectUrl: "https://com.demoapp.linkedin.oauth/oauth"))


class InvitePartnerVC: AppBaseViewController,UITextViewDelegate {
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var viewBgShare: UIView!
    @IBOutlet weak var txtViewShare: NKTextView!
    @IBOutlet weak var btnCopyText: UIButton!
    
    @IBOutlet weak var btnShare: UIButton!
    
    @IBOutlet weak var lblCopyTitle: NKLabel!
    
    @IBOutlet weak var lblShareTitle: NKLabel!
    
    @IBOutlet weak var imgViewCopy: UIImageView!
    
    @IBOutlet weak var imgViewShare: UIImageView!
    
    @IBOutlet weak var lblNotes: NKLabel!
    
    @IBOutlet weak var viewButtons: UIView!
    @IBOutlet weak var lblShareLinkedIn: NKLabel!
    
    @IBOutlet weak var tvShare: NKTextView!
    var smSharingText:String?
    var smSharingImage:String?
    var getId:String = String()
    var tokenStore:String = String()
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 4),
        .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBgShare.isHidden = true
        
        txtViewShare.isEditable = false
        txtViewShare.dataDetectorTypes = .all
        
        tvShare.isEditable = true
        tvShare.dataDetectorTypes = .all
        
        navigationItem.title = "Invite Partners".localized()
        
        lblTitle.text = "Copy the text below and share to your friends".localized()
        
        lblShareTitle.text = "Share".localized()
        lblShareLinkedIn.text = "Share".localized()
        
        lblCopyTitle.text = "Copy".localized()
        
        imgViewCopy.setImageColor(color: .btnLinkColor)
        imgViewShare.setImageColor(color: .btnLinkColor)
        
        viewButtons.layer.cornerRadius = 5
        viewButtons.layer.borderWidth = 1.0
        viewButtons.layer.borderColor = UIColor.lightGray.cgColor
        viewButtons.layer.shadowColor = UIColor.black.cgColor
        viewButtons.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewButtons.layer.shadowOpacity = 0.3
        viewButtons.layer.shadowRadius = 4.0
        
        lblNotes.text = "Note: Tap on Copy Text button to copy the text . Tap on Share icon to share Invite Partner message to Messengers or Social Media apps installed on your phone.".localized()
        
        fetchData()
        
    }
    
    @IBAction func btnShareLinkedIn(_ sender: Any) {
        
        linkedinHelper.authorizeSuccess({ (token) in
            
            
            self.tokenStore = linkedinHelper.lsAccessToken?.accessToken ?? ""
            
            linkedinHelper.requestURL("https://api.linkedin.com/v2/me", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
                
                let json = response.jsonObject
                if let dictionaryJson = json as NSDictionary?{
                    self.getId = dictionaryJson["id"] as! String
                    
                    let s = self.smSharingText?.html2AttributedString
                    let text = s?.string
                    self.tvShare.text = text ?? ""
                    
                    self.viewBgShare.isHidden = false
                    
                }
            }) {(error) -> Void in
                
                DispatchQueue.main.async
                    {
                        self.stopAnimating()
                }
                print(error.localizedDescription)
            }
        }, error: { (error) in
            DispatchQueue.main.async
                {
                    self.stopAnimating()
            }
        }) {
            DispatchQueue.main.async
                {
                    self.stopAnimating()
            }
        }
    }
    func adjustTextViewHeight(arg : NKTextView)
    {
        
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = true
    }
    
    func updateUI(){
        
        txtViewShare.attributedText = smSharingText?.html2AttributedString
        
        adjustTextViewHeight(arg: txtViewShare)
        //  lblShare.text = smSharingText
        
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        UIApplication.shared.open(URL)
        
        return false
    }
    
    @IBAction func btnCopyTextAction(_ sender: Any) {
        
        UIPasteboard.general.string = txtViewShare.text ?? ""
        
        showInfoMessage(message: "Text copied successfully".localized())
        
    }
    
    @IBAction func btnShareAction(_ sender: UIButton) {
        
        let s = smSharingText?.html2AttributedString
        
        let text = s?.string
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view //
        
        
        //  activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        activityViewController.modalPresentationStyle = .fullScreen
        
        present(activityViewController, animated: true, completion: nil)
        
    }
    
    
    func geturl() {
        
        let urll:String? = "urn:li:person:\(getId)"
        
        if urll != nil {
            
            let text = tvShare.text
            
            let targetURLString = "https://api.linkedin.com/v2/ugcPosts"
            let payloadStr: String = "{\"author\":\"urn:li:person:\(getId)\",\"lifecycleState\":\"PUBLISHED\",\"specificContent\":{\"com.linkedin.ugc.ShareContent\":{\"shareCommentary\":{\"text\":\"\(text ?? "")\"},\"shareMediaCategory\":\"NONE\"}},\"visibility\":{\"com.linkedin.ugc.MemberNetworkVisibility\":\"PUBLIC\"}}"
            
            let request = NSMutableURLRequest(url: NSURL(string: targetURLString)! as URL)
            
            request.httpMethod = "POST"
            request.httpBody = payloadStr.data(using: String.Encoding.utf8)
            request.addValue("Bearer\(" ")\(tokenStore)", forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("2.0.0", forHTTPHeaderField: "X-Restli-Protocol-Version")
            let task: URLSessionDataTask = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                
                if statusCode == 201 {
                    
                    guard ((try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any]) != nil else {
                        print("Not containing JSON")
                        return
                    }
                }
                
                DispatchQueue.main.async
                    {
                        self.viewBgShare.isHidden = true
                        self.stopAnimating()
                        
                        if statusCode == 201 || statusCode == 200
                        {
                            self.showErrorMessage(message: "successfully posted.".localized())
                        }
                        else
                        {
                            self.showErrorMessage(message: error?.localizedDescription ?? "")
                        }
                        
                }
            }
            task.resume()
            
        }
    }
    
    
    func fetchData() {
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        
        startAnimating()
        
        
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/invitePartnerSMContent?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(InvitePartnerApiModel.self, from: data)
                                
                                if let apiResponse:String = jsonResponse.smSharingText {
                                    
                                    
                                    weakSelf?.smSharingText = apiResponse
                                    
                                }
                                
                                if let apiResponse:String = jsonResponse.smSharingImage {
                                    
                                    weakSelf?.smSharingImage = apiResponse
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
    @IBAction func btnCancel(_ sender: Any) {
        
        viewBgShare.isHidden = true
    }
    
    @IBAction func btnShare(_ sender: Any) {
        
        DispatchQueue.main.async
            {
                self.startAnimatingAfterSubmit()
        }
        
        self.geturl()
    }
}
