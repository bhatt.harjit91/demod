//
//  BecomePartner.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class BecomePartner: AppBaseViewController, UITextFieldDelegate, UINavigationControllerDelegate,CountryCodeSelectViewControllerDelegate,UIImagePickerControllerDelegate,LocationPermissionVCNewDelegate,CLLocationManagerDelegate,SignUpOTPVerifyViewControllerDelegate {
    
    @IBOutlet weak var imgViewCamera: UIImageView!
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    @IBOutlet weak var imgViewProfilePic: UIImageView!
    @IBOutlet weak var txtFirstName: RCTextField!
    @IBOutlet weak var txtLastName: RCTextField!
    @IBOutlet weak var txtEmail: RCTextField!
    @IBOutlet weak var btnNext: NKButton!
    @IBOutlet weak var imgViewLocation: UIImageView!
    @IBOutlet weak var lblAddress: NKLabel!
    @IBOutlet weak var txtCountryCode: RCTextField!
    @IBOutlet weak var txtMobileNo: RCTextField!
    
    
    var imagePicker: UIImagePickerController!
    
    var isUploadingProceess = false
    var otp:String = ""
    var profilePicUrl:String = ""
    var titleName:String = ""
    
    var isForgotPassword:Bool = false
    var selectedCountryCode : CountryCodeModel?
    
    var currentLoc:CLLocationCoordinate2D?
    
    var locationManager = CLLocationManager()
    
    var locationLat:Double = 0.0
    var locationLon:Double = 0.0
    var addressOfPartner:String = ""
    
    
    override func viewDidAppear(_ animated: Bool) {
        updateLocationName()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let btnName = UIButton()
        btnName.setTitle("Finish -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(callNextScreen(_:)), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        
        if let userDetails:userModel = getUserDetail(){

            txtFirstName.text = "\(userDetails.firstName ?? "")"
            txtLastName.text = "\(userDetails.lastName ?? "")"
            txtEmail.text = "\(userDetails.emailId ?? "")"
            txtMobileNo.text = "\(userDetails.mobileNumber ?? "")"
            txtCountryCode.text = "\(userDetails.mobileIsd ?? "")"
            
            if userDetails.profilePicture?.count ?? 0 > 0 {
                imgViewProfilePic.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(userDetails.profilePicture ?? "")")), completed: nil)
            }
            
        }
        
        imgViewLocation.setImageColor(color: .white)
        
        
        getCurrentLocation()
        
        lblAddress.text = "Fetching Location...".localized()
        
        imgViewCamera.setImageColor(color: UIColor.white)
        
        txtFirstName.delegate = self
        txtMobileNo.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        
        navigationItem.title = "Become a Partner".localized()

        txtFirstName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "First Name".localized())
        
        txtMobileNo.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Mobile Number".localized())
        
        txtEmail.placeholder = "Email ID".localized()
        btnNext.setTitle("Finish".localized(), for: .normal)
        
        txtCountryCode.delegate = self
        
    }
    
    
    override func addPlaceHolderWithText(placeholder:String) -> NSMutableAttributedString
    {
        let starAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.fontForType(textType: 6),
            .foregroundColor: UIColor.getGradientColor(index: 1).first ?? 0]
        
        let star = NSAttributedString(string:"*" , attributes: starAttributes)
        
        let starTexr = NSMutableAttributedString(string: placeholder)
        
        starTexr.append(star)
        
        return starTexr
    }
    
    @IBAction func btnLocation(_ sender: Any) {
        
        openMap()
    }
    
    func openMap() {
        
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        locationPage.lat = currentLoc?.latitude ?? 0.0
        locationPage.lng = currentLoc?.longitude ?? 0.0
        locationPage.modalPresentationStyle = .fullScreen
        navigationController?.present(locationPage, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField ==  txtFirstName {
            let nsString = textField.text as NSString?
            let numbString = nsString?.replacingCharacters(in: range, with: string) ?? ""
            if numbString.count>0{
                txtFirstName.showError(message: "")
            }
            let maxLength = 50
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                    
            return newString.length <= maxLength
        }
            
        else if textField ==  txtLastName {
               
                let maxLength = 50
                    let currentString: NSString = textField.text! as NSString
                    let newString: NSString =
                        currentString.replacingCharacters(in: range, with: string) as NSString
                        
                return newString.length <= maxLength
            }
            
        else if textField ==  txtEmail {
            let nsString = textField.text as NSString?
            let numbString = nsString?.replacingCharacters(in: range, with: string) ?? ""
            if numbString.count>0{
                if  isValidEmail(Str: numbString){
                    txtEmail.showError(message: "")
                }
            }
        }
      else if textField ==  txtMobileNo {
            let nsString = textField.text as NSString?
            let numbString = nsString?.replacingCharacters(in: range, with: string) ?? ""
            if numbString.count>0{
                txtMobileNo.showError(message: "")
            }
            let maxLength = 14
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                    
            return newString.length <= maxLength
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField ==  txtCountryCode {
            openCountrySelector()
            return false;
        }
        return true
    }
    
    
    func openCountrySelector() {
                let storyBoard = UIStoryboard.init(name: CountryCodeSB, bundle: nil)
                let countrySelector:CountryCodeSelectViewController = storyBoard.instantiateViewController(withIdentifier: "CountryCodeSelectViewController") as! CountryCodeSelectViewController
                countrySelector.selectedCountryCode = selectedCountryCode
                countrySelector.delegate = self
                navigationController?.pushViewController(countrySelector, animated: true)
    }
    
    func didFinishSelectingCountryCode(selectedCountry: CountryCodeModel) {
        selectedCountryCode = selectedCountry
        txtCountryCode.text = selectedCountry.dial_code
    }
    
//    func openOtpScreen() {
//        //OTPSegue
//        performSegue(withIdentifier: "OTPSegue", sender:nil)
//    }

    
    func getCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch locationAuthorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.startUpdatingLocation()
                currentLoc = locationManager.location?.coordinate
                
                updateLocationName()
            }
        case .restricted, .denied:
            
           // showLocationRequiredPage()
            
            print("Access Denied")
            
        }
    }
    
    func showLocationRequiredPage(){
        weak var weakSelf = self
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable location access to provide you near by search.".localized(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Enable Location Service".localized(), style: .default, handler: {action in
            
            weakSelf?.openAppSettings()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: Location manager
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
            
            currentLoc = locValue
            
            updateLocationName()
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
       // stopAnimating()
    }
    
    func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        currentLoc = coordinate
        updateLocationName()
    }
    
    func updateLocationName() {
        let geoCoder = GMSGeocoder()
        
        weak var weakSelf = self
        
        geoCoder.reverseGeocodeCoordinate(currentLoc ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), completionHandler: { response, error in
            
            if response != nil {
                let address = response?.results()?.first
                if address != nil {
                    
                    weakSelf?.lblAddress.text = address?.lines?.joined(separator: ",")
                    let locationLatPartner = NSNumber(value:address?.coordinate.latitude ?? 0.0).doubleValue
                    let locationLonPartner = NSNumber(value:address?.coordinate.longitude ?? 0.0).doubleValue

                    weakSelf?.locationLat = locationLatPartner
                    weakSelf?.locationLon = locationLonPartner
                    weakSelf?.addressOfPartner = weakSelf?.lblAddress.text ?? ""
                }
            }
            
        })
    }
    

    @IBAction func btnCamera(_ sender: Any) {
        
        weak var weakSelf = self
        let actionSheet:UIAlertController = UIAlertController.init(title: "Select".localized(), message: "", preferredStyle: .actionSheet)
        actionSheet.view.tintColor = UIColor.btnLinkColor
        
        actionSheet.addAction(UIAlertAction.init(title: "Camera".localized(), style: .default, handler: {action in
            weakSelf?.checkForCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Photo Library".localized(), style: .default, handler: {action in
            weakSelf?.checkForPhotos()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel".localized(), style: .default, handler: {action in
            
        }))
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            present(actionSheet, animated: true, completion: nil)
            break
            
        case .pad:
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            present(actionSheet, animated: true, completion: nil)
            break
            
        case .unspecified:
            break
        case .tv:
            break
        case .carPlay:
            break
        }
        //present(actionSheet, animated: true, completion: nil)
    }
    
    
    func checkForCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            openCamera()
        }
        else
        {
            let actionController: UIAlertController = UIAlertController(title: "Camera is not available".localized(),message: "", preferredStyle: .alert)
            actionController.view.tintColor = UIColor.btnLinkColor
            let cancelAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .cancel) { action -> Void  in
            }
            actionController.addAction(cancelAction)
            self.present(actionController, animated: true, completion: nil)
        }
    }
    func checkForPhotos(){
        if(UIImagePickerController.isSourceTypeAvailable(.photoLibrary))
        {
            openPhotLibrary()
        }
        else
        {
            let actionController: UIAlertController = UIAlertController(title: "Photos are not available".localized(),message: "", preferredStyle: .alert)
            
            actionController.view.tintColor = UIColor.btnLinkColor
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .cancel) { action -> Void  in
            }
            actionController.addAction(cancelAction)
            self.present(actionController, animated: true, completion: nil)
        }
    }
    
    
    func  openCamera() {
        weak var weakSelf = self
        checkCameraPermisson(completionHander: {status in
            if status{
                weakSelf?.openImagePIcker(type: 0)
            }
        })
    }
    
    func openPhotLibrary() {
        weak var weakSelf = self
        checkPhotoLibraryPermission(completionHander: {status in
            if status{
                weakSelf?.openImagePIcker(type: 1)
            }
        })
    }
    
    
    func openImagePIcker(type:Int) {
        imagePicker = UIImagePickerController.init()
        imagePicker.sourceType = type == 1 ? .photoLibrary : .camera
        //imagePicker.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let tempImage:UIImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            imgViewProfilePic.image = tempImage
            uploadImage()
        }
        imagePicker.dismiss(animated: true, completion: nil)
        
    }
    
    
    func uploadImage() {
        
        isUploadingProceess = true
        // updateNextButton()
        spinnerView.startAnimating()
        let compressedImage = imgViewProfilePic.image?.jpeg(.lowest)
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.uploadFile(endUrl: ("\(ApiBaseUrl)service_attachment?lngId=\(Localize.currentLanguageID)"), imageData: compressedImage, withName: "attachment", fileName: "image.JPG", mimeType: "image/JPG", parameters: nil, completionHandler: {response, jsonObj in
            weakSelf?.isUploadingProceess = false
            // updateNextButton()
            weakSelf?.spinnerView.stopAnimating()
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                if let dataObj = jsonObj?.dataObj(){
                    if let urlString:String = dataObj.object(forKey: "a_url") as? String{
                        weakSelf?.profilePicUrl = urlString
                        
                        return
                    }
                }
            }
            weakSelf?.imgViewProfilePic.image = UIImage.init(named: "profilePicIcon")
            weakSelf?.showErrorMessage(message: message)
        })
    }
    
    
    func verifiedOTP(isd: String, mobileNo: String, OTP: String) {
        
        calledBecomePartnerApi(otp: OTP)
    }
    
    
    func verifyPhone(){
        startAnimating()
        weak var weakSelf = self
        
        let viewcontrollerOTP = SendOTPViewController()
        
        viewcontrollerOTP.sendUpdateProfileOTP(isdCode: txtCountryCode.text ?? "", mobileNo: txtMobileNo.text ?? "", completionHandler: { status, message in
            if status {
                let loginSb:UIStoryboard = UIStoryboard.init(name: MainSB, bundle: nil)
                let otpScreen:SignUpOTPVerifyViewController = loginSb.instantiateViewController(withIdentifier: "SignUpOTPVerifyViewController") as! SignUpOTPVerifyViewController
                otpScreen.isdCode =  weakSelf?.txtCountryCode.text! ?? ""
                otpScreen.mobileNo =  weakSelf?.txtMobileNo.text! ?? ""
                otpScreen.navigationTitle = "Become a Partner".localized()
                otpScreen.isForgotPassword = false
                otpScreen.isUpdatePhoneNumber = false
                otpScreen.isBecomePartner = true
                otpScreen.delegate = self
                weakSelf?.navigationController?.pushViewController(otpScreen, animated: true)
            }
            else{
                
                weakSelf?.showErrorMessage(message: message)
            }
            weakSelf?.stopAnimating()
        })
        
        
    }
    
    @IBAction func callNextScreen(_ sender: Any) {
        
        txtMobileNo.resignFirstResponder()
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtEmail.resignFirstResponder()
        
        
        if isUploadingProceess{
            showWarningMessage(message: "Please wait while the image being uploaded.".localized())
            return
        }
        
        if txtFirstName.text?.count==0 {
            txtFirstName.showError(message: "Required".localized())
            //showWarningMessage(message: "Please enter name.".localized())
            return
        }
        
        if txtCountryCode.text?.count == 0 {
            showErrorMessage(message: "Required".localized())
            return
        }
        
        if txtMobileNo.text?.count ?? 0 == 0 {
            txtMobileNo.showError(message: "Required".localized())
            //showWarningMessage(message: "Please enter name.".localized())
            return
        }
        if txtMobileNo.text?.count ?? 0 < 5 {
            
            txtMobileNo.showError(message: "Should be at least 5 characters".localized())
            
            return
        }
        
        if lblAddress.text == "Fetching Location...".localized()
        {
            showWarningMessage(message: "Pleaes Select Address".localized())
            
            return
        }
        
        //////////// --------- NEW CHANGES
        
        if let userDetails:userModel = getUserDetail(){
            
        if userDetails.mobileNumber == txtMobileNo.text && userDetails.mobileIsd == txtCountryCode.text {

            calledBecomePartnerApi(otp: nil)
        }
        else {

            verifyPhone()
          }
            
        }
    }
    
    func calledBecomePartnerApi(otp:String?)
    {
        startAnimating()
        
        let params:NSDictionary = ["mobileIsd":txtCountryCode.text ?? "",
                                   "mobileNumber":txtMobileNo.text ?? "",
                                   "otp":otp ?? "0",
                                   "firstName":txtFirstName.text ?? "",
                                   "lastName":txtLastName.text ?? "",
                                   "emailId":txtEmail.text ?? "",
                                   "profilePicture":profilePicUrl,
                                   "longitude": locationLon,
                                   "latitude" : locationLat,
                                   "address":addressOfPartner]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        print("paramsparamsparams ::::::::::: \(params)")
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/becomePartner?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            //let statusCode = response?.statusCode
            
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                
                //Save userdata
                
                if let dataStr:String = jsonObj?.dataString() {
                    if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        
                        print("dataObjdataObjdataObjdataObj ::::::::::: \(dataObj)")
                        
                        if let userDetails:NSDictionary = dataObj.object(forKey: "userDetails") as? NSDictionary{
                            let token:String =  weakSelf?.getCurrentUserToken() ?? ""
                            let status =  weakSelf?.saveToken(userName: weakSelf?.txtMobileNo.text ?? "", token: token)
                            if status ?? false {
                                //update success
                                
                                if  weakSelf?.saveUserDetails(userDetail: userDetails) ?? false{
                                    
                                    weakSelf?.showInfoMessage(message: message)
                                    
                                    let deadlineTime = DispatchTime.now() + .seconds(3)
                                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                        weakSelf?.navigationController?.popViewController(animated: true)
                                        weakSelf?.stopAnimating()
                                        
                                    }
                                    
                                    self.popViewControllerss(popViews: 2)
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshProfileView"), object: nil, userInfo: [:])
                                    
                                    return
                                }
                            }
                            
                        }
                    }
                    
                }
            }
            weakSelf?.stopAnimating()
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    func popViewControllerss(popViews: Int, animated: Bool = true) {
        
        if self.navigationController!.viewControllers.count > popViews
        {
            let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - popViews - 1]
             self.navigationController?.popToViewController(vc, animated: animated)
        }
    }
    
}
