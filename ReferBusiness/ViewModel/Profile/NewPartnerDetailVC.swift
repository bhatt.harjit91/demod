//
//  NewPartnerDetailVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 28/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class NewPartnerDetailVC: AppBaseViewController {

    @IBOutlet weak var lblSubmittedBy: NKLabel!
    
    @IBOutlet weak var lblProductServiceTitle: NKLabel!
    
    @IBOutlet weak var lblProductServiceText: UILabel!
    
    @IBOutlet weak var lblLocationTitle: NKLabel!
    
    @IBOutlet weak var lblLocationText: NKLabel!
    
    @IBOutlet weak var imgViewCheckBox: UIImageView!
    
    @IBOutlet weak var lblCheckBoxTitle: NKLabel!
    
    @IBOutlet weak var btnSearch: NKButton!
    
    
    @IBOutlet weak var txtSellerName: RCTextField!
    
    
    @IBOutlet weak var lblStatusSegmentTitle: NKLabel!
    
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var segmentStatus: UISegmentedControl!
    
    @IBOutlet weak var heightViewProductsServices: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewLocation: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewSellerInfo: NSLayoutConstraint!
    
    @IBOutlet weak var heightSellerInfoTitle: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceSellerInfo: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceSellerInfoTitle: NSLayoutConstraint!
    
    @IBOutlet weak var lblSellerName: NKLabel!
    
    @IBOutlet weak var imgViewSeller: RCImageView!
    
    @IBOutlet weak var heightTxtSeller: NSLayoutConstraint!
    
    @IBOutlet weak var heightBtnSearch: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var topSpaceTxtViewNotes: NSLayoutConstraint!
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
    @IBOutlet weak var viewUdateHistory: RCView!
    
    
    @IBOutlet weak var heightViewUpdateHistory: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewUpdateHistroyTitle: RCView!
    var count:Int?
    
    var newSellerModel:NewSellerUserModel?
    
    var isSelected:Int = 0
    
    var sellerRequestHistory:[NewSellerUserHistoryModel]?
    
    var requestId:Int = 0
    
    var status:Int = 0
    
    var sellerPresent:Int?
    
    var sellerDetails:FindSellerDetailsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

         navigationItem.title = "Products / Services".localized()
        
//        updateUI()
        
        topSpaceSellerInfo.constant = 0
        topSpaceSellerInfoTitle.constant = 0
        heightSellerInfoTitle.constant = 0
        heightViewSellerInfo.constant = 0
        topSpaceTxtViewNotes.constant = 0
        heightTxtSeller.constant = 0
        heightBtnSearch.constant = 0
        viewUdateHistory.alpha = 0
        viewUpdateHistroyTitle.alpha = 0
        
        txtSellerName.delegate = self
        
        imgViewCheckBox.setImageColor(color: UIColor.getGradientColor(index: 0).first!)
        
      fetchNewSellerList(withStatus: status ,withRequestId:requestId)
        
        segmentStatus.selectedSegmentIndex = status
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchNewSellerList(withStatus: status ,withRequestId:requestId)
    }
    
    func updateUI(){
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
        dateFormatterPrint.timeZone = .current
        
        
        if sellerRequestHistory?.count ?? 0 > 0 {
            
            viewUdateHistory.alpha = 1
            viewUpdateHistroyTitle.alpha = 1
            
            DispatchQueue.main.async(execute: {
                //This code will run in the main thread:
                var frame = self.tableViewList.frame
                frame.size.height = self.tableViewList.contentSize.height
                self.tableViewList.frame = frame
                
                 self.heightViewUpdateHistory.constant = self.tableViewList.contentSize.height + 50
            })

            
        }
        
        if let model:NewSellerUserModel = newSellerModel{
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.crDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            lblSubmittedBy.text = "\("Submitted by ".localized())\(model.createdBy ?? "")\(" on ")\(dateText)"
            
            lblProductServiceTitle.text = "Products / Services".localized()
            lblProductServiceText.text = model.description ?? ""
            lblLocationTitle.text = "Requested Location".localized()
            lblLocationText.text = model.address ?? ""
            lblCheckBoxTitle.text = "I created a Seller for this request".localized()
            lblStatusSegmentTitle.text = "Status of this request".localized()
            
            txtSellerName.alpha = 0
            btnSearch.alpha = 0
            lblStatusSegmentTitle.alpha = 1
            segmentStatus.alpha = 1
            
            
        }
    }
    
    func updateDetail(){
        
        
        if sellerPresent == 1 {
            
        topSpaceTxtViewNotes.constant = 160
            
        if let model:FindSellerDetailsModel = sellerDetails{
        
           lblSellerName.text = model.sellerCompanyName ?? ""
            
            if model.companyLogo?.count ?? 0 > 0 {
                imgViewSeller.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.companyLogo ?? "")")), completed: nil)
            }
            
            topSpaceSellerInfo.constant = 40
            topSpaceSellerInfoTitle.constant = 20
            heightSellerInfoTitle.constant = 40
            heightViewSellerInfo.constant = 100
            
            status = 1
            
            segmentStatus.selectedSegmentIndex = status
        
            }
            
            
        }
        else{
            
            status = 0
            
            segmentStatus.selectedSegmentIndex = status
           
            topSpaceSellerInfo.constant = 0
            topSpaceSellerInfoTitle.constant = 0
            heightSellerInfoTitle.constant = 0
            heightViewSellerInfo.constant = 0
        }
    }
    

    @IBAction func segmentStatus(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            status = 0
            break
        case 1:
            status = 1
        default:
            status = 0
        }
        
        
    }
    
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        
        if txtSellerName.text?.count ?? 0 == 0 {
            
            txtSellerName.showError(message: "Required".localized())
            
            return
        }
        
        getSeller()
        
    }
    
    
    @IBAction func btnCheckBox(_ sender: Any) {
        
        if isSelected == 0 {
        
           txtSellerName.alpha = 1
           btnSearch.alpha = 1
            
        imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
            
            isSelected = 1
            
            topSpaceTxtViewNotes.constant = 10
            heightTxtSeller.constant = 40
            heightBtnSearch.constant = 40
        
         }
        else{
            
            
            txtSellerName.alpha = 0
            btnSearch.alpha = 0
            
            imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBox")
            
             isSelected = 0
            
            topSpaceTxtViewNotes.constant = 0
            heightTxtSeller.constant = 0
            heightBtnSearch.constant = 0
        
        }
        
    }
    
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
      
        if sellerPresent == 1 {
            
           submitUserDetails()
        }
        else{
           
            if txtViewNotes.text.count == 0{
                
                txtViewNotes.showError(message: "Required".localized())
                
            }
            else{
                submitUserDetails()
            }
            
            
        }
        
        
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let size = CGSize(width: view.frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: UIFont.fontForType(textType: 0)]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
        
    }
    
    func updateAddressViewHeight() {
        
        guard let labelText = newSellerModel?.address else { return }
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        heightViewLocation.constant = height + 40
        
    }
    
    func updateProductViewHeight() {
        
        guard let labelText = newSellerModel?.description else { return }
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        heightViewProductsServices.constant = height + 40
        
    }
    
    
 //API Call
    
    func fetchNewSellerList(withStatus:Int,withRequestId:Int) {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        
       
            startAnimating()
        
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getNewSellerRequests?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&status=\(withStatus)&requestId=\(withRequestId)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(NewSellerUserDetailApiModel.self, from: data)
                                
                                if let apiResponse:NewSellerUserModel = jsonResponse.sellerRequestDetails {
                                    
                                    weakSelf?.newSellerModel = apiResponse
                                    
                                }
                                
                                if let apiResponse:[NewSellerUserHistoryModel] = jsonResponse.sellerRequestHistory {
                                    
                                    weakSelf?.sellerRequestHistory = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.count = apiResponse
                                    
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func getSeller() {
        
    txtSellerName.resignFirstResponder()
        
    let apiClient:APIClient = APIClient()
        
    weak var weakSelf = self
        
    startAnimatingAfterSubmit()
        
    apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/findSeller?lngId=\(Localize.currentLanguageID)&search=\(txtSellerName.text ?? "")&token=\(getCurrentUserToken())"), params: nil, completionHandler:{response, jsonObj in
        
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(FindSellerApiModel.self, from: data)
                                
                                
                                if let apiResponse:FindSellerDetailsModel = jsonResponse.sellerDetails {
                                    
                                    weakSelf?.sellerDetails = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.sellerPresent {
                                    
                                    weakSelf?.sellerPresent = apiResponse
                                    
                                }
                               
                                weakSelf?.showErrorMessage(message: message)
                                
                                weakSelf?.updateDetail()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
        
            weakSelf?.showErrorMessage(message: message)
        
        })
        
    }
    
    func submitUserDetails(){
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["requestId":newSellerModel?.requestId ?? 0,
                                   "searchQuery": newSellerModel?.searchQuery ?? "",
                                   "description":newSellerModel?.description ?? "",
                                   "address":newSellerModel?.address ?? "",
                                   "latitude":newSellerModel?.latitude ?? 0.0,
                                   "longitude":newSellerModel?.longitude ?? 0.0,
                                   "status": status,
                                   "sellerCode":sellerDetails?.sellerCode ?? 0,
                                   "notes":txtViewNotes.text ?? ""
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/newSellerRequestSubmit?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                weakSelf?.showInfoMessage(message: message)
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                weakSelf?.showErrorMessage(message: message)
                weakSelf?.dismiss(animated: true, completion: nil)
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    
    

}

extension NewPartnerDetailVC:UITextFieldDelegate{
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtSellerName{
            txtSellerName.showError(message: "")
            sellerPresent = 0
            
            updateDetail()
            
        }
        
      
        
        return true
    }
}

extension NewPartnerDetailVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
       return sellerRequestHistory?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
            if let cell:NewSellerHistorycell = tableView.dequeueReusableCell(withIdentifier: "NewSellerHistorycell", for: indexPath) as? NewSellerHistorycell {
                
                return getCellForHistoryList(cell: cell, indexPath: indexPath)
                
            }
        
        return UITableViewCell.init()
    }
    
    
    func getCellForHistoryList(cell: NewSellerHistorycell,indexPath:IndexPath) ->  NewSellerHistorycell {
        
        if let model:NewSellerUserHistoryModel =  sellerRequestHistory?[indexPath.row] {
            
            var status = ""
            
            if model.status == 1{
               
                status = "Closed ".localized()
            }
            else{
                 status = "Pending".localized()
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.crDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblTitle.text = "\(status)\(" By ".localized())\(model.createdBy ?? "")\(" by ".localized())\(model.createdBy ?? "")\(" on ".localized())\(dateText)"
            
            
        }
        
        return cell
    }
    
    
    
}

class NewSellerHistorycell:UITableViewCell{
    
    
    @IBOutlet weak var lblTitle: NKLabel!
    
}
