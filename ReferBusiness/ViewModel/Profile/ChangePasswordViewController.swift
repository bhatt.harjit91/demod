//
//  ChangePasswordViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ChangePasswordViewController: AppBaseViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtCurrentPassword: RCTextField!
    @IBOutlet weak var txtNewPassword: RCTextField!
    @IBOutlet weak var txtConfirmNewPassword: RCTextField!
    @IBOutlet weak var btnSave: NKButton!
    
    ///
    
    
    @IBOutlet weak var btnCurrentShowHidePwd: UIButton!
    
    @IBOutlet weak var btnNewShowHidePwd: UIButton!
    
    @IBOutlet weak var btnConfirmShowHidePwd: UIButton!
    
    var iconCurrentShowHidePwdClick = true
    var iconNewShowHidePwdClick = true
    var iconConfirmShowHidePwdClick = true
    

    ///
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationItem.title = "Change Password".localized()

        txtCurrentPassword.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Old Password".localized())
        txtNewPassword.attributedPlaceholder = addPlaceHolderWithText(placeholder: "New Password".localized())
        txtConfirmNewPassword.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Confirm Password".localized())
        
        txtCurrentPassword.delegate = self
        txtNewPassword.delegate = self
        txtConfirmNewPassword.delegate = self
        
    }
    
    ////
    @IBAction func btnCurrentPwd(_ sender: Any) {
        
        if iconCurrentShowHidePwdClick == true
        {
            btnCurrentShowHidePwd.setImage(UIImage(named: "showpwd"), for: .normal)
            txtCurrentPassword.isSecureTextEntry = false
        }
        else
        {
            btnCurrentShowHidePwd.setImage(UIImage(named: "hidepwd"), for: .normal)
            txtCurrentPassword.isSecureTextEntry = true
        }
        
        iconCurrentShowHidePwdClick = !iconCurrentShowHidePwdClick

    }
    
    
    @IBAction func btnNewPwd(_ sender: Any) {
        
        if iconNewShowHidePwdClick == true
        {
            btnNewShowHidePwd.setImage(UIImage(named: "showpwd"), for: .normal)
            txtNewPassword.isSecureTextEntry = false
        }
        else
        {
            btnNewShowHidePwd.setImage(UIImage(named: "hidepwd"), for: .normal)
            txtNewPassword.isSecureTextEntry = true
        }
        
        iconNewShowHidePwdClick = !iconNewShowHidePwdClick

    }
    
    
    @IBAction func btnConfirmPwd(_ sender: Any) {
        
        if iconConfirmShowHidePwdClick == true
        {
            btnConfirmShowHidePwd.setImage(UIImage(named: "showpwd"), for: .normal)
            txtConfirmNewPassword.isSecureTextEntry = false
        }
        else
        {
            btnConfirmShowHidePwd.setImage(UIImage(named: "hidepwd"), for: .normal)
            txtConfirmNewPassword.isSecureTextEntry = true
        }
        
        iconConfirmShowHidePwdClick = !iconConfirmShowHidePwdClick

    }
    /////
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        if  textField == txtCurrentPassword {
            if newString?.count ?? 0 > 0 {
                txtCurrentPassword.showError(message: "")
            }
        }
        else if textField == txtNewPassword {
            if newString?.count ?? 0 > 3 || newString?.count ?? 0 < 25 {
                txtNewPassword.showError(message: "")
            }
        }
        else if textField == txtConfirmNewPassword {
            if newString == txtNewPassword.text {
                txtConfirmNewPassword.showError(message: "")
            }
        }
        return true
    }
    
    func validate() -> Bool {
        if txtCurrentPassword.text?.count == 0 {
            txtCurrentPassword.showError(message: "Required".localized())
            return false
        }
        if txtNewPassword.text?.count ?? 0 < 6 {
            txtNewPassword.showError(message: "6 to 24 characters and at least one numeric digit".localized())
            return false
        }
        if  txtNewPassword.text?.count ?? 0 > 24 {
            txtNewPassword.showError(message: "6 to 24 characters and at least one numeric digit".localized())
            return false
        }
        if txtConfirmNewPassword.text?.count == 0 {
            txtConfirmNewPassword.showError(message: "Required".localized())
            return false
        }
        
        if txtNewPassword.text != txtConfirmNewPassword.text {
            txtConfirmNewPassword.showError(message: "Passwords do not match.".localized())
            showWarningMessage(message: "Please enter same password in both the fields.".localized())
            return false
        }
        return true
    }
    @IBAction func saveBtnClicked(_ sender: Any) {
        if validate(){
            view.endEditing(true)
            startAnimatingAfterSubmit()
            let params:NSDictionary = [
                "oldPassword":txtCurrentPassword.text ?? "",
                "newPassword":txtNewPassword.text ?? "",
                "serviceMasterId":appServiceMasterId ?? 0]
            let apiClient:APIClient = APIClient()
            weak var weakSelf = self
            let encryptedParams = ["data":encryptParams(params: params)]
            apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/changePassword?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
                //let statusCode = response?.statusCode
                if let selfObj = weakSelf {
                    var message:String = failedToConnectMessage.localized()
                    if let serverMessage = jsonObj?.message(){
                        message = (serverMessage.count>0) ? serverMessage : message
                    }
                    selfObj.stopAnimating()
                    
                    if let response = response {
                        if  selfObj.validateStatus(response: response){
                            if (jsonObj?.status() ?? false) {
                                
                                //Save userdata
                                selfObj.showInfoMessage(message: message)
                                
                                let deadlineTime = DispatchTime.now() + .seconds(1)
                                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                    selfObj.navigationController?.popViewController(animated: true)
                                }
                                return
                            }
                        }
                    }
                    
                    
                    selfObj.showErrorMessage(message: message)
                }
                
                
            })
        }
    }
    
}

