//
//  EditProfileViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

//
//  EditProfileViewController.swift
//  NearKart
//
//  Created by RaviKiran B on 06/10/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class EditProfileViewController: SendOTPViewController,CountryCodeSelectViewControllerDelegate,LocationPermissionVCNewDelegate {
    
    var selectedCountryCode : CountryCodeModel?
    
    var isReferralMember:Int = 0
    
    @IBOutlet weak var tblProfile: UITableView!
    
    var imageCell:EditProfileViewControllerImageCell!
    var nameCell:EditProfileViewControllerNameCell!
    var phoneCell:EditProfileViewControllerPhoneCell!
    var emailCell:EditProfileViewControllerEmailCell!
    var companyCell:EditProfileViewControllerCompanyName!
    var locationCell:EditProfileViewControllerCityCountry!
    var aboutCell:EditProfileViewControllerAbout!
    var submitCell:EditProfileViewControllerSubmitCell!
    var addressCell:EditProfileViewControllerAddressCell!
    var billingCell:EditProfileViewControllerBillingDetails!
    var currentLoc:CLLocationCoordinate2D?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let userModel = getUserDetail() {
            
            isReferralMember = userModel.isReferralMember ?? 0
            
        }
        
        
        navigationItem.title = "Edit Profile".localized()
        // Do any additional setup after loading the view.
        imageCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerImageCell") as? EditProfileViewControllerImageCell
        nameCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerNameCell") as? EditProfileViewControllerNameCell
        phoneCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerPhoneCell") as? EditProfileViewControllerPhoneCell
        emailCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerEmailCell") as? EditProfileViewControllerEmailCell
      
        companyCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerCompanyName") as? EditProfileViewControllerCompanyName
        locationCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerCityCountry") as? EditProfileViewControllerCityCountry
        aboutCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerAbout") as? EditProfileViewControllerAbout
        submitCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerSubmitCell") as? EditProfileViewControllerSubmitCell
        addressCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerAddressCell") as? EditProfileViewControllerAddressCell
        billingCell = tblProfile.dequeueReusableCell(withIdentifier: "EditProfileViewControllerBillingDetails") as? EditProfileViewControllerBillingDetails
        tblProfile.delegate = self
        tblProfile.dataSource = self
        
//        locationCell.btnLocation.addTarget(self, action: #selector(changeLocation(_:)), for: .touchUpInside)
        tblProfile.estimatedRowHeight = 100
        tblProfile.rowHeight = UITableView.automaticDimension
        nameCell.txtName.delegate = self
        nameCell.txtLastName.delegate = self
        phoneCell.txtCountry.delegate = self
        billingCell.txtBillingCountryCode.delegate = self
        phoneCell.txtMobile.delegate = self
        emailCell.txtEmail.delegate = self
        billingCell.txtBillingCity.delegate = self
        billingCell.txtBillingMobileNo.delegate = self
        
        getProfileData()
                
        nameCell.txtName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Name".localized())
        phoneCell.txtCountry.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Country".localized())
        phoneCell.txtMobile.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Mobile Number".localized())
        
        emailCell.txtEmail.placeholder = "Email".localized()
        
        billingCell.lblTitle.text = "Billing Address".localized()
        billingCell.txtBillingName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Name".localized())
        billingCell.txtBillingEmail.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Email".localized())
        billingCell.txtBillingCountry.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Country".localized())
        billingCell.txtBillingMobileNo.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Mobile Number".localized())
        billingCell.txtBillingAddress.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Address".localized())
        billingCell.txtBillingCountry.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Country".localized())
        billingCell.txtBillingState.attributedPlaceholder = addPlaceHolderWithText(placeholder: "State".localized())
        billingCell.txtBillingCity.attributedPlaceholder = addPlaceHolderWithText(placeholder: "City".localized())
        billingCell.txtBillingPinCode.placeholder = "Postal Code/PO Box".localized()
        
        addressCell.imgLocation.setImageColor(color: .white)
        
        
        let btnName = UIButton()
        //  btnName.setImage(UIImage(named: "walletIcon"), for: .normal)
        btnName.setTitle("Update -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(updateProfileClicked(_:)), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
//        submitCell.btnUpdate.setTitle("Update".localized(), for: .normal)
        //Update
        
        imageCell.imgViewCamera.setImageColor(color: .white)
        
    }
    
    func updateProfile(){
        if let userDetails:userModel = getUserDetail(){
            if userDetails.profilePicture?.count ?? 0 > 0{
                if let url:URL = URL.init(string: "\(StorageUrl)\(userDetails.profilePicture ?? "")"){
                    imageCell.imgProfile.sd_setImage(with: url, completed: nil)
                    
                }
            }
            
            nameCell.txtName.text = userDetails.firstName
            nameCell.txtLastName.text = userDetails.lastName
            phoneCell.txtCountry.text = userDetails.mobileIsd
            phoneCell.txtMobile.text = userDetails.mobileNumber
            emailCell.txtEmail.text = userDetails.emailId
            addressCell.lblAddress.text = userDetails.address
            currentLoc?.latitude = userDetails.latitude ?? 0.0
            currentLoc?.longitude = userDetails.longitude ?? 0.0
            addressCell?.btnLocation.addTarget(self, action: #selector(changeLocation(_:)), for: .touchUpInside)
             addressCell?.btnSelectLocation.addTarget(self, action: #selector(changeLocation(_:)), for: .touchUpInside)
//            jobTitleCell.txtJobTitle.text = userDetails.jobTitle
//            companyCell.txtCompany.text = userDetails.organization
//            locationCell.txtCity.text = userDetails.city
//            aboutCell.txtAbout.text = userDetails.about
            selectedCountryCode = getCountryCodeModel(dialCode: userDetails.mobileIsd ?? "")
            billingCell.txtBillingCountryCode.text = userDetails.billingDetails?.billing_tel_isd ?? ""
            billingCell.txtBillingName.text = userDetails.billingDetails?.billing_name ?? ""
            billingCell.txtBillingEmail.text = userDetails.billingDetails?.billing_email ?? ""
            billingCell.txtBillingMobileNo.text = userDetails.billingDetails?.billing_tel ?? ""
            if userDetails.billingDetails?.billing_tel?.count ?? 0 > 0 {
             billingCell.txtBillingMobileNo.text = userDetails.billingDetails?.billing_tel
            }
            billingCell.txtBillingAddress.text = userDetails.billingDetails?.billing_address
            billingCell.txtBillingCountry.text = userDetails.billingDetails?.billing_country
            billingCell.txtBillingState.text = userDetails.billingDetails?.billing_state
            billingCell.txtBillingCity.text = userDetails.billingDetails?.billing_city
            billingCell.txtBillingPinCode.text = userDetails.billingDetails?.billing_zip
        }
    }
    func getProfileData() {
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getProfileDetails?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                
                //Save userdata
                weakSelf?.stopAnimating()
                if let dataStr:String = jsonObj?.dataString() {
                    if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        if let userDetails:NSDictionary = dataObj.object(forKey: "userDetails") as? NSDictionary{
                            print("\("print user :::")\(userDetails)")
                            let token:String =  weakSelf?.getCurrentUserToken() ?? ""
                            let status =  weakSelf?.saveToken(userName: weakSelf?.phoneCell.txtMobile.text ?? "", token: token)
                            if status ?? false {
                                //update success
                                if  weakSelf?.saveUserDetails(userDetail: userDetails) ?? false{
                                    
                                    //weakSelf?.showInfoMessage(message: message)
                                    
                                    //                                let deadlineTime = DispatchTime.now() + .seconds(3)
                                    //                                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                    //                                    weakSelf?.navigationController?.popViewController(animated: true)
                                    //                                    weakSelf?.stopAnimating()
                                    weakSelf?.updateProfile()
                                    
                                    
                                }
                                return
                            }
                            
                        }
                    }
                    
                }
            }
            weakSelf?.stopAnimating()
            
            weakSelf?.showErrorMessage(message: message)
            
            
            
        })
    }
    
    //MARK: -  IBACTIONS
    
    func didFinishSelectingCountryCode(selectedCountry: CountryCodeModel) {
        selectedCountryCode = selectedCountry
        phoneCell.txtCountry.text = selectedCountryCode?.dial_code
        billingCell.txtBillingCountryCode.text = selectedCountryCode?.dial_code
        
    }
    func openCountrySelector() {
        let storyBoard = UIStoryboard.init(name: CountryCodeSB, bundle: nil)
        let countrySelector:CountryCodeSelectViewController = storyBoard.instantiateViewController(withIdentifier: "CountryCodeSelectViewController") as! CountryCodeSelectViewController
        countrySelector.selectedCountryCode = selectedCountryCode
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
    }
    
    @IBAction func addImageClicked(_ sender: Any) {
        openImagePicker()
        weak var weakSelf = self
        didPickImage = { image in
            weakSelf?.imageCell.imgProfile.image = image
        }
        
        didUploadImage = { status, urlString in
            if !status {
                if let userDetails:userModel = weakSelf?.getUserDetail(){
                    if userDetails.profilePicture?.count ?? 0 > 0{
                        if let url:URL = URL.init(string: "\(StorageUrl)\(userDetails.profilePicture ?? "")"){
                            weakSelf?.imageCell.imgProfile.sd_setImage(with: url, completed: nil)
                            return
                        }
                    }
                }
                weakSelf?.imageCell.imgProfile.image =  UIImage.init(named: "profilePicIcon")
            }
        }
    }
    @IBAction func updateProfileClicked(_ sender: Any) {
        if isUploadingProceess{
            showWarningMessage(message: "Please wait while the image being uploaded.".localized())
            return
        }
        
        if nameCell.txtName.text?.count==0 {
            nameCell.txtName.showError(message: "Please enter name.".localized())
            //showWarningMessage(message: "Please enter name.".localized())
            return
        }
        
        if phoneCell.txtCountry.text?.count == 0 {
            showErrorMessage(message: "Please select country code".localized())
            return
        }
        if phoneCell.txtMobile.text?.count ?? 0 == 0 {
            showErrorMessage(message: "Please enter your mobile number".localized())
            return
        }
        
//        if emailCell.txtEmail.text?.count ?? 0 > 0 && !isValidEmail(Str: emailCell.txtEmail.text ?? ""){
//            emailCell.txtEmail.showError(message: "Please enter valid email address.".localized())
//            return
//        }
//        if emailCell.txtEmail.text?.count ?? 0 == 0 {
//            emailCell.txtEmail.showError(message: "Please enter valid email address.".localized())
//            return
//        }

        /////////////
        
        if let userDetails:userModel = getUserDetail(){
            
            if userDetails.isBuyer == 1  && userDetails.isReferralMember ?? 0 != 1{
                
                // not required
                
                if userDetails.mobileNumber == phoneCell.txtMobile.text && userDetails.mobileIsd == phoneCell.txtCountry.text {
                    //update profile
                    updateProfile(otp: nil)
                }
                else {
                    //verify PHone
                    verifyPhone()
                }
                
            }
            else
            {
                // required
                
                if billingCell.txtBillingName.text?.count ?? 0 == 0 {
                    billingCell.txtBillingName.showError(message: "Please enter billing name.".localized())
                    return
                }
                if billingCell.txtBillingEmail.text?.count ?? 0 > 0 && !isValidEmail(Str: billingCell.txtBillingEmail.text ?? ""){
                    billingCell.txtBillingEmail.showError(message: "Please enter valid email address.".localized())
                    return
                }
                if billingCell.txtBillingEmail.text?.count ?? 0 == 0 {
                    showErrorMessage(message: "Please enter your billing email".localized())
                    return
                }
                if billingCell.txtBillingCountryCode.text?.count ?? 0 == 0 {
                    showErrorMessage(message: "Please select billing country code".localized())
                    return
                }
                if billingCell.txtBillingMobileNo.text?.count ?? 0 == 0 {
                    showErrorMessage(message: "Please enter your billing mobile number".localized())
                    return
                }
                if billingCell.txtBillingAddress.text?.count ?? 0 == 0 {
                    showErrorMessage(message: "Please enter your billing address".localized())
                    return
                }
                if billingCell.txtBillingCountryCode.text?.count ?? 0 == 0 {
                    showErrorMessage(message: "Please select your billing country code".localized())
                    return
                }
                if billingCell.txtBillingCountry.text?.count ?? 0 == 0 {
                    showErrorMessage(message: "Please enter your billing country".localized())
                    return
                }
                if billingCell.txtBillingState.text?.count ?? 0 == 0 {
                    showErrorMessage(message: "Please enter your billing state".localized())
                    return
                }
                if billingCell.txtBillingCity.text?.count ?? 0 == 0 {
                    showErrorMessage(message: "Please enter your billing city".localized())
                    return
                }
                
                if userDetails.mobileNumber == phoneCell.txtMobile.text && userDetails.mobileIsd == phoneCell.txtCountry.text {
                    //update profile
                    updateProfile(otp: nil)
                }
                else {
                    //verify PHone
                    verifyPhone()
                }
                
                ////////////////////////
                
            }
        }
        
        
    }
    
    
    @IBAction func changeLocation(_ sender: Any) {
      
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        locationPage.lat = currentLoc?.latitude ?? 0.0
        locationPage.lng = currentLoc?.longitude ?? 0.0
        locationPage.address = addressCell.lblAddress.text ?? ""
        locationPage.modalPresentationStyle = .fullScreen
        navigationController?.present(locationPage, animated: true)
    }
    
    func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        currentLoc = coordinate
        updateLocationName()
    }
    
    func updateLocationName() {
        let geoCoder = GMSGeocoder()
        
        weak var weakSelf = self
        
        geoCoder.reverseGeocodeCoordinate(currentLoc ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), completionHandler: { response, error in
            
            if response != nil {
                let address = response?.results()?.first
                if address != nil {
                    
                    weakSelf?.addressCell.lblAddress.text = address?.lines?.joined(separator: ",")
                    
                }
            }
            
        })
    }
    
}

//MARK: - CELL CLASSES
class EditProfileViewControllerImageCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imageSpinner: UIActivityIndicatorView!
    @IBOutlet weak var btnAddImage: NKButton!
    @IBOutlet weak var imgViewCamera: UIImageView!
    
}

class EditProfileViewControllerNameCell: UITableViewCell {
    
    @IBOutlet weak var txtName: RCTextField!
    
    @IBOutlet weak var txtLastName: RCTextField!
}

class EditProfileViewControllerPhoneCell: UITableViewCell {
    
    @IBOutlet weak var txtCountry: RCTextField!
    @IBOutlet weak var txtMobile: RCTextField!
}

class EditProfileViewControllerEmailCell: UITableViewCell {
    
    @IBOutlet weak var txtEmail: RCTextField!
}

class EditProfileViewControllerAddressCell:UITableViewCell{
    
    @IBOutlet weak var lblAddress: NKLabel!
    
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var btnSelectLocation: UIButton!
    
    @IBOutlet weak var btnLocation: UIButton!
    
}

class EditProfileViewControllerJobTitle: UITableViewCell {
    
    @IBOutlet weak var txtJobTitle: RCTextField!
}

class EditProfileViewControllerCompanyName: UITableViewCell {
    
    @IBOutlet weak var txtCompany: RCTextField!
}

class EditProfileViewControllerCityCountry: UITableViewCell {
    
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var txtCity: RCTextField!
}

class EditProfileViewControllerAbout: UITableViewCell {
    
    @IBOutlet weak var txtAbout: RCTextField!
}

class EditProfileViewControllerSubmitCell: UITableViewCell {
    
    @IBOutlet weak var btnUpdate: NKButton!
}
class EditProfileViewControllerBillingDetails:UITableViewCell{
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var txtBillingName: RCTextField!
    
    @IBOutlet weak var txtBillingEmail: RCTextField!
    
    @IBOutlet weak var txtBillingCountryCode: RCTextField!
    
    @IBOutlet weak var txtBillingState: RCTextField!
    
    @IBOutlet weak var txtBillingCity: RCTextField!
    
    @IBOutlet weak var txtBillingPinCode: RCTextField!
    @IBOutlet weak var txtBillingMobileNo: RCTextField!
    
    @IBOutlet weak var txtBillingAddress: RCTextField!
    
    @IBOutlet weak var txtBillingCountry: RCTextField!
    
}
//MARK: - TABLEVIEW
extension EditProfileViewController: UITableViewDelegate,UITableViewDataSource{
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if let userDetails:userModel = getUserDetail(){
            
            if userDetails.isBuyer == 1 && userDetails.isReferralMember ?? 0 != 1 {
                
                return 4
            }
            else
            {
                return 6
            }
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.row {
            
        case 0:
            return imageCell
            
        case 1:
            return nameCell
        case 2:
            return phoneCell
        case 3:
            return emailCell
        case 4:
            return addressCell
//        case 4:
//            return companyCell
//        case 5:
//            return locationCell
//        case 6:
//            return aboutCell
        case 5:
            return billingCell
        default:
            break
        }
        return UITableViewCell.init()
    }
    
    
}

//MARK: - TEXTFIELD DELEGATE

extension EditProfileViewController:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  textField == phoneCell.txtCountry || textField == billingCell.txtBillingCountryCode {
            openCountrySelector()
            return false
        }

        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField ==  nameCell.txtName {
            let nsString = textField.text as NSString?
            let numbString = nsString?.replacingCharacters(in: range, with: string) ?? ""
            if numbString.count>0{
                nameCell.txtName.showError(message: "")
            }
            let maxLength = 50
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                    
            return newString.length <= maxLength
        }
        
        else if textField ==  nameCell.txtLastName {
            
            let maxLength = 50
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                    
            return newString.length <= maxLength
        }
        
        else if textField ==  phoneCell.txtMobile {
            
            let maxLength = 14
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                    
            return newString.length <= maxLength
        }
        
        else if textField ==  billingCell.txtBillingMobileNo {
            
            let maxLength = 14
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                    
            return newString.length <= maxLength
        }
        
        return true
    }
}

//MARK: - VERIFY AND UPDATE PROFILE
extension EditProfileViewController: SignUpOTPVerifyViewControllerDelegate{
    func verifiedOTP(isd: String, mobileNo: String, OTP: String) {
        updateProfile(otp: OTP)
    }
    
    func verifyPhone(){
        startAnimating()
        weak var weakSelf = self
        sendUpdateProfileOTP(isdCode: phoneCell.txtCountry.text ?? "", mobileNo: phoneCell.txtMobile.text ?? "", completionHandler: { status, message in
            if status {
                let loginSb:UIStoryboard = UIStoryboard.init(name: MainSB, bundle: nil)
        if let otpScreen:SignUpOTPVerifyViewController = loginSb.instantiateViewController(withIdentifier: "SignUpOTPVerifyViewController") as? SignUpOTPVerifyViewController {
                    
                otpScreen.isdCode =  weakSelf?.phoneCell.txtCountry.text! ?? ""
                otpScreen.mobileNo =  weakSelf?.phoneCell.txtMobile.text! ?? ""
                otpScreen.isForgotPassword = false
                otpScreen.isUpdatePhoneNumber = true
                otpScreen.delegate = self
            
                weakSelf?.navigationController?.pushViewController(otpScreen, animated: true)
                    
                }
                
            }
            else{
                
                weakSelf?.showErrorMessage(message: message)
            }
            weakSelf?.stopAnimating()
        })
        
        
    }
    
    func updateProfilePostMethod(params:NSDictionary)
    {
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/updateProfile?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            //let statusCode = response?.statusCode
            
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if (jsonObj?.status() ?? false) {
                
                //Save userdata
                
                if let dataStr:String = jsonObj?.dataString() {
                    if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        if let userDetails:NSDictionary = dataObj.object(forKey: "userDetails") as? NSDictionary{
                            let token:String =  weakSelf?.getCurrentUserToken() ?? ""
                            let status =  weakSelf?.saveToken(userName: weakSelf?.phoneCell.txtMobile.text ?? "", token: token)
                            if status ?? false {
                                //update success
                                
                                if  weakSelf?.saveUserDetails(userDetail: userDetails) ?? false{
                                    
                                    weakSelf?.showInfoMessage(message: message)
                                    
                                    let deadlineTime = DispatchTime.now() + .seconds(3)
                                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                        weakSelf?.navigationController?.popViewController(animated: true)
                                        weakSelf?.stopAnimating()
                                        
                                    }
                                    return
                                }
                            }
                            
                        }
                    }
                    
                }
            }
            weakSelf?.stopAnimating()
            
            weakSelf?.showErrorMessage(message: message)
            
            
            
        })
    }
    
    func updateProfile(otp:String?){
        startAnimating()

        if let userDetails:userModel = getUserDetail(){
            
            if userDetails.isBuyer == 1 && userDetails.isReferralMember ?? 0 != 1 {
                let params:NSDictionary = [
                    "mobileIsd":phoneCell.txtCountry.text ?? "",
                    "mobileNumber":phoneCell.txtMobile.text ?? "",
                    "otp":otp ?? "0",
                    "displayName":"\(nameCell.txtName.text ?? "")\(" ")\(nameCell.txtLastName.text ?? "")",
                    "firstName":nameCell.txtName.text ?? "",
                    "lastName":nameCell.txtLastName.text ?? "",
                    "emailId":emailCell.txtEmail.text ?? "",
                    "profilePicture":profilePicUrl
                ]
                
                updateProfilePostMethod(params: params)
            }
            else
            {
                let billingParams:NSDictionary = [
                    "billing_name":billingCell.txtBillingName.text ?? "",
                    "billing_address":billingCell.txtBillingAddress.text ?? "",
                    "billing_city":billingCell.txtBillingCity.text ?? "",
                    "billing_state":billingCell.txtBillingState.text ?? "",
                    "billing_zip":billingCell.txtBillingPinCode.text ?? "",
                    "billing_country":billingCell.txtBillingCountry.text ?? "",
                    "billing_tel":billingCell.txtBillingMobileNo.text ?? "",
                    "billing_email":billingCell.txtBillingEmail.text ?? "",
                    "billing_tel_isd":billingCell.txtBillingCountryCode.text ?? ""
                    
                ]
                let params:NSDictionary = [
                    "mobileIsd":phoneCell.txtCountry.text ?? "",
                    "mobileNumber":phoneCell.txtMobile.text ?? "",
                    "otp":otp ?? "0",
                    "displayName":"\(nameCell.txtName.text ?? "")\(" ")\(nameCell.txtLastName.text ?? "")",
                    "firstName":nameCell.txtName.text ?? "",
                    "lastName":nameCell.txtLastName.text ?? "",
                    "emailId":emailCell.txtEmail.text ?? "",
                    "profilePicture":profilePicUrl,
                    "latitude":currentLoc?.latitude ?? "0.0",
                    "longitude":currentLoc?.longitude ?? "0.0",
                    "address":addressCell?.lblAddress.text ?? "",
                    "billingDetails":billingParams
                ]
                
                updateProfilePostMethod(params: params)
            }
        }

    }
}
