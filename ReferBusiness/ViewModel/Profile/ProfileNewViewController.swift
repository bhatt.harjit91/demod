//
//  ProfileNewViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 21/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ProfileNewViewController: AppBaseViewController {

    @IBOutlet weak var btnBecomePartner: UIButton!
    
    @IBOutlet weak var heightOfBtnBecomePartner: NSLayoutConstraint!

    @IBOutlet weak var lblEditProfile: NKLabel!
    
    @IBOutlet weak var lblChangePassword: NKLabel!
    
    @IBOutlet weak var lblChangeLanguage: NKLabel!
    
    @IBOutlet weak var lblSignOut: NKLabel!
    
    @IBOutlet weak var btnTermsOfUse: NKButton!
    
    @IBOutlet weak var btnPrivacyPolicy: NKButton!
    
    @IBOutlet weak var viewMembershipDetail: UIView!
    
    @IBOutlet weak var viewProfileList: UIView!
    
    @IBOutlet weak var lblAboutUs: NKLabel!
    
    
   // var memberShipStatusObj:MemberShipStatusObj?
    
    //var membershipId:String?
    
    
    @IBOutlet weak var lblMobileNo: NKLabel!
    
    @IBOutlet weak var lblMembershipId: NKLabel!
    
    
    @IBOutlet weak var imgViewProfile: RCImageView!
//    @IBOutlet weak var imgViewMembership: UIImageView!
    
    @IBOutlet weak var lblMemberName: NKLabel!
    
//    @IBOutlet weak var lblBillingDate: NKLabel!
    
    @IBOutlet weak var heightViewMembershipDetail: NSLayoutConstraint!
    
    @IBOutlet weak var imgEditProfileArrow: RCImageView!
    
    @IBOutlet weak var imgChangePasswordArrow: UIImageView!
    
    @IBOutlet weak var imgChangeLanguageArrow: UIImageView!
    
    @IBOutlet weak var imgAboutUsArrow: UIImageView!
    
    @IBOutlet weak var imgSignOutArrow: UIImageView!
        
    @IBOutlet weak var heightMembershipId: NSLayoutConstraint!
    
//    @IBOutlet weak var heightBillingDate: NSLayoutConstraint!///////
    
    @IBOutlet weak var heightViewProfileList: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewPartnerAgreement: NSLayoutConstraint!
    
    @IBOutlet weak var heightSellerOpportunities: NSLayoutConstraint!
    
    @IBOutlet weak var heightInvitePartner: NSLayoutConstraint!
    
    @IBOutlet weak var viewPartnerAgreement: RCView!
    
    @IBOutlet weak var imgPartnerAgreement: UIImageView!
    
    @IBOutlet weak var lblPartnerAgreement: NKLabel!
    
    @IBOutlet weak var viewSellerOpportunity: RCView!
    
    @IBOutlet weak var imgPartnerRightArrow: UIImageView!
    
    @IBOutlet weak var imgSellerOpportunity: UIImageView!
    
    @IBOutlet weak var lblSellerOpportunity: NKLabel!
    
    @IBOutlet weak var imgSellerRightArrow: UIImageView!
    
    @IBOutlet weak var viewInvitePartner: RCView!
    
    @IBOutlet weak var lblInvitePartnerTitle: NKLabel!
    
    @IBOutlet weak var imgInvitePartnerRightArrow: UIImageView!
    
    @IBOutlet weak var viewResumeBank: RCView!
    
    @IBOutlet weak var heightViewResumeBank: NSLayoutConstraint!
    
    @IBOutlet weak var lblResumeBank: NKLabel!
    
    @IBOutlet weak var imgRightArrowResume: UIImageView!
    
    let starAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 6),
        .foregroundColor: UIColor.getGradientColor(index: 1).first ?? 0]
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.darkGray]
    
    let mainAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.black]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshProfileView(notification:)), name: NSNotification.Name(rawValue: "refreshProfileView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        initialization()
        
    }

    func initialization()
    {
        viewMembershipDetail.layer.cornerRadius = 5
        viewMembershipDetail.layer.borderWidth = 1.0
        viewMembershipDetail.layer.borderColor = UIColor.lightGray.cgColor
        viewMembershipDetail.layer.shadowColor = UIColor.black.cgColor
        viewMembershipDetail.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewMembershipDetail.layer.shadowOpacity = 0.2
        viewMembershipDetail.layer.shadowRadius = 4.0
        viewMembershipDetail.alpha = 1
        
        viewProfileList.layer.cornerRadius = 5
        viewProfileList.layer.borderWidth = 1.0
        viewProfileList.layer.borderColor = UIColor.lightGray.cgColor
        viewProfileList.layer.shadowColor = UIColor.black.cgColor
        viewProfileList.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewProfileList.layer.shadowOpacity = 0.2
        viewProfileList.layer.shadowRadius = 4.0
        
        
        
        imgEditProfileArrow.setImageColor(color: .lightGray)
        imgAboutUsArrow.setImageColor(color: .lightGray)
        imgChangeLanguageArrow.setImageColor(color: .lightGray)
        imgChangePasswordArrow.setImageColor(color: .lightGray)
        imgSignOutArrow.setImageColor(color: .lightGray)
        imgPartnerRightArrow.setImageColor(color: .lightGray)
        imgSellerRightArrow.setImageColor(color: .lightGray)
        imgInvitePartnerRightArrow.setImageColor(color: .lightGray)
        imgRightArrowResume.setImageColor(color: .lightGray)
        
        lblEditProfile.text = "Edit Profile".localized()
        lblPartnerAgreement.text = "Upload Partner Agreement".localized()
        lblSellerOpportunity.text = "New Seller Opportunities".localized()
        lblInvitePartnerTitle.text = "Invite Partners".localized()
        lblChangeLanguage.text = "Change Language".localized()
        lblChangePassword.text = "Change Password".localized()
        
        lblSignOut.text = "Sign Out".localized()
        lblAboutUs.text = "About Us".localized()
        
        btnTermsOfUse.setTitle("Terms of use".localized(), for: .normal)
        btnTermsOfUse.colorIndex = 3
        
        btnPrivacyPolicy.setTitle("Privacy Policy".localized(), for: .normal)
        btnPrivacyPolicy.colorIndex = 3
        
        navigationItem.title = "Settings".localized()
        
        viewPartnerAgreement.alpha = 0
        viewSellerOpportunity.alpha = 0
        viewInvitePartner.alpha = 0
        viewResumeBank.alpha = 0
        heightSellerOpportunities.constant = 0
        heightViewPartnerAgreement.constant = 0
        heightInvitePartner.constant = 0
        heightViewResumeBank.constant = 0
        heightViewProfileList.constant = 300
        
        
        if checkLogin() {
            if let userDetails:userModel = getUserDetail(){
                
                btnBecomePartner.layer.cornerRadius = 5.0
                btnBecomePartner.backgroundColor = UIColor.primaryColor
                
                btnBecomePartner.isUserInteractionEnabled = false
                btnBecomePartner.setTitle("", for: .normal)
                heightOfBtnBecomePartner.constant = 0
                
                heightViewMembershipDetail.constant = 205
                heightMembershipId.constant = 0
                
                lblMemberName.text = userDetails.displayName ?? ""
                lblMobileNo.text = "\(userDetails.mobileIsd ?? "")\(" ")\(userDetails.mobileNumber ?? "")"
                
                if userDetails.profilePicture?.count ?? 0 > 0 {
                    imgViewProfile.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(userDetails.profilePicture ?? "")")), completed: nil)
                }
                
                var isReferralMember:Int = 0
                
                if let userModel = getUserDetail() {
                    
                    isReferralMember = userModel.isReferralMember ?? 0
                    
                }
                

                if userDetails.membershipId?.count ?? 0 > 0 {
                                        
                    heightViewMembershipDetail.constant = 235
                    
                    let star = NSAttributedString(string:"\(userDetails.membershipId ?? "")" , attributes: starAttributes)
                    
                    let starTexr = NSMutableAttributedString(string: "Partner ID  ")
                    
                    starTexr.append(star)
                    
                    lblMembershipId.attributedText = starTexr
                    
                    heightMembershipId.constant = 25
                }
                
                if isReferralMember == 1
                {
                    viewPartnerAgreement.alpha = 1
                    viewSellerOpportunity.alpha = 1
                    viewInvitePartner.alpha = 1
                    viewResumeBank.alpha = 1
                    heightSellerOpportunities.constant = 60
                    heightViewPartnerAgreement.constant = 60
                    heightInvitePartner.constant = 60
                    heightViewResumeBank.constant = 60
                    heightViewProfileList.constant = 540
                    
                }
               if userDetails.isBuyer == 1 && isReferralMember != 1
                {
                    
                    let firstAttributes: [NSAttributedString.Key: Any] = [
                        .font: UIFont.fontForType(textType: 2),
                        .foregroundColor: UIColor.white]
                    
                    let secondAttributes: [NSAttributedString.Key: Any] = [
                        .font: UIFont.fontForType(textType: 8),
                        .foregroundColor: UIColor.white]
                    
                    
                    let star = NSAttributedString(string:"\("\n")\("Refer buyers and earn...".localized())" , attributes: secondAttributes)
                    
                    let starTexr = NSMutableAttributedString(string: "Become a Partner".localized() , attributes: firstAttributes)
                    
                    starTexr.append(star)
                    
                    btnBecomePartner.titleLabel?.numberOfLines = 0
                    btnBecomePartner.titleLabel?.lineBreakMode = .byWordWrapping
                    btnBecomePartner.setAttributedTitle(starTexr, for: .normal)
                    btnBecomePartner.isUserInteractionEnabled = true
                    heightOfBtnBecomePartner.constant = 45
                    heightViewMembershipDetail.constant = 255
                    
                }
            }
        }
        
        
        
        //heightBillingDate.constant = 0
        
        //imgViewMembership.alpha = 0
        
        
        //        if memberShipStatusObj != nil {
        //
        //             //imgViewMembership.alpha = 1
        //
        //            let dateFormatterGet = DateFormatter()
        //            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        //            let dateFormatterPrint = DateFormatter()
        //            dateFormatterPrint.dateFormat = "dd-MMM-yyyy"
        //            dateFormatterPrint.timeZone = .current
        //
        //            var dateText = ""
        //
        //            if let date = dateFormatterGet.date(from: memberShipStatusObj?.expiryDate ?? "") {
        //
        //                dateText = dateFormatterPrint.string(from: date)
        //            }
        //
        //            let datelblText = NSMutableAttributedString(string:"\("Next Billing Date  ")",
        //                attributes: yourAttributes)
        //
        //            let finaldateLblText = NSMutableAttributedString(string:"\(dateText)",
        //                attributes: mainAttributes)
        //
        //            let combination = NSMutableAttributedString()
        //
        //            combination.append(datelblText)
        //            combination.append(finaldateLblText)
        //
        //            //lblBillingDate.attributedText = combination
        //
        //            ///
        //
        //            let star = NSAttributedString(string:"\(membershipId ?? "")" , attributes: starAttributes)
        //
        //            let starTexr = NSMutableAttributedString(string: "Member ID  ")
        //
        //            starTexr.append(star)
        //
        //            ///
        //
        //            lblMembershipId.attributedText = starTexr
        //
        ////            if memberShipStatusObj?.memberShipType == 1 {
        ////
        ////
        ////                lblMembershipId.text = "\("Member ID ")\(membershipId ?? "")"
        ////                //imgViewMembership.image = UIImage.init(imageLiteralResourceName: "basicMedalIcon")
        ////
        ////            }
        ////
        ////            if memberShipStatusObj?.memberShipType == 2 {
        ////
        ////                lblMembershipId.text = "\("Member ID ")\(membershipId ?? "")"
        ////
        ////                //imgViewMembership.image = UIImage.init(imageLiteralResourceName: "silverMedalIcon")
        ////
        ////            }
        ////            if memberShipStatusObj?.memberShipType == 3 {
        ////
        ////                lblMembershipId.text = "\("Member ID ")\(membershipId ?? "")"
        ////                 //imgViewMembership.image = UIImage.init(imageLiteralResourceName: "goldMedalIcon")
        ////
        ////            }
        ////
        //
        //
        //            heightViewMembershipDetail.constant = 240
        //
        //            //heightBillingDate.constant = 20
        //
        //        }
    }
    
    
    @IBAction func openTerms(_ sender: Any) {
        
        openWebView(urlString: "\("terms_")\(Localize.currentLanguage)\(".html")", title: "Terms of Use".localized(), subTitle: "")
        
    }
    
    @IBAction func openPrivacy(_ sender: Any) {
        
        openWebView(urlString: "\("privacy_")\(Localize.currentLanguage)\(".html")", title: "Privacy Policy".localized(), subTitle: "")
        
    }
    
    
    @IBAction func btnEditProfileAction(_ sender: Any) {
        
        performSegue(withIdentifier: "EditProfileSegue", sender: nil)
        
    }
    
    @IBAction func btnPartnerAgreementAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: SellerSB, bundle: nil)
        if let newSellerPage:UploadSellerAgreementVC = storyBoard.instantiateViewController(withIdentifier: "UploadSellerAgreementVC") as? UploadSellerAgreementVC  {
            
            newSellerPage.isPartner = true
            
            self.navigationController?.pushViewController(newSellerPage, animated: true)
        }
    }
    
    @IBAction func btnSellerOpportunityAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: ProfilePageSB, bundle: nil)
        if let newSellerPage:NewPartnerVC = storyBoard.instantiateViewController(withIdentifier: "NewPartnerVC") as? NewPartnerVC  {
            
            self.navigationController?.pushViewController(newSellerPage, animated: true)
        }
        
    }
    
    @IBAction func btnInvitePartnerAction(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard.init(name: ProfilePageSB, bundle: nil)
               if let invitePartnerPage:InvitePartnerVC = storyBoard.instantiateViewController(withIdentifier: "InvitePartnerVC") as? InvitePartnerVC  {
                   
                   self.navigationController?.pushViewController(invitePartnerPage, animated: true)
               }
        
    }
    
    
    @IBAction func btnResumeBank(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: ResumeBankSB, bundle: nil)
        if let invitePartnerPage:ResumeBankTabBarControllerView = storyBoard.instantiateViewController(withIdentifier: "ResumeBankTabBarControllerView") as? ResumeBankTabBarControllerView  {
            
            self.navigationController?.pushViewController(invitePartnerPage, animated: true)
        }
        
    }
    @IBAction func btnChangePasswordAction(_ sender: Any) {
        
      performSegue(withIdentifier: "ChangePasswordSegue", sender: nil)
    
    }
    
    @IBAction func btnChangeLanguageAction(_ sender: Any) {
        
        openLanguageSelector()
        
    }
    
    
    @IBAction func btnAboutUsAction(_ sender: Any) {
        
       // openWebView(urlString: AboutusUrl, title: "About Us".localized(), subTitle: "")
        
        openWebView(urlString: "\("privacy_")\(Localize.currentLanguage)\(".html")", title: "About Us".localized(), subTitle: "")
        
    }
    
    @IBAction func btnSignOutAction(_ sender: Any) {
        
        let alert = UIAlertController.init(title: "Are you sure you want to logout?".localized(), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "No".localized(), style: .cancel, handler: { (action) in
            
        }))
        alert.addAction(UIAlertAction.init(title: "Yes".localized(), style: .default, handler: { (action) in
    
            self.signOut()
            
        }))
        present(alert, animated: true, completion: nil)
        
    }
    func openLanguageSelector() {
        let sBoard = UIStoryboard.init(name: LanguageSelectorSB, bundle: nil)
        
        present(sBoard.instantiateInitialViewController()!, animated: true, completion: nil)
    }
    
    
    @IBAction func callSignIn(_ sender: Any) {
        
        if checkLogin(){
            
        }
        else{
            
            openSignInPage()
        }
        
        
    }
    
    func openHomePage() {
        
        let HomeStoryBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
        let homePage:UINavigationController = HomeStoryBoard.instantiateInitialViewController() as! UINavigationController
        
        view.window?.rootViewController = homePage
    }
    
    @IBAction func btnBecomPartner(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: ProfilePageSB, bundle: nil)
        let profileSignUp:PartnerListViewController = storyBoard.instantiateViewController(withIdentifier: "PartnerListViewController") as! PartnerListViewController

        navigationController?.pushViewController(profileSignUp, animated: true)
    }
    
    func signOut() {
        
        startAnimating()
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/logout?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: nil, completionHandler:{response, jsonObj in
            if let selfObj = weakSelf {
                selfObj.clearUserDetails()
                selfObj.openSignInPage()
            }
            
            weakSelf?.stopAnimating()
        })
    }
    
    @objc func refreshProfileView(notification: NSNotification) {
        
        UserDefaults.standard.set(1, forKey: "refreshForBecomePartner")
        
        let starTexr = NSMutableAttributedString(string: "")
        
        btnBecomePartner.titleLabel?.numberOfLines = 0
        btnBecomePartner.titleLabel?.lineBreakMode = .byWordWrapping
        btnBecomePartner.setAttributedTitle(starTexr, for: .normal)
        btnBecomePartner.isUserInteractionEnabled = false
        heightOfBtnBecomePartner.constant = 0
        
        if let userDetails:userModel = getUserDetail(){
            
            heightViewMembershipDetail.constant = 205
            heightMembershipId.constant = 0
            
            lblMemberName.text = userDetails.displayName ?? ""
            lblMobileNo.text = "\(userDetails.mobileIsd ?? "")\(" ")\(userDetails.mobileNumber ?? "")"
            
            if userDetails.profilePicture?.count ?? 0 > 0 {
                imgViewProfile.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(userDetails.profilePicture ?? "")")), completed: nil)
            }
            
            var isReferralMember:Int = 0
            
            if let userModel = getUserDetail() {
                
                isReferralMember = userModel.isReferralMember ?? 0
                
            }
            

            if userDetails.membershipId?.count ?? 0 > 0 {
                                
                heightViewMembershipDetail.constant = 235
                
                let star = NSAttributedString(string:"\(userDetails.membershipId ?? "")" , attributes: starAttributes)
                
                let starTexr = NSMutableAttributedString(string: "Partner ID  ".localized())
                
                starTexr.append(star)
                
                lblMembershipId.attributedText = starTexr
                
                heightMembershipId.constant = 25
            }
            
            if isReferralMember == 1
            {
                viewPartnerAgreement.alpha = 1
                viewSellerOpportunity.alpha = 1
                viewInvitePartner.alpha = 1
                viewResumeBank.alpha = 1
                heightSellerOpportunities.constant = 60
                heightViewPartnerAgreement.constant = 60
                heightInvitePartner.constant = 60
                heightViewResumeBank.constant = 60
                heightViewProfileList.constant = 540
                
            }
            
            self.view.layoutIfNeeded()
            
        }
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "refreshProfileView"), object: nil)
    }
    
}

