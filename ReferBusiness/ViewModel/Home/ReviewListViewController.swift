//
//  ReviewListViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 10/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ReviewListViewController: AppBaseViewController {

  //  var ratingListModel:[CWReviewMainList]?
    var ratingValue:Float = 0.0
    var branchCode:Int = 1
    var reviewId = 0
    
    @IBOutlet weak var tableViewList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Review".localized()
        
       // tableViewList.tableFooterView = UIView(frame: .zero)
        
        fetchReviewList()
        
    }
    

    //MARK: - API Call
    
    func fetchReviewList() {
        
        startAnimating()
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getBranchRating?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataObj = jsonObj?.dataObj(){
                        do {
                            
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
//                            let apiResponse = try decoder.decode(CWReviewMainListAPIModel.self, from: data)
//                            if let apiResponse:[CWReviewMainList] = apiResponse.recentReviews {
//                                weakSelf?.ratingListModel = apiResponse
//
//                            }
                            weakSelf?.tableViewList.reloadData()
                            // updateResult(homePageData: HomePageData)
                            
                            weakSelf?.stopAnimating()
                            return
                            
                        } catch {
                            //print(error)
                            message = failedToConnectMessage.localized()
                        }
                    }
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
    }

}
extension ReviewListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
    //    return ratingListModel?.count ?? 0
        
        return 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ReviewListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReviewListTableViewCell", for: indexPath) as! ReviewListTableViewCell
        cell.selectionStyle = .none
        
        return getCellForReviewList(cell: cell, indexPath: indexPath)
    }
    
    func  getCellForReviewList(cell: ReviewListTableViewCell, indexPath : IndexPath) -> ReviewListTableViewCell
    {
//        if let model = ratingListModel?[indexPath.row]
//        {
        
//            let formatter = DateFormatter()
//            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//
//            guard let date = formatter.date(from:model.responseDate ?? "") else {
//                fatalError()
//            }
//
//            formatter.dateFormat = "dd-MMM-yyyy hh:mm a"
//            let finalDate = formatter.string(from: date)
//
//            cell.lblTime.text = "\("Reviewed on ")\(finalDate)"
//            cell.lblDes.text = model.comments ?? ""
//
//            cell.btnEdit.alpha = 0
//            cell.btnEdit.addTarget(self, action:#selector(editReviewAction(_:)), for: .touchUpInside)
//            if model.userId == getCurrentUserID()
//            {
//                cell.btnEdit.alpha = 1
//            }
       // }
        
        
        return cell
    }
    
    @objc func editReviewAction(_ sender: UIButton)
    {
        
//        if let reviewList:ReviewsViewController = UIStoryboard.init(name: CarWashSB, bundle: nil).instantiateViewController(withIdentifier: "ReviewsViewController") as? ReviewsViewController
//        {
//            if let model = ratingListModel?[sender.tag]
//            {
//                reviewList.reviewId = model.reviewId ?? 0
//                reviewList.branchCode = branchCode
//                navigationController?.pushViewController(reviewList, animated: true)
//            }
//        }
        
        
    }
    
}

class ReviewListTableViewCell: UITableViewCell {
    
    
    
}

