//
//  ChangeProductVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

protocol ChangeProductVCDelegate: class {
    
    func didChangeProduct()
    
}

class ChangeProductVC: AppBaseViewController {
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblNoFound: NKLabel!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    
    @IBOutlet weak var heightViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var viewBottom: UIView!
    
    var sellerCode:Int = 0
    
    var productList:[ChangeProductModel]?
    
    var productType:Int = 0
    
    var productCode:Int = 0
    
    var selectedIndex:Int = 0
    
    var transactionId:Int = 0
    
    weak var delegate:ChangeProductVCDelegate?
    
     var btnName = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchProductList()
        
        navigationItem.title = "Product/Service Groups".localized()
        
        selectedIndex = productCode
        
        updateNavBar()
        
        updateBottomView()
    }
    
    func updateNavBar() {
        
        btnName.setTitle("Submit -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(submitAction), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    func updateBottomView(){
        
        if selectedIndex != productCode{
            btnName.alpha = 1
            btnSubmit.alpha = 1
            heightViewBottom.constant = 64
            btnSubmit.addTarget(self, action: #selector(submitAction), for: .touchUpInside)
            viewBottom.alpha = 1
        }
        else{
              btnName.alpha = 0
              btnSubmit.alpha = 0
              heightViewBottom.constant = 0
              viewBottom.alpha = 0
        }
        
    }
    
    @objc func submitAction(){

       saveProduct()
        
    }
    
    
    func fetchProductList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getProductListOnLeadPage?token=\(getCurrentUserToken())&transactionId=\(transactionId)&productType=\(productType)&lngId=\(Localize.currentLanguageID)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ChangeModelApiModel.self, from: data)
                                
                                if let apiResponse:[ChangeProductModel] = jsonResponse.productList {
                                    
                                    weakSelf?.productList = apiResponse
                                    
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func saveProduct(){
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = [
            "transactionId":transactionId,
            "productCode":selectedIndex
        ]
        let encryptedParams = ["data": encryptParams(params: params as NSDictionary)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/submitProductChangeOnLeadPage?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
                
            }
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{

                  self.delegate?.didChangeProduct();
                    let isUpdated:[String: Int] = ["update": 1]
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callMessage"), object: nil, userInfo: isUpdated)
                                                       
                let isHomeUpdated:[String: Int] = ["update": 1]
                                                       
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isHomeUpdated)
                    
                  self.navigationController?.popViewController(animated: true)
                                
                                return
                        }
                    }
            
            weakSelf?.stopAnimating()
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
}

extension ChangeProductVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if productList?.count ?? 0 > 0{
            
            tableViewList.alpha = 1
            
            lblNoFound.alpha = 0
            
            return productList?.count ?? 0
        }
        else{
            
            tableViewList.alpha = 0
            
            lblNoFound.alpha = 1
            
            return 0
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:ChangeProductCell = tableView.dequeueReusableCell(withIdentifier: "ChangeProductCell", for: indexPath) as? ChangeProductCell{
            
            
            return getCellForChangeProduct(cell: cell, indexPath: indexPath)
        }
        
        return UITableViewCell.init()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:ChangeProductModel = productList?[indexPath.row]{
            
            selectedIndex = model.productCode ?? 0
            
            updateBottomView()
            
            tableViewList.reloadData()
        }
    }
    
    func getCellForChangeProduct(cell:ChangeProductCell,indexPath:IndexPath) -> ChangeProductCell {
        
        cell.selectionStyle = .none
        
        if let model:ChangeProductModel = productList?[indexPath.row]{
            
            cell.lblProductName.text = model.productName ?? ""
            
            cell.lblProductCode.text = "\(model.productCode ?? 0)"
            
            if model.productLogo?.count ?? 0 > 0 {
                cell.imgViewProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
            }
            
            cell.imgViewCheckbox.alpha = 0
            
            if selectedIndex == model.productCode{
                
                cell.imgViewCheckbox.alpha = 1
                
            }
            
            
        }
        
        return cell
    }
    
}

class ChangeProductCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblProductCode: NKLabel!
    
    @IBOutlet weak var imgViewProduct: UIImageView!
    
    @IBOutlet weak var imgViewCheckbox: UIImageView!
    
}
