//
//  TabViewController.swift
//  ICanRefer
//
//  Created by Vivek Mishra on 15/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation

class TabViewController: AppBaseViewController,CLLocationManagerDelegate,ViewDesigning {

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var viewBottomBar: UIView!
    
    @IBOutlet var buttons: [UIButton]!
    
    @IBOutlet weak var imgViewHome: UIImageView!
    
    @IBOutlet weak var imgViewActivities: UIImageView!
    
    @IBOutlet weak var imgViewMessage: UIImageView!
    
    @IBOutlet weak var lblMessage: NKLabel!
    
    @IBOutlet weak var lblActivities: NKLabel!
    
    @IBOutlet weak var lblHome: NKLabel!
    
    @IBOutlet weak var stackViewTab: UIStackView!
    
    @IBOutlet weak var bottomSpaceForTabBar: NSLayoutConstraint!
    
     @IBOutlet weak var heightOfTabBar: NSLayoutConstraint!
    
    ///
    // new changes........
    
    @IBOutlet weak var imgViewEvents: UIImageView!
    
    @IBOutlet weak var lblEvents: NKLabel!
    
    ///
    
    var homeViewController: UIViewController!
    var searchViewController: UIViewController!
    var accountViewController: UIViewController!
    var eventViewController: UIViewController!
    
    var locationManager = CLLocationManager()
    
    var viewControllers: [UIViewController]!
    
    var currentLoc:CLLocationCoordinate2D?
    
    var selectedIndex: Int = 0
    
    var buyerDetails:BuyerModel?
    
    var referralMemberDetails:ReferralMemberModel?
    
    var banners:[BannerModel]?
    
    var sellerDetails:HomeSellerDetailApiModel?
    
    var memberShipStatusObj:MemberShipStatusObj?
    
    var isBuyer:Int?
    
    var isReferralMember:Int?
    
    var isSeller:Int?
    
    var isLocatePartnerValue:Int?
    
    var isManageSellerValue:Int?
    
    var membershipId:String?
    
    var isFirstTime:Bool?
    
    var btnIndexTag:Int?
    
    var isAccountStatement:Int?
    
    var claimData:ClaimDataModel?
    
    var filterMasterList:[FilterReferralModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ForceSignOut(_:)), name:NSNotification.Name(rawValue: "ForceSignOut"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(onReceiveDataresond(_:)), name:NSNotification.Name(rawValue: "navigateTorespond"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNavigatetoMessages(_:)), name:NSNotification.Name(rawValue: "navigateToMessages"), object: nil)
        
        
        startAnimating()
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                
            case 1334:
                print("iPhone 6/6S/7/8")
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                
            case 2436:
                print("iPhone X, XS")
               
                bottomSpaceForTabBar.constant = 5
                
            case 2688:
                print("iPhone XS Max")
               
                 bottomSpaceForTabBar.constant = 5
                
            case 1792:
                print("iPhone XR")
               
                 bottomSpaceForTabBar.constant = 5
                
            default:
                print("Unknown")
            }
        }
        
        navigationItem.title = "iCanRefer".localized()

        let storyboardSearchSB = UIStoryboard(name: "SearchSB", bundle: nil)//home
        let storyboard = UIStoryboard(name: "Home", bundle: nil)//activities
        let storyboardMessageSB = UIStoryboard(name: "MessageSB", bundle: nil)
        let storyboardEventsSB = UIStoryboard(name: "Events", bundle: nil)
        

        searchViewController = storyboardSearchSB.instantiateViewController(withIdentifier: "SearchMainVC") as! SearchMainVC
        
        homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        accountViewController = storyboardMessageSB.instantiateViewController(withIdentifier: "MessageMainVC") as! MessageMainVC
        
        eventViewController = storyboardEventsSB.instantiateViewController(withIdentifier: "EventsViewController") as! EventsViewController

        viewControllers = [searchViewController, homeViewController, accountViewController, eventViewController]
        
        showFirstViewController()
        
         getCurrentLocation()
        
        lblHome.text = "Home".localized()
        lblActivities.text = "Activities".localized()
        lblMessage.text = "Messages".localized()
        lblEvents.text = "Events".localized()
        
        self.setupShadow(self.viewBottomBar)
        
       // fetchHomeData()
    }
    
   
    
    @objc func onNavigatetoMessages(_ notification:Notification) {
        // Do something now //reload tableview
        
        selectedIndex = 2
        
        buttons[selectedIndex].isSelected = true
        
        didPressTab(buttons[selectedIndex])
    }
    
    @objc func onReceiveDataresond(_ notification:Notification) {
        // Do something now //reload tableview
        let trsnId = UserDefaults.standard.integer(forKey: "transactionId")
        
        let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
        let referralStatusPage:ReferralPostMessageVC = storyBoard.instantiateViewController(withIdentifier: "ReferralPostMessageVC") as! ReferralPostMessageVC
        
        referralStatusPage.transactionId = trsnId
        
        navigationController?.pushViewController(referralStatusPage, animated: true)
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "navigateTorespond"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ForceSignOut"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "navigateToMessages"), object: nil)
    }
    
    @objc func ForceSignOut(_ notification:Notification) {
        // Do something now //reload tableview
        
        self.clearUserDetailsForce()
        self.openSignInPage()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let dict:NSDictionary = UserDefaults.standard.value(forKey: "config") as? NSDictionary {
            
            let isHome:Int =  dict["isHome"] as? Int ?? 0
            let isActivities:Int =  dict["isActivities"] as? Int ?? 0
            let isMessages:Int =  dict["isMessages"] as? Int ?? 0
            let isEvents:Int =  dict["isEvents"] as? Int ?? 0
            
            if isHome == 1 {
                
                stackViewTab.arrangedSubviews[0].isHidden = false
            }
            else
            {
                stackViewTab.arrangedSubviews[0].isHidden = true
            }
            
            if isActivities == 1 {
                
                stackViewTab.arrangedSubviews[1].isHidden = false
            }
            else
            {
                stackViewTab.arrangedSubviews[1].isHidden = true
            }
            
            if isMessages == 1 {
                
                stackViewTab.arrangedSubviews[2].isHidden = false
            }
            else
            {
                stackViewTab.arrangedSubviews[2].isHidden = true
            }
            
            if isEvents == 1 {
                
                stackViewTab.arrangedSubviews[3].isHidden = false
            }
            else
            {
                stackViewTab.arrangedSubviews[3].isHidden = true
            }
            

            if isHome == 0 && isActivities == 0 && isMessages == 0 && isEvents == 0 {

                stackViewTab.arrangedSubviews[0].isHidden = false
                stackViewTab.arrangedSubviews[1].isHidden = false
                stackViewTab.arrangedSubviews[2].isHidden = false
                stackViewTab.arrangedSubviews[3].isHidden = false
            }
        }

    }
    
    func showFirstViewController()
    {
        if let dict:NSDictionary = UserDefaults.standard.value(forKey: "config") as? NSDictionary {
            
            var isHome:Int =  dict["isHome"] as? Int ?? 0
            var isActivities:Int =  dict["isActivities"] as? Int ?? 0
            var isMessages:Int =  dict["isMessages"] as? Int ?? 0
            var isEvents:Int =  dict["isEvents"] as? Int ?? 0
            
            if isHome == 1 {
                
                selectedIndex = 0;
                buttons[selectedIndex].isSelected = true

                let isUpdated:[String: Int] = ["update": 0]

            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callLocation"), object: nil, userInfo: isUpdated)
                
                didPressTab(buttons[selectedIndex])
                
            }
            
            else if isHome == 0 && isActivities == 1 {
                
                selectedIndex = 1
                
                buttons[selectedIndex].isSelected = true
                
                didPressTab(buttons[selectedIndex])
                
            }
            
            else if isHome == 0 && isActivities == 0 && isMessages == 1 {
                
                selectedIndex = 2
                
                buttons[selectedIndex].isSelected = true
                
                didPressTab(buttons[selectedIndex])
                
            }
                
            else if isHome == 0 && isActivities == 0 && isMessages == 0  && isEvents == 1 {
                
                selectedIndex = 3
                
                buttons[selectedIndex].isSelected = true
                
                didPressTab(buttons[selectedIndex])
                
            }
            
            else if isHome == 0 && isActivities == 0 && isMessages == 0 && isEvents == 0 {

                isHome = 1
                
                isActivities = 1
                
                isMessages = 1
                
                isEvents = 1

                selectedIndex = 0;
                buttons[selectedIndex].isSelected = true
                
                didPressTab(buttons[selectedIndex])
            }
            else
            {
                self.stopAnimating()
                
            }
        }
    }
    
    
    @IBAction func didPressTab(_ sender: UIButton) {
        
        let unselectColor = UIColor.colorWithHexString(hexString: "#58595B")!
        let previousIndex = selectedIndex
        selectedIndex = sender.tag

        if previousIndex != selectedIndex || UserDefaults.standard.integer(forKey: "firstTime") == 1 {
            
            UserDefaults.standard.set(1, forKey: "selectedTab")
            UserDefaults.standard.set(0, forKey: "firstTime")
            
            if sender.tag == 0 {
                
                //
//                UserDefaults.standard.set(1, forKey: "homeSelected")
//                UserDefaults.standard.set(0, forKey: "activitiesSelected")
//                UserDefaults.standard.set(0, forKey: "messageSelected")
//                UserDefaults.standard.set(0, forKey: "eventSelected")
                //
                imgViewHome.image = UIImage(named: "homeSelected")
                imgViewActivities.image = UIImage(named: "activities")
                imgViewMessage.image = UIImage(named: "message")
                imgViewEvents.image = UIImage(named: "event")
                
                lblHome.textColor = UIColor.primaryColor
                lblMessage.textColor = unselectColor
                lblActivities.textColor = unselectColor
                lblEvents.textColor = unselectColor
             
                let isUpdated:[String: Int] = ["update": 0]

            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callLocation"), object: nil, userInfo: isUpdated)
                
                fetchSearchData()
            }
            
            if sender.tag == 1 {
                
                //
//                UserDefaults.standard.set(0, forKey: "homeSelected")
//                UserDefaults.standard.set(1, forKey: "activitiesSelected")
//                UserDefaults.standard.set(0, forKey: "messageSelected")
//                UserDefaults.standard.set(0, forKey: "eventSelected")
                //
                
                imgViewActivities.image = UIImage(named: "activitiesSelected")
                imgViewHome.image = UIImage(named: "home")
                imgViewMessage.image = UIImage(named: "message")
                imgViewEvents.image = UIImage(named: "event")
                
                lblActivities.textColor = UIColor.primaryColor
                lblMessage.textColor = unselectColor
                lblHome.textColor = unselectColor
                lblEvents.textColor = unselectColor
                fetchActivitiesData()
                
            }
            
            if sender.tag == 2 {
                
                //
//                UserDefaults.standard.set(0, forKey: "homeSelected")
//                UserDefaults.standard.set(0, forKey: "activitiesSelected")
//                UserDefaults.standard.set(1, forKey: "messageSelected")
//                UserDefaults.standard.set(0, forKey: "eventSelected")
                //
                
                imgViewHome.image = UIImage(named: "home")
                imgViewActivities.image = UIImage(named: "activities")
                imgViewMessage.image = UIImage(named: "messageSelected")
                imgViewEvents.image = UIImage(named: "event")
                
                lblMessage.textColor = UIColor.primaryColor
                lblActivities.textColor = unselectColor
                lblHome.textColor = unselectColor
                lblEvents.textColor = unselectColor
                
                fetchMessageData()
            }
            
            if sender.tag == 3 {
                
                //
//                UserDefaults.standard.set(0, forKey: "homeSelected")
//                UserDefaults.standard.set(0, forKey: "activitiesSelected")
//                UserDefaults.standard.set(0, forKey: "messageSelected")
//                UserDefaults.standard.set(1, forKey: "eventSelected")
                //
                
                imgViewHome.image = UIImage(named: "home")
                imgViewActivities.image = UIImage(named: "activities")
                imgViewMessage.image = UIImage(named: "message")
                imgViewEvents.image = UIImage(named: "eventSelected")
                
                lblMessage.textColor = unselectColor
                lblActivities.textColor = unselectColor
                lblHome.textColor = unselectColor
                lblEvents.textColor = UIColor.primaryColor
                
                fetchEventData()
            }
            
            buttons[previousIndex].isSelected = false
            
            let previousVC = viewControllers[previousIndex]
            
            previousVC.willMove(toParent: nil)
            previousVC.view.removeFromSuperview()
            previousVC.removeFromParent()
            
            sender.isSelected = true
            
            let vc = viewControllers[selectedIndex]
            addChild(vc)
            
            vc.view.frame = contentView.bounds
            contentView.addSubview(vc.view)
            
            vc.didMove(toParent: self)

        }
        
        else
        {
            print("previousIndexpreviousIndex")
            print(previousIndex)
            
            print("selectedIndexselectedIndexselectedIndex")
            print(selectedIndex)
        }

    }
    
    @IBAction func btnMenuClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: ProfilePageSB, bundle: nil)
        let profile:ProfileNewViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileNewViewController") as! ProfileNewViewController
//        profile.memberShipStatusObj = memberShipStatusObj
//        profile.membershipId = membershipId
        navigationController?.pushViewController(profile, animated: true)
    }
//
//    func updateUI() {
//
//        if let userDetails:userModel = getUserDetail(){
//
//            if userDetails.isHome == 1 {
//
//                stackViewTab.arrangedSubviews[0].isHidden = false
//            }
//            else
//            {
//                stackViewTab.arrangedSubviews[0].isHidden = true
//            }
//
//
//            if userDetails.isSearch == 1 {
//
//                stackViewTab.arrangedSubviews[1].isHidden = false
//
//            }
//            else
//            {
//                stackViewTab.arrangedSubviews[1].isHidden = true
//            }
//
//
//            if userDetails.isMessages == 1 {
//
//                stackViewTab.arrangedSubviews[2].isHidden = false
//            }
//            else
//            {
//                stackViewTab.arrangedSubviews[2].isHidden = true
//            }
//        }
//
//    }
    
    func sendDataToVc() {
        
        let vC = homeViewController as! HomeViewController
        
//        vC.dataFromContainer(buyerModel: buyerDetails, referralMemberModel: referralMemberDetails, bannersModel: banners, sellerDetailsModel: sellerDetails, memberShipStatusObjModel: memberShipStatusObj, membershipID: membershipId, isBuyerValue: isBuyer, isReferralMemberValue: isReferralMember, isSellerValue: isSeller)
        
        vC.dataFromContainer(buyerModel: buyerDetails, referralMemberModel: referralMemberDetails, bannersModel: banners, sellerDetailsModel: sellerDetails, memberShipStatusObjModel: memberShipStatusObj, membershipID: membershipId, isBuyerValue: isBuyer, isReferralMemberValue: isReferralMember, isSellerValue: isSeller,isLocatePartner:isLocatePartnerValue,isManageSeller:isManageSellerValue, isAccountStatementValue: isAccountStatement ?? 0, claimDataModel: claimData, filterMasterModel: filterMasterList)
        
    }
    
    func getCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch locationAuthorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.startUpdatingLocation()
                currentLoc = locationManager.location?.coordinate
                
            }
        case .restricted, .denied:
            
          //  showLocationRequiredPage()
            
            print("Access denied")
            
        }
    }
    
    func showLocationRequiredPage(){
        weak var weakSelf = self
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable location access to provide you near by search.".localized(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Enable Location Service".localized(), style: .default, handler: {action in
            
            weakSelf?.openAppSettings()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if  let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
            //print("locations = \(locValue.latitude) \(locValue.longitude)")
            
            currentLoc = locValue
            
        }
        // updateLocationName()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        stopAnimating()
    }
    
    
    func fetchHomeData() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        
        if isFirstTime == true {
        
         //startAnimating()
         isFirstTime = false
            
        }
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/homePage?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(HomeApiModel.self, from: data)
                                
                                if let apiResponse:BuyerModel = jsonResponse.buyerDetails {
                                    
                                    weakSelf?.buyerDetails = apiResponse
                                    
                                }
                                
                                if let apiResponse:ReferralMemberModel = jsonResponse.referralMemberDetails {
                                    
                                    weakSelf?.referralMemberDetails = apiResponse
                                    
                                }
                                if let apiResponse:[BannerModel] = jsonResponse.banners {
                                    
                                    weakSelf?.banners = apiResponse
                                    
                                }
                                
                                if let apiResponse:HomeSellerDetailApiModel = jsonResponse.sellerDetails {
                                    
                                    weakSelf?.sellerDetails = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.isBuyer {
                                    
                                    weakSelf?.isBuyer = apiResponse
                                    
                                }
                                
                                if let apiResponse:[FilterReferralModel] = jsonResponse.filterMasterList {
                                                                   
                                        weakSelf?.filterMasterList = apiResponse
                                                                   
                                }
                               
                                
                                if let apiResponse:Int = jsonResponse.isReferralMember {
                                    
                                    weakSelf?.isReferralMember = apiResponse
                                    
                                    UserDefaults.standard.set(apiResponse, forKey: "isReferralMember")
                                    
                                }
                                
                                //
                                
//                                if let apiResponse:Int = jsonResponse.isLocatePartner {
//                                    
//                                    weakSelf?.isLocatePartnerValue = apiResponse
//                                    
//                                }
                                
                                if let apiResponse:Int = jsonResponse.isManageSeller {
                                    
                                    weakSelf?.isManageSellerValue = apiResponse
                                    
                                }
                                
                                //
                                
                                if let apiResponse:Int = jsonResponse.isSeller {
                                    
                                    weakSelf?.isSeller = apiResponse
                                    
                                }
                                if let apiResponse:MemberShipStatusObj = jsonResponse.memberShipStatusObj {
                                    
                                    weakSelf?.memberShipStatusObj = apiResponse
                                    
                                }
                                if let apiResponse:String = jsonResponse.membershipId {
                                    
                                    weakSelf?.membershipId = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.isAccountStatement {
                                    
                                    weakSelf?.isAccountStatement = apiResponse
                                    
                                }
                                
                                if let apiResponse:ClaimDataModel = jsonResponse.claimData {
                                    
                                    weakSelf?.claimData = apiResponse
                                    
                                }
                                
                                
                                //weakSelf?.updateUI()
                                
                              //  weakSelf?.sendDataToVc()
                                
                                return
                            } catch {
                                print(error)
                                  weakSelf?.startAnimating()
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
    func fetchActivitiesData()
    {
        self.stopAnimating()
    }
    
    func fetchSearchData() {
        
        
        self.stopAnimating()
//        let vC = searchViewController as! SearchMainVC
//
//       vC.searchProduct(search: "", lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0)
    }
    
    func fetchMessageData() {
        
        
        self.stopAnimating()
//        let vC = accountViewController as! MessageMainVC
//
//        vC.fetchMessageList()
    }
    
    func fetchEventData() {
        
        
        self.stopAnimating()
        //        let vC = searchViewController as! SearchMainVC
        //
        //       vC.searchProduct(search: "", lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0)
    }
}

extension TabViewController {
    
    func showLocalNotification(payload:NSDictionary){

        if let type:Int = payload.object(forKey: "type") as? Int{
            
            if let dataString:String = payload.dataString(){
                
                if let dataObj:NSDictionary = self.decryptData(encryptedString: dataString){
                    
                    if type == NotificationTypes.NotificationTransactionPage.rawValue {
                        
                        
                      //  NotificationCenter.default.post(name: Notification.Name("navigateToMessages"), object: nil)
                        
                    }
                        
                    else if type == NotificationTypes.NotificationResponseMessages.rawValue {
                        
//                        if let description:Int = dataObj.object(forKey: "transactionId") as? Int{
//
//                            UserDefaults.standard.set(description, forKey: "transactionId")
//                        }
//
//                        NotificationCenter.default.post(name: Notification.Name("navigateTorespond"), object: nil)
                        
                    }
                        
                    else if type == NotificationTypes.NotificationRenewMembership.rawValue {
                        
                        // fetchHomeData()
                        
                    }
                        
                        //
                        
                    else if type == NotificationTypes.NotificationAnnouncementPage.rawValue {
                        
                        showFestivalLocalNotification(dataObj: dataObj)
                        
                    }
                        
                    else if type == NotificationTypes.NotificationHomePageLoading.rawValue {
                        
                        NotificationCenter.default.post(name: Notification.Name("ReloadNotification"), object: nil)
                        
                    }
                        
                        else if type == NotificationTypes.NotificationForceMessages.rawValue {
                        
                        showFestivalLocalNotification(dataObj: dataObj)
                        
                        UserDefaults.standard.set(true, forKey: "logoutForce") //Bool
                    }
                    
                    else if type == NotificationTypes.NotificationHomePageRefresh.rawValue {
                                           
                        NotificationCenter.default.post(name: Notification.Name("ReloadHomePage"), object: nil)
                                           
                        }
                    ///
                    
                }
                
            }
        }
        
    }
    
    
    func handleNotificationTap(payload:NSDictionary){
        
        if let type:Int = payload.object(forKey: "type") as? Int{
            
            if let dataString:String = payload.dataString(){
                
                if let dataObj:NSDictionary = self.decryptData(encryptedString: dataString){
                    
                    if type == NotificationTypes.NotificationTransactionPage.rawValue {
                        
                        
                        NotificationCenter.default.post(name: Notification.Name("navigateToMessages"), object: nil)
                        
                    }
                        
                    else if type == NotificationTypes.NotificationResponseMessages.rawValue {
                        
                        if let description:Int = dataObj.object(forKey: "transactionId") as? Int{
                            
                            UserDefaults.standard.set(description, forKey: "transactionId")
                        }
                        
                        NotificationCenter.default.post(name: Notification.Name("navigateTorespond"), object: nil)
                        
                    }
                        
                    else if type == NotificationTypes.NotificationRenewMembership.rawValue {
                        
                       // fetchHomeData()
                        
                    }
                      
                        
                    else if type == NotificationTypes.NotificationAnnouncementPage.rawValue {
                        
                        showFestivalLocalNotification(dataObj: dataObj)
                        
                    }
                        
                    else if type == NotificationTypes.NotificationHomePageLoading.rawValue {
                        
                        NotificationCenter.default.post(name: Notification.Name("ReceiveData"), object: nil)

                    }
                        
                    else if type == NotificationTypes.NotificationForceMessages.rawValue {
                        
                        showFestivalLocalNotification(dataObj: dataObj)
                        
                        UserDefaults.standard.set(true, forKey: "logoutForce") //Bool
                    }
                    
                    else if type == NotificationTypes.NotificationHomePageRefresh.rawValue {
                                                              
                        NotificationCenter.default.post(name: Notification.Name("ReloadHomePage"), object: nil)
                                                              
                    }
                    ///
                    
                }
                
            }
        }
        
    }
    
    
    func showFestivalLocalNotification(dataObj:NSDictionary){
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        
        let home = HomePopUpView.init(frame: UIScreen.main.bounds)
        
        home.contentViewHeightConstraint.constant = 120
        
        home.heightImageViewConstraint.constant = 0
        
        home.heightDescriptionConstraint.constant = 0
        
        if let title:String = dataObj.object(forKey: "title") as? String{
            
            home.lblTitle.text = title
            
            if let imagePath:String = dataObj.object(forKey: "image") as? String{
                
                let picUrl = imagePath
                if picUrl.count > 0 {
                    if let url:URL = URL.init(string: "\(StorageUrl)\(picUrl )"){
                        
                        let cellFrame = home.imgviewPopup.frame.size
                        
                        home.imgviewPopup.sd_setImage(with: url, completed: { (theImage, error, cache, url) in
                            
                            home.heightImageViewConstraint.constant = self.getAspectRatioAccordingToiPhones(cellImageFrame: cellFrame, downloadedImage: theImage!)
                            
                            home.contentViewHeightConstraint.constant = home.contentViewHeightConstraint.constant + home.heightImageViewConstraint.constant
                            
                        })
                    }
                }
            }
            
            if let description:String = dataObj.object(forKey: "description") as? String{
                
                home.heightDescriptionConstraint.constant = 80
                
                home.contentViewHeightConstraint.constant = home.contentViewHeightConstraint.constant + 80
                
                home.lblDescription.text = description
            }
            
            currentWindow?.addSubview(home)
        }
    }
    
    
    
    func getAspectRatioAccordingToiPhones(cellImageFrame:CGSize,downloadedImage: UIImage)->CGFloat {
        
        let widthOffset = downloadedImage.size.width - cellImageFrame.width
        
        let widthOffsetPercentage = (widthOffset*100)/downloadedImage.size.width
        
        let heightOffset = (widthOffsetPercentage * downloadedImage.size.height)/100
        
        let effectiveHeight = downloadedImage.size.height - heightOffset
        
        return(effectiveHeight)
    }
    
    
}
