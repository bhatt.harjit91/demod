//
//  HomeBuyerProductDetailVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 10/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import WebKit
class HomeBuyerProductDetailVC: SendOTPViewController,CurrencySelectorVCDelegate,UITextFieldDelegate {
    
    var Activity:UIActivityIndicatorView!
    
    @IBOutlet weak var imgViewBanner: UIImageView!
    
    @IBOutlet weak var imgViewLogo: UIImageView!
    
    @IBOutlet weak var lblCompanyName: NKLabel!
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblProductDes: NKLabel!
    
    @IBOutlet weak var lblReferredBy: NKLabel!
    
    @IBOutlet weak var lblStatus: NKLabel!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var viewAmountPaid: RCView!
    
    @IBOutlet weak var heightAmountPaid: NSLayoutConstraint!
    
    @IBOutlet weak var viewPurchaseProof: RCView!
    
    @IBOutlet weak var heightViewPurchaseProof: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfImageViewBannerConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewAttachments: UICollectionView!
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
    @IBOutlet weak var heightTxtViewNotes: NSLayoutConstraint!
    
    @IBOutlet weak var viewBgImageBanner: UIView!
    @IBOutlet weak var viewRating: RCView!
    
    @IBOutlet weak var btnPostReview: NKButton!
    
    @IBOutlet weak var lblTime: NKLabel!
    
    @IBOutlet weak var btnEdit: NKButton!
    
    @IBOutlet weak var lblDes: UILabel!
    
    @IBOutlet weak var btnHeightheightOfSubmitBtnBGConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var txtCurrencyCode: RCTextField!
    
    @IBOutlet weak var txtAmount: RCTextField!
    
    @IBOutlet weak var imgViewAttachment: UIImageView!
    
    @IBOutlet weak var heightViewStatusAndAmount: NSLayoutConstraint!
    
    @IBOutlet weak var heightBtnPostReview: NSLayoutConstraint!
    
    @IBOutlet weak var topheightOfRatingViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var lblRating: NKLabel!
    @IBOutlet weak var btnAttachment: UIButton!
    
    @IBOutlet weak var lineView: UIView!
    
    @IBOutlet weak var btnSelectCurrencyType: UIButton!
    @IBOutlet weak var viewAttachementBtnBG: RCView!
    var selectedCurrencyCode : CurrencyModel?
    
    @IBOutlet weak var heightOfStatusBgViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblStatusTitleText: NKLabel!
    
    @IBOutlet weak var heightViewOfCollectionViewBG: NSLayoutConstraint!
    
    @IBOutlet weak var topHeightOfBtnPostreview: NSLayoutConstraint!
    
    @IBOutlet weak var lblSugesstionTitle: NKLabel!
    
    @IBOutlet weak var lblSugesstions: NKLabel!
    
    @IBOutlet weak var lblSpecialTermsTitle: NKLabel!
    
    @IBOutlet weak var lblSpecialTerms: NKLabel!
    
    @IBOutlet weak var viewSpecialTerms: RCView!
    
    @IBOutlet weak var heightViewSpecialTerms: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceViewSpecialTerms: NSLayoutConstraint!
    
    @IBOutlet weak var btnChange: NKButton!
    
    @IBOutlet weak var viewInvoice: UIView!
    
    @IBOutlet weak var btnViewInvoice: UIButton!
    
    @IBOutlet weak var btnSendInvoice: UIButton!
    
    @IBOutlet weak var heightViewInvoice: NSLayoutConstraint!
    
    @IBOutlet weak var viewAppointmentTime: UIView!
    
    @IBOutlet weak var heightViewAppointmentTime: NSLayoutConstraint!
    
    @IBOutlet weak var lblDate: NKLabel!
    
    @IBOutlet weak var lblMemberCount: NKLabel!
    
    @IBOutlet weak var btnSeeMembers: UIButton!
    
    var videoLink:String?
    
    var transactionId:Int?
    var comingFromArchive:Bool = false
    var selectedProgressValue:Bool = false
    
    var attachments:[Any] = []
    
    var masterStageList:[BuyerStageListModel]?
    
    var transactionDetails:TransactionBuyerDetailModel?
    
    var reviewDetails:TransactionBuyerRatingModel?
    
    var buyerStageId:Int = 0
    
    var buyerPaidCurrencyId:Int = 0
    
    var currency:[CurrencyModel]?
    
    let starAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 4),
        .foregroundColor: UIColor.getGradientColor(index: 5).first ?? 0]
    
    var isReferralMember:Int = 0
    
    var isBuyer:Int = 0
    
    var productType:Int = 0
    
    var transId:Int = 0
    
    let lineAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationItem.title = "iCanRefer".localized()
        
        imgViewAttachment.setImageColor(color: .white)
        
        heightAmountPaid.constant = 0
        viewAmountPaid.isHidden = true
        viewPurchaseProof.isHidden = true
        heightViewPurchaseProof.constant = 0
        heightViewStatusAndAmount.constant = 260
        
        viewInvoice.alpha = 0
        heightViewInvoice.constant = 0
        
        fetchUserList()
        
        viewSpecialTerms.alpha = 0
        heightViewSpecialTerms.constant = 0
        topSpaceViewSpecialTerms.constant = 0
        
        txtCurrencyCode.isUserInteractionEnabled = false
        
        btnViewInvoice.setAttributedTitle(NSAttributedString(string: "View Invoice".localized(), attributes: lineAttributes), for: .normal)
        
        btnSendInvoice.setAttributedTitle(NSAttributedString(string: "Send Invoice".localized(), attributes: lineAttributes), for: .normal)
        
        btnViewInvoice.addTarget(self, action: #selector(viewInvoiceAction), for: .touchUpInside)
        
        btnSendInvoice.addTarget(self, action: #selector(sendInvoiceAction), for: .touchUpInside)
        
        if let userModel = getUserDetail() {
            
            isReferralMember = userModel.isReferralMember ?? 0
            isBuyer = userModel.isBuyer ?? 0
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        topSpaceViewSpecialTerms.constant = 0
        viewSpecialTerms.alpha = 0
        heightViewSpecialTerms.constant = 0
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
          Activity.stopAnimating()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
         Activity.stopAnimating()
    }
    
    func updateUI() {
        
        lblStatusTitleText.text = ""
        
        if let model:TransactionBuyerDetailModel = transactionDetails {
            
            viewAppointmentTime.alpha = 0
            heightViewAppointmentTime.constant = 0
            
            if model.isAppointment == 1 {
                viewAppointmentTime.alpha = 1
                heightViewAppointmentTime.constant = 100
                
                lblDate.text = model.appointmentDT ?? ""
                
                lblMemberCount.text = "\(model.noOfPeople ?? 0)"
                
                
                let count = model.resourceCount ?? 0
                
                
                if count > 1{
                    
                    let str1 = NSAttributedString(string: "\(model.resourceTitle ?? "")(\(count - 1) More)", attributes: lineAttributes)
                    
                    btnSeeMembers.setAttributedTitle(str1, for: .normal)
                    
                }
                else{
                    
                    let str1 = NSAttributedString(string: "\(model.resourceTitle ?? "")(\(count - 1) More)", attributes: lineAttributes)
                    
                    btnSeeMembers.setAttributedTitle(str1, for: .normal)
                }
                
                
                btnSeeMembers.addTarget(self, action: #selector(btnSelectAppointmentAction(sender:)), for: .touchUpInside)
            }
            
            productType = model.productType ?? 0
            
            if model.productType != 2 {
                
                viewInvoice.alpha = 0
                heightViewInvoice.constant = 0
                
                for masterStageItem in masterStageList ?? []
                {
                    if buyerStageId == masterStageItem.stageId
                    {
                        if masterStageItem.progressValue == 100
                        {
                            selectedProgressValue = true
                            
                            if comingFromArchive == true
                            {
                                lblStatusTitleText.text = masterStageItem.stageTitle
                                heightViewOfCollectionViewBG.constant = 0
                                heightOfStatusBgViewConstraint.constant = 95 - 50
                                heightAmountPaid.constant = 85
                                viewAmountPaid.isHidden = false
                                viewPurchaseProof.isHidden = false
                                heightViewPurchaseProof.constant = 150
                                heightViewStatusAndAmount.constant = 225 + 85 + 150 - 50
                                
                            }
                            else
                            {
                                
                                heightAmountPaid.constant = 85
                                viewAmountPaid.isHidden = false
                                viewPurchaseProof.isHidden = false
                                heightViewPurchaseProof.constant = 150
                                heightViewStatusAndAmount.constant = 225 + 85 + 150
                                
                            }
                            
                            break;
                        }
                        else
                        {
                            selectedProgressValue = false
                            
                            
                            if comingFromArchive == true
                            {
                                lblStatusTitleText.text = masterStageItem.stageTitle
                                heightViewOfCollectionViewBG.constant = 0
                                heightViewStatusAndAmount.constant = 375 - 50
                                heightAmountPaid.constant = 0
                                viewAmountPaid.isHidden = true
                                viewPurchaseProof.isHidden = true
                                heightViewPurchaseProof.constant = 0
                                heightOfStatusBgViewConstraint.constant = 95 - 50
                                heightViewStatusAndAmount.constant = 225 - 50
                                
                            }
                            else
                            {
                                heightAmountPaid.constant = 0
                                viewAmountPaid.isHidden = true
                                viewPurchaseProof.isHidden = true
                                heightViewPurchaseProof.constant = 0
                                heightViewStatusAndAmount.constant = 225
                                
                            }
                        }
                    }
                }
            }
            else{
                
                btnChange.alpha = 0
                
                for masterStageItem in masterStageList ?? []
                {
                    if buyerStageId == masterStageItem.stageId
                    {
                        if masterStageItem.progressValue == 100
                        {
                            viewInvoice.alpha = 1
                            heightViewInvoice.constant = 40
                            
                            selectedProgressValue = true
                            
                            if comingFromArchive == true
                            {
                                lblStatusTitleText.text = masterStageItem.stageTitle
                                heightViewOfCollectionViewBG.constant = 0
                                heightOfStatusBgViewConstraint.constant = 95 - 50
                                heightAmountPaid.constant = 85
                                viewAmountPaid.isHidden = false
                                viewPurchaseProof.isHidden = false
                                heightViewPurchaseProof.constant = 150
                                heightViewStatusAndAmount.constant = 225 + 85 + 150
                                
                            }
                            else
                            {
                                
                                heightAmountPaid.constant = 85
                                viewAmountPaid.isHidden = false
                                viewPurchaseProof.isHidden = false
                                heightViewPurchaseProof.constant = 150
                                heightViewStatusAndAmount.constant = 225 + 85 + 150
                                
                            }
                            
                            break;
                        }
                        else
                        {
                            selectedProgressValue = false
                            
                            viewInvoice.alpha = 0
                            heightViewInvoice.constant = 0
                            
                            if comingFromArchive == true
                            {
                                lblStatusTitleText.text = masterStageItem.stageTitle
                                heightViewOfCollectionViewBG.constant = 0
                                heightViewStatusAndAmount.constant = 375 - 50
                                heightAmountPaid.constant = 0
                                viewAmountPaid.isHidden = true
                                viewPurchaseProof.isHidden = true
                                heightViewPurchaseProof.constant = 0
                                heightOfStatusBgViewConstraint.constant = 95 - 50
                                heightViewStatusAndAmount.constant = 225 + 50
                                
                            }
                            else
                            {
                                heightAmountPaid.constant = 0
                                viewAmountPaid.isHidden = true
                                viewPurchaseProof.isHidden = true
                                heightViewPurchaseProof.constant = 0
                                heightViewStatusAndAmount.constant = 275 + 50
                                
                            }
                        }
                    }
                }
            }
            
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
        dateFormatterPrint.timeZone = .current
        
        
        if let model:TransactionBuyerDetailModel = transactionDetails {
            
            if model.productType == 1 {
                
                heightOfImageViewBannerConstraint.constant = 0
            }
            else
            {
                if model.banners?.count ?? 0 > 0 {
                    
                    if model.isVideo == 1 {
                        
                        Activity = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
                        Activity.transform = CGAffineTransform(scaleX: 2, y: 2)
                        
                        if let fileName = model.banners?.first
                        {
                            
                            if let imagePath = fileName["cdnPath"]{
                                ///////
                                guard let youtubeURL = URL(string: "https://www.youtube.com/embed/\(imagePath.youtubeID ?? "")") else { return }
                                
                                let webView = WKWebView()
                                webView.frame = viewBgImageBanner.frame
                                   Activity.center = webView.center
                                   webView.addSubview(Activity)
                                viewBgImageBanner.addSubview(webView)
                                viewBgImageBanner.layoutIfNeeded()
                                webView.navigationDelegate = self
                                webView.uiDelegate = self
                                webView.load(URLRequest(url: youtubeURL))
                                ////////
                                
                                imgViewBanner.isHidden = true
                                
                            }
                        }
                        
                    }else{
                        
                        viewBgImageBanner.aspectRation(9.0 / 16.0).isActive = true
                        
                        if let fileName = model.banners?.first
                        {
                            if let imagePath = fileName["cdnPath"]
                            {
                                if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                                    // cell.imgViewHeader.sd_setImage(with: url, completed: nil)
                                    imgViewBanner.isHidden = false
                                    imgViewBanner.sd_setImage(with:url, placeholderImage:UIImage.init(imageLiteralResourceName: "noImageIcon"), options:.init(rawValue: 0), completed: nil)
                                }
                            }
                            
                        }
                    }
                }
                else
                {
                    heightOfImageViewBannerConstraint.constant = 0
                }
            }
            
            if isBuyer == 0 && model.referralNoteToSeller?.count ?? 0 > 0{
                
                lblSpecialTermsTitle.text = "Notes to Seller".localized()
                
                lblSpecialTerms.text = model.referralNoteToSeller ?? ""
                
                topSpaceViewSpecialTerms.constant = 5
                viewSpecialTerms.alpha = 1
                heightViewSpecialTerms.constant = 120
            }
            
            
            if  model.specialTerms?.count ?? 0 > 0{
                
                lblSugesstions.text = model.specialTerms ?? ""
                lblSugesstionTitle.text = "General Notes".localized()
                
                topSpaceViewSpecialTerms.constant = 5
                viewSpecialTerms.alpha = 1
                heightViewSpecialTerms.constant = 120
            }
            
            if isBuyer == 1 {}
            
            
            ////--------------------
            
            if model.buyerAttachments?.count ?? 0 > 0 {
                
                attachments = model.buyerAttachments ?? []
            }
            
            ///---------------------
            
            if model.companyLogo?.count ?? 0 > 0 {
                imgViewLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.companyLogo ?? "")")), completed: nil)
            }
            
            lblProductName.text = model.productName ?? ""
            
            let str = model.description ?? ""
            
            lblProductDes.text = str.removeWhitespace()
            
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.referredDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            lblReferredBy.text = "\("Referred".localized())\(" on ")\(dateText)"
            
            if model.referredBy?.count ?? 0 > 0 {
                
                lblReferredBy.text = "\("Referred by ".localized())\(model.referredBy ?? "")\(" on ")\(dateText)"
                
            }
            lblCompanyName.text = model.sellerCompanyName ?? ""
            
        }
        
        viewRating.alpha = 0
        
        btnPostReview.alpha = 1
        heightBtnPostReview.constant = 40
        
        
        
        if let modelRating:TransactionBuyerRatingModel = reviewDetails {
            
            if let date = dateFormatterGet.date(from: modelRating.crDate ?? "") {
                
                lblTime.text = "\("Reviewed on ".localized())\(dateFormatterPrint.string(from: date))"
                
            }
            
            var uId = 0
            if let userId:Int = getCurrentUserID(){
                
                uId = userId
            }
            
            if modelRating.reviewId == uId {
                
                btnEdit.alpha = 1
                
                btnEdit.isUserInteractionEnabled = true
                
                lblRating.alpha = 0
                
                lblRating.text = ""
            }
            else
            {
                btnEdit.alpha = 0
                
                btnEdit.isUserInteractionEnabled = false
                
                lblRating.alpha = 1
                
                let star = NSMutableAttributedString(string:"★ " , attributes: starAttributes)
                
                let starTexr = NSAttributedString(string: "\(modelRating.rating ?? 0)")
                
                star.append(starTexr)
                
                lblRating.attributedText = star
                
            }
            
            lblDes.text = modelRating.comments ?? ""
            
            if reviewDetails?.crDate?.count ?? 0 > 0 {
                
                viewRating.alpha = 1
                
                btnPostReview.alpha = 0
                heightBtnPostReview.constant = 0
                
            }
            
        }
        
        if !txtViewNotes.text.isEmpty
        {
            txtViewNotes.placeholder = ""
        }
        
        if comingFromArchive == true
        {
            
            heightBtnPostReview.constant = 0
            topHeightOfBtnPostreview.constant = 0
            topheightOfRatingViewConstraint.constant = 3
            
            lineView.isHidden = true
            
            if txtViewNotes.text.isEmpty
            {
                txtViewNotes.isHidden = true
                heightTxtViewNotes.constant = 0
                
                var height = heightViewStatusAndAmount.constant
                height -= 100
                heightViewStatusAndAmount.constant = height
            }
            
            btnSelectCurrencyType.isUserInteractionEnabled = false
            btnEdit.isUserInteractionEnabled = false
            btnSubmit.isUserInteractionEnabled = false
            btnPostReview.isUserInteractionEnabled = false
            btnAttachment.isUserInteractionEnabled = false
            txtAmount.isUserInteractionEnabled = false
            
            btnEdit.isHidden = true
            btnSubmit.isHidden = true
            btnPostReview.isHidden = true
            btnAttachment.isHidden = true
            viewAttachementBtnBG.isHidden = true
            
            collectionViewStatus.isUserInteractionEnabled = false
            btnHeightheightOfSubmitBtnBGConstraint.constant = 0
            txtViewNotes.isUserInteractionEnabled = false
            
        }
        
        collectionViewStatus.reloadData()
        collectionViewAttachments.reloadData()
        
    }
    
    @objc func btnSelectAppointmentAction(sender: UIButton){
           
           if let model:TransactionBuyerDetailModel = transactionDetails {
              
           let storyBoard = UIStoryboard.init(name: AppointmentSB, bundle: nil)
              
             let appointmentPage:AppointmentViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentViewController") as! AppointmentViewController
              
               appointmentPage.appointmentId = model.appointmentId ?? 0
                   
               navigationController?.pushViewController(appointmentPage, animated: true)
                   
               
           }
           
       }
    
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.collectionViewStatus.contentOffset.y ,width : self.collectionViewStatus.frame.width,height : self.collectionViewStatus.frame.height)
        self.collectionViewStatus.scrollRectToVisible(frame, animated: true)
        
    }
    
    @IBAction func btnAttachAction(_ sender: UIButton) {
        
        isAttachment = true
        isDocument = true
        openImagePicker()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            //   weakSelf?.stopAnimating()
            if status {
                print(urlString, imageName)
                var arrayEachImage : [String : String] = [:]
                arrayEachImage["cdnPath"] = urlString
                arrayEachImage["fileName"] = imageName
                weakSelf?.attachments.append(arrayEachImage)
                weakSelf?.collectionViewAttachments.reloadData()
                
                self.isAttachment = false
            }
            
        }
        
    }
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachments.count > sender.tag {
            attachments.remove(at: sender.tag)
            collectionViewAttachments.reloadData()
        }
    }
    
    @objc func viewInvoiceAction() {
        
        openWebView(urlString: transactionDetails?.receiptCdnPath ?? "", title: "Viewer".localized(), subTitle: "")
    }
    
    @objc func sendInvoiceAction() {
        
        sendInvoice(transactionID: transactionDetails?.transactionId ?? 0)
        
    }
    
    
    @IBAction func btnPayNow(_ sender: NKButton) {
        
        
        
        
        
        
    }
    
    @IBAction func btnPostReviewAction(_ sender: UIButton) {
        
        if let transactionModel:TransactionBuyerDetailModel = transactionDetails {
            
            let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
            let postReviews:PostReviewsVC = storyBoard.instantiateViewController(withIdentifier: "PostReviewsVC") as! PostReviewsVC
            postReviews.sellerCode = transactionModel.sellerCode ?? 0
            postReviews.productCode = transactionModel.productCode ?? 0
            postReviews.transactionId = transactionModel.transactionId ?? 0
            postReviews.delegate = self
            navigationController?.pushViewController(postReviews, animated: true)
            
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        txtAmount.showError(message: "")
        
        return true
    }
    
    
    @IBAction func submitTransactionAction(_ sender: NKButton) {
        
        if selectedProgressValue == true {
            
            if txtCurrencyCode.text?.count ?? 0 <= 0{
                
                txtCurrencyCode.showError(message: "Required".localized())
                return
            }
            if txtAmount.text?.count ?? 0 <= 0{
                
                txtAmount.showError(message: "Required".localized())
                return
            }
                
            else if attachments.count == 0 {
                
                showErrorMessage(message: "Please upload purchase proof".localized())
                return
            }
                
            else
            {
                saveTransaction()
            }
        }
        else
        {
            txtAmount.text = "0.0"
            attachments = []
            saveTransaction()
            
        }
        
    }
    
    func finalPage(message:String?) {
        
        if  let finalPage:ReferralSuccessVC = UIStoryboard.init(name: SearchSB, bundle: nil).instantiateViewController(withIdentifier: "ReferralSuccessVC") as? ReferralSuccessVC{
            finalPage.message = message ?? ""
            navigationController?.present(finalPage, animated: true, completion: nil)
            navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    
    @IBAction func btnEditAction(_ sender: UIButton) {
        
        if let transactionModel:TransactionBuyerDetailModel = transactionDetails {
            
            let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
            let postReviews:PostReviewsVC = storyBoard.instantiateViewController(withIdentifier: "PostReviewsVC") as! PostReviewsVC
            
            postReviews.sellerCode = transactionModel.sellerCode ?? 0
            postReviews.productCode = transactionModel.productCode ?? 0
            postReviews.transactionId = transactionModel.transactionId ?? 0
            postReviews.delegate = self
            navigationController?.pushViewController(postReviews, animated: true)
            
        }
        
        
    }
    
    @IBAction func btnChangeAction(_ sender: UIButton) {
        
        if let transactionModel:TransactionBuyerDetailModel = transactionDetails {
            
            let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
            if  let changeProduct:ChangeProductVC = storyBoard.instantiateViewController(withIdentifier: "ChangeProductVC") as? ChangeProductVC {
                
                changeProduct.sellerCode = transactionModel.sellerCode ?? 0
                changeProduct.productCode = transactionModel.productCode ?? 0
                changeProduct.productType = transactionModel.productType ?? 0
                changeProduct.transactionId = transactionModel.transactionId ?? 0
                changeProduct.delegate = self
                navigationController?.pushViewController(changeProduct, animated: true)
                
            }
        }
        
    }
    
    func sendInvoice(transactionID:Int){
        
        let apiClient:APIClient = APIClient()
        
        let params:NSDictionary = ["transactionID":transactionID]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        weak var weakSelf = self
        startAnimatingAfterSubmit()
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/sendTransactionInvoice?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            weakSelf?.stopAnimating()
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            if let message:String = dataObj.object(forKey: "message") as? String{
                                
                                weakSelf?.showInfoMessage(message: message)
                                
                            }
                            
                        }
                    }
                }
                
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    func fetchUserList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/transactionDetailPageForBuyer?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&transactionId=\(transactionId ?? 0)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(TransactionBuyerDetailApiModel.self, from: data)
                                
                                if let apiResponse:[BuyerStageListModel] = jsonResponse.masterStageList {
                                    
                                    weakSelf?.masterStageList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currencyList {
                                    
                                    weakSelf?.currency = apiResponse
                                    
                                }
                                
                                if let apiResponse:TransactionBuyerRatingModel = jsonResponse.reviewDetails {
                                    
                                    weakSelf?.reviewDetails = apiResponse
                                    
                                }
                                
                                if let apiResponse:TransactionBuyerDetailModel = jsonResponse.transactionDetails {
                                    
                                    weakSelf?.transactionDetails = apiResponse
                                    
                                    weakSelf?.buyerStageId = apiResponse.buyerStageId ?? 0
                                    
                                    weakSelf?.buyerPaidCurrencyId = apiResponse.buyerPaidCurrencyId ?? 0
                                    
                                    weakSelf?.txtCurrencyCode.text = apiResponse.buyerPaidCurrencySymbol ?? ""
                                    
                                    if apiResponse.buyerPaidAmount ?? 0.0 > 0.0 {
                                        
                                        weakSelf?.txtAmount.text = "\(apiResponse.buyerPaidAmount ?? 0.0)"
                                        
                                    }
                                    
                                    weakSelf?.txtViewNotes.text = apiResponse.buyerComments ?? ""
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func saveTransaction(){
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = [
            "transactionId":transactionDetails?.transactionId ?? 0,
            "buyerPaidAmount":txtAmount.text ?? "",
            "buyerStageId": buyerStageId,
            "buyerPaidCurrencyId": buyerPaidCurrencyId,
            "buyerComments": txtViewNotes.text ?? "",
            "buyerAttachments":attachments
            
        ]
        let encryptedParams = ["data": encryptParams(params: params as NSDictionary)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        
        print("paramsparamsparamsparamsparamsparams")
        print(params)
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/updateTransactionByBuyer?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
                
            }
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String = jsonObj?.dataString() {
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                
                                print("Uploaded")
                                
                                
                                if let messageText:String = dataObj.object(forKey: "successMessage") as? String{
                                    
                                    let isUpdated:[String: Int] = ["update": 1]
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callMessage"), object: nil, userInfo: isUpdated)
                                    
                                    let isHomeUpdated:[String: Int] = ["update": 1]
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isHomeUpdated)
                                    
                                    if self.selectedProgressValue == true {
                                        weakSelf?.finalPage(message:messageText)
                                    }
                                    else{
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                                
                                return
                            }
                        }
                    }
                }
            }
            
            
            weakSelf?.stopAnimating()
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    @IBAction func btnSelectCurrencyType(_ sender: Any) {
        
        openCurrencySelector()
        
    }
    
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
        
        buyerPaidCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        
        txtCurrencyCode.text = selectedCurrencyCode?.currencySymbol ?? ""
        
    }
    
    
}


class HomebuyerStatusCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var viewStatusCellBackground: RCView!
    
}


extension HomeBuyerProductDetailVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            if masterStageList?.count ?? 0 <= 2
            {
                let countList:Int = masterStageList?.count ?? 0
                
                let myHeight = CGFloat(countList)
                
                return CGSize.init(width: collectionViewStatus.frame.width / myHeight, height: 40.0)
            }
            else
            {
                return CGSize.init(width: collectionViewStatus.frame.width / 3, height: 40.0)
            }
            
        }
        
        if collectionView == collectionViewAttachments
            
        {
            return CGSize.init(width: 100, height: 100)
            
        }
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewStatus {
            
            return masterStageList?.count ?? 0
            
        }
        if collectionView == collectionViewAttachments {
            
            return attachments.count
            
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewAttachments
        {
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachment(cell: cell, indexPath: indexPath)
                
            }
        }
        else {
            
            if let cell:HomebuyerStatusCell = collectionViewStatus.dequeueReusableCell(withReuseIdentifier: "HomebuyerStatusCell", for: indexPath) as? HomebuyerStatusCell {
                
                return getCellForReferralStatusCell(cell: cell, indexPath: indexPath)
                
                
            }
        }
        return UICollectionViewCell.init()
    }
    
    func getCellForReferralStatusCell(cell:HomebuyerStatusCell,indexPath:IndexPath) -> HomebuyerStatusCell {
        
        if let model:BuyerStageListModel = masterStageList?[indexPath.row]{
            
            cell.lblTitle.text = model.stageTitle ?? ""
            
            if buyerStageId == model.stageId {
                
                cell.viewStatusCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                
                cell.viewStatusCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        
        return cell
    }
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
        
        if comingFromArchive == true
        {
            cell.btnDelete.isUserInteractionEnabled = false
            cell.btnDelete.isHidden = true
            
        }
        else
        {
            cell.btnDelete.isUserInteractionEnabled = true
            cell.btnDelete.isHidden = false
            cell.btnDelete .addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        }
        
        if let fileName = attachments[indexPath.row] as? [String : String]
        {
            if let imagePath = fileName["cdnPath"]
            {
                if let filePath = fileName["fileName"] {
                    
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            cell.imgView.sd_setImage(with: url, completed: nil)
                            
                        }
                        else{
                            cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                        }
                        
                    }
                }
                
                
                
            }
            
        }
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionViewStatus
            
        {
            
            if let model:BuyerStageListModel = masterStageList?[indexPath.row]{
                
                if productType != 2 {
                    
                    if model.progressValue == 100
                    {
                        heightAmountPaid.constant = 85
                        viewAmountPaid.isHidden = false
                        
                        ///////////need to take
                        
                        viewPurchaseProof.isHidden = false
                        heightViewPurchaseProof.constant = 150
                        
                        heightViewStatusAndAmount.constant = 225 + 85 + 150
                        
                        ///////
                        selectedProgressValue = true
                    }
                    else
                    {
                        heightAmountPaid.constant = 0
                        
                        viewAmountPaid.isHidden = true
                        
                        //////
                        
                        viewPurchaseProof.isHidden = true
                        heightViewPurchaseProof.constant = 0
                        heightViewStatusAndAmount.constant = 225
                        
                        selectedProgressValue = false
                    }
                    
                    buyerStageId = model.stageId ?? 0
                    collectionViewStatus.reloadData()
                    
                }
                else{
                    
                    viewInvoice.alpha = 0
                    heightViewInvoice.constant = 0
                    
                    if model.progressValue == 100
                    {
                        if transactionDetails?.receiptCdnPath?.count ?? 0 > 0 {
                            viewInvoice.alpha = 1
                            heightViewInvoice.constant = 40
                        }
                        
                        heightAmountPaid.constant = 85
                        viewAmountPaid.isHidden = false
                        
                        ///////////need to take
                        
                        viewPurchaseProof.isHidden = false
                        heightViewPurchaseProof.constant = 150
                        
                        heightViewStatusAndAmount.constant = 225 + 85 + 150 + 40
                        
                        ///////
                        
                        selectedProgressValue = true
                    }
                    else
                    {
                        viewInvoice.alpha = 0
                        heightViewInvoice.constant = 0
                        
                        heightAmountPaid.constant = 0
                        
                        viewAmountPaid.isHidden = true
                        //////
                        
                        viewPurchaseProof.isHidden = true
                        heightViewPurchaseProof.constant = 0
                        heightViewStatusAndAmount.constant = 210
                        
                        selectedProgressValue = false
                    }
                    
                    buyerStageId = model.stageId ?? 0
                    collectionViewStatus.reloadData()
                    
                }
                
            }
            
        }
        
        if collectionView == collectionViewAttachments {
            
            if let fileName = attachments[indexPath.row] as? [String : String] {
                
                if let imagePath = fileName["cdnPath"] {
                    
                    if let filePath = fileName["fileName"] {
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                            let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                            imageViewer.strImg = imagePath
                            navigationController?.pushViewController(imageViewer, animated: true)
                            
                        }
                            
                        else{
                            
                            openWebView(urlString:imagePath, title: filePath, subTitle: "")
                        }
                        
                    }
                }
                
            }
            
        }
        
    }
    
    //    @objc func scrollnext() {
    //        if x < masterStageList?.count ?? 0 {
    //            let indexPath = IndexPath(item: x, section: 0)
    //            coll.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    //            x = x + 1
    //
    //        } else {
    //
    //            x = 0
    //
    //            collectionViewImages.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
    //
    //        }
    //    }
    //    @objc func scrollprev() {
    //
    //    }
    
}

extension HomeBuyerProductDetailVC:PostReviewsVCDelegate,ChangeProductVCDelegate{
    
    func didAddedReview() {
        
        fetchUserList()
        
    }
    
    func didChangeProduct() {
        
        fetchUserList()
    }
    
}
