//
//  HomeViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 03/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class HomeViewController: AppBaseViewController{
    
    @IBOutlet weak var btnManageSellers: NKButton!
    
    @IBOutlet weak var btnLocateReferral: NKButton!
    
    @IBOutlet weak var btnHome: UIButton!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var btnMessage: UIButton!
    
    @IBOutlet weak var collectionViewAttachment: UICollectionView!
    
    @IBOutlet weak var viewDescription: UIView!
    
    @IBOutlet weak var viewSeller: RCView!
    
    @IBOutlet weak var viewReferralPartner: RCView!
    
    @IBOutlet weak var lblSellerReferralCount: NKLabel!
    
    @IBOutlet weak var lblSellerEarnings: NKLabel!
    
    @IBOutlet weak var lblSellerEarningCount: NKLabel!
    
    @IBOutlet weak var lblSellerPipelineCount: NKLabel!
    
    @IBOutlet weak var lblSellerLostCount: NKLabel!
    
    @IBOutlet weak var lblSellerTotalEarnings:NKLabel!
    
    @IBOutlet weak var lblSellerTotalPipeline:NKLabel!
    
    @IBOutlet weak var lblSellerTotalLost:NKLabel!
    
    @IBOutlet weak var lblTopDesText: NKLabel!
    
    @IBOutlet weak var lblBottomDesText: NKLabel!
    
    @IBOutlet weak var lblLoyaltyBonusText: NKLabel!
    
    @IBOutlet weak var lblLoyaltyBonusValue: NKLabel!
    
    @IBOutlet weak var lblAvailablty: NKLabel!
    
    @IBOutlet weak var collectionViewBuyer: UICollectionView!
    
    @IBOutlet weak var lblTotalReferralsText: NKLabel!
    
    @IBOutlet weak var lblTotalEarningsText: NKLabel!
    
    @IBOutlet weak var viewTotalReferrals: RCView!
    
    
    @IBOutlet weak var viewTotalReferralsHeader: RCView!
    
    @IBOutlet weak var viewTotalEarnings: RCView!
    
    
    @IBOutlet weak var viewTotalEarningsHeader: RCView!
    
    @IBOutlet weak var tableViewReferralList: UITableView!
    
    
    @IBOutlet weak var lblSellerEarnedTransaction: NKLabel!
    
    @IBOutlet weak var lblSellerEarnedTransactionCount: NKLabel!
    
    
    @IBOutlet weak var lblSellerPipelineTransactionsCount: NKLabel!
    
    @IBOutlet weak var lblSellerPipelineTransactions: NKLabel!
    
    @IBOutlet weak var lblSellerLostTransactionsCount: NKLabel!
    
    @IBOutlet weak var lblSellerLostTransactions: NKLabel!
    
    @IBOutlet weak var heightViewReferralsDetail: NSLayoutConstraint!
    
    @IBOutlet weak var stackViewBottomBtn: UIStackView!
    
    @IBOutlet weak var viewTopBanners: RCView!
    
    @IBOutlet weak var viewBuyerLoyaltyClaim: RCView!
    
    @IBOutlet weak var viewProductList: UIView!
    
    @IBOutlet weak var heightViewBanner: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewBuyerLoyalty: NSLayoutConstraint!
    
    
    @IBOutlet weak var imgViewHome: UIImageView!
    
    @IBOutlet weak var imgViewSearch: UIImageView!
    
    @IBOutlet weak var imgViewMessage: UIImageView!
    
    @IBOutlet weak var heightProductList: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewDes: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewSeller: NSLayoutConstraint!
    
    @IBOutlet weak var lblSellerLoyaltyValue: NKLabel!
    
    @IBOutlet weak var heightBottomStackView: NSLayoutConstraint!
    
    @IBOutlet weak var viewContentView: UIView!
    
    @IBOutlet weak var lblStatusReferralTitle: NKLabel!
    
    @IBOutlet weak var topSpaceBuyerLoyalty: NSLayoutConstraint!
    
    @IBOutlet weak var lblBuyerProductViewTitle: NKLabel!
    
    @IBOutlet weak var tableViewJobs: UITableView!
    
    
    @IBOutlet weak var heightViewJobs: NSLayoutConstraint!
    
    @IBOutlet weak var lblJobReferralTitle: NKLabel!
    
    @IBOutlet weak var viewJobReferral: RCView!
    
    @IBOutlet weak var viewClaim: RCView!
    
    
    @IBOutlet weak var lblWalletTitle: NKLabel!
    
    @IBOutlet weak var lblAvailableToClaimTitle: NKLabel!
    
    @IBOutlet weak var lblClaimAmount: NKLabel!
    
    @IBOutlet weak var viewClaimButton: RCView!
    
    @IBOutlet weak var imgViewClaim: UIImageView!
    
    @IBOutlet weak var lblClaimTitle: NKLabel!
    
    @IBOutlet weak var btnClaimAmount: UIButton!
    
    @IBOutlet weak var heightViewClaim: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceClaim: NSLayoutConstraint!
    
    @IBOutlet weak var viewReferralCommision: RCView!
    
    @IBOutlet weak var heightViewReferralCommision: NSLayoutConstraint!
    
    @IBOutlet weak var stackViewComapny: UIStackView!
    
    @IBOutlet weak var btnPartner: NKButton!
    
    @IBOutlet weak var tableViewReferralNewList: UITableView!
    
    @IBOutlet weak var heightReferralNewList: NSLayoutConstraint!
    
    @IBOutlet weak var lblReferralListTitle: NKLabel!
    
    var buyerDetails:BuyerModel?
    
    var isFirstTime:Bool?
    
    var referralMemberDetails:ReferralMemberModel?
    
    var banners:[BannerModel]?
    
    var sellerDetails:HomeSellerDetailApiModel?
    
    var memberShipStatusObj:MemberShipStatusObj?
    
    var claimData:ClaimDataModel?
    
    var filterMasterList:[FilterReferralModel]?
    
    var membershipId:String?
    
    var productViewPdf:String?
    
    var delayTime:Double = 8.0
    
    var x = 1
    
    var timer : Timer?
    
    var isBuyer:Int?
    
    var isReferralMember:Int?
    
    var isLocatePartnerValue:Int?
    
    var isManageSellerValue:Int?
    
    var isSeller:Int?
    
    var isAccountStatement:Int?
    
    var isActivePartner:Int?
    
    var requestMessageText:String?
    
    var isManagePartners:Int?
    
    var eventsColorCode:String?
    
    var jobsColorCode:String?
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 4),
        .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let countAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 1),
        .foregroundColor: UIColor.getGradientColor(index: 0).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let countForListAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 4),
        .foregroundColor: UIColor.white,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let attributedPopUpTextOne: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 2),
        .foregroundColor: UIColor.black]
    
    let attributedPopUpTextTwo: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 8),
        .foregroundColor: UIColor.darkGray]
    
    var isFirst:Int = 0
    
    var isRequestForActivation:Int = 0
    
    var isRequestForTraining:Int = 0
    
    var homeManage = HomeManageCompanyPopUp()
    
    var messageManage:String?
    
    var sellerWiseLeadList:[HomeSellerLeadModel]?
    
    
    let underLineAttributes: [NSAttributedString.Key: Any] = [
           .font: UIFont.fontForType(textType: 3),
           .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
           .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var referralLeadLabelText:String?
    
    var listingCompanyLeadLabelText:String?
    
    var companySectionLabelText:String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        startAnimating()
        
        navigationItem.title = "iCanRefer".localized()
        
        stackViewBottomBtn.arrangedSubviews[1].isHidden = true
        
        imgViewHome.setImageColor(color: .white)
        imgViewSearch.setImageColor(color: .primaryColor)
        imgViewMessage.setImageColor(color: .primaryColor)
        
        isFirst = 1
        
        if let userModel = getUserDetail() {
            
            isReferralMember = userModel.isReferralMember ?? 0
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onReceiveData(_:)), name:NSNotification.Name(rawValue: "ReceiveData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(callHomeApi(notification:)), name: NSNotification.Name(rawValue: "callHome"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callHomePage(notification:)), name: NSNotification.Name("ReloadHomePage"), object: nil)
        
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReceiveData"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "callHome"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReloadHomePage"), object: nil)
    }
    
    @objc func onReceiveData(_ notification:Notification) {
        // Do something now //reload tableview
        
        fetchHomeDataFromNotification(isFirst: 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.integer(forKey: "selectedTab") == 1
        {
            isFirst = 1
            UserDefaults.standard.set(0, forKey: "selectedTab")
            fetchHomeDataFromNotification(isFirst: isFirst)
            
            //self.messageManage = requestMessageText ?? ""
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopAnimating()
    }
    
    func dataFromContainer(buyerModel:BuyerModel?,referralMemberModel:ReferralMemberModel?,bannersModel:[BannerModel]?,sellerDetailsModel:HomeSellerDetailApiModel?,memberShipStatusObjModel:MemberShipStatusObj?,membershipID:String?,isBuyerValue:Int?,isReferralMemberValue:Int?,isSellerValue:Int?,isLocatePartner:Int?,isManageSeller:Int?,isAccountStatementValue:Int,claimDataModel:ClaimDataModel?,filterMasterModel:[FilterReferralModel]?){
        
        if let apiResponse:BuyerModel = buyerModel {
            
            buyerDetails = apiResponse
            
        }
        
        if let apiResponse:ReferralMemberModel = referralMemberModel {
            
            referralMemberDetails = apiResponse
            
        }
        if let apiResponse:[BannerModel] = banners {
            
            banners = apiResponse
            
        }
        
        if let apiResponse:HomeSellerDetailApiModel = sellerDetailsModel {
            
            sellerDetails = apiResponse
            
        }
        
        if let apiResponse:Int = isBuyerValue {
            
            isBuyer = apiResponse
            
        }
        
        if let apiResponse:Int = isReferralMemberValue {
            
            isReferralMember = apiResponse
            
        }
        
        //
        
        if let apiResponse:Int = isLocatePartner {
            
            isLocatePartnerValue = apiResponse
            
        }
        
        if let apiResponse:Int = isManageSeller {
            
            isManageSellerValue = apiResponse
            
        }
        //
        
        if let apiResponse:Int = isSellerValue {
            
            isSeller = apiResponse
            
        }
        if let apiResponse:MemberShipStatusObj = memberShipStatusObjModel {
            
            memberShipStatusObj = apiResponse
            
        }
        if let apiResponse:String = membershipID {
            
            membershipId = apiResponse
            
        }
        if let apiResponse:ClaimDataModel = claimData {
            
            claimData = apiResponse
            
        }
        
        updateUI()
    }
    
    func showPopUp(withText:NSMutableAttributedString){
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        
        let home = HomeAmountClaimPopUp.init(frame: UIScreen.main.bounds)
        
        home.lblDescription.attributedText = withText
        
        guard let labelText = home.lblDescription.text else { return }
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        home.contentViewHeightConstraint.constant = height + 20
        
        currentWindow?.addSubview(home)
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let size = CGSize(width: view.frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: UIFont.largeTitleFont]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }
    
    
    func updateUI() {
        
        if isBuyer == 0 && isReferralMember == 0 && isSeller == 0
        {
            isBuyer = 1
        }
        
        viewDescription.alpha = 0
        
        lblStatusReferralTitle.text = sellerDetails?.productStatusLabelText ?? ""
        
        lblJobReferralTitle.text = sellerDetails?.jobStatusLabelText ?? ""
        
        lblTopDesText.text = buyerDetails?.topText ?? ""
        lblBottomDesText.text = buyerDetails?.bottomText ?? ""
        
        lblTopDesText.font = .HomeTitleFont
        lblBottomDesText.font = .HomeSubTitleFont
        
        heightViewDes.constant = 0
        
        imgViewClaim.setImageColor(color: .white)
        
        topSpaceBuyerLoyalty.constant = 0
        
        topSpaceClaim.constant = 0
        
        lblSellerEarningCount.attributedText = NSMutableAttributedString(string:  "\(referralMemberDetails?.referralStatus?.earnedCount ?? 0)",
            attributes: yourAttributes)
        lblSellerEarningCount.textColor = UIColor.btnLinkColor
        
        let totoalSellerEarnings = Int(referralMemberDetails?.referralStatus?.earnedAmount ?? 0.0)
        
        lblSellerTotalEarnings.text = "\(referralMemberDetails?.referralCurrencyTitle ?? "")\(" ")\(totoalSellerEarnings)"
        lblSellerTotalEarnings.textColor = UIColor.black
        
        //   lblSellerPipelineCount.text = "\(referralMemberDetails?.referralStatus?.pipelineCount ?? 0)"
        let totoalSellerPipeline = Int(referralMemberDetails?.referralStatus?.pipelineAmount ?? 0.0)
        
        lblSellerTotalPipeline.text = "\(referralMemberDetails?.referralCurrencyTitle ?? "")\(" ")\(totoalSellerPipeline)"
        lblSellerTotalPipeline.textColor = UIColor.black
        
        lblSellerLostCount.attributedText =  NSMutableAttributedString(string: "\(referralMemberDetails?.referralStatus?.lostCount ?? 0)",
            attributes: yourAttributes)
        lblSellerLostCount.textColor = UIColor.btnLinkColor
        
        let totoalSellerLost = Int(referralMemberDetails?.referralStatus?.lostAmount ?? 0.0)
        
        lblSellerTotalLost.text = "\(referralMemberDetails?.referralCurrencyTitle ?? "")\(" ")\(totoalSellerLost)"
        lblSellerTotalLost.textColor = UIColor.black
        
        lblSellerEarnedTransactionCount.attributedText = NSMutableAttributedString(string:  "\(referralMemberDetails?.sellerTransactionStatus?.earnedCount ?? 0)",
            attributes: yourAttributes)
        lblSellerEarnedTransactionCount.textColor = UIColor.btnLinkColor
        
        let totoalEarnedTransaction = Int(referralMemberDetails?.sellerTransactionStatus?.earnedAmount ?? 0.0)
        
        lblSellerEarnedTransaction.text = "\(referralMemberDetails?.referralCurrencyTitle ?? "")\(" ")\(totoalEarnedTransaction)"
        lblSellerEarnedTransaction.textColor = UIColor.black
        
        lblSellerPipelineCount.attributedText = NSMutableAttributedString(string:  "\(referralMemberDetails?.referralStatus?.pipelineCount ?? 0)",
            attributes: yourAttributes)
        
        lblSellerPipelineCount.textColor = UIColor.btnLinkColor
        
        let sellerLostTransactions = Int(referralMemberDetails?.sellerTransactionStatus?.lostAmount ?? 0)
        
        lblSellerLostTransactions.text = "\(referralMemberDetails?.currencyTitle ?? "")\(" ")\(sellerLostTransactions)"
        lblSellerLostTransactions.textColor = UIColor.black
        
        lblSellerLostTransactionsCount.attributedText = NSMutableAttributedString(string: "\(referralMemberDetails?.sellerTransactionStatus?.lostCount ?? 0)",
            attributes: yourAttributes)
        lblSellerLostTransactionsCount.textColor = UIColor.btnLinkColor
        
        let sellerLostTransactionsAmount = Int(referralMemberDetails?.sellerTransactionStatus?.lostAmount ?? 0)
        
        lblSellerLostTransactions.text = "\(referralMemberDetails?.currencyTitle ?? "")\(" ")\(sellerLostTransactionsAmount)"
        
        lblLoyaltyBonusValue.text = "\(buyerDetails?.currencyTitle ?? "")\(" ")\(buyerDetails?.loyaltyBonus ?? 0.0)"
        
        let sellerEarnings = Int(referralMemberDetails?.totalEarnings ?? 0.0)
        
        lblSellerEarnings.text = "\(referralMemberDetails?.currencyTitle ?? "")\(" ")\(sellerEarnings)"
        
        let attributeTotalReferralCount = NSMutableAttributedString(string: "\(referralMemberDetails?.totalReferralCount ?? 0)",
            attributes: countAttributes)
        
        lblSellerReferralCount.attributedText = attributeTotalReferralCount
        lblSellerReferralCount.textColor = UIColor.btnLinkColor
        
        let sellerLoyaltyValue = Int(referralMemberDetails?.referralClaimAmount ?? 0.0)
        
        lblSellerLoyaltyValue.text = "\(referralMemberDetails?.referralCurrencyTitle ?? "")\(" ")\(sellerLoyaltyValue)"
        
        let sellerPipelineTransactions = Int(referralMemberDetails?.sellerTransactionStatus?.pipelineAmount ?? 0.0)
        
        lblSellerPipelineTransactions.text = "\(referralMemberDetails?.currencyTitle ?? "")\(" ")\(sellerPipelineTransactions)"
        lblSellerPipelineTransactions.textColor = UIColor.black
        
        let attributePipelineTransactionsCount = NSMutableAttributedString(string: "\(referralMemberDetails?.sellerTransactionStatus?.pipelineCount ?? 0)",
            attributes: yourAttributes)
        
        lblSellerPipelineTransactionsCount.attributedText = attributePipelineTransactionsCount
        lblSellerPipelineTransactionsCount.textColor = UIColor.btnLinkColor
        
        //        btnClaimSeller.colorIndex = 9
        //
        //        btnClaimSeller.isUserInteractionEnabled = false
        
        
        tableViewReferralList.reloadData()
        tableViewJobs.reloadData()
        
        
        heightViewReferralsDetail.constant = 0
        
        btnLocateReferral.setTitle("Locate Partners".localized(), for: .normal)
        
        //        btnClaimBuyerLoyalty.colorIndex = 9
        //
        //        btnClaimBuyerLoyalty.isUserInteractionEnabled = false
        
        //        btnLocateReferral.layer.borderColor = UIColor.primaryColor.cgColor
        //        btnBecomeReferralPartner.layer.borderColor = UIColor.primaryColor.cgColor
        //        
        //        btnLocateReferral.backgroundColor = .white
        //        btnBecomeReferralPartner.backgroundColor = .white
        //        
        //        btnLocateReferral.layer.borderWidth = 1
        //        btnBecomeReferralPartner.layer.borderWidth = 1
        //        
        //        btnLocateReferral.setTitleColor(.primaryColor, for: .normal)
        //        btnBecomeReferralPartner.setTitleColor(.primaryColor, for: .normal)
        
        
        heightViewBanner.constant = 0
        
        heightViewBuyerLoyalty.constant = 0
        
        heightViewDes.constant = 0
        
        heightViewBuyerLoyalty.constant = 0
        
        heightProductList.constant = 0
        
        heightViewSeller.constant = 0
        
        heightViewJobs.constant = 0
        
        heightBottomStackView.constant = 0
        
        stackViewBottomBtn.arrangedSubviews[0].isHidden = true
        
        stackViewBottomBtn.arrangedSubviews[1].isHidden = true
        
        heightViewReferralsDetail.constant = 0
        
        //   viewClaim.alpha = 0
        // heightViewClaim.constant = 0
        
        if banners?.count ?? 0 > 0 {
            
            heightViewBanner.constant = 90
            
        }
        
        
        // if isBuyer == 1{
        
        if buyerDetails?.transactionList?.count ?? 0 > 0 {
            
            heightProductList.constant = 340
            
            heightViewDes.constant = 0
            
            viewDescription.alpha = 0
            
            lblBuyerProductViewTitle.text = "Sellers referred to you by Partners".localized()
            
        }
        else{
            
            if buyerDetails?.topText?.count ?? 0 > 0 {
                
                heightViewDes.constant = view.frame.height - 100
                viewDescription.alpha = 1
                
            }
            
        }
        
        if buyerDetails?.loyaltyBonus ?? 0.0 > 0.0 {
            
            heightViewBuyerLoyalty.constant = 55
            
            topSpaceBuyerLoyalty.constant = 10
            
        }
        
        //  if claimData?.totalClaimAmount ?? 0.0 > 0.0 {
        
        viewClaim.alpha = 1
        heightViewClaim.constant = 80
        topSpaceClaim.constant = 10
        
        lblClaimAmount.text = "\(claimData?.currencyTitle ?? "")\(" ")\(claimData?.totalClaimAmount ?? 0.0)"
        
        
        if sellerDetails?.productLeadStatus?.count ?? 0 > 0 {
            
            DispatchQueue.main.async(execute: {
                
                let heightReferral = self.sellerDetails?.productLeadStatus?.count ?? 0
                
               // self.heightViewSeller.constant = CGFloat((heightReferral * 44) + 60)
                
                self.heightViewSeller.constant = 0
                
                self.tableViewReferralList.reloadData()
            })
            
        }
        
        if sellerDetails?.jobLeadStatus?.count ?? 0 > 0 {
            
            DispatchQueue.main.async(execute: {
                
                let heightJobs = self.sellerDetails?.jobLeadStatus?.count ?? 0
                
              //  self.heightViewJobs.constant = CGFloat((heightJobs * 44) + 60)
                
                 self.heightViewJobs.constant = 0
                
                self.tableViewJobs.reloadData()
            })
            
        }
        
        
        //        if isManageSellerValue == 1 {
        //
        //          //  stackViewBottomBtn.arrangedSubviews[1].isHidden = false
        //
        //            //heightBottomStackView.constant = 60
        //
        //            // if referralMemberDetails?.totalSellerCount ?? 0 > 0 {
        //
        //            btnManageSellers.setTitle("\("Companies".localized())\(" ")\("(\(referralMemberDetails?.totalSellerCount ?? 0))")", for: .normal)
        //            //            }
        //            //            else{
        //            //
        //            //                btnManageSellers.setTitle("\("Manage Seller".localized())", for: .normal)
        //            //
        //            //            }
        //
        //            btnPartner.setTitle("Partners".localized(), for: .normal)
        //        }
        //        else
        //        {
        //            stackViewBottomBtn.arrangedSubviews[1].isHidden = true
        //
        //            heightBottomStackView.constant = 0
        //        }
        //
        //
        
        if referralMemberDetails?.totalReferralCount ?? 0 > 0 || referralMemberDetails?.totalEarnings ?? 0.0 > 0.0 {
            
            heightViewReferralsDetail.constant = 485
            heightViewReferralCommision.constant = 0
            viewReferralCommision.alpha = 0
            
            if referralMemberDetails?.referralClaimAmount ?? 0.0 > 0.0
            {
                
                heightViewReferralsDetail.constant = 540
                heightViewReferralCommision.constant = 55
                viewReferralCommision.alpha = 1
            }
            
        }
        
        
        
        heightBottomStackView.constant = 0
        
        
        if isLocatePartnerValue == 1 {
            
            stackViewBottomBtn.arrangedSubviews[0].isHidden = false
            
            heightBottomStackView.constant = 60
        }
        else
        {
            stackViewBottomBtn.arrangedSubviews[0].isHidden = true
        }
        
        if isManageSellerValue == 1 || isManagePartners == 1 {
            
            
            if referralMemberDetails?.totalSellerCount ?? 0 > 0 {
                
                btnManageSellers.setTitle("\("Companies".localized())\(" ")\("(\(referralMemberDetails?.totalSellerCount ?? 0))")", for: .normal)
                
            }
            else{
                
                btnManageSellers.setTitle("Companies".localized(), for: .normal)
            }
            
            if referralMemberDetails?.myPartnerCount ?? 0 > 0 {
                
                btnPartner.setTitle("\("Partners (".localized())\(referralMemberDetails?.myPartnerCount ?? 0))", for: .normal)
                
            }
                
            else{
                
                btnPartner.setTitle("Partners".localized(), for: .normal)
                
            }
            
            stackViewBottomBtn.arrangedSubviews[1].isHidden = false
            
            heightBottomStackView.constant = 60
            
            stackViewComapny.arrangedSubviews[1].isHidden = true
            stackViewComapny.arrangedSubviews[0].isHidden = true
            
            if isManagePartners == 1 {
                
                stackViewComapny.arrangedSubviews[1].isHidden = false
            }
            if isManageSellerValue == 1{
                
                stackViewComapny.arrangedSubviews[0].isHidden = false
            }
        }
        else
        {
            stackViewBottomBtn.arrangedSubviews[1].isHidden = true
        }
        
        ///
        
        if isLocatePartnerValue == 1 && ( isManageSellerValue == 1 || isManagePartners == 1 ) {
            
            heightBottomStackView.constant = 120
            
            stackViewBottomBtn.arrangedSubviews[1].isHidden = false
            
            stackViewBottomBtn.arrangedSubviews[0].isHidden = false
            
        }
        
        if isLocatePartnerValue == 0 && isManageSellerValue == 0 &&  isManagePartners == 0{
            
            heightBottomStackView.constant = 0
            
            stackViewBottomBtn.arrangedSubviews[1].isHidden = true
            
            stackViewBottomBtn.arrangedSubviews[0].isHidden = true
        }
        
        for model in sellerWiseLeadList ?? []{
            
            //           let modelMain = HomeReferralLeadsModel()
            //            modelMain.title = model.sellerCompanyName
            //             model.referralLeads?.append(modelMain)
            
            if model.eventCount ?? 0 > 0 {
                let modelMain = HomeReferralLeadsModel()
                modelMain.title = "Events".localized()
                modelMain.count = model.eventCount ?? 0
                modelMain.stageColorCode = model.eventColorCode ?? ""
                modelMain.eventRegisteredCount = model.eventRegisteredCount ?? 0
                model.referralLeads?.append(modelMain)
            }
            if model.jobCount ?? 0 > 0 {
                let modelMain = HomeReferralLeadsModel()
                modelMain.title = "Jobs".localized()
                modelMain.count = model.jobCount ?? 0
                modelMain.stageColorCode = model.jobColorCode ?? ""
                modelMain.jobRegisteredResumeCount = model.jobRegisteredResumeCount ?? 0
                model.referralLeads?.append(modelMain)
                
            }
        }
        
        
        
        //  for modelMain in sellerWiseLeadList ?? []{
        let count = self.sellerWiseLeadList?.count ?? 0
        
        self.heightReferralNewList.constant = 0
        
        if count > 0 {
        
        DispatchQueue.main.async(execute: {
            
            var heightJobs = 0
            
           
            let heightMain:CGFloat =  CGFloat(count * 100)
            
            for modelMain in self.sellerWiseLeadList ?? []{
                
                heightJobs = heightJobs+(modelMain.referralLeads?.count ?? 0)
                
            }
            
            self.heightReferralNewList.constant = CGFloat((heightJobs * 44) + 80 + Int(heightMain))
            
            self.tableViewReferralNewList.reloadData()
            
        })
        
      }
        
        
        lblReferralListTitle.text = companySectionLabelText
            
        // }
        
        tableViewReferralList.reloadData()
        
        collectionViewAttachment.reloadData()
        
        collectionViewBuyer.reloadData()
        
        //tableViewReferralNewList.reloadData()
    }
    
    @IBAction func btnManageSellersAction(_ sender: NKButton) {
        
        if checkLogin() {
            
            
            if isActivePartner == 1 {
                
                let storyBoard = UIStoryboard.init(name: SellerSB, bundle: nil)
                let sellerPage:SellerListVC = storyBoard.instantiateViewController(withIdentifier: "SellerListVC") as! SellerListVC
                sellerPage.filterMasterList = filterMasterList ?? []
                navigationController?.pushViewController(sellerPage, animated: true)
                
            }
                
            else{
                
                alertForManageCompany()
            }
            
        }
        else{
            openSignInPage()
        }
        
    }
    
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        if checkLogin() {
            
            let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
            let searchPage:SearchMainVC = storyBoard.instantiateViewController(withIdentifier: "SearchMainVC") as! SearchMainVC
            
            let isUpdated:[String: Int] = ["update": 0]
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callLocation"), object: nil, userInfo: isUpdated)
            
            navigationController?.pushViewController(searchPage, animated: true)
            
        }
        else{
            openSignInPage()
        }
    }
    
    @IBAction func btnMessageAction(_ sender: UIButton) {
        
        if checkLogin() {
            
            let storyBoard = UIStoryboard.init(name: MessageSB, bundle: nil)
            let sellerPage:MessageMainVC = storyBoard.instantiateViewController(withIdentifier: "MessageMainVC") as! MessageMainVC
            navigationController?.pushViewController(sellerPage, animated: true)
            
        }
        else{
            openSignInPage()
        }
    }
    
    @IBAction func btnLocateReferralPartnerAction(_ sender: Any) {
        
        if checkLogin() {
            
            let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
            let sellerPage:LocateReferralPartnerVC = storyBoard.instantiateViewController(withIdentifier: "LocateReferralPartnerVC") as!  LocateReferralPartnerVC
            navigationController?.pushViewController(sellerPage, animated: true)
            
        }
        else{
            openSignInPage()
        }
        
    }
    
    @IBAction func btnClaimAction(_ sender: Any) {
        
        if claimData?.isClaimActive == 1 {
            
            let storyBoard = UIStoryboard.init(name: PaymentSB, bundle: nil)
            let paymentClaimPage:PaymentClaimVC = storyBoard.instantiateViewController(withIdentifier: "PaymentClaimVC") as!  PaymentClaimVC
            paymentClaimPage.strAmount = "\(claimData?.currencyTitle ?? "")\(" ")\(claimData?.totalClaimAmount ?? 0.0)"
            navigationController?.pushViewController(paymentClaimPage, animated: true)
            
        }
        else{
            
            let combinedString  = NSMutableAttributedString()
            
            let str1 = NSMutableAttributedString(string: "\(claimData?.claimAlertTextMessage ?? "") \n\n", attributes: attributedPopUpTextOne)
            
            let str2 = NSMutableAttributedString(string: "\(claimData?.claimAlertTextMessageTwo ?? "")", attributes: attributedPopUpTextTwo)
            
            combinedString.append(str1)
            
            combinedString.append(str2)
            
            showPopUp(withText:combinedString)
        }
        
    }
    
    @IBAction func btnPartnerAction(_ sender: UIButton) {
        
        if checkLogin() {
            
            let storyBoard = UIStoryboard.init(name: PartnerSB, bundle: nil)
            if let partnerPage:PartnerListVC = storyBoard.instantiateViewController(withIdentifier: "PartnerListVC") as?  PartnerListVC {
                
                
                navigationController?.pushViewController(partnerPage, animated: true)
                
            }
            
        }
        else{
            openSignInPage()
        }
        
    }
    @IBAction func btnTotalReferralsAction(_ sender: UIButton) {
        
        openReferralLeadsPage(withType: 1, withStageType: 0, withTitle: "Total Referrals".localized(), progressValue: "-1")
    }
    
    @IBAction func btnSellerEarnedReferrals(_ sender: UIButton) {
        
        openReferralLeadsPage(withType: 2, withStageType: 1, withTitle: "Your Earned Referrals".localized(), progressValue: "-1")
    }
    
    @IBAction func btnSellerPipelineReferralsAction(_ sender: Any) {
        
        openReferralLeadsPage(withType: 2, withStageType: 2, withTitle: "Your Pipelined Referrals".localized(), progressValue: "-1")
    }
    
    @IBAction func btnSellerLostReferralsAction(_ sender: Any) {
        
        openReferralLeadsPage(withType: 2, withStageType: 3, withTitle: "Your Lost Referrals".localized(), progressValue: "-1")
    }
    
    @IBAction func btnSellerEarnedTransactions(_ sender: Any) {
        
        openReferralLeadsPage(withType: 3, withStageType: 1, withTitle: "Listed Seller Transactions".localized(), progressValue: "-1")
    }
    
    @IBAction func btnSellerPipelineTransaction(_ sender: Any) {
        
        openReferralLeadsPage(withType: 3, withStageType: 2, withTitle: "Listed Seller Transactions".localized(), progressValue: "-1")
        
    }
    
    
    @IBAction func btnSellerLostTransaction(_ sender: Any) {
        
        openReferralLeadsPage(withType: 3, withStageType: 2, withTitle: "Listed Seller Transactions".localized(), progressValue: "-1")
    }
    
    func alertForManageCompany() {
        
        //        let alert = UIAlertController(title: nil, message: requestMessageText ?? "", preferredStyle: .actionSheet)
        //
        //        alert.view.tintColor = UIColor.btnLinkColor
        //
        //        alert.addAction(UIAlertAction(title: "Cancel".localized(),
        //                                      style: UIAlertAction.Style.cancel,
        //                                      handler: {(_: UIAlertAction!) in
        //
        //             self.dismiss(animated: true, completion: nil)
        //
        //        }))
        //
        //        alert.addAction(UIAlertAction(title: "Request for Training".localized(), style: UIAlertAction.Style.default, handler: { _ in
        //
        //
        //            self.isRequestForTraining = 1
        //             self.isRequestForActivation = 0
        //
        //            self.showAlertCompanyMessage()
        //
        //            self.dismiss(animated: true, completion: nil)
        //
        //        }))
        //
        //        alert.addAction(UIAlertAction(title: "Request for Activation".localized(), style: UIAlertAction.Style.default, handler: { _ in
        //
        //
        //            self.isRequestForActivation = 1
        //             self.isRequestForTraining = 0
        //
        //            self.showAlertCompanyMessage()
        //
        //            self.dismiss(animated: true, completion: nil)
        //
        //        }))
        //
        //        self.present(alert, animated: true, completion: nil
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        
        homeManage = HomeManageCompanyPopUp.init(frame: UIScreen.main.bounds)
        
        homeManage.viewCancelBg.backgroundColor = .primaryColor
        
        homeManage.imgClose.setImageColor(color: .white)
        
        homeManage.lblDescription.text = requestMessageText ?? ""
        
        
        
        guard let labelText = homeManage.lblDescription.text else { return }
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        homeManage.contentViewHeightConstraint.constant = height + 76
        
        homeManage.btnRequestTraining.addTarget(self, action: #selector(btnTrainngAction(_:)), for: .touchUpInside)
        
        homeManage.btnRequestActivation.addTarget(self, action: #selector(btnActivationAction), for: .touchUpInside)
        
        if isRequestForTraining == 0 && isRequestForActivation == 0 {
            
            //            homeManage.btnRequestTraining.colorIndex = 9
            //            homeManage.btnRequestActivation.colorIndex = 9
            //
            //            homeManage.btnRequestActivation.alpha = 0
            //            homeManage.btnRequestTraining.alpha = 0
            
            showPopUpAfterSuccess(withMsg: messageManage ?? "")
            
            homeManage.contentViewHeightConstraint.constant = height + 56
        }
        else{
            
            homeManage.btnRequestActivation.alpha = 1
            homeManage.btnRequestTraining.alpha = 1
            
            if isRequestForTraining == 0{
                homeManage.btnRequestTraining.colorIndex = 9
            }
            else{
                homeManage.btnRequestTraining.colorIndex = 1
            }
            
            if isRequestForActivation == 0{
                homeManage.btnRequestActivation.colorIndex = 9
            }
            else{
                homeManage.btnRequestActivation.colorIndex = 0
            }
            
            homeManage.contentViewHeightConstraint.constant = height + 76
            
            currentWindow?.addSubview(homeManage)
        }
        
        
        
    }
    
    @objc func btnTrainngAction(_ sender: UIButton){
        
        if isRequestForTraining == 1 {
            
            self.isRequestForTraining = 1
            
            self.isRequestForActivation = 0
            
            self.showAlertCompanyMessage()
        }
        
    }
    
    @objc func btnActivationAction(_ sender: UIButton){
        
        if isRequestForTraining == 0 && isRequestForActivation == 0 {
            
            
        }
            
        else{
            
            if isRequestForActivation == 1 {
                
                self.isRequestForActivation = 1
                
                self.isRequestForTraining = 0
                
                self.showAlertCompanyMessage()
                
            }
        }
        
    }
    
    
    func showPopUpAfterSuccess(withMsg:String){
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        
        let home = HomeAmountClaimPopUp.init(frame: UIScreen.main.bounds)
        
        home.lblDescription.text = withMsg
        
        guard let labelText = home.lblDescription.text else { return }
        
        home.imgviewPopup.image = UIImage.init(named: "grenSucess")
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        home.contentViewHeightConstraint.constant = height + 120
        
        currentWindow?.addSubview(home)
    }
    
    func openReferralLeadsPage(withType:Int,withStageType:Int,withTitle:String,progressValue:String){
        
        let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
        let referalLeadsPage:ReferalLeadsVC = storyBoard.instantiateViewController(withIdentifier: "ReferalLeadsVC") as!   ReferalLeadsVC
        referalLeadsPage.stageType = withStageType 
        referalLeadsPage.type = withType
        referalLeadsPage.strTitle = withTitle
        referalLeadsPage.isFromHome = true
        referalLeadsPage.filterMasterList = filterMasterList ?? []
        referalLeadsPage.progressValue = progressValue
        navigationController?.pushViewController(referalLeadsPage, animated: true)
        
    }
    
    func showAlertCompanyMessage(){
        
        let apiClient:APIClient = APIClient()
        
        let params:NSDictionary = ["isRequestForTraining":isRequestForTraining,
                                   "isRequestForActivation":isRequestForActivation
        ]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        weak var weakSelf = self
        startAnimatingAfterSubmit()
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/sendPartnerRequest?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            weakSelf?.stopAnimating()
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.homeManage.removeFromSuperview()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                
                                
                                if let isRequestForTrainingValue:Int = dataObj.object(forKey: "isRequestForTraining") as? Int {
                                    
                                    
                                    weakSelf?.isRequestForTraining = isRequestForTrainingValue
                                }
                                
                                if let isRequestForActivationValue:Int = dataObj.object(forKey: "isRequestForActivation") as? Int {
                                    
                                    weakSelf?.isRequestForActivation = isRequestForActivationValue
                                    
                                }
                                // let decoder = JSONDecoder()
                                
                                
                                self.messageManage = message
                                
                                weakSelf?.showPopUpAfterSuccess(withMsg: self.messageManage ?? "")
                                
                                print(dataObj)
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                        
                    }
                }
            }
            
            // weakSelf?.showPopUpAfterSuccess(withMsg: message)
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
    
    func fetchHomeDataFromNotification(isFirst:Int) {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        
        DispatchQueue.main.async {
            if isFirst == 1 {
                weakSelf?.startAnimating()
            }
        }
        
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/homePage?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: nil, completionHandler:{response, jsonObj in
            
            DispatchQueue.main.async {
                weakSelf?.stopAnimating()
            }
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(HomeApiModel.self, from: data)
                                
                                if let apiResponse:BuyerModel = jsonResponse.buyerDetails {
                                    
                                    weakSelf?.buyerDetails = apiResponse
                                    
                                }
                                
                                if let apiResponse:ReferralMemberModel = jsonResponse.referralMemberDetails {
                                    
                                    weakSelf?.referralMemberDetails = apiResponse
                                    
                                }
                                if let apiResponse:[BannerModel] = jsonResponse.banners {
                                    
                                    weakSelf?.banners = apiResponse
                                    
                                }
                                
                                if let apiResponse:HomeSellerDetailApiModel = jsonResponse.sellerDetails {
                                    
                                    weakSelf?.sellerDetails = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.isBuyer {
                                    
                                    weakSelf?.isBuyer = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.isReferralMember {
                                    
                                    weakSelf?.isReferralMember = apiResponse
                                    
                                    UserDefaults.standard.set(apiResponse, forKey: "isReferralMember")
                                    
                                }
                                
                                //
                                
                                if let apiResponse:Int = jsonResponse.isLocateReferralPartner {
                                    
                                    weakSelf?.isLocatePartnerValue = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.isManageSeller {
                                    
                                    weakSelf?.isManageSellerValue = apiResponse
                                    
                                }
                                
                                //
                                
                                if let apiResponse:Int = jsonResponse.isSeller {
                                    
                                    weakSelf?.isSeller = apiResponse
                                    
                                }
                                if let apiResponse:MemberShipStatusObj = jsonResponse.memberShipStatusObj {
                                    
                                    weakSelf?.memberShipStatusObj = apiResponse
                                    
                                }
                                if let apiResponse:String = jsonResponse.membershipId {
                                    
                                    weakSelf?.membershipId = apiResponse
                                    
                                }
                                
                                if let apiResponse:String = jsonResponse.requestMessageText {
                                    
                                    weakSelf?.requestMessageText = apiResponse
                                    
                                    self.messageManage = self.requestMessageText ?? ""
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.isAccountStatement {
                                    
                                    weakSelf?.isAccountStatement = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.isActivePartner {
                                    
                                    weakSelf?.isActivePartner = apiResponse
                                    
                                }
                                if let apiResponse:Int = jsonResponse.isRequestForTraining {
                                    
                                    weakSelf?.isRequestForTraining = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.isRequestForActivation {
                                    
                                    weakSelf?.isRequestForActivation = apiResponse
                                    
                                }
                                
                                if let apiResponse:ClaimDataModel = jsonResponse.claimData {
                                    
                                    weakSelf?.claimData = apiResponse
                                    
                                }
                                
                                if let apiResponse:[FilterReferralModel] = jsonResponse.filterMasterList {
                                    
                                    weakSelf?.filterMasterList = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.isManagePartners {
                                    
                                    weakSelf?.isManagePartners = apiResponse
                                    
                                }
                                
                                if let apiResponse:[HomeSellerLeadModel] = jsonResponse.sellerWiseLeadList {
                                    
                                    weakSelf?.sellerWiseLeadList = apiResponse
                                    
                                }
                                
                                if let apiResponse:String = jsonResponse.referralLeadLabelText {
                                                                   
                                        weakSelf?.referralLeadLabelText = apiResponse
                                                                
                                }
                                
                                if let apiResponse:String = jsonResponse.listingCompanyLeadLabelText{
                                                                                                  
                                    weakSelf?.listingCompanyLeadLabelText = apiResponse
                                                                                               
                                 }
                                
                                if let apiResponse:String = jsonResponse.companySectionLabelText{
                                                    
                                    weakSelf?.companySectionLabelText = apiResponse
                                                                                                                              
                                 }
                                
                                weakSelf?.updateUI()
                                
                                // weakSelf?.sendDataToHomeVc()
                                
                                return
                            } catch {
                                print(error)
                                weakSelf?.startAnimating()
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            weakSelf?.startAnimating()
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func sendDataToHomeVc() {
        
        
        dataFromContainer(buyerModel: buyerDetails, referralMemberModel: referralMemberDetails, bannersModel: banners, sellerDetailsModel: sellerDetails, memberShipStatusObjModel: memberShipStatusObj, membershipID: membershipId, isBuyerValue: isBuyer, isReferralMemberValue: isReferralMember, isSellerValue: isSeller,isLocatePartner:isLocatePartnerValue,isManageSeller:isManageSellerValue,isAccountStatementValue:isAccountStatement ?? 0, claimDataModel: claimData, filterMasterModel: filterMasterList)
        
    }
    
    @objc func callHomeApi(notification: NSNotification) {
        if let isFlag = notification.userInfo?["update"] as? Int {
            
            if isFlag == 1 {
                
                fetchHomeDataFromNotification(isFirst: 1)
                
            }
            
        }
    }
    
    @objc func callHomePage(notification: NSNotification) {
        
        fetchHomeDataFromNotification(isFirst: 0)
        
        
    }
}

extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewAttachment {
            
            return CGSize.init(width: collectionViewAttachment.frame.width, height: collectionViewAttachment.frame.height)
            
        }
        
        if collectionView == collectionViewBuyer {
            
            return CGSize.init(width: collectionViewBuyer.frame.width, height: collectionViewBuyer.frame.height)
            
        }
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewAttachment {
            
            if  banners?.count ?? 0 > 0 {
                
                return banners?.count ?? 0
            }
        }
        
        if collectionView == collectionViewBuyer {
            
            if buyerDetails?.transactionList?.count ?? 0 > 0 {
                
                return buyerDetails?.transactionList?.count ?? 0
            }
            
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewAttachment {
            
            if let cell:HomeBannerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBannerCell", for: indexPath) as? HomeBannerCell {
                
                return getCellForHomeBanner(cell: cell, indexPath: indexPath)
                
            }
        }
        
        if collectionView == collectionViewBuyer {
            
            if let cell:HomeBuyerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBuyerCell", for: indexPath) as? HomeBuyerCell {
                
                return getCellForHomeBuyer(cell: cell, indexPath: indexPath)
                
            }
        }
        
        
        return UICollectionViewCell.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewBuyer
        {
            if buyerDetails?.transactionList?.count ?? 0 > 0
            {
                if let model:HomeTransactionModel = buyerDetails?.transactionList?[indexPath.row]
                {
                    let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
                    let productDetail:HomeBuyerProductDetailVC = storyBoard.instantiateViewController(withIdentifier: "HomeBuyerProductDetailVC") as! HomeBuyerProductDetailVC
                    productDetail.transactionId = model.transactionId ?? 0
                    navigationController?.pushViewController(productDetail, animated: true)
                    
                }
            }
        }
        
        
    }
    
    func getCellForHomeBanner(cell:HomeBannerCell,indexPath:IndexPath) -> HomeBannerCell {
        
        if banners?.count ?? 0 > 0 {
            
            if let fileName = banners?[indexPath.row]
            {
                if let imagePath = fileName.cdnPath
                {
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        // cell.imgViewHeader.sd_setImage(with: url, completed: nil)
                        
                        cell.imgViewHeader.sd_setImage(with:url, placeholderImage:UIImage.init(imageLiteralResourceName: "noImageIcon"), options:.init(rawValue: 0), completed: nil)
                        
                        
                    }
                }
                
            }
        }
        return cell
    }
    
    
    func getCellForHomeBuyer(cell:HomeBuyerCell,indexPath:IndexPath) -> HomeBuyerCell {
        
        if buyerDetails?.transactionList?.count ?? 0 > 0 {
            
            if let model:HomeTransactionModel = buyerDetails?.transactionList?[indexPath.row]{
                
                cell.lblViewCount.text = "\(indexPath.row + 1)\(" of ".localized())\(buyerDetails?.transactionList?.count ?? 0 )"
                
                let mainAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 14)]
                let preAttributes = [NSAttributedString.Key.font: UIFont.fontForType(textType: 8)]
                
                let rating = model.rating ?? 0.0
                
                cell.lblRatingCount.alpha = 0
                
                cell.widthRatingCount.constant = 0
                
                if rating > 0.0 {
                    
                    let first = NSMutableAttributedString(string: String(format:"%.1f",rating), attributes: preAttributes)
                    let second = NSMutableAttributedString(string:" ✭", attributes: mainAttributes)
                    let reviewText = NSMutableAttributedString()
                    reviewText.append(first)
                    reviewText.append(second)
                    cell.widthRatingCount.constant = 70
                    cell.lblRatingCount.alpha = 1
                    cell.lblRatingCount.attributedText = reviewText
                    
                    
                }
                
                cell.viewAppoinmentTime.alpha = 0
                cell.viewNoAppointment.alpha = 1
                cell.heightViewAppoinment.constant = 100
                
                if model.isAppointment == 1 {
                            
                    cell.viewAppoinmentTime.alpha = 1
                    cell.viewNoAppointment.alpha = 0
                    cell.heightViewAppoinment.constant = 100
                    
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
                    
                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
                    dateFormatterPrint.timeZone = .current
                    
                    var dateAppText = ""
                    
                    if let date = dateFormatterGet.date(from: model.appointmentDT ?? "") {
                        
                        dateAppText = dateFormatterPrint.string(from: date)
                    }
                    
                    cell.lblDate.text = dateAppText
                    
                    cell.lblMemberCount.text = "\(model.noOfPeople ?? 0)"
                    
                    let count = model.resourceCount ?? 0
                    
                    
                    if count > 1{
                        
                        let str1 = NSAttributedString(string: "\(model.resourceTitle ?? "")(\(count - 1) More)", attributes: underLineAttributes)
                        
                        cell.btnSeeMembers.setAttributedTitle(str1, for: .normal)
                        
                    }
                    else{
                        
                        let str1 = NSAttributedString(string: "\(model.resourceTitle ?? "")", attributes: underLineAttributes)
                        
                        cell.btnSeeMembers.setAttributedTitle(str1, for: .normal)
                    }
                    
                    cell.btnSeeMembers.tag = indexPath.row
                    
                    cell.btnSeeMembers.addTarget(self, action: #selector(btnSelectAppointmentAction(sender:)), for: .touchUpInside)
                    
                }
                
                //   cell.lblRatingCount.text = "\(model.rating ?? 0.0)"
                
                
                //                if model.banners?.count ?? 0 > 0 {
                //
                //                    if let fileName = model.banners?.first
                //                    {
                //                        if let imagePath = fileName["cdnPath"]
                //                        {
                //                            if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                //                                // cell.imgViewHeader.sd_setImage(with: url, completed: nil)
                //
                //                                cell.imgViewProduct.sd_setImage(with:url, placeholderImage:UIImage.init(imageLiteralResourceName: "noImageIcon"), options:.init(rawValue: 0), completed: nil)
                //
                //
                //                            }
                //                        }
                //
                //                    }
                //                }
                
                cell.imgProduct.image = nil
                
                if model.productLogo?.count ?? 0 > 0 {
                    cell.imgProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
                }
                else{
                    
                    cell.imgProduct.image = UIImage.init(named: "Logo")
                    
                }
                
                if model.productType == 1 {
                    
                    cell.imgProduct.image = UIImage.init(named: "jobDefault")
                    
                }
                
                cell.lblProductName.text = model.productName ?? ""
                
                cell.lblProductDes.text = "\("By ".localized())\(model.sellerCompanyName ?? "")"
                
                cell.lblFooter.attributedText = NSMutableAttributedString(string: model.footerText ?? "",
                                                                          attributes: yourAttributes)
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
                dateFormatterPrint.timeZone = .current
                
                var dateText = ""
                
                if let date = dateFormatterGet.date(from: model.referredDate ?? "") {
                    
                    dateText = dateFormatterPrint.string(from: date)
                }
                
                cell.lblReferredBy.text = "\("Referred")\(" on ")\(dateText)"
                
                if model.referredBy?.count ?? 0 > 0 {
                    
                    cell.lblReferredBy.text = "\("Referred by ")\(model.referredBy ?? "")\(" on ")\(dateText)"
                    
                }
                
                cell.lblLoyalty.alpha = 0
                
                if model.loyaltyAmount?.count ?? 0 > 0{
                    
                    cell.lblLoyalty.alpha = 1
                    
                    cell.lblLoyalty.text = "\(model.currencyTitle ?? "")\(" ")\(model.loyaltyAmount ?? "")\(" Loyalty Bonus".localized())"
                    
                }
                
                
            }
            
            cell.viewBackground.layer.cornerRadius = 5
            cell.viewBackground.layer.borderWidth = 1.0
            cell.viewBackground.layer.borderColor = UIColor.lightGray.cgColor
            cell.viewBackground.layer.shadowColor = UIColor.black.cgColor
            cell.viewBackground.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.viewBackground.layer.shadowOpacity = 0.3
            cell.viewBackground.layer.shadowRadius = 4.0
            
        }
        
        return cell
    }
    
    @objc func btnSelectAppointmentAction(sender: UIButton){
          
          if let model:HomeTransactionModel = buyerDetails?.transactionList?[sender.tag] {
             
          let storyBoard = UIStoryboard.init(name: AppointmentSB, bundle: nil)
             
            let appointmentPage:AppointmentViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentViewController") as! AppointmentViewController
             
              appointmentPage.appointmentId = model.appointmentId ?? 0
                  
              navigationController?.pushViewController(appointmentPage, animated: true)
                  
              
          }
          
      }
    
}

extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    
            if tableView == tableViewReferralNewList{
                  return 20
            }
    
            return 0
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableViewReferralNewList{
            
            if indexPath.row == 0 {
                
                return 80
            }
            else{
                
                return UITableView.automaticDimension
                
            }
            
        }
        
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == tableViewReferralNewList{
            
            return sellerWiseLeadList?.count ?? 0
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableViewReferralList {
            
            return sellerDetails?.productLeadStatus?.count ?? 0
            
        }
        
        if tableView == tableViewJobs {
            
            return sellerDetails?.jobLeadStatus?.count ?? 0
            
        }
        
        if tableView == tableViewReferralNewList{
            
            let count = sellerWiseLeadList?[section].referralLeads?.count ?? 0
            
            return count + 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewReferralList {
            
            if let cell:ReferralListCell = tableView.dequeueReusableCell(withIdentifier: "ReferralListCell", for: indexPath) as? ReferralListCell{
                
                return getCellForProductReferralList(cell: cell, indexPath: indexPath)
                
            }
        }
        
        if tableView == tableViewJobs {
            
            if let cell:ReferralListCell = tableView.dequeueReusableCell(withIdentifier: "ReferralListCell", for: indexPath) as? ReferralListCell{
                
                return getCellForJobReferralList(cell: cell, indexPath: indexPath)
                
            }
        }
        
        if tableView == tableViewReferralNewList {
            
            if indexPath.row == 0 {
                
                if let cell:HomeReferralHeaderNewCell = tableView.dequeueReusableCell(withIdentifier: "HomeReferralHeaderNewCell", for: indexPath) as? HomeReferralHeaderNewCell{
                    
                    return getCellForReferralNewHeader(cell: cell, indexPath: indexPath)
                    
                }
            }
            else{
                
                if let cell:HomeReferralListNewCell = tableView.dequeueReusableCell(withIdentifier: "HomeReferralListNewCell", for: indexPath) as? HomeReferralListNewCell{
                    
                    return getCellForReferralNewList(cell: cell, indexPath: indexPath)
                    
                }
            }
        }
        
        return UITableViewCell.init()
    }
    
    func getCellForProductReferralList(cell:ReferralListCell,indexPath:IndexPath) -> ReferralListCell {
        
        cell.selectionStyle = .none
        
        if let model:HomeSellerDetailModel = sellerDetails?.productLeadStatus?[indexPath.row] {
            
            let attributeTitle = NSMutableAttributedString(string: model.title ?? "",
                                                           attributes: yourAttributes)
            
            let attributeCount = NSMutableAttributedString(string: "\(model.count ?? 0 )",
                attributes: countForListAttributes)
            
            cell.lblTitle.attributedText = attributeTitle
            
            cell.lblCount.attributedText = attributeCount
            
        }
        
        return cell
        
    }
    
    func getCellForJobReferralList(cell:ReferralListCell,indexPath:IndexPath) -> ReferralListCell {
        
        cell.selectionStyle = .none
        
        if let model:HomeSellerDetailModel = sellerDetails?.jobLeadStatus?[indexPath.row] {
            
            let attributeTitle = NSMutableAttributedString(string: model.title ?? "",
                                                           attributes: yourAttributes)
            
            let attributeCount = NSMutableAttributedString(string: "\(model.count ?? 0 )",
                attributes: countForListAttributes)
            
            cell.lblTitle.attributedText = attributeTitle
            
            cell.lblCount.attributedText = attributeCount
            
        }
        
        return cell
        
    }
    
    func getCellForReferralNewList(cell:HomeReferralListNewCell,indexPath:IndexPath) -> HomeReferralListNewCell {
        
        cell.selectionStyle = .none
        
        if let modelMain:HomeSellerLeadModel = sellerWiseLeadList?[indexPath.section] {
            
            if let model:HomeReferralLeadsModel = modelMain.referralLeads?[indexPath.row - 1] {
                
                var cellAttributes: [NSAttributedString.Key: Any] = [
                    .font: UIFont.fontForType(textType: 4),
                    .foregroundColor: UIColor.getGradientColor(index: 3).first!,
                    .underlineStyle: NSUnderlineStyle.single.rawValue]
                
                if model.title == "Events" || model.title == "Jobs" {
                    
                  cellAttributes = [
                    .font: UIFont.fontForType(textType: 4),
                    .foregroundColor: UIColor.colorWithHexString(hexString: model.stageColorCode ?? "") ?? UIColor.getGradientColor(index: 3).first!,
                    .underlineStyle: NSUnderlineStyle.single.rawValue]
                }
                
                let attributeTitle = NSMutableAttributedString(string: model.title ?? "",
                                                               attributes: cellAttributes)
                
                let attributeCount = NSMutableAttributedString(string: "\(model.count ?? 0 )",
                    attributes: countForListAttributes)
                
                cell.lblTitle.attributedText = attributeTitle
                
                cell.lblCount.attributedText = attributeCount
                
                if model.title == "Events" || model.title == "Jobs" {
                    
                    cell.viewCountColor.backgroundColor =  UIColor.colorWithHexString(hexString: model.stageColorCode ?? "") ?? UIColor.getGradientColor(index: 3).first!
                    
                    cell.viewMainBackground.borderColor = UIColor.colorWithHexString(hexString: model.stageColorCode ?? "") ?? UIColor.getGradientColor(index: 3).first!
                    
                }
                else{
                    cell.viewCountColor.backgroundColor = UIColor.getGradientColor(index: 3).first!
                                       
                    cell.viewMainBackground.borderColor = UIColor.getGradientColor(index: 3).first!
                }
                if model.title == "Events" {
                    
                    if model.eventRegisteredCount ?? 0 > 0 {
                        
                        let attributeCount = NSMutableAttributedString(string: "\(model.count ?? 0 )(\(model.eventRegisteredCount ?? 0) Members)",attributes: countForListAttributes)
                        
                        cell.lblCount.attributedText = attributeCount
                    }
                    
                }
                if model.title == "Jobs"{
                    let attributeCount = NSMutableAttributedString(string: "\(model.count ?? 0 )(\(model.jobRegisteredResumeCount ?? 0) Resumes)",attributes: countForListAttributes)
                    
                    cell.lblCount.attributedText = attributeCount
                    
                }
                
            }
        }
        
        return cell
        
    }
    
    func getCellForReferralNewHeader(cell:HomeReferralHeaderNewCell,indexPath:IndexPath) -> HomeReferralHeaderNewCell {
        
        cell.selectionStyle = .none
        
        if let model:HomeSellerLeadModel = sellerWiseLeadList?[indexPath.section] {
            
            cell.imgViewLogo.image = nil
            
            if model.logo?.count ?? 0 > 0 {
                cell.imgViewLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.logo ?? "")")), completed: nil)
            }
            else{
                cell.imgViewLogo.image = UIImage.init(named: "Logo")
            }
            
            cell.lblCompanyName.text = model.sellerCompanyName ?? ""
            cell.lblTitle.text = model.referralLeadLabelText ?? ""
            cell.lblSellerID.text = model.sellerMemberId ?? ""
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableViewReferralList {
            
            if let model:HomeSellerDetailModel = sellerDetails?.productLeadStatus?[indexPath.row] {
                
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let referalLeadsPage:ReferalLeadsVC = storyBoard.instantiateViewController(withIdentifier: "ReferalLeadsVC") as!   ReferalLeadsVC
                referalLeadsPage.stageType = model.id ?? 0
                referalLeadsPage.type = 4
                referalLeadsPage.progressValue = String(model.progressValue ?? 0)
                referalLeadsPage.isFromHome = true
                referalLeadsPage.strTitle = "Referrals Leads".localized()
                referalLeadsPage.filterMasterList = filterMasterList ?? []
                navigationController?.pushViewController(referalLeadsPage, animated: true)
                
            }
            
        }
        if tableView == tableViewJobs {
            
            if let model:HomeSellerDetailModel = sellerDetails?.jobLeadStatus?[indexPath.row] {
                
                let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                let referalLeadsPage:ReferalLeadsVC = storyBoard.instantiateViewController(withIdentifier: "ReferalLeadsVC") as!   ReferalLeadsVC
                referalLeadsPage.stageType = model.id ?? 0
                referalLeadsPage.type = 5
                referalLeadsPage.isFromHome = true
                referalLeadsPage.progressValue = String(model.progressValue ?? 0)
                referalLeadsPage.strTitle = "Referrals Leads".localized()
                referalLeadsPage.filterMasterList = filterMasterList ?? []
                navigationController?.pushViewController(referalLeadsPage, animated: true)
                
            }
            
        }
        
        if tableView == tableViewReferralNewList{
            
            if indexPath.row > 0 {
            
             if let modelMain:HomeSellerLeadModel = sellerWiseLeadList?[indexPath.section] {
                      
              if let model:HomeReferralLeadsModel = modelMain.referralLeads?[indexPath.row - 1] {
                
                if model.title == "Jobs" {
                        
                          let storyBoard = UIStoryboard.init(name: JobSB, bundle: nil)
                                        let jobListPage:JobListFromHomeVC = storyBoard.instantiateViewController(withIdentifier: "JobListFromHomeVC") as! JobListFromHomeVC
                                        jobListPage.sellerCode = modelMain.sellerCode ?? 0
                                        jobListPage.filterMasterList = filterMasterList ?? []
    
//                                        if let colorModel:ColorModel = colorCodes{
//                                            jobListPage.colorCodes = colorModel
//                                        }
                                       // jobListPage.statusList = statusList ?? []
                                      //  jobListPage.delegate = self
//                                        jobListPage.isAdmin = model.isAdmin ?? 0
//                                        jobListPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
//                                        jobListPage.companyType = model.companyType ?? 0
                                        navigationController?.pushViewController(jobListPage, animated: true)
                    
                }
                
                else if model.title == "Events" {
                    let storyBoard = UIStoryboard.init(name: EventsSB, bundle: nil)
                              
                              if let eventPage:EventListFromHomeVC = storyBoard.instantiateViewController(withIdentifier: "EventListFromHomeVC") as? EventListFromHomeVC {
                                 
                                 eventPage.sellerCode = modelMain.sellerCode ?? 0
                                 eventPage.filterMasterList = filterMasterList ?? []
//                                 eventPage.isAdmin = model.isAdmin ?? 0
//                                 eventPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
                             //    eventPage.companyType = model.companyType ?? 0
                              //   eventPage.statusList = statusList ?? []
                            //     eventPage.delegate = self
//                                  if let colorModel:ColorModel = colorCodes{
//                                      eventPage.colorCodes = colorModel
//                                  }
                                 navigationController?.pushViewController(eventPage, animated: true)
                                  
                              }
                    
                  }
                else{
                    if let modelMain:HomeSellerLeadModel = sellerWiseLeadList?[indexPath.section] {
                        
                        if let model:HomeReferralLeadsModel = modelMain.referralLeads?[indexPath.row - 1] {
                            
                            let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
                            let referalLeadsPage:ReferalLeadsVC = storyBoard.instantiateViewController(withIdentifier: "ReferalLeadsVC") as!   ReferalLeadsVC
                            //
                            referalLeadsPage.sellerCode = modelMain.sellerCode ?? 0
                            referalLeadsPage.progressValue = String(model.progressValue ?? 0)
                            referalLeadsPage.isFromHomeNew = true
                            referalLeadsPage.isFromHome = true
                            referalLeadsPage.strTitle = "Referrals Leads".localized()
                            referalLeadsPage.filterMasterList = filterMasterList ?? []
                            navigationController?.pushViewController(referalLeadsPage, animated: true)
                            
                            //sellerCode,
                            
                        }
                    }
                }
            }
          }
        }
      }
    }
    
}

class HomeBannerCell:UICollectionViewCell{
    
    @IBOutlet weak var imgViewHeader: UIImageView!
    
}

class HomeBuyerCell:UICollectionViewCell{
    
    
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet weak var lblViewCount: NKLabel!
    
    @IBOutlet weak var viewRating: UIView!
    
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblRatingCount: NKLabel!
    
    @IBOutlet weak var imgViewProduct: UIImageView!
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblProductDes: NKLabel!
    
    @IBOutlet weak var lblFooter: NKLabel!
    
    @IBOutlet weak var lblLoyalty: NKLabel!
    
    @IBOutlet weak var lblReferredBy: NKLabel!
    
    @IBOutlet weak var widthRatingCount: NSLayoutConstraint!
    
    @IBOutlet weak var viewAppoinmentTime: UIView!
    
    @IBOutlet weak var heightViewAppoinment: NSLayoutConstraint!
    
    @IBOutlet weak var viewNoAppointment: UIView!
    
    @IBOutlet weak var lblDate: NKLabel!
      
    @IBOutlet weak var lblMemberCount: NKLabel!
      
    @IBOutlet weak var btnSeeMembers: UIButton!
}

class ReferralListCell:UITableViewCell{
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var lblCount: NKLabel!
    
}

class HomeReferralHeaderNewCell:UITableViewCell{
    @IBOutlet weak var lblCompanyName: NKLabel!
    @IBOutlet weak var imgViewLogo: UIImageView!
    @IBOutlet weak var lblSellerID: NKLabel!
    @IBOutlet weak var lblTitle: NKLabel!
}
class HomeReferralListNewCell:UITableViewCell{
    
    @IBOutlet weak var lblTitle: NKLabel!
    @IBOutlet weak var lblCount: NKLabel!
    @IBOutlet weak var viewCountColor: UIView!
    @IBOutlet weak var viewMainBackground: RCView!
    
}
