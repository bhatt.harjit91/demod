//
//  HomePopUpView.swift
//  NearKart
//
//  Created by TalentMicro on 13/05/19.
//  Copyright © 2019 RaviKiran B. All rights reserved.
//

import UIKit

class HomePopUpView: UIView {
    
    let kCONTENT_XIB_NAME = "HomePopUpView"
    
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightDescriptionConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightImageViewConstraint: NSLayoutConstraint!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imgviewPopup: UIImageView!
    @IBOutlet weak var btnGotIt: NKButton!
    @IBOutlet weak var lblTitle: NKLabel!
    @IBOutlet weak var lblDescription: NKLabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
    }

    
    @IBAction func gotItBtn(_ sender: Any) {
        
        if UserDefaults.standard.bool(forKey: "logoutForce") == true
        {
            UserDefaults.standard.removeObject(forKey: "logoutForce")
            
            self.removeFromSuperview()

            NotificationCenter.default.post(name: Notification.Name("ForceSignOut"), object: nil)
        }
        else
        {
            self.removeFromSuperview()
        }
    }
}

extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
