//
//  HomeAmountClaimPopUp.swift
//  iCanRefer
//
//  Created by TalentMicro on 28/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class HomeAmountClaimPopUp: UIView {

    let kCONTENT_XIB_NAME = "HomeAmountClaimPopUp"
    
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    //@IBOutlet weak var heightDescriptionConstraint: NSLayoutConstraint!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imgviewPopup: UIImageView!
    @IBOutlet weak var btnGotIt: NKButton!
    @IBOutlet weak var lblDescription: NKLabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
   //     btnGotIt.setTitle("Ok, Got It".localized(), for: .normal)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView.fixInView(self)
    }

    
    @IBAction func gotItBtn(_ sender: Any) {
        
//        if UserDefaults.standard.bool(forKey: "logoutForce") == true
//        {
//            UserDefaults.standard.removeObject(forKey: "logoutForce")
//
//            self.removeFromSuperview()
//
//            NotificationCenter.default.post(name: Notification.Name("ForceSignOut"), object: nil)
//        }
//        else
//        {
            self.removeFromSuperview()
        //}
    }

}
