//
//  HomeManageCompanyPopUp.swift
//  iCanRefer
//
//  Created by TalentMicro on 13/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

//protocol HomeManageCompanyPopUpDelegate: class {
//
//    func didSendValue(isRequestForTraining:Int,isRequestForActivation:String)
//
//}
class HomeManageCompanyPopUp: UIView {

   let kCONTENT_XIB_NAME = "HomeManageCompanyPopUp"
      
      @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
      @IBOutlet var contentView: UIView!
      @IBOutlet weak var btnCancel: UIButton!
      @IBOutlet weak var btnRequestTraining: NKButton!
      @IBOutlet weak var btnRequestActivation: NKButton!
      @IBOutlet weak var lblDescription: NKLabel!
    
    @IBOutlet weak var imgClose: UIImageView!
    
    @IBOutlet weak var viewCancelBg: RCView!
    
    @IBOutlet weak var viewPopUp: UIView!
    override init(frame: CGRect) {
            super.init(frame: frame)


            commonInit()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonInit()
        }
        
        func commonInit() {
            Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
            contentView.fixInView(self)
        }

        
        @IBAction func btnCancelACtion(_ sender: Any) {
            
  
                self.removeFromSuperview()
           
        }
    
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != viewPopUp {
            self.removeFromSuperview()
       }
   }


}
