//
//  LocateReferralPartnerVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class LocateReferralPartnerVC: AppBaseViewController,LocationPermissionVCNewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var lblReferalPartnerCount: NKLabel!
    
    @IBOutlet weak var imgViewLocation: UIImageView!
    
    @IBOutlet weak var txtReferralPartnerCode: RCTextField!
    
    @IBOutlet weak var btnLocatePartner: NKButton!
    
    @IBOutlet weak var btnLocation: UIButton!
    
    @IBOutlet weak var lblAddress: NKLabel!
    
    @IBOutlet weak var heightViewAddress: NSLayoutConstraint!
    
    var currentLoc:CLLocationCoordinate2D?
    
    var locationManager = CLLocationManager()
    
    var referredUserList:[GetLocateReferralModel]?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
         getCurrentLocation()
        
        lblAddress.text = "Fetching Location...".localized()
        
        updateAddressViewHeight()
        
        imgViewLocation.setImageColor(color: .white)

        navigationItem.title = "iCanRefer".localized()
        
        lblReferalPartnerCount.text = "Locate nearby Partners".localized()
        
        btnLocatePartner.setTitle("Locate Partners".localized(), for: .normal)
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateLocationName()
    }
    
    func openMap() {
        
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        locationPage.lat = currentLoc?.latitude ?? 0.0
        locationPage.lng = currentLoc?.longitude ?? 0.0
        locationPage.modalPresentationStyle = .fullScreen
        navigationController?.present(locationPage, animated: true)
    }
    

    @IBAction func btnLocationAction(_ sender: UIButton) {
        
        openMap()
    }
    
    func updateAddressViewHeight() {
        
        guard let labelText = lblAddress.text else { return }
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        heightViewAddress.constant = height + 40
        
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let size = CGSize(width: view.frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: UIFont.fontForType(textType: 6)]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }
    

    @IBAction func btnSelectLocation(_ sender: Any) {
       
        openMap()
        
    }
    @IBAction func btnLocatePartnerAction(_ sender: NKButton) {
        
        if txtReferralPartnerCode.text?.count ?? 0 > 0 {
            
            getReferralPartner()
            
        }
        
       else if currentLoc != nil {
        
        let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
        let sellerPage:SelectReferalPartnerVC = storyBoard.instantiateViewController(withIdentifier: "SelectReferalPartnerVC") as!  SelectReferalPartnerVC
        sellerPage.currentLoc = currentLoc
        
        navigationController?.pushViewController(sellerPage, animated: true)
            
        }
        
    }
    
    func getCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch locationAuthorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.startUpdatingLocation()
                currentLoc = locationManager.location?.coordinate
                
                updateLocationName()
            }
        case .restricted, .denied:
            
           // showLocationRequiredPage()
            
            print("Access Denied")
            
        }
    }
    
    func showLocationRequiredPage(){
        weak var weakSelf = self
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable location access to provide you near by search.".localized(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Enable Location Service".localized(), style: .default, handler: {action in
            
            weakSelf?.openAppSettings()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: Location manager
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    if let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
        
        currentLoc = locValue
        
        updateLocationName()
            
      }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        stopAnimating()
    }
    
    func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        currentLoc = coordinate
        updateLocationName()
    }
    
    func updateLocationName() {
        let geoCoder = GMSGeocoder()
        
        weak var weakSelf = self
        
        geoCoder.reverseGeocodeCoordinate(currentLoc ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), completionHandler: { response, error in
            
            if response != nil {
                let address = response?.results()?.first
                if address != nil {
                    
                    weakSelf?.lblAddress.text = address?.lines?.joined(separator: ",")
                    
                    weakSelf?.updateAddressViewHeight()
                }
            }
            
        })
    }
    
    func getReferralPartner() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        
       startAnimatingAfterSubmit()
        
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/referredBy?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&referralCode=\(txtReferralPartnerCode.text ?? "")"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataObj = jsonObj?.dataObj(){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(GetLocateReferralApiModel.self, from: data)
                                
                                if let apiResponse:[GetLocateReferralModel] = jsonResponse.referredUserList {

                                    weakSelf?.referredUserList = apiResponse

                                }
                                
                                 weakSelf?.showErrorMessage(message: message)
                                
                                let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
                                let sellerPage:SelectReferalPartnerVC = storyBoard.instantiateViewController(withIdentifier: "SelectReferalPartnerVC") as!  SelectReferalPartnerVC
                                sellerPage.currentLoc = self.currentLoc
                                
                                sellerPage.referredUserList = self.referredUserList ?? []
                                
                                if self.referredUserList?.count ?? 0 > 0 {
                                   self.navigationController?.pushViewController(sellerPage, animated: true)
                                    
                                }
                                
                                 weakSelf?.showErrorMessage(message: message)
                                
                                
                         //       weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
}
