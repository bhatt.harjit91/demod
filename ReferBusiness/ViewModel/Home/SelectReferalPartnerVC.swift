//
//  SelectReferalPartnerVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 01/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation

class SelectReferalPartnerVC: AppBaseViewController {

   
    @IBOutlet weak var lblTopTitle: NKLabel!
    
    @IBOutlet weak var viewBranches: RCView!
    @IBOutlet weak var tableViewBranchList: UITableView!
    
    @IBOutlet weak var lblStepThreeTitle: NKLabel!
    @IBOutlet weak var txtViewStepThree: NKTextView!
    
    @IBOutlet weak var lblStepOneTitle: NKLabel!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    var topTitle:String?
    var stepOne:String?
    var stepTwo:String?
    var placeHolder:String?
    var referralMemberList:[ReferralMemberList]?
    
     var referredUserList:[GetLocateReferralModel]?
    
    var count:Int?
    var isSelected:Int = 0
    var transactionId:Int = 0
    
    var currentLoc:CLLocationCoordinate2D?
    
    
    
    @IBOutlet weak var heightViewPartners: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       navigationItem.title = "iCanRefer".localized()
        
       txtViewStepThree.delegate = self
        
         fetchReferralDetails()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       // fetchReferralDetails()
        
    }
    override func viewDidLayoutSubviews() {
        
        if referralMemberList?.count ?? 0 > 0 {
            
            heightViewPartners.constant = tableViewBranchList.contentSize.height + 50
         
        }
        
    }
    
    func updateUI() {
        
        lblTopTitle.text = topTitle ?? ""
        lblStepThreeTitle.text = stepTwo ?? ""
        lblStepOneTitle.text = stepOne ?? ""
        
        if referralMemberList?.count ?? 0 > 0 {
            
            heightViewPartners.constant = tableViewBranchList.contentSize.height + 50
            
        }
      //  viewDidLayoutSubviews()
        
    }
    

    @IBAction func btnSubmitAction(_ sender: NKButton) {
        
        if txtViewStepThree.text.count == 0 {
            
            txtViewStepThree.showError(message: "Required".localized())
            
            return
        }
        
        submitSellerDetails()
    }
    
    func fetchReferralDetails() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/searchReferralMembers?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&latitude=\(currentLoc?.latitude ?? 0.0)&longitude=\(currentLoc?.longitude ?? 0.0)&keywords=\("")"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(LocateReferralApiMaster.self, from: data)
                                
                                if let apiResponse:[ReferralMemberList] = jsonResponse.referralMemberList {

                                    weakSelf?.referralMemberList = apiResponse

                                }
                                if let apiResponse:String = jsonResponse.title {
                                    
                                    weakSelf?.topTitle = apiResponse
                                }
                                
                        
                                
                                if let apiResponse:String = jsonResponse.stepOne {
                                    
                                    weakSelf?.stepOne = apiResponse
                                    
                                }
                                
                                if let apiResponse:String = jsonResponse.stepTwo {
                                    
                                    weakSelf?.stepTwo = apiResponse
                                    
                                }
                                
                                if let apiResponse:String = jsonResponse.placeHolder {
                                    
                                    weakSelf?.placeHolder = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.count = apiResponse
                                    
                                }
                                
                                weakSelf?.tableViewBranchList.reloadData()
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func submitSellerDetails(){
        
        startAnimatingAfterSubmit()
        
        var referalUserID:Int = 0
        
        if referredUserList?.count ?? 0 > 0 {
            
            if let model:GetLocateReferralModel = referredUserList?[isSelected]{
                   
                referalUserID = model.referredUserId ?? 0
                
            }
        }
        else{
        
        if let model:ReferralMemberList = referralMemberList?[isSelected] {
            
            referalUserID = model.referralUserId ?? 0
            
        }
        
        }
        
        
        let params:NSDictionary = [ "transactionId": transactionId,
                                    "acutalMessage": txtViewStepThree.text ?? "",
                                    "referralUserId": referalUserID

        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/submRequirementToReferralPartner?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                //                            let apiResponse = try decoder.decode(CategoryListModel.self, from: data)
                                //                            weakSelf?.categoryNewList = apiResponse.categoriesList ?? []
                                //                            weakSelf?.searchCategoryList = apiResponse.categoriesList ?? []
                                //
                                //                            DispatchQueue.main.async {
                                //
                                //                                weakSelf?.collectionViewCategory.reloadData()
                                //                            }
                                
                                let isUpdated:[String: Int] = ["update": 1]

                                   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
                                
                                
                                weakSelf?.stopAnimating()
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
             weakSelf?.stopAnimating()
            //Show error
              weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
}

extension SelectReferalPartnerVC:UITextViewDelegate {
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        if textView == txtViewStepThree{
            
            txtViewStepThree.showError(message: "")
            
        }
        
        return true
        
    }
    
    
}

extension SelectReferalPartnerVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if referredUserList?.count ?? 0 > 0 {
            
          return referredUserList?.count ?? 0
        }
        
        else{
            
            return referralMemberList?.count ?? 0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          if let cell:LocateReferralBranchesCell = tableView.dequeueReusableCell(withIdentifier:"LocateReferralBranchesCell", for: indexPath) as? LocateReferralBranchesCell{
            
            return getCellForLocateBranches(cell: cell, indexPath: indexPath)
          }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        isSelected = indexPath.row
        
        tableViewBranchList.reloadData()
    }
    
    
    func getCellForLocateBranches(cell:LocateReferralBranchesCell,indexPath:IndexPath) -> LocateReferralBranchesCell {
        
        cell.selectionStyle = .none
        
        if referredUserList?.count ?? 0 > 0 {

            if let model:GetLocateReferralModel = referredUserList?[indexPath.row] {
                
                cell.lblTitle.text = model.displayName ?? ""
                
                cell.imgViewRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled")
                
                if isSelected == indexPath.row {
                           
                           cell.imgViewRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled")
                           
                       }
             else{
                    cell.imgViewRadio.image = UIImage.init(imageLiteralResourceName: "radio")
                }
                
               // isSelected = indexPath.row
            }
        
        }
        else {
        
        if let model:ReferralMemberList = referralMemberList?[indexPath.row] {
        
          cell.lblTitle.text = model.displayName ?? ""
            
        }
        if isSelected == indexPath.row {
            
            cell.imgViewRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled")
            
        }
        else{
            cell.imgViewRadio.image = UIImage.init(imageLiteralResourceName: "radio")
        }
            
      }
        
        return cell
    }
    

}


class LocateReferralBranchesCell: UITableViewCell {
    
    
    @IBOutlet weak var imgViewRadio: UIImageView!
    
    @IBOutlet weak var lblTitle: NKLabel!
    
}



