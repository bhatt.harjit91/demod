//
//  PostReviewsVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 10/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class RatingMasterApiModel: Codable {
    
    var ratings:[RatingMasterModel]?
    
}

protocol PostReviewsVCDelegate: class {
    func didAddedReview()
}

class PostReviewsVC: AppBaseViewController {

    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    var ratingValue:Float = 0.0
    var ratingsMainList:[RatingMasterModel]?
    var afterRatingListModel:[PostRatingModel] = []
    var sellerCode:Int = 0
    var orderNo:Int = 0
    var reviewId = 0
    var ratingGlobalVar : Int = 0
    var selectedRatingVal : Double = 0.0
    var txtViewReviewText : String = ""
    var previousCommentText = ""
    
    var productCode:Int = 0
    var transactionId:Int = 0
    
    weak var delegate:PostReviewsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Review".localized()
        
         btnSubmit.alpha = 0
        
         btnSubmit.setTitle("Submit".localized(), for: .normal)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        fetchReviewList()
        
    }
    

    @IBAction func btnSubmitAction(_ sender: Any) {
        
        saveReviews()
        
    }
    
    //MARK: - API Call
    
    func fetchReviewList() {
        
        startAnimating()
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getreviewmaster?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        do {
                            
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
                            
                            print(dataObj)
                            let apiResponse = try decoder.decode(RatingMasterApiModel.self, from: data)
                            if let apiResponse:[RatingMasterModel] = apiResponse.ratings {
                                weakSelf?.ratingsMainList = apiResponse

                            }
                            if let copyModel = weakSelf?.ratingsMainList
                            {
                                for eachVal in copyModel
                                {
                                    let eachModel = PostRatingModel()
                                    eachModel.ratingType = eachVal.ratingTypeId
                                    eachModel.userRating = eachVal.rating
                                    weakSelf?.afterRatingListModel.append(eachModel)
                                }
                                weakSelf?.tableViewList.reloadData()
                            }
                            
                            
                            weakSelf?.tableViewList.reloadData()
                            
                            if weakSelf?.ratingsMainList?.count ?? 0 > 0 {
                                
                                weakSelf?.btnSubmit.alpha = 1
                            }
                            
                            
                            return
                            
                        } catch {
                            //print(error)
                            message = failedToConnectMessage.localized()
                        }
                      }
                    }
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    func saveReviews(){
        startAnimating()
        var array : [Any] = []
        for i in afterRatingListModel{
            var eachData:[String : Any] = [:]
            eachData["ratingType"] = i.ratingType
            eachData["rating"] = i.userRating
            array.append(eachData)
        }
        let params:NSDictionary = [
            "sellerCode":sellerCode,
            "transactionId":transactionId,
            "productCode":productCode,
            "comments":txtViewReviewText,
            "responseMessage": " ",
            "reviewId":reviewId,
            "ratingdetails":array
        ]
        let encryptedParams = ["data": encryptParams(params: params as NSDictionary)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/savereviewrating?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
                
            }
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String = jsonObj?.dataString() {
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                
                                let isUpdated:[String: Int] = ["update": 1]

                                   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
                                
                                
                                weakSelf?.delegate?.didAddedReview()
                               
                                print("Uploaded")
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                //return
                            }
                        }
                    }
                }
            }
            
            
            weakSelf?.stopAnimating()
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }

}

extension PostReviewsVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        txtViewReviewText = textView.text
    }
}

extension PostReviewsVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return ratingsMainList?.count ?? 0
        }
        else if section == 1
        {
            
            return 1
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            
            let cell:ReviewRatingStarCell = tableView.dequeueReusableCell(withIdentifier: "ReviewRatingStarCell", for: indexPath) as! ReviewRatingStarCell
            cell.selectionStyle = .none
            return getCellForReviewRatingStarCell(cell: cell, indexPath: indexPath, tableView: tableView)
            
        }
        
        if indexPath.section == 1{
            
            let cell:ReviewRatingNotesCell = tableView.dequeueReusableCell(withIdentifier: "ReviewRatingNotesCell", for: indexPath) as! ReviewRatingNotesCell
            cell.selectionStyle = .none
            return getCellForReviewRatingNotesCell(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
    }
    
    func  getCellForReviewRatingStarCell(cell: ReviewRatingStarCell, indexPath : IndexPath, tableView : UITableView) -> ReviewRatingStarCell
    {
        // print("\("rating:::::::")\(cell.ratingValue ?? 0.0)")
        cell.cosmosView.didTouchCosmos = didTouchCosmos
        cell.cosmosView.didFinishTouchingCosmos = didFinishTouchingCosmos
        cell.cosmosView.tag = indexPath.row
        let totalNumberOfStars = 10
        cell.cosmosView.totalStars = totalNumberOfStars
        cell.cosmosView.starSize = Double((tableView.frame.size.width - 35 ) / CGFloat(totalNumberOfStars))  - cell.cosmosView.starMargin
        if let model = ratingsMainList?[indexPath.row] {
            cell.lblTitle.text = model.ratingName ?? ""
            cell.cosmosView.rating = model.rating ?? 0.0
        }
//
        
        return cell
    }
    func  getCellForReviewRatingNotesCell(cell: ReviewRatingNotesCell, indexPath : IndexPath) -> ReviewRatingNotesCell
    {
        
        cell.txtViewNotes.text = previousCommentText
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //          let cell:ReviewRatingStarCell = tableView.dequeueReusableCell(withIdentifier: "ReviewRatingStarCell", for: indexPath) as! ReviewRatingStarCell
        
        //         print("\("rating:::::::")\(cell.ratingValue ?? 0.0)")
        ratingGlobalVar = indexPath.row
        print("Stars")
//        print(afterRatingListModel[indexPath.row].userRating ?? 0.0)
//        afterRatingListModel[indexPath.row].userRating = selectedRatingVal
//        print(afterRatingListModel[indexPath.row].userRating ?? 0.0)
        
        
    }
    private func didTouchCosmos(_ rating: Double) {
        ratingValue = Float(rating)
        
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        ratingValue = Float(rating)
        selectedRatingVal = rating
        
    }
    
    
}

class ReviewRatingStarCell: UITableViewCell {
    
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var cosmosView: CosmosView!
    
    var ratingValue:Float?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
    }
    
    
}


class ReviewRatingNotesCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
}
