//
//  UploadSellerAgreementVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class UploadSellerAgreementVC: SendOTPViewController {
    
    var sellerCode:Int = 0
    
    var aggreementHistory:[SellerAgreementModel]?
    
    @IBOutlet weak var collectionViewAttachments: UICollectionView!
    
    @IBOutlet weak var heightMainAggreementHistory: NSLayoutConstraint!
    
    @IBOutlet weak var heightAgreementList: NSLayoutConstraint!
    
    @IBOutlet weak var viewAggreementTitle: RCView!
    
    @IBOutlet weak var viewAggreement: RCView!
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var topSpaceViewSellerAgreementTitle: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceViewAgreementHistory: NSLayoutConstraint!
    
    @IBOutlet weak var heightBtnSubmit: NSLayoutConstraint!
    
    @IBOutlet weak var imgAttachmentIcon: UIImageView!
    
    var attachments:[AttachmentMainModel] = []
    
    var isPartner:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isPartner {
            
            fetchPartnerAgreementDetail()
            
             navigationItem.title = "Upload Signed Partner Agreement".localized()
        }
        else{
            
             navigationItem.title = "Upload Signed Seller Agreement".localized()
         
             fetchSellerAgreementDetail()
            
        }
        
         btnSubmit.alpha = 0
        
         heightBtnSubmit.constant = 0
        
        imgAttachmentIcon.setImageColor(color: .white)
        
    }
    
    
    override func viewDidLayoutSubviews() {
        
        if aggreementHistory?.count ?? 0 > 0 {
            
            viewAggreementTitle.alpha = 1
            viewAggreement.alpha = 1
            
            topSpaceViewSellerAgreementTitle.constant = 20
            topSpaceViewAgreementHistory.constant = 40
            heightAgreementList.constant = tableViewList.contentSize.height + 10
            heightMainAggreementHistory.constant = heightAgreementList.constant + 20
            
            
            
            DispatchQueue.main.async(execute: {
               
                var frame = self.tableViewList.frame
                frame.size.height = self.tableViewList.contentSize.height
                self.tableViewList.frame = frame
                
                self.heightAgreementList.constant = self.tableViewList.contentSize.height + 10
                
                self.heightMainAggreementHistory.constant = self.heightAgreementList.constant + 20
            })
            
        }
        
        else{
            
            viewAggreementTitle.alpha = 0
            viewAggreement.alpha = 0
        }
        
        
    }
    
    
    @IBAction func btnAttachment(_ sender: UIButton) {
        
        isAttachment = true
        isDocument = true
        openImagePicker()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
           
            if status {
                print(urlString, imageName)
                let arrayEachImage:AttachmentMainModel = AttachmentMainModel()
                arrayEachImage.cdnPath = urlString
                arrayEachImage.fileName = imageName
                weakSelf?.attachments.append(arrayEachImage)
                weakSelf?.collectionViewAttachments.reloadData()
                
                self.isAttachment = false
            }
            
        }
        
    }
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachments.count > sender.tag {
            attachments.remove(at: sender.tag)
            collectionViewAttachments.reloadData()
        }
    }
    

    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
        if isPartner {
          
            savePartnerAgreement()
        }
        else{
            
            saveSellerAgreement()
            
        }
        
    }
    
    
    func fetchSellerAgreementDetail() {
        
        let apiClient:APIClient = APIClient()
        
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getSellerAgreements?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(SellerAgreementApiModel.self, from: data)
                                
                                if let apiResponse:[SellerAgreementModel] = jsonResponse.aggreementHistory {

                                    weakSelf?.aggreementHistory = apiResponse

                                }
                                
                             //   weakSelf?.updateUI()
                                
                                weakSelf?.tableViewList.reloadData()
                                weakSelf?.collectionViewAttachments.reloadData()
                                
                            //    weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                        
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func saveSellerAgreement(){
        
       
        
        startAnimatingAfterSubmit()
        
        var array : [Any] = []
              
                     for dict in attachments
                     {
                         var model:[String : Any] = [:]
                         model["cdnPath"] = dict.cdnPath
                         model["fileName"] = dict.fileName
                         array.append(model)
                     }
        
        let params:NSDictionary = [
                                   "agreementId":0,
                                   "agreements":array
                
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/uploadSellerAgreements?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&sellerCode=\(sellerCode)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                              
                               
                                weakSelf?.showInfoMessage(message: message.localized())
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
  // If Partner
    
    func fetchPartnerAgreementDetail() {
        
        let apiClient:APIClient = APIClient()
        
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getPartnerAgreements?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(SellerAgreementApiModel.self, from: data)
                                
                                if let apiResponse:[SellerAgreementModel] = jsonResponse.aggreementHistory {
                                    
                                    weakSelf?.aggreementHistory = apiResponse
                                    
                                }
                                
                                //   weakSelf?.updateUI()
                                
                                weakSelf?.tableViewList.reloadData()
                                weakSelf?.collectionViewAttachments.reloadData()
                                
                                //    weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                        
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func savePartnerAgreement(){
        
        
        
        startAnimatingAfterSubmit()
        
        var array : [Any] = []
        
               for dict in attachments
               {
                   var model:[String : Any] = [:]
                   model["cdnPath"] = dict.cdnPath
                   model["fileName"] = dict.fileName
                   array.append(model)
               }
        
        let params:NSDictionary = [
            "agreementId":0,
            "agreements":array
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/uploadPartnerAgreements?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                
                                weakSelf?.showInfoMessage(message: message.localized())
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
}

extension UploadSellerAgreementVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        
        return aggreementHistory?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:UploadAggreementCell = tableView.dequeueReusableCell(withIdentifier: "UploadAggreementCell", for: indexPath) as? UploadAggreementCell
        {
            
            return getCellForUploadAggreement(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
     if let model:SellerAgreementModel = aggreementHistory?[indexPath.row]{
        
        let storyBoard = UIStoryboard.init(name: SellerSB, bundle: nil)
        if let newPage:EditSellerAgreementVC = storyBoard.instantiateViewController(withIdentifier: "EditSellerAgreementVC") as? EditSellerAgreementVC  {
            
        newPage.sellerCode = model.sellerCode ?? 0
        newPage.agreementId = model.agreementId ?? 0
        newPage.attachments = model.agreements ?? []
        newPage.isPartner = isPartner
        newPage.delegate = self
        self.navigationController?.pushViewController(newPage, animated: true)
            
        }
      }
    }
    
    func getCellForUploadAggreement(cell:UploadAggreementCell, indexPath:IndexPath) -> UploadAggreementCell {
        
        cell.selectionStyle = .none
        
        if let model:SellerAgreementModel = aggreementHistory?[indexPath.row]{
            
            cell.lblTitle.text = "\("Agreement uploaded by".localized())\(" ")\(model.createdBy ?? "")"
            
            if model.agreements?.count ?? 0 > 0 {
            
                if model.agreements?.count == 1 {
                
                    cell.lblAttachmentCount.text = "\(model.agreements?.count ?? 0 )\(" ")\("Attachment".localized())"
            
            }
            else{
                
                    cell.lblAttachmentCount.text = "\(model.agreements?.count ?? 0 )\(" ")\(" Attachments".localized())"
            }
         }
            
            cell.lblAttachmentCount.textColor = UIColor.getGradientColor(index: 0).first!
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.crDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblDate.text = dateText
            
        }
     
        return cell
    }
    
    
}

extension UploadSellerAgreementVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 100, height: 100)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if attachments.count > 0 {
                
                btnSubmit.alpha = 1
                
                topSpaceViewSellerAgreementTitle.constant = 70
                topSpaceViewAgreementHistory.constant = 90
                
                heightBtnSubmit.constant = 44
                
                return attachments.count
            }
        
           btnSubmit.alpha = 0
        topSpaceViewSellerAgreementTitle.constant = 20
        topSpaceViewAgreementHistory.constant = 40
        
        heightBtnSubmit.constant = 0
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachment(cell: cell, indexPath: indexPath)
                
            }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewAttachments {
                
            if let fileName:AttachmentMainModel = attachments[indexPath.row]{
                    

                if fileName.cdnPath?.count ?? 0 > 0 {
                        
                  if fileName.fileName?.count ?? 0 > 0 {
                    
                    let strFileType = fileName.fileName?.fileExtension()

                    if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType!.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                     {
                           
                let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                      let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                        imageViewer.strImg = fileName.cdnPath ?? ""
                      navigationController?.pushViewController(imageViewer, animated: true)
                        
                    }
                    
                    else{
                       
                    openWebView(urlString:fileName.cdnPath ?? "", title: fileName.fileName ?? "", subTitle: "")
                    }
                    
                    }
                 }
                    
                }
                    
            }
    }
    
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.alpha = 1
        cell.btnDelete .addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        if let fileName:AttachmentMainModel = attachments[indexPath.row]
        {
            if fileName.cdnPath?.count ?? 0 > 0
                {
                    if fileName.fileName?.count ?? 0 > 0{
                      
                    if let url:URL = URL.init(string: "\(StorageUrl)\(fileName.cdnPath ?? "")"){
                      
                        let strFileType = fileName.fileName?.fileExtension()

                        if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                           {
                      
                        cell.imgView.sd_setImage(with: url, completed: nil)
                          
                      }
                       else{
                          cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                      }
                      
                    }
                }
             
                              
                
            }
            else{
                       cell.btnDelete.alpha = 0
                       
                   }
            
        }
        
        return cell
    }
    
    
    
}

class UploadAggreementCell: UITableViewCell {
    
    @IBOutlet weak var viewBg: RCView!
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var lblAttachmentCount: NKLabel!
    
    @IBOutlet weak var lblDate: NKLabel!
}

extension UploadSellerAgreementVC:EditSellerAgreementVCDelegate{
    
    func didAddedAttachment() {
        
        if isPartner {
                  
            fetchPartnerAgreementDetail()
                  
        }
        else{
                
            fetchSellerAgreementDetail()
            
        }
    }
}
