//
//  SellerListVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 08/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class SellerListVC: AppBaseViewController {
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblSellerCount: NKLabel!
    
    @IBOutlet weak var lblNoData: NKLabel!
    
    var sellerList:[SellerModel]?
    
    var statusList:[SellerStatusModel]?
    
    var sellerCount:Int = 0
    
    var colorCodes:ColorModel?
    
    var billingCycle:[BillingCycleModel]?
    
    var attachmentTypes:[AttachmentTypeModel]?
    
    var categoryList:[SellerCategoryModel]?
    
    var currency:[CurrencyModel]?
    
    var timePeriodList:[TimePeriodModel]?
    
    var accessUserTypes:[AccessUserTypesModel]?
    
    var filterMasterList:[FilterReferralModel]?
    
    var companyType:[CompanyTypesModel]?
    
    let mainTitleAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 6),
        .foregroundColor: UIColor.getGradientColor(index: 0).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let mainCountAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 6),
        .foregroundColor: UIColor.white,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let attributedPopUpTextOne: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 2),
        .foregroundColor: UIColor.black]
    
    let attributedPopUpTextTwo: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 8),
        .foregroundColor: UIColor.primaryColor]
    
    var homeManage = HomeManageCompanyPopUp()
    
    var isReferralMember:Int = 0
    
    
    
    var masterDetails:SellerProductBranchPopUpModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btnName = UIButton()
        //  btnName.setImage(UIImage(named: "walletIcon"), for: .normal)
        btnName.setTitle("New -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(newSellerAction), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        btnName.alpha = 0
        
        navigationItem.title = "Manage Sellers".localized()
        
        lblNoData.text = "No Data Found".localized()
        
        if let userModel = getUserDetail() {
            
            isReferralMember = userModel.isReferralMember ?? 0
            
        }
        
        if isReferralMember == 1 {
            
            btnName.alpha = 1
            
        }
        
        fetchSellerList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.integer(forKey: "isBranch") == 0 && UserDefaults.standard.integer(forKey: "isProduct") == 1
        {
            alertForProduct()
            
            UserDefaults.standard.set(0, forKey: "isProduct")
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        stopAnimating()
    }
    
    @objc func newSellerAction(sender: UIButton){
        
        let storyBoard = UIStoryboard.init(name: SellerSB, bundle: nil)
        let newSellerPage:NewSellerVC = storyBoard.instantiateViewController(withIdentifier: "NewSellerVC") as! NewSellerVC
        newSellerPage.stausArray = statusList ?? []
        newSellerPage.billingCycle = billingCycle ?? []
        newSellerPage.attachmentTypes = attachmentTypes ?? []
        newSellerPage.delegate = self
        newSellerPage.categoryList = categoryList ?? []
        newSellerPage.currency = currency ?? []
        newSellerPage.timePeriodList = timePeriodList ?? []
        newSellerPage.isPartnerForCompany = 1
        newSellerPage.companyType = companyType ?? []
        navigationController?.pushViewController(newSellerPage, animated: true)
        
    }
    
    @objc func btnBranchesAction(sender: UIButton){
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: BranchSB, bundle: nil)
            let branchListPage:BranchListVC = storyBoard.instantiateViewController(withIdentifier: "BranchListVC") as! BranchListVC
            
            branchListPage.sellerCode = model.sellerCode ?? 0
            
            if let colorModel:ColorModel = colorCodes{
                branchListPage.colorCodes = colorModel
            }
            branchListPage.delegate = self
            branchListPage.isAdmin = model.isAdmin ?? 0
            branchListPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
            navigationController?.pushViewController(branchListPage, animated: true)
            
        }
    }
    
    @objc func btnBranchesFromPopUpAction(sender: UIButton){
        
        homeManage.removeFromSuperview()
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: BranchSB, bundle: nil)
            let branchListPage:BranchListVC = storyBoard.instantiateViewController(withIdentifier: "BranchListVC") as! BranchListVC
            
            branchListPage.sellerCode = model.sellerCode ?? 0
            
            if let colorModel:ColorModel = colorCodes{
                branchListPage.colorCodes = colorModel
            }
            branchListPage.delegate = self
            branchListPage.isAdmin = model.isAdmin ?? 0
            branchListPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
            navigationController?.pushViewController(branchListPage, animated: true)
            
        }
    }
    
    
    
    @objc func btnProductsAction(sender: UIButton){
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            if model.branchCount ?? 0 > 0 {
                
                let storyBoard = UIStoryboard.init(name: ProductSB, bundle: nil)
                let productListPage:ProductListVC = storyBoard.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
                productListPage.sellerCode = model.sellerCode ?? 0
                if let colorModel:ColorModel = colorCodes{
                    productListPage.colorCodes = colorModel
                }
                productListPage.statusList = statusList ?? []
                productListPage.delegate = self
                productListPage.isAdmin = model.isAdmin ?? 0
                productListPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
                productListPage.companyType = model.companyType ?? 0
                navigationController?.pushViewController(productListPage, animated: true)
                
            }
            else{
                
                (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.showErrorMessage(message: "Please add Branch".localized())
                
            }
        }
    }
    
    @objc func btnProductFromPopUpAction(sender: UIButton){
        
        homeManage.removeFromSuperview()
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            if model.branchCount ?? 0 > 0 {
                
                let storyBoard = UIStoryboard.init(name: ProductSB, bundle: nil)
                let productListPage:ProductListVC = storyBoard.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
                productListPage.sellerCode = model.sellerCode ?? 0
                if let colorModel:ColorModel = colorCodes{
                    productListPage.colorCodes = colorModel
                }
                productListPage.statusList = statusList ?? []
                productListPage.delegate = self
                productListPage.isAdmin = model.isAdmin ?? 0
                productListPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
                productListPage.companyType = model.companyType ?? 0
                navigationController?.pushViewController(productListPage, animated: true)
                
            }
        }
    }
    
    @objc func btnReferalAction(sender: UIButton){
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
            let referalLeadsPage:ReferalLeadsVC = storyBoard.instantiateViewController(withIdentifier: "ReferalLeadsVC") as!   ReferalLeadsVC
            referalLeadsPage.sellerCode = model.sellerCode ?? 0
            referalLeadsPage.filterMasterList = filterMasterList ?? []
            
            //            if let colorModel:ColorModel = colorCodes{
            //                productListPage.colorCodes = colorModel
            //            }
            //         referalLeadsPage.isAdmin = model.isAdmin ?? 0
            //       referalLeadsPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
            navigationController?.pushViewController(referalLeadsPage, animated: true)
        }
    }
    @objc func btnUserAction(sender: UIButton){
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: UserSB, bundle: nil)
            let productListPage:UserListVC = storyBoard.instantiateViewController(withIdentifier: "UserListVC") as! UserListVC
            
            productListPage.sellerCode = model.sellerCode ?? 0
            productListPage.statusList = statusList ?? []
            productListPage.accessUserTypes = accessUserTypes ?? []
            
            if let colorModel:ColorModel = colorCodes{
                productListPage.colorCodes = colorModel
            }
            productListPage.delegate = self
            productListPage.isAdmin = model.isAdmin ?? 0
            productListPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
            navigationController?.pushViewController(productListPage, animated: true)
            
        }
    }
    
    @objc func btnCustomerAction(sender: UIButton){
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: CustomerSB, bundle: nil)
            
            let customerPage:CustomerListVC = storyBoard.instantiateViewController(withIdentifier: "CustomerListVC") as!   CustomerListVC
            
            customerPage.sellerCode = model.sellerCode ?? 0
            
            if let colorModel:ColorModel = colorCodes{
                customerPage.colorCodes = colorModel
            }
            
            customerPage.delegate = self
            
            //   customerPage.statusList = statusList ?? []
            customerPage.isAdmin = model.isAdmin ?? 0
            customerPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
            navigationController?.pushViewController(customerPage, animated: true)
        }
    }
    
    @objc func btnTestimonialAction(sender: UIButton){
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: TestimonialSB, bundle: nil)
            let testimonialListPage:TestimonialListVC = storyBoard.instantiateViewController(withIdentifier: "TestimonialListVC") as!   TestimonialListVC
            testimonialListPage.sellerCode = model.sellerCode ?? 0
            if let colorModel:ColorModel = colorCodes{
                testimonialListPage.colorCodes = colorModel
            }
            
            testimonialListPage.delegate = self
            testimonialListPage.isAdmin = model.isAdmin ?? 0
            testimonialListPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
            navigationController?.pushViewController(testimonialListPage, animated: true)
            
        }
    }
    
    @objc func btnGiftsAction(sender: UIButton){
        
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: GiftSB, bundle: nil)
            if let giftsListPage:GiftViewController = storyBoard.instantiateViewController(withIdentifier: "GiftViewController") as? GiftViewController {
                
                giftsListPage.sellerCode = model.sellerCode ?? 0
                if let colorModel:ColorModel = colorCodes{
                    giftsListPage.colorCodes = colorModel
                }
                giftsListPage.delegate = self
                giftsListPage.isAdmin = model.isAdmin ?? 0
                giftsListPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
                navigationController?.pushViewController(giftsListPage, animated: true)
            }
        }
    }
    
    @objc func btnJobsAction(sender: UIButton){
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            if model.branchCount ?? 0 > 0 {
                
                let storyBoard = UIStoryboard.init(name: JobSB, bundle: nil)
                let jobListPage:JobListVC = storyBoard.instantiateViewController(withIdentifier: "JobListVC") as! JobListVC
                jobListPage.sellerCode = model.sellerCode ?? 0
                if let colorModel:ColorModel = colorCodes{
                    jobListPage.colorCodes = colorModel
                }
                jobListPage.statusList = statusList ?? []
                jobListPage.delegate = self
                jobListPage.isAdmin = model.isAdmin ?? 0
                jobListPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
                jobListPage.companyType = model.companyType ?? 0
                navigationController?.pushViewController(jobListPage, animated: true)
                
            }
            else{
                
                (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.showErrorMessage(message: "Please add Branch".localized())
                
            }
        }
    }
    
    ///
    
    
    @objc func btnSellerAcountAction(sender: UIButton){
        
        if let model:SellerModel = sellerList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: "Invoices", bundle: nil)
            let sellerPage:InvoicesViewController = storyBoard.instantiateViewController(withIdentifier: "InvoicesViewController") as! InvoicesViewController
            
            sellerPage.sellerCode = model.sellerCode ?? 0
            
            navigationController?.pushViewController(sellerPage, animated: true)
            
        }
    }
    
    @objc func btnEventAction(sender: UIButton){
           
           if let model:SellerModel = sellerList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: EventsSB, bundle: nil)
            
            if let eventPage:EventsViewController = storyBoard.instantiateViewController(withIdentifier: "EventsViewController") as? EventsViewController {
               
               eventPage.sellerCode = model.sellerCode ?? 0
               eventPage.isAdmin = model.isAdmin ?? 0
               eventPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
               eventPage.companyType = model.companyType ?? 0
               eventPage.statusList = statusList ?? []
               eventPage.delegate = self
                if let colorModel:ColorModel = colorCodes{
                    eventPage.colorCodes = colorModel
                }
               navigationController?.pushViewController(eventPage, animated: true)
                
            }
               
           }
       }
    
    @objc func btnJobManagerAction(sender: UIButton){
              
              if let model:SellerModel = sellerList?[sender.tag] {
                
                if model.branchCount ?? 0 > 0 {
                              
                              let storyBoard = UIStoryboard.init(name: JobManagerSB, bundle: nil)
                              if let jobManagerList:JobManagerListVC = storyBoard.instantiateViewController(withIdentifier: "JobManagerListVC") as? JobManagerListVC {
                              jobManagerList.sellerCode = model.sellerCode ?? 0
                              if let colorModel:ColorModel = colorCodes{
                                  jobManagerList.colorCodes = colorModel
                              }
                              jobManagerList.statusList = statusList ?? []
                              jobManagerList.delegate = self
                              jobManagerList.isAdmin = model.isAdmin ?? 0
                              jobManagerList.isPartnerForCompany = model.isPartnerForCompany ?? 0
                              jobManagerList.companyType = model.companyType ?? 0
                              navigationController?.pushViewController(jobManagerList, animated: true)
                             }
                          }
                          else{
                              
                              (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.showErrorMessage(message: "Please add Branch".localized())
                              
                          }

                  
              }
          }
    
    ///
    @objc func btnActions(sender: UIButton){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.view.tintColor = UIColor.btnLinkColor
        
        alert.addAction(UIAlertAction(title: "Edit".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            
            if let model:SellerModel = self.sellerList?[sender.tag]
            {
                let storyBoard = UIStoryboard.init(name: SellerSB, bundle: nil)
                let newSellerPage:NewSellerVC = storyBoard.instantiateViewController(withIdentifier: "NewSellerVC") as! NewSellerVC
                newSellerPage.stausArray = self.statusList ?? []
                newSellerPage.sellerCode = model.sellerCode ?? 0
                newSellerPage.billingCycle = self.billingCycle ?? []
                newSellerPage.attachmentTypes = self.attachmentTypes ?? []
                newSellerPage.isDetail = true
                newSellerPage.delegate = self
                newSellerPage.categoryList = self.categoryList ?? []
                newSellerPage.currency = self.currency ?? []
                newSellerPage.timePeriodList = self.timePeriodList ?? []
                newSellerPage.isAdmin = model.isAdmin ?? 0
                newSellerPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
                newSellerPage.companyType = self.companyType ?? []
                self.navigationController?.pushViewController(newSellerPage, animated: true)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Send proposal".localized(), style: UIAlertAction.Style.default, handler: { _ in
                   
                   if let model:SellerModel = self.sellerList?[sender.tag]
                   {
                       
                       self.alertSendProposalLink(selectedSellerCode: model.sellerCode ?? 0)
                       
                   }
                   
               }))
        
        alert.addAction(UIAlertAction(title: "Send update link to Company".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            if let model:SellerModel = self.sellerList?[sender.tag]
            {
                
                self.alertSendUpdateLink(selectedSellerCode: model.sellerCode ?? 0)
                
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Send Company Agreement for Signing".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            
            if let model:SellerModel = self.sellerList?[sender.tag]
            {
                if model.productCount ?? 0 > 0 || model.jobsCount ?? 0 > 0 {
                    
                    
                    self.alertSendAgreementLink(selectedSellerCode: model.sellerCode ?? 0)
                    
                    
                }
                    
                else {
                    
                    (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.showErrorMessage(message: "Please add a Product / Job".localized())
                }
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Upload Signed Company Agreement".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            if let model:SellerModel = self.sellerList?[sender.tag]
            {
                
                if model.productCount ?? 0 > 0 {
                    
                    let storyBoard = UIStoryboard.init(name: SellerSB, bundle: nil)
                    if let newSellerPage:UploadSellerAgreementVC = storyBoard.instantiateViewController(withIdentifier: "UploadSellerAgreementVC") as? UploadSellerAgreementVC  {
                        
                        newSellerPage.sellerCode = model.sellerCode ?? 0
                        
                        self.navigationController?.pushViewController(newSellerPage, animated: true)
                        
                    }
                }
                    
                else {
                    
                    (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.showErrorMessage(message: "Please add Product".localized())
                }
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(),
                                      style: UIAlertAction.Style.cancel,
                                      handler: {(_: UIAlertAction!) in
                                        
        }))
        
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = sender.bounds
        }
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func alertForBranch() {
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        
        homeManage = HomeManageCompanyPopUp.init(frame: UIScreen.main.bounds)
        
        //homeManage.lblDescription.text = requestMessageText ?? ""
        
        homeManage.btnRequestTraining.colorIndex = 1
        
        let combinedString  = NSMutableAttributedString()
        
        let str1 = NSMutableAttributedString(string: "\(masterDetails?.newBranchMessage ?? "") \n\n", attributes: attributedPopUpTextOne)
        
        let str2 = NSMutableAttributedString(string: "\(masterDetails?.newBranchNote ?? "")", attributes: attributedPopUpTextTwo)
        
        combinedString.append(str1)
        
        combinedString.append(str2)
        
        homeManage.lblDescription.attributedText = combinedString
        
        guard let labelText = homeManage.lblDescription.text else { return }
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        //    homeManage.contentViewHeightConstraint.constant = height + 70
        
        homeManage.btnRequestTraining.setTitle("Later".localized(), for: .normal)
        
        homeManage.btnRequestActivation.setTitle("Proceed".localized(), for: .normal)
        
        homeManage.viewCancelBg.backgroundColor = .primaryColor
        
        homeManage.imgClose.setImageColor(color: .white)
        
        homeManage.btnRequestTraining.addTarget(self, action: #selector(btnLaterAction(sender:)), for: .touchUpInside)
        
        homeManage.btnRequestActivation.addTarget(self, action: #selector(btnBranchesFromPopUpAction(sender:)), for: .touchUpInside)
        
        homeManage.contentViewHeightConstraint.constant = height + 76
        
        currentWindow?.addSubview(homeManage)
        
    }
    
    func alertForProduct() {
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        
        homeManage = HomeManageCompanyPopUp.init(frame: UIScreen.main.bounds)
        
        homeManage.viewCancelBg.backgroundColor = .primaryColor
        
        homeManage.imgClose.setImageColor(color: .white)
        
        //homeManage.lblDescription.text = requestMessageText ?? ""
        
        let combinedString  = NSMutableAttributedString()
        
        let str1 = NSMutableAttributedString(string: "\(masterDetails?.newProductMessage ?? "") \n\n", attributes: attributedPopUpTextOne)
        
        let str2 = NSMutableAttributedString(string: "\(masterDetails?.newProductNote ?? "")", attributes: attributedPopUpTextTwo)
        
        combinedString.append(str1)
        
        combinedString.append(str2)
        
        homeManage.lblDescription.attributedText = combinedString
        
        guard let labelText = homeManage.lblDescription.text else { return }
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        // homeManage.contentViewHeightConstraint.constant = height + 70
        
        homeManage.btnRequestTraining.colorIndex = 1
        
        homeManage.btnRequestTraining.addTarget(self, action: #selector(btnLaterAction(sender:)), for: .touchUpInside)
        
        homeManage.btnRequestActivation.addTarget(self, action: #selector(btnProductFromPopUpAction(sender:)), for: .touchUpInside)
        
        homeManage.btnRequestTraining.setTitle("Later".localized(), for: .normal)
        
        homeManage.btnRequestActivation.setTitle("Proceed".localized(), for: .normal)
        
        homeManage.contentViewHeightConstraint.constant = height + 76
        
        currentWindow?.addSubview(homeManage)
        
    }
    
    @objc func btnLaterAction(sender: UIButton){
        
        UserDefaults.standard.set(0, forKey: "isProduct")
        UserDefaults.standard.set(0, forKey: "isBranch")
        
        homeManage.removeFromSuperview()
        
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let size = CGSize(width: view.frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: UIFont.largeTitleFont]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }
    
    func alertSendProposalLink(selectedSellerCode:Int) {
          
          let alert = UIAlertController(title: nil, message: "Are you sure to send the porposal to company?".localized(), preferredStyle: .alert)
          
          alert.view.tintColor = UIColor.btnLinkColor
          
          alert.addAction(UIAlertAction(title: "No".localized(),
                                        style: UIAlertAction.Style.default,
                                        handler: {(_: UIAlertAction!) in
                                          
                                          self.dismiss(animated: true, completion: nil)
                                          
          }))
          
          alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
              
              self.sendPorposalLink(sellerCode: selectedSellerCode)
              
              self.dismiss(animated: true, completion: nil)
              
          }))
          
          self.present(alert, animated: true, completion: nil)
      }
    
    
    func alertSendUpdateLink(selectedSellerCode:Int) {
        
        let alert = UIAlertController(title: nil, message: "Are you sure to send the link to seller for updating profile?".localized(), preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.btnLinkColor
        
        alert.addAction(UIAlertAction(title: "No".localized(),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            self.sendAlertLink(sellerCode: selectedSellerCode)
            
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendPorposalLink(sellerCode:Int){
          
          let apiClient:APIClient = APIClient()
          
          weak var weakSelf = self
          startAnimatingAfterSubmit()
          apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/sendProposalToSeller?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
              
              weakSelf?.stopAnimating()
              
              var message:String = failedToConnectMessage.localized()
              if let serverMessage = jsonObj?.message(){
                  message = (serverMessage.count>0) ? serverMessage : message
              }
              
              weakSelf?.showErrorMessage(message: message)
              
          })
      }
    
    func sendAlertLink(sellerCode:Int){
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        startAnimatingAfterSubmit()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/sendSellerprofileUpdateLink?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            weakSelf?.stopAnimating()
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    func alertSendAgreementLink(selectedSellerCode:Int) {
        
        let alert = UIAlertController(title: nil, message: "Are you sure to send the agreemnet to Seller by Email and copy(cc) to Partner?".localized(), preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.btnLinkColor
        
        alert.addAction(UIAlertAction(title: "No".localized(),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            self.sendSellerAgreement(sellerCode: selectedSellerCode)
            
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendSellerAgreement(sellerCode:Int){
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        startAnimatingAfterSubmit()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/sendSellerAgreement?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            weakSelf?.stopAnimating()
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    func fetchSellerList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/sellerList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(SellerApiModel.self, from: data)
                                
                                if let apiResponse:[SellerModel] = jsonResponse.sellerList {
                                    
                                    weakSelf?.sellerList = apiResponse
                                    
                                }
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.sellerCount = apiResponse
                                    
                                    weakSelf?.lblSellerCount.text = ("\("You have ")\(weakSelf?.sellerCount ?? 0)\(" ")\("sellers".localized())")
                                }
                                if let apiResponse:[SellerStatusModel] = jsonResponse.statusList {
                                    
                                    weakSelf?.statusList = apiResponse
                                    
                                }
                                if let apiResponse:ColorModel = jsonResponse.colorCodes {
                                    
                                    weakSelf?.colorCodes = apiResponse
                                    
                                }
                                
                                if let apiResponse:[BillingCycleModel] = jsonResponse.billingCycle{
                                    
                                    weakSelf?.billingCycle = apiResponse
                                }
                                
                                if let apiResponse:[AttachmentTypeModel] = jsonResponse.attachmentTypes{
                                    
                                    weakSelf?.attachmentTypes = apiResponse
                                }
                                
                                if let apiResponse:[SellerCategoryModel] = jsonResponse.categoryList{
                                    
                                    weakSelf?.categoryList = apiResponse
                                }
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currency{
                                    
                                    weakSelf?.currency = apiResponse
                                }
                                
                                if let apiResponse:[TimePeriodModel] = jsonResponse.timePeriodList{
                                    
                                    weakSelf?.timePeriodList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[AccessUserTypesModel] = jsonResponse.accessUserTypes{
                                    
                                    weakSelf?.accessUserTypes = apiResponse
                                    
                                }
                                
                                if let apiResponse:[CompanyTypesModel] = jsonResponse.companyTypes{
                                    
                                    weakSelf?.companyType = apiResponse
                                    
                                }
                                
                                if let apiResponse:SellerProductBranchPopUpModel = jsonResponse.masterDetails{
                                    
                                    weakSelf?.masterDetails = apiResponse
                                    
                                }
                                
                                weakSelf?.stopAnimating()
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.stopAnimating()
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
}

extension SellerListVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = sellerList?.count ?? 0
        
        if count > 0 {
            
            tableViewList.alpha = 1
            
            return sellerList?.count ?? 0
            
        }
            
        else{
            
            tableViewList.alpha = 0
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:SellerListCell = tableView.dequeueReusableCell(withIdentifier: "SellerListCell", for: indexPath) as? SellerListCell
        {
            
            return getCellForSellerList(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
    }
    
    func getCellForSellerList(cell:SellerListCell, indexPath:IndexPath) -> SellerListCell {
        
        //        var shadowLayer: CAShapeLayer = CAShapeLayer()
        //
        cell.selectionStyle = .none
        
        if let model:SellerModel = sellerList?[indexPath.row]
        {
            
            if model.sellerCompanyName?.count ?? 0 > 0 {
                
                cell.lblCompanyName.text = model.sellerCompanyName ?? ""
                
            }
            else{
                
                cell.lblCompanyName.text = model.sellerName ?? ""
                
            }
            
            cell.lblSellerID.alpha = 0
            
            if model.sellerMemberId?.count ?? 0 > 0 {
                
                cell.lblSellerID.alpha = 1
                
                cell.lblSellerID.text = "\(model.sellerMemberId ?? "")"
                
            }
            
            let branchAttributedString = NSMutableAttributedString(string: "Branches".localized(),
                                                                   attributes: mainTitleAttributes)
            let productsAttributedString = NSMutableAttributedString(string: "Product/Service Groups".localized(),
                                                                     attributes: mainTitleAttributes)
            let referralLeadsAttributedString = NSMutableAttributedString(string: "Referral Leads".localized(),
                                                                          attributes: mainTitleAttributes)
            let usersAttributedString = NSMutableAttributedString(string: "Users".localized(),
                                                                  attributes: mainTitleAttributes)
            
            let customersAttributedString = NSMutableAttributedString(string: "Customers".localized(),
                                                                      attributes: mainTitleAttributes)
            
            let testimonialsAttributedString = NSMutableAttributedString(string: "Testimonials".localized(),
                                                                         attributes: mainTitleAttributes)
            
            let giftsAttributedString = NSMutableAttributedString(string: "Gifts".localized(),attributes: mainTitleAttributes)
            
            let jobsAttributedString = NSMutableAttributedString(string: "Jobs".localized(),attributes: mainTitleAttributes)
            
            let billingAttributedString = NSMutableAttributedString(string: "Billing".localized(),attributes: mainTitleAttributes)
            
             let eventsAttributedString = NSMutableAttributedString(string: "Events".localized(),attributes: mainTitleAttributes)
            
            cell.lblBranch.attributedText = branchAttributedString
            
            cell.lblProduct.attributedText = productsAttributedString
            
            cell.lblReferral.attributedText = referralLeadsAttributedString
            
            cell.lblUsers.attributedText = usersAttributedString
            
            cell.lblCustomers.attributedText = customersAttributedString
            
            cell.lblTestimonials.attributedText = testimonialsAttributedString
            
            cell.lblGifts.attributedText = giftsAttributedString
            
            cell.lblJobTitle.attributedText = jobsAttributedString
            
            cell.lblSellerAccountName.attributedText = billingAttributedString
            
            cell.lblEventTitle.attributedText = eventsAttributedString
            
            cell.lblBranchCount.attributedText = NSMutableAttributedString(string:("\(model.branchCount ?? 0)"),
                                                                           attributes: mainCountAttributes)
            cell.lblProductCount.attributedText = NSMutableAttributedString(string:("\(model.productCount ?? 0)"),
                                                                            attributes: mainCountAttributes)
            cell.lblUsersCount.attributedText = NSMutableAttributedString(string:("\(model.userCount ?? 0)"),
                                                                          attributes: mainCountAttributes)
            cell.lblReferralCount.attributedText = NSMutableAttributedString(string:("\(model.referralLeads ?? 0)"),
                                                                             attributes: mainCountAttributes)
            
            cell.lblCustomerCount.attributedText = NSMutableAttributedString(string:("\(model.customerCount ?? 0)"),attributes: mainCountAttributes)
            
            cell.lblTestimonialsCount.attributedText = NSMutableAttributedString(string:("\(model.testimonialCount ?? 0)"),attributes: mainCountAttributes)
            
            cell.lblGiftsCount.attributedText = NSMutableAttributedString(string:("\(model.giftCount ?? 0)"),attributes: mainCountAttributes)
            
            cell.lblJobCount.attributedText = NSMutableAttributedString(string:("\(model.jobsCount ?? 0)"),attributes: mainCountAttributes)
            
            
            cell.lblSellerAccountCount.attributedText = NSMutableAttributedString(string:("\(model.billingCount ?? 0)"),attributes: mainCountAttributes)
            
            cell.lblEventCount.attributedText = NSMutableAttributedString(string:("\(model.eventCount ?? 0)"),attributes: mainCountAttributes)
            
            cell.imgViewCompany.image = nil
            
            if model.logo?.count ?? 0 > 0 {
                cell.imgViewCompany.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.logo ?? "")")), completed: nil)
            }
            else{
                cell.imgViewCompany.image = UIImage.init(named: "Logo")
            }
            
            cell.btnBranches.tag = indexPath.row
            cell.btnProducts.tag = indexPath.row
            cell.btnReferral.tag = indexPath.row
            cell.btnSellerUsers.tag = indexPath.row
            cell.btnActions.tag = indexPath.row
            cell.btnTestimonials.tag = indexPath.row
            cell.btnCustomers.tag = indexPath.row
            cell.btnGifts.tag = indexPath.row
            cell.btnJobCount.tag = indexPath.row
            cell.btnSellerAccount.tag = indexPath.row
            cell.btnEventCount.tag = indexPath.row
            cell.btnJobManagerCount.tag = indexPath.row
            
            cell.btnActions.setTitleColor(UIColor.btnLinkColor, for: .normal)
            
            cell.btnActions.addTarget(self, action: #selector(btnActions(sender:)), for: .touchUpInside)
            
            cell.viewUsers.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.userCountColor ?? "")
            
            cell.viewUsersBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.userCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewBranchCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.branchCountColor ?? "")
            
            cell.viewBranchesBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.branchCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewProductCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.productCountColor ?? "")
            
            cell.viewProductsBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.productCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewReferralCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.referralLeadsColor ?? "")
            
            cell.viewReferralLeadsBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.referralLeadsColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewCustomerCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.customerCountColor ?? "")
            
            cell.viewCustomerBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.customerCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewTestimonialsCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.testimonialCountColor ?? "")
            
            cell.viewTestimonialsBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.testimonialCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewGiftsCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.giftCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewGiftsBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.giftCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewJobBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.jobsCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewJobCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.jobsCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewSellerAccountBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.billingCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewSellerAccountCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.billingCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
           cell.viewEventBackgroundd.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.eventCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
                      
            cell.viewEventCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.eventCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            
            
            cell.viewJobManagerCount.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.giftCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            cell.viewJobManagerBackground.borderColor = UIColor.colorWithHexString(hexString: colorCodes?.giftCountColor ?? "") ?? UIColor.getGradientColor(index: 0).first!
            
            
            
            cell.btnProducts.addTarget(self, action: #selector(btnProductsAction), for: .touchUpInside)
            
            cell.btnBranches.addTarget(self, action: #selector(btnBranchesAction), for: .touchUpInside)
            
            cell.btnSellerUsers.addTarget(self, action:  #selector(btnUserAction(sender:)), for: .touchUpInside)
            
            cell.btnReferral.addTarget(self, action: #selector(btnReferalAction(sender:)), for: .touchUpInside)
            
            cell.btnCustomers.addTarget(self, action: #selector(btnCustomerAction(sender:)), for: .touchUpInside)
            
            cell.btnTestimonials.addTarget(self, action: #selector(btnTestimonialAction(sender:)), for: .touchUpInside)
            
            cell.btnGifts.addTarget(self, action: #selector(btnGiftsAction(sender:)), for: .touchUpInside)
            
            cell.btnJobCount.addTarget(self, action: #selector(btnJobsAction(sender:)), for: .touchUpInside)
            
            cell.btnSellerAccount.addTarget(self, action: #selector(btnSellerAcountAction(sender:)), for: .touchUpInside)
            
             cell.btnEventCount.addTarget(self, action: #selector(btnEventAction(sender:)), for: .touchUpInside)
            
             cell.btnJobManagerCount.addTarget(self, action: #selector(btnJobManagerAction(sender:)), for: .touchUpInside)
            
            
        }
        
        return cell
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //        if indexPath.row == 0 {
        //
        //        return 170
        //
        //        }
        //        else{
        
        return UITableView.automaticDimension
        // }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:SellerModel = sellerList?[indexPath.row]
        {
            let storyBoard = UIStoryboard.init(name: SellerSB, bundle: nil)
            let newSellerPage:NewSellerVC = storyBoard.instantiateViewController(withIdentifier: "NewSellerVC") as! NewSellerVC
            newSellerPage.stausArray = statusList ?? []
            newSellerPage.sellerCode = model.sellerCode ?? 0
            newSellerPage.billingCycle = billingCycle ?? []
            newSellerPage.attachmentTypes = attachmentTypes ?? []
            newSellerPage.isDetail = true
            newSellerPage.delegate = self
            newSellerPage.categoryList = categoryList ?? []
            newSellerPage.currency = currency ?? []
            newSellerPage.timePeriodList = timePeriodList ?? []
            newSellerPage.isAdmin = model.isAdmin ?? 0
            newSellerPage.isPartnerForCompany = model.isPartnerForCompany ?? 0
            newSellerPage.companyType = companyType ?? []
            navigationController?.pushViewController(newSellerPage, animated: true)
            
        }
        
    }
    
}

class SellerListCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var btnBranches: UIButton!
    
    @IBOutlet weak var btnProducts: UIButton!
    
    @IBOutlet weak var btnReferral: UIButton!
    
    @IBOutlet weak var lblCompanyName: NKLabel!
    
    @IBOutlet weak var imgViewCompany: UIImageView!
    
    @IBOutlet weak var lblBranch: NKLabel!
    
    @IBOutlet weak var lblBranchCount: NKLabel!
    
    @IBOutlet weak var lblProduct: NKLabel!
    
    @IBOutlet weak var lblProductCount: NKLabel!
    
    @IBOutlet weak var lblReferral: NKLabel!
    
    @IBOutlet weak var lblReferralCount: NKLabel!
    
    @IBOutlet weak var lblUsers: NKLabel!
    
    @IBOutlet weak var lblUsersCount: NKLabel!
    
    @IBOutlet weak var viewBranchCount: UIView!
    
    @IBOutlet weak var viewProductCount: UIView!
    
    @IBOutlet weak var viewReferralCount: UIView!
    
    @IBOutlet weak var viewUsers: UIView!
    
    @IBOutlet weak var btnSellerUsers: UIButton!
    
    @IBOutlet weak var btnActions: UIButton!
    
    @IBOutlet weak var lblSellerID: NKLabel!
    
    @IBOutlet weak var lblCustomers: NKLabel!
    
    @IBOutlet weak var lblCustomerCount: NKLabel!
    
    @IBOutlet weak var btnCustomers: UIButton!
    
    @IBOutlet weak var viewCustomerCount: UIView!
    
    @IBOutlet weak var lblTestimonialsCount: NKLabel!
    
    @IBOutlet weak var viewTestimonialsCount: UIView!
    
    @IBOutlet weak var lblTestimonials: NKLabel!
    
    @IBOutlet weak var btnTestimonials: UIButton!
    
    
    @IBOutlet weak var viewBranchesBackground: RCView!
    
    @IBOutlet weak var viewProductsBackground: RCView!
    
    @IBOutlet weak var viewReferralLeadsBackground: RCView!
    
    @IBOutlet weak var viewUsersBackground: RCView!
    
    @IBOutlet weak var viewCustomerBackground: RCView!
    
    @IBOutlet weak var viewTestimonialsBackground: RCView!
    
    @IBOutlet weak var lblGiftsCount: NKLabel!
    
    @IBOutlet weak var lblGifts: NKLabel!
    
    @IBOutlet weak var viewGiftsCount: UIView!
    
    @IBOutlet weak var viewGiftsBackground: RCView!
    
    @IBOutlet weak var btnGifts: UIButton!
    
    @IBOutlet weak var viewJobBackground: RCView!
    
    @IBOutlet weak var viewJobCount: UIView!
    
    @IBOutlet weak var lblJobTitle: NKLabel!
    
    @IBOutlet weak var lblJobCount: NKLabel!
    
    @IBOutlet weak var btnJobCount: UIButton!
    
    @IBOutlet weak var viewSellerAccountBackground: RCView!
    
    @IBOutlet weak var viewSellerAccountCount: UIView!
    
    @IBOutlet weak var lblSellerAccountName: NKLabel!
    
    @IBOutlet weak var btnSellerAccount: UIButton!
    
    @IBOutlet weak var lblSellerAccountCount: NKLabel!
    
    @IBOutlet weak var viewEventBackgroundd:RCView!
    
    @IBOutlet weak var viewEventCount:UIView!
    
    @IBOutlet weak var lblEventCount:NKLabel!
    
    @IBOutlet weak var lblEventTitle:NKLabel!
    
    @IBOutlet weak var btnEventCount:UIButton!
    
    @IBOutlet weak var viewJobManagerBackground:RCView!
    
    @IBOutlet weak var viewJobManagerCount:UIView!
    
    @IBOutlet weak var lblJobManagerTitle:NKLabel!
    
    @IBOutlet weak var lblJobManagerCount:NKLabel!
    
    @IBOutlet weak var btnJobManagerCount:UIButton!
}

extension SellerListVC:NewSellerVCDelegate,UserListVCDelegate,ProductListVCDelegate,BranchListVCDelegate,TestimonialListVCDelegate,CustomerListVCDelegate,GiftViewControllerDelegate,JobListVCDelegate,EventsVCDelegate{
    
    func didAddedNewBranch() {
        
        let isUpdated:[String: Int] = ["update": 1]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
        
        fetchSellerList()
        
        
        //        if UserDefaults.standard.integer(forKey: "isProduct") == 1
        //        {
        //            alertForProduct()
        //
        //            UserDefaults.standard.set(0, forKey: "isProduct")
        //        }
        
    }
    
    
    func didAddedNewProduct() {
        
        let isUpdated:[String: Int] = ["update": 1]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
        
        
        fetchSellerList()
        
    }
    
    func didAddedNewUser() {
        
        let isUpdated:[String: Int] = ["update": 1]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
        
        fetchSellerList()
    }
    
    func didAddedSeller() {
        
        let isUpdated:[String: Int] = ["update": 1]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
        
        fetchSellerList()
        
        
        
        if UserDefaults.standard.integer(forKey: "isBranch") == 1
        {
            alertForBranch()
            
            // UserDefaults.standard.set(0, forKey: "isBranch")
        }
        
    }
    
    func didCalledTestimonial() {
        
        let isUpdated:[String: Int] = ["update": 1]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
        
        fetchSellerList()
        
    }
    
    func didCalledCustomer() {
        
        let isUpdated:[String: Int] = ["update": 1]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
        
        fetchSellerList()
        
    }
    
    func didCalledGift() {
        
        let isUpdated:[String: Int] = ["update": 1]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
        
        fetchSellerList()
        
    }
    
    func didCallJob() {
        
        let isUpdated:[String: Int] = ["update": 1]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
        
        fetchSellerList()
        
    }
    
    func didCallEvents() {
      
        let isUpdated:[String: Int] = ["update": 1]
               
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
               
        fetchSellerList()
        
    }
    
    
}
