//
//  EditSellerAgreementVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol EditSellerAgreementVCDelegate: class {
    func didAddedAttachment()
}

class EditSellerAgreementVC: SendOTPViewController {
    
    @IBOutlet weak var collectionViewAttachments: UICollectionView!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var imgAttachmentIcon: UIImageView!
    
    @IBOutlet weak var heightBtnSubmit: NSLayoutConstraint!
    
    var attachments:[AttachmentMainModel] = []
    
     var sellerCode:Int = 0
    
    var agreementId:Int = 0
    
    var isPartner:Bool = false
    
    weak var delegate:EditSellerAgreementVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isPartner {
            
            navigationItem.title = "Upload Signed Partner Agreement".localized()
        }
        else{
            
            navigationItem.title = "Upload Signed Seller Agreement".localized()
            
        }
        
         imgAttachmentIcon.setImageColor(color: .white)
        
    }
    
    
    @IBAction func btnAttachment(_ sender: UIButton) {
        
        isAttachment = true
        isDocument = true
        openImagePicker()
        
        weak var weakSelf = self

        didUploadHelpDeskImage = {status, urlString, imageName in
            
            if status {
                print(urlString, imageName)
                let arrayEachImage:AttachmentMainModel = AttachmentMainModel()
                arrayEachImage.cdnPath = urlString
                arrayEachImage.fileName = imageName
                weakSelf?.attachments.append(arrayEachImage)
                weakSelf?.collectionViewAttachments.reloadData()
                self.isAttachment = false
            }
            
        }
        
    }
    
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachments.count > sender.tag {
            attachments.remove(at: sender.tag)
            collectionViewAttachments.reloadData()
        }
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
        if isPartner {
            
            savePartnerAgreement()
        }
        else{
            
            saveSellerAgreement()
            
        }
        
    }
    

    func saveSellerAgreement(){
        
        startAnimatingAfterSubmit()
        
        var array : [Any] = []
                     
        for dict in attachments
         {
            var model:[String : Any] = [:]
            model["cdnPath"] = dict.cdnPath
            model["fileName"] = dict.fileName
            array.append(model)
         }
        
        let params:NSDictionary = [
            "agreementId":agreementId,
            "agreements":array
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/uploadSellerAgreements?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&sellerCode=\(sellerCode)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                weakSelf?.delegate?.didAddedAttachment()
                                
                                weakSelf?.showInfoMessage(message: message.localized())
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    // If Partner
    
    func savePartnerAgreement(){
        
        
        
        startAnimatingAfterSubmit()
        
        var array : [Any] = []
                            
               for dict in attachments
                {
                   var model:[String : Any] = [:]
                   model["cdnPath"] = dict.cdnPath
                   model["fileName"] = dict.fileName
                   array.append(model)
                }
        
        let params:NSDictionary = [
            "agreementId":agreementId,
            "agreements":array
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/uploadPartnerAgreements?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                  weakSelf?.delegate?.didAddedAttachment()
                                
                                weakSelf?.showInfoMessage(message: message.localized())
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }

   

}

extension EditSellerAgreementVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 100, height: 100)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if attachments.count > 0 {
            
            btnSubmit.alpha = 1
            heightBtnSubmit.constant = 44
            
            return attachments.count
        }
        
         heightBtnSubmit.constant = 0
         btnSubmit.alpha = 0
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
            
            return getCellForAttachment(cell: cell, indexPath: indexPath)
            
        }
        
        return UICollectionViewCell.init()
        
    }
    
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete .addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        if let fileName:AttachmentMainModel = attachments[indexPath.row]
        {
            if fileName.cdnPath?.count ?? 0 > 0
                {
                    if fileName.fileName?.count ?? 0 > 0 {
                      
                    if let url:URL = URL.init(string: "\(StorageUrl)\(fileName.cdnPath ?? "")"){
                      
                        let strFileType = fileName.fileName?.fileExtension()

                        if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType!.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                           {
                      
                        cell.imgView.sd_setImage(with: url, completed: nil)
                          
                      }
                       else{
                          cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                      }
                      
                    }
                }
             
                              
                
            }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionViewAttachments {
               if let fileName:AttachmentMainModel = attachments[indexPath.row]
                       {
                        if fileName.cdnPath?.count ?? 0 > 0
                               {
                                if fileName.fileName?.count ?? 0 > 0 {
                               
                                    let strFileType = fileName.fileName?.fileExtension()

                                    if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                                {
                                      
                           let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                                 let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                                    imageViewer.strImg = fileName.cdnPath ?? ""
                                 navigationController?.pushViewController(imageViewer, animated: true)
                                   
                               }
                               
                               else{
                                  
                                openWebView(urlString:fileName.cdnPath ?? "", title: fileName.fileName ?? "", subTitle: "")
                               }
                               
                               }
                            }
                    
                }
                    
              }
        
    }
    
    
    
}
