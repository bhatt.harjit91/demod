//
//  NewSellerVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 08/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

protocol NewSellerVCDelegate: class {
    func didAddedSeller()
}

class NewSellerVC: SendOTPViewController,CountryCodeSelectViewControllerDelegate,LocationPermissionVCNewDelegate,CurrencySelectorVCDelegate,RCTextFieldDelegate {
    
    @IBOutlet weak var lblAnnualMaintenanceText: NKLabel!
    @IBOutlet weak var imgLocation: UIImageView!
    
    
    @IBOutlet weak var btnLocation: UIButton!
    
    @IBOutlet weak var collectionViewAttachment: UICollectionView!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var lblCoolingperiodTitle: NKLabel!
    
    @IBOutlet weak var txtDays: RCTextField!
    
    @IBOutlet weak var txtSelectHours: RCTextField!
    
    var attachments:[AttachmentBannerTypeModel] = []
    
    var stausArray:[SellerStatusModel] = []
    
    var selectedIndex:Int = 1
    
    var selectedCompanyType:Int = 1
    
    var sellerList:SellerDetailModel?
    
    var sellerCode:Int = 0
    
    var sellerType:Int = 0
    
    var categoryCode:Int = 0
    
    var companyType:[CompanyTypesModel]?
    
    var currentLoc:CLLocationCoordinate2D?
    
    @IBOutlet weak var imageViewLogo: UIImageView!
    
    @IBOutlet weak var txtCompanyName: RCTextField!
    
    @IBOutlet weak var txtMobileISd: RCTextField!
    
    @IBOutlet weak var txtMobileNo: RCTextField!
    
    @IBOutlet weak var txtPhoneIsd: RCTextField!
    
    @IBOutlet weak var txtPhoneNo: RCTextField!
    
    @IBOutlet weak var imgViewLogo: UIImageView!
    
    @IBOutlet weak var btnLogo: NKButton!
    
    @IBOutlet weak var txtFirstName: RCTextField!
    
    @IBOutlet weak var txtLastName: RCTextField!
    
    @IBOutlet weak var txtJobTitle: RCTextField!
    
    @IBOutlet weak var txtEmailId: RCTextField!
    
    @IBOutlet weak var txtTax: RCTextField!
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
    @IBOutlet weak var imgAnnualFixedRadio: UIImageView!
    
    @IBOutlet weak var heightAnnualCharge: NSLayoutConstraint!
    
    @IBOutlet weak var viewAnnualFee: RCView!
    
    @IBOutlet weak var imgAnnualPercentageCharge: UIImageView!
    
    @IBOutlet weak var txtBillingCycle: RCTextField!
    
    @IBOutlet weak var imgViewAttachment: UIImageView!
    
    @IBOutlet weak var txtAnnualFeeCurrencyCode: RCTextField!
    
    @IBOutlet weak var txtAnnualFeeAmount: RCTextField!
    
    @IBOutlet weak var txtCompanyTagLine: RCTextField!
    
    @IBOutlet weak var txtViewCompanyProfile: NKTextView!
    
    @IBOutlet weak var txtCompanyWebsite: RCTextField!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var txtViewregisteredAddress: NKTextView!
    
    @IBOutlet weak var tfYoutubeLink: RCTextField!
    
    @IBOutlet weak var txtCategory: RCTextField!
    
    @IBOutlet weak var txtPercentageReferralFee: RCTextField!
    
    @IBOutlet weak var heightPercentageReferralFee: NSLayoutConstraint!
    
    @IBOutlet weak var viewPercentageReferralfee: RCView!
    
    @IBOutlet weak var heightViewAnnualFee: NSLayoutConstraint!
    
    @IBOutlet weak var lblWorkflowTypeTitle: NKLabel!
    
    @IBOutlet weak var imgRadioStandard: UIImageView!
    
    @IBOutlet weak var lblStandardTitle: NKLabel!
    
    @IBOutlet weak var imgSimplified: UIImageView!
    
    @IBOutlet weak var lblSimplifiedTitle: NKLabel!
    
    @IBOutlet weak var lblMoveToNoShowTitle: NKLabel!
    
    @IBOutlet weak var txtSimplifiedDuration: RCTextField!
    
    @IBOutlet weak var txtSlot: RCTextField!
    
    @IBOutlet weak var imgChecBoxEnableAppointment: UIImageView!
    
    @IBOutlet weak var viewMoveToNoShow: RCView!
    
    @IBOutlet weak var heightWorkFlowType: NSLayoutConstraint!
    
    @IBOutlet weak var btnFixedAmc: UIButton!
    
    @IBOutlet weak var btnFixedPercentage: UIButton!
    
    @IBOutlet weak var lblCompanyDes: NKLabel!
    
    @IBOutlet weak var btnCompanyDes: UIButton!
    
    @IBOutlet weak var collectionViewCompanyType: UICollectionView!
    
    var strLogo:String?
    
    var isFixedSelected:Int = 0
    
    var isPercentageSelected:Int = 0
    
    var billingCycle:[BillingCycleModel]?
    
    var attachmentTypes:[AttachmentTypeModel]?
    
    var pickerOptions:[String] = []
    
    var categoryOptions:[String] = []
    
    var selectedCountryCode : CountryCodeModel?
    
    var activeTextField = UITextField()
    
    var selectedCurrencyCode:CurrencyModel?
    
    var currency:[CurrencyModel]?
    
    var isDetail:Bool = false
    
    var billingType:Int = 0
    
    var btnName = UIButton()
    
    weak var delegate : NewSellerVCDelegate?
    
    var bannerType:Int = 0
    
    var amcType:Int = 0
    
    var amcCurrencyId:Int = 0
    
    var currencyDetailModel:CurrencyDetailModel?
    
    var categoryList:[SellerCategoryModel]?
    
    var timePeriodList:[TimePeriodModel]?
    
    var timePeriodOptions:[String] = []
    
    var workFlowType:Int = 0
    
    var enableAppointment:Int = 1
    
    var slotCode:Int = 0
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    var thumbnailLogo:String?
    
    var isLockAmc:Int = 0
    
    var amcMessage:String?
    
    var htmlDes:String = ""
    
    let countAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 8),
        .foregroundColor: UIColor.btnLinkColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
      var countryList:[CountryCodeModel] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
          countryList =  getCountryList()
        
          viewLoad()
        
    }
    
    func viewLoad(){
        
        btnName.alpha = 0
        btnSubmit.alpha = 0
        
        txtPercentageReferralFee.text = "20"
        
        if isAdmin == 1 || isPartnerForCompany == 1 {
            
            btnName.alpha = 1
            btnSubmit.alpha = 1
        }
        
        lblMoveToNoShowTitle.text = "Move to Noshow, if transaction is in pending beyond".localized()
        
        txtCategory.RCDelegate = self
        
        tfYoutubeLink.placeholder = "YouTube Link".localized()
        
        imgLocation.setImageColor(color: .white)
        
        txtCompanyName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Company Name".localized())
        //  txtFirstName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "First Name".localized())
        
        txtFirstName.placeholder = "First Name".localized()
        
        //   txtMobileNo.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Mobile Number".localized())
        
        txtMobileNo.placeholder = "Mobile Number".localized()
        
        txtAnnualFeeAmount.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        lblAnnualMaintenanceText.attributedText = addPlaceHolderWithText(placeholder: "Annual Maintenance Fee".localized())
        
        //   txtMobileISd.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Country".localized())
        
        txtMobileISd.placeholder = "Country".localized()
        
        //  txtPhoneIsd.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Country".localized())
        
        txtPhoneIsd.placeholder = "Country".localized()
        
        txtPercentageReferralFee.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        txtAnnualFeeCurrencyCode.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
        txtAnnualFeeAmount.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        btnName.setTitle("Submit -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(callSubmit), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        imageViewLogo.setImageColor(color: UIColor.white)
        
        imgViewAttachment.setImageColor(color: UIColor.white)
        
        navigationItem.title = "Seller".localized()
        
        btnLogo.addTarget(self, action: #selector(btnLogoAction), for: .touchUpInside)
        
        // viewAnnualFee.alpha = 0
        
        viewPercentageReferralfee.alpha = 0
        
        heightPercentageReferralFee.constant = 0
        
        // heightViewAnnualFee.constant = 0
        
        heightAnnualCharge.constant = 100
        
        txtMobileISd.delegate = self
        
        txtPhoneIsd.delegate = self
        
        btnSubmit.setTitle("Submit".localized(), for: .normal)
        
        if billingCycle?.count ?? 0 > 0 {
            
            for model in billingCycle ?? []{
                
                pickerOptions.append(model.title ?? "")
                
            }
            
            txtBillingCycle.pickerOptions = pickerOptions
            txtBillingCycle.dropDownPicker?.reloadAllComponents()
            
            txtBillingCycle.selectedOption = pickerOptions.first ?? ""
            
        }
        if categoryList?.count ?? 0 > 0 {
            
            for model in categoryList ?? []{
                
                categoryOptions.append(model.title ?? "")
                
            }
            
            txtCategory.pickerOptions = categoryOptions
            txtCategory.dropDownPicker?.reloadAllComponents()
            
            txtCategory.selectedOption = categoryOptions.first ?? ""
            
            txtSelectHours.text = "\(categoryList?.first?.defaultCPHours ?? 0)"
            txtDays.text = "\(categoryList?.first?.defaultCPDays ?? 0)"
            
        }
        
        if timePeriodList?.count ?? 0 > 0 {
            
            for model in timePeriodList ?? []{
                
                timePeriodOptions.append(model.title ?? "")
                
            }
            
            txtSlot.pickerOptions = timePeriodOptions
            txtSlot.dropDownPicker?.reloadAllComponents()
            
            txtSlot.selectedOption = timePeriodOptions.first ?? ""
        }
        
        if let currentCountry =  getLocalCountryCode(){
            
            selectedCountryCode = currentCountry
            
            txtMobileISd.text = currentCountry.dial_code
            txtPhoneIsd.text = currentCountry.dial_code
            
            let countryCode = NSLocale.locales1(countryName1: "\(currentCountry.name)")
            
            let countryCodeCA = countryCode
            let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA])
            let localeCA = NSLocale(localeIdentifier: localeIdCA)
            let currencySymbolCA = localeCA.object(forKey: NSLocale.Key.currencySymbol)
            let currencyCodeCA = localeCA.object(forKey: NSLocale.Key.currencyCode)
            
            print("\(currencyCodeCA ?? "")")
            
            // let currencyKey = currencyCodeCA ?? "" as? String
            
            
            let sym = currencyCodeCA ?? ""
            
            for model in currency ?? [] {
                
                if model.currencySymbol == sym as? String {
                    
                    selectedCurrencyCode = model
                    
                }
                
            }
            
        }
        
        if isDetail == true {
            
            fetchSellerDetail()
            
            btnSubmit.setTitle("Update".localized(), for: .normal)
            btnName.setTitle("Update -->".localized(), for: .normal)
            
        }
        
        txtCompanyName.delegate = self
        txtCompanyWebsite.delegate = self
        tfYoutubeLink.delegate = self
        
        txtMobileNo.delegate = self
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtPhoneNo.delegate = self
        txtAnnualFeeAmount.delegate = self
        txtAnnualFeeCurrencyCode.delegate = self
        txtSelectHours.delegate = self
        txtDays.delegate = self
        
        updateCheckBoxEnableAppoinment(type: enableAppointment)
        updateRadioWorkFlow(type: workFlowType)
        
        
        imgAnnualFixedRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled")
        
        imgAnnualPercentageCharge.image = UIImage.init(imageLiteralResourceName: "radio")
        
        isFixedSelected = 1
        
        amcType = 2
        
        isPercentageSelected = 0
        
        heightAnnualCharge.constant = 185
        
        heightViewAnnualFee.constant = 80
        
        viewAnnualFee.alpha = 1
        
        viewPercentageReferralfee.alpha = 0
        
        heightPercentageReferralFee.constant = 0
        
        
        txtAnnualFeeCurrencyCode.text = selectedCurrencyCode?.currencySymbol ?? ""
        
        txtAnnualFeeAmount.text = "\(selectedCurrencyCode?.defaultAMCAmount ?? 0.0)"
        
        let productDesTitle = NSMutableAttributedString(string: "Click here to add company description".localized(),
                                                        attributes: countAttributes)
        
        lblCompanyDes.attributedText = productDesTitle
        
        //  btnProductDes.setAttributedTitle(productDesTitle, for: .normal)
        
        btnCompanyDes.addTarget(self, action: #selector(openHtmlView), for: .touchUpInside)
        
        if billingCycle?.count ?? 0 > 0 {
            txtBillingCycle.selectedOption = pickerOptions.first ?? ""
        }
        
        collectionViewCompanyType.reloadData()
        
        txtSimplifiedDuration.delegate = self
        
    }
    
    @objc func openHtmlView() {
        
        let storyBoard = UIStoryboard.init(name: ProductSB, bundle: nil)
        
        if let htmlSelector:HtmlEditorVC = storyBoard.instantiateViewController(withIdentifier: "HtmlEditorVC") as? HtmlEditorVC {
            
            htmlSelector.htmlText = htmlDes
            htmlSelector.delegate = self
            
            navigationController?.pushViewController(htmlSelector, animated: true)
            
        }
    }
    
    @IBAction func btnLocation(_ sender: Any) {
        
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        //        locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        //        locationPage.lat = currentLoc?.latitude ?? 0.0
        //        locationPage.lng = currentLoc?.longitude ?? 0.0
        //        locationPage.address = addressCell.lblAddress.text ?? ""
        locationPage.modalPresentationStyle = .fullScreen
        navigationController?.present(locationPage, animated: true)
    }
    
    
    func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        
        currentLoc = coordinate
        
        if txtViewregisteredAddress.text.count <= 5 {
             updateLocationName()
        }
        
    }
    
    func didChangeOption(textField: RCTextField) {
        
        if categoryList?.count ?? 0 > 0 {
            
            for model in categoryList ?? []{
                
                if model.title == txtCategory.selectedOption {
                    
                    txtSelectHours.text = "\(model.defaultCPHours ?? 0)"
                    txtDays.text = "\(model.defaultCPDays ?? 0)"
                    
                }
                
            }
            
            
        }
        
    }
    
    
    
    func updateLocationName() {
        let geoCoder = GMSGeocoder()
        
        weak var weakSelf = self
        
        geoCoder.reverseGeocodeCoordinate(currentLoc ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), completionHandler: { response, error in
            
            if response != nil {
                let address = response?.results()?.first
                if address != nil {
                    
                    weakSelf?.txtViewregisteredAddress.text = address?.lines?.joined(separator: ",")
                    
                    if !(weakSelf?.txtViewregisteredAddress.text.isEmpty ?? false)
                    {
                        weakSelf?.txtViewregisteredAddress.placeholder = ""
                    }
                    
                }
            }
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if htmlDes.count > 0 {
            
            let productDesTitle = NSMutableAttributedString(string: "Click here to update company description".localized(),
                                                            attributes: countAttributes)
            
            lblCompanyDes.attributedText = productDesTitle
            
        }
        
    }
    
    func updateUI(){
        
        if sellerList != nil
        {
            txtCompanyName.text = sellerList?.sellerCompanyName ?? ""
            txtTax.text = sellerList?.taxNumber ?? ""
            txtEmailId.text = sellerList?.emailId ?? ""
            txtPhoneNo.text = sellerList?.phoneNumber ?? ""
            txtPhoneIsd.text = sellerList?.phoneIsd ?? ""
            txtFirstName.text = sellerList?.firstName ?? ""
            txtLastName.text = sellerList?.lastName ?? ""
            txtMobileNo.text = sellerList?.mobileNumber ?? ""
            txtMobileISd.text = sellerList?.mobileIsd ?? ""
            strLogo = sellerList?.logo ?? ""
            thumbnailLogo = sellerList?.companyThumbnailLogo ?? ""
            attachments = sellerList?.attachmentList ?? []
            txtCompanyTagLine.text = sellerList?.tagLine ?? ""
            // txtViewCompanyProfile.text = sellerList?.sellerProfile ?? ""
            
            for model in countryList {
                
                if model.dial_code == sellerList?.mobileIsd ?? "" {
                    
                    selectedCountryCode = model
                    
                }
                
            }
            let des = sellerList?.sellerProfile ?? ""
            htmlDes = des.removeWhitespaceSpecial()
            
            txtJobTitle.text = sellerList?.jobTitle ?? ""
            txtCompanyWebsite.text = sellerList?.website ?? ""
            txtViewNotes.text = sellerList?.notes ?? ""
            
            if htmlDes.count > 0 {
                
                let productDesTitle = NSMutableAttributedString(string: "Click here to update company description".localized(),
                                                                attributes: countAttributes)
                
                lblCompanyDes.attributedText = productDesTitle
                
            }
            
            if sellerList?.amcCurrencySymbol?.count ?? 0 > 0 {
                txtAnnualFeeCurrencyCode.text = sellerList?.amcCurrencySymbol ?? ""
            }
            
            txtViewregisteredAddress.text = sellerList?.registeredAddress ?? ""
            
            if !(txtViewregisteredAddress.text.isEmpty )
            {
                txtViewregisteredAddress.placeholder = ""
            }
            
            tfYoutubeLink.text = sellerList?.youtubeLink ?? ""
            
            amcType = sellerList?.amcType ?? 0
            
            selectedIndex = sellerList?.status ?? 0
            
            billingType = sellerList?.billingType ?? 0
            
            categoryCode = sellerList?.categoryCode ?? 0
            
            amcCurrencyId = sellerList?.amcCurrencyId ?? 0
            
            slotCode = sellerList?.moveTransAfterTPType ?? 0
            
            workFlowType = sellerList?.workFlowType ?? 0
            
            updateRadioWorkFlow(type: workFlowType)
            
            txtSimplifiedDuration.text = "\(sellerList?.moveTransAfterTP ?? 0)"
            
            enableAppointment = sellerList?.enableAppointmentTimeInRN ?? 0
            
            isLockAmc = sellerList?.lockAmcField ?? 0
            
            amcMessage = sellerList?.amcFieldAlertMessage ?? ""
            
            if billingType > 0 {
                
                //  txtBillingCycle.selectedOption = pickerOptions[billingType-1]
                
                for model in billingCycle ?? [] {
                    
                    if model.billingType == billingType {
                        
                        txtBillingCycle.selectedOption = model.title ?? ""
                        
                    }
                    
                }
                
            }
            
            if categoryCode > 0 {
                
                for model in categoryList ?? [] {
                    
                    if model.categoryCode == categoryCode {
                        
                        txtCategory.selectedOption = model.title ?? ""
                        
                    }
                    
                }
            }
            if slotCode > 0 {
                
                for model in timePeriodList ?? [] {
                    
                    if model.timeCode == slotCode {
                        
                        txtSlot.selectedOption = model.title ?? ""
                        
                    }
                    
                }
            }
            
            if amcType == 2 {
                
                imgAnnualFixedRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled")
                
                imgAnnualPercentageCharge.image = UIImage.init(imageLiteralResourceName: "radio")
                
                isFixedSelected = 1
                
                isPercentageSelected = 0
                
                heightAnnualCharge.constant = 185
                
                heightViewAnnualFee.constant = 80
                
                viewAnnualFee.alpha = 1
                
                viewPercentageReferralfee.alpha = 0
                
                heightPercentageReferralFee.constant = 0
                
                txtAnnualFeeAmount.text = "\(sellerList?.amcValue ?? 0)"
                
                txtAnnualFeeCurrencyCode.text = sellerList?.amcCurrencySymbol ?? ""
                
            }
            
            if amcType == 1 {
                
                imgAnnualFixedRadio.image = UIImage.init(imageLiteralResourceName: "radio")
                
                imgAnnualPercentageCharge.image = UIImage.init(imageLiteralResourceName: "radioFilled")
                
                isPercentageSelected = 1
                
                heightAnnualCharge.constant = 185
                
                isFixedSelected = 0
                
                viewAnnualFee.alpha = 0
                
                viewPercentageReferralfee.alpha = 1
                
                heightPercentageReferralFee.constant = 80
                
                txtPercentageReferralFee.text = "\(sellerList?.amcValue ?? 0)"
                
                heightViewAnnualFee.constant = 0
                
            }
            
            txtSelectHours.text = "\(sellerList?.cpHours ?? 0)"
                       
            txtDays.text = "\(sellerList?.cpDays ?? 0)"
            
            
            if sellerList?.sellerProfile?.count ?? 0 > 0 {
                
                txtViewCompanyProfile.placeholder = ""
            }
            
            if sellerList?.notes?.count ?? 0 > 0 {
                
                txtViewNotes.placeholder = ""
            }
            
            if strLogo?.count ?? 0 > 0 {
                self.imgViewLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(strLogo ?? "")")), completed: nil)
            }
            
            
            
            collectionViewStatus.reloadData()
            collectionViewAttachment.reloadData()
            
            
            if isDetail == true && isLockAmc == 2 {
                
                btnFixedAmc.isUserInteractionEnabled = false
                btnFixedPercentage.isUserInteractionEnabled = false
                
            }
            
            selectedCompanyType = sellerList?.companyType ?? 0
            
            collectionViewCompanyType.reloadData()
        }
    }
    
    
    
    
    func attachImage(){
        
        isAttachment = true
        isDocument = true
        openImagePicker()
        
      startAnimatingAfterSubmit()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            weakSelf?.stopAnimating()
            if status {
                let arrayEachImage:AttachmentBannerTypeModel = AttachmentBannerTypeModel()
                arrayEachImage.cdnPath = urlString
                arrayEachImage.fileName = imageName
                arrayEachImage.attachmentType = self.bannerType
                weakSelf?.attachments.append(arrayEachImage)
                weakSelf?.collectionViewAttachment.reloadData()
                weakSelf?.stopAnimating()
                self.isAttachment = false
            }
            
           
        }
    }
    
    
    @IBAction func btnAttachmentAction(_ sender: Any) {
        
         weak var weakSelf = self
        
        if attachmentTypes?.count ?? 0 > 0 {
            
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alert.view.tintColor = UIColor.btnLinkColor
            
            for model in attachmentTypes ?? [] {
                
                print(model.attachmentType ?? 0)
                
                alert.addAction(UIAlertAction(title: model.title ?? "", style: .default , handler:alertSheetAction))
                
            }
            
            alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{ (UIAlertAction)in
                
                 weakSelf?.stopAnimating()
                
            }))
            self.present(alert, animated: true, completion:nil)
            
        }
        
    }
    
    
    func alertSheetAction(action: UIAlertAction) {
        
        for model in attachmentTypes ?? [] {
            
            if action.title == model.title {
                
                bannerType = model.attachmentType ?? 0
                
            }
            
        }
        
        self.attachImage()
    }
    
    
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachments.count > sender.tag {
            attachments.remove(at: sender.tag)
            collectionViewAttachment.reloadData()
        }
    }
    
    
    @objc func btnLogoAction() {
        
        openImagePicker()
        
        weak var weakSelf = self
        
        isLogo = true
        
        startAnimating()
        
        didUploadLogoImage = { status, urlString in
            weakSelf?.thumbnailLogo = urlString
            
            self.isLogo = false
        }
        
        didUploadImage = { status, urlString in
            
            weakSelf?.stopAnimating()
            
            weakSelf?.strLogo = urlString
            
            if weakSelf?.strLogo?.count ?? 0 > 0 {
                self.imgViewLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(weakSelf?.strLogo ?? "")")), completed: nil)
            }
        }
    }
    
    @IBAction func segmentSellerTypeAction(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            sellerType = 0
            break;
        case 1:
            sellerType = 1
            break;
        default:
            sellerType = 0
        }
    }
    
    
    @IBAction func btnFixed(_ sender: Any) {
        
        if isDetail == true && isLockAmc == 1 {
            
            
            let alert = UIAlertController(title:nil, message: amcMessage ?? "", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .default, handler: { _ in
                
                self.dismiss(animated: true, completion: nil)
                
                return
                
            }))
            
            alert.addAction(UIAlertAction(title: "Proceed".localized(), style: .default, handler: { _ in
                
                self.isLockAmc = 0
                
                if self.isFixedSelected == 0
                {
                    
                    
                    self.imgAnnualFixedRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled")
                    
                    self.imgAnnualPercentageCharge.image = UIImage.init(imageLiteralResourceName: "radio")
                    
                    self.isFixedSelected = 1
                    
                    self.isPercentageSelected = 0
                    
                    self.heightAnnualCharge.constant = 185
                    
                    self.heightViewAnnualFee.constant = 80
                    
                    self.viewAnnualFee.alpha = 1
                    
                    self.viewPercentageReferralfee.alpha = 0
                    
                    self.heightPercentageReferralFee.constant = 0
                }
                
                self.amcType = 2
                
                self.dismiss(animated: true, completion: nil)
                
            }))
            
            present(alert, animated: true, completion: nil)
            
        }
            
        else {
            
            if isFixedSelected == 0
            {
                // getCurrencyDetail()
                
                imgAnnualFixedRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled")
                
                imgAnnualPercentageCharge.image = UIImage.init(imageLiteralResourceName: "radio")
                
                isFixedSelected = 1
                
                isPercentageSelected = 0
                
                heightAnnualCharge.constant = 185
                
                heightViewAnnualFee.constant = 80
                
                viewAnnualFee.alpha = 1
                
                viewPercentageReferralfee.alpha = 0
                
                heightPercentageReferralFee.constant = 0
            }
            
            amcType = 2
            
        }
        
    }
    
    
    @IBAction func btnPercentage(_ sender: Any) {
        
        if isDetail == true && isLockAmc == 1 {
            
            
            let alert = UIAlertController(title:nil, message: amcMessage ?? "", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .default, handler: { _ in
                
                self.dismiss(animated: true, completion: nil)
                
                return
                
            }))
            
            alert.addAction(UIAlertAction(title: "Proceed".localized(), style: .default, handler: { _ in
                
                self.isLockAmc = 0
                
                if self.isPercentageSelected == 0
                {
                    self.imgAnnualFixedRadio.image = UIImage.init(imageLiteralResourceName: "radio")
                    
                    self.imgAnnualPercentageCharge.image = UIImage.init(imageLiteralResourceName: "radioFilled")
                    
                    self.isPercentageSelected = 1
                    
                    self.isFixedSelected = 0
                    
                    self.viewAnnualFee.alpha = 0
                    
                    self.heightViewAnnualFee.constant = 0
                    
                    self.heightAnnualCharge.constant = 185
                    
                    self.viewPercentageReferralfee.alpha = 1
                    
                    self.heightPercentageReferralFee.constant = 80
                }
                
                self.amcType = 1
                
                self.dismiss(animated: true, completion: nil)
                
            }))
            
            present(alert, animated: true, completion: nil)
            
        }
            
        else {
            
            if isPercentageSelected == 0
            {
                imgAnnualFixedRadio.image = UIImage.init(imageLiteralResourceName: "radio")
                
                imgAnnualPercentageCharge.image = UIImage.init(imageLiteralResourceName: "radioFilled")
                
                isPercentageSelected = 1
                
                isFixedSelected = 0
                
                viewAnnualFee.alpha = 0
                
                heightViewAnnualFee.constant = 0
                
                heightAnnualCharge.constant = 185
                
                viewPercentageReferralfee.alpha = 1
                
                heightPercentageReferralFee.constant = 80
            }
            
            amcType = 1
            
        }
        
    }
    
    
    func validateUrl (stringURL : String) -> Bool {
        
        let urlRegEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[urlRegEx])
        return predicate.evaluate(with: stringURL)
    }
    
    func updateCheckBoxEnableAppoinment(type:Int){
        
        if type == 0 {
            
            imgChecBoxEnableAppointment.image = UIImage.init(imageLiteralResourceName: "checkBox")
            
        }
        else{
            imgChecBoxEnableAppointment.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
        }
        
    }
    
    func updateRadioWorkFlow(type:Int){
        
//        if type == 0 {
//            
//            imgRadioStandard.image = UIImage.init(imageLiteralResourceName: "radioFilled")
//            imgSimplified.image = UIImage.init(imageLiteralResourceName: "radio")
//            heightWorkFlowType.constant = 100
//            viewMoveToNoShow.alpha = 0
//            
//        }
//        else{
//            imgSimplified.image = UIImage.init(imageLiteralResourceName: "radioFilled")
//            imgRadioStandard.image = UIImage.init(imageLiteralResourceName: "radio")
//            
//            heightWorkFlowType.constant = 235
//            viewMoveToNoShow.alpha = 1
//            
//        }
        
    }
    
    @IBAction func btnStandardAction(_ sender: UIButton) {
        
        workFlowType = 0
        
        updateRadioWorkFlow(type: workFlowType)
        
        
    }
    
    @IBAction func btnSimplifiedAction(_ sender: UIButton) {
        
        workFlowType = 1
        
        updateRadioWorkFlow(type: workFlowType)
        
    }
    
    @IBAction func btnEnableAppointmentTime(_ sender: UIButton) {
        
        
        if enableAppointment == 1 {
            
            updateCheckBoxEnableAppoinment(type: enableAppointment)
            
            enableAppointment = 0
        }
        else{
            
            updateCheckBoxEnableAppoinment(type: enableAppointment)
            
            enableAppointment = 1
            
        }
        
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        callSubmit()
    }
    
    @objc func callSubmit(){
        
        
        if txtCompanyName.text?.count ?? 0 == 0 {
            
            txtCompanyName.showError(message: "Required".localized())
            
            return
        }
        
        //                if txtFirstName.text?.count ?? 0 == 0 {
        //
        //                    txtFirstName.showError(message: "Required".localized())
        //
        //                     return
        //                }
        
        
        //                if txtMobileNo.text?.count ?? 0 == 0 {
        //
        //                 txtMobileNo.showError(message: "Required".localized())
        //
        //                 return
        //                }
        
        //               if txtMobileNo.text?.count ?? 0 < 5 {
        //
        //                  txtMobileNo.showError(message: "Should be at least 5 characters".localized())
        //
        //                 return
        //                }
        
        if txtMobileNo.text?.count ?? 0 > 5 && txtMobileISd.text?.count == 0 {
            
            txtMobileISd.showError(message: "Required".localized())
            
            return
        }
        
        if txtPhoneNo.text?.count ?? 0 > 0 && txtPhoneNo.text?.count ?? 0 < 5 {
            
            txtPhoneNo.showError(message: "Should be at least 5 characters".localized())
            
            return
        }
        if txtPhoneNo.text?.count ?? 0 > 5 && txtPhoneIsd.text?.count ?? 0 == 0 {
            
            txtPhoneIsd.showError(message: "Required".localized())
            
            return
        }
        
        if workFlowType == 1 {
            
            if txtSimplifiedDuration.text?.count ?? 0 == 0 {
                
                txtSimplifiedDuration.showError(message: "Required".localized())
                
                return
            }
            
            let intHours = Int(txtSimplifiedDuration.text ?? "0")
            
            if intHours ?? 0 <= 0 {
                
                txtSimplifiedDuration.showError(message: "Required".localized())
                
                return
            }
            
        }
        //
        if amcType == 1 {
            
            let per = Double(txtPercentageReferralFee.text ?? "") ?? 0.0
            
            if (per <= 0.0) {
                
                txtPercentageReferralFee.showError(message: "Required".localized())
                
                return
            }
            
        }
        if amcType == 2 {
            
            if txtAnnualFeeCurrencyCode.text?.count ?? 0 > 0 && txtAnnualFeeAmount.text?.count ?? 0 == 0 {
                
                txtAnnualFeeAmount.showError(message: "Required".localized())
                
                return
            }
            
            if txtAnnualFeeAmount.text?.count ?? 0 > 0 && txtAnnualFeeCurrencyCode.text?.count ?? 0 == 0 {
                
                txtAnnualFeeCurrencyCode.showError(message: "Required".localized())
                
                return
            }
            
            let min = Double(txtAnnualFeeAmount.text ?? "") ?? 0.0
            
            if !(selectedCurrencyCode?.minAMCAmount ?? 0.0 <= min) {
                
                showErrorMessage(message: "\("Minimum annual maintenance fee should be greater than ".localized())\(selectedCurrencyCode?.minAMCAmount ?? 0.0)")
                return
            }
            
            
            
        }
        submitSellerDetails()
        
    }
    
    func openCountrySelector() {
        let storyBoard = UIStoryboard.init(name: CountryCodeSB, bundle: nil)
        let countrySelector:CountryCodeSelectViewController = storyBoard.instantiateViewController(withIdentifier: "CountryCodeSelectViewController") as! CountryCodeSelectViewController
        countrySelector.selectedCountryCode = selectedCountryCode
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
    }
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
        
        txtAnnualFeeCurrencyCode.text = selectedCurrencyCode?.currencySymbol ?? ""
        txtAnnualFeeAmount.text = "\(selectedCurrencyCode?.defaultAMCAmount ?? 0)"
        
    }
    
    func didFinishSelectingCountryCode(selectedCountry: CountryCodeModel) {
        selectedCountryCode = selectedCountry
        
        if txtMobileISd == activeTextField {
            txtMobileISd.text = selectedCountry.dial_code
        }
        if txtPhoneIsd == activeTextField {
            txtPhoneIsd.text = selectedCountry.dial_code
        }
        
        //   getCurrencyDetail()
        
    }
    
    
    func fetchSellerDetail() {
        
        let apiClient:APIClient = APIClient()
        
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/sellerCompanyDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(SellerDetailApiModel.self, from: data)
                                
                                if let apiResponse:SellerDetailModel = jsonResponse.sellerCompanyDetails {
                                    
                                    weakSelf?.sellerList = apiResponse
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                        
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func getCurrencyDetail() {
        
        let apiClient:APIClient = APIClient()
        
        
        weak var weakSelf = self
        
        startAnimatingAfterSubmit()
        
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getCurrencyDetail?&lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&countryId=\(selectedCountryCode?.dial_code ?? "")"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(CurrencyDetailApiModel.self, from: data)
                                
                                if let apiResponse:CurrencyDetailModel = jsonResponse.currencyDetail {
                                    
                                    weakSelf?.currencyDetailModel = apiResponse
                                    
                                }
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                        
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func submitSellerDetails(){
        
        var mobileISD = ""
        var phoneISD = ""
        
        if selectedCurrencyCode != nil {
            amcCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        }
        
        if billingCycle?.count ?? 0 > 0 {
            
            for model in billingCycle ?? []{
                
                if model.title == txtBillingCycle.selectedOption {
                    
                    billingType = model.billingType ?? 0
                    
                }
                
            }
            
            
        }
        
        if categoryList?.count ?? 0 > 0 {
            
            for model in categoryList ?? []{
                
                if model.title == txtCategory.selectedOption {
                    
                    categoryCode = model.categoryCode ?? 0
                    
                }
                
            }
            
            
        }
        
        if timePeriodList?.count ?? 0 > 0 {
            
            for model in timePeriodList ?? []{
                
                if model.title == txtSlot.selectedOption {
                    
                    slotCode = model.timeCode ?? 0
                    
                }
                
            }
            
        }
        
        var amcValue = 0.0
        
        if amcType == 2 {
            
            if let cost = Double(txtAnnualFeeAmount.text ?? "0.0") {
                amcValue = cost
            }
        }
        if amcType == 1 {
            
            if let cost = Double(txtPercentageReferralFee.text ?? "0.0") {
                amcValue = cost
            }
        }
        
        var array : [Any] = []
        for dict in attachments
        {
            var model:[String : Any] = [:]
            model["cdnPath"] = dict.cdnPath
            model["attachmentType"] = dict.attachmentType
            model["fileName"] = dict.fileName
            array.append(model)
        }
        
        var moveTransAfterTP:Int = 0
        
        if let cost = Int(txtSimplifiedDuration.text ?? "0") {
            moveTransAfterTP = cost
        }
        
        if let cost = txtMobileISd.text {
            mobileISD = cost
        }
        
        if let cost = txtPhoneIsd.text {
            phoneISD = cost
        }
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["sellerCode": sellerCode,
                                   "sellerType": 1,
                                   "lngId":Localize.currentLanguageID,
                                   "sellerCompanyName":txtCompanyName.text ?? "",
                                   "logo":strLogo ?? "",
                                   "companyThumbnailLogo":thumbnailLogo ?? "",
                                   "firstName":txtFirstName.text ?? "",
                                   "lastName":txtLastName.text ?? "",
                                   "jobTitle": txtJobTitle.text ?? "",
                                   "mobileIsd": mobileISD,
                                   "mobileNumber": txtMobileNo.text ?? "",
                                   "phoneIsd": phoneISD,
                                   "phoneNumber": txtPhoneNo.text ?? "",
                                   "notes": txtViewNotes.text ?? "",
                                   "taxNumber": txtTax.text ?? "",
                                   "status": selectedIndex,
                                   "attachmentList":array,
                                   "billingType":billingType,
                                   "amcType" : amcType,
                                   "amcCurrencyId" : amcCurrencyId,
                                   "amcValue" :amcValue,
                                   "tagLine" : txtCompanyTagLine.text ?? "",
                                   "sellerProfile" : htmlDes,
                                   "website":txtCompanyWebsite.text ?? "",
                                   "emailId":txtEmailId.text ?? "",
                                   "registeredAddress" : txtViewregisteredAddress.text ?? "",
                                   "categoryCode":categoryCode,
                                   "youtubeLink" : tfYoutubeLink.text ?? "",
                                   "workFlowType":workFlowType,
                                   "moveTransAfterTP":moveTransAfterTP,
                                   "moveTransAfterTPType":slotCode,
                                   "enableAppointmentTimeInRN":enableAppointment,
                                   "cpDays":txtDays.text ?? "0",
                                   "cpHours":txtSelectHours.text ?? "0",
                                   "companyType":selectedCompanyType
            
        ]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerCompany?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                //                            let apiResponse = try decoder.decode(CategoryListModel.self, from: data)
                                //                            weakSelf?.categoryNewList = apiResponse.categoriesList ?? []
                                //                            weakSelf?.searchCategoryList = apiResponse.categoriesList ?? []
                                //
                                //                            DispatchQueue.main.async {
                                //
                                //                                weakSelf?.collectionViewCategory.reloadData()
                                //                            }
                                
                                weakSelf?.stopAnimating()
                                
                                if weakSelf?.isDetail == false {
                                    
                                    UserDefaults.standard.set(1, forKey: "isProduct")
                                    UserDefaults.standard.set(1, forKey: "isBranch")
                                    
                                }else{
                                    UserDefaults.standard.set(0, forKey: "isProduct")
                                    UserDefaults.standard.set(0, forKey: "isBranch")
                                }
                                
                                weakSelf?.delegate?.didAddedSeller()
                                
                                let isUpdated:[String: Int] = ["update": 1]
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callSearch"), object: nil, userInfo: isUpdated)
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
            //Show error
            weakSelf?.stopAnimating()
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
}

extension NewSellerVC:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField ==  txtPhoneIsd || textField == txtMobileISd {
            
            self.activeTextField = textField
            
            openCountrySelector()
            
            return false;
        }
        
        if textField == txtAnnualFeeCurrencyCode {
            
            self.activeTextField = textField
            
            if isDetail == true && isLockAmc == 1 {
                
                
                let alert = UIAlertController(title:nil, message: amcMessage ?? "", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { _ in
                    
                    self.dismiss(animated: true, completion: nil)
                    
                    return
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Proceed", style: .default, handler: { _ in
                    
                    self.isLockAmc = 0
                    
                    self.dismiss(animated: true, completion: nil)
                }))
                
                present(alert, animated: true, completion: nil)
                
            }
            
            if isDetail == true && isLockAmc == 2 {
                
                return false
                
            }
            
            openCurrencySelector()
            
            return false
        }
        
        if textField == txtAnnualFeeAmount {
            
            if isDetail == true && isLockAmc == 1 {
                
                
                let alert = UIAlertController(title:nil, message: amcMessage ?? "", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .default, handler: { _ in
                    
                    self.dismiss(animated: true, completion: nil)
                    
                    return
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Proceed".localized(), style: .default, handler: { _ in
                    
                    self.isLockAmc = 0
                    
                    self.dismiss(animated: true, completion: nil)
                    
                }))
                
                present(alert, animated: true, completion: nil)
                
            }
            
            if isDetail == true && isLockAmc == 2 {
                
                return false
                
            }
            
        }
        
        if textField ==  txtPercentageReferralFee {
            
            if isDetail == true && isLockAmc == 1 {
                
                
                let alert = UIAlertController(title:nil, message: amcMessage ?? "", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .default, handler: { _ in
                    
                    self.dismiss(animated: true, completion: nil)
                    
                    return
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Proceed".localized(), style: .default, handler: { _ in
                    
                    self.isLockAmc = 0
                    
                    self.dismiss(animated: true, completion: nil)
                    
                }))
                
                present(alert, animated: true, completion: nil)
                
            }
            
            if isDetail == true && isLockAmc == 2 {
                
                return false
                
            }
            
        }
        
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField ==  txtCompanyName {
            
            txtCompanyName.showError(message: "")
            
        }
        
        if textField ==  txtCompanyWebsite {
            
            txtCompanyWebsite.showError(message: "")
            
        }
        if textField ==  tfYoutubeLink {
            
            tfYoutubeLink.showError(message: "")
            
        }
        
        if textField ==  txtFirstName {
            
            txtFirstName.showError(message: "")
            
        }
        
        if textField ==  txtLastName {
            
            txtLastName.showError(message: "")
            
        }
        
        if textField ==  txtSimplifiedDuration {
            
            txtSimplifiedDuration.showError(message: "")
            
        }
        
        if textField ==  txtPhoneNo {
            
            txtPhoneNo.showError(message: "")
            
            let maxLength = 14
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        if textField ==  txtMobileNo {
            
            txtMobileNo.showError(message: "")
            
            let maxLength = 14
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        if textField ==  txtAnnualFeeAmount {
                   
          txtAnnualFeeAmount.showError(message: "")
                   
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
                   
            return newString.length <= maxLength
                   
        }
        
        if textField ==  txtMobileISd {
            
            txtMobileISd.showError(message: "")
            
        }
        
        if textField ==  txtPhoneIsd {
            
            txtPhoneIsd.showError(message: "")
            
        }
        
        
        if textField == txtAnnualFeeAmount {
            
            txtAnnualFeeAmount.showError(message: "")
            
        }
        
        if textField ==  txtAnnualFeeCurrencyCode {
            
            txtAnnualFeeCurrencyCode.showError(message: "")
            
        }
        
        if textField ==  txtPercentageReferralFee {
            
            txtPercentageReferralFee.showError(message: "")
            
        }
        
        if textField ==  txtDays {
            
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        if textField ==  txtSimplifiedDuration {
            
            txtSimplifiedDuration.showError(message: "")
            
            let maxLength = 5
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        if textField ==  txtSelectHours {
            
            if let text = textField.text {
                
                let newStr = (text as NSString)
                    .replacingCharacters(in: range, with: string) as NSString
                if newStr == "" {
                    return true
                }
                let intvalue = Int(newStr as String)
                return (intvalue ?? 0 >= 0 && intvalue ?? 0 <= 23 && newStr.length <= 2)
            }
        }
        return true
    }
}

extension NewSellerVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            return CGSize.init(width: collectionViewStatus.frame.width / 4, height: 40.0)
            
        }
        
        if collectionView == collectionViewCompanyType
            
        {
            return CGSize.init(width: collectionViewCompanyType.frame.width / 3, height: 40.0)
            
        }
        
        if collectionView == collectionViewAttachment
            
        {
            return CGSize.init(width: 100, height: 100)
            
        }
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewAttachment
        {
            if attachments.count > 0 {
                
                return attachments.count
            }
            
        }
        
        if collectionView == collectionViewStatus
            
        {
            if stausArray.count > 0 {
                
                return stausArray.count
            }
            
        }
        
        if collectionView == collectionViewCompanyType
        {
            if companyType?.count ?? 0 > 0 {
                
                return companyType?.count ?? 0
                
            }
            
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewAttachment
            
        {
            
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachment(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if collectionView == collectionViewStatus
            
        {
            
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForStatus(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if collectionView == collectionViewCompanyType
        {
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForCompanyType(cell: cell, indexPath: indexPath)
                
            }
            
        }
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionViewStatus
            
        {
            selectedIndex = indexPath.row
            
            collectionViewStatus.reloadData()
            
        }
        
        
        if collectionView == collectionViewCompanyType
            
        {
            if let model:CompanyTypesModel = companyType?[indexPath.row]{
                
                selectedCompanyType = model.companyType ?? 0
                
                collectionViewCompanyType.reloadData()
                
            }
        }
        
        if collectionView == collectionViewAttachment {
            
            if let fileName:AttachmentBannerTypeModel = attachments[indexPath.row]  {
                
                if let imagePath = fileName.cdnPath {
                    
                    if let filePath = fileName.fileName {
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                            let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                            imageViewer.strImg = imagePath
                            navigationController?.pushViewController(imageViewer, animated: true)
                            
                        }
                            
                        else{
                            
                            openWebView(urlString:imagePath, title: filePath, subTitle: "")
                        }
                        
                    }
                }
                
            }
            
        }
        
    }
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete .addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        
        if let model:AttachmentBannerTypeModel = attachments[indexPath.row]
        {
            if let imagePath = model.cdnPath
            {
                if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                    
                    let strFileType = model.fileName?.fileExtension()
                    
                    if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                    {
                        
                        cell.imgView.sd_setImage(with: url, completed: nil)
                        
                    }
                    else{
                        
                        cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                        
                    }
                }
                
            }
            
            for modelMain in attachmentTypes ?? [] {
                
                if modelMain.attachmentType == model.attachmentType {
                    
                    cell.lblTitle.text = modelMain.title ?? ""
                    
                }
                
            }
            
        }
        
        return cell
    }
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:SellerStatusModel = stausArray[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.statusTitle ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
    func getCellForCompanyType(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:CompanyTypesModel = companyType?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.title ?? ""
            
            if selectedCompanyType == model.companyType{
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
    
}

//MARK:-

class AttachmentPictureCell:UICollectionViewCell {
    
    @IBOutlet weak var btnDelete: NKButton!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblTitle: NKLabel!
    
}

class StatusCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCellBackground: RCView!
    
    @IBOutlet weak var lblTitle: NKLabel!
    
}

extension Locale {
    static func locales1(countryName1 : String) -> String {
        let locales : String = ""
        for localeCode in NSLocale.isoCountryCodes {
            let countryName = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: localeCode)
            if countryName1.lowercased() == countryName?.lowercased() {
                return localeCode
            }
        }
        return locales
    }
}

extension NewSellerVC:HtmlEditorVCDelegate{
    
    func didAddedString(htmlStr:String){
        
        htmlDes = htmlStr
        
        if htmlDes.count > 0 {
            
            let productDesTitle = NSMutableAttributedString(string: "Click here to update company description".localized(),attributes: countAttributes)
            
            lblCompanyDes.attributedText = productDesTitle
            
        }
        
    }
    
}
