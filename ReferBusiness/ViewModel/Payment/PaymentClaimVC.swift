//
//  PaymentClaimVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 09/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class PaymentClaimVC: AppBaseViewController,CurrencySelectorVCDelegate {

    @IBOutlet weak var lblAmount: NKLabel!
    
    @IBOutlet weak var txtCurrencyCode: RCTextField!
    
    @IBOutlet weak var txtAmount: RCTextField!
    
    @IBOutlet weak var txtBeneficiaryName: RCTextField!
    
    @IBOutlet weak var txtPanTaxNo: RCTextField!
    
    @IBOutlet weak var txtBankName: RCTextField!
    
    @IBOutlet weak var txtAccountNumber: RCTextField!
    
    @IBOutlet weak var txtIFSCCode: RCTextField!
    
    @IBOutlet weak var txtViewSuggestions: NKTextView!
    
    @IBOutlet weak var viewBGAvailableAmount: UIView!
    
    @IBOutlet weak var imgCheckBox: UIImageView!
    
    var isBuyerClaim:Int = 0
    
    var beneficiaryDetails:BenificiaryAccountDetailModel?
    
    var message:String?
    
    var currency:[CurrencyModel]?
    
    var selectedCurrencyCode : CurrencyModel?
    
    var isSelected:Int = 0
    
    var strAmount:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

       navigationItem.title = "Payment Claim".localized()

        txtAmount.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        txtBeneficiaryName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Benificiary Name".localized())
        
        txtBankName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Bank Name".localized())
        
        txtAccountNumber.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Account Number".localized())
        
        txtIFSCCode.attributedPlaceholder = addPlaceHolderWithText(placeholder: "IFSC Code".localized())

        
        viewBGAvailableAmount.layer.cornerRadius = 5
        viewBGAvailableAmount.layer.shadowColor = UIColor.black.cgColor
        viewBGAvailableAmount.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewBGAvailableAmount.layer.shadowOpacity = 0.3
        viewBGAvailableAmount.layer.shadowRadius = 4.0

        getBeneficiaryDetails()
        
        txtCurrencyCode.isUserInteractionEnabled = false
        
        if let currentCurrency =  getLocalCurrencyCode(){
            
            print("\("Currency::::::::")\(currentCurrency)")
            
            txtCurrencyCode.text = currentCurrency
            
        }
        
      //  lblAmount.text = strAmount ?? ""
        
    }
    
    func updateUI() {
        
        if let model:BenificiaryAccountDetailModel = beneficiaryDetails {
        
            txtBankName.text = model.bankName ?? ""
            txtIFSCCode.text = model.ifscCode ?? ""
            txtViewSuggestions.text = model.beneficiaryComments ?? ""
            txtPanTaxNo.text = model.taxNumber ?? ""
            if let currentCurrency:String =  model.currencySymbol{
                
               txtCurrencyCode.text = model.currencySymbol ?? ""
                
            }
            lblAmount.text = "\(model.currencySymbol ?? "")\(" ")\(model.claimAmount ?? 0.0)"
            txtBeneficiaryName.text = model.beneficiaryName ?? ""
            txtAccountNumber.text = model.accountNumber ?? ""
            
        }
        
    }
    

    @IBAction func btnCheckBoxDeclaration(_ sender: Any) {
        
        if isSelected == 0 {
            
            imgCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
            
            isSelected = 1
        }
        else{
            
            imgCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBox")
            
            isSelected = 0
        }
    }
    
    @IBAction func btnProceedAction(_ sender: Any) {
        
        if Double(txtAmount.text ?? "") ?? 0.0 > beneficiaryDetails?.claimAmount ?? 0.0 {
            
            showErrorMessage(message: "Claim amount should not exceed than available amount".localized())
            
            return
            
        }
        else if Double(txtAmount.text ?? "") ?? 0.0 == 0.0  {
            
            showErrorMessage(message: "Claim amount should be more than 0".localized())
            
           return
        }
        else if txtBeneficiaryName.text?.count ?? 0 == 0  {
            
            txtBeneficiaryName.showError(message: "Required".localized())
            
            return
        }
        else if txtBankName.text?.count ?? 0 == 0  {
            
            txtBankName.showError(message: "Required".localized())
            
            return
        }
        else if txtAccountNumber.text?.count ?? 0 == 0  {
            
            txtAccountNumber.showError(message: "Required".localized())
            
            return
        }
        else if txtIFSCCode.text?.count ?? 0 <= 0  {
            
             txtIFSCCode.showError(message: "Required".localized())
            
            return
        }
        else if isSelected == 0  {
            
            showErrorMessage(message: "Please check the terms".localized())
            
            return
        }
        
        else{
              showAlert()
        }
    
    }
    
    func showAlert() {
        
        let alert = UIAlertController(title: "Please ensure this information accurate to avoid problems in payment transfer.".localized(), message:"Are you sure to submit?".localized(), preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.btnLinkColor
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
             self.submitClaimRequest()
            
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "No".localized(),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                        
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func finalPage() {
        
        if  let finalPage:ReferralSuccessVC = UIStoryboard.init(name: SearchSB, bundle: nil).instantiateViewController(withIdentifier: "ReferralSuccessVC") as? ReferralSuccessVC{
            finalPage.message = message ?? ""
            
            navigationController?.present(finalPage, animated: true, completion: nil)
            navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
    
        
            txtCurrencyCode.text = selectedCurrencyCode?.currencySymbol ?? ""
        
    }
    
    
    func getBeneficiaryDetails() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getBeneficiaryDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&isBuyerClaim=\(isBuyerClaim)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(BenificiaryAccountDetailApiModel.self, from: data)
                                
                                if let apiResponse:BenificiaryAccountDetailModel = jsonResponse.beneficiaryDetails {
                                    
                                    weakSelf?.beneficiaryDetails = apiResponse
                                    
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func submitClaimRequest(){
        
        startAnimating()
        
        let params:NSDictionary = [
            "amountToClaim": txtAmount.text ?? "",
            "currencyId" : txtCurrencyCode.text ?? "",
            "taxNumber" : txtPanTaxNo.text ?? "",
            "beneficiaryName": txtBeneficiaryName.text ?? "",
            "bankName": txtBankName.text ?? "",
            "accountNumber": txtAccountNumber.text ?? "",
            "ifscCode": txtIFSCCode.text ?? "",
            "beneficiaryComments":txtViewSuggestions.text ?? ""
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/referNow?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            if let messageText:String = dataObj.object(forKey:"successMessage") as? String{
                                
                                let isUpdated:[String: Int] = ["update": 1]

                                   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
                                
                                message = messageText
                                
                                weakSelf?.finalPage()
                            }
                            
                            return
                            
                        }
                        
                    }
                    
                }
                if let serverMessage = jsonObj?.message(){
                    message = (serverMessage.count>0) ? serverMessage : message
                }
                weakSelf?.showErrorMessage(message: message)
                
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
}
extension PaymentClaimVC:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if  textField == txtCurrencyCode  {
            
            openCurrencySelector()
            
            return false
        }
        
        return true
    }
}
