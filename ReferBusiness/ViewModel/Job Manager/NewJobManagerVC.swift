//
//  NewJobManagerVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 24/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class NewJobManagerVC: AppBaseViewController {
    
    @IBOutlet weak var txtProductName: RCTextField!
    
    @IBOutlet weak var txtJobCode: RCTextField!
    
    @IBOutlet weak var lblLocationtext: NKLabel!
    
    @IBOutlet weak var txtViewProductDes: NKTextView!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var lblStatusTitle: NKLabel!
    
    @IBOutlet weak var viewLocation: RCView!
    
    @IBOutlet weak var btnSumit: NKButton!
    
    @IBOutlet weak var tableViewLocationList: UITableView!
    
    @IBOutlet weak var heightViewLocation: NSLayoutConstraint!
    
    @IBOutlet weak var txtExpFrom: RCTextField!
    
    @IBOutlet weak var txtExpTo: RCTextField!
    
    @IBOutlet weak var txtJobType: RCTextField!
    
    @IBOutlet weak var tableViewSkillsList: UITableView!
    
    @IBOutlet weak var heightViewSkills: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewEducation: NSLayoutConstraint!
    
    @IBOutlet weak var viewProximity: RCView!
    
    @IBOutlet weak var txtProximity: RCTextField!
    
    @IBOutlet weak var txtProximityType: RCTextField!
    
    @IBOutlet weak var tableViewEducation: UITableView!
    
    @IBOutlet weak var txtNoticePeriodFrom: RCTextField!
    
    @IBOutlet weak var txtNoticePeriodTo: RCTextField!
    
    @IBOutlet weak var txtExpectedCurrencyCode: RCTextField!
    
    @IBOutlet weak var txtExpectedAmtFrom: RCTextField!
    
    @IBOutlet weak var txtExpectedAmtTo: RCTextField!
    
    @IBOutlet weak var txtTotalCVRequired: RCTextField!
    
    @IBOutlet weak var btnSubmitCV: NKButton!
    
    @IBOutlet weak var viewCVSourcing: UIView!
    
    @IBOutlet weak var viewPlacement: UIView!
    
    @IBOutlet weak var imgRadioCVSourcing: UIImageView!
    
    @IBOutlet weak var imgRadioPlacement: UIImageView!
    
    @IBOutlet weak var segmentDirectScreened: UISegmentedControl!
    
    @IBOutlet weak var lblDirectScreenedText: NKLabel!
    
    @IBOutlet weak var viewSegmentDirectScreened: UIView!
    
    @IBOutlet weak var heightViewSegmentDirectScreened: NSLayoutConstraint!
    
    @IBOutlet weak var heightCVrequirement: NSLayoutConstraint!
    
    @IBOutlet weak var heightCVReqRadio: NSLayoutConstraint!
    
    @IBOutlet weak var heightAssessment: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewLocJobSeekers: UITableView!
    
    @IBOutlet weak var heightLocJobSeeker: NSLayoutConstraint!
    
    var assessmentDetail:AssesmentModel?
    
    var branch = Set<Int>()
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    var branchCode:[Int] = []
    
    var selectedCurrencyCode : CurrencyModel?
    
    var sellerCode:Int = 0
    
    var productCode:Int = 0
    
    var attachmentsProduct:[AttachmentBannerTypeModel] = []
    
    var productCount:Int = 0
    
    var selectedIndex:Int = 1
    
    var statusList:[SellerStatusModel]?
    
    var productList:[ProductModel]?
    
    var colorCodes:ColorModel?
    
    var responseTypes:[ResponseTypeModel]?
    
    var currency:[CurrencyModel]?
    
    var branchList:[BranchModel] = []
    
    var preferredLocList:[BranchModel] = []
    
    var attachmentTypes:[AttachmentTypeModel]?
    
    var referralValue = 0.0
    
    var referralFeeType:Int = 0
    
    var referralCurrencyId:Int?
    
    var productDetail:ProductDetailModel?
    
    var isDetail:Bool = false
    
    var btnName = UIButton()
    
    weak var delegate:NewJobVCDelegate?
    
    var htmlDes:String = ""
    
    var companyType:Int = 1
    
    let countAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 8),
        .foregroundColor: UIColor.btnLinkColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var jobTypeList:[JobTypeFilter]?
    
    var jobOptions:[String] = []
    
    var proximityOptions:[String] = []
    
    var jobType:Int?
    
    var isCVPlacementSelected:Int?
    
    var educationList:[EducationModel]?
    var specializationList:[SpecializationModel]?
    var skillCategoryList:[SkillCategoryModel]?
    var skillList:[SkillListModel]?
    var scaleDurationList:[ScaleDurationModel]?
    var countryList:[CountryListModel]?
    var currencyList:[CurrencyModel]?
    var skillListDetail:[JobSkillDetailModel]?
    var yearList:[YearModel]?
    
    var educationDetail:[EducationDetailModel]?
    
    var percentageList:[PercentageModel]?
    
     var btnName = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Job Manager".localized()
        
        fetchJobMaster()
        
        updateCVRadioView(isSelected:0)
        
        //        proximityOptions.append("<-- Select -->".localized())
        //              proximityOptions.append("kms".localized())
        //              proximityOptions.append("mi".localized())
        //
        //              txtProximityType.pickerOptions = proximityOptions
    }
    
    func updateUI(){
        calLocationViewHeight()
        calSkillViewHeight()
        calEducationViewHeight()
        calAssessmentViewHeight(isDataPresent: 0)
        calJobSeekerLocationViewHeight()
    }
    
    func updateCVRadioView(isSelected:Int){
        
        if isSelected == 1 {
            imgRadioCVSourcing.image = UIImage.init(named: "radioFilled")
            imgRadioPlacement.image = UIImage.init(named: "radio")
            btnSubmitCV.alpha = 0
            viewSegmentDirectScreened.alpha = 1
            heightViewSegmentDirectScreened.constant = 80
            heightCVReqRadio.constant = 180
            heightCVrequirement.constant = 280
        }
        else if isSelected == 2 {
            imgRadioPlacement.image = UIImage.init(named: "radioFilled")
            imgRadioCVSourcing.image = UIImage.init(named: "radio")
            btnSubmitCV.alpha = 0
            viewSegmentDirectScreened.alpha = 0
            heightViewSegmentDirectScreened.constant = 0
            heightCVReqRadio.constant = 100
            heightCVrequirement.constant = 200
        }
        else{
            imgRadioPlacement.image = UIImage.init(named: "radio")
            imgRadioCVSourcing.image = UIImage.init(named: "radio")
            btnSubmitCV.alpha = 0
            viewSegmentDirectScreened.alpha = 0
            heightViewSegmentDirectScreened.constant = 0
            heightCVReqRadio.constant = 100
            heightCVrequirement.constant = 200
        }
        
    }
    
    func updateNavBar(){
        <#function body#>
    }
    
    
    func calLocationViewHeight(){
        
        let heightBranch = branchList.count
        
        heightViewLocation.constant = CGFloat((heightBranch * 100) + 60)
        
        tableViewLocationList.reloadData()
    }
    
    func calSkillViewHeight(){
        
        let heightSkills = skillListDetail?.count ?? 0
        
        heightViewSkills.constant = CGFloat((heightSkills * 44) + 60)
        
        tableViewSkillsList.reloadData()
        
    }
    
    func calEducationViewHeight(){
        
        let heightEducation = educationDetail?.count ?? 0
        
        heightViewEducation.constant = CGFloat((heightEducation * 200) + 60)
        
        tableViewEducation.reloadData()
        
    }
    
    func calAssessmentViewHeight(isDataPresent:Int){
        
        var heightAssessmentView:CGFloat = 50
        
        if isDataPresent == 1 {
            
            heightAssessmentView = 150
        }
        
        heightAssessment.constant = heightAssessmentView
        
    }
    
    func calJobSeekerLocationViewHeight(){
        
        let heightJobSeeker = preferredLocList.count
        
        heightLocJobSeeker.constant = CGFloat((heightJobSeeker * 100) + 70)
        
        tableViewLocJobSeekers.reloadData()
        
    }
    
    
    @IBAction func btnAddLocationAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: BranchSB, bundle: nil)
        let branchListPage:BranchListVC = storyBoard.instantiateViewController(withIdentifier: "BranchListVC") as! BranchListVC
        
        branchListPage.sellerCode = sellerCode
        
        if let colorModel:ColorModel = colorCodes{
            branchListPage.colorCodes = colorModel
        }
        branchListPage.delegate = self
        branchListPage.isAdmin = isAdmin ?? 0
        branchListPage.isPartnerForCompany = isPartnerForCompany ?? 0
        branchListPage.isFromJob = 1
        navigationController?.pushViewController(branchListPage, animated: true)
        
    }
    
    @IBAction func btnAddSkillsAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: JobManagerSB, bundle: nil)
        let newSkillsPage:NewJobSkillsVC = storyBoard.instantiateViewController(withIdentifier: "NewJobSkillsVC") as! NewJobSkillsVC
        
        newSkillsPage.skillCategoryList = skillCategoryList ?? []
        newSkillsPage.skillList = skillList ?? []
        newSkillsPage.skillListDetail = skillListDetail ?? []
        newSkillsPage.delegate = self
        navigationController?.pushViewController(newSkillsPage, animated: true)
    }
    
    @IBAction func btnAddEducationAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: JobManagerSB, bundle: nil)
        let newEducationPage:NewJobEducationVC = storyBoard.instantiateViewController(withIdentifier: "NewJobEducationVC") as! NewJobEducationVC
        newEducationPage.specializationList = specializationList ?? []
        newEducationPage.educationList = educationList
        newEducationPage.percentageList = percentageList ?? []
        newEducationPage.educationDetail = educationDetail ?? []
        newEducationPage.yearList = yearList ?? []
        newEducationPage.delegate = self
        navigationController?.pushViewController(newEducationPage, animated: true)
        
        
    }
    
    
    @IBAction func btnAddAssessmentTemplate(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: JobManagerSB, bundle: nil)
        let newAssessmentPage:AssesmentListVC = storyBoard.instantiateViewController(withIdentifier: "AssesmentListVC") as! AssesmentListVC
        newAssessmentPage.sellerCode = sellerCode
        newAssessmentPage.productCode = productCode
        newAssessmentPage.delegate = self
        navigationController?.pushViewController(newAssessmentPage, animated: true)
        
    }
    
    @IBAction func btnAddPreferredLocation(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: BranchSB, bundle: nil)
        let branchListPage:BranchListVC = storyBoard.instantiateViewController(withIdentifier: "BranchListVC") as! BranchListVC
        
        branchListPage.sellerCode = sellerCode
        
        if let colorModel:ColorModel = colorCodes{
            branchListPage.colorCodes = colorModel
        }
        branchListPage.delegate2 = self
        branchListPage.isAdmin = isAdmin ?? 0
        branchListPage.isPartnerForCompany = isPartnerForCompany ?? 0
        branchListPage.isFromJob = 2
        navigationController?.pushViewController(branchListPage, animated: true)
    }
    
    @IBAction func btnSelectCVSourcingAction(_ sender: Any) {
        
        isCVPlacementSelected = 1
        updateCVRadioView(isSelected:isCVPlacementSelected ?? 0)
    }
    
    @IBAction func btnPlacementAction(_ sender: Any) {
        isCVPlacementSelected = 2
        updateCVRadioView(isSelected:isCVPlacementSelected ?? 0)
    }
    
    
    @IBAction func segmentDirectAction(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            lblDirectScreenedText.text = "INR 50 per cv"
            break
        case 1:
            lblDirectScreenedText.text = "INR 150 per cv"
            break
        default:
            print("Burning inside")
        }
        
        
    }
    
    @objc func btnDeleteLocation(_ sender: UIButton){
        
        if branchList.count > 0 {
            
            branchList.remove(at: sender.tag)
            calLocationViewHeight()
        }
    }
    
    @objc func btnDeletePreJobSeekerLocation(_ sender: UIButton){
          
          if preferredLocList.count > 0 {
              
              preferredLocList.remove(at: sender.tag)
              calJobSeekerLocationViewHeight()
          }
      }
    
    @objc func btnDeleteEducation(_ sender: UIButton){
        
        if educationDetail?.count ?? 0 > 0 {
            
            educationDetail?.remove(at: sender.tag)
            calEducationViewHeight()
        }
    }
    
    
    func fetchJobMaster() {
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        
        //        if isFirst == true
        //        {
        //            isFirst = false
        //            startAnimating()
        //        }
        //        else
        //        {
        //            startAnimatingAfterSubmit()
        //        }
        startAnimating()
        
        let params:NSDictionary = ["sellerCode":sellerCode,"productCode":productCode,"productType":1]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/JobDetailsWithMaster?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            DispatchQueue.main.async
                                {
                                    weakSelf?.stopAnimating()
                            }
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(JobDetailApiModel.self, from: data)
                                
                                if let apiResponse:[SkillListModel] = jsonResponse.skillList {
                                    
                                    weakSelf?.skillList = apiResponse
                                    
                                }
                                if let apiResponse:[SkillCategoryModel] = jsonResponse.skillCategoryList {
                                    
                                    weakSelf?.skillCategoryList = apiResponse
                                    
                                }
                                if let apiResponse:[SpecializationModel] = jsonResponse.specializationList {
                                    
                                    weakSelf?.specializationList = apiResponse
                                    
                                }
                                if let apiResponse:[ScaleDurationModel] = jsonResponse.scaleDurationList {
                                    
                                    weakSelf?.scaleDurationList = apiResponse
                                    
                                }
                                if let apiResponse:[EducationModel] = jsonResponse.educationList {
                                    
                                    weakSelf?.educationList = apiResponse
                                    
                                }
                                if let apiResponse:[JobTypeFilter] = jsonResponse.jobTypeList {
                                    
                                    weakSelf?.jobTypeList = apiResponse
                                    
                                }
                                if let apiResponse:[CountryListModel] = jsonResponse.countryList {
                                    
                                    weakSelf?.countryList = apiResponse
                                    
                                }
                                if let apiResponse:[CurrencyModel] = jsonResponse.currencyList {
                                    
                                    weakSelf?.currencyList = apiResponse
                                    
                                }
                                if let apiResponse:[PercentageModel] = jsonResponse.percentageList {
                                    
                                    weakSelf?.percentageList = apiResponse
                                    
                                }
                                if let apiResponse:[YearModel] = jsonResponse.yearList {
                                    
                                    weakSelf?.yearList = apiResponse
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            //Show error
            DispatchQueue.main.async
                {
                    weakSelf?.stopAnimating()
            }
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    func submitProductDetails(){
     
        var expTo = 0
        var expFrom = 0
        var noticePeriodFrom = 0
        var noticePeriodTo = 0
        var expSalaryFrom = 0
        var expSalaryTo = 0
        var totalCvReq = 0
        
        if let exp = Int(txtExpTo.text ?? "0") {
           expTo = exp
        }
        
        if let exp = Int(txtExpFrom.text ?? "0") {
            expFrom = exp
        }
        
        if let noticeTo = Int(txtNoticePeriodTo.text ?? "0") {
            noticePeriodTo = noticeTo
        }
        
        if let noticeFrom = Int(txtNoticePeriodFrom.text ?? "0") {
            noticePeriodFrom = noticeFrom
        }
        if let totalCv = Int(txtTotalCVRequired.text ?? "0") {
            totalCvReq = totalCv
        }
        
        if expTo < expFrom {
           showErrorMessage(message: "'Experience From' should be lesser than 'Experience TO'".localized())
           return
        }
    
         
        
        if jobTypeList?.count ?? 0 > 0 {
            
            for model in jobTypeList ?? []{
                
                if model.title == txtJobType.selectedOption {
                    
                    jobType = model.jobType ?? 0
                    
                }
                
            }
            
        }
        
        var array : [Any] = []
        
        for dict in attachmentsProduct
        {
            var model:[String : Any] = [:]
            model["cdnPath"] = dict.cdnPath
            model["attachmentType"] = dict.attachmentType
            model["fileName"] = dict.fileName
            array.append(model)
        }

        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["sellerCode": sellerCode,
                                   "productCode": productCode,
                                   "productName" : txtProductName.text ?? "",
                                   "description" : txtViewProductDes.text ?? "",
                                
                                   "status" : selectedIndex,
                                   
                                   "branchCode": branchCode,
                                   
                                   "productType":1,
                        
                                   "productCodeText":txtJobCode.text ?? "",
                                  
                                   "skill":"skill list",
                                   
                                   "education":"education",
                                   
                                   "expTo":expTo,
                                   
                                   "expFrom":expFrom,
                                   
                                   "jobType":jobType ?? 0,
            
                                   "noticePeriodFrom":noticePeriodFrom,
             
                                   "noticePeriodTo":noticePeriodTo,
             
                                   "expSalaryCurrId": 3,
             
                                   "expSalaryFrom":expSalaryFrom,
            
                                   "expSalaryTo":expSalaryTo,
             
                                   "expSalaryScaleDurationId": 3,
            
                                   "totalCVReq": totalCvReq,
             
                                   "publishJobFor": 0,
             
                                   "sourcingType": isCVPlacementSelected ?? 0,
             
                                   "assessmentTemplateCode": 1,

            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerProducts?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                //                            let apiResponse = try decoder.decode(CategoryListModel.self, from: data)
                                //                            weakSelf?.categoryNewList = apiResponse.categoriesList ?? []
                                //                            weakSelf?.searchCategoryList = apiResponse.categoriesList ?? []
                                //
                                //                            DispatchQueue.main.async {
                                //
                                //                                weakSelf?.collectionViewCategory.reloadData()
                                //                            }
                                
                               // weakSelf?.delegate?.didAddedJob()
                                
                                
                                weakSelf?.showInfoMessage(message: message.localized())
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    
    
    
}

extension NewJobManagerVC:NewJobSkillsVCDelegate,NewJobEducationVCDelegate,AssesmentListVCDelegate{
    
    func didAddedEducation(withModel: [EducationDetailModel]) {
        educationDetail = withModel
        calEducationViewHeight()
    }
    
    
    func didAddedSkills(withModel: [JobSkillDetailModel]) {
        
        skillListDetail = withModel
        
        calSkillViewHeight()
        
    }
    
    func didAddedAssesment(withModel:AssesmentModel){
        
        assessmentDetail = withModel
        calAssessmentViewHeight(isDataPresent: 1)
        
    }
    
    
}

extension NewJobManagerVC:BranchListVCDelegate{
    
    func didAddedNewBranch() {
        
    }
    
    func didSelectedBranch(withModel:BranchModel) {
        
        if branchList.count > 0 {
            
            if !self.branch.contains(withModel.branchCode ?? 0) {
                branchList.append(withModel)
                return
            }
            
        }
        else{
            
            branchList.append(withModel)
            branch.insert(withModel.branchCode ?? 0)
            return
        }
        
        calLocationViewHeight()
    }
    
    func didSelectedPreferredBranch(withModel:BranchModel){
        
        preferredLocList.append(withModel)
        calJobSeekerLocationViewHeight()
    }
    
    
}


extension NewJobManagerVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tableViewLocationList {
            
            return UITableView.automaticDimension
            
        }
        
        if tableView == tableViewSkillsList {
            
            return 44
            
        }
        
        if tableView == tableViewEducation {
            
            return UITableView.automaticDimension
            
        }
        
        if tableView == tableViewLocJobSeekers {
            
            return UITableView.automaticDimension
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableViewLocationList {
            
            return branchList.count
            
        }
        
        if tableView == tableViewSkillsList {
            
            return skillListDetail?.count ?? 0
            
        }
        
        if tableView == tableViewEducation {
            
            return educationDetail?.count ?? 0
            
        }
        if tableView == tableViewLocJobSeekers {
                  
            return preferredLocList.count
                  
        }
        
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewLocationList {
            
            if let cell:SelectLocationCell = tableView.dequeueReusableCell(withIdentifier: "SelectLocationCell", for: indexPath) as? SelectLocationCell {
                
                return getCellForJobLocation(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if tableView == tableViewSkillsList {
            
            if let cell:ShowSkills = tableView.dequeueReusableCell(withIdentifier: "ShowSkills", for: indexPath) as? ShowSkills {
                
                return  getCellForJobSkills(cell:cell,indexPath:indexPath)
                
            }
            
        }
        
        if tableView == tableViewEducation {
            
            if let cell:ShowEducationCell = tableView.dequeueReusableCell(withIdentifier: "ShowEducationCell", for: indexPath) as? ShowEducationCell {
                
                return  getCellForEducation(cell:cell,indexPath:indexPath)
                
            }
            
        }
        
        if tableView == tableViewLocJobSeekers {
            
            if let cell:SelectLocationCell = tableView.dequeueReusableCell(withIdentifier: "SelectLocationCell", for: indexPath) as? SelectLocationCell {
                
                return  getCellForPreferredJobLocation(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if let model:ProductBranchModel = branchList?[indexPath.row]{
        
        //            if model.isSelected == 0 {
        //
        //                model.isSelected = 1
        //
        //            }
        //            else{
        //                model.isSelected = 0
        //            }
        //
        //            tableViewLocationList.reloadData()
        //    }
        
    }
    
    func getCellForJobLocation(cell:SelectLocationCell,indexPath:IndexPath) -> SelectLocationCell {
        
        cell.selectionStyle = .none
        
        cell.imgViewDelete.setImageColor(color: .white)
        
        if let model:BranchModel = branchList[indexPath.row]{
            
            cell.lblTitle.text = model.branchName ?? ""
            
            cell.lblAddress.text = model.address ?? ""
            
            cell.btnDelete.tag = indexPath.row
            
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteLocation(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    
    func getCellForPreferredJobLocation(cell:SelectLocationCell,indexPath:IndexPath) -> SelectLocationCell {
        
        cell.selectionStyle = .none
        
        cell.imgViewDelete.setImageColor(color: .white)
        
        if let model:BranchModel = preferredLocList[indexPath.row]{
            
            cell.lblTitle.text = model.branchName ?? ""
            
            cell.lblAddress.text = model.address ?? ""
            
            cell.btnDelete.tag = indexPath.row
            
            cell.btnDelete.addTarget(self, action: #selector(btnDeletePreJobSeekerLocation(_:)), for: .touchUpInside)
        }
        
        
        return cell
    }
    
    func getCellForJobSkills(cell:ShowSkills,indexPath:IndexPath) -> ShowSkills {
        
        cell.selectionStyle = .none
        
        if let model:JobSkillDetailModel = skillListDetail?[indexPath.row]{
            
            cell.lblTitle.text = model.skillTitle ?? ""
            cell.lblSkillsRange.text = "\(model.minSkillLevel ?? 0)* to \(model.maxSkillLevel ?? 0)*"
            cell.lblExpRange.text = "\(model.expFrom ?? 0) to \(model.expTo ?? 0) years"
            
        }
        
        return cell
    }
    
    func getCellForEducation(cell:ShowEducationCell,indexPath:IndexPath) -> ShowEducationCell {
        
        cell.selectionStyle = .none
        
        if let model:EducationDetailModel = educationDetail?[indexPath.row]{
            
            cell.lblEducation.text = model.educationTitle ?? ""
            cell.lblSpecialization.text = model.specilaizationTitle ?? ""
            cell.lblCompleted.text = "\(model.yopFrom ?? 0) - \(model.yopTo ?? 0)"
            cell.lblPercentage.text = model.percentageTitle ?? ""
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteEducation(_:)), for: .touchUpInside)
            cell.imgViewDelete.setImageColor(color: .white)
            
        }
        
        return cell
    }
    
}


extension NewJobManagerVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            return CGSize.init(width: collectionViewStatus.frame.width / 4, height: 40.0)
            
        }
        
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == collectionViewStatus{
            
            if statusList?.count ?? 0 > 0 {
                
                return statusList?.count ?? 0
            }
            
        }
        
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //        if collectionView == collectionViewAttachment
        //        {
        //            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
        //
        //                return getCellForAttachment(cell: cell, indexPath: indexPath)
        //
        //            }
        //        }
        //        else{
        
        if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
            
            return getCellForStatus(cell: cell, indexPath: indexPath)
            
        }
        
        //  }
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionViewStatus
            
        {
            selectedIndex = indexPath.row
            
            collectionViewStatus.reloadData()
            
        }
        
    }
    
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:SellerStatusModel = statusList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.statusTitle ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
}

class SelectLocationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle:NKLabel!
    
    @IBOutlet weak var imgViewDelete:UIImageView!
    
    @IBOutlet weak var viewBg: RCView!
    
    @IBOutlet weak var lblAddress: NKLabel!
    
    @IBOutlet weak var btnDelete: UIButton!
    
}

class ShowSkills: UITableViewCell {
    
    @IBOutlet weak var lblTitle:NKLabel!
    
    @IBOutlet weak var lblSkillsRange:NKLabel!
    
    @IBOutlet weak var lblExpRange:NKLabel!
    
    @IBOutlet weak var viewBg: RCView!
    
}

class ShowEducationCell: UITableViewCell {
    
    @IBOutlet weak var lblEducation:NKLabel!
    @IBOutlet weak var lblSpecialization:NKLabel!
    @IBOutlet weak var lblPercentage:NKLabel!
    @IBOutlet weak var lblCompleted:NKLabel!
    
    @IBOutlet weak var imgViewDelete: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    
    
}
