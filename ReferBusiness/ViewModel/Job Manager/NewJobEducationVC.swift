//
//  NewJobEducationVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

protocol NewJobEducationVCDelegate:class {
    
    func didAddedEducation(withModel:[EducationDetailModel])
    
}

class NewJobEducationVC: AppBaseViewController,RCTextFieldDelegate {
    
    
    @IBOutlet weak var txtEducation: RCTextField!
    @IBOutlet weak var txtField: RCTextField!
    @IBOutlet weak var txtPercentage: RCTextField!
    @IBOutlet weak var txtEduCompletedFrom: RCTextField!
    @IBOutlet weak var txtEduCompletedTo: RCTextField!
    
    @IBOutlet weak var btnAddEducation: NKButton!
    
    var educationList:[EducationModel]?
    
    var specializationList:[SpecializationModel]?
    
    var percentageList:[PercentageModel]?
    
    var educationDetail:[EducationDetailModel] = []
    
    var yearList:[YearModel]?
    
    weak var delegate:NewJobEducationVCDelegate?
    
    var educationOptions:[String] = []
      
    var specializationOptions:[String] = []
    
    var percentageOptions:[String] = []
    
    var yearOptions:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Education".localized()
        
         updateUI()
        
    }
    
    func updateUI(){
        
        txtEducation.RCDelegate = self
        
        if educationList?.count ?? 0 > 0 {
            
            for model in educationList ?? [] {
                
                educationOptions.append(model.title ?? "")
            }
            
            txtEducation.pickerOptions = educationOptions
            txtEducation.dropDownPicker?.reloadAllComponents()
            
        }
        
        if specializationList?.count ?? 0 > 0 {
            
            for model in specializationList ?? [] {
                
                specializationOptions.append(model.title ?? "")
            }
            
            txtField.pickerOptions = specializationOptions
            txtField.dropDownPicker?.reloadAllComponents()
            
        }
        
        if percentageList?.count ?? 0 > 0 {
            
            for model in percentageList ?? [] {
                
                percentageOptions.append(model.title ?? "")
            }
            
            txtPercentage.pickerOptions = percentageOptions
            txtPercentage.dropDownPicker?.reloadAllComponents()
            
        }
        
        if yearList?.count ?? 0 > 0 {
            
            for model in yearList ?? [] {
                
                yearOptions.append("\(model.year ?? 0)")
            }
            
            txtEduCompletedFrom.pickerOptions = yearOptions
            txtEduCompletedFrom.dropDownPicker?.reloadAllComponents()
            
            txtEduCompletedTo.pickerOptions = yearOptions
            txtEduCompletedTo.dropDownPicker?.reloadAllComponents()
        }
        
        
    }
    
    
    @IBAction func btnAddEducation(_ sender: Any) {
        
        if txtEducation?.text?.count == 0 {
            
            txtEducation.showError(message: "required")
            
            return
        }
        
        if txtField?.text?.count == 0 {
            
            txtField.showError(message: "required")
            return
        }
        
        
        if txtEduCompletedFrom?.text?.count == 0 {
            
            txtEduCompletedFrom.showError(message: "required")
            return
        }
        
        if txtEduCompletedTo?.text?.count == 0 {
            
            txtEduCompletedTo.showError(message: "required")
            return
        }
        
    let model = EducationDetailModel()
        
    if educationList?.count ?? 0 > 0 {
        
        for item in educationList ?? [] {
            
            if txtEducation.selectedOption == item.title ?? "" {
                
                model.educationCode = item.educationCode ?? 0
                model.educationTitle = item.title ?? ""
              
                
            }
            
        }
        
    }
    
    if specializationList?.count ?? 0 > 0 {
        
        for item in specializationList ?? [] {
            
            if txtField.selectedOption == item.title ?? "" {
                
                model.specializationCode = item.specializationCode ?? 0
                model.specilaizationTitle = item.title ?? ""
                
            }
            
        }
        
      }
        
        if percentageList?.count ?? 0 > 0 {
            
            for item in percentageList ?? [] {
                
                if txtPercentage.selectedOption == item.title ?? "" {
                    
                    model.percentageCode = item.code ?? 0
                    model.percentageTitle = item.title ?? ""
                    
                }
            }
            
        }
        
        
        model.yopFrom = Int(txtEduCompletedFrom.selectedOption)
        model.yopTo = Int( txtEduCompletedTo.selectedOption)
    

        educationDetail.append(model)
        
        delegate?.didAddedEducation(withModel: educationDetail)
        
        navigationController?.popViewController(animated: true)
    }
    
 func didChangeOption(textField: RCTextField) {
        
    if textField == txtEducation {
       
        if educationList?.count ?? 0 > 0 {
            
            for model in educationList ?? []{
                
                if model.title == txtEducation.selectedOption {
                    
                    if model.noSpecialization == 1 {
                        txtField.isUserInteractionEnabled = false
                        return
                    }
                    else{
                         txtField.isUserInteractionEnabled = true
                    }
                    
                  }
              }
           }
        }
    }
}


extension NewJobEducationVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell:JobAddSkillCell = tableView.dequeueReusableCell(withIdentifier: "JobAddSkillCell", for: indexPath) as? JobAddSkillCell {
            
            return getCellForSkills(cell: cell, indexPath: indexPath)
        }
        
        return UITableViewCell.init()
    }
    
    func getCellForSkills(cell:JobAddSkillCell,indexPath:IndexPath)->JobAddSkillCell{
        
        return cell
    }
    
}

extension NewJobEducationVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField ==  txtEducation {
            
            txtEducation.showError(message: "")
            
        }
        if textField ==  txtField {
            
            txtField.showError(message: "")
            
        }
        
        if textField ==  txtPercentage {
            
            txtPercentage.showError(message: "")
            
        }
        
        if textField ==  txtEduCompletedFrom {
            
            txtEduCompletedFrom.showError(message: "")
            
        }
        
        if textField ==  txtEduCompletedTo {
            
            txtEduCompletedTo.showError(message: "")
            
        }
        
        return true
    }
}
