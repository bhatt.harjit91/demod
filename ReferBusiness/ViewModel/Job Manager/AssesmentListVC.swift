//
//  AssesmentListVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 25/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

protocol AssesmentListVCDelegate:class {
    func didAddedAssesment(withModel:AssesmentModel)
}

class AssesmentListVC: AppBaseViewController {
    
    @IBOutlet weak var tableViewList: UITableView!
    
    var sellerCode:Int = 0
    var productCode:Int = 0
    var limit:Int = 40
    
    var assessmentList:[AssesmentModel]?
    
    weak var delegate:AssesmentListVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchAssessmentList()
    }
    
    
    func fetchAssessmentList() {
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        
        //        if isFirst == true
        //        {
        //            isFirst = false
        //            startAnimating()
        //        }
        //        else
        //        {
        //            startAnimatingAfterSubmit()
        //        }
        startAnimating()
        
        let params:NSDictionary = ["sellerCode":sellerCode,"productCode":productCode,"limit":limit]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/getAssessmentList?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            DispatchQueue.main.async
                                {
                                    weakSelf?.stopAnimating()
                            }
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(AssesmentApiModel.self, from: data)
                                
                                if let apiRespone:[AssesmentModel] = jsonResponse.assessmentList {
                                    
                                    weakSelf?.assessmentList = apiRespone
                                    
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            //Show error
            DispatchQueue.main.async
                {
                    weakSelf?.stopAnimating()
            }
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    
}

extension AssesmentListVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return assessmentList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:AssesmentListCell = tableView.dequeueReusableCell(withIdentifier: "AssesmentListCell", for: indexPath) as? AssesmentListCell {
            
            return getCellForAssement(cell: cell, indexPath: indexPath)
        }
        
        return UITableViewCell.init()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:AssesmentModel = assessmentList?[indexPath.row]{
        
              delegate?.didAddedAssesment(withModel: model)
             navigationController?.popViewController(animated: true)
            
        }
    }
    
    func getCellForAssement(cell:AssesmentListCell,indexPath:IndexPath)->AssesmentListCell{
        
        if let model:AssesmentModel = assessmentList?[indexPath.row]{
            
            cell.lblTitle.text = model.title ?? ""
            cell.lblCutOffScore.text = "\(model.cutOff ?? 0)"
            //cell.lblStatusText.text = model.
            
        }
        
        return cell
    }
    
    
}

class AssesmentListCell:UITableViewCell{
    
    @IBOutlet weak var lblTitle:NKLabel!
    
    @IBOutlet weak var lblCutOffScore:NKLabel!
    
    @IBOutlet weak var lblStatusText: NKLabel!
    
    @IBOutlet weak var viewStatus: RCView!
    
}
