//
//  NewJobSkillsVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

protocol NewJobSkillsVCDelegate:class {
    
    func didAddedSkills(withModel:[JobSkillDetailModel])
    
}

class NewJobSkillsVC: AppBaseViewController {
    
    
    @IBOutlet weak var txtSkillCategory: RCTextField!
    
    @IBOutlet weak var txtAddSkill: RCTextField!
    
    @IBOutlet weak var btnAddSkill: NKButton!
    
    @IBOutlet weak var tableViewExpList: UITableView!
    
    @IBOutlet weak var viewSlider: RangeSeekSlider!
    
    @IBOutlet weak var txtExpFrom: RCTextField!
    
    @IBOutlet weak var txtExpTo: RCTextField!
    
    @IBOutlet weak var lblSelectedSkills: NKLabel!
    
    @IBOutlet weak var heightLblSelectedSkills: NSLayoutConstraint!
    
    var skillCategoryList:[SkillCategoryModel]?
    
    var skillListDetail:[JobSkillDetailModel] = []
    
    var skillList:[SkillListModel]?
    
    var educationOptions:[String] = []
    
    var skillOptions:[String] = []
    
    var skillMinValue:Int = 1
    
    var skillMaxValue:Int = 5
    
    weak var delegate:NewJobSkillsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationItem.title = "Skills".localized()
        
        updateUI()
        
    }
    
    func updateUI(){
        
        txtSkillCategory.delegate = self
        txtAddSkill.delegate = self
        txtExpFrom.delegate = self
        txtExpTo.delegate = self
        
        viewSlider.delegate = self
        
        if skillCategoryList?.count ?? 0 > 0 {
            
            for model in skillCategoryList ?? [] {
                
                educationOptions.append(model.title ?? "")
            }
            
            txtSkillCategory.pickerOptions = educationOptions
            txtSkillCategory.dropDownPicker?.reloadAllComponents()
            
        }
        
        if skillList?.count ?? 0 > 0 {
            
            for model in skillList ?? [] {
                
                skillOptions.append(model.title ?? "")
            }
            
            txtAddSkill.pickerOptions = skillOptions
            txtAddSkill.dropDownPicker?.reloadAllComponents()
            
        }
        
    }
    
    
    @IBAction func btnAddSkillAction(_ sender: Any) {
        
        if txtSkillCategory?.text?.count == 0 {
            
            txtSkillCategory.showError(message: "required")
            
            return
        }
        
        if txtAddSkill?.text?.count == 0 {
            
            txtAddSkill.showError(message: "required")
            return
        }
        
//        if txtExpTo?.text?.count == 0 {
//
//            txtExpTo.showError(message: "required")
//            return
//        }
//
//        if txtExpFrom?.text?.count == 0 {
//
//            txtExpFrom.showError(message: "required")
//            return
//        }
        
        let model = JobSkillDetailModel()
        
        for item in skillCategoryList ?? [] {
            
            if txtSkillCategory.selectedOption == item.title ?? "" {
                
                model.skillCatCode = item.skillCatCode ?? 0
                model.skillCatTitle = item.title ?? ""
            
            }
            
        }
        
        for item in skillList ?? [] {
            
            if txtAddSkill.selectedOption == item.title ?? "" {
                
                model.skillCode = item.skillCode ?? 0
                model.skillTitle = item.title ?? ""
            }
            
        }
        
        model.expFrom = Int(txtExpFrom.text ?? "0")
        model.expTo = Int(txtExpTo.text ?? "0")
        model.minSkillLevel = skillMinValue
        model.maxSkillLevel = skillMaxValue
        
        skillListDetail.append(model)
        
        delegate?.didAddedSkills(withModel: skillListDetail)
        
        tableViewExpList.reloadData()
        
    }
    
    @objc func deleteSkill(_ sender:UIButton){
        
        if skillListDetail.count > 0 {
            skillListDetail.remove(at: sender.tag)
        }
        
        delegate?.didAddedSkills(withModel: skillListDetail)
        
        tableViewExpList.reloadData()
        
    }
    
    
    
}

extension NewJobSkillsVC: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
            skillMinValue = Int(minValue)
          
            skillMaxValue = Int(maxValue)
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
    
}

extension NewJobSkillsVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return skillListDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell:JobAddSkillCell = tableView.dequeueReusableCell(withIdentifier: "JobAddSkillCell", for: indexPath) as? JobAddSkillCell {
            
            return getCellForSkills(cell: cell, indexPath: indexPath)
        }
        
        return UITableViewCell.init()
    }
    
    func getCellForSkills(cell:JobAddSkillCell,indexPath:IndexPath)->JobAddSkillCell{
        
        if let model:JobSkillDetailModel = skillListDetail[indexPath.row]{
            
            cell.lblTitle.text = model.skillTitle ?? ""
            cell.lblSkillsRange.text = "\(model.minSkillLevel ?? 0)* to \(model.maxSkillLevel ?? 0)*"
            cell.lblExpRange.text = "\(model.expFrom ?? 0) to \(model.expTo ?? 0) years"
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(deleteSkill(_:)), for: .touchUpInside)
            cell.imgViewDelete.setImageColor(color: .white)
            
        }
        
        return cell
    }
    
}

extension NewJobSkillsVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField ==  txtSkillCategory {
            
            txtSkillCategory.showError(message: "")
            
        }
        if textField ==  txtAddSkill {
            
            txtAddSkill.showError(message: "")
            
        }
        
        if textField ==  txtExpFrom {
            
            txtExpFrom.showError(message: "")
            
        }
        
        if textField ==  txtExpTo {
            
            txtExpTo.showError(message: "")
            
        }
        
        return true
    }
}

class JobAddSkillCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle:NKLabel!
    
    @IBOutlet weak var lblSkillsRange:NKLabel!
    
    @IBOutlet weak var lblExpRange:NKLabel!
    
    @IBOutlet weak var viewBg: RCView!
    
    @IBOutlet weak var imgViewDelete: UIImageView!
    
    @IBOutlet weak var btnDelete: UIButton!
    
}
