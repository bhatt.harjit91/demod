//
//  NewUserVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class NewUserVC: AppBaseViewController,CountryCodeSelectViewControllerDelegate {
    
    @IBOutlet weak var txtMobileNo: RCTextField!
    
    @IBOutlet weak var txtCountryCode: RCTextField!
    
    @IBOutlet weak var viewUserDetails: RCView!
    
    @IBOutlet weak var viewUserInfoTitle: RCView!
    
    @IBOutlet weak var viewStatus: RCView!
    
    @IBOutlet weak var btnSearch: NKButton!
    
    @IBOutlet weak var viewFindUserBg: UIView!
    @IBOutlet weak var viewFindUserBgConstant: NSLayoutConstraint!
    
    //    @IBOutlet weak var viewUserFound: RCView!
    
    @IBOutlet weak var heightUserDetail: NSLayoutConstraint!
    
    @IBOutlet weak var txtFirstName: RCTextField!
    
    @IBOutlet weak var txtLastName: RCTextField!
    
    @IBOutlet weak var txtJobTitle: RCTextField!
    
    @IBOutlet weak var txtEmailId: RCTextField!
    
    //    @IBOutlet weak var lblUserName: NKLabel!
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var heightViewResetPassword: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceForResetPassword: NSLayoutConstraint!
    
    @IBOutlet weak var widthBtnSearch: NSLayoutConstraint!
    
    @IBOutlet weak var widthImageViewTick: NSLayoutConstraint!
    
    @IBOutlet weak var lblFindUser: NKLabel!
    
    @IBOutlet weak var viewUserId: RCView!
    
    @IBOutlet weak var heightViewUserId: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceViewUserId: NSLayoutConstraint!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var topSpaceJobTitle: NSLayoutConstraint!
    
    @IBOutlet weak var lblUserId: NKLabel!
    
    @IBOutlet weak var btnResetPassword: NKButton!
    
    @IBOutlet weak var tblreferral: UITableView!
    
    @IBOutlet weak var viewReferralBG: UIView!
    
    @IBOutlet weak var heightOfReferralBgConstraint: NSLayoutConstraint!
    
    var selectedIndexPath: NSInteger = -1
    
    @IBOutlet weak var collectionViewAccessLevel: UICollectionView!
    ///
    @IBOutlet weak var lblAccessLevelTitle: NKLabel!
    
    @IBOutlet weak var imgViewCheckAdmin: UIImageView!
    
    @IBOutlet weak var lblUserAdminTitle: NKLabel!
    
    @IBOutlet weak var txtBusinessEmail: RCTextField!
    
    @IBOutlet weak var heightBusinessEmail: NSLayoutConstraint!
    
    @IBOutlet weak var txtHrEmail: RCTextField!
    
    @IBOutlet weak var heightHrEmail: NSLayoutConstraint!
    
    @IBOutlet weak var txtOfficialEmail: RCTextField!
    
    @IBOutlet weak var heightOfficialMail: NSLayoutConstraint!
    
    var sellerCode:Int = 0
    
    var sellerUserId:Int = 0
    
    var userId:Int = 0
    
    var UserId:Int = 0
    
    var sellerUserDetails:SellerUserDetailModel?
    
    var selectedIndex:Int = 1
    
    var selectedAccess:Int = 1
    
    var statusList:[SellerStatusModel]?
    
    var selectedCountryCode : CountryCodeModel?
    
    var isDetail:Bool = false
    
    var isUserPresent:Int?
    
    weak var delegate:NewSellerUserVCDelegate?
    
    var accessUserTypes:[AccessUserTypesModel]?
    
    let btnName = UIButton()
    
    var isAdmin = 0
    
    let firstAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.darkGray]
    
    let secondAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 1),
        .foregroundColor: UIColor.primaryColor]
    
    var userDetails:[ReferBuyerUserModel]?
    
    var userPresent:Int?
    
    var businessEmailID:String?
    
    var hrEmaildID:String?
    
    var officialEmailId:String?
    
    var isBusinessUser:Int?
    
    var isHRUser:Int?
    
    var isPartnerForCompany:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnResetPassword.setTitle("Send Reset Password Link".localized(), for: .normal)
        
        referralCodeUI()
        
        txtCountryCode.delegate = self
        txtMobileNo.delegate = self
        txtBusinessEmail.delegate = self
        txtHrEmail.delegate = self
        txtOfficialEmail.delegate = self
        txtFirstName.delegate = self
        
        navigationItem.title = "User".localized()
        
        btnName.setTitle("Submit -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(callSubmit), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        lblFindUser.text = "Find by Mobile Number".localized()
        
        txtFirstName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "First Name".localized())
        
        if let currentCountry =  getLocalCountryCode(){
            selectedCountryCode = currentCountry
            
            txtCountryCode.text = currentCountry.dial_code
            
        }
        
        txtMobileNo.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Mobile Number".localized())
        
        txtCountryCode.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
        
    }
    
    
    func referralCodeUI()
    {
        heightOfReferralBgConstraint.constant = 0
        viewReferralBG.isHidden = true
        tblreferral.isHidden = true
        selectedIndexPath = -1
    }
    
    func referralCodeUIUpdate(height:CGFloat)
    {
        heightOfReferralBgConstraint.constant = height
        viewReferralBG.isHidden = false
        tblreferral.isHidden = false
        
        heightUserDetail.constant = CGFloat(height + 40 + 360 + 80)
        
        
        topSpaceJobTitle.constant = 10
        
    }
    
    func updateView() {
        
        viewUserDetails.alpha = 0
        //        viewUserFound.alpha = 0
        viewUserInfoTitle.alpha = 0
        
        viewUserId.alpha = 0
        heightViewUserId.constant = 0
        topSpaceViewUserId.constant = 0
        
        heightUserDetail.constant = 0
        
        heightViewResetPassword.constant = 0
        topSpaceForResetPassword.constant = 0
        btnResetPassword.alpha = 0
        
        btnSubmit.alpha = 0
        btnName.alpha = 0
        
        widthBtnSearch.constant = 70
        widthImageViewTick.constant = 0
        
        selectedAccess = 1
        
        collectionViewAccessLevel.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isDetail {
            
            fetchUserDetails()
            
            btnName.setTitle("Update -->".localized(), for: .normal)
            btnSubmit.setTitle("Update".localized(), for: .normal)
            
            btnName.alpha = 0
            btnSubmit.alpha = 0
            btnResetPassword.alpha = 0
            
            if isAdmin == 1 || isPartnerForCompany == 1 {
                
                btnName.alpha = 1
                btnSubmit.alpha = 1
                btnResetPassword.alpha = 1
                
            }
        }
        else{
            
            updateView()
            
        }
        
    }
    
    func updateUI(){
        
        if isUserPresent == 1 {
            
            
            //            lblUserName.text = userDetails?.displayName
            //            txtFirstName.text = userDetails?.firstName ?? ""
            //            txtLastName.text = userDetails?.lastName ?? ""
            //            txtEmailId.text = userDetails?.emailId ?? ""
            //            txtJobTitle.text = userDetails?.jobTitle ?? ""
            
            //            viewUserFound.alpha = 1
            
            viewUserDetails.alpha = 1
            heightUserDetail.constant = 556
            btnSubmit.alpha = 1
            btnName.alpha = 1
            
            if isDetail {
                btnSubmit.alpha = 0
                btnName.alpha = 0
                
                if isAdmin == 1 || isPartnerForCompany == 1 {
                               
                               btnName.alpha = 1
                               btnSubmit.alpha = 1
                               
                    }
            }
           
            
            viewUserId.alpha = 0
            heightViewUserId.constant = 0
            topSpaceViewUserId.constant = 0
            
            topSpaceJobTitle.constant = 10
            
            txtFirstName.alpha = 0
            txtLastName.alpha = 0
            txtEmailId.alpha = 0
            
            widthBtnSearch.constant = 0
            widthImageViewTick.constant = 40
            
            viewUserInfoTitle.alpha = 1
            
            if userDetails?.count ?? 0 > 0
            {
                if userDetails?.count ?? 0 == 1
                {
                    selectedIndexPath = 0
                    
                    if let model:ReferBuyerUserModel = userDetails?[0] {
                        
                        
                        UserId = model.userId ?? 0
                        
                    }
                    
                }
                let height = userDetails?.count ?? 0
                
                referralCodeUIUpdate(height: CGFloat(height * 62))
                
                tblreferral.reloadData()
            }
            else
            {
                referralCodeUI()
            }
            
        }
        else{
            //            heightUserDetail.constant = 350
            //            viewUserDetails.alpha = 1
            //            viewUserInfoTitle.alpha = 1
            //            widthBtnSearch.constant = 70
            //            widthImageViewTick.constant = 0
            //            viewUserId.alpha = 0
            //            heightViewUserId.constant = 0
            //            topSpaceViewUserId.constant = 0
            //            btnSubmit.alpha = 1
            //            btnName.alpha = 1
            //            topSpaceJobTitle.constant = 24
            //            txtFirstName.alpha = 1
            //            txtLastName.alpha = 1
            //            txtEmailId.alpha = 1
        }
        
    }
    
    func updateDetail() {
        
        viewUserDetails.alpha = 1
        
        heightViewResetPassword.constant = 0
        topSpaceForResetPassword.constant = 0
        btnResetPassword.alpha = 0
        
//        heightViewResetPassword.constant = 44
//        topSpaceForResetPassword.constant = 10
//        btnResetPassword.alpha = 1
        
        if isDetail && (isAdmin == 1 || isPartnerForCompany == 1) {
           
           heightViewResetPassword.constant = 44
            topSpaceForResetPassword.constant = 10
            btnResetPassword.alpha = 1
            
        }
        //        viewUserFound.alpha = 1
        //        lblUserName.text = sellerUserDetails?.displayName
        
        selectedAccess = sellerUserDetails?.accessType ?? 0
        
        
        txtBusinessEmail.text = sellerUserDetails?.officialMailId ?? ""
        
        txtHrEmail.text = sellerUserDetails?.officialMailId ?? ""
        
        txtOfficialEmail.text = sellerUserDetails?.officialMailId ?? ""
        
        
        txtJobTitle.text = sellerUserDetails?.jobTitle ?? ""
        txtMobileNo.text = sellerUserDetails?.mobileNumber ?? ""
        txtCountryCode.text = sellerUserDetails?.mobileIsd ?? ""
        
        viewUserInfoTitle.alpha = 1
        
        if isDetail {
            
            selectedIndexPath = 1
            
            UserId = sellerUserDetails?.sellerUserId ?? 0
        }
        
        let firstText = NSMutableAttributedString(string:"\("User ID  ")",
            attributes: firstAttributes)
        
        let secondText = NSMutableAttributedString(string:"\(sellerUserDetails?.userId ?? "")",
            attributes: secondAttributes)
        
        let combination = NSMutableAttributedString()
        
        combination.append(firstText)
        combination.append(secondText)
        
        lblUserId.attributedText = combination
        
        viewUserDetails.alpha = 1
        heightUserDetail.constant = 556
        viewUserId.alpha = 1
        heightViewUserId.constant = 40
        topSpaceViewUserId.constant = 10
        
        topSpaceJobTitle.constant = 10
        
        if sellerUserDetails?.noteToReferralMember?.count ?? 0 > 0 {
            
            txtViewNotes.placeholder = ""
            
        }
        txtViewNotes.text = sellerUserDetails?.noteToReferralMember ?? ""
        
        btnSubmit.alpha = 1
        btnName.alpha = 1
        
        if isDetail {
            btnSubmit.alpha = 0
            btnName.alpha = 0
            
            if isAdmin == 1 || isPartnerForCompany == 1 {
                           
                           btnName.alpha = 1
                           btnSubmit.alpha = 1
                           
                }
        }
        
        txtFirstName.text = sellerUserDetails?.firstName ?? ""
        txtLastName.text = sellerUserDetails?.lastName ?? ""
        txtEmailId.text = sellerUserDetails?.emailId ?? ""
        txtFirstName.alpha = 1
        txtLastName.alpha = 1
        txtEmailId.alpha = 1
        
        txtMobileNo.isUserInteractionEnabled = false
        
        txtCountryCode.isUserInteractionEnabled = false
        
        widthBtnSearch.constant = 0
        widthImageViewTick.constant = 40
        
        viewFindUserBgConstant.constant = 0
        
        lblFindUser.text = ""
        
        viewFindUserBg.isHidden = true
        
        lblFindUser.isHidden = true
        
        
        txtCountryCode.text = sellerUserDetails?.mobileIsd ?? ""
        txtMobileNo.text = sellerUserDetails?.mobileNumber ?? ""
        
        selectedIndex = sellerUserDetails?.status ?? 0
        
        isAdmin = sellerUserDetails?.isAdmin ?? 0
        
        updateCheckBox(isFalg: isAdmin)
        
        collectionViewStatus.reloadData()
        
        collectionViewAccessLevel.reloadData()
    }
    
    
    func openCountrySelector() {
        let storyBoard = UIStoryboard.init(name: CountryCodeSB, bundle: nil)
        let countrySelector:CountryCodeSelectViewController = storyBoard.instantiateViewController(withIdentifier: "CountryCodeSelectViewController") as! CountryCodeSelectViewController
        countrySelector.selectedCountryCode = selectedCountryCode
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
    }
    
    func didFinishSelectingCountryCode(selectedCountry: CountryCodeModel) {
        selectedCountryCode = selectedCountry
        
        txtCountryCode.text = selectedCountry.dial_code
        
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        if txtCountryCode.text?.count ?? 0 == 0 {
            
            txtCountryCode.showError(message: "Required".localized())
            
            return
            
        }
        
        if txtMobileNo.text?.count ?? 0 == 0 {
            
            txtMobileNo.showError(message: "Required".localized())
            
            return
            
        }
        
        
        
        searchBuyerDetails()
        
    }
    
    @IBAction func btnResetPasswordAction(_ sender: Any) {
        
        
        //        let storyBoard = UIStoryboard.init(name: UserSB, bundle: nil)
        //        let newProductPage:UserResetPasswordViewController = storyBoard.instantiateViewController(withIdentifier: "UserResetPasswordViewController") as! UserResetPasswordViewController
        //        newProductPage.sellerCode = sellerCode
        //        newProductPage.sellerUserId = sellerUserId
        //        navigationController?.pushViewController(newProductPage, animated: true)
        
        alertSendResetPasswordLink(selectedUserId: sellerUserId,selectedSellerCode:sellerCode)
        
    }
    
    func updateCheckBox(isFalg:Int){
        
        if isFalg == 0 {
            
            isAdmin = 0
            
            imgViewCheckAdmin.image = UIImage.init(named: "checkBox")
            
        }
        else{
            
            isAdmin = 1
            
            imgViewCheckAdmin.image = UIImage.init(named: "checkBoxFilled")
        }
        
    }
    
    
    
    @IBAction func btnAdminAction(_ sender: UIButton) {
        
        if isAdmin == 1{
            
            isAdmin = 0
        }
        else {
            
            isAdmin = 1
        }
        
        updateCheckBox(isFalg: isAdmin)
        
        
    }
    
    func alertSendResetPasswordLink(selectedUserId:Int,selectedSellerCode:Int) {
        
        let alert = UIAlertController(title: nil, message: "Are you sure to send the link to User for resetting password?".localized(), preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.btnLinkColor
        
        alert.addAction(UIAlertAction(title: "No".localized(),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            self.sendResetPasswordLink(userId: selectedUserId)
            
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendResetPasswordLink(userId:Int){
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        startAnimatingAfterSubmit()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/sendSetPasswordLink?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&userId=\(userId)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            weakSelf?.stopAnimating()
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        callSubmit()
        
    }
    
    @objc func callSubmit(){
        
        if !isDetail && isUserPresent == 0 {
            
            if txtFirstName.text?.count ?? 0 == 0{
                
                txtFirstName.showError(message: "Required".localized())
                
                return
            }
            
            if selectedAccess == 1 {
                
                if txtBusinessEmail.text?.count == 0 {
                    
                    showErrorMessage(message: "Business Email ID Required".localized())
                    
                    return
                }
                
            }
            else if selectedAccess == 2{
                
                if txtHrEmail.text?.count == 0 {
                    
                    showErrorMessage(message: "HR Email ID Required".localized())
                    
                    
                    return
                }
                
            }
            else if selectedAccess == 3{
                
                if txtOfficialEmail.text?.count == 0 {
                    
                    showErrorMessage(message: "Official Email ID Required".localized())
                    
                    return
                }
                
            }
            
            submitUserDetails()
            
        }
        else
        {
            if selectedAccess == 1 {
                
                if txtBusinessEmail.text?.count == 0 {
                    
                    showErrorMessage(message: "Business Email ID Required".localized())
                    
                    return
                }
                
            }
            else if selectedAccess == 2{
                
                if txtHrEmail.text?.count == 0 {
                    
                    showErrorMessage(message: "HR Email ID Required".localized())
                    
                    return
                }
                
            }
            if selectedAccess == 3{
                
                if txtOfficialEmail.text?.count == 0 {
                    
                    showErrorMessage(message: "Ofiicial Email ID Required".localized())
                    
                    return
                }
                
            }
            
            if selectedIndexPath >= 0
            {
                
                submitUserDetails()
            }
            else
            {
                showErrorMessage(message: "Please select a User".localized())
            }
        }
        
        //submitUserDetails()
        
    }
    
    func fetchUserDetails() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getSellerUserDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&sellerUserId=\(sellerUserId)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(SellerUserDetailApiModel.self, from: data)
                                
                                
                                if let apiResponse:SellerUserDetailModel = jsonResponse.sellerUserDetails {
                                    
                                    weakSelf?.sellerUserDetails = apiResponse
                                    
                                }
                                
                                weakSelf?.updateDetail()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func searchBuyerDetails(){
        
        txtCountryCode.resignFirstResponder()
        txtMobileNo.resignFirstResponder()
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["sellerCode": sellerCode,
                                   "mobileIsd": selectedCountryCode?.dial_code ?? "",
                                   "mobileNumber": txtMobileNo.text ?? ""
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/checkiUser?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                let apiResponse = try decoder.decode(ReferBuyerUserApiModel.self, from: data)
                                
                                if let apiResponse:[ReferBuyerUserModel] = apiResponse.userDetails{
                                    
                                    weakSelf?.userDetails = apiResponse
                                    
                                }
                                if let apiResponse:Int = apiResponse.userPresent{
                                    
                                    weakSelf?.isUserPresent = apiResponse
                                    
                                }
                                
                                if weakSelf?.isUserPresent == 0 {
                                    
                                    weakSelf?.showErrorMessage(message: message)
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                        
                    }
                    
                }
                
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    func submitUserDetails(){
        
        txtCountryCode.resignFirstResponder()
        txtMobileNo.resignFirstResponder()
        txtEmailId.resignFirstResponder()
        txtFirstName.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtJobTitle.resignFirstResponder()
        txtViewNotes.resignFirstResponder()
        
        startAnimatingAfterSubmit()
        
        var mailID = ""
        
        if txtBusinessEmail.text?.count ?? 0 > 0 {
            
            mailID = txtBusinessEmail.text ?? ""
        }
        else if txtHrEmail.text?.count ?? 0 > 0 {
            
            mailID = txtHrEmail.text ?? ""
        }
        else if txtOfficialEmail.text?.count ?? 0 > 0 {
            
            mailID = txtOfficialEmail.text ?? ""
        }
        
        let params:NSDictionary = ["sellerCode": sellerCode,
                                   "sellerUserId":UserId,/////viviviviviviviv
            "firstName":txtFirstName.text ?? "",
            "lastName":txtLastName.text ?? "",
            "jobTitle": txtJobTitle.text ?? "",
            "mobileIsd": txtCountryCode.text ?? "",
            "mobileNumber": txtMobileNo.text ?? "",
            "noteToReferralMember":txtViewNotes.text ?? "",
            "status":selectedIndex,
            "emailId":txtEmailId.text ?? "",
            "accessType":selectedAccess,
            "isAdmin":isAdmin,
            "officialMailId":mailID
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerUsers?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                weakSelf?.delegate?.didAddedUser()
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                weakSelf?.showErrorMessage(message: message)
                weakSelf?.dismiss(animated: true, completion: nil)
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    
}


extension NewUserVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return userDetails?.count ?? 0 > 0 ? userDetails?.count ?? 0 : 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return userDetails?.count ?? 0 > 0 ? 62 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let tableViewCell:ReferralCodeUserCell = tableView.dequeueReusableCell(withIdentifier: "ReferralCodeUserCell", for: indexPath) as? ReferralCodeUserCell
        {
            return getCellForReferralCode(cell: tableViewCell, indexPath: indexPath)
        }
        
        return UITableViewCell.init()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedIndexPath = indexPath.row
        
        
        
        if let model:ReferBuyerUserModel = userDetails?[indexPath.row] {
            
            
            UserId = model.userId ?? 0
            
        }
        
        tblreferral.reloadData()
    }
    
    
    
    func getCellForReferralCode(cell:ReferralCodeUserCell,indexPath: IndexPath) -> ReferralCodeUserCell {
        
        cell.selectionStyle = .none
        
        if indexPath.row == selectedIndexPath {
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
        
        if let model:ReferBuyerUserModel = userDetails?[indexPath.row] {
            
            
            if model.profilePicture?.count ?? 0 > 0{
                
                if let url:URL = URL.init(string: "\(StorageUrl)\(model.profilePicture ?? "")"){
                    cell.imgViewReferralConatct.sd_setImage(with: url, completed: nil)
                }
            }
            
            cell.lblDisplayName.text = model.displayName
            
        }
        
        return cell
    }
    
}


class ReferralCodeUserCell: UITableViewCell {
    
    @IBOutlet weak var lblDisplayName: NKLabel!
    
    @IBOutlet weak var imgViewReferralConatct: RCImageView!
    
}


extension NewUserVC:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField ==  txtCountryCode {
            
            openCountrySelector()
            
            return false;
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtHrEmail{
            
            txtHrEmail.showError(message: "")
            
        }
        
        if textField == txtBusinessEmail{
            
            txtBusinessEmail.showError(message: "")
            
        }
        
        if textField == txtOfficialEmail{
            
            txtOfficialEmail.showError(message: "")
            
        }
        
        if textField == txtFirstName{
                   
            txtFirstName.showError(message: "")
                   
        }
        
        
        if textField ==  txtMobileNo {
            
            referralCodeUI()
            
            updateView()
            
            let maxLength = 14
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return newString.length <= maxLength
            
        }
        
        return true
    }
}

extension NewUserVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus {
            
            return CGSize.init(width: collectionViewStatus.frame.width / 4, height: 40.0)
            
        }
        
        if collectionView == collectionViewAccessLevel {
            
            return CGSize.init(width: collectionViewAccessLevel.frame.width / 3, height: 40.0)
            
        }
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewStatus {
            
            if statusList?.count ?? 0 > 0 {
                
                return statusList?.count ?? 0
            }
            
        }
        if collectionView == collectionViewAccessLevel {
            
            if statusList?.count ?? 0 > 0 {
                
                return accessUserTypes?.count ?? 0
            }
            
            // return 4
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewStatus {
            
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForStatus(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if collectionView == collectionViewAccessLevel {
            
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForAccessLevel(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewStatus {
            
            selectedIndex = indexPath.row
            
            collectionViewStatus.reloadData()
            
        }
        
        if collectionView == collectionViewAccessLevel {
            
            if let model:AccessUserTypesModel = accessUserTypes?[indexPath.row] {
                
                selectedAccess = model.accessType ?? 0
                
                collectionViewAccessLevel.reloadData()
                
            }
            
        }
        
    }
    
    
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:SellerStatusModel = statusList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
            cell.lblTitle.text = model.statusTitle ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
    func getCellForAccessLevel(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:AccessUserTypesModel = accessUserTypes?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
            cell.lblTitle.text = model.title ?? ""
            
            if selectedAccess == model.accessType {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
            if selectedAccess == 1 {
                
                txtBusinessEmail.alpha = 1
                txtHrEmail.alpha = 0
                txtOfficialEmail.alpha = 0
                heightBusinessEmail.constant = 36
                heightHrEmail.constant = 0
                heightOfficialMail.constant = 0
                
            }
            else if selectedAccess == 2{
                
                txtHrEmail.alpha = 1
                txtBusinessEmail.alpha = 0
                txtOfficialEmail.alpha = 0
                heightHrEmail.constant = 36
                heightBusinessEmail.constant = 0
                heightOfficialMail.constant = 0
            }
            else if selectedAccess == 3{
                
                txtOfficialEmail.alpha = 1
                heightOfficialMail.constant = 36
                txtHrEmail.alpha = 0
                txtBusinessEmail.alpha = 0
                heightHrEmail.constant = 0
                heightBusinessEmail.constant = 0
            }
            
        }
        return cell
    }
    
}

