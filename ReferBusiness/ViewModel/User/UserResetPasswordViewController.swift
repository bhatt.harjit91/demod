//
//  UserResetPasswordViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 13/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class UserResetPasswordViewController: AppBaseViewController {

    @IBOutlet weak var txtNewPassword: RCTextField!
    
    @IBOutlet weak var txtConfirmPassword: RCTextField!
    
    var sellerUserId:Int?
    
    var sellerCode:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        navigationItem.title = "Reset Password".localized()
        
        txtNewPassword.delegate = self
        txtConfirmPassword.delegate = self
    }
    

    @IBAction func btnSubmirAction(_ sender: Any) {
        
        if txtNewPassword.text?.count ?? 0 <= 0 {
            
            txtNewPassword.showError(message: "Required")
            
            return
        }
       
        if txtNewPassword.text?.count ?? 0 < 6 {
            txtNewPassword.showError(message: "6 to 24 characters and at least one numeric digit".localized())
            
            return
        }
            
    if txtConfirmPassword.text?.count ?? 0 <= 0 {
            
            txtConfirmPassword.showError(message: "Required")
            
            return
            
        }
        
     if txtNewPassword.text != txtConfirmPassword.text {
            
          //  showErrorMessage(message: "Password does not matches".localized())
            
            (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.showErrorMessage(message: "Password does not matches".localized())
            
            return
        }
            
        else  {
            
           submitUserDetails()
        }
        
    }
    
    
    func submitUserDetails(){
        
       startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["sellerUserId":sellerUserId ?? 0,
                                   "sellerCode":sellerCode ?? 0,
                                   "password": txtNewPassword.text ?? ""
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/sellerPasswordReset?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                weakSelf?.showInfoMessage(message: message)
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                
                 (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.showInfoMessage(message:message)
                
                weakSelf?.navigationController?.popViewController(animated: true)
                
            }
            
            //Show error
            
            (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.showErrorMessage(message:message)
            
        })
        
        
    }
    

}

extension UserResetPasswordViewController:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let txtField:RCTextField = textField as? RCTextField{
            txtField.showError(message: "")
        }
        
        return true
     }
}
