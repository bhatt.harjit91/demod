//
//  UserListVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 12/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol UserListVCDelegate: class {
    func didAddedNewUser()
}

class UserListVC: AppBaseViewController,NewSellerUserVCDelegate {

    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblUserProductCount: NKLabel!
    
 @IBOutlet weak var lblNoData: NKLabel!
    
    var sellerCode:Int = 0
    
    var userCount:Int = 0
    
    var userList:[SellerUserModel]?
    
    var colorCodes:ColorModel?
    
    var statusList:[SellerStatusModel]?
    
    var accessUserTypes:[AccessUserTypesModel]?
    
    weak var delegate: UserListVCDelegate?
    
    var isAdmin:Int?
       
    var isPartnerForCompany:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Users".localized()
        
        let btnName = UIButton()
        //  btnName.setImage(UIImage(named: "walletIcon"), for: .normal)
        btnName.setTitle("New-->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(newUserAction(sender:)), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        lblNoData.text = "No Data Found".localized()
        
        btnName.alpha = 0
                      
        if isAdmin == 1 || isPartnerForCompany == 1 {
                          
          btnName.alpha = 1
         }
        
        fetchUserList()
        
    }
    
    func didAddedUser() {
        fetchUserList()
        delegate?.didAddedNewUser()
    }
    
    @objc func newUserAction(sender: UIButton){
        
      openNewUser()
        
    }
    
    func openNewUser() {
        let storyBoard = UIStoryboard.init(name: UserSB, bundle: nil)
               let newUserPage:NewUserVC = storyBoard.instantiateViewController(withIdentifier: "NewUserVC") as! NewUserVC
               newUserPage.sellerCode = sellerCode
               newUserPage.statusList = statusList ?? []
               newUserPage.accessUserTypes = accessUserTypes ?? []
               newUserPage.isDetail = false
               newUserPage.delegate = self

               navigationController?.pushViewController(newUserPage, animated: true)
    }
    
    

    func fetchUserList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/sellerUserList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        do {
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
                            print(dataObj)
                            
                            let jsonResponse = try decoder.decode(SellerUserApiModel.self, from: data)
                            
                            if let apiResponse:[SellerUserModel] = jsonResponse.sellerUserList {
                                
                              weakSelf?.userList = apiResponse
                                
                            }
                            if let apiResponse:Int = jsonResponse.count {
                                
                                weakSelf?.userCount = apiResponse
                                
                                weakSelf?.lblUserProductCount.text = ("\("This seller has ".localized())\(weakSelf?.userCount ?? 0)\(" users")")
                            }
                            
                            if  weakSelf?.userList?.count ?? 0 == 0 {
                                                           
                                weakSelf?.openNewUser()
                                                           
                                }

                            
                            weakSelf?.tableViewList.reloadData()
                            
                            return
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                            
                        }
                      }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }

}

extension UserListVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = userList?.count ?? 0
        
        if count > 0 {
            
            tableViewList.alpha = 1
            
            return userList?.count ?? 0
            
        }
            
        else{
            
            tableViewList.alpha = 0
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:UserListCell = tableView.dequeueReusableCell(withIdentifier: "UserListCell", for: indexPath) as? UserListCell
        {
            
            return getCellForUserList(cell: cell, indexPath: indexPath)
            
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model:SellerUserModel = userList?[indexPath.row]
        {
         let storyBoard = UIStoryboard.init(name: UserSB, bundle: nil)
         let newUserPage:NewUserVC = storyBoard.instantiateViewController(withIdentifier: "NewUserVC") as! NewUserVC
         newUserPage.sellerCode = sellerCode
         newUserPage.sellerUserId = model.sellerUserId ?? 0
            
         newUserPage.statusList = statusList ?? []
         newUserPage.accessUserTypes = accessUserTypes ?? []
         newUserPage.delegate = self
         newUserPage.isDetail = true
         newUserPage.isAdmin = isAdmin ?? 0
         newUserPage.isPartnerForCompany = isPartnerForCompany
         navigationController?.pushViewController(newUserPage, animated: true)
        }
    }
    
    func getCellForUserList(cell:UserListCell,indexPath:IndexPath) -> UserListCell {
        
        cell.selectionStyle = .none
        
        if let model:SellerUserModel = userList?[indexPath.row]
        {
            cell.lblProductNameText.text = model.displayName ?? ""
            
            cell.lblStatusText.text = model.statusTitle ?? ""
            
            if model.status == 0 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString:"#42A5F5")
            }
            
            if model.status == 1 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.activeColor ?? "")
            }
                
            else if model.status == 2 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.inactiveColor ?? "")
            }
                
            else if model.status == 3 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.blacklistedColor ?? "")
            }
            
        }

       
        return cell
    }
    
}

class UserListCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var lblProductNameText: NKLabel!
    
    @IBOutlet weak var lblStatusText: NKLabel!
    
    @IBOutlet weak var viewStatus: RCView!
    
    @IBOutlet weak var imgViewUser: RCImageView!
}
