//
//  NewCustomerVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 10/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol NewCustomerVCDelegate: class {
    
    func didAddedCustomer()
}

class NewCustomerVC: SendOTPViewController {
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var lblStatusTitle: NKLabel!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var btnLogo: NKButton!
    
    @IBOutlet weak var txtCustomerName: RCTextField!
    
    @IBOutlet weak var txtViewDes: NKTextView!
    
    @IBOutlet weak var txtWebsite: RCTextField!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    var statusList:[SellerStatusModel]?
    
    var sellerCode:Int = 0
    
    var isDetail:Bool = false
    
    var selectedIndex:Int = 0
    
    var strLogo:String?
    
    var strLogoName:String?
    
    var customerCode:Int = 0
    
    var sellerCustomerDetails:CustomerModel?
    
    weak var delegate:NewCustomerVCDelegate?
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    var btnName = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLogo.addTarget(self, action: #selector(btnLogoAction), for: .touchUpInside)
        
        btnSubmit.addTarget(self, action: #selector(btnSubmitAction), for: .touchUpInside)
        
        txtCustomerName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Customer Name".localized())
        
        txtViewDes.changePlaceHolderColor = true
        txtViewDes.placeholder = "Description".localized()
        
        txtWebsite.delegate = self
        txtViewDes.delegate = self
        txtCustomerName.delegate = self
        
        updateNavBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isDetail {
            
            fetchCustomerDetail()
            
        }
        
    }
    
    
    func updateNavBar() {
        let btnName = UIButton()
        
        if isDetail {
            btnName.alpha = 0
            btnSubmit.alpha = 0
            
            btnName.setTitle("Update -->".localized(), for: .normal)
            btnSubmit.setTitle("Update".localized(), for: .normal)
            
            if isAdmin == 1 || isPartnerForCompany == 1 {
                
                btnName.alpha = 1
                btnSubmit.alpha = 1
                
            }
        }
        else{
            btnName.setTitle("Submit -->".localized(), for: .normal)
            btnSubmit.setTitle("Submit".localized(), for: .normal)
        }
        
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(btnSubmitAction), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        navigationItem.title = "Customer".localized()
        
    }
    
    func updateUI() {
        
        if let model:CustomerModel = sellerCustomerDetails {
            
            txtCustomerName.text = model.customerName ?? ""
            txtWebsite.text = model.website ?? ""
            txtViewDes.text = model.description ?? ""
            
            if model.description?.count ?? 0 > 0 {
                txtViewDes.placeholder = ""
            }
            strLogo = model.logo ?? ""
            strLogoName = model.logoName ?? ""
            if strLogo?.count ?? 0 > 0 {
                imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(strLogo ?? "")")), completed: nil)
            }
            
            if model.status == 1 {
                
                selectedIndex = 0
                
            }
            if model.status == 2 {
                
                selectedIndex = 1
                
            }
            if model.status == 3 {
                
                selectedIndex = 2
                
            }
            
            collectionViewStatus.reloadData()
            
        }
    }
    
    @objc func btnLogoAction() {
        
        weak var weakSelf = self
        
        isAttachment = true
        
        openImagePicker()
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            //     weakSelf?.stopAnimating()
            if status {
                print(urlString, imageName)
                weakSelf?.strLogo = urlString
                weakSelf?.strLogoName = imageName
                if weakSelf?.strLogo?.count ?? 0 > 0 {
                    self.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(weakSelf?.strLogo ?? "")")), completed: nil)
                    
                    self.isAttachment = false
                    
                }
                
            }
            
        }
    }
    
    @objc func btnSubmitAction() {
        
        if txtCustomerName.text?.count ?? 0 == 0 {
            
            txtCustomerName.showError(message: "Required".localized())
            
            return
        }
        
        if txtViewDes.text?.count ?? 0 == 0 {
            
            txtViewDes.showError(message: "Required".localized())
            
            return
        }
        
        submitCustomerDetails()
        
    }
    
    func fetchCustomerDetail() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getSellerCustomerDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&customerCode=\(customerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(CustomerDetailApiModel.self, from: data)
                                //
                                if let apiResponse:CustomerModel = jsonResponse.sellerCustomerDetails {
                                    //
                                    weakSelf?.sellerCustomerDetails = apiResponse
                                    //
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
    func submitCustomerDetails(){
        
        let status:Int  = statusList?[selectedIndex].status ?? 0
        
        startAnimating()
        let params:NSDictionary = ["customerCode":customerCode,
                                   "sellerCode": sellerCode,
                                   "customerName":txtCustomerName.text ?? "",
                                   "description":txtViewDes.text ?? "",
                                   "website":txtWebsite.text ?? "",
                                   "status": status,
                                   "logo":strLogo ?? "",
                                   "logoName":strLogoName ?? ""
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerCustomer?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                weakSelf?.delegate?.didAddedCustomer()
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                weakSelf?.showErrorMessage(message: message)
                weakSelf?.dismiss(animated: true, completion: nil)
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    
}

extension NewCustomerVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            return CGSize.init(width: collectionViewStatus.frame.width / 3, height: 40.0)
            
        }
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if statusList?.count ?? 0 > 0 {
            
            return statusList?.count ?? 0
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
            
            return getCellForStatus(cell: cell, indexPath: indexPath)
            
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        selectedIndex = indexPath.row
        
        collectionViewStatus.reloadData()
        
    }
    
    
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:SellerStatusModel = statusList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
            cell.lblTitle.text = model.statusTitle ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
}

extension NewCustomerVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let txtField:RCTextField = textField as? RCTextField{
            txtField.showError(message: "")
        }
        
        return true
    }
    
}
extension NewCustomerVC:UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if let textView:NKTextView = textView as? NKTextView{
            textView.showError(message: "")
        }
        
        return true
    }
    
}
