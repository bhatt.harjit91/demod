//
//  CustomerListVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 10/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol CustomerListVCDelegate: class {
    func didCalledCustomer()
    
}

class CustomerListVC: AppBaseViewController{

    @IBOutlet weak var tableViewList: UITableView!
       
    @IBOutlet weak var lblUserProductCount: NKLabel!
       
    @IBOutlet weak var lblNoData: NKLabel!
    
    var sellerCode:Int = 0
       
    var userCount:Int = 0
       
    var customerList:[CustomerModel]?
       
    var colorCodes:ColorModel?
       
    var statusList:[SellerStatusModel]?
    
     var isDetail:Bool = false
       
    weak var delegate: CustomerListVCDelegate?
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
              
              lblNoData.text = "No Data Found".localized()
        
              updateNavBar()
              
              fetchCustomerList()
        
    }
    
    func updateNavBar() {
        let btnName = UIButton()
                   
        btnName.setTitle("New -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(newCustomerAction(sender:)), for: .touchUpInside)
                    
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        navigationItem.title = "Customers".localized()
        
        btnName.alpha = 0
                      
        if isAdmin == 1 || isPartnerForCompany == 1 {
                          
          btnName.alpha = 1
         }
    }
    
    @objc func newCustomerAction(sender: UIButton){
          
         openCustomer()
          
      }
    
    func openCustomer(){
        
        let storyBoard = UIStoryboard.init(name: CustomerSB, bundle: nil)
                let newCustomerPage:NewCustomerVC = storyBoard.instantiateViewController(withIdentifier: "NewCustomerVC") as! NewCustomerVC
                newCustomerPage.sellerCode = sellerCode
                newCustomerPage.statusList = statusList ?? []
                newCustomerPage.isDetail = false
                newCustomerPage.delegate = self

                navigationController?.pushViewController(newCustomerPage, animated: true)
        
    }
    
func fetchCustomerList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getSellerCustomerList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        do {
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
                            print(dataObj)
                            
                            let jsonResponse = try decoder.decode(CustomerApiModel.self, from: data)
                            
                            if let apiResponse:[CustomerModel] = jsonResponse.sellerCustomerList {
                                
                              weakSelf?.customerList = apiResponse
                                
                            }
                            if let apiResponse:[SellerStatusModel] = jsonResponse.statusList {
                                                          
                            weakSelf?.statusList = apiResponse
                                                          
                                                      }
                    
                            if let apiResponse:Int = jsonResponse.count {
                                
                                weakSelf?.userCount = apiResponse
                                
                                weakSelf?.lblUserProductCount.text = ("\("This seller has ".localized())\(weakSelf?.userCount ?? 0)\(" Customers")")
                            }
                            
                            if weakSelf?.customerList?.count ?? 0 == 0 {
                            
                                weakSelf?.openCustomer()
                                
                            }
                            
                            weakSelf?.tableViewList.reloadData()
                            
                            return
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                            
                        }
                      }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
  
}

extension CustomerListVC:NewCustomerVCDelegate{
    
  func didAddedCustomer(){
        
       fetchCustomerList()
    
      delegate?.didCalledCustomer()
    
    }
}

extension CustomerListVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = customerList?.count ?? 0
        
        if count > 0 {
            
            tableViewList.alpha = 1
            
            return customerList?.count ?? 0
            
        }
            
        else{
            
            tableViewList.alpha = 0
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:CustomerListCell = tableView.dequeueReusableCell(withIdentifier: "CustomerListCell", for: indexPath) as? CustomerListCell
        {
            
            return getCellForCustomerList(cell: cell, indexPath: indexPath)
            
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model:CustomerModel = customerList?[indexPath.row]
        {
         let storyBoard = UIStoryboard.init(name: CustomerSB, bundle: nil)
         let newUserPage:NewCustomerVC = storyBoard.instantiateViewController(withIdentifier: "NewCustomerVC") as! NewCustomerVC
         newUserPage.sellerCode = sellerCode
         newUserPage.customerCode = model.customerCode ?? 0
            
         newUserPage.statusList = statusList ?? []
         newUserPage.delegate = self
         newUserPage.sellerCustomerDetails = model
         newUserPage.isDetail = true
         newUserPage.isAdmin = isAdmin
         newUserPage.isAdmin = isPartnerForCompany
         navigationController?.pushViewController(newUserPage, animated: true)
        }
    }
    
    func getCellForCustomerList(cell:CustomerListCell,indexPath:IndexPath) -> CustomerListCell {
        
        cell.selectionStyle = .none
        
        if let model:CustomerModel = customerList?[indexPath.row]
        {
            cell.lblCustomerName.text = model.customerName ?? ""

            cell.lblStatusText.text = model.statusTitle ?? ""
            
            if model.logo?.count ?? 0 > 0 {
                cell.imgViewUser.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.logo ?? "")")), completed: nil)
                     }
            
        if model.status == 1 {
                cell.viewStatus.backgroundColor =   UIColor.colorWithHexString(hexString: colorCodes?.activeColor ?? "")
        }
                
        else if model.status == 2 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.inactiveColor ?? "")
        }
                
        else if model.status == 3 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.blacklistedColor ?? "")
        }
            
        else {
                    cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString:"#42A5F5")
        }
            
    }

       
        return cell
    }
    
}

class CustomerListCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
       
    @IBOutlet weak var lblCustomerName: NKLabel!
       
    @IBOutlet weak var lblStatusText: NKLabel!
       
    @IBOutlet weak var viewStatus: RCView!
       
    @IBOutlet weak var imgViewUser: RCImageView!
}
