//
//  CountryCodeSelectViewController.swift
//  NearKart
//
//  Created by RaviKiran B on 07/09/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit

protocol CountryCodeSelectViewControllerDelegate: class {
    func didFinishSelectingCountryCode(selectedCountry:CountryCodeModel)
}

class CountryCodeSelectViewController: AppBaseViewController,UISearchResultsUpdating,UISearchControllerDelegate,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tblView: UITableView!
    var countryList:[CountryCodeModel] = []
    var searchResult:[CountryCodeModel] = []
    var searchController: UISearchController!
    var selectedCountryCode : CountryCodeModel?
    
    weak var delegate : CountryCodeSelectViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        countryList =  getCountryList()
        searchResult = countryList
        setupNavBar()
        
       navigationController?.navigationBar.barTintColor = .primaryColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupNavBar(){
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.searchBar.tintColor = UIColor.primaryColor
        searchController.searchBar.sizeToFit()
        searchController.searchBar.autocapitalizationType = .none
        //searchController.searchBar.barTintColor = UIColor.primaryColor
        searchController.searchBar.placeholder = "Search".localized()
        if #available(iOS 11.0, *) {
            searchController.hidesNavigationBarDuringPresentation = false
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
            
            searchController.searchBar.setTextFieldColor(color: UIColor.primaryColor)
        } else {
            // Fallback on earlier versions
            
            searchController.searchBar.delegate = self
            tblView.tableHeaderView = searchController.searchBar
            
        }
        
        
        searchController.searchBar.localizeSearchBar()
        searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = false // default is YES
        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */
        definesPresentationContext = true
        
    }
    
    
    @IBAction func closeSelection(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    //MARK: SearchUpdater
    func updateSearchResults(for searchController: UISearchController) {
        let searchText:String = searchController.searchBar.text!
        filterCountry(searchText: searchText)
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterCountry(searchText: searchText)
    }
    func filterCountry(searchText:String) {
        if searchText.count>0{
            searchResult = countryList.filter({( country : CountryCodeModel) -> Bool in
                let locale = Locale(identifier: Localize.currentLanguage)
                let countryName = locale.localizedString(forRegionCode: country.code)
                return country.name.lowercased().contains(searchText.lowercased()) ||  country.dial_code.lowercased().contains(searchText.lowercased()) || (countryName?.lowercased().contains(searchText.lowercased()))!
            })
        }
        else{
            searchResult = countryList
        }
        
        tblView.reloadData()
    }
    //MARK: TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell")
        let countryModel:CountryCodeModel = searchResult[indexPath.row]
        
        let locale = Locale(identifier: Localize.currentLanguage)
        let countryName = locale.localizedString(forRegionCode: countryModel.code)
        cell?.textLabel?.text = countryName
        cell?.detailTextLabel?.text = countryModel.dial_code
        cell?.accessoryType = .none
        if countryModel.dial_code == selectedCountryCode?.dial_code {
            cell?.accessoryType = .checkmark
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let countryModel:CountryCodeModel = searchResult[indexPath.row]
        delegate?.didFinishSelectingCountryCode(selectedCountry: countryModel)
        navigationController?.popViewController(animated: true)
    }
    
}
