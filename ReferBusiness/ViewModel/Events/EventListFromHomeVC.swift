//
//  EventListFromHomeVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 19/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class EventListFromHomeVC: AppBaseViewController {

    @IBOutlet weak var lblNoEventsMsg: NKLabel!
       
       @IBOutlet weak var viewNoEventsBG: UIView!
       
       @IBOutlet weak var lblProductCount:NKLabel!
       
       @IBOutlet weak var tableViewList: UITableView!
       
       var sellerCode:Int = 0
       
       var statusList:[SellerStatusModel]?
       
       var productList:[ProductModel]?
       
       var colorCodes:ColorModel?
       
       var responseTypes:[ResponseTypeModel]?
       
       var currency:[CurrencyModel]?
       
       var branchList:[ProductBranchModel]?
       
       var attachmentTypes:[AttachmentTypeModel]?
       
       var buttonTypeList:[ButtonTypeMasterModel]?
       
       var buttonTemplateList:[ButtonTemplateMasterModel]?
       
       var paymentCollectTypeList:[PaymentCollectTypeMasterModel]?
       
       var cancellationTemplateList:[CancellationTemplateMasterModel]?
       
       var taxTemplateList:[TaxTemplateMasterModel]?
       
       var isAdmin:Int?
       
       var isPartnerForCompany:Int?
       
       var companyType:Int = 1
       
       var productCount:Int = 0
    
       var productCode:Int = 0
    
       var filterMasterList:[FilterReferralModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblNoEventsMsg.text = "No Events Available".localized()
        
        navigationItem.title = "Events".localized()
               
               fetchJobList()
        
    }
    
    func fetchJobList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/productList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&productType=\(2)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ProductApiModel.self, from: data)
                                
                                /// vivek
                                
                                if let apiResponse:[AttachmentTypeModel] = jsonResponse.attachmentTypes {
                                    
                                    weakSelf?.attachmentTypes = apiResponse
                                    
                                }
                                
                                if let apiResponse:[ProductModel] = jsonResponse.productList {
                                    
                                    weakSelf?.productList = apiResponse
                                    
                                }
                                //                                if let apiResponse:Int = jsonResponse.count {
                                //
                                //                                    weakSelf?.productCount = apiResponse
                                //
                                //                                    weakSelf?.lblProductCount.text = ("\("This seller has ".localized())\(weakSelf?.productCount ?? 0)\(" events")")
                                //                                }
                                //
                                if let apiResponse:[ResponseTypeModel] = jsonResponse.responseTypes {
                                    
                                    weakSelf?.responseTypes = apiResponse
                                    
                                }
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currency {
                                    
                                    weakSelf?.currency = apiResponse
                                    
                                }
                                
                                if let apiResponse:[ProductBranchModel] = jsonResponse.branchList {
                                    
                                    weakSelf?.branchList = apiResponse
                                    
                                }
                                if let apiResponse:Int = jsonResponse.count {
                                                                   
                                    weakSelf?.lblProductCount.text = ("Events (\(apiResponse))")
                                                                   
                                }
                                if let apiResponse:[ButtonTypeMasterModel] = jsonResponse.buttonTypeList {
                                    weakSelf?.buttonTypeList = apiResponse
                                }
                                
                                if let apiResponse:[ButtonTemplateMasterModel] = jsonResponse.buttonTemplateList {
                                    weakSelf?.buttonTemplateList = apiResponse
                                }
                                
                                if let apiResponse:[PaymentCollectTypeMasterModel] = jsonResponse.paymentCollectTypeList {
                                    
                                    weakSelf?.paymentCollectTypeList = apiResponse
                                    
                                }
                                if let apiResponse:[CancellationTemplateMasterModel] = jsonResponse.cancellationTemplateList {
                                    
                                    weakSelf?.cancellationTemplateList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[TaxTemplateMasterModel] = jsonResponse.taxTemplateList {
                                    
                                    weakSelf?.taxTemplateList = apiResponse
                                    
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }

}


extension EventListFromHomeVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return productList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:ProductListCell = tableView.dequeueReusableCell(withIdentifier: "ProductListCell", for: indexPath) as? ProductListCell {
            
            return getCellForProductList(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
        
        
    }
    
    
    func getCellForProductList(cell:ProductListCell,indexPath:IndexPath) -> ProductListCell {
        
        cell.selectionStyle = .none
        
        if let model:ProductModel = productList?[indexPath.row]
        {
            cell.lblProductNameText.text = model.productName ?? ""
            
            cell.lblStatusText.text = "\(model.transactionCount ?? 0) \("Members".localized())"
            
            cell.imageView?.image = nil
            
            if model.productLogo?.count ?? 0 > 0 {
                cell.imgViewProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
            }
            else{
                cell.imgViewProduct.image = UIImage.init(named: "eventIcon")
            }
            
//            if model.status == 1 {
//                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.activeColor ?? "")
//            }
//
//            else if model.status == 2 {
//                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.inactiveColor ?? "")
//            }
//
//            else if model.status == 3 {
//                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.blacklistedColor ?? "")
//            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:ProductModel = productList?[indexPath.row]
        {
            

         let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
         let referalLeadsPage:ReferalLeadsVC = storyBoard.instantiateViewController(withIdentifier: "ReferalLeadsVC") as!   ReferalLeadsVC
         //
         referalLeadsPage.sellerCode = model.sellerCode ?? 0
            referalLeadsPage.productCode = model.productCode ?? 0
         referalLeadsPage.progressValue = "-1"
         referalLeadsPage.isFromHomeNew = true
         referalLeadsPage.isFromHome = true
         referalLeadsPage.strTitle = "Referrals Leads".localized()
         referalLeadsPage.filterMasterList = filterMasterList ?? []
         navigationController?.pushViewController(referalLeadsPage, animated: true)
                
            }
            
        }
    }

