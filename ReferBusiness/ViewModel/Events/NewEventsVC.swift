//
//  NewEventsVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 06/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

protocol NewEventsVCDelegate: class {
    func didAddedEvent()
}

class NewEventsVC:SendOTPViewController,CurrencySelectorVCDelegate {
    
    @IBOutlet weak var txtProductName: RCTextField!
    
    @IBOutlet weak var txtProductCode: RCTextField!
    
    @IBOutlet weak var lblEventStartDate: NKLabel!
    
    @IBOutlet weak var txtViewProductDes: NKTextView!
    
    @IBOutlet weak var lblEventEndDate: NKLabel!
    
    @IBOutlet weak var txtStartDate: RCTextField!
    
    @IBOutlet weak var txtEndDate: RCTextField!
    
    @IBOutlet weak var lblVisibleUpto: NKLabel!
    
    @IBOutlet weak var txtVisibleUpto: RCTextField!
    
    @IBOutlet weak var lblBranchestext: NKLabel!
    
    @IBOutlet weak var lblBannersTitle: NKLabel!
    
    @IBOutlet weak var collectionViewAttachments: UICollectionView!
    
    @IBOutlet weak var lblAttachmentVideoTitle: NKLabel!
    
    @IBOutlet weak var lblVideoTitle: NKLabel!
    
    @IBOutlet weak var imgViewRadioAttachment: UIImageView!
    
    @IBOutlet weak var imgViewVideo: UIImageView!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var txtVideos: RCTextField!
    
    @IBOutlet weak var viewPrice: RCView!
    
    @IBOutlet weak var lblPriceType: NKLabel!
    
    @IBOutlet weak var viewFixedPriceType: RCView!
    
    @IBOutlet weak var heightViewFixedPrice: NSLayoutConstraint!
    
    @IBOutlet weak var lblFixedPriceType: NKLabel!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var txtFixedCurrencyTitle: RCTextField!
    
    @IBOutlet weak var txtFixedAmount: RCTextField!
    
    @IBOutlet weak var viewRange: RCView!
    
    @IBOutlet weak var lblRangeTitle: NKLabel!
    
    @IBOutlet weak var txtRangeCurrencyTitle: RCTextField!
    
    @IBOutlet weak var txtStartRange: RCTextField!
    
    @IBOutlet weak var txtEndRange: RCTextField!
    
    @IBOutlet weak var heightViewRange: NSLayoutConstraint!
    
    @IBOutlet weak var viewGuideline: RCView!
    
    @IBOutlet weak var heightViewGuideline: NSLayoutConstraint!
    
    @IBOutlet weak var viewReferalFee: RCView!
    
    @IBOutlet weak var viewReferralFixedPrice: RCView!
    
    @IBOutlet weak var heightViewReferralFixedPrice: NSLayoutConstraint!
    
    @IBOutlet weak var viewReferralPercentage: RCView!
    
    @IBOutlet weak var heightViewReferralPercentage: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewBranchList: UITableView!
    
    @IBOutlet weak var heightViewBranchList: NSLayoutConstraint!
    
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
    @IBOutlet weak var txtKeywords: RCTextField!
    
    @IBOutlet weak var topSpaceForFixedPrice: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceForReferralFixedPrice: NSLayoutConstraint!
    
    
    @IBOutlet weak var txtGuidelineCurrency: RCTextField!
    
    @IBOutlet weak var txtGuidelinePrice: RCTextField!
    
    @IBOutlet weak var txtReferralCurrency: RCTextField!
    
    @IBOutlet weak var txtReferralPrice: RCTextField!
    
    @IBOutlet weak var txtReferralPercentage: RCTextField!
    
    @IBOutlet weak var segmentPriceType: UISegmentedControl!
    
    @IBOutlet weak var segmentReferralType: UISegmentedControl!
    
    @IBOutlet weak var imgViewAttachementBtn: UIImageView!
    
    @IBOutlet weak var imgCheckBoxPriceType: UIImageView!
    
    @IBOutlet weak var imgCheckBoxReferralType: UIImageView!
    
    @IBOutlet weak var heightViewPriceType: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewReferralType: NSLayoutConstraint!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var txtViewPricing: NKTextView!
    
    ////-->>>  New Changes
    
    @IBOutlet weak var btnChangeLog: UIButton!
    
    @IBOutlet weak var heightOfBtnChangeLog: NSLayoutConstraint!
    
    @IBOutlet weak var lblSpecialMsgPartner: NKLabel!
    
    @IBOutlet weak var txtViewMsgPartner: NKTextView!
    
    @IBOutlet weak var lblSpecialMsgUser: NKLabel!
    
    @IBOutlet weak var txtViewMsgUser: NKTextView!
    
    @IBOutlet weak var txtBtnTemplate: RCTextField!
    
    @IBOutlet weak var txtBtnLabelTemplate: RCTextField!
    
    @IBOutlet weak var txtPaymentTemplate: RCTextField!
    
    @IBOutlet weak var txtTaxTemplate: RCTextField!
    
    @IBOutlet weak var txtCancellationTemplate: RCTextField!
    
    @IBOutlet weak var heightAttachmentVideo: NSLayoutConstraint!
    
    @IBOutlet weak var viewAttachmentVideo: RCView!
    
    @IBOutlet weak var heightCollectionAttachments: NSLayoutConstraint!
    
    @IBOutlet weak var viewAttachIcon: RCView!
    
    @IBOutlet weak var heightVideoImage: NSLayoutConstraint!
    
    @IBOutlet weak var viewVideoImageBg: UIView!
    
    @IBOutlet weak var heightTxtVideos: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewThumbnail: UIImageView!
    
    @IBOutlet weak var collectionViewAttachment: UICollectionView!
    
    @IBOutlet weak var imgCheckBoxFree: UIImageView!
    
    @IBOutlet weak var imgCheckBoxRegistration: UIImageView!
    
    @IBOutlet weak var imgCheckBoxEvent: UIImageView!
    
    @IBOutlet weak var viewExtraCharge: RCView!
    
    @IBOutlet weak var heightPaymentType: NSLayoutConstraint!
    
    @IBOutlet weak var txtExtraChargeCountryCode: RCTextField!
    
    @IBOutlet weak var txtExtraChargeAmount: RCTextField!
    
    @IBOutlet weak var imgAttachment: UIImageView!
    
    weak var delegate:NewEventsVCDelegate?
    
    var attachmentTypes:[AttachmentTypeModel]?
    
    var bannerType:Int = 0
    
    ////
    
    var sellerCode:Int = 0
    
    var productCode:Int = 0
    
    var attachments:[Any] = []
    
    var productDetail:ProductDetailModel?
    
    var responseTypes:[ResponseTypeModel]?
    
    var currency:[CurrencyModel]?
    
    var branchList:[ProductBranchModel]?
    
    var statusList:[SellerStatusModel]?
    
    var giftList:[ProductGiftModel]?
    
    var giftCode:[Int] = []
    
    var isSelected:Int = -1
    
    var branchCode:[Int] = []
    
    var strLogo:String?
    
    var thumbnailLogo:String?
    
    var selectedIndex:Int = 1
    
    var selectedCurrencyCode : CurrencyModel?
    
    var activeTextField = UITextField()
    
    var isPriceTypeSelected:Int = 0
    
    var isReferralTypeSelected:Int = 0
    
    var isLoyaltyTypeSelected:Int = 0
    
    var isCharityTypeSelected:Int = 0
    
    var isDetail:Bool = false
    
    var btnName = UIButton()
    
    var referralValue = 0.0
    var charityValue = 0.0
    var loyaltyValue = 0.0
    
    var priceType:Int = 0
    var charityType:Int = 0
    var loyaltyType:Int = 0
    var referralFeeType:Int = 0
    
    var avgRECurrencyId:Int?
    var productCurrencyId:Int?
    var referralCurrencyId:Int?
    var charityCurrencyId:Int?
    var loyaltyCurrencyId:Int?
    var extraCurrencyId:Int?
    
    var minPrice = 0.0
    var maxPrice = 0.0
    
    var isReferralAttachmentVisible:Int = 1
    
    var isVideo:Int = 0
    
    let segmentTitleAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.white]
    
    let countAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 8),
        .foregroundColor: UIColor.getGradientColor(index: 0).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let desAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 8),
        .foregroundColor: UIColor.btnLinkColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    var companyType:Int = 1
    
    var isAppointmentSelected:Int = 0
    
    var btnTemplateOptions:[String] = []
    
    var btnLabelTemplateOptions:[String] = []
    
    var cancelTemplateOptions:[String] = []
    
    var paymentTemplateOptions:[String] = []
    
    var taxTemplateOptions:[String] = []
    
    var buttonTypeList:[ButtonTypeMasterModel]?
    var buttonTemplateList:[ButtonTemplateMasterModel]?
    var paymentCollectTypeList:[PaymentCollectTypeMasterModel]?
    var cancellationTemplateList:[CancellationTemplateMasterModel]?
    var taxTemplateList:[TaxTemplateMasterModel]?
    
    var buttonTypeCode:Int?
    var buttonTemplateCode:Int?
    var paymentCollectTypeCode:Int?
    var cancellationTemplateCode:Int?
    var taxTemplateCode:Int?
    
    var thumbnailYoutube:String?
    
    var attachmentsProduct:[AttachmentBannerTypeModel] = []
    
    var isFree:Int = 0
    var isPayRegistration:Int = 0
    var isPayEvent:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "New Event".localized()
        
         
        viewLoad()
    }
    
    func viewLoad(){
        
        txtStartDate.tag = 11
        txtEndDate.tag = 22
        txtStartDate.RCDelegate = self
        txtEndDate.RCDelegate = self
        txtVideos.RCDelegate = self
        txtVisibleUpto.RCDelegate = self
        txtExtraChargeCountryCode.delegate = self
        //updateSegmentUI()
        
        updateAttachmentView(isVideoValue:0)
        
        txtProductName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Event name".localized())
        
        // txtViewProductDes.changePlaceHolderColor = true
        txtViewProductDes.placeholder = "Event description".localized()
        
        // lblBannersTitle.attributedText = addPlaceHolderWithText(placeholder: "Banners".localized())
        
        lblBannersTitle.text = "Banners".localized()
        
        lblBranchestext.attributedText = addPlaceHolderWithText(placeholder: "Branches".localized())
        
        //////////
        
        txtFixedCurrencyTitle.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
        
        txtFixedAmount.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        txtRangeCurrencyTitle.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
        
        txtStartRange.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        txtEndRange.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        txtGuidelineCurrency.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
        
        txtGuidelinePrice.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        //               txtCharityCurrency.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
        //
        //               txtCharityPrice.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        //               txtLoyaltyCurrency.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
        //
        //               txtLoyaltyPrice.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        //
        //               txtLoyaltyPercentage.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.0".localized())
        
        txtReferralCurrency.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
        
        txtReferralPrice.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        txtReferralPercentage.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
        
        txtViewProductDes.placeholder = "Product description".localized()
        
        ////////
        
        
        //  btnName.setImage(UIImage(named: "walletIcon"), for: .normal)
        btnName.setTitle("Submit -->", for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(btnSubmitAction), for: .touchUpInside)
        
        //   btnName.addTarget(self, action: #selector(callSubmit), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        heightViewFixedPrice.constant = 0
        heightViewRange.constant = 0
        heightViewGuideline.constant = 0
        heightViewReferralFixedPrice.constant = 0
        heightViewReferralPercentage.constant = 0
        
        
        heightViewPriceType.constant = 40
        heightViewReferralType.constant = 40
        
        
        txtRangeCurrencyTitle.delegate = self
        txtFixedCurrencyTitle.delegate = self
        
        txtReferralCurrency.delegate = self
        txtGuidelineCurrency.delegate = self
        
        txtReferralPercentage.delegate = self
        txtReferralPrice.delegate = self
        
        txtGuidelinePrice.delegate = self
        
        
        
        txtReferralPercentage.tag = 100
        txtReferralPrice.tag = 100
        txtFixedAmount.tag = 100
        txtStartRange.tag = 100
        txtEndRange.tag = 100
        
        imgViewAttachementBtn.setImageColor(color: UIColor.white)
        
        txtProductName.delegate = self
        
        txtViewProductDes.delegate = self
        
        btnSubmit.setTitle("Submit".localized(), for: .normal)
        
        btnChangeLog.alpha = 0
        
        heightOfBtnChangeLog.constant = 0
        
        
        let changeLogTitle = NSMutableAttributedString(string: "\("View change log".localized())",
            attributes: countAttributes)
        
        btnChangeLog.setAttributedTitle(changeLogTitle, for: .normal)
        
        if isDetail == true {
            
            fetchProductDetail()
            
            btnSubmit.setTitle("Update".localized(), for: .normal)
            btnName.setTitle("Update -->".localized(), for: .normal)
            
            btnName.alpha = 0
            btnSubmit.alpha = 0
            
            if isAdmin == 1 || isPartnerForCompany == 1 {
                
                btnName.alpha = 1
                btnSubmit.alpha = 1
                
            }
            
        }
        
        if buttonTypeList?.count ?? 0 > 0 {
            
            for model in buttonTypeList ?? []{
                
                btnTemplateOptions.append(model.title ?? "")
                
            }
            
            txtBtnTemplate.pickerOptions = btnTemplateOptions
            txtBtnTemplate.dropDownPicker?.reloadAllComponents()
            
            txtBtnTemplate.selectedOption = btnTemplateOptions.first ?? ""
            
        }
        
        if buttonTemplateList?.count ?? 0 > 0 {
            
            for model in buttonTemplateList ?? []{
                
                btnLabelTemplateOptions.append(model.title ?? "")
                
            }
            
            txtBtnLabelTemplate.pickerOptions = btnLabelTemplateOptions
            
            txtBtnLabelTemplate.dropDownPicker?.reloadAllComponents()
            
            txtBtnLabelTemplate.selectedOption = btnLabelTemplateOptions.first ?? ""
            
        }
        
        if paymentCollectTypeList?.count ?? 0 > 0 {
            
            for model in paymentCollectTypeList ?? []{
                
                paymentTemplateOptions.append(model.title ?? "")
                
            }
            
            txtPaymentTemplate.pickerOptions = paymentTemplateOptions
            
            txtPaymentTemplate.dropDownPicker?.reloadAllComponents()
            
            txtPaymentTemplate.selectedOption = paymentTemplateOptions.first ?? ""
            
        }
        
        
        if taxTemplateList?.count ?? 0 > 0 {
            
            for model in taxTemplateList ?? []{
                
                taxTemplateOptions.append(model.title ?? "")
                
            }
            
            txtTaxTemplate.pickerOptions = taxTemplateOptions
            
            txtTaxTemplate.dropDownPicker?.reloadAllComponents()
            
            txtTaxTemplate.selectedOption = taxTemplateOptions.first ?? ""
            
        }
        
        if cancellationTemplateList?.count ?? 0 > 0 {
            
            for model in cancellationTemplateList ?? []{
                
                cancelTemplateOptions.append(model.title ?? "")
                
            }
            
            txtCancellationTemplate.pickerOptions = cancelTemplateOptions
            
            txtCancellationTemplate.dropDownPicker?.reloadAllComponents()
            
            txtCancellationTemplate.selectedOption = cancelTemplateOptions.first ?? ""
            
        }
        
        heightPaymentType.constant = 140
        
        viewExtraCharge.alpha = 0
        
        imgAttachment.setImageColor(color: .white)
        
        txtEndDate.selectedDate = Calendar.current.date(byAdding: .month, value: 1, to: txtStartDate.selectedDate) ?? Date()
    }
    
    func updateAttachmentView(isVideoValue:Int){
        
        if isVideoValue == 1 {
            if imgViewThumbnail.image != nil{
               videoAttachmentIsDone(true)
            }else{
               videoAttachmentIsDone(false)
            }
             
            collectionViewAttachments.alpha = 0
            imgViewVideo.image = UIImage.init(imageLiteralResourceName: "radioFilled")
            imgViewRadioAttachment.image = UIImage.init(imageLiteralResourceName: "radio")
            viewAttachIcon.alpha = 0
            
            isVideo = 1
        } else{
            
            txtVideos.alpha = 0
            heightTxtVideos.constant = 0
            viewVideoImageBg.alpha = 0
            heightVideoImage.constant = 0
            heightAttachmentVideo.constant = 200
            heightCollectionAttachments.constant = 100
            collectionViewAttachments.alpha = 1
            
            imgViewRadioAttachment.image = UIImage.init(imageLiteralResourceName: "radioFilled")
            imgViewVideo.image = UIImage.init(imageLiteralResourceName: "radio")
            viewAttachIcon.alpha = 1
            isVideo = 0
        }
        
        
    }
    
    @IBAction func btnChangeLogAction(_ sender: Any) {
          
          if let model:ProductDetailModel = productDetail
          {
                     
            let storyBoard = UIStoryboard.init(name: ProductSB, bundle: nil)
            let newProductPage:ProductChangeLogVC = storyBoard.instantiateViewController(withIdentifier: "ProductChangeLogVC") as! ProductChangeLogVC
            newProductPage.sellerCode = model.sellerCode ?? 0
            newProductPage.productCode = model.productCode ?? 0
            navigationController?.pushViewController(newProductPage, animated: true)
                     
          }
          
      }
    
    @IBAction func btnRadioAttachmentAction(_ sender: UIButton) {
        
        updateAttachmentView(isVideoValue:0)
         
        
    }
    
    
    @IBAction func btnRadioVideoAction(_ sender: UIButton) {
        
        updateAttachmentView(isVideoValue:1)
        
    }
    
    @IBAction func btnYouTube(_ sender: UIButton) {
         
        let videoPage  = YouTubePlayerViewController()
        videoPage.videoLink = txtVideos.text ?? ""
        navigationController?.pushViewController(videoPage, animated: false)
         
    }
    
    
    @IBAction func btnFreeOfCostAction(_ sender: Any) {
        
        if isFree == 1 {
            imgCheckBoxFree.image = UIImage.init(named: "checkBox")
            isFree = 0
        }
        else{
            imgCheckBoxFree.image = UIImage.init(named: "checkBoxFilled")
            isFree = 1
        }
        
    }
    
    @IBAction func btnPayOnRegistration(_ sender: Any) {
        
        if isPayRegistration == 1 {
            imgCheckBoxRegistration.image = UIImage.init(named: "checkBox")
            isPayRegistration = 0
        }
        else{
            imgCheckBoxRegistration.image = UIImage.init(named: "checkBoxFilled")
            isPayRegistration = 1
        }
    }
    
    @IBAction func btnPayAtEvent(_ sender: Any) {
        
        if isPayEvent == 1 {
            imgCheckBoxEvent.image = UIImage.init(named: "checkBox")
            isPayEvent = 0
            heightPaymentType.constant = 140
            viewExtraCharge.alpha = 0
        }
        else{
            imgCheckBoxEvent.image = UIImage.init(named: "checkBoxFilled")
            heightPaymentType.constant = 220
            viewExtraCharge.alpha = 1
            isPayEvent = 1
        }
    }
    
    func updateUI() {
        
        if let model:ProductDetailModel = productDetail {
            
            txtProductName.text = model.productName ?? ""
            
            let shortDes = model.shortDescription ?? ""
            let des = model.description ?? ""
            
            if shortDes.count > 0 || des.count > 0 {
            
            if companyType != 1 {
                
                txtViewProductDes.placeholder = ""
                txtViewProductDes.text = shortDes.removeWhitespaceSpecial()
                
                
            }
              else{
                
                txtViewProductDes.placeholder = ""
                txtViewProductDes.text = des.removeWhitespaceSpecial()
                
              }
                
            }
            
            selectedIndex = model.status ?? 0
            txtFixedCurrencyTitle.text = model.productCurrencySymbol ?? ""
            txtRangeCurrencyTitle.text = model.productCurrencySymbol ?? ""
            txtGuidelineCurrency.text = model.productCurrencySymbol ?? ""
            
            
            txtViewPricing.text = model.priceGuideline ?? ""
            
            isReferralAttachmentVisible = model.showAttachmentsToBuyer ?? 0
            
            //   updateCheckBoxReferralAttachment(type: isReferralAttachmentVisible)
            
            
            avgRECurrencyId = model.avgRECurrencyId ?? 0
            
            productCurrencyId = model.productCurrencyId ?? 0
            
            referralCurrencyId = model.referralCurrencyId ?? 0
            
            charityCurrencyId = model.charityCurrencyId ?? 0
            
            loyaltyCurrencyId = model.loyaltyCurrencyId ?? 0
            
            if model.referralCurrencySymbol?.count ?? 0 > 0 {
                
                txtReferralCurrency.text = "\( model.referralCurrencySymbol ?? "")"
            }
            
            if model.notesForReferralMember?.count ?? 0 > 0 {
                
                txtViewNotes.placeholder = ""
                
            }
            
//            if model.description?.count ?? 0 > 0 {
//
//                txtViewProductDes.placeholder = ""
//
//            }
            
            attachments = model.banners
            
            isVideo = model.isVideo ?? 0
            
            updateAttachmentView(isVideoValue: model.isVideo ?? 0)
            
            if model.isVideo == 1{
                
                if let fileName = attachments[0] as? [String : String] {
                    
                    if let imagePath = fileName["cdnPath"] {
                        
                        txtVideos.text = imagePath
                        
                        thumbnailYoutube = imagePath.youtubeID
                        
                        if thumbnailYoutube?.count ?? 0 > 0 {
                            
                            genarateThumbnailFromYouTubeID(youTubeID: thumbnailYoutube ?? "")
                            
                            attachments.removeAll()
                            
                        }
                    }
                    
                }
                
                
            }
            
            collectionViewAttachments.reloadData()
            
            txtViewNotes.text = model.notesForReferralMember ?? ""
            
            strLogo = model.productLogo ?? ""
            
            thumbnailLogo = model.prodThumbnailLogo ?? ""
            
            if model.displayStartTime?.count ?? 0 > 0 {
                
                txtStartDate.selectedDate = getDateTime(stringDateTime: model.displayStartTime ?? "")
                
            }
            
            if model.displayEndTime?.count ?? 0 > 0 {
                
                txtEndDate.selectedDate = getDateTime(stringDateTime:model.displayEndTime ?? "")
                
            }
            
            if model.visibleUpto?.count ?? 0 > 0 {
                
                txtVisibleUpto.selectedDate = getDateTime(stringDateTime:model.visibleUpto ?? "")
                
            }
            
            
            if model.keywords?.count ?? 0 > 0 {
                
                txtKeywords.text = model.keywords ?? ""
                
            }
            
            if model.referralFormMsg?.count ?? 0 > 0 {
                
                txtViewMsgPartner.placeholder = ""
                
                txtViewMsgPartner.text = model.referralFormMsg ?? ""
                
            }
            
            if model.enquiryOrApplyJobFormMsg?.count ?? 0 > 0 {
                
                txtViewMsgUser.placeholder = ""
                
                txtViewMsgUser.text = model.enquiryOrApplyJobFormMsg ?? ""
            }
            
            if model.productCodeText?.count ?? 0 > 0 {
                
                txtProductCode.placeholder = ""
                
                txtProductCode.text = model.productCodeText ?? ""
                
            }
            
            if model.isPriceType == 1 {
                
                imgCheckBoxPriceType.image = UIImage(named: "checkBoxFilled")
                
                isPriceTypeSelected = 1
                
                if model.priceType ?? 0 == 1 {
                    
                    if model.maxPrice ?? 0.0 > 0.0 {
                        
                        txtFixedAmount.text = "\(model.maxPrice ?? 0.0)"
                        
                    }
                }
                
                if model.priceType ?? 0 == 2 {
                    
                    if model.minPrice ?? 0.0 >= 0.0 {
                        
                        txtStartRange.text = "\(model.minPrice ?? 0.0)"
                        
                    }
                    
                    if model.maxPrice ?? 0.0 > 0.0 {
                        
                        txtEndRange.text = "\(model.maxPrice ?? 0.0)"
                        
                    }
                }
                
                if model.priceType ?? 0 == 3 {
                    
                    if model.maxPrice ?? 0.0 > 0.0 {
                        
                        txtGuidelinePrice.text = "\(model.maxPrice ?? 0.0)"
                        
                    }
                }
                
                let priceTypeSegment = model.priceType ?? 0
                
                segmentPriceType.selectedSegmentIndex = priceTypeSegment - 1
                
                segmentPriceType.setSegmentStyle()
                
                segmentPriceTypeAction(segmentPriceType)
                
            }
            
            if model.isReferralFeeType == 1 {
                
                referralFeeType = model.referralFeeType ?? 0
                
                imgCheckBoxReferralType.image = UIImage(named: "checkBoxFilled")
                
                isReferralTypeSelected = 1
                
                if model.referralFeeType == 2 {
                    
                    if model.referralFee ?? 0 > 0 {
                        
                        txtReferralPrice.text = "\( model.referralFee ?? 0)"
                    }
                    
                    segmentReferralType.selectedSegmentIndex = 0
                    
                }
                if model.referralFeeType == 1 {
                    
                    if model.referralFee ?? 0 > 0 {
                        
                        txtReferralPercentage.text = "\( model.referralFee ?? 0)"
                    }
                    
                    segmentReferralType.selectedSegmentIndex = 1
                    
                }
                
                btnChangeLog.alpha = 1
                
                heightOfBtnChangeLog.constant = 30
                
                segmentReferralType.setSegmentStyle()
                segmentFeeType(segmentReferralType)
                
            }
            
            branchCode = model.branchCode ?? []
            
            giftCode = model.gifts ?? []
            
            let response = model.responseType ?? 0
            
            isSelected = response - 1
            
            selectedIndex = model.status ?? 0
            
            isAppointmentSelected = model.enableAppointment ?? 0
            
            //  updateAppointment(type: isAppointmentSelected)
            
            for index in 0..<branchCode.count{
                
                for modelMain in branchList ?? []  {
                    
                    if modelMain.branchCode == branchCode[index]{
                        
                        modelMain.isSelected = 1
                        
                    }
                }
            }
            
            buttonTemplateCode = model.buttonTemplateCode ?? 0
            buttonTypeCode = model.buttonType
            taxTemplateCode = model.taxTemplateCode ?? 0
            paymentCollectTypeCode = model.paymentCollectType ?? 0
            cancellationTemplateCode = model.cancellationTemplateCode ?? 0
            
            isFree =  model.paymentType?.isFreeOfCost ?? 0
            isPayRegistration = model.paymentType?.isPayonRegistration ?? 0
            isPayEvent =  model.paymentType?.isPayAtEvent ?? 0
            extraCurrencyId = model.paymentType?.extraCurrencyId ?? 0
            txtExtraChargeAmount.text = "\(model.paymentType?.extraFee ?? 0.0)"
            txtExtraChargeCountryCode.text = model.paymentType?.currencySymbol ?? ""
            
            if buttonTypeList?.count ?? 0 > 0 {
                
                for model in buttonTypeList ?? []{
                    
                    if model.buttonType == buttonTypeCode{
                        
                        txtBtnTemplate.selectedOption = model.title ?? ""
                        
                    }
                    
                }
                
            }
            
            if buttonTemplateList?.count ?? 0 > 0 {
                
                for model in buttonTemplateList ?? []{
                    
                    if model.buttonTemplateCode == buttonTemplateCode {
                        
                        txtBtnLabelTemplate.selectedOption = model.title ?? ""
                        
                    }
                    
                }
                
                
            }
            
            if paymentCollectTypeList?.count ?? 0 > 0 {
                
                for model in paymentCollectTypeList ?? []{
                    
                    if model.paymentCollectType == paymentCollectTypeCode {
                        
                        txtPaymentTemplate.selectedOption = model.title ?? ""
                        
                    }
                    
                }
                
            }
            
            
            if taxTemplateList?.count ?? 0 > 0 {
                
                for model in taxTemplateList ?? []{
                    
                    if model.paymentCollectType == taxTemplateCode {
                        
                        txtTaxTemplate.selectedOption = model.title ?? ""
                        
                    }
                    
                }
                
            }
            
            if cancellationTemplateList?.count ?? 0 > 0 {
                
                for model in cancellationTemplateList ?? []{
                    
                    if model.paymentCollectType == cancellationTemplateCode {
                        
                        txtCancellationTemplate.selectedOption = model.title ?? ""
                        
                    }
                    
                }
                
            }
            
            
            tableViewBranchList.reloadData()
            
            collectionViewStatus.reloadData()
            
        }
        
    }
    
    func genarateThumbnailFromYouTubeID(youTubeID: String) {
        
        let urlString = "http://img.youtube.com/vi/\(youTubeID)/1.jpg"
        
        if let url:URL = URL.init(string: urlString){
             
            imgViewThumbnail.sd_setImage(with: url) { (image, error, type, url) in
                if error == nil{
                    self.videoAttachmentIsDone(true)
                }else{
                    self.videoAttachmentIsDone(false)
                }
            }
             
        }
    }
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
        
        if txtFixedCurrencyTitle == activeTextField {
            txtFixedCurrencyTitle.text = selectedCurrencyCode?.currencySymbol ?? ""
            productCurrencyId = selectedCurrencyCode?.currencyId ?? 0
            
        }
        if txtRangeCurrencyTitle == activeTextField {
            txtRangeCurrencyTitle.text = selectedCurrencyCode?.currencySymbol ?? ""
            productCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        }
        if txtReferralCurrency == activeTextField {
            txtReferralCurrency.text = selectedCurrencyCode?.currencySymbol ?? ""
            referralCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        }
        
        if txtGuidelineCurrency == activeTextField {
            txtGuidelineCurrency.text = selectedCurrencyCode?.currencySymbol ?? ""
            productCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        }
        
        if txtExtraChargeCountryCode == activeTextField {
           txtExtraChargeCountryCode.text = selectedCurrencyCode?.currencySymbol ?? ""
            extraCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        }
        
    }
    
    @IBAction func segmentPriceTypeAction(_ sender: UISegmentedControl) {
        
        segmentPriceType.setSegmentStyle()
        
        switch sender.selectedSegmentIndex {
        case 0:
            // topSpaceForFixedPrice.constant = 16
            heightViewFixedPrice.constant = 120
            heightViewRange.constant = 0
            heightViewGuideline.constant = 0
            heightViewPriceType.constant = 160
            break
        case 1:
            // topSpaceForFixedPrice.constant = 16
            heightViewFixedPrice.constant = 0
            heightViewRange.constant = 120
            heightViewGuideline.constant = 0
            heightViewPriceType.constant = 160
            break
        case 2:
            // topSpaceForFixedPrice.constant = 16
            heightViewFixedPrice.constant = 0
            heightViewRange.constant = 0
            heightViewGuideline.constant = 180
            heightViewPriceType.constant = 250
            break
        default:
            heightViewFixedPrice.constant = 0
            heightViewRange.constant = 0
            heightViewGuideline.constant = 0
            heightViewPriceType.constant = 40
        }
    }
    
    
    @IBAction func segmentFeeType(_ sender: UISegmentedControl) {
        
        segmentReferralType.setSegmentStyle()
        
        switch sender.selectedSegmentIndex {
        case 0:
            //  topSpaceForReferralFixedPrice.constant = 16
            heightViewReferralFixedPrice.constant = 120
            heightViewReferralPercentage.constant = 0
            heightViewReferralType.constant = 160
            if isDetail == true {
                btnChangeLog.alpha = 1
                heightOfBtnChangeLog.constant = 30
                heightViewReferralType.constant = 190
            }
            
            break
        case 1:
            // topSpaceForReferralFixedPrice.constant = 16
            heightViewReferralFixedPrice.constant = 0
            heightViewReferralPercentage.constant = 80
            heightViewReferralType.constant = 130
            if isDetail == true {
                btnChangeLog.alpha = 1
                heightOfBtnChangeLog.constant = 30
                heightViewReferralType.constant = 160
            }
            
            break
        default:
            heightViewReferralFixedPrice.constant = 0
            heightViewReferralPercentage.constant = 0
            heightViewReferralType.constant = 40
        }
        
    }
    
    
    @IBAction func btnPriceTypeAction(_ sender: Any) {
        
        if isPriceTypeSelected == 0 {
            
            imgCheckBoxPriceType.image = UIImage(named: "checkBoxFilled")
            
            // heightViewPriceType.constant = 80
            
            segmentPriceType.alpha = 1
            
            isPriceTypeSelected = 1
            
            segmentPriceTypeAction(segmentPriceType)
        }
        else{
            
            imgCheckBoxPriceType.image = UIImage(named: "checkBox")
            
            heightViewPriceType.constant = 40
            
            segmentPriceType.alpha = 0
            
            isPriceTypeSelected = 0
        }
    }
    
    @IBAction func btnReferralTypeAction(_ sender: Any) {
        
        if isReferralTypeSelected == 0 {
            
            imgCheckBoxReferralType.image = UIImage(named: "checkBoxFilled")
            
            // heightViewReferralType.constant = 80
            
            segmentReferralType.alpha = 1
            
            segmentFeeType(segmentReferralType)
            
            isReferralTypeSelected = 1
            
            //  btnChangeLog.alpha = 0
            
            //   heightOfBtnChangeLog.constant = 0
        }
        else{
            
            imgCheckBoxReferralType.image = UIImage(named: "checkBox")
            
            heightViewReferralType.constant = 40
            
            segmentReferralType.alpha = 0
            
            isReferralTypeSelected = 0
            
            //  btnChangeLog.alpha = 0
            
            //  heightOfBtnChangeLog.constant = 0
        }
        
    }
    
    @IBAction func btnAttachmentAction(_ sender: UIButton) {
        
        isAttachment = true
        isBanner = false
        
        openImagePicker()
        
         startAnimatingAfterSubmit()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            //     weakSelf?.stopAnimating()
            if status {
                print(urlString, imageName)
                var arrayEachImage : [String : String] = [:]
                arrayEachImage["cdnPath"] = urlString
                arrayEachImage["fileName"] = imageName
                weakSelf?.attachments.append(arrayEachImage)
                weakSelf?.collectionViewAttachments.reloadData()
                self.isAttachment = false
                self.isBanner = false
                
                weakSelf?.stopAnimating()
                
            }
            
        }
    }
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachments.count > sender.tag {
            attachments.remove(at: sender.tag)
            collectionViewAttachments.reloadData()
        }
    }
    
    @objc func deleteAttachment(_ sender: NKButton) {
        if attachmentsProduct.count > sender.tag {
            attachmentsProduct.remove(at: sender.tag)
            collectionViewAttachment.reloadData()
        }
    }
    
    @IBAction func btnUploadTermsAction(_ sender: UIButton){
        
        isAttachment = true
        isDocument = true
        openImportDocumentPicker()
        
        startAnimatingAfterSubmit()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            
            if status {
                
                let arrayEachImage:AttachmentBannerTypeModel = AttachmentBannerTypeModel()
                arrayEachImage.cdnPath = urlString
                arrayEachImage.fileName = imageName
                arrayEachImage.attachmentType = 0
                weakSelf?.attachmentsProduct.append(arrayEachImage)
                weakSelf?.collectionViewAttachment.reloadData()
                weakSelf?.isAttachment = false
                
                weakSelf?.stopAnimating()
            }
        }
        
    }
    
    
    
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
        if txtProductName.text?.count == 0 {
            
            txtProductName.showError(message: "Required".localized())
            
            return
        }
        
        if txtViewProductDes.text?.count == 0 {
            
            txtViewProductDes.showError(message: "Required".localized())
            return
        }
        
        branchCode = []
        
        if let intArray:[ProductBranchModel] = branchList{
            
            for model in intArray{
                
                if model.isSelected == 1{
                    
                    branchCode.append(model.branchCode ?? 0)
                    
                }
                
            }
            
        }
        
        if branchCode.count == 0{
            showErrorMessage(message: "Please select a branch".localized())
            return
        }
        
        if isPriceTypeSelected == 1 {
            
            switch segmentPriceType.selectedSegmentIndex {
                
            case 0:
                if txtFixedCurrencyTitle.text?.count ?? 0 == 0 {
                    
                    txtFixedCurrencyTitle.showError(message: "Required".localized())
                    
                    return
                }
                
                if txtFixedAmount.text?.count ?? 0 == 0 {
                    
                    txtFixedAmount.showError(message: "Required".localized())
                    
                    return
                    
                }
                
                if let cost = Double(txtFixedAmount.text ?? "0.0") {
                    if cost > 0.0 {
                        maxPrice = cost
                    }
                }
                
                priceType = 1
                
            case 1:
                if txtRangeCurrencyTitle.text?.count ?? 0 == 0 {
                    
                    txtRangeCurrencyTitle.showError(message: "Required".localized())
                    
                    return
                    
                }
                
                if txtStartRange.text?.count ?? 0 == 0 {
                    
                    txtStartRange.showError(message: "Required".localized())
                    
                    return
                }
                
                if txtEndRange.text?.count ?? 0 == 0 {
                    
                    txtEndRange.showError(message: "Required".localized())
                    
                    
                    return
                }
                
                if let cost = Double(txtStartRange.text ?? "0.0") {
                    
                    minPrice = cost
                }
                
                if let cost = Double(txtEndRange.text ?? "0.0") {
                    if cost > 0.0 {
                        maxPrice = cost
                    }
                    
                }
                
                priceType = 2
                
            case 2:
                
                if txtGuidelineCurrency.text?.count ?? 0 == 0 {
                    
                    txtGuidelineCurrency.showError(message: "Required".localized())
                    
                    return
                    
                }
                
                if txtGuidelinePrice.text?.count ?? 0 == 0 {
                    
                    txtGuidelinePrice.showError(message: "Required".localized())
                    
                    return
                }
                
                if let cost = Double(txtGuidelinePrice.text ?? "0.0") {
                    if cost > 0.0 {
                        maxPrice = cost
                    }
                    
                }
                
                priceType = 3
                
                
            default: print("No")
                
            }
        }
        if isReferralTypeSelected == 1 {
            
            switch segmentReferralType.selectedSegmentIndex {
                
            case 0:
                if txtReferralCurrency.text?.count ?? 0 == 0 {
                    
                    txtReferralCurrency.showError(message: "Required".localized())
                    
                    return
                    
                }
                
                if txtReferralPrice.text?.count ?? 0 == 0 {
                    
                    txtReferralPrice.showError(message: "Required".localized())
                    
                    
                    return
                    
                }
                
                if let cost = Double(txtReferralPrice.text ?? "0.0") {
                    referralValue = cost
                }
                
                referralFeeType = 2
                
            case 1:
                
                if txtReferralPercentage.text?.count ?? 0 == 0 {
                    
                    txtReferralPercentage.showError(message: "Required".localized())
                    
                    return
                    
                }
                
                if let cost = Double(txtReferralPercentage.text ?? "0.0") {
                    referralValue = cost
                }
                
                if referralValue <= 0.0 {
                    
                    txtReferralPercentage.showError(message: "Required".localized())
                    return
                }
                
                referralFeeType = 1
                
            default:
                print("Pathetic::::")
                
            }
        }
        
        if isPayEvent == 1 {
            
              if txtExtraChargeCountryCode.text?.count == 0 {
                
                txtExtraChargeCountryCode.showError(message: "Required".localized())
                
                return
             }
            
        }

        
        if buttonTypeList?.count ?? 0 > 0 {
            
            for model in buttonTypeList ?? []{
                
                if model.title == txtBtnTemplate.selectedOption {
                    
                    buttonTypeCode = model.buttonType ?? 0
                    
                }
                
            }
            
        }
        
        if buttonTemplateList?.count ?? 0 > 0 {
            
            for model in buttonTemplateList ?? []{
                
                if model.title == txtBtnLabelTemplate.selectedOption {
                    
                    buttonTemplateCode = model.buttonTemplateCode ?? 0
                    
                }
                
            }
            
        }
        
        if paymentCollectTypeList?.count ?? 0 > 0 {
            
            for model in paymentCollectTypeList ?? []{
                
                if model.title == txtPaymentTemplate.selectedOption {
                    
                    paymentCollectTypeCode = model.paymentCollectType ?? 0
                    
                }
                
            }
            
        }
        
        if cancellationTemplateList?.count ?? 0 > 0 {
            
            for model in cancellationTemplateList ?? []{
                
                if model.title == txtCancellationTemplate.selectedOption {
                    
                    cancellationTemplateCode = model.paymentCollectType ?? 0
                    
                }
                
            }
            
        }
        
        if taxTemplateList?.count ?? 0 > 0 {
            
            for model in taxTemplateList ?? []{
                
                if model.title == txtTaxTemplate.selectedOption {
                    
                    taxTemplateCode = model.paymentCollectType ?? 0
                    
                }
                
            }
            
            
        }
        
        if isVideo == 1 {
            
            if txtVideos.text?.count ?? 0 > 0 {
                
                if !(isYoutubeLink(checkString: txtVideos.text ?? "")) {
                    
                    showErrorMessage(message: "Invalid youtube url".localized())
                    
                    return
                }
                else{
                    
                    attachments = []
                    
                    var arrayEachImage : [String : String] = [:]
                    arrayEachImage["cdnPath"] = txtVideos.text ?? ""
                    arrayEachImage["fileName"] = ""
                    attachments.append(arrayEachImage)
                    
                }
                
            }
            
        }
        
        submitProductDetails()
        
    }
    
    func isYoutubeLink(checkString: String) -> Bool {
        
        let youtubeRegex = "(http(s)?:\\/\\/)?(www\\.|m\\.)?youtu(be\\.com|\\.be)(\\/watch\\?([&=a-z]{0,})(v=[\\d\\w]{1,}).+|\\/[\\d\\w]{1,})"
        
        let youtubeCheckResult = NSPredicate(format: "SELF MATCHES %@", youtubeRegex)
        return youtubeCheckResult.evaluate(with: checkString)
        
    }
    
    func getDateTimeWithFormat(stringDate:Date) -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: stringDate)
    }
    
    
    func getDateTime(stringDateTime:String) -> Date! {
        
        var dateStartText = ""
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterPrint.timeZone = .current
        
        if let date = dateFormatterGet.date(from: stringDateTime) {
            
            dateStartText = dateFormatterPrint.string(from: date)
            
        }
        
        return getDateTime(stringDate:dateStartText)
    }
    
    func getDateTime(stringDate:String) -> Date! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: stringDate)
    }
    
    
    
    func fetchProductDetail() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/productDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&productCode=\(productCode)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ProductDetailApiModel.self, from: data)
                                
                                if let apiResponse:ProductDetailModel = jsonResponse.productDetails {
                                    
                                    weakSelf?.productDetail = apiResponse
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func submitProductDetails(){
        
        startAnimatingAfterSubmit()
        
        giftCode = []
        
        var avgRefVal = 0.0
        
        var extraFee = 0.0
        
      //  var shortDescription = ""
        var description = ""
        var paymentType:NSDictionary?
        
//        if companyType != 1{
//
//            shortDescription = txtViewProductDes.text ?? ""
//            description = ""
//
//        }
//        else{
            
            description = txtViewProductDes.text ?? ""
         //   shortDescription = ""
      //  }
        
        if let cost:Double = 0.0 {
            avgRefVal = cost
        }
        
        if let cost:Double = Double(txtExtraChargeAmount.text ?? "") {
                extraFee = cost
          }
        
        if let intArray:[ProductGiftModel] = giftList {
            
            for model in intArray  {
                
                if model.isSelected == 1 {
                    
                    giftCode.append(model.giftCode ?? 0)
                    
                }
            }
        }
        
        var responseType:Int = 0
        
        if isSelected > -1 {
            
            responseType = isSelected + 1
            
        }
        
        if isFree == 1 {
        
        paymentType = ["isFreeOfCost":isFree,"isPayonRegistration":isPayRegistration,"isPayAtEvent":isPayEvent]
        }
        
        else if isPayEvent == 1 {
          paymentType = ["isFreeOfCost":isFree,"isPayonRegistration":isPayRegistration,"isPayAtEvent":isPayEvent,"extraCurrencyId":extraCurrencyId ?? 0 ,"extraFee":extraFee]
        }
        else if isPayEvent > 0 || isPayRegistration > 0{
        
        paymentType = ["isFreeOfCost":isFree,"isPayonRegistration":isPayRegistration,"isPayAtEvent":isPayEvent,"extraCurrencyId":extraCurrencyId ?? 0 ,"extraFee":0.0]
            
        }
        
        let params:NSDictionary = ["sellerCode": sellerCode,
                                   "productCode": productCode,
                                   "productName" : txtProductName.text ?? "",
                                   "description" : description,
                                   "productLogo" : strLogo ?? "",
                                   "prodThumbnailLogo":thumbnailLogo ?? "",
                                   "banners":attachments,
                                   "priceType":priceType,
                                   "productCurrencyId" : productCurrencyId ?? 0,
                                   "minPrice" :minPrice,
                                   "maxPrice" : maxPrice,
                                   "priceGuideline":txtViewPricing.text ?? "",
                                   "referralFeeType" : referralFeeType,
                                   "referralCurrencySymbol":selectedCurrencyCode?.currencySymbol ?? "",
                                   "referralCurrencyId":referralCurrencyId ?? 0,
                                   "referralFee" : referralValue,
                                   "notesForReferralMember" : txtViewNotes.text ?? "",
                                   "status" : selectedIndex,
                                   "responseType":responseType,
                                   "branchCode": branchCode,
                                   "keywords":txtKeywords.text ?? "",
            "isPriceType":isPriceTypeSelected,
            "isReferralFeeType":isReferralTypeSelected,
            "gifts":giftCode,
            "showAttachmentsToBuyer":isReferralAttachmentVisible,
            "avgRECurrencyId": avgRECurrencyId ?? 0,
            "avgREAmount": avgRefVal,
            "productCodeText":txtProductCode.text ?? "",
            "referralFormMsg":txtViewMsgPartner.text ?? "",
            "enquiryOrApplyJobFormMsg":txtViewMsgUser.text ?? "",
            "enableAppointment":isAppointmentSelected,
            "isVideo":isVideo,
            "displayEndTime":getDateTimeWithFormat(stringDate: txtEndDate.selectedDate),
            "displayStartTime":getDateTimeWithFormat(stringDate: txtStartDate.selectedDate),
            "visibleUpto":getDateTimeWithFormat(stringDate: txtVisibleUpto.selectedDate),
            "buttonTemplateCode":buttonTemplateCode ?? 0,
            "buttonType":buttonTypeCode ?? 0,
            "taxTemplateCode":taxTemplateCode ?? 0,
            "paymentCollectType":paymentCollectTypeCode ?? 0,
            "cancellationTemplateCode":cancellationTemplateCode ?? 0,
            "productType":2,
            "paymentType":paymentType ?? [:]
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerProducts?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                //                            let apiResponse = try decoder.decode(CategoryListModel.self, from: data)
                                //                            weakSelf?.categoryNewList = apiResponse.categoriesList ?? []
                                //                            weakSelf?.searchCategoryList = apiResponse.categoriesList ?? []
                                //
                                //                            DispatchQueue.main.async {
                                //
                                //                                weakSelf?.collectionViewCategory.reloadData()
                                //                            }
                                
                                weakSelf?.delegate?.didAddedEvent()
                                weakSelf?.showInfoMessage(message: message.localized())
                                
                                let isUpdated:[String: Int] = ["update": 1]
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callSearch"), object: nil, userInfo: isUpdated)
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    
}


extension NewEventsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableViewBranchList {
            
            return branchList?.count ?? 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewBranchList {
            
            if let cell:NewProductBranchCell = tableView.dequeueReusableCell(withIdentifier: "NewProductBranchCell", for: indexPath) as? NewProductBranchCell {
                
                return getCellForProductBranch(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableViewBranchList {
            
            if let model:ProductBranchModel = branchList?[indexPath.row]{
                
                if model.isSelected == 0 {
                    
                    model.isSelected = 1
                    
                }
                else{
                    model.isSelected = 0
                }
                
                tableViewBranchList.reloadData()
            }
            
        }
        
    }
    
    func getCellForProductBranch(cell:NewProductBranchCell,indexPath:IndexPath) -> NewProductBranchCell {
        
        cell.selectionStyle = .none
        
        if let model:ProductBranchModel = branchList?[indexPath.row]{
            
            cell.lblTitle.text = model.branchName ?? ""
            
            if model.isSelected == 0 {
                
                cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBox")
                
            }
            else{
                cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
            }
        }
        
        return cell
    }
    
}


extension NewEventsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            return CGSize.init(width: collectionViewStatus.frame.width / 4, height: 40.0)
            
        }
        
        if collectionView == collectionViewAttachments || collectionView == collectionViewAttachment
            
        {
            return CGSize.init(width: 100, height: 100)
            
        }
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewAttachments
        {
            if attachments.count > 0 && isVideo == 0{
                
                return attachments.count
            }
            
        }
        if collectionView == collectionViewAttachment
        {
            if attachmentsProduct.count > 0 {
                
                return attachmentsProduct.count
                
            }
            
        }
        if collectionView == collectionViewStatus{
            
            if statusList?.count ?? 0 > 0 {
                
                return statusList?.count ?? 0
            }
            
        }
        
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewAttachments
        {
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachment(cell: cell, indexPath: indexPath)
                
            }
        }
        
        if collectionView == collectionViewAttachment
        {
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachmentTerms(cell: cell, indexPath: indexPath)
                
            }
        }
        
        if collectionView == collectionViewStatus{
            
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForStatus(cell: cell, indexPath: indexPath)
                
            }
            
        }
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionViewStatus
            
        {
            selectedIndex = indexPath.row
            
            collectionViewStatus.reloadData()
            
        }
        
        if collectionView == collectionViewAttachments {
            
            if let fileName = attachments[indexPath.row] as? [String : String] {
                
                if let imagePath = fileName["cdnPath"] {
                    
                    
                    let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                    let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                    imageViewer.strImg = imagePath
                    navigationController?.pushViewController(imageViewer, animated: true)
                    
                }
                
            }
            
        }
        
        if collectionView == collectionViewAttachment {
            
            if let model:AttachmentBannerTypeModel = attachmentsProduct[indexPath.row]
            {
                if model.cdnPath?.count ?? 0 > 0 {
                    
                    openWebView(urlString: model.cdnPath ?? "", title: model.fileName ?? "", subTitle: "")
                    
                }
            }
        }
    }
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete .addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        
        if attachments.count > 0 {
            
            if let fileName = attachments[indexPath.row] as? [String : String]
            {
                if let imagePath = fileName["cdnPath"]
                {
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        cell.imgView.sd_setImage(with: url, completed: nil)
                    }
                }
                
            }
            
        }
        return cell
    }
    
    
    func getCellForAttachmentTerms(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
        
        cell.btnDelete .addTarget(self, action: #selector(deleteAttachment(_:)), for: .touchUpInside)
        
        if attachmentsProduct.count > 0 {
            
            if let model:AttachmentBannerTypeModel = attachmentsProduct[indexPath.row]
            {
                if let imagePath = model.cdnPath
                {
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        
                        let strFileType = model.fileName?.fileExtension()
                        
                        if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            cell.imgView.sd_setImage(with: url, completed: nil)
                            
                        }
                        else{
                            
                            cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                        }
                    }
                    
                    for modelMain in attachmentTypes ?? [] {
                        
                        if modelMain.attachmentType == model.attachmentType {
                            
                            cell.lblTitle.text = modelMain.title ?? ""
                            
                        }
                        
                    }
                    
                }
                
            }
        }
        return cell
    }
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:SellerStatusModel = statusList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.statusTitle ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
    func videoAttachmentIsDone(_ isDone:Bool){
        if isDone{
            heightTxtVideos.constant = 40
            txtVideos.alpha = 1
            viewVideoImageBg.alpha = 1
            heightVideoImage.constant = 75
            heightAttachmentVideo.constant = 230
            heightCollectionAttachments.constant = 0
        }else{
            heightTxtVideos.constant = 40
            txtVideos.alpha = 1
            viewVideoImageBg.alpha = 0
            heightVideoImage.constant = 0
            heightAttachmentVideo.constant = 230 - 80
            heightCollectionAttachments.constant = 0
        }
          
    }
    
}


extension NewEventsVC:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if  textField == txtRangeCurrencyTitle || textField == txtFixedCurrencyTitle || textField == txtGuidelineCurrency || textField == txtReferralCurrency || textField == txtGuidelineCurrency || textField == txtExtraChargeCountryCode{
            
            self.activeTextField = textField
            
            openCurrencySelector()
            
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let txtField:RCTextField = textField as? RCTextField{
            txtField.showError(message: "")
        }
        
        return true
    }
    
}

extension NewEventsVC:UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if companyType != 1 {
            
            if textView == txtViewProductDes {
                txtViewProductDes.showError(message: "")
                
                return textView.text.count + (text.count - range.length) <= 150
            }
        }
        
        if textView == txtViewMsgPartner{
            
            return textView.text.count + (text.count - range.length) <= 250
            
        }
        
        if textView == txtViewMsgUser{
            
            return textView.text.count + (text.count - range.length) <= 250
            
        }
        
        return true
    }
    
}
extension NewEventsVC:RCTextFieldDelegate{
    
    func didChangeDate(textField: RCTextField) {
        
        if textField.tag == 11 {
            txtStartDate.dateTimePicker.minimumDate = Date()
            txtEndDate.dateTimePicker.minimumDate = txtStartDate.selectedDate
             
//            if txtEndDate.selectedDate < txtStartDate.selectedDate {
//
//                txtEndDate.selectedDate = txtStartDate.selectedDate
//
//            }
            
        }
         txtVisibleUpto.dateTimePicker.minimumDate = txtStartDate.selectedDate
         txtVisibleUpto.dateTimePicker.maximumDate = txtEndDate.selectedDate
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 222{
           genarateThumbnailFromYouTubeID(youTubeID: textField.text?.youtubeID ?? "")
                                       
        }
    }

    
}
