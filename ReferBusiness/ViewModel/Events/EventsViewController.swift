//
//  EventsViewController.swift
//  iCanRefer
//
//  Created by Vivek Mishra on 04/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol EventsVCDelegate: class {
    func didCallEvents()
    
}

class EventsViewController: AppBaseViewController {
    
    @IBOutlet weak var lblNoEventsMsg: NKLabel!
    
    @IBOutlet weak var viewNoEventsBG: UIView!
    
    @IBOutlet weak var lblProductCount:NKLabel!
    
    @IBOutlet weak var tableViewList: UITableView!
    
    var sellerCode:Int = 0
    
    var statusList:[SellerStatusModel]?
    
    var productList:[ProductModel]?
    
    var colorCodes:ColorModel?
    
    var responseTypes:[ResponseTypeModel]?
    
    var currency:[CurrencyModel]?
    
    var branchList:[ProductBranchModel]?
    
    var attachmentTypes:[AttachmentTypeModel]?
    
    var buttonTypeList:[ButtonTypeMasterModel]?
    
    var buttonTemplateList:[ButtonTemplateMasterModel]?
    
    var paymentCollectTypeList:[PaymentCollectTypeMasterModel]?
    
    var cancellationTemplateList:[CancellationTemplateMasterModel]?
    
    var taxTemplateList:[TaxTemplateMasterModel]?
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    var companyType:Int = 1
    
    var productCount:Int = 0
    
    weak var delegate:EventsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblNoEventsMsg.text = "No Events Available".localized()
        
        fetchJobList()
        
        updateNavButton()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.integer(forKey: "selectedTab") == 1
        {
            UserDefaults.standard.set(0, forKey: "selectedTab")
        }
        
    }
    
    func updateNavButton(){
        
        let btnName = UIButton()
        btnName.setTitle("New -->", for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(openProductCreationPage), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        // btnName.alpha = 0
        
        //             if isAdmin == 1 || isPartnerForCompany == 1 {
        //
        //                btnName.alpha = 1
        //             }
        
        navigationItem.title = "Events".localized()
        
    }
    
    @objc func openProductCreationPage() {
        
        let storyBoard = UIStoryboard.init(name: EventsSB, bundle: nil)
        if let newEventPage:NewEventsVC = storyBoard.instantiateViewController(withIdentifier: "NewEventsVC") as? NewEventsVC {
            
            newEventPage.sellerCode = sellerCode
            newEventPage.statusList = statusList ?? []
            newEventPage.currency = currency
            newEventPage.branchList = branchList
            newEventPage.responseTypes = responseTypes
            newEventPage.attachmentTypes = attachmentTypes ?? []
            newEventPage.delegate = self
            newEventPage.companyType = companyType
            
            newEventPage.buttonTypeList = buttonTypeList ?? []
               
            newEventPage.buttonTemplateList = buttonTemplateList ?? []
            
            newEventPage.paymentCollectTypeList = paymentCollectTypeList ?? []
               
            newEventPage.cancellationTemplateList = cancellationTemplateList ?? []
               
            newEventPage.taxTemplateList = taxTemplateList ?? []
               
            navigationController?.pushViewController(newEventPage, animated: true)
            
        }
    }
    
    func fetchJobList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/productList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&productType=\(2)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ProductApiModel.self, from: data)
                                
                                /// vivek
                                
                                if let apiResponse:[AttachmentTypeModel] = jsonResponse.attachmentTypes {
                                    
                                    weakSelf?.attachmentTypes = apiResponse
                                    
                                }
                                
                                if let apiResponse:[ProductModel] = jsonResponse.productList {
                                    
                                    weakSelf?.productList = apiResponse
                                    
                                }
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.productCount = apiResponse
                                    
                                    weakSelf?.lblProductCount.text = ("\("This company has ".localized())\(weakSelf?.productCount ?? 0)\(" events")")
                                }
                                //                                
                                if let apiResponse:[ResponseTypeModel] = jsonResponse.responseTypes {
                                    
                                    weakSelf?.responseTypes = apiResponse
                                    
                                }
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currency {
                                    
                                    weakSelf?.currency = apiResponse
                                    
                                }
                                
                                if let apiResponse:[ProductBranchModel] = jsonResponse.branchList {
                                    
                                    weakSelf?.branchList = apiResponse
                                    
                                }
                                if let apiResponse:[ButtonTypeMasterModel] = jsonResponse.buttonTypeList {
                                    weakSelf?.buttonTypeList = apiResponse
                                }
                                
                                if let apiResponse:[ButtonTemplateMasterModel] = jsonResponse.buttonTemplateList {
                                    weakSelf?.buttonTemplateList = apiResponse
                                }
                                
                                if let apiResponse:[PaymentCollectTypeMasterModel] = jsonResponse.paymentCollectTypeList {
                                    
                                    weakSelf?.paymentCollectTypeList = apiResponse
                                    
                                }
                                if let apiResponse:[CancellationTemplateMasterModel] = jsonResponse.cancellationTemplateList {
                                    
                                    weakSelf?.cancellationTemplateList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[TaxTemplateMasterModel] = jsonResponse.taxTemplateList {
                                    
                                    weakSelf?.taxTemplateList = apiResponse
                                    
                                }
                                
                                if weakSelf?.productList?.count ?? 0 == 0 {
                                    
                                    if weakSelf?.isAdmin == 1 || weakSelf?.isPartnerForCompany == 1 {
                                        
                                          weakSelf?.openProductCreationPage()
                                        
                                    }
                                    
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
}

extension EventsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if productList?.count ?? 0 > 0 {
          
            tableViewList.alpha = 1
            
            return productList?.count ?? 0
            
        }
        else{
             tableViewList.alpha = 0
        }
        
        return  0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:ProductListCell = tableView.dequeueReusableCell(withIdentifier: "ProductListCell", for: indexPath) as? ProductListCell {
            
            return getCellForProductList(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
        
        
    }
    
    
    func getCellForProductList(cell:ProductListCell,indexPath:IndexPath) -> ProductListCell {
        
        cell.selectionStyle = .none
        
        if let model:ProductModel = productList?[indexPath.row]
        {
            cell.lblProductNameText.text = model.productName ?? ""
            cell.lblStatusText.text = model.statusTitle ?? ""
            
            cell.imageView?.image = nil
            
            if model.productLogo?.count ?? 0 > 0 {
                cell.imgViewProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
            }
            else{
                cell.imgViewProduct.image = UIImage.init(named: "eventIcon")
            }
            
            if model.status == 1 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.activeColor ?? "")
            }
                
            else if model.status == 2 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.inactiveColor ?? "")
            }
                
            else if model.status == 3 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.blacklistedColor ?? "")
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:ProductModel = productList?[indexPath.row]
        {
            
            let storyBoard = UIStoryboard.init(name: EventsSB, bundle: nil)
            if let newEventPage:NewEventsVC = storyBoard.instantiateViewController(withIdentifier: "NewEventsVC") as? NewEventsVC {
            newEventPage.sellerCode = model.sellerCode ?? 0
            newEventPage.productCode = model.productCode ?? 0
            newEventPage.branchList = branchList
            newEventPage.responseTypes = responseTypes
            newEventPage.statusList = statusList
            newEventPage.currency = currency
            newEventPage.attachmentTypes = attachmentTypes ?? []
            newEventPage.isDetail = true
            newEventPage.delegate = self
            newEventPage.isAdmin = isAdmin
            newEventPage.isAdmin = isPartnerForCompany
            newEventPage.companyType = companyType
            newEventPage.buttonTypeList = buttonTypeList ?? []
            newEventPage.buttonTemplateList = buttonTemplateList ?? []
            newEventPage.paymentCollectTypeList = paymentCollectTypeList ?? []
            newEventPage.cancellationTemplateList = cancellationTemplateList ?? []
            newEventPage.taxTemplateList = taxTemplateList ?? []
            navigationController?.pushViewController(newEventPage, animated: true)
                
            }
            
        }
    }
    
}

extension EventsViewController:NewEventsVCDelegate {
    
    func didAddedEvent() {
        
         fetchJobList()
        
        delegate?.didCallEvents()
    }
    
}
