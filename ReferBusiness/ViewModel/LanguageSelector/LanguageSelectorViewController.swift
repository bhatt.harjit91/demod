//
//  LanguageSelectorViewController.swift
//  NearKart
//
//  Created by RaviKiran B on 05/09/18.
//  Copyright © 2018 RaviKiran B. All rights reserved.
//

import UIKit

class LanguageSelectorViewController: AppBaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableVIew: UITableView!
    
    @IBOutlet weak var btnDone: UIBarButtonItem!
    @IBOutlet weak var navItem: UINavigationItem!
    
    @IBOutlet weak var btnCancel: UIBarButtonItem!
    
    var availableLangauges:[String] = []
    var languageCodes:[String]=[]
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        availableLangauges = ["English".localized(),"Arabic".localized(),"Spanish".localized(),"Thai".localized(),"French".localized(),"Portuguese".localized()]
        languageCodes = ["en","ar","es","th","fr","pt-PT"]
        // Do any additional setup after loading the view.
        navigationItem.title = "Select Language".localized()
        btnDone.title = "Done".localized()
        btnCancel.title = "Cancel".localized()
        navigationController?.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availableLangauges.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        //let langauges:[String] = Localize.availableLanguages()
        cell?.textLabel?.text = availableLangauges[indexPath.row].localized()
        cell?.textLabel?.textAlignment =  textAlignmentForLanguage()
        cell?.accessoryType = UITableViewCell.AccessoryType.none
        if (selectedIndex<0 && Localize.currentLanguage == languageCodes[indexPath.row]){
            cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
        }
        else if indexPath.row == selectedIndex {
            cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
        }
        else if selectedIndex == -1 && UserDefaults.standard.bool(forKey: "isFromAppDelegate") {
           if indexPath.row == 0 {
                cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
            }
        
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row;
        tableView.reloadData()
    }
    
    @IBAction func doneSelection(_ sender: Any) {
        if selectedIndex < 0 {
            if UserDefaults.standard.bool(forKey: "isFromAppDelegate") {
                UserDefaults.standard.set(0, forKey: "isFromAppDelegate")
                
                Localize.setCurrentLanguage(selectedLanguage: languageCodes[0])
                let storyBoard = UIStoryboard.init(name: LaunchSB, bundle: nil)
                view.window?.rootViewController = storyBoard.instantiateInitialViewController()
                
                return
                
            }
            navigationController?.dismiss(animated: true, completion: nil)
            return
        }
        UserDefaults.standard.set(0, forKey: "isFromAppDelegate")
        Localize.setCurrentLanguage(selectedLanguage: languageCodes[selectedIndex])
        let storyBoard = UIStoryboard.init(name: LaunchSB, bundle: nil)
        view.window?.rootViewController = storyBoard.instantiateInitialViewController()
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        if UserDefaults.standard.bool(forKey: "isFromAppDelegate") {
            
            UIControl().sendAction(#selector(NSXPCConnection.suspend),
                                   to: UIApplication.shared, for: nil)
            
        }
        
        dismiss(animated: true, completion: nil)
        
    }
}
