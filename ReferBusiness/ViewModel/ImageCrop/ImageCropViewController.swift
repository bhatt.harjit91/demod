//
//  ImageCropViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 14/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import IGRPhotoTweaks
import UIKit

class ImageCropViewController: IGRPhotoTweakViewController {

    override func viewDidLoad() {
        
        super.viewDidLoad()

        navigationItem.title = "Crop Image".localized()
        self.setCropAspectRect(aspect: "16:9")
        self.lockAspectRatio(true)
    }

    @IBAction func cancelBtn(_ sender: Any) {
        
        self.dismissAction()
    }
    
    @IBAction func chooseBtn(_ sender: Any) {
        
        cropAction()
    }
    
}
