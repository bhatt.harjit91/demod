//
//  AppointmentViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 20/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class AppointmentViewController:AppBaseViewController{
    
    
   @IBOutlet weak var collectionShowSelectedResources: UICollectionView!
    
    var resourceList:[ResourceTypeModel]?
    
    var appointmentId:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
         fetchList(withAppointmentId:appointmentId)
        
        title = "Appointment"
    }
    
    func fetchList(withAppointmentId:Int){
    
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["appointmentId": withAppointmentId]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/appointedResourceList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            print(dataObj)
                            do {
                                
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                let apiResponse = try decoder.decode(ResourceTypeApiModel.self, from: data)
                                
                                if let apiResponse:[ResourceTypeModel] = apiResponse.resourceList{
                                    
                                    weakSelf?.resourceList = apiResponse
                                    
                                }
                              
                                weakSelf?.collectionShowSelectedResources.reloadData()
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                        
                    }
                    
                }
                
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    
 
}

extension AppointmentViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func getCellForCollectionViewAppointmentLevel(cell:CollvwAppointmentLevelCell,indexPath:IndexPath) -> CollvwAppointmentLevelCell {
            
            cell.data = resourceList?[indexPath.row]
            cell.bgView.backgroundColor = UIColor.getGradientColor(index: 2).first!
            cell.lblLevelName.textColor = UIColor.white
           
            return cell
        }
     
     //MARK: tableView Delegate & DataSource
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return resourceList?.count ?? 0
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell:CollvwAppointmentLevelCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "CollvwAppointmentLevelCell", for: indexPath) as? CollvwAppointmentLevelCell)!
         return getCellForCollectionViewAppointmentLevel(cell: cell, indexPath: indexPath)
         
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           
          return CGSize.init(width: 100, height: 120)
     }
     

}
