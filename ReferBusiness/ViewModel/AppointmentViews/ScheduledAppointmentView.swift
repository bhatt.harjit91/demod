//
//  ScheduledAppointmentView.swift
//  iCanRefer
//
//  Created by Hirecraft on 20/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class ScheduledAppointmentView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var lblAppointmentTime:NKLabel!
    @IBOutlet weak var lblNoOfPersons:NKLabel!
    @IBOutlet weak var btnShowAppintmentResources:UIButton!
    @IBOutlet weak var btnCloseTheView:UIButton!
    @IBOutlet weak var contentView:UIView!
    
    private func commonInit(){
        Bundle.main.loadNibNamed("", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,. flexibleWidth]
    }

}
