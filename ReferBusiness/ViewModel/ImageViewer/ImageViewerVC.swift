//
//  ImageViewerVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 11/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ImageViewerVC: AppBaseViewController {

    @IBOutlet weak var imgView: UIImageView!
    
    var strImg = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Viewer".localized()

       if let url:URL = URL.init(string: "\(StorageUrl)\(strImg)"){
            imgView.sd_setImage(with: url, completed: nil)
        }
        
    }
}
