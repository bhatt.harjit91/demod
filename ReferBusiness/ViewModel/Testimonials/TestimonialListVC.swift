//
//  TestimonialListVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 10/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol TestimonialListVCDelegate: class {
    func didCalledTestimonial()
    
}
class TestimonialListVC: AppBaseViewController {
    
   @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblUserProductCount: NKLabel!
          
    @IBOutlet weak var lblNoData: NKLabel!
    
    var sellerCode:Int = 0
    
    var colorCodes:ColorModel?
          
    var statusList:[SellerStatusModel]?
    
    var sellerTestimonialList:[TestimonialModel]?
    
    var userCount:Int = 0
    
    var isFromProduct:Bool = false
    
    weak var delegate:TestimonialListVCDelegate?
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblNoData.text = "No Data Found".localized()
        
        fetchTestimonialList()
        
        if  !isFromProduct{
            
            updateNavBar()
        }
        
    }
    
    func updateNavBar() {
         let btnName = UIButton()
                    
         btnName.setTitle("New -->".localized(), for: .normal)
         btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
         btnName.setTitleColor(UIColor.white, for: .normal)
         btnName.addTarget(self, action: #selector(newCustomerAction(sender:)), for: .touchUpInside)
                     
         let rightBarButton = UIBarButtonItem()
         rightBarButton.customView = btnName
         self.navigationItem.rightBarButtonItem = rightBarButton
        
        btnName.alpha = 0
                      
        if isAdmin == 1 || isPartnerForCompany == 1 {
                          
          btnName.alpha = 1
            
         }
         
        // navigationItem.title = "Testimonials".localized()
     }
    
    @objc func newCustomerAction(sender: UIButton){
          
          let storyBoard = UIStoryboard.init(name: TestimonialSB, bundle: nil)
          let newCustomerPage:NewTestimonialVC = storyBoard.instantiateViewController(withIdentifier: "NewTestimonialVC") as! NewTestimonialVC
          newCustomerPage.sellerCode = sellerCode
          newCustomerPage.statusList = statusList ?? []
          newCustomerPage.isDetail = false
          newCustomerPage.delegate = self

          navigationController?.pushViewController(newCustomerPage, animated: true)
          
      }
    
    func openNewNewTestimonial() {
        
        let storyBoard = UIStoryboard.init(name: TestimonialSB, bundle: nil)
                 let newCustomerPage:NewTestimonialVC = storyBoard.instantiateViewController(withIdentifier: "NewTestimonialVC") as! NewTestimonialVC
                 newCustomerPage.sellerCode = sellerCode
                 newCustomerPage.statusList = statusList ?? []
                 newCustomerPage.isDetail = false
                 newCustomerPage.delegate = self

                 navigationController?.pushViewController(newCustomerPage, animated: true)
        
    }
    
    func fetchTestimonialList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getSellerTestimonialList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        do {
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
                            print(dataObj)
                            
                            let jsonResponse = try decoder.decode(TestimonialApiModel.self, from: data)
                            
                            if let apiResponse:[TestimonialModel] = jsonResponse.sellerTestimonialList {
                                
                              weakSelf?.sellerTestimonialList = apiResponse
                                
                            }
                            if let apiResponse:[SellerStatusModel] = jsonResponse.statusList {
                                                          
                            weakSelf?.statusList = apiResponse
                                                          
                            }
                    
                            if let apiResponse:Int = jsonResponse.count {

                                weakSelf?.userCount = apiResponse
                                
                                self.navigationItem.title = "Testimonials".localized()
                                
                                if weakSelf?.userCount ?? 0 > 0 {
                                    
                                    self.navigationItem.title = "\("Testimonials (".localized())\(weakSelf?.userCount ?? 0)\(")")"
                                }
                                
//                                weakSelf?.lblUserProductCount.text = ("\("This seller has ")\(weakSelf?.userCount ?? 0)\(" Testimonials".localized())")
                            }
                            
                            if weakSelf?.sellerTestimonialList?.count ?? 0 == 0 {
                            
                                if weakSelf?.isAdmin == 1 || weakSelf?.isPartnerForCompany == 1 {
                                    
                                   weakSelf?.openNewNewTestimonial()
                                    
                                }
                                
                            }
                            weakSelf?.tableViewList.reloadData()
                            
                            return
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                            
                        }
                      }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }


}

extension TestimonialListVC:NewTestimonialVCDelegate{
    
    func didAddedTestimonial(){
        
        fetchTestimonialList()
        
        delegate?.didCalledTestimonial()
        
    }
}

extension TestimonialListVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = sellerTestimonialList?.count ?? 0
        
        if count > 0 {
            
            tableViewList.alpha = 1
            
            return sellerTestimonialList?.count ?? 0
            
        }
            
        else{
            
            tableViewList.alpha = 0
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:TestimonialListCell = tableView.dequeueReusableCell(withIdentifier: "TestimonialListCell", for: indexPath) as? TestimonialListCell
        {
            
            return getCellForTestimonialList(cell: cell, indexPath: indexPath)
            
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    if  !isFromProduct{
        
        if let model:TestimonialModel = sellerTestimonialList?[indexPath.row]
        {
         let storyBoard = UIStoryboard.init(name: TestimonialSB, bundle: nil)
         let newTestimonialPage:NewTestimonialVC = storyBoard.instantiateViewController(withIdentifier: "NewTestimonialVC") as! NewTestimonialVC
         newTestimonialPage.sellerCode = sellerCode
         newTestimonialPage.testimonialCode = model.testimonialCode ?? 0
            
         newTestimonialPage.statusList = statusList ?? []
         newTestimonialPage.delegate = self
         newTestimonialPage.sellerTestimonialDetails = model
         newTestimonialPage.isDetail = true
         newTestimonialPage.isAdmin = isAdmin
         newTestimonialPage.isPartnerForCompany = isPartnerForCompany
         navigationController?.pushViewController(newTestimonialPage, animated: true)
        }
        
      }
    }
    
    func getCellForTestimonialList(cell:TestimonialListCell,indexPath:IndexPath) -> TestimonialListCell {
        
        cell.selectionStyle = .none
        
        if let model:TestimonialModel = sellerTestimonialList?[indexPath.row]
        {
            cell.lblDes.text = model.description ?? ""
            
            cell.lblCreatedBy.text = model.createdBy ?? ""

            cell.lblStatusText.text = model.statusTitle ?? ""
            
            cell.lblDesignation.text = model.userProfile ?? ""
            
            if model.profilePicture?.count ?? 0 > 0 {
                cell.imgViewUser.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.profilePicture ?? "")")), completed: nil)
                     }
            
            if model.status == 1 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.activeColor ?? "")
            }
                
            else if model.status == 2 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.inactiveColor ?? "")
            }
                
            else if model.status == 3 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.blacklistedColor ?? "")
            }
            else {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString:"#42A5F5")
            }
            
        }

       
        return cell
    }
    
}

class TestimonialListCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
          
    @IBOutlet weak var lblDes: NKLabel!
    
     @IBOutlet weak var lblCreatedBy: NKLabel!
          
    @IBOutlet weak var lblStatusText: NKLabel!
          
    @IBOutlet weak var viewStatus: RCView!
          
    @IBOutlet weak var imgViewUser: RCImageView!
    
    @IBOutlet weak var lblDesignation: NKLabel!
    
}
