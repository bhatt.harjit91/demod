//
//  NewTestimonialVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 10/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol NewTestimonialVCDelegate: class {
    
    func didAddedTestimonial()
}

class NewTestimonialVC: SendOTPViewController {
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var imgViewLogo: UIImageView!
    
    @IBOutlet weak var txtViewDes: NKTextView!
    
    @IBOutlet weak var txtCreatedBy: RCTextField!
    
    @IBOutlet weak var txtDesignation: RCTextField!
    
    weak var delegate:NewTestimonialVCDelegate?
    
    var selectedIndex:Int = 0
    
    var statusList:[SellerStatusModel]?
    
    var isDetail:Bool = false
    
    @IBOutlet weak var btnLogo: NKButton!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    //   @IBOutlet weak var btnAttachments: NKButton!
    
    var testimonialCode:Int = 0
    
    var sellerCode:Int = 0
    
    var strLogo:String?
    
    var strLogoName:String?
    
    var sellerTestimonialDetails:TestimonialModel?
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        txtCreatedBy.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Created By".localized())
        
        txtViewDes.changePlaceHolderColor = true
        txtViewDes.placeholder = "Description".localized()
        
        btnLogo.addTarget(self, action: #selector(btnLogoAction), for: .touchUpInside)
        
        btnSubmit.addTarget(self, action: #selector(btnSubmitAction), for: .touchUpInside)
        
        updateNavBar()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isDetail {
            
            fetchTestimonialDetail()
        }
        
    }
    
    func updateNavBar() {
        let btnName = UIButton()
        
        if isDetail {
            btnName.setTitle("Update -->".localized(), for: .normal)
            btnSubmit.setTitle("Update".localized(), for: .normal)
            btnName.alpha = 0
            btnSubmit.alpha = 0
            
            if isAdmin == 1 || isPartnerForCompany == 1 {
                
                btnName.alpha = 1
                btnSubmit.alpha = 1
                
            }
            
        }
        else{
            btnName.setTitle("Submit -->".localized(), for: .normal)
            btnSubmit.setTitle("Submit".localized(), for: .normal)
        }
        
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(btnSubmitAction), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        navigationItem.title = "Testimonial".localized()
        
    }
    
    func adjustTextViewHeight(arg : NKTextView)
    {
        
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = true
    }
    
    func updateUI() {
        
        if let model:TestimonialModel = sellerTestimonialDetails {
            
            txtCreatedBy.text = model.createdBy ?? ""
            
            txtViewDes.text = model.description ?? ""
            
            if model.description?.count ?? 0 > 0 {
                txtViewDes.placeholder = ""
            }
            
            if model.description?.count ?? 0 > 100 {
                
                adjustTextViewHeight(arg: txtViewDes)
                
            }
            
            strLogo = model.profilePicture ?? ""
            
            strLogoName = model.ppFileName ?? ""
            
            txtDesignation.text = model.userProfile ?? ""
            
            if strLogo?.count ?? 0 > 0 {
                imgViewLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(strLogo ?? "")")), completed: nil)
            }
            
            if model.status == 1 {
                
                selectedIndex = 0
                
            }
            if model.status == 2 {
                
                selectedIndex = 1
                
            }
            if model.status == 3 {
                
                selectedIndex = 2
                
            }
            
            collectionViewStatus.reloadData()
            
        }
    }
    
    @objc func btnLogoAction() {
        
        isAttachment = true
        
        openImagePicker()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            //     weakSelf?.stopAnimating()
            if status {
                print(urlString, imageName)
                weakSelf?.strLogo = urlString
                weakSelf?.strLogoName = imageName
                if weakSelf?.strLogo?.count ?? 0 > 0 {
                    self.imgViewLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(weakSelf?.strLogo ?? "")")), completed: nil)
                    
                    self.isAttachment = false
                    
                }
                
            }
            
        }
    }
    
    
    @objc func btnSubmitAction() {
        
        if txtViewDes.text?.count ?? 0 == 0 {
            
            txtViewDes.showError(message: "Required".localized())
            
            return
        }
        
        if txtCreatedBy.text?.count ?? 0 == 0 {
            
            txtCreatedBy.showError(message: "Required".localized())
            
            return
        }
        
        submitTestimonials()
        
    }
    
    func fetchTestimonialDetail() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getSellerTestimonialDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&testimonialCode=\(testimonialCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(TestimonialDetailApiModel.self, from: data)
                                
                                if let apiResponse:TestimonialModel = jsonResponse.sellerTestimonialDetails {
                                    
                                    weakSelf?.sellerTestimonialDetails = apiResponse
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func submitTestimonials(){
        
        let status:Int  = statusList?[selectedIndex].status ?? 0
        
        startAnimating()
        let params:NSDictionary = ["testimonialCode":testimonialCode,
                                   "profilePicture": strLogo ?? "",
                                   "ppFileName":strLogoName ?? "",
                                   "sellerCode":sellerCode,
                                   "status": status,
                                   "description":txtViewDes.text ?? "",
                                   "createdBy":txtCreatedBy.text ?? "",
                                   "userProfile":txtDesignation.text ?? ""
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerTestimonial?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                weakSelf?.delegate?.didAddedTestimonial()
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                weakSelf?.showErrorMessage(message: message)
                weakSelf?.dismiss(animated: true, completion: nil)
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
}
extension NewTestimonialVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            return CGSize.init(width: collectionViewStatus.frame.width / 3, height: 40.0)
            
        }
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if statusList?.count ?? 0 > 0 {
            
            return statusList?.count ?? 0
        }
        
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewStatus {
            
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForStatus(cell: cell, indexPath: indexPath)
                
            }
            
        }
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionViewStatus
            
        {
            selectedIndex = indexPath.row
            
            collectionViewStatus.reloadData()
            
        }
        
    }
    
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:SellerStatusModel = statusList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.statusTitle ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
}
