//
//  NewProductVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 14/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit


protocol NewProductVCDelegate: class {
    func didAddedProduct()
}

class NewProductVC: SendOTPViewController,CurrencySelectorVCDelegate {
    
    @IBOutlet weak var txtProductName: RCTextField!
    
    @IBOutlet weak var lblBranchestext: NKLabel!
    
    @IBOutlet weak var txtViewProductDes: NKTextView!
    
    @IBOutlet weak var lblBannersTitle: NKLabel!
    
    @IBOutlet weak var collectionViewAttachments: UICollectionView!
    
    @IBOutlet weak var lblAttachmentTitle: NKLabel!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var viewPrice: RCView!
    
    @IBOutlet weak var lblPriceType: NKLabel!
    
    @IBOutlet weak var viewFixedPriceType: RCView!
    
    @IBOutlet weak var heightViewFixedPrice: NSLayoutConstraint!
    
    @IBOutlet weak var lblFixedPriceType: NKLabel!
    
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var txtFixedCurrencyTitle: RCTextField!
    
    @IBOutlet weak var txtFixedAmount: RCTextField!
    
    @IBOutlet weak var viewRange: RCView!
    
    @IBOutlet weak var lblRangeTitle: NKLabel!
    
    @IBOutlet weak var txtRangeCurrencyTitle: RCTextField!
    
    @IBOutlet weak var txtStartRange: RCTextField!
    
    @IBOutlet weak var txtEndRange: RCTextField!
    
    @IBOutlet weak var heightViewRange: NSLayoutConstraint!
    
    @IBOutlet weak var viewGuideline: RCView!
    
    @IBOutlet weak var heightViewGuideline: NSLayoutConstraint!
    
    @IBOutlet weak var viewReferalFee: RCView!
    
    @IBOutlet weak var viewReferralFixedPrice: RCView!
    
    @IBOutlet weak var heightViewReferralFixedPrice: NSLayoutConstraint!
    
    @IBOutlet weak var viewReferralPercentage: RCView!
    
    @IBOutlet weak var heightViewReferralPercentage: NSLayoutConstraint!
    
    @IBOutlet weak var viewCharityOffer: RCView!
    
    @IBOutlet weak var viewCharityFixedPrice: RCView!
    
    @IBOutlet weak var heightViewCharityFixedPrice: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewBranchList: UITableView!
    
    @IBOutlet weak var tableViewResponseType: UITableView!
    
    @IBOutlet weak var heightViewBranchList: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewResponseList: NSLayoutConstraint!
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
    @IBOutlet weak var txtViewKeywords: NKTextView!
    
    @IBOutlet weak var topSpaceForFixedPrice: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceForReferralFixedPrice: NSLayoutConstraint!
    
    @IBOutlet weak var segmentLoyaltyBonus: UISegmentedControl!
    
    @IBOutlet weak var viewLoyaltyFixedPrice: RCView!
    
    @IBOutlet weak var viewLoyaltyPercentage: RCView!
    
    @IBOutlet weak var heightLoyaltyFixedPrice: NSLayoutConstraint!
    
    @IBOutlet weak var heightLoyaltyPercentage: NSLayoutConstraint!
    
    @IBOutlet weak var txtGuidelineCurrency: RCTextField!
    
    @IBOutlet weak var txtGuidelinePrice: RCTextField!
    
    @IBOutlet weak var txtReferralCurrency: RCTextField!
    
    @IBOutlet weak var txtReferralPrice: RCTextField!
    
    @IBOutlet weak var txtReferralPercentage: RCTextField!
    
    @IBOutlet weak var txtCharityCurrency: RCTextField!
    
    @IBOutlet weak var txtCharityPrice: RCTextField!
    
    @IBOutlet weak var txtLoyaltyCurrency: RCTextField!
    
    @IBOutlet weak var txtLoyaltyPrice: RCTextField!
    
    @IBOutlet weak var txtLoyaltyPercentage: RCTextField!
    
    @IBOutlet weak var segmentPriceType: UISegmentedControl!
        
    @IBOutlet weak var segmentReferralType: UISegmentedControl!
    
    @IBOutlet weak var btnLogo: UIButton!
    
    @IBOutlet weak var imgViewBtncamera: UIImageView!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var imgViewAttachementBtn: UIImageView!
    
    @IBOutlet weak var imgCheckBoxPriceType: UIImageView!
    
    @IBOutlet weak var imgCheckBoxReferralType: UIImageView!
    
    @IBOutlet weak var imgCheckBoxCharityOffer: UIImageView!
    
    @IBOutlet weak var imgCheckBoxLoyalty: UIImageView!
    
    @IBOutlet weak var heightViewPriceType: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewReferralType: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewCharity: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewLoyaltyBonus: NSLayoutConstraint!
    
    @IBOutlet weak var btnSubmit: NKButton!

    @IBOutlet weak var txtViewPricing: NKTextView!
    
    ////-->>>  New Changes
    
    @IBOutlet weak var collectionViewAttachment: UICollectionView!
    
    @IBOutlet weak var imgViewAttachment: UIImageView!
    
    @IBOutlet weak var tableViewGiftList: UITableView!
    
    @IBOutlet weak var heightViewGifts: NSLayoutConstraint!
    
    @IBOutlet weak var lblReferralAttachmentVisible: NKLabel!
    
    @IBOutlet weak var imgCheckBoxReferralAttachment: UIImageView!
    
    @IBOutlet weak var txtAvgReferralEarnings: RCTextField!
    
    @IBOutlet weak var txtAvgRefEarningsCurrency: RCTextField!
    
    @IBOutlet weak var btnChangeLog: UIButton!
    
    @IBOutlet weak var heightOfBtnChangeLog: NSLayoutConstraint!
    
    @IBOutlet weak var txtProductCode: RCTextField!
    
    @IBOutlet weak var lblSpecialMsgPartner: NKLabel!
    
    @IBOutlet weak var txtViewMsgPartner: NKTextView!
    
    @IBOutlet weak var lblSpecialMsgUser: NKLabel!
    
    @IBOutlet weak var txtViewMsgUser: NKTextView!
    
    @IBOutlet weak var lblProductDes: NKLabel!
    
    @IBOutlet weak var btnProductDes: UIButton!
    
    @IBOutlet weak var heightHtmlProductDes: NSLayoutConstraint!
    
    @IBOutlet weak var viewHtmlProductDes: RCView!
    
    @IBOutlet weak var topSpaceForHtmlDes: NSLayoutConstraint!
    
    @IBOutlet weak var viewBanners: RCView!
    
    @IBOutlet weak var heightBanners: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceForBanners: NSLayoutConstraint!
    
    @IBOutlet weak var viewAppointment: RCView!
    
    @IBOutlet weak var imgAppointment: UIImageView!
    
    var htmlDes:String = ""
    
    var attachmentTypes:[AttachmentTypeModel]?
    
    var attachmentsProduct:[AttachmentBannerTypeModel] = []
    
    var bannerType:Int = 0
    
    ////
    
    var sellerCode:Int = 0
    
    var productCode:Int = 0
    
    var attachments:[Any] = []
    
    var productDetail:ProductDetailModel?
    
    var responseTypes:[ResponseTypeModel]?
    
    var currency:[CurrencyModel]?
    
    var branchList:[ProductBranchModel]?
    
    var statusList:[SellerStatusModel]?
    
    var giftList:[ProductGiftModel]?
    
    var giftCode:[Int] = []
    
    var isSelected:Int = -1
    
    var branchCode:[Int] = []
    
    var strLogo:String?
    
    var thumbnailLogo:String?
    
    var selectedIndex:Int = 1
    
    var selectedCurrencyCode : CurrencyModel?
    
    var activeTextField = UITextField()
    
    var isPriceTypeSelected:Int = 0
    
    var isReferralTypeSelected:Int = 0
    
    var isLoyaltyTypeSelected:Int = 0
    
    var isCharityTypeSelected:Int = 0
    
    var isDetail:Bool = false
    
    var btnName = UIButton()
    
    weak var delegate : NewProductVCDelegate?
    
    var referralValue = 0.0
    var charityValue = 0.0
    var loyaltyValue = 0.0
    
    var priceType:Int = 0
    var charityType:Int = 0
    var loyaltyType:Int = 0
    var referralFeeType:Int = 0
    
    var avgRECurrencyId:Int?
    var productCurrencyId:Int?
    var referralCurrencyId:Int?
    var charityCurrencyId:Int?
    var loyaltyCurrencyId:Int?
    
    var minPrice = 0.0
    var maxPrice = 0.0
    
    var isReferralAttachmentVisible:Int = 1
    
    let segmentTitleAttributes: [NSAttributedString.Key: Any] = [
    .font: UIFont.fontForType(textType: 3),
    .foregroundColor: UIColor.white]
    
    let countAttributes: [NSAttributedString.Key: Any] = [
    .font: UIFont.fontForType(textType: 8),
    .foregroundColor: UIColor.getGradientColor(index: 0).first ?? 0,
    .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let desAttributes: [NSAttributedString.Key: Any] = [
       .font: UIFont.fontForType(textType: 8),
       .foregroundColor: UIColor.btnLinkColor,
       .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var isAdmin:Int?
          
    var isPartnerForCompany:Int?
    
    var companyType:Int = 1
    
    var isAppointmentSelected:Int = 0
    
    var isGoldPartner:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       viewLoad()
        
    }
    
    func viewLoad(){
        
        if companyType != 1 {
            
            topSpaceForHtmlDes.constant = 10
            heightHtmlProductDes.constant = 40
            viewHtmlProductDes.alpha = 1
            topSpaceForBanners.constant = 10
            heightBanners.constant = 160
            viewBanners.alpha = 1
            
        }
        else{
             topSpaceForHtmlDes.constant = 0
             heightHtmlProductDes.constant = 0
             viewHtmlProductDes.alpha = 0
             topSpaceForBanners.constant = 0
             heightBanners.constant = 0
             viewBanners.alpha = 0
        }
        
        navigationItem.title = "Product/Service".localized()
               
                //updateSegmentUI()
        
               txtProductName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Product Name".localized())

              // txtViewProductDes.changePlaceHolderColor = true
               txtViewProductDes.placeholder = "Product Description".localized()
               
              // lblBannersTitle.attributedText = addPlaceHolderWithText(placeholder: "Banners".localized())
        
        lblBannersTitle.text = "Banners".localized()
        
               lblBranchestext.attributedText = addPlaceHolderWithText(placeholder: "Branches".localized())
               
               //////////
               
               txtFixedCurrencyTitle.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
               
               txtFixedAmount.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
               
               txtRangeCurrencyTitle.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
               
               txtStartRange.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
               
               txtEndRange.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
               
               txtGuidelineCurrency.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
               
               txtGuidelinePrice.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
               
//               txtCharityCurrency.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
//
//               txtCharityPrice.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
               
//               txtLoyaltyCurrency.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
//
//               txtLoyaltyPrice.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
//
//               txtLoyaltyPercentage.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.0".localized())

               txtReferralCurrency.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
               
               txtReferralPrice.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
               
               txtReferralPercentage.attributedPlaceholder = addPlaceHolderWithText(placeholder: "0.00".localized())
               
        txtViewProductDes.placeholder = "Product description".localized()
               
               ////////

               
               //  btnName.setImage(UIImage(named: "walletIcon"), for: .normal)
               btnName.setTitle("Submit -->", for: .normal)
               btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
               btnName.setTitleColor(UIColor.white, for: .normal)
            //   btnName.addTarget(self, action: #selector(openHtmlView), for: .touchUpInside)
        
            btnName.addTarget(self, action: #selector(callSubmit), for: .touchUpInside)
               
               let rightBarButton = UIBarButtonItem()
               rightBarButton.customView = btnName
               self.navigationItem.rightBarButtonItem = rightBarButton
               
               heightViewFixedPrice.constant = 0
               heightViewRange.constant = 0
               heightViewGuideline.constant = 0
               heightViewReferralFixedPrice.constant = 0
               heightViewReferralPercentage.constant = 0
               heightViewCharityFixedPrice.constant = 0
               heightLoyaltyFixedPrice.constant = 0
               heightLoyaltyPercentage.constant = 0
               
               heightViewPriceType.constant = 40
               heightViewReferralType.constant = 40
               heightViewCharity.constant = 0
               heightViewLoyaltyBonus.constant = 0
               
               txtRangeCurrencyTitle.delegate = self
               txtFixedCurrencyTitle.delegate = self
               txtLoyaltyCurrency.delegate = self
               txtCharityCurrency.delegate = self
               txtReferralCurrency.delegate = self
               txtGuidelineCurrency.delegate = self
               txtLoyaltyPercentage.delegate = self
               txtLoyaltyPrice.delegate = self
               txtCharityPrice.delegate = self
               txtReferralPercentage.delegate = self
               txtReferralPrice.delegate = self
               txtLoyaltyPercentage.delegate = self
               txtGuidelinePrice.delegate = self
               txtAvgReferralEarnings.delegate = self
               txtAvgRefEarningsCurrency.delegate = self
        
               txtAvgReferralEarnings.tag = 100
               txtLoyaltyPrice.tag = 100
               txtLoyaltyPercentage.tag = 100
               txtReferralPercentage.tag = 100
               txtReferralPrice.tag = 100
               txtFixedAmount.tag = 100
               txtStartRange.tag = 100
               txtEndRange.tag = 100
        
               btnLogo.addTarget(self, action: #selector(btnLogoAction), for: .touchUpInside)
               imgViewBtncamera.setImageColor(color: UIColor.white)
               
               imgViewAttachementBtn.setImageColor(color: UIColor.white)
               
               txtProductName.delegate = self
               
               txtViewProductDes.delegate = self
               
               btnSubmit.setTitle("Submit".localized(), for: .normal)
        
               btnChangeLog.alpha = 0
        
              heightOfBtnChangeLog.constant = 0
              //heightViewReferralType.constant = 173
        
        let changeLogTitle = NSMutableAttributedString(string: "\("View change log".localized())",
                   attributes: countAttributes)
        
        btnChangeLog.setAttributedTitle(changeLogTitle, for: .normal)
               
               if isDetail == true {
                   
                   fetchProductDetail()
                   
                   btnSubmit.setTitle("Update".localized(), for: .normal)
                   btnName.setTitle("Update -->".localized(), for: .normal)
                
                btnName.alpha = 0
                btnSubmit.alpha = 0
                
                if isAdmin == 1 || isPartnerForCompany == 1 {
                                 
                         btnName.alpha = 1
                         btnSubmit.alpha = 1
                    
                      }
    
               }
                   
               updateCheckBoxReferralAttachment(type: isReferralAttachmentVisible)
        
        let productDesTitle = NSMutableAttributedString(string: "Click here to add product description".localized(),
                          attributes: desAttributes)
        
        lblProductDes.attributedText = productDesTitle
               
             //  btnProductDes.setAttributedTitle(productDesTitle, for: .normal)
        
        btnProductDes.addTarget(self, action: #selector(openHtmlView), for: .touchUpInside)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if giftList?.count ?? 0 > 0 {
            
        let height = giftList?.count ?? 0
            
        heightViewGifts.constant = CGFloat((height * 44) + 40)
        
        tableViewGiftList.reloadData()
        
        }
        
        else{
            
             heightViewGifts.constant = 0
            
        }
           
            let heightBranch = branchList?.count ?? 0
                       
            heightViewBranchList.constant = CGFloat((heightBranch * 44) + 40)
                   
            tableViewBranchList.reloadData()
        
        
        let heightResponse = responseTypes?.count ?? 0
                   
        heightViewResponseList.constant = CGFloat((heightResponse * 44) + 40)
               
        tableViewResponseType.reloadData()
            
        
    }
    
    func updateUI() {
        
        if let model:ProductDetailModel = productDetail {
            
            txtProductName.text = model.productName ?? ""
            
            let shortDes = model.shortDescription ?? ""
            let des = model.description ?? ""
            
            if companyType != 1 {
                
              
                txtViewProductDes.text = shortDes.removeWhitespaceSpecial()
                htmlDes = des.removeWhitespaceSpecial()
                
            }
            else{
                txtViewProductDes.text = des.removeWhitespaceSpecial()
            }
            attachments = model.banners
            selectedIndex = model.status ?? 0
            attachmentsProduct = model.attachmentList ?? []
            txtFixedCurrencyTitle.text = model.productCurrencySymbol ?? ""
            txtRangeCurrencyTitle.text = model.productCurrencySymbol ?? ""
            txtGuidelineCurrency.text = model.productCurrencySymbol ?? ""
            txtCharityCurrency.text = model.charityCurrencySymbol ?? ""
            
           // txtLoyaltyPrice.text = "\(model.loyaltyValue ?? 0.0)"
            
           // txtGuidelinePrice.text = model.priceGuideline ?? ""
            
            txtViewPricing.text = model.priceGuideline ?? ""
            
            isReferralAttachmentVisible = model.showAttachmentsToBuyer ?? 0
            
            updateCheckBoxReferralAttachment(type: isReferralAttachmentVisible)
            
            txtAvgRefEarningsCurrency.text = model.avgRECurrencySymbol ?? ""
            
            txtAvgReferralEarnings.text = "\(Int(model.avgREAmount ?? 0.0))"
            
            avgRECurrencyId = model.avgRECurrencyId ?? 0
             
            productCurrencyId = model.productCurrencyId ?? 0
            
            referralCurrencyId = model.referralCurrencyId ?? 0
            
            charityCurrencyId = model.charityCurrencyId ?? 0
            
            loyaltyCurrencyId = model.loyaltyCurrencyId ?? 0
            
            if model.referralCurrencySymbol?.count ?? 0 > 0 {
                
                txtReferralCurrency.text = "\( model.referralCurrencySymbol ?? "")"
            }
            
            if model.loyaltyCurrencySymbol?.count ?? 0 > 0 {
                
                txtLoyaltyCurrency.text = "\( model.loyaltyCurrencySymbol ?? "")"
            }
            
            if model.notesForReferralMember?.count ?? 0 > 0 {
                
                txtViewNotes.placeholder = ""
                
            }
            
            if model.description?.count ?? 0 > 0 {
                
                txtViewProductDes.placeholder = ""
                
            }
            
            txtViewNotes.text = model.notesForReferralMember ?? ""
            
            strLogo = model.productLogo ?? ""
            
            thumbnailLogo = model.prodThumbnailLogo ?? ""
            
            if strLogo?.count ?? 0 > 0 {
                self.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(strLogo ?? "")")), completed: nil)
            }
            
//            if thumbnailLogo?.count ?? 0 > 0 {
////                self.imgLogo.sd_s
//            }
            
            if model.keywords?.count ?? 0 > 0 {
                
                txtViewKeywords.text = model.keywords ?? ""
                
                txtViewKeywords.placeholder = ""
                
            }
            
            if model.referralFormMsg?.count ?? 0 > 0 {
                
                txtViewMsgPartner.placeholder = ""
                
                txtViewMsgPartner.text = model.referralFormMsg ?? ""
                
            }
            
            if model.enquiryOrApplyJobFormMsg?.count ?? 0 > 0 {
                           
               txtViewMsgUser.placeholder = ""
                           
               txtViewMsgUser.text = model.enquiryOrApplyJobFormMsg ?? ""
            }
            
            if model.productCodeText?.count ?? 0 > 0 {
                           
               txtProductCode.placeholder = ""
                           
               txtProductCode.text = model.productCodeText ?? ""
                 
            }
            
            if htmlDes.count > 0 {
                              
                let productDesTitle = NSMutableAttributedString(string: "Click here to update product description".localized(),attributes:desAttributes)
                              
                        lblProductDes.attributedText = productDesTitle
                
                              
            }
            
            if model.isPriceType == 1 {
                
                imgCheckBoxPriceType.image = UIImage(named: "checkBoxFilled")
                
                isPriceTypeSelected = 1
                
                if model.priceType ?? 0 == 1 {
                
                if model.maxPrice ?? 0.0 > 0.0 {
                    
                    txtFixedAmount.text = "\(model.maxPrice ?? 0.0)"
                    
                }
               }
                
                if model.priceType ?? 0 == 2 {
                    
                    if model.minPrice ?? 0.0 >= 0.0 {
                        
                        txtStartRange.text = "\(model.minPrice ?? 0.0)"
                        
                    }
                    
                    if model.maxPrice ?? 0.0 > 0.0 {
                        
                        txtEndRange.text = "\(model.maxPrice ?? 0.0)"
                        
                    }
                }
                
                if model.priceType ?? 0 == 3 {
                                   
                 if model.maxPrice ?? 0.0 > 0.0 {
                                       
                txtGuidelinePrice.text = "\(model.maxPrice ?? 0.0)"
                                       
                             }
                        }
                
         let priceTypeSegment = model.priceType ?? 0
                
               segmentPriceType.selectedSegmentIndex = priceTypeSegment - 1
                
                segmentPriceType.setSegmentStyle()
                
                segmentPriceTypeAction(segmentPriceType)
                
            }
            
            if model.isCharityOffer == 1 {
                
               // imgCheckBoxCharityOffer.image = UIImage(named: "checkBoxFilled")
//                heightViewCharity.constant = 140
//                heightViewCharityFixedPrice.constant = 85
                  heightViewCharity.constant = 0
                  heightViewCharityFixedPrice.constant = 0
                 isCharityTypeSelected = 1
                
                charityType = model.charityType ?? 0
//
//            if model.charityType == 2 {
//
////                if model.charityValue ?? 0.0 > 0.0 {
////
////                    txtCharityPrice.text = "\( model.charityValue ?? 0.0)"
////
////                }
//
//               }
                
//                if model.charityType == 1 {
//
//                 if model.charityValue ?? 0.0 > 0.0 {
//
//                     txtCharityPrice.text = "\( model.charityValue ?? 0.0)"
//                 }
//
//                }
            }
            
            if model.isLoyaltyBonus == 1 {
                
                imgCheckBoxLoyalty.image = UIImage(named: "checkBoxFilled")
                
                isLoyaltyTypeSelected = 1
                
                loyaltyType = model.loyaltyType ?? 0
//
//                if model.loyaltyType == 2 {
//
//                    if model.loyaltyValue ?? 0 > 0 {
//
//                        txtLoyaltyPrice.text = "\( model.loyaltyValue ?? 0.0)"
//                    }
//
                   heightLoyaltyFixedPrice.constant = 0
//
                    heightViewLoyaltyBonus.constant = 0
                    
                   // segmentLoyaltyBonus.selectedSegmentIndex = 0
                    
              //  }
                
//                if model.loyaltyType == 1 {
//
//                    if model.loyaltyValue ?? 0 > 0 {
//
//                        txtLoyaltyPercentage.text = "\( model.loyaltyValue ?? 0.0)"
//                    }
//
//                    segmentLoyaltyBonus.selectedSegmentIndex = 1
//
//                }
                
//                segmentLoyaltyBonus.setSegmentStyle()
//
//                segmentLoyaltyAction(segmentLoyaltyBonus)
            }
            
        if model.isReferralFeeType == 1 {
                
                referralFeeType = model.referralFeeType ?? 0
                
                imgCheckBoxReferralType.image = UIImage(named: "checkBoxFilled")
                
                isReferralTypeSelected = 1
                
        if model.referralFeeType == 2 {
                
            if model.referralFee ?? 0 > 0 {
                    
                    txtReferralPrice.text = "\( model.referralFee ?? 0)"
                }
                    
                    segmentReferralType.selectedSegmentIndex = 0
                    
             }
                if model.referralFeeType == 1 {
                    
                    if model.referralFee ?? 0 > 0 {
                        
                        txtReferralPercentage.text = "\( model.referralFee ?? 0)"
                    }
                    
                    segmentReferralType.selectedSegmentIndex = 1
                    
                }
                
//                    btnChangeLog.alpha = 1
//                    heightOfBtnChangeLog.constant = 30
//                   heightViewReferralType.constant = 200
                 segmentReferralType.setSegmentStyle()
                 segmentFeeType(segmentReferralType)
                
            }
            
            branchCode = model.branchCode ?? []
            
            giftCode = model.gifts ?? []
            
            let response = model.responseType ?? 0
            
            isSelected = response - 1
            
            selectedIndex = model.status ?? 0
            
            isAppointmentSelected = model.enableAppointment ?? 0
            
             updateAppointment(type: isAppointmentSelected)
            
            for index in 0..<branchCode.count{
                
                for modelMain in branchList ?? []  {
                    
                    if modelMain.branchCode == branchCode[index]{
                        
                        modelMain.isSelected = 1
                        
                    }
                }
            }
            
        for index in 0..<giftCode.count{
                
                for modelMain in giftList ?? []  {
                    
                    if modelMain.giftCode == giftCode[index]{
                        
                        modelMain.isSelected = 1
                        
                    }
                }
            
            tableViewGiftList.reloadData()
            
            }
        
            
            tableViewBranchList.reloadData()
            
            collectionViewStatus.reloadData()
            
            collectionViewAttachments.reloadData()
            
            collectionViewAttachment.reloadData()
            
            tableViewResponseType.reloadData()
            
          //  updateSegmentUI()
            
        }
        
        
    }
    
    
    
    func updateCheckBoxReferralAttachment(type:Int){
        
        if type == 0 {
            
            imgCheckBoxReferralAttachment.image = UIImage.init(imageLiteralResourceName: "checkBox")
            
        }
        else{
            imgCheckBoxReferralAttachment.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
        }
        
    }

    
    @objc func btnLogoAction() {
        
        openImagePicker()
        
         isBanner = false
        
        weak var weakSelf = self
        
        isLogo = true
        
        didUploadLogoImage = { status, urlString in
                   weakSelf?.thumbnailLogo = urlString
            if weakSelf?.thumbnailLogo?.count ?? 0 > 0 {
                self.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(weakSelf?.strLogo ?? "")")), completed: nil)

                }
               self.isLogo = false
            }
        
        didUploadImage = { status, urlString in
            weakSelf?.strLogo = urlString
            if weakSelf?.strLogo?.count ?? 0 > 0 {
                self.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(weakSelf?.strLogo ?? "")")), completed: nil)
                
               // self.isLogo = false
            }
        }
        
    }
    
    func showPopUpForDeletion(){
      
        let alert = UIAlertController(title: nil, message: "Multiple banners are allowed only for Paltinum type companies. Do you want to replace it?".localized(), preferredStyle: .alert)
               
               alert.view.tintColor = UIColor.btnLinkColor
               
               alert.addAction(UIAlertAction(title: "No".localized(),
                                             style: UIAlertAction.Style.default,
                                             handler: {(_: UIAlertAction!) in
                    
                    self.dismiss(animated: true, completion: nil)
                                               
               }))
               
               alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
                   
                self.isGoldPartner = 1
                
                      self.openBannerImagePicker()
                   
                 //  self.dismiss(animated: true, completion: nil)
                   
               }))
               
               self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    func openBannerImagePicker(){
       
        if companyType == 2 {
        
          if isGoldPartner == 0 {
            
            showPopUpForDeletion()
            
            return
            
           }
        
         }
        
        // startAnimating()
        isAttachment = true
        isBanner = false
        
        openImagePicker()
                
        weak var weakSelf = self

        didUploadHelpDeskImage = {status, urlString, imageName in
            //     weakSelf?.stopAnimating()
            if status {
                print(urlString, imageName)
                
                if self.attachments.count > 0 {
                                  
                    self.attachments.removeAll()
                    // self.collectionViewAttachments.reloadData()
                                  
                              }
                var arrayEachImage : [String : String] = [:]
                arrayEachImage["cdnPath"] = urlString
                arrayEachImage["fileName"] = imageName
                weakSelf?.attachments.append(arrayEachImage)
                weakSelf?.collectionViewAttachments.reloadData()
                self.isAttachment = false
                self.isBanner = false
                self.isGoldPartner = 0
            }
            
        }
    }
    
    
    @IBAction func btnAttachmentAction(_ sender: Any) {
        
        openBannerImagePicker()
        
    }
    
    
    @IBAction func btnAppointmentAction(_ sender: UIButton) {
        
        if isAppointmentSelected == 0 {
            
            isAppointmentSelected = 1
        }
        else{
            
            isAppointmentSelected = 0
        }
        
        updateAppointment(type: isAppointmentSelected)
        
    }
    
    func updateAppointment(type:Int){
        
        if type == 1 {
                   
             imgAppointment.image = UIImage.init(named: "checkBoxFilled")
            
         }
        else{
                   
             imgAppointment.image = UIImage.init(named: "checkBox")
            
       }
    }
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachments.count > sender.tag {
            attachments.remove(at: sender.tag)
            collectionViewAttachments.reloadData()
        }
    }
    
    
    
    @objc func deleteProductImage(_ sender: NKButton) {
        if attachmentsProduct.count > sender.tag {
            attachmentsProduct.remove(at: sender.tag)
            collectionViewAttachment.reloadData()
        }
    }
    
    
    
    @IBAction func segmentPriceTypeAction(_ sender: UISegmentedControl) {
        
        segmentPriceType.setSegmentStyle()
        
        switch sender.selectedSegmentIndex {
        case 0:
            // topSpaceForFixedPrice.constant = 16
            heightViewFixedPrice.constant = 120
            heightViewRange.constant = 0
            heightViewGuideline.constant = 0
            heightViewPriceType.constant = 160
            break
        case 1:
            // topSpaceForFixedPrice.constant = 16
            heightViewFixedPrice.constant = 0
            heightViewRange.constant = 120
            heightViewGuideline.constant = 0
            heightViewPriceType.constant = 160
            break
        case 2:
            // topSpaceForFixedPrice.constant = 16
            heightViewFixedPrice.constant = 0
            heightViewRange.constant = 0
            heightViewGuideline.constant = 180
            heightViewPriceType.constant = 250
            break
        default:
            heightViewFixedPrice.constant = 0
            heightViewRange.constant = 0
            heightViewGuideline.constant = 0
            heightViewPriceType.constant = 40
        }
    }
    
    @IBAction func segmentFeeType(_ sender: UISegmentedControl) {
        
       segmentReferralType.setSegmentStyle()
        
        switch sender.selectedSegmentIndex {
        case 0:
            //  topSpaceForReferralFixedPrice.constant = 16
            heightViewReferralFixedPrice.constant = 120
            heightViewReferralPercentage.constant = 0
            heightViewReferralType.constant = 160
            if isDetail == true {
                btnChangeLog.alpha = 1
                heightOfBtnChangeLog.constant = 30
                heightViewReferralType.constant = 190
            }
            
            break
        case 1:
            // topSpaceForReferralFixedPrice.constant = 16
            heightViewReferralFixedPrice.constant = 0
            heightViewReferralPercentage.constant = 80
            heightViewReferralType.constant = 130
           if isDetail == true {
                btnChangeLog.alpha = 1
                heightOfBtnChangeLog.constant = 30
                heightViewReferralType.constant = 160
            }
            
            break
        default:
            heightViewReferralFixedPrice.constant = 0
            heightViewReferralPercentage.constant = 0
            heightViewReferralType.constant = 40
        }
        
    }
    
    @IBAction func segmentLoyaltyAction(_ sender: UISegmentedControl) {
        
      segmentLoyaltyBonus.setSegmentStyle()
        
        switch sender.selectedSegmentIndex {
        case 0:
            
            heightLoyaltyFixedPrice.constant = 120
            heightLoyaltyPercentage.constant = 0
            heightViewLoyaltyBonus.constant = 160
            
            break
        case 1:
            // bottomSpaceForCharityOffer.constant = 16
            heightLoyaltyFixedPrice.constant = 0
            heightLoyaltyPercentage.constant = 80
            heightViewLoyaltyBonus.constant = 130
            
            break
        default:
            heightLoyaltyFixedPrice.constant = 0
            heightLoyaltyPercentage.constant = 0
            heightViewLoyaltyBonus.constant = 0
        }
        
    }
    
    
    @IBAction func btnPriceTypeAction(_ sender: Any) {
        
        if isPriceTypeSelected == 0 {
            
            imgCheckBoxPriceType.image = UIImage(named: "checkBoxFilled")
            
           // heightViewPriceType.constant = 80
            
            segmentPriceType.alpha = 1
            
            isPriceTypeSelected = 1
            
             segmentPriceTypeAction(segmentPriceType)
        }
        else{
            
            imgCheckBoxPriceType.image = UIImage(named: "checkBox")
            
            heightViewPriceType.constant = 40
            
            segmentPriceType.alpha = 0
            
            isPriceTypeSelected = 0
        }
    }
    
    @IBAction func btnReferralTypeAction(_ sender: Any) {
        
        if isReferralTypeSelected == 0 {
            
            imgCheckBoxReferralType.image = UIImage(named: "checkBoxFilled")
            
           // heightViewReferralType.constant = 80
            
            segmentReferralType.alpha = 1
            
            segmentFeeType(segmentReferralType)
            
            isReferralTypeSelected = 1
            
            btnChangeLog.alpha = 0
                   
            heightOfBtnChangeLog.constant = 0
        }
        else{
            
            imgCheckBoxReferralType.image = UIImage(named: "checkBox")
            
            heightViewReferralType.constant = 40
            
            segmentReferralType.alpha = 0
            
            isReferralTypeSelected = 0
            
            btnChangeLog.alpha = 0
                   
            heightOfBtnChangeLog.constant = 0
        }
        
    }
    
    
    @IBAction func btnCharityOfferAction(_ sender: Any) {/////checkbox selected vivek
        
        if isCharityTypeSelected == 0 {
            
            imgCheckBoxCharityOffer.image = UIImage(named: "checkBoxFilled")
            heightViewCharity.constant = 140
            heightViewCharityFixedPrice.constant = 85
            isCharityTypeSelected = 1
        }
        else{
            
            imgCheckBoxCharityOffer.image = UIImage(named: "checkBox")
            heightViewCharity.constant = 40
            heightViewCharityFixedPrice.constant = 0
            isCharityTypeSelected = 0
        }
    }
    
    
    @IBAction func btnLoyaltyAction(_ sender: Any) {
        
         if isLoyaltyTypeSelected == 0 {
                   
                   imgCheckBoxLoyalty.image = UIImage(named: "checkBoxFilled")
                   
                   heightViewLoyaltyBonus.constant = 80
                   
                   segmentLoyaltyBonus.alpha = 1
                   
                   isLoyaltyTypeSelected = 1
               }
               else{
                   
                   imgCheckBoxLoyalty.image = UIImage(named: "checkBox")
                   
                   isLoyaltyTypeSelected = 0
                   
                   heightViewLoyaltyBonus.constant = 40
                   
                   segmentLoyaltyBonus.alpha = 0
               }
        
    }
    
    @IBAction func btnReferralAttachmentAction(_ sender: Any) {
        
        if isReferralAttachmentVisible == 0 {
            
            isReferralAttachmentVisible = 1
                     
        updateCheckBoxReferralAttachment(type: isReferralAttachmentVisible)
                     
    }
    else{
           isReferralAttachmentVisible = 0
                                  
        updateCheckBoxReferralAttachment(type: isReferralAttachmentVisible)
            
      }
        
    }
    
    @IBAction func btnChangeLogAction(_ sender: Any) {
        
        if let model:ProductDetailModel = productDetail
        {
                   
          let storyBoard = UIStoryboard.init(name: ProductSB, bundle: nil)
          let newProductPage:ProductChangeLogVC = storyBoard.instantiateViewController(withIdentifier: "ProductChangeLogVC") as! ProductChangeLogVC
          newProductPage.sellerCode = model.sellerCode ?? 0
          newProductPage.productCode = model.productCode ?? 0
          navigationController?.pushViewController(newProductPage, animated: true)
                   
        }
        
    }
    
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        callSubmit()
    }
    
    @objc func callSubmit(){
        
        branchCode = []
        
        if let intArray:[ProductBranchModel] = branchList{
            
            for model in intArray{
                
                if model.isSelected == 1{
                    
                    branchCode.append(model.branchCode ?? 0)
                    
                }
                
            }
            
        }
        
      
        
//        if strLogo?.count ?? 0 == 0 {
//            showErrorMessage(message: "Please add logo".localized())
//            return
//        }
            
        if txtProductName.text?.count ?? 0 == 0 {
            txtProductName.showError(message: "Required".localized())
            return
        }
            
//
//        else if txtViewProductDes.text?.count ?? 0 == 0 {
//            txtViewProductDes.showError(message: "Required".localized())
//            return
//        }
        
//        if attachments.count == 0 {
//
//            showErrorMessage(message: "Please add banner".localized())
//            return
//        }
        
        if branchCode.count == 0 {
            
            showErrorMessage(message: "Please select Branch".localized())
            return
        }
        
        if isPriceTypeSelected == 1 {
        
        switch segmentPriceType.selectedSegmentIndex {
            
        case 0:
            if txtFixedCurrencyTitle.text?.count ?? 0 == 0 {
                
                txtFixedCurrencyTitle.showError(message: "Required".localized())
                
                return
            }
            
            if txtFixedAmount.text?.count ?? 0 == 0 {
                
                txtFixedAmount.showError(message: "Required".localized())
                
                return
                
            }
            
            if let cost = Double(txtFixedAmount.text ?? "0.0") {
                if cost > 0.0 {
                    maxPrice = cost
                }
            }
            
            priceType = 1
            
        case 1:
            if txtRangeCurrencyTitle.text?.count ?? 0 == 0 {
                
                txtRangeCurrencyTitle.showError(message: "Required".localized())
                
                return
                
            }
            
            if txtStartRange.text?.count ?? 0 == 0 {
                
                txtStartRange.showError(message: "Required".localized())
                
                return
            }
            
            if txtEndRange.text?.count ?? 0 == 0 {
                
                txtEndRange.showError(message: "Required".localized())
                
                
                return
            }
            
            if let cost = Double(txtStartRange.text ?? "0.0") {
                           
                    minPrice = cost
             }
            
            if let cost = Double(txtEndRange.text ?? "0.0") {
                 if cost > 0.0 {
                        maxPrice = cost
                    }
                                
               }
            
             priceType = 2
            
        case 2:
            
            if txtGuidelineCurrency.text?.count ?? 0 == 0 {
                
                txtGuidelineCurrency.showError(message: "Required".localized())
                
                return
                
            }
            
            if txtGuidelinePrice.text?.count ?? 0 == 0 {
                
                txtGuidelinePrice.showError(message: "Required".localized())
                
                return
            }
            
            if let cost = Double(txtGuidelinePrice.text ?? "0.0") {
                if cost > 0.0 {
                    maxPrice = cost
                  }
                        
               }
            
             priceType = 3
            
            
        default: print("No")
            
        }
      }
      
//        if isCharityTypeSelected == 1 {
//
//            if txtCharityCurrency.text?.count ?? 0 == 0 {
//
//                txtCharityCurrency.showError(message: "Required".localized())
//
//                return
//
//            }
//
//            if txtCharityPrice.text?.count ?? 0 == 0 {
//
//                txtCharityPrice.showError(message: "Required".localized())
//
//
//                return
//
//            }
//
//            if let cost = Double(txtCharityPrice.text ?? "0.0") {
//                charityValue = cost
//
//            }
//            charityType = 2
//      }
        
//        if isLoyaltyTypeSelected == 1 {
//
//      switch segmentLoyaltyBonus.selectedSegmentIndex {
//
//             case 0:
//
//            if txtLoyaltyCurrency.text?.count ?? 0 == 0 {
//
//                txtLoyaltyCurrency.showError(message: "Required".localized())
//
//                return
//
//            }
//
//            if txtLoyaltyPrice.text?.count ?? 0 == 0 {
//
//                txtLoyaltyPrice.showError(message: "Required".localized())
//
//
//                return
//
//            }
//
//            if let cost = Double(txtLoyaltyPrice.text ?? "0.0") {
//                loyaltyValue = cost
//
//            }
//
//            loyaltyType = 2
//
//      case 1:
//              if txtLoyaltyPercentage.text?.count ?? 0 == 0{
//
//               txtLoyaltyPercentage.showError(message: "Required".localized())
//
//                return
//            }
//
//            if let cost = Double(txtLoyaltyPercentage.text ?? "0.0") {
//
//                loyaltyValue = cost
//
//            }
//              loyaltyType = 1
//
//      default:
//                 print("NO::::Jana")
//            }
//     }
        if isReferralTypeSelected == 1 {
        
        switch segmentReferralType.selectedSegmentIndex {
            
        case 0:
            if txtReferralCurrency.text?.count ?? 0 == 0 {
                
                txtReferralCurrency.showError(message: "Required".localized())
                
                return
                
            }
            
            if txtReferralPrice.text?.count ?? 0 == 0 {
                
                txtReferralPrice.showError(message: "Required".localized())
                
                
                return
                
            }
            
            if let cost = Double(txtReferralPrice.text ?? "0.0") {
                referralValue = cost
            }
            
            referralFeeType = 2
            
        case 1:
           
            if txtReferralPercentage.text?.count ?? 0 == 0 {
                
                txtReferralPercentage.showError(message: "Required".localized())
                
                return
                
            }
            
            if let cost = Double(txtReferralPercentage.text ?? "0.0") {
                referralValue = cost
            }
            
            if referralValue <= 0.0 {
                
                txtReferralPercentage.showError(message: "Required".localized())
                  return
            }
            
             referralFeeType = 1
            
        default:
            print("NO::::Jana")
            
        }
      }
        
        
        submitProductDetails()
        
        
    }
    
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
        
        if txtFixedCurrencyTitle == activeTextField {
            txtFixedCurrencyTitle.text = selectedCurrencyCode?.currencySymbol ?? ""
            productCurrencyId = selectedCurrencyCode?.currencyId ?? 0
            
        }
        if txtRangeCurrencyTitle == activeTextField {
            txtRangeCurrencyTitle.text = selectedCurrencyCode?.currencySymbol ?? ""
            productCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        }
        if txtReferralCurrency == activeTextField {
            txtReferralCurrency.text = selectedCurrencyCode?.currencySymbol ?? ""
            referralCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        }
//        if txtCharityCurrency == activeTextField {
//            txtCharityCurrency.text = selectedCurrencyCode?.currencySymbol ?? ""
//            charityCurrencyId = selectedCurrencyCode?.currencyId ?? 0
//        }
//        if txtLoyaltyCurrency == activeTextField {
//            txtLoyaltyCurrency.text = selectedCurrencyCode?.currencySymbol ?? ""
//            loyaltyCurrencyId = selectedCurrencyCode?.currencyId ?? 0
//        }
        if txtGuidelineCurrency == activeTextField {
            txtGuidelineCurrency.text = selectedCurrencyCode?.currencySymbol ?? ""
            productCurrencyId = selectedCurrencyCode?.currencyId ?? 0
        }
        if txtAvgRefEarningsCurrency == activeTextField {
            txtAvgRefEarningsCurrency.text = selectedCurrencyCode?.currencySymbol ?? ""
             avgRECurrencyId = selectedCurrencyCode?.currencyId ?? 0
        }
        
    }
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
   @objc func openHtmlView() {
    
     let storyBoard = UIStoryboard.init(name: ProductSB, bundle: nil)
    
        if let htmlSelector:HtmlEditorVC = storyBoard.instantiateViewController(withIdentifier: "HtmlEditorVC") as? HtmlEditorVC {
            
            htmlSelector.htmlText = htmlDes
            htmlSelector.delegate = self
            
           navigationController?.pushViewController(htmlSelector, animated: true)
           
        }
     }
    
    
    func submitProductDetails(){
        
        startAnimatingAfterSubmit()
        
        giftCode = []
        
        var avgRefVal = 0.0
        
        var shortDescription = ""
        var description = ""
               
        if companyType != 1{
            
            shortDescription = txtViewProductDes.text ?? ""
            description = htmlDes
            
         }
        else{
            
            description = txtViewProductDes.text ?? ""
            shortDescription = ""
        }
        
        if let cost = Double(txtAvgReferralEarnings.text ?? "0.0") {
          avgRefVal = cost
        }
               
        if let intArray:[ProductGiftModel] = giftList {
                   
            for model in intArray  {
                       
                if model.isSelected == 1 {
                           
                        giftCode.append(model.giftCode ?? 0)
                           
                    }
                }
            }
        
        var responseType:Int = 0
        
        if isSelected > -1 {
            
            responseType = isSelected + 1
            
        }
        
        var array : [Any] = []
        
        for dict in attachmentsProduct
        {
            var model:[String : Any] = [:]
            model["cdnPath"] = dict.cdnPath
            model["attachmentType"] = dict.attachmentType
            model["fileName"] = dict.fileName
            array.append(model)
        }
        
       
        
        
        let params:NSDictionary = ["sellerCode": sellerCode,
                                   "productCode": productCode,
                                   "productName" : txtProductName.text ?? "",
                                   "shortDescription":shortDescription,
                                   "description" : description,
                                   "productLogo" : strLogo ?? "",
                                   "prodThumbnailLogo":thumbnailLogo ?? "",
                                   "banners":attachments,
                                   "priceType":priceType,
                                   "productCurrencyId" : productCurrencyId ?? 0,
                                   "minPrice" :minPrice,
                                   "maxPrice" : maxPrice,
                                   "priceGuideline":txtViewPricing.text ?? "",
                                   "referralFeeType" : referralFeeType,
                                   "referralCurrencySymbol":selectedCurrencyCode?.currencySymbol ?? "",
                                   "referralCurrencyId":referralCurrencyId ?? 0,
                                   "referralFee" : referralValue,
                                   "notesForReferralMember" : txtViewNotes.text ?? "",
                                   "status" : selectedIndex,
                                   "responseType":responseType,
                                   "branchCode": branchCode,
                                   "charityType" : charityType,
                                   "charityCurrencyId" : charityCurrencyId ?? 0,
                                   "charityCurrencySymbol":txtCharityCurrency.text ?? "",
                                   "charityValue" : charityValue,
                                   "loyaltyType": loyaltyType,
                                   "loyaltyValue": loyaltyValue,
                                   "loyaltyCurrencyId": loyaltyCurrencyId ?? 0,
                                   "loyaltyCurrencySymbol":txtLoyaltyCurrency.text ?? "",
                                   "keywords":txtViewKeywords.text ?? "",
                                   "isPriceType":isPriceTypeSelected,
                                   "isLoyaltyBonus":isLoyaltyTypeSelected,
                                   "isCharityOffer":isCharityTypeSelected,
                                   "attachmentList":array,
                                   "isReferralFeeType":isReferralTypeSelected,
                                   "gifts":giftCode,
                                   "showAttachmentsToBuyer":isReferralAttachmentVisible,
                                   "avgRECurrencyId": avgRECurrencyId ?? 0,
                                   "avgREAmount": avgRefVal,
                                   "productCodeText":txtProductCode.text ?? "",
                                   "referralFormMsg":txtViewMsgPartner.text ?? "",
                                   "enquiryOrApplyJobFormMsg":txtViewMsgUser.text ?? "",
                                   "enableAppointment":isAppointmentSelected,
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerProducts?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                //                            let apiResponse = try decoder.decode(CategoryListModel.self, from: data)
                                //                            weakSelf?.categoryNewList = apiResponse.categoriesList ?? []
                                //                            weakSelf?.searchCategoryList = apiResponse.categoriesList ?? []
                                //
                                //                            DispatchQueue.main.async {
                                //
                                //                                weakSelf?.collectionViewCategory.reloadData()
                                //                            }
                                
                                weakSelf?.delegate?.didAddedProduct()
                                weakSelf?.showInfoMessage(message: message.localized())
                                
                                let isUpdated:[String: Int] = ["update": 1]

                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callSearch"), object: nil, userInfo: isUpdated)
                                
                                weakSelf?.navigationController?.popViewController(animated: true)
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    func fetchProductDetail() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/productDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&productCode=\(productCode)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ProductDetailApiModel.self, from: data)
                                
                                if let apiResponse:ProductDetailModel = jsonResponse.productDetails {
                                    
                                    weakSelf?.productDetail = apiResponse
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    @IBAction func btnAttachmentProductAction(_ sender: Any) {
        
        isBanner = false
        isAttachment = true
        isDocument = true
        
        if attachmentTypes?.count ?? 0 > 0 {
            
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alert.view.tintColor = UIColor.btnLinkColor
            
            for model in attachmentTypes ?? [] {
                
                print(model.attachmentType ?? 0)
                
                alert.addAction(UIAlertAction(title: model.title ?? "", style: .default , handler:alertSheetAction))
                
            }
            
            alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{ (UIAlertAction)in
                
                
                
            }))
            self.present(alert, animated: true, completion:nil)
            
        }
        
    }
    
    
    
    func alertSheetAction(action: UIAlertAction) {
        
        for model in attachmentTypes ?? [] {
            
            if action.title == model.title {
                
                bannerType = model.attachmentType ?? 0
                
            }
            
        }
        
        self.attachImage()
    }
    
    
    func attachImage(){
        
        // startAnimating()
        isAttachment = true
        isDocument = true
        openImagePicker()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            //  weakSelf?.stopAnimating()
            if status {
                let arrayEachImage:AttachmentBannerTypeModel = AttachmentBannerTypeModel()
                arrayEachImage.cdnPath = urlString
                arrayEachImage.fileName = imageName
                arrayEachImage.attachmentType = self.bannerType
                weakSelf?.attachmentsProduct.append(arrayEachImage)
                weakSelf?.collectionViewAttachment.reloadData()
                weakSelf?.isAttachment = false
            }
        }
    }
    
}

extension NewProductVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            return CGSize.init(width: collectionViewStatus.frame.width / 4, height: 40.0)
            
        }
        
        if collectionView == collectionViewAttachments
            
        {
            return CGSize.init(width: 100, height: 100)
            
        }
        
        if collectionView == collectionViewAttachment
            
        {
            return CGSize.init(width: 100, height: 100)
            
        }
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewAttachment
        {
            if attachmentsProduct.count > 0 {
                
                return attachmentsProduct.count
            }
            else
            {
                return 0
            }
            
        }
        
        if collectionView == collectionViewAttachments
        {
            if attachments.count > 0 {
                
                return attachments.count
            }
            
        }
        else{
            
            if statusList?.count ?? 0 > 0 {
                
                return statusList?.count ?? 0
            }
            
        }
        
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewAttachment
            
        {
            
            if let cell:AttachmentPictureProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureProductCell", for: indexPath) as? AttachmentPictureProductCell {
                
                return getCellForProductAttachment(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if collectionView == collectionViewAttachments
        {
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachment(cell: cell, indexPath: indexPath)
                
            }
        }
        else{
            
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForStatus(cell: cell, indexPath: indexPath)
                
            }
            
        }
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionViewStatus
            
        {
            selectedIndex = indexPath.row
            
            collectionViewStatus.reloadData()
            
        }
        
        if collectionView == collectionViewAttachments {
          
          if let fileName = attachments[indexPath.row] as? [String : String] {
              
              if let imagePath = fileName["cdnPath"] {
                             
                     
          let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                imageViewer.strImg = imagePath
                navigationController?.pushViewController(imageViewer, animated: true)
                  
              }
              
          }
              
        }
        
        if collectionView == collectionViewAttachment {
          
          if let model:AttachmentBannerTypeModel = attachmentsProduct[indexPath.row] {
              
            if let imagePath = model.cdnPath {
                
                let strFileType = model.fileName?.fileExtension()

                if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
              {
                             
                     
             let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                imageViewer.strImg = imagePath
                navigationController?.pushViewController(imageViewer, animated: true)
                  
              }
          else{
            
            
            openWebView(urlString: model.cdnPath ?? "", title: model.fileName ?? "", subTitle: "")
                }
              
          }
              
        }
        }
    }
    
    
    func getCellForProductAttachment(cell:AttachmentPictureProductCell,indexPath:IndexPath) -> AttachmentPictureProductCell {
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete .addTarget(self, action: #selector(deleteProductImage(_:)), for: .touchUpInside)
        
        if let model:AttachmentBannerTypeModel = attachmentsProduct[indexPath.row]
        {
            if let imagePath = model.cdnPath
            {
                if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                
                     let strFileType = model.fileName?.fileExtension()

                       if(strFileType?.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType?.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                     {
                        
                    cell.imgView.sd_setImage(with: url, completed: nil)
                        
                    }
                     else{
                        
                        cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                    }
                }
                
                for modelMain in attachmentTypes ?? [] {
                    
                    if modelMain.attachmentType == model.attachmentType {
                        
                        cell.lblTitle.text = modelMain.title ?? ""
                        
                    }
                    
                }
                
            }
            
        }
        return cell
    }
    
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete .addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        if let fileName = attachments[indexPath.row] as? [String : String]
        {
            if let imagePath = fileName["cdnPath"]
            {
                if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                    cell.imgView.sd_setImage(with: url, completed: nil)
                }
            }
            
        }
        return cell
    }
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:SellerStatusModel = statusList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.statusTitle ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
}

extension NewProductVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableViewBranchList {
            
            return branchList?.count ?? 0
        }
        
        if tableView == tableViewGiftList {
                   
            return giftList?.count ?? 0
        }
        
        if tableView == tableViewResponseType {
            
            return responseTypes?.count ?? 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewBranchList {
            
            if let cell:NewProductBranchCell = tableView.dequeueReusableCell(withIdentifier: "NewProductBranchCell", for: indexPath) as? NewProductBranchCell {
                
                return getCellForProductBranch(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if tableView == tableViewGiftList {
            
            if let cell:NewProductBranchCell = tableView.dequeueReusableCell(withIdentifier: "NewProductBranchCell", for: indexPath) as? NewProductBranchCell {
                
                return getCellForProductGift(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if tableView == tableViewResponseType {
            
            if let cell:NewProductResponseTypeCell = tableView.dequeueReusableCell(withIdentifier: "NewProductResponseTypeCell", for: indexPath) as? NewProductResponseTypeCell {
                
                return getCellForProductResponseType(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableViewBranchList {
            
            if let model:ProductBranchModel = branchList?[indexPath.row]{
                
                if model.isSelected == 0 {
                    
                    model.isSelected = 1
                    
                }
                else{
                    model.isSelected = 0
                }
                
                tableViewBranchList.reloadData()
            }
            
        }
        
    if tableView == tableViewGiftList {
                   
        if let model:ProductGiftModel = giftList?[indexPath.row]{
                       
            if model.isSelected == 0 {
                           
                    model.isSelected = 1
                           
                }
          else{
                model.isSelected = 0
              }
                       
                tableViewGiftList.reloadData()
            }
                   
        }
        
        if tableView == tableViewResponseType {
            
            isSelected = indexPath.row
            
            tableViewResponseType.reloadData()
        }
    }
    
    func getCellForProductBranch(cell:NewProductBranchCell,indexPath:IndexPath) -> NewProductBranchCell {
        
        cell.selectionStyle = .none
        
        if let model:ProductBranchModel = branchList?[indexPath.row]{
            
            cell.lblTitle.text = model.branchName ?? ""
            
            if model.isSelected == 0 {
                
                cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBox")
                
            }
            else{
                cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
            }
        }
        
        return cell
    }
    
func getCellForProductGift(cell:NewProductBranchCell,indexPath:IndexPath) -> NewProductBranchCell {
           
           cell.selectionStyle = .none
           
           if let model:ProductGiftModel = giftList?[indexPath.row]{
               
               cell.lblTitle.text = model.title ?? ""
               
               if model.isSelected == 0 {
                   
                   cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBox")
                   
               }
               else{
                   cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
               }
           }
           
           return cell
       }
    
    func getCellForProductResponseType(cell:NewProductResponseTypeCell,indexPath:IndexPath) -> NewProductResponseTypeCell {
        
        cell.selectionStyle = .none
        
        if let model:ResponseTypeModel = responseTypes?[indexPath.row]{
            
            cell.lblTitle.text = model.title ?? ""
            
            print(model.responseType)
            
        }
        if isSelected == indexPath.row {
            
            cell.imgViewRadio.image = UIImage.init(imageLiteralResourceName: "radioFilled")
            
        }
        else{
            cell.imgViewRadio.image = UIImage.init(imageLiteralResourceName: "radio")
        }
        
        return cell
    }
    
}


class AttachmentPictureProductCell:UICollectionViewCell {
    
    @IBOutlet weak var btnDelete: NKButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: NKLabel!
}




class NewProductBranchCell:UITableViewCell{
    
    @IBOutlet weak var imgViewCheckBox: UIImageView!
    
    @IBOutlet weak var lblTitle: NKLabel!
    
}
class NewProductResponseTypeCell:UITableViewCell{
    
    @IBOutlet weak var imgViewRadio: UIImageView!
    
    @IBOutlet weak var lblTitle: NKLabel!
    
}

extension NewProductVC:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if  textField == txtRangeCurrencyTitle || textField == txtFixedCurrencyTitle || textField == txtGuidelineCurrency || textField == txtCharityCurrency || textField == txtReferralCurrency || textField == txtLoyaltyCurrency || textField == txtGuidelineCurrency  || textField == txtAvgRefEarningsCurrency{
            
            self.activeTextField = textField
            
            openCurrencySelector()
            
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let txtField:RCTextField = textField as? RCTextField{
            txtField.showError(message: "")
        }
        
        return true
    }
    
}

extension NewProductVC:UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if companyType != 1 {
        
           if textView == txtViewProductDes {
              txtViewProductDes.showError(message: "")
            
               return textView.text.count + (text.count - range.length) <= 150
            }
        }
        
        if textView == txtViewMsgPartner{
            
            return textView.text.count + (text.count - range.length) <= 250
            
        }
        
        if textView == txtViewMsgUser{
            
            return textView.text.count + (text.count - range.length) <= 250
            
        }
        
        return true
    }
    
}

extension NewProductVC:HtmlEditorVCDelegate{
    
    func didAddedString(htmlStr:String){
       
        htmlDes = htmlStr
        
        if htmlDes.count > 0 {
                   
            let productDesTitle = NSMutableAttributedString(string: "Click here to update product description".localized(), attributes: desAttributes)
                   
              lblProductDes.attributedText = productDesTitle
                   
        }
        
    }
    
}
