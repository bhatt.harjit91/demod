//
//  HtmlEditorVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 23/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit
import RichEditorView

protocol HtmlEditorVCDelegate: class {
    func didAddedString(htmlStr:String)
}
class HtmlEditorVC: AppBaseViewController {

 @IBOutlet var editorView: RichEditorView!
    
 var btnSubmit = UIButton()
    
 var htmlText:String = ""
    
    weak var delegate:HtmlEditorVCDelegate?
    
 lazy var toolbar: RichEditorToolbar = {
    let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width:self.view.bounds.width, height: 44))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

          editorView.delegate = self
        
        //  editorView.webView.delegate = self
        
                      editorView.inputAccessoryView = toolbar
                      editorView.placeholder = "Type some text..."

                      toolbar.delegate = self
                      toolbar.editor = editorView
        
                       editorView.html = htmlText
                      
       //               editorView.layer.borderWidth = 1
       //               editorView.layer.borderColor = UIColor.primaryColor.cgColor
               

                      // We will create a custom action that clears all the input text when it is pressed
                      let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
                          toolbar.editor?.html = ""
                      }

                      var options = toolbar.options
                      options.append(item)
                      toolbar.options = options
        
        
        
        navigationItem.title = "Editor".localized()
        
        
        //  btnName.setImage(UIImage(named: "walletIcon"), for: .normal)
        btnSubmit.setTitle("Save -->".localized(), for: .normal)
                      btnSubmit.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                      btnSubmit.setTitleColor(UIColor.white, for: .normal)
                      btnSubmit.addTarget(self, action: #selector(submit), for: .touchUpInside)
        
                     let rightBarButton = UIBarButtonItem()
                     rightBarButton.customView = btnSubmit
                     self.navigationItem.rightBarButtonItem = rightBarButton
        
        
    }
    
    @objc func submit(){
        
        delegate?.didAddedString(htmlStr: htmlText)
        
        navigationController?.popViewController(animated: true)
        
    }
    

}

extension HtmlEditorVC: RichEditorDelegate {

    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {
        if content.isEmpty {
           // htmlTextView.text = "HTML Preview"
        } else {
            htmlText = content
        }
    }
    
    func richEditor(_ editor: RichEditorView, shouldInteractWith url: URL) -> Bool {
        
        UIApplication.shared.open(url, options: [:])
        
        return true
        
    }
    
}

extension HtmlEditorVC: RichEditorToolbarDelegate {

    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }

    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }

    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }

    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
       
        // toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }

    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
        // Can only add links to selected text, so make sure there is a range selection first
        if toolbar.editor?.hasRangeSelection == true {
            toolbar.editor?.insertLink(editorView.text, title: "Link")
        }
    }
}
