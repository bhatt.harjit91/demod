//
//  ProductChangeLogVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 30/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ProductChangeLogApiModel:Codable{
    
     var logData:[ProductLogModel]?
    
}

class ProductChangeLogVC: AppBaseViewController {

    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var lblNoData: NKLabel!
    
    var changeLog:[ProductLogModel]?
    var sellerCode:Int = 0
    var productCode:Int = 0
    
    let priceAttributes: [NSAttributedString.Key: Any] = [
           .font: UIFont.fontForType(textType: 4),
           .foregroundColor: UIColor.black]
       
       let descriptionAttributes: [NSAttributedString.Key: Any] = [
           .font: UIFont.fontForType(textType: 8),
           .foregroundColor: UIColor.black]
    
    override func viewDidLoad() {
        super.viewDidLoad()

         lblNoData.text = "No Data Found".localized()
        
        navigationItem.title = "Change Log".localized()
        
        fetchProductList()
        
    }
    
    func fetchProductList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/productChangeLog?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&productCode=\(productCode) "), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ProductChangeLogApiModel.self, from: data)
                                
                              
                                if let apiResponse:[ProductLogModel] = jsonResponse.logData {
                                    
                                    weakSelf?.changeLog = apiResponse
                                    
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }


}

extension ProductChangeLogVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let count = changeLog?.count ?? 0
               if count > 0 {
                   
                   tableViewList.alpha = 1
                   
                   return changeLog?.count ?? 0
                   
               }
               else{
                   
                   tableViewList.alpha = 0
               }
               
               return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            if let cell:ProductChangeLogCell = tableView.dequeueReusableCell(withIdentifier: "ProductChangeLogCell", for: indexPath) as? ProductChangeLogCell {
                
                return getCellForProductCell(cell: cell, indexPath: indexPath)
                
            }
        
        return UITableViewCell.init()
    }
    
    func getCellForProductCell(cell:ProductChangeLogCell,indexPath:IndexPath) -> ProductChangeLogCell {
        
        cell.selectionStyle = .none
        
        if let model:ProductLogModel = changeLog?[indexPath.row]{
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM"
            dateFormatterPrint.timeZone = .current
            
            let dateFormatterPrint2 = DateFormatter()
            dateFormatterPrint2.dateFormat = "hh:mm a"
            dateFormatterPrint2.timeZone = .current
            
            var dateText = ""
            
            var timeText = ""
            
            if let date = dateFormatterGet.date(from: model.updatedDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            if let date = dateFormatterGet.date(from: model.updatedDate ?? "") {
                
                timeText = dateFormatterPrint2.string(from: date)
            }
            
            cell.lblDate.text = dateText
            
            cell.lblTime.text = timeText
            
            let price = NSAttributedString(string:"\(model.productPrice ?? "")" , attributes: priceAttributes)
            
            let description = NSAttributedString(string:"\(" updated by ".localized())\(model.updatedBy ?? "")" , attributes: descriptionAttributes)
            
            let priceDes = NSMutableAttributedString()
            
            priceDes.append(price)
            
            priceDes.append(description)
            
            cell.lblLogText.attributedText = priceDes
            
             cell.viewTopLine.alpha = 1
            
            cell.viewBottomLine.alpha = 1
            
            if indexPath.row == 0{
                
                cell.viewTopLine.alpha = 0
                
            }
            
            let count  = changeLog?.count ?? 0
            
            if indexPath.row == count - 1 {
                
                cell.viewBottomLine.alpha = 0
                
            }
            
        }
        
        return cell
    }
    
   
}

class ProductChangeLogCell: UITableViewCell {
    
    @IBOutlet weak var viewDate:RCView!
    @IBOutlet weak var lblDate: NKLabel!
    @IBOutlet weak var lblTime: NKLabel!
    @IBOutlet weak var lblLogText: NKLabel!
    @IBOutlet weak var viewLogDes:RCView!
    @IBOutlet weak var viewTopLine:UIView!
    @IBOutlet weak var viewBottomLine:UIView!
}
