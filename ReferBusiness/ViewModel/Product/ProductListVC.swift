//
//  ProductListVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 10/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol ProductListVCDelegate: class {
    func didAddedNewProduct()
}

class ProductListVC: AppBaseViewController,NewProductVCDelegate {
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblProductCount: NKLabel!
    
    @IBOutlet weak var lblNoFound: NKLabel!
    
    var sellerCode:Int = 0
    
    var productCount:Int = 0
    
    var statusList:[SellerStatusModel]?
    
    var productList:[ProductModel]?
    
    var colorCodes:ColorModel?
    
    var responseTypes:[ResponseTypeModel]?
    
    var currency:[CurrencyModel]?
    
    var branchList:[ProductBranchModel]?
    
    var attachmentTypes:[AttachmentTypeModel]?
    
    var giftList:[ProductGiftModel]?
    
    weak var delegate:ProductListVCDelegate?
    
    var isAdmin:Int?
       
    var isPartnerForCompany:Int?
    
    var companyType:Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let btnName = UIButton()
        btnName.setTitle("New -->", for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(newProductAction(sender:)), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        btnName.alpha = 0
               
        if isAdmin == 1 || isPartnerForCompany == 1 {
                   
           btnName.alpha = 1
        }
        
        navigationItem.title = "Product/Service Groups".localized()
        
        lblNoFound.text = "No Data Found".localized()
        
        lblNoFound.alpha = 0
        
        fetchProductList()
        
    }
    
    func didAddedProduct() {
        fetchProductList()
        
        delegate?.didAddedNewProduct()
    }
    
    
    @objc func newProductAction(sender: UIButton){
        
        openProductCreationPage()
        
    }
    
    func openProductCreationPage() {
        
        let storyBoard = UIStoryboard.init(name: ProductSB, bundle: nil)
              let newProductPage:NewProductVC = storyBoard.instantiateViewController(withIdentifier: "NewProductVC") as! NewProductVC
              newProductPage.sellerCode = sellerCode
              newProductPage.statusList = statusList ?? []
              newProductPage.currency = currency
              newProductPage.branchList = branchList
              newProductPage.responseTypes = responseTypes
              newProductPage.attachmentTypes = attachmentTypes ?? []
              
              newProductPage.giftList = giftList ?? []
              newProductPage.delegate = self
              newProductPage.companyType = companyType
              navigationController?.pushViewController(newProductPage, animated: true)
        
    }
    
    
    func fetchProductList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/productList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&productType=\(0) "), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ProductApiModel.self, from: data)
                                
                                /// vivek
                                
                                if let apiResponse:[AttachmentTypeModel] = jsonResponse.attachmentTypes {
                                    
                                    weakSelf?.attachmentTypes = apiResponse
                                    
                                }
                                
                                if let apiResponse:[ProductModel] = jsonResponse.productList {
                                    
                                    weakSelf?.productList = apiResponse
                                    
                                    
                                }
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.productCount = apiResponse
                                    
                                    weakSelf?.lblProductCount.text = ("\("This seller has ".localized())\(weakSelf?.productCount ?? 0)\(" products")")
                                }
                                
                                if let apiResponse:[ResponseTypeModel] = jsonResponse.responseTypes {
                                    
                                    weakSelf?.responseTypes = apiResponse
                                    
                                }
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currency {
                                    
                                    weakSelf?.currency = apiResponse
                                    
                                }
                                
                                if let apiResponse:[ProductBranchModel] = jsonResponse.branchList {
                                    
                                    weakSelf?.branchList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[ProductGiftModel] = jsonResponse.giftList {
                                    
                                    weakSelf?.giftList = apiResponse
                                    
                                }
                                
                                if weakSelf?.productList?.count ?? 0 == 0{
                                   
                                   if weakSelf?.isAdmin == 1 || weakSelf?.isPartnerForCompany == 1 {
                                        
                                        weakSelf?.openProductCreationPage()
                                    
                                    }
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
    
}

extension ProductListVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if productList?.count ?? 0 > 0{
            
            tableViewList.alpha = 1
            
            lblNoFound.alpha = 0
            
            return productList?.count ?? 0
        }
        else{
            
            tableViewList.alpha = 0
            
            lblNoFound.alpha = 1
            
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:ProductListCell = tableView.dequeueReusableCell(withIdentifier: "ProductListCell", for: indexPath) as? ProductListCell {
            
            return getCellForProductList(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:ProductModel = productList?[indexPath.row]
        {
            
            let storyBoard = UIStoryboard.init(name: ProductSB, bundle: nil)
            let newProductPage:NewProductVC = storyBoard.instantiateViewController(withIdentifier: "NewProductVC") as! NewProductVC
            newProductPage.sellerCode = model.sellerCode ?? 0
            newProductPage.productCode = model.productCode ?? 0
            newProductPage.branchList = branchList
            newProductPage.responseTypes = responseTypes
            newProductPage.statusList = statusList
            newProductPage.currency = currency
            newProductPage.attachmentTypes = attachmentTypes ?? []
            newProductPage.isDetail = true
            newProductPage.delegate = self
            newProductPage.giftList = giftList ?? []
            newProductPage.isAdmin = isAdmin
            newProductPage.isAdmin = isPartnerForCompany
            newProductPage.companyType = companyType
            navigationController?.pushViewController(newProductPage, animated: true)
            
        }
    }
    
    func getCellForProductList(cell:ProductListCell,indexPath:IndexPath) -> ProductListCell {
        
        cell.selectionStyle = .none
        
        if let model:ProductModel = productList?[indexPath.row]
        {
            cell.lblProductNameText.text = model.productName ?? ""
            cell.lblStatusText.text = model.statusTitle ?? ""
            
            if model.productLogo?.count ?? 0 > 0 {
                cell.imgViewProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
            }
            
            if model.status == 1 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.activeColor ?? "")
            }
                
            else if model.status == 2 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.inactiveColor ?? "")
            }
                
            else if model.status == 3 {
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.blacklistedColor ?? "")
            }
            
        }
        
        return cell
    }
    
    
    
}

class ProductListCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblProductNameText: NKLabel!
    
    @IBOutlet weak var lblStatusText: NKLabel!
    
    @IBOutlet weak var viewStatus: RCView!
    
    @IBOutlet weak var imgViewProduct: UIImageView!
    
    
}
