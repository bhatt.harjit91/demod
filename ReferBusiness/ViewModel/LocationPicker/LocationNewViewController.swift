//
//  LocationNewViewController.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit

protocol LocationPermissionVCNewDelegate: class {
    func didFinishSelectingLocation(coordinate:CLLocationCoordinate2D)
}
class LocationNewViewController: UIViewController,UISearchBarDelegate, GMSMapViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource,CLLocationManagerDelegate {
    
    @IBOutlet weak var viewMAIN: UIView!
    
    @IBOutlet weak var tf_searchbar: UISearchBar!
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var bottomSpaceForViewMain: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceForViewMain: NSLayoutConstraint!
    
    @IBOutlet weak var btnCancel: UIBarButtonItem!
    @IBOutlet weak var btnSelect: UIBarButtonItem!
    @IBOutlet weak var lblNavigationTitle: UINavigationItem!
    
    @IBOutlet weak var tbl_searchResult: UITableView!
    @IBOutlet weak var view_Tble: UIView!
    //    @IBOutlet weak var bottomSpaceForTable: NSLayoutConstraint!
    //    @IBOutlet weak var btn_FinishSelection: UIButton!
    //    @IBOutlet weak var btn_CancelSelection: UIButton!
    //    @IBOutlet weak var view_close: UIView!
    @IBOutlet weak var mapView_: GMSMapView!
    @IBOutlet weak var image_pointer: UIImageView!
    var lat = 0.0
    var lng = 0.0
    var address = ""
    //    @IBOutlet weak var tv_Address: UITextView!
    @IBOutlet var keyboardBar: UIToolbar!
    
    @IBAction func call_Done(_ sender: Any) {
        view.endEditing(true)
    }
    weak var delegate:LocationPermissionVCNewDelegate?
    var currentLocation:CLLocationCoordinate2D?
    //    @IBOutlet weak var bottomForTextView: NSLayoutConstraint!
    //    @IBOutlet weak var btn_PickLatest: UIButton!
    var isCancelable:Bool = true
    var isMessanger = false
    //    @IBOutlet weak var viewTempOption: UIView!
    //    @IBOutlet weak var heightForTempOption: NSLayoutConstraint!
    //    @IBOutlet weak var txtDays: UITextField!
    //    @IBOutlet weak var daysStepper: UIStepper!
    //    @IBOutlet weak var heightForBottomVIew: NSLayoutConstraint!
    //    @IBOutlet weak var btnTempOption: UIButton!
    //    @IBOutlet weak var widthForTempButton: NSLayoutConstraint!
    var locationManager: CLLocationManager?
    var Predictions: [Any] = []
    var lastLatitude:Double = 0.0
    var lastLongitude:Double = 0.0
    var City:String = ""
    var Country:String = ""
    var State:String = ""
    var Locality:String = ""
    var Street:String = ""
    var PostalCode:String = ""
    var latestAddress:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                
            case 1334:
                print("iPhone 6/6S/7/8")
                
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                
            case 2436:
                print("iPhone X, XS")
                topSpaceForViewMain.constant = 50
                bottomSpaceForViewMain.constant = 10
                
            case 2688:
                print("iPhone XS Max")
                topSpaceForViewMain.constant = 50
                bottomSpaceForViewMain.constant = 10
                
            case 1792:
                print("iPhone XR")
                topSpaceForViewMain.constant = 40
                bottomSpaceForViewMain.constant = 10
                
            default:
                print("Unknown")
            }
        }
        
        //        if isCancelable {
        //
        //        }
        //        else{
        //           btn_CancelSelection.alpha = 0
        //        }
        upDateMap()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        // bottomSpaceForTable.constant = view.frame.size.height - 110
        
        image_pointer.setImageColor(color: UIColor.getGradientColor(index: 0).first!)
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyKilometer
        requestInUseAuthorization()
        locationManager?.distanceFilter = CLLocationDistance(500)
        
        if lat == 0 && lng == 0 {
            locationManager?.startUpdatingLocation()
        }
        
        navBar.barTintColor = .primaryColor
        
    }
    
    //    func updateButtons(){
    //        if latestAddress.count <= 0 {
    //            btn_PickLatest.alpha = 0
    //        }
    //        if tv_Address.text.count <= 0 {
    //            btn_FinishSelection.alpha = 0
    //        }
    //    }
    
    func upDateMap(){
        btnCancel.title = "Cancel".localized()
        btnSelect.title = "\("Select".localized())\(" -->")"
        lblNavigationTitle.title = "Select location".localized()
        tf_searchbar.placeholder = "Search place".localized()
        view.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
        tf_searchbar.layer.masksToBounds = false
        tf_searchbar.layer.shadowOffset = CGSize(width: 0, height: 5)
        tf_searchbar.layer.shadowRadius = 3
        tf_searchbar.layer.shadowOpacity = 0.5
        tf_searchbar.setShowsCancelButton(false, animated: true)
        
        locationManager = CLLocationManager()
        mapView_.isMyLocationEnabled = true
        mapView_.settings.myLocationButton = true
        mapView_.accessibilityElementsHidden = false
        mapView_.settings.compassButton = true
        mapView_.delegate = self
        
        
        // tv_Address.text = address
        
        self.tf_searchbar.text = address
        
        if (CLLocationCoordinate2DIsValid(currentLocation ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))) {
            lat = currentLocation?.latitude ?? 0.0
            lng = currentLocation?.longitude ?? 0.0
        }
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 18)
        mapView_.animate(to: camera)
        //        if isAddressEditable {
        //            tv_Address.isEditable = true
        //            tv_Address.backgroundColor = UIColor.white
        //            tv_Address.textColor = UIColor.black
        //            btn_PickLatest.backgroundColor = UIColor(red: 244.0 / 255.0, green: 81.0 / 255.0, blue: 30.0 / 255.0, alpha: 1.0)
        //            btn_PickLatest.isEnabled = true
        //        } else {
        //            tv_Address.isEditable = false
        //            tv_Address.backgroundColor = UIColor.clear
        //            tv_Address.textColor = UIColor.white
        //            btn_PickLatest.backgroundColor = UIColor.clear
        //            btn_PickLatest.isEnabled = false
        //        }
        
        //  tv_Address.inputAccessoryView = keyboardBar
        
    }
    func requestInUseAuthorization() {
        let status = CLLocationManager.authorizationStatus()
        
        // If the status is denied or only granted for when in use, display an alert
        if status == .denied {
            var title: String
            title = "Location services are OFF".localized()
            let message = "Enable location service to load your current location.".localized()
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                       
                       alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .default, handler: { _ in
                           
                       }))
                       
                       alert.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { _ in
                           
                           self.openAppSettings()
                           
                       }))
                       
                       self.present(alert, animated: false, completion: nil)
        } else if status == .notDetermined {
            locationManager?.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // If the status is denied or only granted for when in use, display an alert
        if status == .denied {
            var title: String
            title = "Location services are OFF".localized()
            let message = "Enable location services to load your current location.".localized()
            
//            let alertView = UIAlertView(title: title, message: message, delegate: self as? UIAlertViewDelegate, cancelButtonTitle: "Cancel".localized(), otherButtonTitles: "Settings".localized())
//            alertView.show()
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .default, handler: { _ in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { _ in
                
                self.openAppSettings()
                
            }))
            
            self.present(alert, animated: false, completion: nil)
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let last = locations.last {
            //print("didUpdateToLocation: \(last)")
        }
        let currentLocation = locations.last
        
        if currentLocation != nil {
            if (lat == 0) && (lng == 0) {
                loadMapwithLatitude(currentLocation?.coordinate.latitude ?? 0.0, andLongitude: currentLocation?.coordinate.longitude ?? 0.0)
                locationManager?.stopUpdatingLocation()
            }
        }
    }
    func loadMapwithLatitude(_ latitude: Double, andLongitude longitude: Double) {
        
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 18)
        mapView_.animate(to: camera)
        
    }
    
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 1 {
            // Send the user to the Settings for this app
            let settingsURL = URL(string: UIApplication.openSettingsURLString)
            if let settingsURL = settingsURL {
                UIApplication.shared.openURL(settingsURL)
            }
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //print("didFailWithError: \(error)")
//        let errorAlert = UIAlertView(title: "Error".localized(), message: "Failed to Get Your Location".localized(), delegate: nil, cancelButtonTitle: "OK".localized(), otherButtonTitles: "")
//        errorAlert.show()
        
        let alert = UIAlertController(title:  "Error".localized(), message: "Failed to Get Your Location".localized(), preferredStyle: .alert)
                  
        alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { _ in
                      
                  }))
        
         self.present(alert, animated: false, completion: nil)
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func keyboardWasShown(_ aNotification: Notification?) {
        let info = aNotification?.userInfo
        let kbSize = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as AnyObject).cgRectValue.size
        
        //        if tv_Address.isFirstResponder {
        //            bottomForTextView.constant = kbSize.height
        //        } else {
        //            bottomSpaceForTable.constant = kbSize.height
        //        }
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillBeHidden(_ aNotification: Notification?) {
        // bottomForTextView.constant = 0
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        })
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Predictions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        }
        let prediction = Predictions[indexPath.row] as? GMSAutocompletePrediction
        let array = prediction?.attributedFullText.string.components(separatedBy: ",")
        var title = ""
        var detail = ""
        var marray = array
        if (marray?.count ?? 0) > 0 {
            title = marray?[0] ?? ""
            marray?.remove(at: 0)
        }
        
        detail = marray?.joined(separator: ",") ?? ""
        
        cell?.textLabel?.text = title
        cell?.detailTextLabel?.text = detail
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Predictions.count > indexPath.row {
            let prediction = Predictions[indexPath.row] as? GMSAutocompletePrediction
            let placeID = prediction?.placeID
            var placesClient = GMSPlacesClient()
            placesClient.lookUpPlaceID(placeID!, callback: { place, error in
                if error != nil {
                    //print("Place Details error \(error?.localizedDescription ?? "")")
                    return
                }
                
                if place != nil {
                    
                    if let name = place?.name {
                        //print("Place name \(name)")
                    }
                    if let formattedAddress = place?.formattedAddress {
                        //print("Place address \(formattedAddress)")
                    }
                    if let placeID = place?.placeID {
                        //print("Place placeID \(placeID)")
                    }
                    if let attributions = place?.attributions {
                        //print("Place attributions \(attributions)")
                    }
                    if let latitude = place?.coordinate.latitude, let longitude = place?.coordinate.longitude {
                        //print("Place lat lng \(latitude),\(longitude)")
                    }
                    self.tf_searchbar.text = place?.formattedAddress
                    // self.tv_Address.text = place?.formattedAddress
                    self.latestAddress = place?.formattedAddress ?? ""
                    self.loadMapwithLatitude(place?.coordinate.latitude ?? 0.0, andLongitude: place?.coordinate.longitude ?? 0.0)
                    self.searchBarCancelButtonClicked(self.tf_searchbar)
                } else {
                    //print("No place details for \(placeID ?? "")")
                }
            })
            
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        let coor: CLLocationCoordinate2D = self.mapView_.camera.target
        lastLatitude = coor.latitude
        lastLongitude = coor.longitude
        let geoCoder = GMSGeocoder()
        geoCoder.reverseGeocodeCoordinate(coor, completionHandler: { response, error in
            
            if response != nil {
                let address = response?.results()?.first as? GMSAddress
                if address?.thoroughfare != nil {
                    if let thoroughfare = address?.thoroughfare {
                        self.Street = thoroughfare
                    }
                    else{ self.Street = ""}
                    if address?.subLocality != nil {
                        self.Locality = address?.subLocality ?? ""
                    }
                    else {self.Locality = ""}
                    if address?.locality != nil {
                        self.City = address?.locality ?? ""
                    }
                    else {self.City = ""}
                    if address?.administrativeArea != nil {
                        self.State = address?.administrativeArea ?? ""
                    }
                    else {self.State = ""}
                    
                    if address?.country != nil {
                        self.Country = address?.country ?? ""
                    }
                    else {self.Country = ""}
                    
                    if address?.postalCode != nil {
                        self.PostalCode = address?.postalCode ?? ""
                    }else {self.PostalCode = ""}
                    //                        if self.tv_Address.text == "" {
                    
                    //                            self.tv_Address.text = [address.lines, componentsJoinedByString:@","];
                    //}
                    //  self.tv_Address.text = address?.lines?.joined(separator: ",")
                    
                    self.tf_searchbar.text = address?.lines?.joined(separator: ",")
                    // }
                    //                        else if (!_isAddressEditable){
                    //                            self.tv_Address.text = [address.lines componentsJoinedByString:@","];
                    //                        }
                    self.latestAddress = address?.lines?.joined(separator: ",") ?? ""
                }
            }
            
        })
        
        var center:CLLocationCoordinate2D = coor
        center.latitude = lastLatitude
        center.longitude = lastLongitude
        
        let cameraPos:GMSCameraPosition = GMSCameraPosition(latitude: lastLatitude, longitude: lastLongitude, zoom: mapView.camera.zoom)
        
        
        mapView.camera = cameraPos
        
        
        tbl_searchResult.reloadData()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !(searchText == "") {
            
            let filter = GMSAutocompleteFilter()
            //filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter
            let placesClient = GMSPlacesClient()
            let visibleRegion = mapView_.projection.visibleRegion
            let bounds = GMSCoordinateBounds(coordinate: visibleRegion().nearLeft, coordinate: visibleRegion().nearRight)
            
            placesClient.autocompleteQuery(searchText, bounds: bounds, filter: filter, callback: { results, error in
                if error != nil {
                    //print("Autocomplete error \(error?.localizedDescription ?? "")")
                    return
                }
                if let results = results {
                    self.Predictions = results
                }
                self.tbl_searchResult.reloadData()
                
            })
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        tf_searchbar.text = ""
        searchBar.setShowsCancelButton(true, animated: true)
        view_Tble.isHidden = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        tf_searchbar.resignFirstResponder()
        // bottomSpaceForTable.constant = view.frame.size.height - 110
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { finished in
            self.view_Tble.isHidden = true
        }
        
        //self.view_Tble.isHidden = true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        tf_searchbar.resignFirstResponder()
        self.view_Tble.isHidden = true
        
    }
    @IBAction func call_PickLatest(_ sender: Any){
        if latestAddress.count > 0 {
            // tv_Address.text = latestAddress
            self.tf_searchbar.text = latestAddress
        }
    }
    
    @IBAction func cancelSelection(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func call_Cancel(_ sender: Any){
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func finishSelection(_ sender: Any) {
        
        //        if tv_Address.text != "" {
        dismiss(animated: true) {
            let coor:CLLocationCoordinate2D = self.mapView_.camera.target
            self.delegate?.didFinishSelectingLocation(coordinate: coor)
        }
        //        }
    }
    
    
    //    @IBAction func call_Finish(_ sender: Any){
    //        if tv_Address.text != "" {
    //            dismiss(animated: true) {
    //                let coor:CLLocationCoordinate2D = self.mapView_.camera.target
    //                self.delegate?.didFinishSelectingLocation(coordinate: coor)
    //            }
    //        }
    //    }
    
}

//CGPoint point = self.mapView_.center;



//        if ([self.delegate respondsToSelector:@selector(didFinishPickingAddress:Latitude:Longitude:)]) {
//        [self.delegate didFinishPickingAddress:self.tv_Address.text Latitude:coor.latitude Longitude:coor.longitude];
//        }
//
//        if ([self.delegate respondsToSelector:@selector(didFinishPickingAddress:Country:Latitude:Longitude:)]) {
//        [self.delegate didFinishPickingAddress:self.tv_Address.text Country:Country Latitude:coor.latitude Longitude:coor.longitude];
//        }
//
//
//        }];



