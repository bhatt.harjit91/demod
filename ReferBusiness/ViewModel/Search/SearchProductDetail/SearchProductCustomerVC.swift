//
//  SearchProductCustomerVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 11/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class SearchProductCustomerVC: AppBaseViewController {
    
 @IBOutlet weak var collectionViewCustomer: UICollectionView!
    
    var customerList:[CustomerModel]?
    
    var sellerCode:Int = 0
          
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Customers".localized()
        
        fetchCustomerList()

    }
    
    func fetchCustomerList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getSellerCustomerList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&isProductDetailPage=\(1)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        do {
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
                            print(dataObj)
                            
                            let jsonResponse = try decoder.decode(CustomerApiModel.self, from: data)
                            
                            if let apiResponse:[CustomerModel] = jsonResponse.sellerCustomerList {
                                
                              weakSelf?.customerList = apiResponse
                                
                            }
                            
                            
                            weakSelf?.collectionViewCustomer.reloadData()
                            
                            return
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                            
                        }
                      }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
   

}

extension SearchProductCustomerVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let yourWidth:CGFloat = collectionViewCustomer.bounds.width/2.0 - 10
        let yourHeight:CGFloat = CGFloat(200)

        return CGSize(width: yourWidth, height: yourHeight)
            
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            if customerList?.count ?? 0 > 0 {
                
                return customerList?.count ?? 0
            }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            if let cell:SearchProductCustomerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchProductCustomerCell", for: indexPath) as? SearchProductCustomerCell {
                
                return getCellForCustomer(cell: cell, indexPath: indexPath)
                
            }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    if let model:CustomerModel = customerList?[indexPath.row]
    {
        
        let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
        if let customerDetailPage:SearchProductCustomerDetailVC = storyBoard.instantiateViewController(withIdentifier: "SearchProductCustomerDetailVC") as? SearchProductCustomerDetailVC {
                 customerDetailPage.customerDetail = model
            navigationController?.pushViewController(customerDetailPage, animated: true)
          }
                 
        }

     }
    
    
    

    
    func getCellForCustomer(cell:SearchProductCustomerCell,indexPath:IndexPath) -> SearchProductCustomerCell {
        
        if let model:CustomerModel = customerList?[indexPath.row]
        {
          cell.lblName.text = model.customerName ?? ""

            
        if model.logo?.count ?? 0 > 0 {
            
            cell.imgViewUser.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.logo ?? "")")), completed: nil)
            
            }
        
        }
       
        return cell
    }
    
}



class SearchProductCustomerCell:UICollectionViewCell{
    
    @IBOutlet weak var imgViewUser: UIImageView!
    
    @IBOutlet weak var lblName: NKLabel!
    
}
