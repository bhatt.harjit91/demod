//
//  SearchProductCustomerDetailVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 12/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import SafariServices

class SearchProductCustomerDetailVC: AppBaseViewController {

    @IBOutlet weak var imgViewCustomer: UIImageView!
    
    @IBOutlet weak var lblName: NKLabel!
    
    @IBOutlet weak var txtViewDes: NKTextView!
    
    @IBOutlet weak var btnCompanyWebsite: NKButton!
    
    @IBOutlet weak var heightTxtViewDes: NSLayoutConstraint!
    
    var customerDetail:CustomerModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if customerDetail?.logo?.count ?? 0 > 0 {
                   
                   imgViewCustomer.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(customerDetail?.logo ?? "")")), completed: nil)
                   
                }
        
        lblName.text = customerDetail?.customerName ?? ""
        
        txtViewDes.text = customerDetail?.description ?? ""
        
        if customerDetail?.description?.count ?? 0 > 100 {
            
            heightTxtViewDes.constant = txtViewDes.contentSize.height
            
        }
        
        navigationItem.title = "Customer".localized()
        
    }
    
    

    @IBAction func btnCompanyWebsiteAction(_ sender: Any) {
        
        if let strUrl = customerDetail?.website
        {
            let url: URL?
            if strUrl.hasPrefix("http://") || strUrl.hasPrefix("https://") {
                url = URL(string: strUrl)
            } else {
                url = URL(string: "https://" + strUrl)
            }
            if let url = url {
                
                let sfViewController = SFSafariViewController(url: url)
                self.present(sfViewController, animated: true, completion: nil)
            }
        }
        
        
    }
    
}
