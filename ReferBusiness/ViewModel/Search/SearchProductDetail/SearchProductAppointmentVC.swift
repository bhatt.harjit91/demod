//
//  SearchProductAppointmentVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 31/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class SearchProductAppointmentVC: AppBaseViewController {
    
    @IBOutlet weak var tableViewList: UITableView!
    
    var workingDays:[AppointmentModel]?
    
    var productCode:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Working hours".localized()
        
        fetchAppointmentList()
        
    }
    
    
    func fetchAppointmentList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/workingDayList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&productCode=\(productCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(AppointmentApiModel.self, from: data)
                                
                                if let apiResponse:[AppointmentModel] = jsonResponse.workingDays {
                                    
                                    weakSelf?.workingDays = apiResponse
                                    
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
}

extension SearchProductAppointmentVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return workingDays?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let model:AppointmentModel = workingDays?[indexPath.row]{
            
            if model.isHoliday == 1 {
                
                if let cell:AppointmentHolidayCell = tableView.dequeueReusableCell(withIdentifier: "AppointmentHolidayCell", for: indexPath) as? AppointmentHolidayCell{
                    
                    return getCellForHoliday(cell: cell, indexPath: indexPath)
                }
            }
            else{
                
                if let cell:AppointmentListCell = tableView.dequeueReusableCell(withIdentifier: "AppointmentListCell", for: indexPath) as? AppointmentListCell{
                    
                    return getCellForAppointmentList(cell: cell, indexPath: indexPath)
                }
                
            }
            
        }
        
        return UITableViewCell.init()
    }
    
    func getCellForAppointmentList(cell:AppointmentListCell,indexPath:IndexPath) -> AppointmentListCell
    {
        if let model:AppointmentModel = workingDays?[indexPath.row]{
            
            cell.lblDay.text = model.dayName ?? ""
            cell.workingTime = model.workingTime ?? []
            cell.collectionViewSlots.reloadData()
            
        }
        
        return cell
    }
    
    func getCellForHoliday(cell:AppointmentHolidayCell,indexPath:IndexPath) -> AppointmentHolidayCell
    {
        
        if let model:AppointmentModel = workingDays?[indexPath.row]{
            
            cell.lblDay.text = model.dayName ?? ""
            cell.lblHoliday.text = "Holiday".localized()
            
        }
        
        
        return cell
    }
    
    
}

extension AppointmentListCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 220, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return workingTime?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell:AppointmentSlotCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppointmentSlotCell", for: indexPath) as? AppointmentSlotCell {
            
            return getCellForAppointment(cell: cell, indexPath: indexPath)
            
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func getCellForAppointment(cell:AppointmentSlotCell,indexPath:IndexPath) -> AppointmentSlotCell
    {
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "HH:mm a"
        dateFormatterPrint.timeZone = .current
        
        if let model:AppointmentTimeModel = workingTime?[indexPath.row]{
            
            if let date = dateFormatterGet.date(from: model.startTime ?? "") {
                
                cell.lblStartTime.text = dateFormatterPrint.string(from: date)
                
            }
            if let date = dateFormatterGet.date(from: model.endTime ?? "") {
                
                cell.lblEndTime.text = dateFormatterPrint.string(from: date)
                
            }
            
        }
        
        return cell
    }
    
    
}



class AppointmentSlotCell: UICollectionViewCell {
    
    @IBOutlet weak var lblStartTime: NKLabel!
    
    @IBOutlet weak var lblEndTime: NKLabel!
    
    @IBOutlet weak var viewBg: RCView!
    
    
    @IBOutlet weak var viewLine: UIView!
    
}

class AppointmentHolidayCell: UITableViewCell {
    
    @IBOutlet weak var lblHoliday: NKLabel!
    
    @IBOutlet weak var viewBg: RCView!
    
    @IBOutlet weak var lblDay:NKLabel!
    
    @IBOutlet weak var viewBackground: RCView!
    
}

class AppointmentListCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var lblDay:NKLabel!
    
    @IBOutlet weak var collectionViewSlots: UICollectionView!
    
    var workingTime:[AppointmentTimeModel]?
    
    override func awakeFromNib() {
        
        collectionViewSlots.delegate = self
        collectionViewSlots.dataSource = self
        
    }
}
