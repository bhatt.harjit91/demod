//
//  SearchProductDetailVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 29/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import MapKit
import SafariServices
import WebKit
 
class SearchProductDetailVC: AppBaseViewController {
    
    var Activity:UIActivityIndicatorView!
    
    @IBOutlet weak var viewBannerBg: UIView!
    
    @IBOutlet weak var btnReferNow: UIButton!
    
    @IBOutlet weak var collectionViewBanners: UICollectionView!
    
    @IBOutlet weak var stackViewRating: UIStackView!
    @IBOutlet weak var lblCompanyName: NKLabel!
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblProductShortDes: NKLabel!
    
    @IBOutlet weak var lblProductDes: NKLabel!
    
    @IBOutlet weak var lblPrice: NKLabel!
    
    @IBOutlet weak var lblReferralFee: NKLabel!
    
    @IBOutlet weak var lblReferralFeeTitle: NKLabel!
    
    @IBOutlet weak var lblReferralText: NKLabel!
    
    @IBOutlet weak var stackViewPriceReferral: UIStackView!
    
    @IBOutlet weak var heightStackViewPrice: NSLayoutConstraint!
    
    @IBOutlet weak var lblDistance: NKLabel!
    
    @IBOutlet weak var lblShortDes: NKLabel!
    
    @IBOutlet weak var lblSellerDes: NKLabel!
    
    @IBOutlet weak var imgLogo: RCImageView!
    
    @IBOutlet weak var imgViewLocation: UIImageView!
    
    @IBOutlet weak var heightViewProductInfo: NSLayoutConstraint!
    
    @IBOutlet weak var btnSeeMore: UIButton!
    
    @IBOutlet weak var heightSellerInfo: NSLayoutConstraint!
    
    @IBOutlet weak var heightReferralText: NSLayoutConstraint!
    
    @IBOutlet weak var lblAddressTitle: NKLabel!
    
    @IBOutlet weak var lblAddressText: NKLabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var heightViewlocationAndReferNow: NSLayoutConstraint!
    
    @IBOutlet weak var lblNotesToReferralMember: NKLabel!
    
    @IBOutlet weak var collectionViewAttachment: UICollectionView!
    
    @IBOutlet weak var lblCustomersCount: NKLabel!
    
    @IBOutlet weak var imgCustomerRightArrow: UIImageView!
    
    @IBOutlet weak var lblTestimonials: NKLabel!
    
    @IBOutlet weak var imgTestimonialRightArrow: UIImageView!
    
    @IBOutlet weak var viewCustomerCount: RCView!
    
    @IBOutlet weak var viewTestimonial: RCView!
    
    @IBOutlet weak var heightViewTestimonial: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewCustomer: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewAttachments: NSLayoutConstraint!
    
    @IBOutlet weak var lblPriceTitle: NKLabel!
    
    @IBOutlet weak var viewAttachments: RCView!
    
    @IBOutlet weak var btnGift: UIButton!
    
    @IBOutlet weak var btnWeb: UIButton!
    
    @IBOutlet weak var btnYoutubeLink: UIButton!
    
    @IBOutlet weak var lblGift: NKLabel!
    
    @IBOutlet weak var lblWeb: NKLabel!
    
    @IBOutlet weak var lblYouTube: NKLabel!
    
    @IBOutlet weak var stackViewGiftWebLink: UIStackView!
    
    @IBOutlet weak var heightOfStackViewGiftWebLink: NSLayoutConstraint!
    
    @IBOutlet weak var bottomLblNotesToSeller: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceLblNotes: NSLayoutConstraint!
    
    
    @IBOutlet weak var topSpaceTestimonial: NSLayoutConstraint!
    
    @IBOutlet weak var lblAttachmentsTitle: NKLabel!
    
    @IBOutlet weak var lblSellerInfoTitle: NKLabel!
    
    @IBOutlet weak var lblProfileTitle: NKLabel!
    
    @IBOutlet weak var lblProductInfoTitle: NKLabel!
    
    @IBOutlet weak var rightSpaceForCompanyName: NSLayoutConstraint!
    
    @IBOutlet weak var rightSpaceForLblShortDes: NSLayoutConstraint!
    
    @IBOutlet weak var viewDes: UIView!
    
    @IBOutlet weak var lblHtmlDes: NKLabel!
    
    @IBOutlet weak var imgViewProduct: RCImageView!
    
    @IBOutlet weak var lblProductCode: NKLabel!
    
    @IBOutlet weak var viewStatusStack: UIView!
    
    @IBOutlet weak var heightStackView: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewStatus: UIImageView!
    
    @IBOutlet weak var lblStatus: NKLabel!
    
    @IBOutlet weak var imgViewReviews: UIImageView!
    
    @IBOutlet weak var lblReviews: NKLabel!
    
    @IBOutlet weak var imgViewVerify: UIImageView!
    
    @IBOutlet weak var viewStatusStackBg: UIView!
    
    @IBOutlet weak var stackViewStatus: UIStackView!
    
    @IBOutlet weak var lblVerified: NKLabel!
    
    @IBOutlet weak var imgViewRegister: UIImageView!
       
    @IBOutlet weak var imgViewRefer: UIImageView!
    
     @IBOutlet weak var stackViewReferRegister: UIStackView!
    
     @IBOutlet weak var btnRegister: UIButton!
    
    
    @IBOutlet weak var lblRegister: NKLabel!
    
    @IBOutlet weak var lblReferNow: NKLabel!
    
    @IBOutlet weak var viewRegister: RCView!
    
    @IBOutlet weak var viewReferNow: RCView!
    
    var productList:SearchMainModel?
    
    var delayTime:Double = 8.0
    
    var x = 1
    
    var timer : Timer?
    
    var isExpand:Bool = false
    
    var lblHeight:CGFloat = 0.0
    
    var isReferralMember:Int = 0
    
    var isBuyer:Int = 0
    
    var productDetails:SearchDetailModel?
    
    var currentLoc:CLLocationCoordinate2D?
    
    var ipAddress = ""
    
    let linkAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 4),
        .foregroundColor: UIColor.btnLinkColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let locAttributes: [NSAttributedString.Key: Any] = [
    .font: UIFont.fontForType(textType: 8),
    .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
    .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var branchCode:Int = 0
    var sellerCode:Int = 0
    var productCode:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Activity = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        Activity.transform = CGAffineTransform(scaleX: 2, y: 2)
        
        heightStackView.constant = 0
        viewStatusStack.alpha = 0
        
       // stackViewReferRegister.arrangedSubviews[0].isHidden = true
        if viewRegister != nil{
            stackViewReferRegister.removeArrangedSubview(viewRegister)
        }
       
        updateInitialView()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if self.isMovingFromParent {
            
            let isUpdated:[String: Int] = ["update": 1]
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callLocation"), object: nil, userInfo: isUpdated)
            
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
       Activity.stopAnimating()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
        Activity.stopAnimating()
    }
    
    
    func updateInitialView(){
        
        imgViewRefer.setImageColor(color: .white)
        
        imgViewRegister.setImageColor(color: .white)
        
        lblReferralFeeTitle.text = "Referral Fee".localized()
        
        lblSellerInfoTitle.text = "Company Information".localized()
        
        bottomLblNotesToSeller.constant = 0
        
        topSpaceLblNotes.constant = 0
        
        navigationItem.title = "iCanRefer".localized()
        
        btnReferNow.addTarget(self, action: #selector(btnReferNowAction(_:)), for: .touchUpInside)
        
        btnRegister.addTarget(self, action: #selector(btnRegisterProductAction(_:)), for: .touchUpInside)
        
        lblRegister.text = " Register".localized()
        
        if let userModel = getUserDetail() {
            
            isReferralMember = userModel.isReferralMember ?? 0
            
            isBuyer = userModel.isBuyer ?? 0
            
        }
        
        
        // let color = UIColor.getGradientColor(index: 0).first!
        
        imgViewLocation.setImageColor(color: UIColor.btnLinkColor)
        
        imgCustomerRightArrow.setImageColor(color: .lightGray)
        
        imgTestimonialRightArrow.setImageColor(color: .lightGray)
        
        lblDistance.textColor = UIColor.btnLinkColor
        
        btnSeeMore.alpha = 1
        
        heightSellerInfo.constant = 210
        
        heightViewCustomer.constant = 0
        
        heightViewTestimonial.constant = 0
        
        btnSeeMore.addTarget(self, action: #selector(updateHeight), for: .touchUpInside)
        
        btnSeeMore.setTitleColor(UIColor.btnLinkColor, for: .normal)
        
        lblNotesToReferralMember.textColor = UIColor.colorWithHexString(hexString: "#F25E65")
        
        heightViewAttachments.constant = 0
        
        viewAttachments.alpha = 0
        
        btnWeb.addTarget(self, action: #selector(btnWebAction), for: .touchUpInside)
        
        btnGift.addTarget(self, action: #selector(btnSurpriseGiftAction), for: .touchUpInside)
        
        btnYoutubeLink.addTarget(self, action: #selector(btnYouTubeLinkAction), for: .touchUpInside)
        
        
        fetchProductDetail(branchCode: branchCode, lat: currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, sellerCode: sellerCode , productCode: productCode )
    }
    
    func updateStackStatus(){
        
        if let model:SearchDetailModel = productDetails{
            
        imgViewStatus.setImageColor(color: .primaryColor)
        imgViewReviews.setImageColor(color: .btnLinkColor)
    
        lblStatus.textColor = .primaryColor
        lblReviews.textColor = UIColor.btnLinkColor
        
        viewStatusStackBg.layer.cornerRadius = 5
        viewStatusStackBg.layer.borderWidth = 1.0
        viewStatusStackBg.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        viewStatusStackBg.layer.shadowColor = UIColor.black.cgColor
        viewStatusStackBg.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewStatusStackBg.layer.shadowOpacity = 0.3
        viewStatusStackBg.layer.shadowRadius = 4.0
            
        stackViewStatus.arrangedSubviews[0].alpha = 0
        stackViewStatus.arrangedSubviews[1].alpha = 0
        stackViewStatus.arrangedSubviews[2].alpha = 0
            
            if (model.enableAppointment == 1 && model.workingStatus ?? 0 > 0) || model.rating ?? 0.0 > 0.0 || model.isVerified == 1{
                heightStackView.constant = 60
                viewStatusStack.alpha = 1
            }
            else{
                 heightStackView.constant = 0
                 viewStatusStack.alpha = 0
            }
            
            if model.enableAppointment == 1 {
                
                if model.workingStatus ?? 0  == 1 {
                    
                    lblStatus.text = model.workingStatusText ?? ""
                    lblStatus.textColor = .greenSea
                    imgViewStatus.setImageColor(color: .greenSea)
                    
                }
                else if model.workingStatus ?? 0  == 2{
                  
                  lblStatus.text = model.workingStatusText ?? ""
                  lblStatus.textColor = .primaryColor
                  imgViewStatus.setImageColor(color: .primaryColor)
                    
                }
                
                stackViewStatus.arrangedSubviews[0].alpha = 1
            }
            
            if model.rating ?? 0.0 > 0{
                
             stackViewStatus.arrangedSubviews[1].alpha = 1
             imgViewReviews.alpha = 1
                            
            if model.totalReviews ?? 0 == 1
             {
          
                lblReviews.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(" ")\("Review".localized())\(")")"
                                
                }
                else
                 {
                    lblReviews.text =  "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(" ")\("Reviews".localized())\(")")"
                  }
        
              }
            
            if model.isVerified == 1{
                stackViewStatus.arrangedSubviews[2].alpha = 1
                lblVerified.text = "Verified".localized()
            }
            
        }
    }
    
    
    @IBAction func btnNavigateToMapView(_ sender: Any) {
        
        openNaviagationMap(latitude: productDetails?.latitude ?? 0.0, longitude: productDetails?.longitude ?? 0.0, name: productDetails?.branchName ?? "")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // updateUI()
        
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        if timer != nil {
            timer!.invalidate()
            timer = nil
        }
        
    }
    
    @objc func updateHeight(){
        
        
        var heightView:CGFloat = 190.0
        
        if isExpand == false {
            
            
            lblSellerDes.numberOfLines = 0
            
            guard let labelText = lblSellerDes.text else { return }
            let height = estimatedHeightOfLabel(text: labelText)
            
            lblHeight = height
            
            if isBuyer == 0 {
                
                if productDetails?.customerCount ?? 0 > 0 {
                    
                    heightView = 230
                }
                
                if productDetails?.testimonialCount ?? 0 > 0 {
                    
                    heightView = 280
                }
                
            }
            
            heightSellerInfo.constant = lblHeight + heightView
            
            isExpand = true
            
            btnSeeMore.setTitle("See Less".localized(), for: .normal)
            
        }
            
        else{
            
            isExpand = false
            
            lblSellerDes.numberOfLines = 2
            
            heightView = 180
            
            if isBuyer == 0 {
                
                if productDetails?.customerCount ?? 0 > 0 {
                    
                    heightView = 230
                }
                if productDetails?.testimonialCount ?? 0 > 0 {
                    
                    heightView = 280
                }
                
            }
            
            heightSellerInfo.constant = heightView
            
            btnSeeMore.setTitle("See More".localized(), for: .normal)
        }
        
    }
    
    
    
    func updateUI() {
        
        if let model:SearchDetailModel = productDetails{
            
            if model.productLogo?.count ?? 0 > 0 {
                
                imgViewProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
                
            }
            else{
                
                imgViewProduct.image = UIImage.init(named: "Logo")
                
            }
            
            let des = model.description ?? ""
            
            if des.count > 0 {
                
                if des.contains("<") {
                    
                    lblProductShortDes.attributedText = des.html2AttributedString
                    
                }
                else {
                    
                    lblProductShortDes.text = des
                }
            }
            
            let shortDes = model.shortDescription
            
            if shortDes?.count ?? 0 > 0 && model.companyType != 1 {
                
                if shortDes?.contains("<") ?? false {
                    
                    lblHtmlDes.attributedText = shortDes?.html2AttributedString
                    
                }
                else {
                    
                    lblHtmlDes.text = shortDes
                }
                
                viewDes.layer.cornerRadius = 5
                viewDes.layer.borderWidth = 1.0
                viewDes.layer.borderColor = UIColor.groupTableViewBackground.cgColor
                viewDes.layer.shadowColor = UIColor.black.cgColor
                viewDes.layer.shadowOffset = CGSize(width: 3, height: 3)
                viewDes.layer.shadowOpacity = 0.3
                viewDes.layer.shadowRadius = 4.0
            }
            else{
                
                if viewDes != nil {
                    if self.view.subviews.contains(viewDes) {
                        viewDes.removeFromSuperview()
                    }
                }
            }
            
            
            let desSeller = model.sellerProfile ?? ""
            
            if desSeller.contains("<") {
                
                lblSellerDes.attributedText = desSeller.html2AttributedString
                
                lblSellerDes.text = lblSellerDes.text?.removeWhitespaceSpecial()
                
            }
            else {
                
                lblSellerDes.text = desSeller
                
                lblSellerDes.text = lblSellerDes.text?.removeWhitespaceSpecial() 
                
            }
            
            lblProductCode.text = model.productCodeText ?? ""
            
            lblProductName.text = model.productName ?? ""
            // lblProductShortDes.text = "\(model.description ?? "")"
            lblNotesToReferralMember.alpha = 0
            lblPrice.text = "\(model.productPrice ?? "")"
            lblReferralFee.text = "\(model.referralFee ?? "")"
            lblCompanyName.text = model.companyName ?? ""
            
            // lblProductDes.text = model.description ?? ""
            
            imgViewVerify.alpha = 0
            rightSpaceForCompanyName.constant = 10
            rightSpaceForLblShortDes.constant = 10
            
            lblProductInfoTitle.text = "Product Information".localized()
            
//            if model.isVerified == 1 {
//
//                rightSpaceForCompanyName.constant = 10
//                rightSpaceForLblShortDes.constant = 10
//            }
            
            if model.productType == 1 {
                lblProductInfoTitle.text = "Job Information".localized()
                imgViewProduct.image = UIImage.init(named: "jobDefault")
            }
            
            if model.productType == 2 {
                lblProductInfoTitle.text = "Event Information".localized()
                imgViewProduct.image = UIImage.init(named: "eventIcon")
            }
            
            heightReferralText.constant = 0
            
            stackViewRating.arrangedSubviews[0].isHidden = true
        
            heightViewlocationAndReferNow.constant = 0
            
            if model.isGift == 1
            {
                lblGift.text = "Gift".localized()
                stackViewGiftWebLink.arrangedSubviews[0].isHidden = false
            }
            else
            {
                stackViewGiftWebLink.arrangedSubviews[0].isHidden = true
            }
            if model.website?.count ?? 0 > 0
            {
                lblWeb.text = "Web".localized()
                stackViewGiftWebLink.arrangedSubviews[1].isHidden = false
            }
            else
            {
                stackViewGiftWebLink.arrangedSubviews[1].isHidden = true
            }
            
            if model.youtubeLink?.count ?? 0 > 0
            {
                lblYouTube.text = "YouTube".localized()
                stackViewGiftWebLink.arrangedSubviews[2].isHidden = false
            }
            else
            {
                stackViewGiftWebLink.arrangedSubviews[2].isHidden = true
            }
            
            if model.isGift == 0 && model.website?.count ?? 0 <= 0 && model.youtubeLink?.count ?? 0 <= 0
            {
                heightOfStackViewGiftWebLink.constant = 0
                stackViewGiftWebLink.arrangedSubviews[0].isHidden = true
                stackViewGiftWebLink.arrangedSubviews[1].isHidden = true
                stackViewGiftWebLink.arrangedSubviews[2].isHidden = true
            }
            
            if model.notesForReferralMember?.count ?? 0 > 0 && isReferralMember == 1{
                
                bottomLblNotesToSeller.constant = 6
                
                topSpaceLblNotes.constant = 6
                
                lblNotesToReferralMember.alpha = 1
                
                lblNotesToReferralMember.text = "\(model.notesForReferralMember ?? "")"
                
            }
            
            
            if model.referredCount ?? 0 > 0 {
                
                heightReferralText.constant = 21
                
                lblReferralText.text = "\("You referred this to ".localized())\(model.referredCount ?? 0)\(" ")\("Persons".localized())"
                
                
            }
            
            
            
//            if model.rating ?? 0.0 > 0
//            {
//                stackViewRating.arrangedSubviews[0].isHidden = true
//                imageViewStar.alpha = 1
//
//                if model.rating ?? 0.0 == 1
//                {
////                    lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(" ")\("Review".localized())\(")")"
//
//                    lblReviews.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(" ")\("Review".localized())\(")")"
//
//                }
//                else
//                {
//                   // lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(" ")\("Reviews".localized())\(")")"
//
//                     lblReviews.text =  "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(" ")\("Reviews".localized())\(")")"
//                }
//
//                heightViewlocationAndReferNow.constant = 40
//            }
//            else
//            {
//                stackViewRating.arrangedSubviews[0].isHidden = true
//                imageViewStar.alpha = 0
//            }
            
            if model.distance?.count ?? 0 > 0
            {
                stackViewRating.arrangedSubviews[0].isHidden = false
                imgViewLocation.alpha = 1
                
                lblDistance.text = "\(model.distance ?? "")"
                
                heightViewlocationAndReferNow.constant = 40
            }
            else
            {
                stackViewRating.arrangedSubviews[0].isHidden = true
                imgViewLocation.alpha = 0
                
            }
            
            
            // lblSellerDes.text = model.sellerProfile ?? ""
            
            lblShortDes.text  = model.tagLine ?? ""
            
            stackViewPriceReferral.arrangedSubviews[0].isHidden = true
            
            
            
            stackViewPriceReferral.alpha = 0
            heightStackViewPrice.constant = 0
            
            // heightViewProductInfo.constant = 100
            
            
            if lblSellerDes.text?.count ?? 0 > 0 {
                btnSeeMore.alpha = 0
                lblProfileTitle.alpha = 1
                heightSellerInfo.constant = 190
                if lblSellerDes.text?.count ?? 0 > 50{
                    btnSeeMore.alpha = 1
                }
            }
            else{
                
                heightSellerInfo.constant = 80
                btnSeeMore.alpha = 0
                lblProfileTitle.alpha = 0
            }
            
            if model.productPrice?.count ?? 0 > 0 {
                
                stackViewPriceReferral.arrangedSubviews[0].isHidden = false
                stackViewPriceReferral.arrangedSubviews[1].isHidden = true
                heightStackViewPrice.constant = 80
                stackViewPriceReferral.alpha = 1
                
            }
            
            if model.referralFee?.count ?? 0 > 0 {
                
                stackViewPriceReferral.arrangedSubviews[1].isHidden = false
                stackViewPriceReferral.arrangedSubviews[0].isHidden = true
                heightStackViewPrice.constant = 80
                stackViewPriceReferral.alpha = 1
                
            }
            
            
            
            if model.productPrice?.count ?? 0 > 0 && model.referralFee?.count ?? 0 > 0 {
                 
                stackViewPriceReferral.arrangedSubviews[0].isHidden = false
                stackViewPriceReferral.arrangedSubviews[1].isHidden = false
                stackViewPriceReferral.alpha = 1
                heightStackViewPrice.constant = 188
                
            }
            
            if model.companyLogo?.count ?? 0 > 0 {
                imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.companyLogo ?? "")")), completed: nil)
            }
            
            if model.banners?.count ?? 0 > 0 {
                
                if model.isVideo == 1 {
                    viewBannerBg.aspectRation(9.0 / 16.0).isActive = true
                    if let fileName = model.banners?.first {
                        Activity.startAnimating()
                        if let imagePath = fileName["cdnPath"]{
                            
                            guard let youtubeURL = URL(string: "https://www.youtube.com/embed/\(imagePath.youtubeID ?? "")") else {
                                return
                            }
                            let webView = WKWebView()
                           // webView.uiDelegate = self as! WKUIDelegate
                             
                              
                            
                            //webView.aspectRation(9.0 / 16.0).isActive = true
                            webView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height * 0.316)
                            Activity.center = webView.center
                            webView.addSubview(Activity)
                            viewBannerBg.addSubview(webView)
                            viewBannerBg.layoutIfNeeded()
                            webView.navigationDelegate = self
                            webView.uiDelegate = self
                            webView.load(URLRequest(url: youtubeURL))
                            webView.backgroundColor = .yellow
                             collectionViewBanners.isHidden = true
                           // viewBannerBg.load(withVideoId: imagePath.youtubeID ?? "")
                            
                         }
                    }
                    
                }else{
                     viewBannerBg.aspectRation(9.0 / 16.0).isActive = true
                    collectionViewBanners.reloadData()
                    startTimer()
                }
                 
            }
            
            if model.attachmentList?.count ?? 0 > 0 {
                
                heightViewAttachments.constant = 210
                
                viewAttachments.alpha = 1
                
                collectionViewAttachment.reloadData()
                
            }
            
            topSpaceTestimonial.constant = 0
            
            if isBuyer == 0 {
                
                if model.customerCount ?? 0 > 0 {
                    
                    let custAttributedString = NSMutableAttributedString(string:"\("Customers (")\(model.customerCount ?? 0)\(")")" ,attributes: linkAttributes)
                    
                    lblCustomersCount.attributedText = custAttributedString
                    
                    heightSellerInfo.constant = 240
                    
                    heightViewCustomer.constant = 40
                    
                    topSpaceTestimonial.constant = 10
                    
                }
                
                if model.testimonialCount ?? 0 > 0 {
                    
                    let custAttributedString = NSMutableAttributedString(string:"\("Testimonial (")\(model.testimonialCount ?? 0)\(")")" ,attributes: linkAttributes)
                    
                    lblTestimonials.attributedText = custAttributedString
                    
                    heightViewTestimonial.constant = 40
                    
                    heightSellerInfo.constant = 290
                    
                    topSpaceTestimonial.constant = 10
                    
                }
                
            }
            
            if isReferralMember == 1
            {
                lblReferNow.text = " Refer Now".localized()
                
            }
            
            if isReferralMember == 0
            {
                lblReferNow.text = " Send Enquiry".localized()
                stackViewPriceReferral.arrangedSubviews[1].isHidden = true
                heightStackViewPrice.constant = 0
                stackViewPriceReferral.alpha = 0
                
                if model.productPrice?.count ?? 0 > 0 {
                    
                    stackViewPriceReferral.arrangedSubviews[0].isHidden = false
                    heightStackViewPrice.constant = 80
                    stackViewPriceReferral.alpha = 1
                    
                }
                
            }
            
            lblAddressText.text = model.address ?? ""
            lblAddressTitle.text = "Address".localized()
            
         lblAddressText.attributedText =  NSMutableAttributedString(string:  model.address ?? "",attributes: locAttributes)
            
            
            getMapUpdate(latitude: model.latitude ?? 0.0, longitude: model.longitude ?? 0.0, address:model.address ?? "")
                   
            imgViewRegister.setImageColor(color: .white)
            
          
                self.imgViewRefer.setImageColor(color: .white)
                self.imgViewRegister.setImageColor(color: .white)
                
                 if model.productType ?? 0 == 2 {
                   
                   // self.stackViewReferRegister.arrangedSubviews[0].isHidden = false
                    self.stackViewReferRegister.insertArrangedSubview(viewRegister, at: 0)
                    
                 }
          
           
                      
            
            updateStackStatus()
        }
        
    }
    
    @objc func btnWebAction()
    {
        if let model:SearchDetailModel = productDetails{
            
            if let strUrl = model.website
            {
                let url: URL?
                if strUrl.hasPrefix("http://") || strUrl.hasPrefix("https://") {
                    url = URL(string: strUrl)
                } else {
                    url = URL(string: "https://" + strUrl)
                }
                if let url = url {
                    
                    let sfViewController = SFSafariViewController(url: url)
                    self.present(sfViewController, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func btnYouTubeLinkAction()
    {
        if let model:SearchDetailModel = productDetails{
            
            if let strUrl = model.youtubeLink
            {
                let url: URL?
                if strUrl.hasPrefix("http://") || strUrl.hasPrefix("https://") {
                    url = URL(string: strUrl)
                } else {
                    url = URL(string: "https://" + strUrl)
                }
                if let url = url {
                    
                    let sfViewController = SFSafariViewController(url: url)
                    self.present(sfViewController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    
    @objc func btnSurpriseGiftAction()
    {
        
        if let model:SearchDetailModel = productDetails{
            
            let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
            
            if let surpriseGiftVC:SearchProductGiftDetailVC = storyBoard.instantiateViewController(withIdentifier: "SearchProductGiftDetailVC") as? SearchProductGiftDetailVC {
                
                surpriseGiftVC.productCode = model.productCode ?? 0
                
                surpriseGiftVC.sellerCode = model.sellerCode ?? 0
                
                self.navigationController?.pushViewController(surpriseGiftVC, animated: true)
                
            }
            
        }
    }
    
    @IBAction func btnStatusAction(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
        
        if let productAppoinment:SearchProductAppointmentVC = storyBoard.instantiateViewController(withIdentifier: "SearchProductAppointmentVC") as? SearchProductAppointmentVC {
        
          productAppoinment.productCode = productDetails?.productCode ?? 0
      
         navigationController?.pushViewController(productAppoinment, animated: true)
            
        }
    }
    
    @IBAction func btnReviewsAction(_ sender: UIButton) {
    }
    
    
    @IBAction func btnNavAction(_ sender: UIButton) {
    }
    func getMapUpdate(latitude:Double,longitude:Double,address:String)
    {
        let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(region, animated: true)
        
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        myAnnotation.title = address
        mapView.addAnnotation(myAnnotation)
        
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let neededSize = lblSellerDes.sizeThatFits(CGSize(width: view.frame.width - 16, height: CGFloat.greatestFiniteMagnitude))
        
//        let size = CGSize(width: view.frame.width - 16, height: 1000)
//
//        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
//
//        let attributes = [NSAttributedString.Key.font: UIFont.fontForType(textType: 3)]
//
//        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        let rectangleHeight = neededSize.height
        
        return rectangleHeight
    }
    
    
    @IBAction func btnReferNowAction(_ sender: Any)
    {
        
        let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
        let newProductPage:SearchReferProductVC = storyBoard.instantiateViewController(withIdentifier: "SearchReferProductVC") as! SearchReferProductVC
        newProductPage.productCode = productDetails?.productCode ?? 0
        newProductPage.productName = productDetails?.productName ?? ""
        newProductPage.sellerCode = productDetails?.sellerCode ?? 0
        newProductPage.referralNoteToSeller = productDetails?.notesForReferralMember ?? ""
        newProductPage.companyName = productDetails?.companyName ?? ""
        newProductPage.productLogo = productDetails?.productLogo ?? ""
        newProductPage.productType = productDetails?.productType ?? 0
        newProductPage.isEnableAppointment = productDetails?.enableAppointment ?? 0
        newProductPage.enablePayment = productDetails?.enablePayment ?? 0
        
        if isReferralMember == 1 {
            
            newProductPage.referralMsg = productDetails?.referralFormMsg ?? ""
            
        }
        else{
            
            newProductPage.referralMsg = productDetails?.enquiryOrApplyJobFormMsg ?? ""
        }
        
        navigationController?.pushViewController(newProductPage, animated: true)
        
    }
    
    @objc func btnRegisterProductAction(_ sender: UIButton){
            
            let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
            let referProductPage:SearchReferProductVC = storyBoard.instantiateViewController(withIdentifier: "SearchReferProductVC") as! SearchReferProductVC
            referProductPage.productCode = productDetails?.productCode ?? 0
            referProductPage.productName = productDetails?.productName ?? ""
            referProductPage.companyName = productDetails?.companyName ?? ""
            referProductPage.productLogo = productDetails?.productLogo ?? ""
            referProductPage.sellerCode = productDetails?.sellerCode ?? 0
            referProductPage.productType = productDetails?.productType ?? 0
            referProductPage.referralNoteToSeller = productDetails?.notesForReferralMember ?? ""
            referProductPage.isEnableAppointment = productDetails?.enableAppointment ?? 0
            referProductPage.enablePayment = productDetails?.enablePayment ?? 0
            referProductPage.isRegister = 1
            referProductPage.paymentType = productDetails?.paymentType
            referProductPage.productType = productDetails?.productType ?? 0
            
            if isReferralMember == 1 {
                
                referProductPage.referralMsg = productDetails?.referralFormMsg ?? ""
                
            }
            else{
                referProductPage.referralMsg = productDetails?.enquiryOrApplyJobFormMsg ?? ""
            }
            navigationController?.pushViewController(referProductPage, animated: true)
        
    }
    
    @IBAction func btnRatingAction(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
        let reviews:SearchRatingVC = storyBoard.instantiateViewController(withIdentifier: "SearchRatingVC") as! SearchRatingVC
        reviews.sellerCode = productDetails?.sellerCode ?? 0
        reviews.productCode = productDetails?.productCode ?? 0
        
        navigationController?.pushViewController(reviews, animated: true)
        
    }
    
    
    @IBAction func btnLocationAction(_ sender: UIButton) {
        
        openNaviagationMap(latitude: productDetails?.latitude ?? 0.0, longitude: productDetails?.longitude ?? 0.0, name: productDetails?.branchName ?? "")
    }
    
    @IBAction func btnCustomerAction(_ sender: Any) {
       
        let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
        let newCustomerPage:SearchProductCustomerVC = storyBoard.instantiateViewController(withIdentifier: "SearchProductCustomerVC") as! SearchProductCustomerVC
        newCustomerPage.sellerCode = productDetails?.sellerCode ?? 0
        
        navigationController?.pushViewController(newCustomerPage, animated: true)
        
    }
    
    @IBAction func btnTestimonialAction(_ sender: Any) {
        
        let storyBoard = UIStoryboard.init(name: TestimonialSB, bundle: nil)
        
        let testimonialListPage:TestimonialListVC = storyBoard.instantiateViewController(withIdentifier: "TestimonialListVC") as!   TestimonialListVC
        testimonialListPage.sellerCode = productDetails?.sellerCode ?? 0
        testimonialListPage.isFromProduct = true
        
        navigationController?.pushViewController(testimonialListPage, animated: true)
        
    }
    
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        
        if x < productDetails?.banners?.count ?? 0 {
            let indexPath = IndexPath(item: x, section: 0)
            collectionViewBanners.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            x = x + 1
            
        } else {
            
            x = 0
            
            collectionViewBanners.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
            
        }
        
        
    }
    
    
    func startTimer() {
        
        timer =  Timer.scheduledTimer(timeInterval: delayTime, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        
    }
    
    func fetchProductDetail(branchCode:Int,lat:Double,long:Double,sellerCode:Int,productCode:Int){
        
        weak var weakSelf = self
        
        
        DispatchQueue.main.async {
            
            weakSelf?.startAnimating()
        }
        
        let params:NSDictionary = ["branchCode":branchCode,
                                   "latitude" : lat,
                                   "longitude" : long,
                                   "productCode": productCode,
                                   "sellerCode":sellerCode,
                                   "ip":ipAddress
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        
        let apiClient:APIClient = APIClient()
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/productsToReferDetailPage?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                
                                
                                print(dataObj)
                                
                                let decoder = JSONDecoder()
                                let jsonResponse = try decoder.decode(SearchDetailApiModel.self, from: data)
                                
                                
                                
                                if let apiResponse:SearchDetailModel = jsonResponse.productDetails {
                                    
                                    weakSelf?.productDetails = apiResponse
                                    
                                }
                                
                                weakSelf?.stopAnimating()
                                
                                weakSelf?.updateUI()
                                
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
            //Show error
            weakSelf?.stopAnimating()
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        weakSelf?.stopAnimating()
        
    }
    
    
    
}

extension SearchProductDetailVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView == collectionViewBanners {
            
            return CGSize.init(width: collectionViewBanners.frame.width, height: collectionViewBanners.frame.height)
            
        }
        
        if collectionView == collectionViewAttachment {
            
            return CGSize.init(width: 100.0, height: 100.0)
            
        }
        
        return CGSize.init()
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewBanners {
            
            if productDetails?.banners?.count ?? 0 > 0 {
                
                return productDetails?.banners?.count ?? 0
            }
        }
        
        if collectionView == collectionViewAttachment {
            
            if productDetails?.attachmentList?.count ?? 0 > 0 {
                
                return productDetails?.attachmentList?.count ?? 0
            }
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewBanners {
            
            if let cell:SearchDetailHeaderCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchDetailHeaderCell", for: indexPath) as? SearchDetailHeaderCell {
                
                return getCellForSearchDetailHeader(cell: cell, indexPath: indexPath)
                
            }
        }
        
        if collectionView == collectionViewAttachment {
            
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachment(cell: cell, indexPath: indexPath)
                
            }
            
            
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewAttachment {
            
            if let fileName = productDetails?.attachmentList?[indexPath.row] {
                
                if let imagePath = fileName["cdnPath"] {
                    
                    if let filePath = fileName["fileName"] {
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                            let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                            imageViewer.strImg = imagePath
                            navigationController?.pushViewController(imageViewer, animated: true)
                            
                        }
                            
                        else{
                            
                            openWebView(urlString:imagePath, title: filePath, subTitle: "")
                        }
                        
                    }
                }
                
            }
            
        }
        
        //    if collectionView == collectionViewBanners {
        //
        //        if let fileName = productDetails?.banners[indexPath.row] {
        //
        //            if let imagePath = fileName["cdnPath"] {
        //
        //
        //            let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
        //                  let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
        //                  imageViewer.strImg = imagePath
        //                  navigationController?.pushViewController(imageViewer, animated: true)
        //
        //                }
        //
        //            }
        //
        //       }
        
        
    }
    
    
    func getCellForSearchDetailHeader(cell:SearchDetailHeaderCell,indexPath:IndexPath) -> SearchDetailHeaderCell {
        
        if productDetails?.banners?.count ?? 0 > 0 {
            
            if let fileName:[String:String] = productDetails?.banners?[indexPath.row]
            {
                if let imagePath = fileName["cdnPath"] 
                {
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        
                        cell.imgViewHeader.sd_setImage(with: url, completed: nil)
                    }
                    
                }
                
            }
        }
        return cell
    }
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.alpha = 0
        
        
        if let model = productDetails?.attachmentList?[indexPath.row]
        {
            if let imagePath = model["cdnPath"]
            {
                if let filePath = model["fileName"] {
                    
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            cell.imgView.sd_setImage(with: url, completed: nil)
                            
                        }
                        else{
                            cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                        }
                        
                    }
                }
                
            }
            if let attachmentType = model["attachmentType"]{
                
                cell.lblTitle.text = attachmentType
            }
            
        }
        return cell
    }
    
    
}

class SearchDetailHeaderCell:UICollectionViewCell{
    
    @IBOutlet weak var imgViewHeader: UIImageView!
    
    
}
