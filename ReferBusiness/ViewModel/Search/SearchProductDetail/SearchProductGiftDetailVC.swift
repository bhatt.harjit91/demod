//
//  SearchProductGiftDetailVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 12/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class SearchProductGiftDetailVC: AppBaseViewController {
    
    var sellerCode:Int = 0
    
    var productCode:Int = 0
    
    var giftData:SearchGiftDetailModel?

    @IBOutlet weak var imgViewGift: UIImageView!
    
    @IBOutlet weak var txtViewDes: NKTextView!
    
    @IBOutlet weak var heightTxtViewDes: NSLayoutConstraint!
    
    @IBOutlet weak var txtViewSpecialNotes: NKTextView!
    
    @IBOutlet weak var heightTxtViewSpecialNotes: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtViewSpecialNotes.textColor = .primaryColor
        
        navigationItem.title = "Gift".localized()

       fetchGiftDetail()
        
    }

    
    func updateUI() {
        
        if giftData != nil {
            
            if giftData?.imagePath?.count ?? 0 > 0 {
               
               imgViewGift.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(giftData?.imagePath ?? "")")), completed: nil)
               
            }
            
            txtViewSpecialNotes.text = giftData?.scheduleText ?? ""
            
            txtViewDes.text = giftData?.description ?? ""
            
            if giftData?.description?.count ?? 0 > 100{
                
               // adjustTextViewHeight(arg: txtViewDes)
                
                 heightTxtViewDes.constant = txtViewDes.contentSize.height
                
            }
            
        }
        
    }
    
    func fetchGiftDetail() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/productGiftPage?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&productCode=\(productCode)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        do {
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
                            print(dataObj)
                            
                            let jsonResponse = try decoder.decode(SearchGiftDetailApiModel.self, from: data)
                            
                            if let apiResponse:SearchGiftDetailModel = jsonResponse.giftData {

                              weakSelf?.giftData = apiResponse

                            }
                            
                            weakSelf?.updateUI()
                            
                            return
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                            
                        }
                      }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
   

}
