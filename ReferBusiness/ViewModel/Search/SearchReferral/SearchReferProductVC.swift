//
//  SearchReferProductVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 09/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import Razorpay

class SearchReferProductVC: SendOTPViewController,CountryCodeSelectViewControllerDelegate {
    
    @IBOutlet weak var circularViewMobileNumber: CircularView!
    
    @IBOutlet weak var viewBuyerCircularView: CircularView!
    
    @IBOutlet weak var viewBuyerCC: RCTextField!
    
    @IBOutlet weak var viewBuyerMobNo: RCTextField!
    
    @IBOutlet weak var circularViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewBuyerCircularHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewBottomBG: UIView!
    @IBOutlet weak var heightOfBottomView: NSLayoutConstraint!
    @IBOutlet weak var lbltextBottom: NKLabel!
    @IBOutlet weak var imgViewSucessImage: UIImageView!
    @IBOutlet weak var lblProductTitle: NKLabel!
    
    @IBOutlet weak var lblKeepAsAnonymous: NKLabel!
    
    @IBOutlet weak var heightViewOffirstView: NSLayoutConstraint!
    @IBOutlet weak var lblProvidedBy: NKLabel!
    
    @IBOutlet weak var txtCountryCode: RCTextField!
    
    @IBOutlet weak var txtMobileNo: RCTextField!
    
    @IBOutlet weak var imgViewProduct: RCImageView!
    
    @IBOutlet weak var btnSearch: NKButton!
    
    @IBOutlet weak var viewBuyerInfo: RCView!
    
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var imgCheckbox: UIImageView!
    
    @IBOutlet weak var txtFirstName: RCTextField!
    
    @IBOutlet weak var txtLastName: RCTextField!
    
    @IBOutlet weak var txtJobTitle: RCTextField!
    
    @IBOutlet weak var txtCompany: RCTextField!
    
    @IBOutlet weak var txtEmaild: RCTextField!
    
    @IBOutlet weak var heightOfBottomViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var txtViewNotesToSeller: NKTextView!
    
    @IBOutlet weak var txtViewGeneralNotes: NKTextView!
    
    //    @IBOutlet weak var viewBuyerName: RCView!
    
    // @IBOutlet weak var topSpaceViewSugesstion: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewBuyerInfo: NSLayoutConstraint!
    
    // @IBOutlet weak var lblBuyerName: NKLabel!
    @IBOutlet weak var viewScheduledAppointment: RCView!
    @IBOutlet weak var lblDateScheduledAppointment:NKLabel!
    @IBOutlet weak var lblNoOfPeopleScheduledAppointment:NKLabel!
    @IBOutlet weak var btnResourcesScheduledAppointment:UIButton!
    
    ///
    
    @IBOutlet weak var tblreferral: UITableView!
    @IBOutlet weak var viewReferralBG: UIView!
    @IBOutlet weak var heightOfReferralBgConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblNotes: NKLabel!
    
    @IBOutlet weak var heightViewNotifyBuyer: NSLayoutConstraint!
    
    @IBOutlet weak var viewNotifyBuyer: UIView!
    
    @IBOutlet weak var lblReferralMsg: NKLabel!
    
    @IBOutlet weak var heightViewReferralMsg: NSLayoutConstraint!
    
    
    @IBOutlet weak var heightCompanyName: NSLayoutConstraint!
    
    @IBOutlet weak var topSpaceForCompanyName: NSLayoutConstraint!
    
    
    var referralMsg:String?
    
    var selectedIndexPath: NSInteger = -1
    
    ///
    
    var userDetails:[ReferBuyerUserModel] = []
    
    var userModels:[userModel] = []
    
    var userPresent:Int?
    var buyeruserId:Int = 0
    
    var productCode:Int?
    
    var productName:String?
    
    var companyName:String?
    
    var selectedCountryCode : CountryCodeModel?
    
    var isSelected:Int = 0
    
    var userType:Int?
    
    var sellerCode:Int?
    
    var referralNoteToSeller:String?
    
    var productLogo:String?
    
    var rmAnonymous:Int = 0
    
    var productType:Int = 0
    
    var hideTransForBuyer:Int = 0
    
    var isTnC:Int = 0
    
    var resourceTypeArray = [ResourceTypeModel]()
    
    var btnResourcesTitle = ""
    
    @IBOutlet weak var viewBuyerInfoTitle: RCView!
    
    @IBOutlet weak var widthBtnSearch: NSLayoutConstraint!
    
    @IBOutlet weak var widthSucessImage: NSLayoutConstraint!
    
    @IBOutlet weak var viewAttachments: RCView!
    
    @IBOutlet weak var collectionViewAttachment: UICollectionView!
    
    @IBOutlet weak var imgViewAttachment: UIImageView!
    
    @IBOutlet weak var lblTopReferralText: NKLabel!
    
    @IBOutlet weak var viewNotes: UIView!
    
    @IBOutlet weak var heightViewNotes: NSLayoutConstraint!
    
    @IBOutlet weak var bottomSpaceViewNotes: NSLayoutConstraint!
    
    @IBOutlet weak var lblDoNotNotifyBuyer: NKLabel!
    
    @IBOutlet weak var imgCheckBoxNotifyBuyer: UIImageView!
    
    @IBOutlet weak var heightEnableAppointment: NSLayoutConstraint!
    
    @IBOutlet weak var viewAppointment: UIView!
    
    @IBOutlet weak var lblSelectAppointment: NKLabel!
    
    @IBOutlet weak var viewAppointmentMain: RCView!
    
    @IBOutlet weak var viewTermsAndConditions: UIView!
    
    @IBOutlet weak var heightViewTermsAndConditions: NSLayoutConstraint!
    
    @IBOutlet weak var imgCheckBoxTNC: UIImageView!
    
    @IBOutlet weak var lblTnCNote: NKLabel!
    
    @IBOutlet weak var stackViewBtnBottom: UIStackView!
    
    @IBOutlet weak var btnPayNow: NKButton!
    
    @IBOutlet weak var btnPayLater: NKButton!
    
    var isReferralMember:Int = 0
    
    var attachments:[Any] = []
    
    var bookDate:String?
    
    var noOfPeople:Int?
    
    var slotList:[Any] = []
    
    var rsCodeArr = [Int]()
    var isEnableAppointment:Int = 0
    
    let linkAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 8),
        .foregroundColor: UIColor.btnLinkColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var paymentDetails:PaymentModel?
    var billingModel:billingUserModel?
    var paymentController = InitialViewController()
    
    var enablePayment:Int = 0
    
    private var razorpay:Razorpay?
    
    var isRegister:Int = 0
    
    var isReferNow:Int = 0
    
    var order_id = ""
    
    var paymentType:EventPaymentTypeModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stackViewBtnBottom.arrangedSubviews[0].isHidden = true
        stackViewBtnBottom.arrangedSubviews[1].isHidden = false
        btnPayLater.setTitle("Submit".localized(), for: .normal)
        
        
            //  heightEnableAppointment.constant = 10.0  TEMP
                heightEnableAppointment.constant = 10
                viewAppointmentMain.alpha = 0
        
                if isEnableAppointment == 1 {
        
                    heightEnableAppointment.constant = 60
                   //  heightEnableAppointment.constant = 60   TEMP
                     viewAppointment.alpha = 1
                     viewAppointmentMain.alpha = 1
                }
        
        viewSchecduledAppointmentIsHidden = true
     //   heightEnableAppointment.constant = 60
        
        updateUserType(type: userType ?? 0)
        
        txtViewGeneralNotes.placeholder = "Notes to Company".localized()
        
        lblSelectAppointment.attributedText = NSAttributedString(string:"Select appointment time".localized(), attributes: linkAttributes)
        
        heightViewReferralMsg.constant = 0
        
        if referralMsg?.count ?? 0 > 0 {
            
            lblReferralMsg.text = referralMsg ?? ""
            
            guard let labelText =  lblReferralMsg.text else { return }
            let height = estimatedHeightOfLabel(text: labelText)
            
            heightViewReferralMsg.constant = height + 10
            
        }
        
        imgViewAttachment.setImageColor(color: .white)
        
        if let userModel = getUserDetail() {
            
            isReferralMember = userModel.isReferralMember ?? 0
            
        }
        
        txtCountryCode.attributedPlaceholder = addPlaceHolderWithText(placeholder: "CC".localized())
        txtMobileNo.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Mobile Number".localized())
        
        txtFirstName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "First Name".localized())
        //        txtLastName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Last Name".localized())
        //        txtCompany.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Company Name".localized())
        
        txtLastName.placeholder = "Last Name".localized()
        txtCompany.placeholder = "Company Name".localized()
        
        
        referralCodeUI()
        
        navigationItem.title = "iCanRefer".localized()
        
        lblKeepAsAnonymous.text = "Do not show my name to Company (Seller)".localized()
        
        lblDoNotNotifyBuyer.text = "Do not notify Buyer".localized()
        
        viewBuyerInfo.alpha = 0
        
        viewBuyerInfoTitle.alpha = 0
        
        viewBottom.alpha = 0
        
        txtCountryCode.delegate = self
        
        viewNotes.alpha = 0
        
        heightViewNotes.constant = 0
        
        bottomSpaceViewNotes.constant = 0
        
        lblTopReferralText.text = "You are referring".localized()
        
        if productType == 1{
            
            lblTopReferralText.text = "You are applying for".localized()
            
            if isReferralMember == 1 {
                lblTopReferralText.text = "You are referring".localized()
            }
            viewNotes.alpha = 1
            heightViewNotes.constant = 42
            bottomSpaceViewNotes.constant = 8
            
            lblNotes.text = "Note: After this submission, an email will be sent to Jobseeker to upload CV".localized()
            
        }
        
        
        
        // viewBuyerName.alpha = 0
        
        //  topSpaceViewSugesstion.constant = 230
        
        txtMobileNo.delegate = self
        
        if let currentCountry =  getLocalCountryCode(){
            
            selectedCountryCode = currentCountry
            
            txtCountryCode.text = currentCountry.dial_code
            
        }
        
        heightOfBottomViewConstraint.constant = 180
        heightViewTermsAndConditions.constant = 0
        viewTermsAndConditions.alpha = 0
        
        if productType == 1{
            
            heightOfBottomViewConstraint.constant = 240
            heightViewTermsAndConditions.constant = 0
            viewTermsAndConditions.alpha = 0
            
        }

                if productType == 2 {
                    
        //            stackViewBtnBottom.arrangedSubviews[0].isHidden = false
        //            stackViewBtnBottom.arrangedSubviews[1].isHidden = false

        //
        //            btnPayNow.setTitle("Pay now".localized(), for: .normal)
        //            btnPayLater.setTitle("Pay later".localized(), for: .normal)
                
                    lblTnCNote.alpha = 0
                    
                    if isRegister == 1 {
                                  
                      lblTnCNote.text = "Note: USD 150 will be charges extra on Pay later".localized()
                        
                      // lblTnCNote.alpha = 1
                        
                   }
                   
                    if productType == 2{
                              
                    heightOfBottomViewConstraint.constant = 240
                    heightViewTermsAndConditions.constant = 60
                    viewTermsAndConditions.alpha = 1
                    
                    if paymentType?.isFreeOfCost == 1 {
                              
                      stackViewBtnBottom.arrangedSubviews[1].isHidden = false
                      btnPayLater.setTitle("Submit".localized(), for: .normal)
                      heightOfBottomViewConstraint.constant = 270
                      heightViewTermsAndConditions.constant = 30
                        if isRegister == 1 {
                        lblTnCNote.alpha = 0
                            
                        }
                              
                    }
                    
                    if paymentType?.isPayAtEvent == 1 {
                                     
                     stackViewBtnBottom.arrangedSubviews[1].isHidden = false
                      btnPayLater.setTitle("Pay later".localized(), for: .normal)
                        heightOfBottomViewConstraint.constant = 270
                        heightViewTermsAndConditions.constant = 60
                        if isRegister == 1 {
                         lblTnCNote.alpha = 1
                        }
                     }
                    
                    if paymentType?.isPayonRegistration == 1 {
                                                
                        stackViewBtnBottom.arrangedSubviews[0].isHidden = false
                         btnPayNow.setTitle("Pay now".localized(), for: .normal)
                        heightOfBottomViewConstraint.constant = 270
                        heightViewTermsAndConditions.constant = 60
                        if isRegister == 1 {
                         lblTnCNote.alpha = 1
                        }
                                                
                    }
                }
              }
        
        if isReferralMember == 0
        {
            circularViewMobileNumber.isHidden = true
            
            imgViewSucessImage.isHidden = true
            
            btnSearch.isHidden = true
            
            widthSucessImage.constant = 0
            
            widthBtnSearch.constant = 0
            
            circularViewHeightConstraint.constant = 0
            
            heightViewOffirstView.constant = heightViewOffirstView.constant - 80 + heightViewReferralMsg.constant
            
            txtCountryCode.isHidden = true
            
            txtMobileNo.isHidden = true
            
            viewBuyerInfo.alpha = 1
            viewBottom.alpha = 1
            //  topSpaceViewSugesstion.constant = 10
            // viewBuyerName.alpha = 1
            
            viewBuyerInfoTitle.alpha = 1
            txtFirstName.alpha = 0
            txtLastName.alpha = 0
            txtEmaild.alpha = 0
            txtCompany.alpha = 0
            txtJobTitle.alpha = 0
            heightViewBuyerInfo.constant = 290
            widthSucessImage.constant = 40
            widthBtnSearch.constant = 0
            
            viewBottomBG.alpha = 0
            viewNotifyBuyer.alpha = 0
            
            heightOfBottomView.constant = 0
            viewTermsAndConditions.alpha = 0
            
            viewBuyerCircularHeightConstraint.constant = 40
            
            viewBuyerCircularView.isHidden = false
            
            lbltextBottom.alpha = 0
            
            lbltextBottom.text = ""
            
            heightOfBottomViewConstraint.constant = 70
            heightViewTermsAndConditions.constant = 0
            if productType == 1{
                heightViewTermsAndConditions.constant = 0
                heightOfBottomViewConstraint.constant = 130
                viewTermsAndConditions.alpha = 0
            }
            

                    if productType == 2 {
                        
            //            stackViewBtnBottom.arrangedSubviews[0].isHidden = false
            //            stackViewBtnBottom.arrangedSubviews[1].isHidden = false

            //
            //            btnPayNow.setTitle("Pay now".localized(), for: .normal)
            //            btnPayLater.setTitle("Pay later".localized(), for: .normal)
                    
                        lblTnCNote.alpha = 0
                        
                        if isRegister == 1 {
                                      
                          lblTnCNote.text = "Note: USD 150 will be charges extra on Pay later".localized()
                            
                          // lblTnCNote.alpha = 1
                            
                       }
                       
                        if productType == 2{
                                  
                        heightOfBottomViewConstraint.constant = 190
                        heightViewTermsAndConditions.constant = 60
                        viewTermsAndConditions.alpha = 1
                        
                        if paymentType?.isFreeOfCost == 1 {
                                  
                          stackViewBtnBottom.arrangedSubviews[1].isHidden = false
                          btnPayLater.setTitle("Submit".localized(), for: .normal)
                          heightOfBottomViewConstraint.constant = 190
                          heightViewTermsAndConditions.constant = 30
                            if isRegister == 1 {
                            lblTnCNote.alpha = 0
                                
                            }
                                  
                        }
                        
                        if paymentType?.isPayAtEvent == 1 {
                                         
                         stackViewBtnBottom.arrangedSubviews[1].isHidden = false
                          btnPayLater.setTitle("Pay later".localized(), for: .normal)
                            heightOfBottomViewConstraint.constant = 190
                            heightViewTermsAndConditions.constant = 60
                            if isRegister == 1 {
                             lblTnCNote.alpha = 1
                            }
                         }
                        
                        if paymentType?.isPayonRegistration == 1 {
                                                    
                            stackViewBtnBottom.arrangedSubviews[0].isHidden = false
                             btnPayNow.setTitle("Pay now".localized(), for: .normal)
                            heightOfBottomViewConstraint.constant = 190
                            heightViewTermsAndConditions.constant = 60
                            if isRegister == 1 {
                             lblTnCNote.alpha = 1
                            }
                                                    
                        }
                    }
                  }
            
            userPresent = 1
            if let userDetailsModel:userModel = getUserDetail(){
                
                userModels = [userDetailsModel]
                
                print("userDetails.countuserDetails.countuserDetails.count :::::\(userModels.count)")
                
                updateUI()
            }
            
        }
            
        else
        {
            viewBuyerCircularHeightConstraint.constant = 0
            viewBuyerCircularView.isHidden = true
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        let isUpdated:[String: Int] = ["update": 1]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callLocation"), object: nil, userInfo: isUpdated)
        
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let size = CGSize(width: view.frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: UIFont.fontForType(textType: 8)]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }
    
    func referralCodeUI()
    {
        heightOfReferralBgConstraint.constant = 0
        viewReferralBG.isHidden = true
        tblreferral.isHidden = true
        selectedIndexPath = -1
    }
    
    func referralCodeUIUpdate(height:CGFloat)
    {
        heightOfReferralBgConstraint.constant = height
        viewReferralBG.isHidden = false
        tblreferral.isHidden = false
        
        heightViewBuyerInfo.constant = CGFloat(height + 40 + 370)
        
        //topSpaceViewSugesstion.constant = 10
        
        
        viewBuyerInfo.alpha = 1
        viewBottom.alpha = 1
        // topSpaceViewSugesstion.constant = 230
        // viewBuyerName.alpha = 0////
        viewBuyerInfoTitle.alpha = 1
        txtFirstName.alpha = 1
        txtLastName.alpha = 1
        txtEmaild.alpha = 1
        txtCompany.alpha = 1
        txtJobTitle.alpha = 1
        heightViewBuyerInfo.constant = 620
        widthSucessImage.constant = 0
        widthBtnSearch.constant = 70
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        lblProductTitle.text = productName ?? ""
        lblProvidedBy.text = "\("Provided by ".localized())\(companyName ?? "")"
        
        if productLogo?.count ?? 0 > 0 {
            imgViewProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(productLogo ?? "")")), completed: nil)
        }
        
    }
    
    var viewSchecduledAppointmentIsHidden: Bool = false {
        didSet{
            if isEnableAppointment == 1 {
            if viewSchecduledAppointmentIsHidden{
               heightEnableAppointment.constant = 60
               viewScheduledAppointment.isHidden = true
               heightViewBuyerInfo.constant = heightViewBuyerInfo.constant +  heightEnableAppointment.constant
            }else{
               heightEnableAppointment.constant = 100
               viewScheduledAppointment.isHidden = false
            heightViewBuyerInfo.constant = heightViewBuyerInfo.constant +  heightEnableAppointment.constant
            }
            }
        }
    }
     
     
    func updateUI() {
        
        if userPresent == 1 {
            viewBuyerInfo.alpha = 1
            viewBottom.alpha = 1
            
            var heightOfBuyerInfo:Int = 0
            //  viewBuyerName.alpha = 1
            
            // lblBuyerName.text = userDetails?.displayName ?? ""
            
            if isReferralMember == 0
            {
                if userModels.count == 1
                {
                    selectedIndexPath = 0
                    
                    if let model:userModel = userModels[0] {
                        
                        buyeruserId  = model.userId ?? 0
                        
                    }
                }
                let height = userModels.count
                
                referralCodeUIUpdate(height: CGFloat(height * 62))
                
                tblreferral.reloadData()
                
                viewBuyerInfo.alpha = 1
                viewBottom.alpha = 1
                //   topSpaceViewSugesstion.constant = 230
                // viewBuyerName.alpha = 0////
                viewBuyerInfoTitle.alpha = 1
                txtFirstName.alpha = 1
                txtLastName.alpha = 1
                txtEmaild.alpha = 1
                txtCompany.alpha = 1
                txtJobTitle.alpha = 1
                heightViewBuyerInfo.constant = 570
                widthSucessImage.constant = 0
                widthBtnSearch.constant = 70
                
                heightOfBuyerInfo = 50
                
                updateFields()
            }
            else////////// viviviviviviviviviviviv
            {
                heightOfBuyerInfo = 0
                
                if userDetails.count == 1
                {
                    selectedIndexPath = 0
                    
                    if let model:ReferBuyerUserModel = userDetails[0] {
                        
                        buyeruserId  = model.userId ?? 0
                        
                    }
                }
                let height = userDetails.count
                
                referralCodeUIUpdate(height: CGFloat(height * 62))
                
                tblreferral.reloadData()
                
                viewBuyerInfo.alpha = 1
                viewBottom.alpha = 1
                // topSpaceViewSugesstion.constant = 230
                // viewBuyerName.alpha = 0////
                viewBuyerInfoTitle.alpha = 1
                txtFirstName.alpha = 1
                txtLastName.alpha = 1
                txtEmaild.alpha = 1
                txtCompany.alpha = 1
                txtJobTitle.alpha = 1
                heightViewBuyerInfo.constant = 570
                widthSucessImage.constant = 0
                widthBtnSearch.constant = 70
                
                updateFields()
            }
            
            
            viewBuyerInfo.alpha = 1
            viewBottom.alpha = 1
            // topSpaceViewSugesstion.constant = 230
            // viewBuyerName.alpha = 0////
            viewBuyerInfoTitle.alpha = 1
            txtFirstName.alpha = 1
            txtLastName.alpha = 1
            txtEmaild.alpha = 1
            txtCompany.alpha = 1
            txtJobTitle.alpha = 1
            
            heightViewBuyerInfo.constant = 700 + CGFloat(heightOfBuyerInfo)
            
            widthSucessImage.constant = 40
            widthBtnSearch.constant = 0
            
        }
        else
        {
            viewBuyerInfo.alpha = 1
            viewBottom.alpha = 1
            // topSpaceViewSugesstion.constant = 230
            // viewBuyerName.alpha = 0////
            viewBuyerInfoTitle.alpha = 1
            txtFirstName.alpha = 1
            txtLastName.alpha = 1
            txtEmaild.alpha = 1
            txtCompany.alpha = 1
            txtJobTitle.alpha = 1
            heightViewBuyerInfo.constant = 630
            widthSucessImage.constant = 0
            widthBtnSearch.constant = 70
        }
        
    }
    
    func updateFields(){
        
        if userDetails.count > 0 {
            
            if let model:ReferBuyerUserModel = userDetails.first {
                
                txtFirstName.text = model.firstName ?? ""
                txtLastName.text = model.lastName ?? ""
                txtEmaild.text = model.emailId ?? ""
                txtJobTitle.text = model.jobTitle ?? ""
                txtCompany.text = model.organization ?? ""
                
            }
            
        }
        else{
            
            txtFirstName.text = ""
            txtLastName.text = ""
            txtEmaild.text = ""
            txtJobTitle.text = ""
            txtCompany.text = ""
            viewBuyerMobNo.text = ""
            viewBuyerCC.text = ""
        }
        
        if isReferralMember == 0 {
            
            if userModels.count > 0 {
                
                if let model:userModel = userModels[0]{
                    
                    txtFirstName.text = model.firstName ?? ""
                    txtLastName.text = model.lastName ?? ""
                    txtEmaild.text = model.emailId ?? ""
                    txtJobTitle.text = model.jobTitle ?? ""
                    txtCompany.text = model.organization ?? ""
                    viewBuyerMobNo.text = model.mobileNumber ?? ""
                    viewBuyerCC.text = model.mobileIsd ?? ""
                    
                }
            }
            else{
                
                txtFirstName.text = ""
                txtLastName.text = ""
                txtEmaild.text = ""
                txtJobTitle.text = ""
                txtCompany.text = ""
                viewBuyerMobNo.text = ""
                viewBuyerCC.text = ""
            }
            
        }
        
        
    }
    
    func openCountrySelector() {
        let storyBoard = UIStoryboard.init(name: CountryCodeSB, bundle: nil)
        let countrySelector:CountryCodeSelectViewController = storyBoard.instantiateViewController(withIdentifier: "CountryCodeSelectViewController") as! CountryCodeSelectViewController
        countrySelector.selectedCountryCode = selectedCountryCode
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
    }
    
    func didFinishSelectingCountryCode(selectedCountry: CountryCodeModel) {
        
        selectedCountryCode = selectedCountry
        txtCountryCode.text = selectedCountry.dial_code
    }
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachments.count > sender.tag {
            attachments.remove(at: sender.tag)
            collectionViewAttachment.reloadData()
        }
    }
    
    @IBAction func btnSelectAppointmentAction(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
        if let bookingVC:BookingViewController = storyBoard.instantiateViewController(withIdentifier: "BookingViewController") as? BookingViewController {
            bookingVC.productCode = productCode ?? 0
            bookingVC.sellerCode = sellerCode ?? 0
            bookingVC.delegate = self
            navigationController?.pushViewController(bookingVC, animated: true)
        }
        
    }
    @IBAction func btnAttachmentAction(_ sender: Any) {
        
        // startAnimating()
        isAttachment = true
        
        isDocument = true
        openImagePicker()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            //     weakSelf?.stopAnimating()
            if status {
                print(urlString, imageName)
                var arrayEachImage : [String : String] = [:]
                arrayEachImage["cdnPath"] = urlString
                arrayEachImage["fileName"] = imageName
                weakSelf?.attachments.append(arrayEachImage)
                weakSelf?.collectionViewAttachment.reloadData()
                self.isAttachment = false
                
            }
            
        }
        
    }
    
    @IBAction func btnHideAppointmentScheduleView(_ sender:UIButton){
        viewSchecduledAppointmentIsHidden = true
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if txtCountryCode.text?.count ?? 0 == 0 && txtMobileNo.text?.count == 0 {
            
            txtCountryCode.showError(message: "Required".localized())
            txtMobileNo.showError(message: "Required".localized())
            
            return
        }
            
        else if txtCountryCode.text?.count ?? 0 == 0 {
            
            txtCountryCode.showError(message: "Required".localized())
            
            return
        }
        else if txtMobileNo.text?.count == 0 {
            
            txtMobileNo.showError(message: "Required".localized())
            
            return
        }
        
        if txtMobileNo.text?.count ?? 0 < 5 {
            
            txtMobileNo.showError(message: "Should be at least 5 characters".localized())
            
            return
        }
            
        else  if (selectedCountryCode?.digits ?? 0 > 0 && txtMobileNo.text?.count ?? 0 != selectedCountryCode?.digits ) {
            
            showWarningMessage(message: "Pleaes enter a valid mobile number.".localized())
            
            return
        }
        else
        {
            searchBuyerDetails()
        }
        
        
    }
    
    func updateUserType(type:Int){
        
        if type == 0{
            
            heightCompanyName.constant = 0
            txtCompany.alpha = 0
            topSpaceForCompanyName.constant = 0
        }
        else{
            
            heightCompanyName.constant = 36
            txtCompany.alpha = 1
            topSpaceForCompanyName.constant = 10
        }
        
    }
    
    @IBAction func segmentSellerType(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            
            userType = 0
            
        }
        else
        {
            userType = 1
        }
        
        updateUserType(type: userType ?? 0)
    }
    
    @IBAction func btnCheckBox(_ sender: Any) {
        
        if isSelected == 0 {
            
            imgCheckbox.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
            
            isSelected = 1
            
            rmAnonymous = 1
            
            
        }
        else{
            imgCheckbox.image = UIImage.init(imageLiteralResourceName: "checkBox")
            
            isSelected = 0
            
            rmAnonymous = 0
        }
        
    }
    
    @IBAction func btnNotificationAction(_ sender: Any) {
        
        if hideTransForBuyer == 0 {
            
            imgCheckBoxNotifyBuyer.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
            
            hideTransForBuyer = 1
            
        }
        else{
            
            imgCheckBoxNotifyBuyer.image = UIImage.init(imageLiteralResourceName: "checkBox")
            
            hideTransForBuyer = 0
            
        }
    }
    
    func callSubmit(){
        
        if enablePayment == 1 {
            
            getGcDetails()
            
        }
        else{
            
            submitReferral()
            
        }
        
    }
    
    @IBAction func btnPayNowAction(_ sender:Any){
           
           if isReferralMember == 0
               
           {
               if userType == 1 {
                   if txtCompany.text?.count ?? 0 <= 0 {
                       txtCompany.showError(message: "Required".localized())
                       return
                   }
               }
               getGcDetails()
           }
           else
           {
               
               if userPresent == 1 {
                   
                   if selectedIndexPath >= 0
                   {
                       if userType == 1 {
                           if txtCompany.text?.count ?? 0 <= 0 {
                               txtCompany.showError(message: "Required".localized())
                               return
                           }
                       }
                       
                       getGcDetails()
                   }
                   else
                   {
                       showErrorMessage(message: "Please select a User".localized())
                   }
                   
               }
                   
               else {
                   
                   if txtFirstName.text?.count ?? 0 <= 0 {
                       
                       txtFirstName.showError(message: "Required".localized())
                       
                       return
                   }
                   else if txtLastName.text?.count ?? 0 <= 0 {
                       
                       txtLastName.showError(message: "Required".localized())
                       
                       return
                   }
                   else if userType == 1 {
                       
                       if txtCompany.text?.count ?? 0 <= 0 {
                           
                           txtCompany.showError(message: "Required".localized())
                           return
                       }
                   }
                       
                   else
                   {
                       
                       getGcDetails()
                       
                   }
               }
           }
        
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        
        if isReferralMember == 0
            
        {
            if userType == 1 {
                if txtCompany.text?.count ?? 0 <= 0 {
                    txtCompany.showError(message: "Required".localized())
                    return
                }
            }
             submitReferral()
        }
        else
        {
            
            if userPresent == 1 {
                
                if selectedIndexPath >= 0
                {
                    if userType == 1 {
                        if txtCompany.text?.count ?? 0 <= 0 {
                            txtCompany.showError(message: "Required".localized())
                            return
                        }
                    }
                    
                     submitReferral()
                }
                else
                {
                    showErrorMessage(message: "Please select a User".localized())
                }
                
            }
                
            else {
                
                if txtFirstName.text?.count ?? 0 <= 0 {
                    
                    txtFirstName.showError(message: "Required".localized())
                    
                    return
                }
                else if txtLastName.text?.count ?? 0 <= 0 {
                    
                    txtLastName.showError(message: "Required".localized())
                    
                    return
                }
                else if userType == 1 {
                    
                    if txtCompany.text?.count ?? 0 <= 0 {
                        
                        txtCompany.showError(message: "Required".localized())
                        return
                    }
                }
                    
                else
                {
                    
                     submitReferral()
                    
                }
            }
        }
        
    }
    
    @IBAction func btnTnCAction(_ sender: Any) {
        
        if isTnC == 0 {
            
            imgCheckBoxTNC.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
            
            isTnC = 1
            
        }
        else{
            
            imgCheckBoxTNC.image = UIImage.init(imageLiteralResourceName: "checkBox")
            
            isTnC = 0
            
        }
        
    }
    
    
    func finalPage(message:String?) {
        
        if  let finalPage:ReferralSuccessVC = UIStoryboard.init(name: SearchSB, bundle: nil).instantiateViewController(withIdentifier: "ReferralSuccessVC") as? ReferralSuccessVC{
            finalPage.message = message ?? ""
            navigationController?.present(finalPage, animated: true, completion: nil)
            navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    func openPaymentPage()
    {
        if paymentDetails != nil {
            
            if billingModel?.billing_address?.count ?? 0 > 0{
                
                if paymentDetails?.billingDetails != nil{
                    
                    if (paymentDetails?.currencyTitle?.caseInsensitiveCompare("AED") == ComparisonResult.orderedSame) {
                        
                        openCCAvenuePayment()
                    }
                    if (paymentDetails?.currencyTitle?.caseInsensitiveCompare("INR") == ComparisonResult.orderedSame) {
                        
                        openRazorpay()
                        
                    }
                    else
                    {
                        openPayPal()
                        // self.paymentContext?.presentPaymentOptionsViewController()
                    }
                }
                else{
                    print("billing_address is empty")
                    
                }
            }
        }
    }
    
    func openRazorpay()
    {
        let amount = paymentDetails?.amount ?? 0.0
        let options: [String:Any] = [
            "amount" : amount * 100, //mandatory in paise like:- 1000 paise ==  10 rs
            "currency" : paymentDetails?.currencyTitle ?? "",
            "order_id":paymentDetails?.razorpay_order_id ?? "",
            "receipt": paymentDetails?.order_id ?? "",
            "prefill": [
                "contact": billingModel?.billing_tel ?? "",
                "email": billingModel?.billing_email ?? ""
            ]
        ]
        print("options::\(options)")
        razorpay?.open(options)
    }
    
    func openCCAvenuePayment()
    {
        
        
        //        let storyBoard = UIStoryboard.init(name: "Invoices", bundle: nil)
        //        let sellerPage:CCWebViewController = storyBoard.instantiateViewController(withIdentifier: "CCWebViewController") as! CCWebViewController
        //
        //        sellerPage.paymentDetails = paymentDetails
        //
        //        navigationController?.pushViewController(sellerPage, animated: true)
        
        
        //print("\("PayemetModel::::::::::::::")\(String(describing: paymentDetails?.billingDetails))\(String(describing: paymentDetails))")
        paymentController = InitialViewController.init(orderId: String(paymentDetails?.order_id ?? ""), merchantId: paymentDetails?.merchantId!, accessCode: paymentDetails?.accessCode!, custId: String(paymentDetails?.customerId ?? 0), amount: String(paymentDetails?.amount ?? 0.0), currency: paymentDetails?.currencyTitle!, rsaKeyUrl: paymentDetails?.rsaKeyUrl!, redirectUrl: paymentDetails?.redirectUrl!, cancelUrl: paymentDetails?.cancelUrl!, showAddress: "N", billingName: billingModel?.billing_name!, billingAddress: billingModel?.billing_address!, billingCity: billingModel?.billing_city!, billingState: billingModel?.billing_state!, billingCountry: billingModel?.billing_country!, billingTel: billingModel?.billing_tel!, billingEmail: billingModel?.billing_email!, deliveryName: paymentDetails?.delivery_name!, deliveryAddress: paymentDetails?.delivery_address!, deliveryCity: paymentDetails?.delivery_city!, deliveryState: paymentDetails?.delivery_state!, deliveryCountry: paymentDetails?.delivery_country!, deliveryTel: paymentDetails?.delivery_tel!, promoCode: "", merchant_param1: "Param 1", merchant_param2: "Param 2", merchant_param3: "Param 3", merchant_param4: "Param 4", merchant_param5: "Param 5", useCCPromo: "N")
        
        
        paymentController.delegate = self
        
        present(paymentController, animated: true, completion: nil)
        
    }
    
    func openPayPal() {
        
        guard let url = URL(string: paymentDetails?.paymentUrl ?? "") else { return }
        UIApplication.shared.open(url)
        
    }
    
    
    func searchBuyerDetails(){
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["productCode": productCode ?? 0,
                                   "mobileIsd": selectedCountryCode?.dial_code ?? "",
                                   "mobileNumber": txtMobileNo.text ?? ""
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/checkBuyer?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            print("dataObjdataObjdataObjdataObjdataObjdataObjdataObj")
                            print(dataObj)
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                let apiResponse = try decoder.decode(ReferBuyerUserApiModel.self, from: data)
                                
                                if let apiResponse:[ReferBuyerUserModel] = apiResponse.userDetails{
                                    
                                    weakSelf?.userDetails = apiResponse
                                    
                                }
                                if let apiResponse:Int = apiResponse.userPresent{
                                    
                                    weakSelf?.userPresent = apiResponse
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                        
                    }
                    
                }
                
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    @IBAction func showSelectedResources(){
        let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
               if let bookingVC:BookingViewController = storyBoard.instantiateViewController(withIdentifier: "BookingViewController") as? BookingViewController {
//                   bookingVC.productCode = productCode ?? 0
//                   bookingVC.sellerCode = sellerCode ?? 0
                   bookingVC.delegate = self
                   bookingVC.resourceList = resourceTypeArray
                   bookingVC.showSelectedResources = true
                   navigationController?.pushViewController(bookingVC, animated: true)
               }
        
           }
    
    func getGcDetails() {
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        
        //        if isFirst == true
        //        {
        //            isFirst = false
        //            startAnimating()
        //        }
        //        else
        //        {
        startAnimatingAfterSubmit()
        //  }
        
        let params:NSDictionary = ["sellerCode":sellerCode ?? 0,"productCode":productCode ?? 0, "paymentMethod":1,"mobileNumber":txtMobileNo.text ?? "","mobileIsd":txtCountryCode.text ?? ""]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/gcdetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            DispatchQueue.main.async
                                {
                                    weakSelf?.stopAnimating()
                            }
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(PaymentAPIModel.self, from: data)
                                
                                if let paymentResponse:PaymentModel = jsonResponse.details {
                                    weakSelf?.paymentDetails = paymentResponse
                                    weakSelf?.razorpay = Razorpay.initWithKey(weakSelf?.paymentDetails?.accessCode ?? "", andDelegate: self)
                                    weakSelf?.billingModel = weakSelf?.paymentDetails?.billingDetails
                                    
                                    weakSelf?.openPaymentPage()
                                }
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            //Show error
            DispatchQueue.main.async
                {
                    weakSelf?.stopAnimating()
            }
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    func submitReferral(){
        
        startAnimatingAfterSubmit()
        
        var params:NSDictionary = [:]
        
        if productType != 2 {
            params  = ["productCode": productCode ?? 0,
                       "sellerCode" : sellerCode ?? 0,
                       "actualMessage":referralNoteToSeller ?? "",
                       "referralNoteToSeller":txtViewGeneralNotes.text ?? "",
                       "specialTerms": txtViewNotesToSeller.text ?? "",
                       "rmAnonymous" : rmAnonymous,
                       "buyerUserId" : buyeruserId,///
                "userType" : userType ?? 0,
                "firstName": txtFirstName.text ?? "",
                "lastName": txtLastName.text ?? "",
                "organization" : txtCompany.text ?? "",
                "jobTitle": txtJobTitle.text ?? "",
                "emailId": txtEmaild.text ?? "",
                "mobileIsd":selectedCountryCode?.dial_code ?? "",
                "mobileNumber": txtMobileNo.text ?? "",
                "phoneIsd": txtCountryCode.text ?? "",
                "phoneNumber": txtMobileNo.text ?? "",
                "attachmentList":attachments,
                "hideTransForBuyer":hideTransForBuyer,
                "date": bookDate ?? "",
                "noOfPeople": noOfPeople ?? 0,
                "slotList":slotList,
                // "resourceList":1,
                
            ]
        }
        else{
            params  = ["productCode": productCode ?? 0,
                       "sellerCode" : sellerCode ?? 0,
                       "actualMessage":referralNoteToSeller ?? "",
                       "referralNoteToSeller":txtViewGeneralNotes.text ?? "",
                       "specialTerms": txtViewNotesToSeller.text ?? "",
                       "rmAnonymous" : rmAnonymous,
                       "buyerUserId" : buyeruserId,///
                "userType" : userType ?? 0,
                "firstName": txtFirstName.text ?? "",
                "lastName": txtLastName.text ?? "",
                "organization" : txtCompany.text ?? "",
                "jobTitle": txtJobTitle.text ?? "",
                "emailId": txtEmaild.text ?? "",
                "mobileIsd":selectedCountryCode?.dial_code ?? "",
                "mobileNumber": txtMobileNo.text ?? "",
                "phoneIsd": txtCountryCode.text ?? "",
                "phoneNumber": txtMobileNo.text ?? "",
                "attachmentList":attachments,
                "hideTransForBuyer":hideTransForBuyer,
                "date": bookDate ?? "",
                "noOfPeople": noOfPeople ?? 0,
                "slotList":slotList,
                // "resourceList":1,
                "order_id": order_id,
                "isRegister":isRegister,
                "isRefer":isReferNow
                
            ]
        }
        
        if isReferralMember == 0
        {
            if let userDetails:userModel = getUserDetail(){
                
                userPresent = userDetails.userId
            }
            
            rmAnonymous = 1
        }
        
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/referNow?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            if let messageText:String = dataObj.object(forKey: "successMessage") as? String{
                                
                                let isUpdated:[String: Int] = ["update": 1]
                                
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callSearch"), object: nil, userInfo: isUpdated)
                                weakSelf?.finalPage(message:messageText)
                            }
                            
                            
                            
                            
                            return
                            
                        }
                        
                    }
                    
                }
                if let serverMessage = jsonObj?.message(){
                    message = (serverMessage.count>0) ? serverMessage : message
                }
                weakSelf?.showErrorMessage(message: message)
                
                
            }
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
}


extension SearchReferProductVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if isReferralMember == 0
        {
            return userModels.count > 0 ? userModels.count : 0
        }
        return userDetails.count > 0 ? userDetails.count : 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if isReferralMember == 0
        {
            return userModels.count > 0 ? 62 : 0
        }
        
        return userDetails.count > 0 ? 62 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let tableViewCell:ReferralCodeProductCell = tableView.dequeueReusableCell(withIdentifier: "ReferralCodeProductCell", for: indexPath) as? ReferralCodeProductCell
        {
            return getCellForReferralCode(cell: tableViewCell, indexPath: indexPath)
        }
        
        return UITableViewCell.init()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedIndexPath = indexPath.row
        
        
        if isReferralMember == 0
        {
            if let model:userModel = userModels[indexPath.row] {
                
                
                buyeruserId  = model.userId ?? 0
                
            }
        }
            
        else
        {
            if let model:ReferBuyerUserModel = userDetails[indexPath.row] {
                
                buyeruserId  = model.userId ?? 0
                
            }
        }
        
        
        tblreferral.reloadData()
    }
    
    
    
    func getCellForReferralCode(cell:ReferralCodeProductCell,indexPath: IndexPath) -> ReferralCodeProductCell {
        
        cell.selectionStyle = .none
        
        if indexPath.row == selectedIndexPath {
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
        
        
        if isReferralMember == 0
        {
            
            if let model:userModel = userModels[indexPath.row] {
                
                
                if model.profilePicture?.count ?? 0 > 0{
                    
                    if let url:URL = URL.init(string: "\(StorageUrl)\(model.profilePicture ?? "")"){
                        cell.imgViewReferralConatct.sd_setImage(with: url, completed: nil)
                    }
                }
                
                cell.lblDisplayName.text = model.displayName
                
            }
        }
            
        else
        {
            
            if let model:ReferBuyerUserModel = userDetails[indexPath.row] {
                
                
                if model.profilePicture?.count ?? 0 > 0{
                    
                    if let url:URL = URL.init(string: "\(StorageUrl)\(model.profilePicture ?? "")"){
                        cell.imgViewReferralConatct.sd_setImage(with: url, completed: nil)
                    }
                }
                
                cell.lblDisplayName.text = model.displayName
                
            }
        }
        
        return cell
    }
    
}


class ReferralCodeProductCell: UITableViewCell {
    
    @IBOutlet weak var lblDisplayName: NKLabel!
    
    @IBOutlet weak var imgViewReferralConatct: RCImageView!
    
}


extension SearchReferProductVC:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtCountryCode {
            
            
            openCountrySelector()
            
            return false;
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        referralCodeUI()
        
        ///
        
        btnSearch.isHidden = false
        widthSucessImage.constant = 0
        widthBtnSearch.constant = 70
        
        //
        if let txtField:RCTextField = textField as? RCTextField{
            txtField.showError(message: "")
            
            viewBuyerInfo.alpha = 0
            
            viewBuyerInfoTitle.alpha = 0
            
            viewBottom.alpha = 0
            
            txtFirstName.text = ""
            txtLastName.text = ""
            txtEmaild.text = ""
            txtJobTitle.text = ""
            txtCompany.text = ""
            viewBuyerMobNo.text = ""
            viewBuyerCC.text = ""
            
            // viewBuyerName.alpha = 0
            
            //            topSpaceViewSugesstion.constant = 230
        }
        
        //        if textField ==  txtMobileNo {
        //
        //            let maxLength = 12
        //            let currentString: NSString = textField.text! as NSString
        //            let newString: NSString =
        //                currentString.replacingCharacters(in: range, with: string) as NSString
        //
        //            return newString.length <= maxLength
        //
        //        }
        return true
    }
}

extension SearchReferProductVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView == collectionViewAttachment
            
        {
            return CGSize.init(width: 100, height: 100)
            
        }
        
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if attachments.count > 0 {
            
            return attachments.count
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
            
            return getCellForAttachment(cell: cell, indexPath: indexPath)
            
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewAttachment {
            
            if let fileName = attachments[indexPath.row] as? [String : String] {
                
                if let imagePath = fileName["cdnPath"] {
                    
                    if let filePath = fileName["fileName"] {
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                            let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                            imageViewer.strImg = imagePath
                            navigationController?.pushViewController(imageViewer, animated: true)
                            
                        }
                        else{
                            
                            openWebView(urlString:imagePath, title: filePath, subTitle: "")
                        }
                        
                    }
                }
                
            }
        }
    }
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete .addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        if let fileName = attachments[indexPath.row] as? [String : String]
        {
            if let imagePath = fileName["cdnPath"]
            {
                if let filePath = fileName["fileName"] {
                    
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            cell.imgView.sd_setImage(with: url, completed: nil)
                            
                        }
                        else{
                            cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                        }
                        
                    }
                }
                
            }
            
        }
        return cell
    }
    
}

extension SearchReferProductVC:BookingVCDelegate{
    
    func didBooked(date: String, no: Int, slot: [Any],rsCodeArray:[ResourceTypeModel]) {
        
        bookDate = date
        noOfPeople = no
        slotList = slot
        
        for rsCode in rsCodeArray {
            self.rsCodeArr.append(rsCode.rsCode ?? 0)
        }
        
        resourceTypeArray = rsCodeArray
        
        lblNoOfPeopleScheduledAppointment.text = String(no)
        lblDateScheduledAppointment.text = date
        if rsCodeArray.count > 1{
            btnResourcesTitle = "\(rsCodeArray[0].title ?? "") (\(rsCodeArray.count - 1) More)"
            
        }else{
            btnResourcesTitle = "\(rsCodeArray[0].title ?? "")"
        }
        
        
        let attrs = [ NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: 14), NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        let buttonTitleStr = NSMutableAttributedString(string:btnResourcesTitle, attributes:attrs)
        btnResourcesScheduledAppointment.setAttributedTitle(buttonTitleStr, for: .normal)
        viewSchecduledAppointmentIsHidden = false
    }
}

extension SearchReferProductVC:InitialViewControllerDelegate
{
    func getResponse(_ responseDict_: NSMutableDictionary!) {
        
        let orderNum = "\(responseDict_["order_id"] ?? "")"
        let orderStatus = "\(responseDict_["order_status"] ?? "")"
        let orderTrackId = "\(responseDict_["tracking_id"] ?? "")"
        
        order_id = orderNum
        
        let msg = "Your order # \(orderNum) is \(orderStatus)\n\nPayment Reference Number : \(orderTrackId)\n\nYou may use this number for any future communications.";
        
        let alert = UIAlertController(title: "Payment Status".localized(), message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Done".localized(), style: UIAlertAction.Style.default, handler: nil));
        
        self.present(alert, animated: true, completion: nil);
    }
}

extension SearchReferProductVC:RazorpayPaymentCompletionProtocol
{
    func onPaymentSuccess(_ payment_id: String) {
        
        saveRazorPaymentGatewayResponse(payment_id: payment_id)
        
        let alert = UIAlertController(title: "Paid".localized(), message: "Your payment was successful. The payment ID is \(payment_id)".localized(), preferredStyle: .alert)
        let action = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        
        submitReferral()
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        let alert = UIAlertController(title: "Error".localized(), message: "Your payment failed due to an error.\nCode: \(code)\nDescription: \(str)".localized(), preferredStyle: .alert)
        let action = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func saveRazorPaymentGatewayResponse(payment_id: String)
    {
        
        weak var weakSelf = self
        
        order_id = paymentDetails?.order_id ?? ""
        
        DispatchQueue.main.async {
            
            weakSelf?.startAnimatingAfterSubmit()
        }
        
        let params:NSDictionary = ["order_id":paymentDetails?.order_id ?? "",
                                   "razorpay_order_id" : paymentDetails?.razorpay_order_id ?? "",
                                   "razorpay_payment_id" : payment_id
        ]
        
        print("params::\(params)")
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        let apiClient:APIClient = APIClient()
        
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveGatewayPaymentResponse?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                weakSelf?.stopAnimating()
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    
                    print("response :: \(response)")
                    print("jsonObj:: \(String(describing: jsonObj))")
                    //............
                }
            }
            
            //Show error
            weakSelf?.stopAnimating()
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
}
