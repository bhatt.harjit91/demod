//
//  ReferralSuccessVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 09/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ReferralSuccessVC: UIViewController {

    @IBOutlet weak var lblSuccessMessage: NKLabel!
    
    var message:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblSuccessMessage.text = message ?? ""
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    

    
    @IBAction func btnBackToHome(_ sender: Any) {
        
       self.dismiss(animated: true, completion: nil)
    }
    
}
