//
//  BookingViewController.swift
//  iCanRefer
//
//  Created by TalentMicro on 01/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

protocol BookingVCDelegate: class {
    func didBooked(date:String,no:Int,slot:[Any], rsCodeArray:[ResourceTypeModel])
    
}
class BookingViewController: AppBaseViewController {
    
    @IBOutlet weak var collectionViewSlots: UICollectionView!
    
    @IBOutlet weak var collectionViewDay: UICollectionView!
    
    @IBOutlet weak var collectionViewPeople: UICollectionView!
    
    @IBOutlet weak var collectionViewAppointmentLevel: UICollectionView!
    
    @IBOutlet weak var collectionShowSelectedResources: UICollectionView!
    
    @IBOutlet weak var viewShowSelectedResources: UIView!
    
    @IBOutlet weak var scrollViewMain: UIScrollView!
    
    @IBOutlet weak var lblDay:NKLabel!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var hightConstraintAppointmentLblVw:NSLayoutConstraint!
    @IBOutlet weak var lblAppointmentLevelCount:UILabel!
    
    @IBOutlet weak var lblNoteAppointment:UILabel!
    
    var rsCodeArray = [Int]()
    
    var workingTime:[AppointmentTimeModel]?
    
    var sellerCode:Int = 0
    
    var productCode:Int = 0
    
    var dayList:[String] = []
    
    var timeSlots:Int = 0
    
    var showSelectedResources = false{
        didSet{
            viewShowSelectedResources.frame = self.view.frame
            self.view.addSubview(viewShowSelectedResources)
            collectionShowSelectedResources.delegate = self
            collectionShowSelectedResources.dataSource = self
        }
    }
    
    var selectedIndex:Int = -1
    
    var selectedIndexArray:[Int] = []
    
    var arrayPeople:[Int] = []
    
    var selectedDate:String?
    
    var daySelected:Int = 0
    
    
    
    var slotList:[AppointmentSlotModel]?
    
    var isAvailable:Bool = false
    
    var slotCount:Int = 1
    
    var selectedSlotList : [AppointmentSlotModel] = []
    
    //MARK:for no of people
    var maxNumberOfPeople:Int = 0
    
    var peopleCount = 5
    
    var noOfPeople:Int = 1
    
    var selectedPeople:Int = 0
    
    weak var delegate:BookingVCDelegate?
    
    var resourceList:[ResourceTypeModel]?
     
   // var resourceSet = Set<ResourceTypeModel>()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        scrollViewMain.delegate = self
        btnSubmit.isUserInteractionEnabled = false
        btnSubmit.isEnabled = false
        btnSubmit.colorIndex = 10
        if !showSelectedResources{
            getSlotDayDetails()
        }
        
        
        btnSubmit.setTitle("Submit".localized(), for: .normal)
        
        self.hightConstraintAppointmentLblVw.constant = 0
        
        navigationItem.title = "Select appointment time".localized()
        
    }
    
    func updateNoOfPeople(){
        
        let limit:Int = maxNumberOfPeople
        
        for i in 1...limit {
            arrayPeople.append(i)
        }
        
        collectionViewPeople.reloadData()
        
        selectedDate = dayList.first
        
        getSlots(withDate:selectedDate ?? "",withPeople:noOfPeople)
        
    }
    
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
        var arrayApp : [ResourceTypeModel] = []
        
        for model in resourceList ?? [] {
            if model.isSelected == 1{
                arrayApp.append(model)
            }
        }
        if arrayApp.count < 1{
            showErrorMessage(message: "Please a choice")
            return
        }
        var array : [Any] = []
        for eachVal in selectedSlotList {
            var eachData:[String : Any] = [:]
            eachData["slotId"] = eachVal.slotId
            eachData["slotTime"] = eachVal.slotTime
            array.append(eachData)
        }
        
        delegate?.didBooked(date: selectedDate ?? "", no: noOfPeople, slot: array,rsCodeArray: arrayApp)
        
        navigationController?.popViewController(animated: true)
        
    }
    
    
    func getSlotDayDetails(){
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["productCode": productCode ,
                                   "sellerCode": sellerCode]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/getDayList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            print(dataObj)
                            do {
                                
                                if let apiRespose:[String] = dataObj.object(forKey: "dayList") as? [String]{
                                    
                                    weakSelf?.dayList = apiRespose
                                    
                                }
                                
                                if let apiRespose:Int = dataObj.object(forKey: "timeSlots") as? Int{
                                    
                                    weakSelf?.timeSlots = apiRespose
                                    
                                }
                                
                                if let apiRespose:Int = dataObj.object(forKey: "maxNumberOfPeople") as? Int{
                                    
                                    weakSelf?.maxNumberOfPeople = apiRespose
                                    
                                }
                                
                                weakSelf?.updateNoOfPeople()
                                
                                weakSelf?.collectionViewDay.reloadData()
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                        
                    }
                    
                }
                
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    func getSlots(withDate:String,withPeople:Int){
        
        selectedIndex = -1
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["productCode": productCode ,
                                   "sellerCode": sellerCode,
                                   "date": withDate,
                                   "noOfPeople": withPeople,
                                   
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/getSlots?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            print(dataObj)
                            do {
                                
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                let apiResponse = try decoder.decode(AppointmentSlotApiModel.self, from: data)
                                
                                if let apiResponse:[AppointmentSlotModel] = apiResponse.slotList{
                                    
                                    weakSelf?.slotList = apiResponse
                                    
                                }
                                weakSelf?.hightConstraintAppointmentLblVw.constant = 0
                                UIView.animate(withDuration: 0.5) {
                                    weakSelf?.view.layoutIfNeeded()
                                }
                                weakSelf?.collectionViewSlots.reloadData()
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                        
                    }
                    
                }
                
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    func getResource(withDate:String,withPeople:Int,withSlot:[Any]){
        
        selectedIndex = -1
        
        startAnimatingAfterSubmit()
        
        let params:NSDictionary = ["productCode": productCode ,
                                   "slotList": withSlot,
                                   "date": withDate,
                                   "noOfPeople": withPeople,
                                   
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/getResourceAvailableList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            print(dataObj)
                            do {
                                
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                let apiResponse = try decoder.decode(ResourceTypeApiModel.self, from: data)
                                
                                if let apiResponse:[ResourceTypeModel] = apiResponse.resourceList{
                                    
                                    weakSelf?.resourceList = apiResponse
                                    weakSelf?.hightConstraintAppointmentLblVw.constant = 200
                                    UIView.animate(withDuration: 0.5) {
                                        weakSelf?.view.layoutIfNeeded()
                                        weakSelf?.scrollViewMain.contentOffset = CGPoint(x: 0, y:  (weakSelf?.scrollViewMain.contentSize.height ?? 0) -  (weakSelf?.scrollViewMain.bounds.size.height ?? 0))
                                    }
                                    weakSelf?.lblAppointmentLevelCount.text = "Your choice (\(weakSelf?.resourceList?.count ?? 0) Available)"
                                    
                                }
                                
                               
                                if let apiResponse:String = apiResponse.note{
                                    weakSelf?.lblNoteAppointment.text = apiResponse
                                }
                                
                                
                                
                                weakSelf?.collectionViewAppointmentLevel.reloadData()
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                        
                    }
                    
                }
                
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    //MARK: people count buttons
    @IBAction func peopleCountPlus(_sender:UIButton){
        if peopleCount < maxNumberOfPeople {
            peopleCount =  (peopleCount) + 1
            selectedPeople = peopleCount
            collectionViewPeople.reloadData()
        }
        
        //MARK:TESTING //scrollViewMain.contentOffset = CGPoint(x: 0, y: scrollViewMain.contentSize.height - scrollViewMain.bounds.size.height)
    }
    
    @IBAction func peopleCountMinus(_sender:UIButton){
        if peopleCount > 0{
            peopleCount =  (peopleCount) - 1
            selectedPeople = peopleCount - 1
            collectionViewPeople.reloadData()
        }
    }
    
    @IBAction func btnDots(_ sender:UIButton){
        sender.isHidden = true
        selectedPeople = 4
        collectionViewPeople.reloadData()
    }
    
}

extension BookingViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewPeople {
            if indexPath.row > 3 {
                return CGSize.init(width:  collectionView.frame.size.width/2.3 , height: collectionView.frame.size.height-25)
            }
            
            return CGSize.init(width: collectionView.frame.size.width/5 - 10, height: collectionView.frame.size.height-25)
        }
        
        if collectionView == collectionViewAppointmentLevel || collectionView == collectionShowSelectedResources{
            return CGSize.init(width: 100, height: 120)
        }
        //  return CGSize.init(width: 100, height: 60)
        
        return CGSize.init(width: collectionView.frame.size.width/3, height: collectionView.frame.size.height-40)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewDay {
            
            return dayList.count
            
        }
        
        if collectionView == collectionViewSlots {
            
            return slotList?.count ?? 0
            
        }
        
        if collectionView == collectionViewPeople {
            
            return 5
            // return arrayPeople.count
            
        }
        
        if collectionView == collectionViewAppointmentLevel || collectionView == collectionShowSelectedResources{
            return resourceList?.count ?? 0
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewDay {
            
            if let cell:CollectionViewDayCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewDayCell", for: indexPath) as? CollectionViewDayCell {
                
                return getCellForCollectionViewDay(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if collectionView == collectionViewSlots {
            
            if let cell:CollectionViewSlotCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewSlotCell", for: indexPath) as? CollectionViewSlotCell {
                
                return getCellForCollectionViewSlot(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if collectionView == collectionViewPeople {
            if maxNumberOfPeople > 5 &&  indexPath.row == 4{
                
                if let cell:CollVwNoOfPeopleCountChange = collectionView.dequeueReusableCell(withReuseIdentifier: "CollVwNoOfPeopleCountChange", for: indexPath) as? CollVwNoOfPeopleCountChange {
                    return getCellForCollectionViewPeopleChangeCount(cell: cell, indexPath: indexPath)
                }
                
            }
            //             if indexPath.row > 3{
            //                if let cell:CollVwNoOfPeopleCountChange = collectionView.dequeueReusableCell(withReuseIdentifier: "CollVwNoOfPeopleCountChange", for: indexPath) as? CollVwNoOfPeopleCountChange {
            //                        return getCellForCollectionViewPeopleChangeCount(cell: cell, indexPath: indexPath)
            //                }
            //             }
            
            
            if let cell:CollectionViewNoOfPeople = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewNoOfPeople", for: indexPath) as? CollectionViewNoOfPeople {
                
                return getCellForCollectionViewPeople(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if collectionView == collectionViewAppointmentLevel || collectionView == collectionShowSelectedResources{
            if let cell:CollvwAppointmentLevelCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollvwAppointmentLevelCell", for: indexPath) as? CollvwAppointmentLevelCell {
                return getCellForCollectionViewAppointmentLevel(cell: cell, indexPath: indexPath)
            }
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func getCellForCollectionViewDay(cell:CollectionViewDayCell,indexPath:IndexPath) -> CollectionViewDayCell
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE \n dd-MMM-yyyy"
        dateFormatterPrint.timeZone = .current
        
        if let title:String = dayList[indexPath.row]{
            
            if let date = dateFormatterGet.date(from: title ) {
                
                cell.lblDay.text = dateFormatterPrint.string(from: date)
                
            }
            
        }
        
        if daySelected == indexPath.row{
            
            cell.viewBg.backgroundColor = UIColor.getGradientColor(index: 8).first!
            cell.lblDay.textColor = .white
        }
        else{
            cell.viewBg.backgroundColor = UIColor.white
            cell.lblDay.textColor = .black
        }
        
        
        return cell
    }
    
    func getCellForCollectionViewSlot(cell:CollectionViewSlotCell,indexPath:IndexPath) -> CollectionViewSlotCell
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy HH:mm: a"
        dateFormatterPrint.timeZone = .current
        
//        if selectedIndex == -1 {
//
//            btnSubmit.colorIndex = 10
//            btnSubmit.isUserInteractionEnabled = false
//            btnSubmit.isEnabled = false
//        }
        
        
        if let model:AppointmentSlotModel = slotList?[indexPath.row]{
            
            if let date = dateFormatterGet.date(from: model.slotTime ?? "" ) {
                
                cell.lblTime.text = dateFormatterPrint.string(from: date)
                
            }
            
            if model.status == 0{
                
                cell.viewBg.backgroundColor  = UIColor.getGradientColor(index: 5).first
                cell.lblTime.textColor = UIColor.white
                
            }
            
            
            if isAvailable == false {
                
                if model.status == 0 {
                    
                    collectionViewSlots.scrollToItem(at: indexPath, at: .right, animated: true)
                    
                    isAvailable = true
                }
                
            }
            
            if model.status == 1{
                
                cell.viewBg.backgroundColor = UIColor.getGradientColor(index: 8).first!
                
            }
            if(model.status == 2){
                cell.viewBg.backgroundColor = UIColor.getGradientColor(index: 10).first!
            }
            
            if selectedIndexArray.contains(indexPath.row)
            {
//                btnSubmit.colorIndex = 0
//                btnSubmit.isUserInteractionEnabled = true
//                btnSubmit.isEnabled = true
                cell.viewBg.backgroundColor  = UIColor.getGradientColor(index: 3).first
            }
            
        }
        
        
        return cell
    }
    
    func getCellForCollectionViewPeople(cell:CollectionViewNoOfPeople,indexPath:IndexPath) -> CollectionViewNoOfPeople {
        if arrayPeople.count < 1{
            return cell
        }
        
        if let model:String = String(arrayPeople[indexPath.row]){
            cell.lblPeople.text =  model
        }
        
        if selectedPeople == indexPath.row{
            cell.viewBg.backgroundColor = UIColor.getGradientColor(index: 8).first!
            cell.lblPeople.textColor = .white
        } else{
            cell.viewBg.backgroundColor = UIColor.white
            cell.lblPeople.textColor = .black
        }
        
        return cell
    }
    
    func getCellForCollectionViewPeopleChangeCount(cell:CollVwNoOfPeopleCountChange,indexPath:IndexPath) -> CollVwNoOfPeopleCountChange{
        cell.txtPeople.text = String(peopleCount)
        
        if peopleCount < 5{
            cell.btnDots.isHidden = false
            peopleCount = 5
        }else{
            cell.btnDots.isHidden = true
        }
        
        if selectedPeople <= 3{
            cell.btnDots.isHidden = false
        }
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func getCellForCollectionViewAppointmentLevel(cell:CollvwAppointmentLevelCell,indexPath:IndexPath) -> CollvwAppointmentLevelCell {
        
        cell.data = resourceList?[indexPath.row]
        
        if let model:ResourceTypeModel = resourceList?[indexPath.row] {
            
            if model.isSelected == 0{
                  cell.bgView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
                  cell.lblLevelName.textColor = UIColor.black
            }
            else{
                 cell.bgView.backgroundColor = UIColor.getGradientColor(index: 2).first!
                 cell.lblLevelName.textColor = UIColor.white
            }
            
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewPeople && indexPath.row > 3 {
            return
        }else{
            peopleCount = 5
        }
        
        if collectionView == collectionViewAppointmentLevel{
          
            if let model:ResourceTypeModel = resourceList?[indexPath.row] {
             
                if model.isSelected == 0 {
                    model.isSelected = 1
                }
                else{
                    model.isSelected = 0
                }
                
                btnSubmit.colorIndex = 10
                btnSubmit.isUserInteractionEnabled = false
                btnSubmit.isEnabled = false
                
                for model in resourceList ?? [] {
                    if model.isSelected == 1{
                        btnSubmit.colorIndex = 0
                        btnSubmit.isUserInteractionEnabled = true
                        btnSubmit.isEnabled = true
                    }
                }
                
             
                
                collectionViewAppointmentLevel.reloadData()
            
//            let cell = collectionView.cellForItem(at: indexPath) as? CollvwAppointmentLevelCell
//            cell?.bgView.backgroundColor = UIColor.getGradientColor(index: 2).first!
//            cell?.lblLevelName.textColor = UIColor.white
            
//            if rsCodeArray.contains(resourceList?[indexPath.row].rsCode ?? -1){
//                let index = rsCodeArray.index(of: (resourceList?[indexPath.row].rsCode!)!)
//                rsCodeArray.remove(at: index!)
//
//                cell?.bgView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
//                cell?.lblLevelName.textColor = UIColor.black
//            }else{
//                rsCodeArray.append(resourceList?[indexPath.row].rsCode ?? -1)
//
//                cell?.bgView.backgroundColor = UIColor.getGradientColor(index: 2).first!
//                cell?.lblLevelName.textColor = UIColor.white
//            }
                
                
                
          }
            
        }
        
        if collectionView == collectionViewDay {
            
            selectedDate = dayList[indexPath.row]
            
            daySelected = indexPath.row
            
            collectionViewDay.reloadData()
            
            selectedIndexArray.removeAll()
            
            collectionViewSlots.reloadData()
            
            getSlots(withDate:selectedDate ?? "",withPeople:noOfPeople )
            
            
            
        }
        
        if collectionView == collectionViewPeople {
            let cell = collectionView.cellForItem(at: IndexPath(row: 4, section: 0)) as? CollVwNoOfPeopleCountChange
            if  cell?.btnDots.isHidden == false{
                noOfPeople = arrayPeople[indexPath.row]
            }else{
                noOfPeople = peopleCount
            }
            
            selectedPeople = indexPath.row
            
            collectionViewPeople.reloadData()
            
            selectedIndexArray.removeAll()
            
            collectionViewSlots.reloadData()
            
            getSlots(withDate:selectedDate ?? "",withPeople:noOfPeople )
        }
        
        if collectionView == collectionViewSlots {
            
            selectedIndex = indexPath.row
            
            slotCount = timeSlots
            
            var isSelected : Bool = true
            var copySelectedSlotList : [AppointmentSlotModel] = []
            var copySelectedIndexArray : [Int] = []
            
            let availableSlot = ((slotList?.count ?? 0) - (selectedIndex ?? 0 + slotCount))
            
            if (availableSlot) >= slotCount
            {
                if let slots = slotList
                {
                    var slotsLeft = slotCount
                    for (index,val) in slots.enumerated()
                    {
                        if (index >= selectedIndex ?? 0) && (slotsLeft > 0)
                        {
                            slotsLeft = slotsLeft - 1
                            print(index)
                            if val.status == 0
                            {
                                copySelectedSlotList.append(val)
                                
                                copySelectedIndexArray.append(index)
                                
                                isSelected = true
                            }
                            else{
                                isSelected = false
                                showErrorMessage(message: "Try another slot".localized())
                                
                                return
                            }
                        }
                        else if slotsLeft > 0
                        {
                            
                            isSelected = false
                        }
                    }
                }
                else
                {
                    isSelected = false
                }
                
            }
            else
            {
                isSelected = false
            }
            
            if isSelected {
                selectedSlotList = copySelectedSlotList
                selectedIndexArray = copySelectedIndexArray
                
                collectionViewSlots.reloadData()
                
                var array : [Any] = []
                for eachVal in selectedSlotList {
                    var eachData:[String : Any] = [:]
                    eachData["slotId"] = eachVal.slotId
                    eachData["slotTime"] = eachVal.slotTime
                    array.append(eachData)
                }
                
                getResource(withDate: selectedDate ?? "", withPeople: noOfPeople , withSlot: array)
            } else {
                collectionViewSlots.reloadData()
            }
            
        }
        
        
    }
    
}


class CollectionViewDayCell:UICollectionViewCell  {
    
    @IBOutlet weak var lblDay: NKLabel!
    
    @IBOutlet weak var viewBg: RCView!
    
}

class CollectionViewSlotCell:UICollectionViewCell {
    
    @IBOutlet weak var lblTime: NKLabel!
    @IBOutlet weak var viewBg: RCView!
    
}

class CollectionViewNoOfPeople: UICollectionViewCell {
    
    @IBOutlet weak var lblPeople: NKLabel!
    @IBOutlet weak var viewBg: RCView!
    
}

class CollVwNoOfPeopleCountChange: UICollectionViewCell {
    @IBOutlet weak var txtPeople: RCTextField!
    @IBOutlet weak var btnMinus:UIButton!
    @IBOutlet weak var btnPlus:UIButton!
    
    @IBOutlet weak var btnDots:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        self.btnMinus.layer.borderWidth = 0.5
        //        self.btnPlus.layer.borderWidth = 0.5
        //        self.btnMinus.layer.borderColor = UIColor.lightGray.cgColor
        //        self.btnPlus.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        btnMinus.clipsToBounds = true
        btnMinus.layer.cornerRadius =  btnMinus.frame.height / 2
        
        btnPlus.clipsToBounds = true
        btnPlus.layer.cornerRadius =  btnMinus.frame.height / 2
    }
    
}

class CollvwAppointmentLevelCell:UICollectionViewCell{
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblLevelName: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    var data:ResourceTypeModel?{
        didSet{
            imgVw.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(data?.imagePath ?? "")")), completed: nil)
            lblLevelName.text = data?.title
        }
    }
    
}
