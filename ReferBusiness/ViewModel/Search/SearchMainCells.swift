//
//  SearchMainVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 23/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation
import SkeletonView
import SafariServices
import GoogleMaps
//import YoutubePlayer_in_WKWebView
import WebKit

protocol SearchListCellDelegate: class {
    func didSelectedBanner(withModel:SearchMainModel)
}


class SearchListCell: UITableViewCell {
    
    // @IBOutlet weak var imgLogo: RCImageView!
    
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblProductShortDes: NKLabel!
    
    // @IBOutlet weak var lblProductPicsCount: NKLabel!
    
    @IBOutlet weak var lblCompanyName: NKLabel!
    
    @IBOutlet weak var lblPrice: NKLabel!
    
    @IBOutlet weak var lblReferralFee: NKLabel!
    
    @IBOutlet weak var lblProductDes: NKLabel!
    
    @IBOutlet weak var lblLoyaltyBonus: NKLabel!
    
    @IBOutlet weak var lblCharityAmnt: NKLabel!
    
    @IBOutlet weak var lblReferralText: NKLabel!
    
    @IBOutlet weak var lblDistance: NKLabel!
    
    @IBOutlet weak var lblreferNow: NKLabel!
    
    @IBOutlet weak var lblRating: NKLabel!
    
    @IBOutlet weak var stackViewPriceReferral: UIStackView!
    
    @IBOutlet weak var stackViewLoyaltyCharity: UIStackView!
    
    @IBOutlet weak var heightStackViewPrice: NSLayoutConstraint!
    
    @IBOutlet weak var heightStackViewLoyalty: NSLayoutConstraint!
    
    @IBOutlet weak var btnReferNow: UIButton!
    
    @IBOutlet weak var imgViewStar: UIImageView!
    
    @IBOutlet weak var imgViewLocation: UIImageView!
    
    @IBOutlet weak var imgViewReferNow: UIImageView!
    
    @IBOutlet weak var imgViewVerified: UIImageView!
    
    @IBOutlet weak var lblVerified: NKLabel!
    
    //@IBOutlet weak var viewImageCount: UIView!
    
    @IBOutlet weak var viewBgProdDesc: RCView!
    
    @IBOutlet weak var btnLocation: UIButton!
    
    @IBOutlet weak var heightReferalCount: NSLayoutConstraint!
    
    
    @IBOutlet weak var rightSpaceLblCompanyName: NSLayoutConstraint!
    
    
    @IBOutlet weak var topSpaceLblReferral: NSLayoutConstraint!
    
    @IBOutlet weak var btnRating: UIButton!
    
    // @IBOutlet weak var imgViewRating: UIImageView!
    
    @IBOutlet weak var stackViewRatingRefer: UIStackView!
    
    @IBOutlet weak var topSpaceStackViewPrice: NSLayoutConstraint!
    //////
    
    @IBOutlet weak var btnGift: UIButton!
    
    @IBOutlet weak var btnWeb: UIButton!
    
    @IBOutlet weak var btnYoutubeLink: UIButton!
    
    @IBOutlet weak var lblGift: NKLabel!
    
    @IBOutlet weak var lblWeb: NKLabel!
    
    @IBOutlet weak var lblYouTube: NKLabel!
    
    @IBOutlet weak var stackViewGiftWebLink: UIStackView!
    
    @IBOutlet weak var heightOfStackViewGiftWebLink: NSLayoutConstraint!
    
    //  @IBOutlet weak var imgViewJob: UIImageView!
    
    @IBOutlet weak var heightViewCollView: NSLayoutConstraint!
    
    @IBOutlet weak var viewBannerView: UIView!
    
    @IBOutlet weak var collectionViewBanners: UICollectionView!
    
    var arrayBanners:[[String:String]]?{
        didSet{
            collectionViewBanners.reloadData()
        }
    }
    
    var delayTime:Double = 8.0
    
    var x:Int = 0
    
    var timer: Timer?
    
    weak var delegate:SearchListCellDelegate?
    
    var modelmain:SearchMainModel?
    
    override func awakeFromNib() {
        
        collectionViewBanners.delegate = self
        
        collectionViewBanners.dataSource = self
        
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        let count:Int =  arrayBanners?.count ?? 0
        
        x = (x + 1) % count
        let indexPath = IndexPath(item: x, section: 0)
        
        collectionViewBanners.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        
    }
    
    
    func startTimer() {
        
        timer =  Timer.scheduledTimer(timeInterval: delayTime, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        
        
    }
    
}

class SearchListImageCell: UITableViewCell {
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblProductTagLine: NKLabel!
    
    @IBOutlet weak var lblEventTime: NKLabel!
    
    
    @IBOutlet weak var lblProductDes: NKLabel!
    
    @IBOutlet weak var lblDistance: NKLabel!
    
    
    @IBOutlet weak var lblRating: NKLabel!
    
    @IBOutlet weak var imgViewLocation: UIImageView!
    
    @IBOutlet weak var btnLocation: UIButton!
    
    @IBOutlet weak var btnRating: UIButton!
    
    @IBOutlet weak var imgViewRating: UIImageView!
    
    @IBOutlet weak var stackViewRatingLike: UIStackView!
    
    @IBOutlet weak var heightStackViewRatingLike: NSLayoutConstraint!
    //////
    
    
    @IBOutlet weak var stackViewReferRegister: UIStackView!
    
    @IBOutlet weak var heightOfStackViewReferRegister: NSLayoutConstraint!
    
    
    @IBOutlet weak var heightViewCollView: NSLayoutConstraint!
    
    @IBOutlet weak var collectionViewBanners: UICollectionView!
    
    
    @IBOutlet weak var btnRegister: UIButton!
    
    @IBOutlet weak var btnRefer: UIButton!
    
    @IBOutlet weak var imgViewLike: UIImageView!
    
    @IBOutlet weak var lblLike: NKLabel!
    
    
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var imgViewRegister: UIImageView!
    
    @IBOutlet weak var imgViewRefer: UIImageView!
    
    var arrayBanners:[[String:String]]?{
           didSet{
               collectionViewBanners.reloadData()
           }
       }
       
       var delayTime:Double = 8.0
       
       var x:Int = 0
       
       var timer: Timer?
       
       weak var delegate:SearchListCellDelegate?
       
       var modelmain:SearchMainModel?
    
    override func awakeFromNib() {
           
           collectionViewBanners.delegate = self
           
           collectionViewBanners.dataSource = self
           
       }
       
       @objc func scrollAutomatically(_ timer1: Timer) {
           
           let count:Int =  arrayBanners?.count ?? 0
           
           x = (x + 1) % count
           let indexPath = IndexPath(item: x, section: 0)
           
           collectionViewBanners.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
           
           
       }
       
       
       func startTimer() {
           
           timer =  Timer.scheduledTimer(timeInterval: delayTime, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
           
           
       }
    
}

class SearchListVideoCell: UITableViewCell {
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblProductTagLine: NKLabel!
    
    @IBOutlet weak var lblEventTime: NKLabel!
    
    
    @IBOutlet weak var lblProductDes: NKLabel!
    
    @IBOutlet weak var playerView: UIView!
    
    @IBOutlet weak var lblDistance: NKLabel!
    
    @IBOutlet weak var lblRating: NKLabel!
    
    @IBOutlet weak var imgViewLocation: UIImageView!
    
    @IBOutlet weak var btnLocation: UIButton!
    
    @IBOutlet weak var btnRating: UIButton!
    
    @IBOutlet weak var imgViewRating: UIImageView!
    
    @IBOutlet weak var stackViewRatingLike: UIStackView!
    
    @IBOutlet weak var heightStackViewRatingLike: NSLayoutConstraint!
   
    @IBOutlet weak var stackViewReferRegister: UIStackView!
    
    @IBOutlet weak var heightOfStackViewReferRegister: NSLayoutConstraint!
    
    @IBOutlet weak var btnRegister: UIButton!
    
    @IBOutlet weak var btnRefer: UIButton!
    
    @IBOutlet weak var imgViewLike: UIImageView!
    
    @IBOutlet weak var lblLike: NKLabel!
    
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var heightPlayerView: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewRegister: UIImageView!
    
    @IBOutlet weak var imgViewRefer: UIImageView!
    
}


class SearchListJobCell: UITableViewCell {
      
    @IBOutlet weak var lblCompanyName: NKLabel!

    @IBOutlet weak var imgProduct: UIImageView!
       
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var imgJob:UIImageView!
       
    @IBOutlet weak var lblProductShortDes: NKLabel!
    
    @IBOutlet weak var lblExp: NKLabel!
    
   // @IBOutlet weak var lblReferralText: NKLabel!
       
    @IBOutlet weak var lblDistance: NKLabel!
    
    @IBOutlet weak var imgJobLoc:UIImageView!
    
    @IBOutlet weak var btnWeb: UIButton!
    
    @IBOutlet weak var btnYoutubeLink: UIButton!
    
    @IBOutlet weak var imgViewYoutube: UIImageView!
    
    @IBOutlet weak var lblWeb: NKLabel!
    
    @IBOutlet weak var lblYouTube: NKLabel!
    
    @IBOutlet weak var stackViewGiftWebLink: UIStackView!
    
    @IBOutlet weak var heightOfStackViewGiftWebLink: NSLayoutConstraint!
    
    @IBOutlet weak var btnReferNow: UIButton!
    
    @IBOutlet weak var lblReferNow:NKLabel!
       
    @IBOutlet weak var imgViewStar: UIImageView!
       
    @IBOutlet weak var imgViewLocation: UIImageView!
       
    @IBOutlet weak var imgViewReferNow: UIImageView!
    
//    @IBOutlet weak var btnRating: UIButton!
//
//    @IBOutlet weak var lblRating: NKLabel!
    
    @IBOutlet weak var btnLoc: UIButton!
    
    @IBOutlet weak var lblBranch:NKLabel!
    
    @IBOutlet weak var stackViewLoactionRefer: UIStackView!
       
    @IBOutlet weak var heightStackViewLoactionRefer: NSLayoutConstraint!
    
}

extension SearchListCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: collectionViewBanners.frame.size.width, height: 200)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    //    func numberOfSections(in collectionView: UICollectionView) -> Int {
    //
    //        collectionViewBanners.collectionViewLayout.invalidateLayout()
    //
    //        return 1
    //    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if arrayBanners?.count ?? 0 > 0 {
            
            return arrayBanners?.count ?? 0
        }
        
        return 0
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell:SearchMainHeaderCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchMainHeaderCell", for: indexPath) as? SearchMainHeaderCell {
            
            return getCellForSearcMainHeader(cell: cell, indexPath: indexPath)
            
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let model:SearchMainModel = modelmain {
            
            delegate?.didSelectedBanner(withModel: model)
            
        }
        
    }
    
    func getCellForSearcMainHeader(cell:SearchMainHeaderCell,indexPath:IndexPath) -> SearchMainHeaderCell {
        
        if arrayBanners?.count ?? 0 > 0 {
            
            if let fileName:[String:String] = arrayBanners?[indexPath.row]
            {
                if let imagePath = fileName["cdnPath"]
                {
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        
                        cell.imgViewHeader.sd_setImage(with: url, completed: nil)
                        
                    }
                    
                }
                
            }
        }
        return cell
    }
}


extension SearchListImageCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: collectionViewBanners.frame.size.width, height: 200)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    //    func numberOfSections(in collectionView: UICollectionView) -> Int {
    //
    //        collectionViewBanners.collectionViewLayout.invalidateLayout()
    //
    //        return 1
    //    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if arrayBanners?.count ?? 0 > 0 {
            
            return arrayBanners?.count ?? 0
        }
        
        return 0
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell:SearchMainHeaderCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchMainHeaderCell", for: indexPath) as? SearchMainHeaderCell {
            
            return getCellForSearcMainHeader(cell: cell, indexPath: indexPath)
            
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let model:SearchMainModel = modelmain {
            
            delegate?.didSelectedBanner(withModel: model)
            
        }
        
    }
    
    func getCellForSearcMainHeader(cell:SearchMainHeaderCell,indexPath:IndexPath) -> SearchMainHeaderCell {
        
        if arrayBanners?.count ?? 0 > 0 {
            
            if let fileName:[String:String] = arrayBanners?[indexPath.row]
            {
                if let imagePath = fileName["cdnPath"]
                {
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        
                        cell.imgViewHeader.sd_setImage(with: url, completed: nil)
                        
                    }
                    
                }
                
            }
        }
        return cell
    }
}


enum Network: String {
    case wifi = "en0"
    case cellular = "pdp_ip0"
    case ipv4 = "ipv4"
    case ipv6 = "ipv6"
}

class SearchMainHeaderCell:UICollectionViewCell{
    
    @IBOutlet weak var imgViewHeader: UIImageView!
    
}
class TableViewCellForHeader:UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var lblHeaderTitle:UILabel!
    @IBOutlet weak var lblFooterTitle:UILabel!
    @IBOutlet weak var collVwHeader:UICollectionView!
    
    var categoryList:[ModelCategorySearchList]?
    var headerFooterTitle = ("",""){
        didSet{
            lblHeaderTitle.text = headerFooterTitle.0
            lblFooterTitle.text = headerFooterTitle.1
            collVwHeader.reloadData()
        }
    }
  
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCellForHeaderView", for: indexPath) as? CollectionViewCellForHeaderView
        cell?.categoryListObj = categoryList?[indexPath.row]
        cell?.btDidSelect.tag = indexPath.row
        cell?.layoutIfNeeded()
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.height - 4 , height:  collectionView.frame.size.height - 4)
    }
}

class CollectionViewCellForHeaderView:UICollectionViewCell{
    @IBOutlet weak var imgVw:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var btDidSelect:UIButton!
    @IBOutlet weak var bgView:UIView!
    
    override func layoutIfNeeded() {
        self.contentView.layer.cornerRadius = 6.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
     
    
    
    var categoryListObj:ModelCategorySearchList?{
        didSet{
              imgVw.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(categoryListObj?.imagePath ?? "")")), completed: nil)
              lblTitle.text = categoryListObj?.title
             
        }
    }
}


