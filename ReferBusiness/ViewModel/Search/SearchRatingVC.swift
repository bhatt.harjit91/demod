//
//  SearchRatingVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 25/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class SearchRatingVC: AppBaseViewController {

    @IBOutlet weak var scrollViewMain: UIScrollView!
    
    @IBOutlet weak var viewRatingSummary: UIView!
    
    @IBOutlet weak var collectionViewSummary: UICollectionView!
    
    @IBOutlet weak var viewRatingHistory: RCView!
    
    @IBOutlet weak var heightViewRatingSummary: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewRatingHistory: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewRatingHistory: UITableView!
    
    @IBOutlet weak var lblRatingCount: NKLabel!
    
    @IBOutlet weak var lblPostReview: NKLabel!
    
    @IBOutlet weak var btnPostReview: UIButton!
    
    var sellerCode:Int = 0
    var productCode:Int = 0
    var transactionId:Int = 0
    
    var avgReviewData:AverageRatingModel?
    var recentReviews:[RatingHistoryModel]?
    var avgReviewDetailedData:[RatingMasterModel]?
    var masterReviewTypes:[RatingMasterModel]?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

       navigationItem.title = "iCanRefer".localized()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        fetchReviewList()
        
    }
    

    func updateViewHeight(){
        
        
        tableViewRatingHistory.reloadData()
        
        collectionViewSummary.reloadData()
        
        heightViewRatingSummary.constant = 100
        
        collectionViewSummary.alpha = 0
        
        if recentReviews?.count ?? 0 > 0 {
            
            DispatchQueue.main.async(execute: {
                //This code will run in the main thread:
                var frame = self.tableViewRatingHistory.frame
                frame.size.height = self.tableViewRatingHistory.contentSize.height
                self.tableViewRatingHistory.frame = frame
                
                self.heightViewRatingHistory.constant = self.tableViewRatingHistory.contentSize.height + 20
                
                 self.heightViewRatingSummary.constant =  self.heightViewRatingHistory.constant + 280
                
            })
         
        collectionViewSummary.alpha = 1
            
        }
        
        lblPostReview.text = "Post a Review".localized()
        
        lblRatingCount.text = "\(recentReviews?.count ?? 0)\(" ")\("Reviews".localized())"
        
        lblRatingCount.textColor = .greenSea
        
    }
    
    @IBAction func btnPostReview(_ sender: UIButton) {
            
            let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
            let postReviews:PostReviewsVC = storyBoard.instantiateViewController(withIdentifier: "PostReviewsVC") as! PostReviewsVC
            postReviews.sellerCode = sellerCode
            postReviews.productCode = productCode
            
            navigationController?.pushViewController(postReviews, animated: true)
        
    }
    
    @objc func editReviewAction(_ sender: UIButton){
        
        if let model:RatingHistoryModel = recentReviews?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
            let postReviews:PostReviewsVC = storyBoard.instantiateViewController(withIdentifier: "PostReviewsVC") as! PostReviewsVC
            postReviews.sellerCode = model.sellercode ?? 0
            postReviews.productCode = model.productCode ?? 0
            
            navigationController?.pushViewController(postReviews, animated: true)
            
        }
        
    }
    
    func fetchReviewList() {
        
        startAnimating()
        
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getProductReviewDetails?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&sellerCode=\(sellerCode)&productCode=\(productCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                
                                print(dataObj)
                                let apiResponse = try decoder.decode(SearchRatingApiModel.self, from: data)
                                
                                if let apiResponse:AverageRatingModel = apiResponse.avgReviewData {
                                    weakSelf?.avgReviewData = apiResponse
                                    
                                }
                                
                                if let apiResponse:[RatingMasterModel] = apiResponse.avgReviewDetailedData {
                                    weakSelf?.avgReviewDetailedData = apiResponse
                                    
                                }
                                if let apiResponse:[RatingHistoryModel] = apiResponse.recentReviews {
                                    weakSelf?.recentReviews = apiResponse
                                    
                                }
                                if let apiResponse:[RatingMasterModel] = apiResponse.masterReviewTypes {
                                    weakSelf?.masterReviewTypes = apiResponse
                                    
                                }
                                
                                weakSelf?.updateViewHeight()
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    

}

extension SearchRatingVC:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
        return recentReviews?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        if let cell:RatingHistoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RatingHistoryTableViewCell", for: indexPath) as? RatingHistoryTableViewCell{
            cell.selectionStyle = .none
            return getCellForRatingHistory(cell: cell, indexPath: indexPath)
        }
        
        
        return UITableViewCell.init()
    }
    
    func getCellForRatingHistory(cell:RatingHistoryTableViewCell, indexPath:IndexPath) -> RatingHistoryTableViewCell {
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
        dateFormatterPrint.timeZone = .current
        
        if let model:RatingHistoryModel = recentReviews?[indexPath.row] {
        
         if let date = dateFormatterGet.date(from: model.createdDate ?? "") {
            
            cell.lblTitle.text = "\("Reviewed on ".localized())\(dateFormatterPrint.string(from: date))"
            
         }
         
         cell.btnEdit.tag = indexPath.row
            
        cell.btnEdit.addTarget(self, action: #selector(editReviewAction(_:)), for: .touchUpInside)
        
         cell.lblDes.text = model.comments ?? ""
            
         cell.btnEdit.alpha = 0
            
            if let userId:Int = getCurrentUserID() {
                
                if userId == model.userId {
                    
                    cell.btnEdit.alpha = 1
                }
                
            }
            
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      
    }
    
}
extension SearchRatingVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return avgReviewDetailedData?.count ?? 0
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : RatingSummaryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RatingSummaryCollectionViewCell", for: indexPath) as! RatingSummaryCollectionViewCell
        
        return getCellForReviewSummary(cell: cell, indexPath: indexPath)
        
    }
    
    func getCellForReviewSummary(cell : RatingSummaryCollectionViewCell, indexPath : IndexPath) -> RatingSummaryCollectionViewCell
    {
        if let modelMain = avgReviewDetailedData?[indexPath.item]
        {
            cell.lblReview.text = modelMain.ratingName
            cell.lblReviewValue.text = String(format: "%.2f" ,modelMain.rating ?? 0.0 )
            cell.progressReviewBar.progress = Float(modelMain.rating ?? 0.0) / 10.0
            if modelMain.rating ?? 0.0 <= 3.3
            {
                cell.progressReviewBar.progressTintColor = .red
            }
            if modelMain.rating ?? 0.0 > 3.3 && modelMain.rating ?? 0.0 <= 6.7 {
                cell.progressReviewBar.progressTintColor = .yellow
            }
            if modelMain.rating ?? 0.0 > 6.7 {
                cell.progressReviewBar.progressTintColor = .greenSea
            }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.size.width / 2 - 10, height: 60)
    }
    
    
}


class RatingStarTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var cosmosView: CosmosView!
    
    var ratingValue:Float?
    
    
    
    
}

class PostRatingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
    
    
    
}

class RatingHistoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var lblDes: NKLabel!

    @IBOutlet weak var btnEdit: NKButton!
    
}

class RatingSummaryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var progressReviewBar: UIProgressView!
    
    @IBOutlet weak var lblReview: NKLabel!
    
    @IBOutlet weak var lblReviewValue: NKLabel!
    
    @IBOutlet weak var viewBg: RCView!
    
    
    
    
}

