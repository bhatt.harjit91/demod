
import UIKit
import CoreLocation
import SkeletonView
import SafariServices
import GoogleMaps
import WebKit

//productType : 0 // send -1 if no filter selected
//experience : 3 ,      // if productType is 1
//FromDate : “2020-01-02”,   // if productType is 2
//FromTo : “2020-01-02”,    // if productType is 2
//jobType : [2,3,4]     // if productType is 1

class SearchMainVC: AppBaseViewController,LocationPermissionVCNewDelegate,CLLocationManagerDelegate{
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var Activity:UIActivityIndicatorView!
    
    @IBOutlet weak var searchBarProducts: UISearchBar!
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblNoData: NKLabel!
    
    @IBOutlet weak var lblSellerNotFoundTitle: NKLabel!
    
    @IBOutlet weak var heightViewSellerNotFound: NSLayoutConstraint!
    
    @IBOutlet weak var viewLocationText: UIView!
    
    @IBOutlet weak var lblLocationText: NKLabel!
    
    @IBOutlet weak var heightViewlocation: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewCollView: NSLayoutConstraint!
    
    @IBOutlet weak var viewCollView: UIView!
    
    @IBOutlet weak var collectionViewBanners: UICollectionView!
    
    @IBOutlet weak var viewSearchLocBg: UIView!
    
    @IBOutlet weak var heightViewSearchLogBg: NSLayoutConstraint!
    
    @IBOutlet weak var viewSellerNotFound: UIView!
    
    
    @IBOutlet var categoryButton: [UIButton]!
    @IBOutlet weak var hightCategoryButtonView: NSLayoutConstraint!
    
    var headerFooterTitle = ("","")
      
    var productList:[SearchMainModel]?
    
    var advertiseList:[AdvertisementModel]?
    
    var categoryList:[ModelCategorySearchList]?
    
    var categoryCodeNew = 0
    
    var locationManager = CLLocationManager()
    
    var currentLoc:CLLocationCoordinate2D?
    
    var delayTime:Double = 8.0
    
    var x = 1
    
    var timer : Timer?
    
    var addressIsNil:Bool?
    
    let mainTitleAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.getGradientColor(index: 0).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var isDataLoading:Bool=false
    var pageNo:Int=1
    var limit:Int=10
    var offset:Int=0
    var didEndReached:Bool=false
    
    var count:Int = 0
    
    var tblVwheaderHight = 0.01
    
    var searchText:String = ""
    
    var isReferralMember:Int = 0
    
    var isFirst:Int = 0
     
    var isSelectedNextPage:Int = 0
    
    var branch = Set<Int>()
    
    var product = Set<Int>()
    
    var ipAddress = ""
    
    var maxHeaderHeight: CGFloat = 0//240
    
    let minHeaderHeight: CGFloat = 0
    
    var previousScrollOffset: CGFloat = 0
    
    var previousScrollViewHeight: CGFloat = 0
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 8),
        .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let appDel = AppDelegate()
    
    var isAdvLoaded:Int = 0
    
    private var didTapDeleteKey = false
    
    var isVideoPlayed = true
    
   var arrayFilterJobType:[Int] = []
    
    var selectedExp:Int = -1
    var typeProduct =  -1
    var fromDateEvent = ""
    var fromToEvent = ""
    var jobTypeList:[Int] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        Activity = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        Activity.transform = CGAffineTransform(scaleX: 2, y: 2)
        
        
        
        //        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //                       layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //        layout.itemSize = CGSize(width: view.frame.size.width, height: collectionViewBanners.frame.size.height)
        //        layout.minimumInteritemSpacing = 10
        //        layout.minimumLineSpacing = 10
        //        layout.scrollDirection = .horizontal
        //        collectionViewBanners!.collectionViewLayout = layout
        
        viewSearchLocBg.layer.cornerRadius = 5
        viewSearchLocBg.layer.borderWidth = 1.0
        viewSearchLocBg.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        viewSearchLocBg.layer.shadowColor = UIColor.black.cgColor
        viewSearchLocBg.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewSearchLocBg.layer.shadowOpacity = 0.3
        viewSearchLocBg.layer.shadowRadius = 4.0
        
        categoryButtonSetup()
        
        heightViewlocation.constant = 0
        
        heightViewSearchLogBg.constant = 120
        hightCategoryButtonView.constant = 0
        viewLocationText.alpha = 0
        
        startAnimating()
        
        if !checkLogin(){
            
            openSignInPage()
        }
        
        
        if let userModel = getUserDetail() {
            
            isReferralMember = userModel.isReferralMember ?? 0
            
        }
        
        
        //        if let locationDictionary = UserDefaults.standard.object(forKey: "locationFromSignUp") as? Dictionary<String,NSNumber> {
        //            currentLoc?.latitude = locationDictionary["lat"]!.doubleValue
        //            currentLoc?.longitude = locationDictionary["lon"]!.doubleValue
        //        }
        
        isFirst = 1
        
        let color = UIColor.getGradientColor(index: 0).first!
        
        getCurrentLocation(isUpdateName: 1)
        
        navigationItem.title = "Search".localized()
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.bodyFont
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.bodyFont
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = .white
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.btnLinkColor]
        
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: UIControl.State.normal)
        
        //
        // searchBarProducts.setImage(UIImage(), for: .clear, state: .normal)
        
        lblNoData.text = "We are sorry for not providing you the products or services you are finding. Our Partners are registering thousands of products and services every day. Hope you will find your needs very soon...".localized()
        
        lblNoData.alpha = 0
        
        heightViewSellerNotFound.constant = 0
        lblSellerNotFoundTitle.alpha = 0
        
        viewSellerNotFound.layer.cornerRadius = 5.0
        viewSellerNotFound.layer.masksToBounds = true
        
        
        //        lblSellerNotFoundTitle.attributedText = NSMutableAttributedString(string: "Not found?".localized(),attributes: mainTitleAttributes)
        //        lblSellerNotFoundTitle.textColor = color
        
        lblSellerNotFoundTitle.text = "Not found".localized()
        lblSellerNotFoundTitle.textColor = .primaryColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(callSearchApi(notification:)), name: NSNotification.Name(rawValue: "callSearch"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData(_:)), name: NSNotification.Name("ReloadNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callLocNotification(_:)), name: NSNotification.Name("callLocation"), object: nil)
         
        getIpAddress()
        
         UserDefaults.standard.set(10, forKey: "smartSelected")
        
    }
    
    func updateNavButton(){
        
        let btnName = UIButton()
       // btnName.setTitle("New -->", for: .normal)
        btnName.setImage(UIImage(named: "Home_white"), for: .normal)
        btnName.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        btnName.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(rightBarButtonItem), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        if self.parent != nil{
           self.parent!.navigationItem.rightBarButtonItem = rightBarButton
        }
       // searchBarProducts.text = ""
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if !checkLogin(){
            
            openSignInPage()
            
        }
        
       // self.heightViewCollView.constant = self.maxHeaderHeight
        
      //  self.updateHeader()
        
        //getCurrentLocation(isUpdateName: 1)
        
        UserDefaults.standard.set(1, forKey: "")
        
        if UserDefaults.standard.integer(forKey: "refreshForBecomePartner") == 1
        {
            if let userModel = getUserDetail() {
                
                UserDefaults.standard.removeObject(forKey: "selrefreshForBecomePartnerectedTab")
                UserDefaults.standard.synchronize()
                isReferralMember = userModel.isReferralMember ?? 0
                tableViewList.reloadData()
            }
        }
    }
    
    @objc func rightBarButtonItem(){
         categoryCodeNew = 0
        if tblVwheaderHight == 250{
           tblVwheaderHight = 0.01
            
             UserDefaults.standard.set(0, forKey: "smartSelected")
            
            self.tableViewList.reloadData()
            
            updateNavButton()
           
        }else{
            tblVwheaderHight = 250
            
             UserDefaults.standard.set(1, forKey: "smartSelected")
             
            searchProduct(search: "" , lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage:1,limit: limit, isFirst: 1, withIp: ipAddress)
            
            
            if self.parent != nil {
                    self.parent!.navigationItem.rightBarButtonItem = UIBarButtonItem()
            }
        }
       
    }
    
    func categoryButtonSetup(){
        for btn in categoryButton{
            btn.layer.cornerRadius = 7
            btn.layer.borderColor = UIColor.getGradientColor(index: 0).first!.cgColor
            btn.layer.borderWidth = 1
            btn.setTitleColor(UIColor.getGradientColor(index: 0).first!, for: .normal)
            btn.titleLabel?.font = .primaryButtonFont
            btn.backgroundColor = UIColor.white
            btn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 7)
            
        }
    }
    
    @objc func reloadData(_ notification: Notification?) {
        
        getCurrentLocation(isUpdateName: 1)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0){
            
            let locA = CLLocation(latitude: self.appDel.currentLocAppDelegate?.latitude ?? 0.0, longitude: self.appDel.currentLocAppDelegate?.longitude ?? 0.0)
            
            let locB = CLLocation(latitude: self.currentLoc?.latitude ?? 0.0, longitude: self.currentLoc?.longitude ?? 0.0)
            
            let distance = locA.distance(from: locB)
            
            if distance >= 100 || self.appDel.productListAppDelegate?.count ?? 0 == 0{
                
                self.pageNo = 1
                
                self.searchProduct(search: "", lat:  self.currentLoc?.latitude ?? 0.0, long: self.currentLoc?.longitude ?? 0.0, startPage: self.pageNo,limit: self.limit, isFirst: self.isFirst, withIp: self.ipAddress)
            }
            else{
                
                self.productList = self.appDel.productListAppDelegate ?? []
                self.advertiseList = self.appDel.advertiseListAppDelegate ?? []
                
            }
            
        }
        
    }
    
    @objc func callLocNotification(_ notification: Notification?){
        
        if let isFlag = notification?.userInfo?["update"] as? Int {
            
            if isFlag == 1 {
                
                getCurrentLocation(isUpdateName: 0)
                
            }
            else{
                getCurrentLocation(isUpdateName: 1)
            }
        }
    }
    
    @IBAction func showCategoriesAction(_ sender: UIButton) {
        if hightCategoryButtonView.constant == 0{
            if addressIsNil == true{
                self.heightViewlocation.constant = 0
                self.hightCategoryButtonView.constant = 30
                self.heightViewSearchLogBg.constant =  40 + (self.heightViewlocation.constant )
            }else{
                self.heightViewlocation.constant = 30
                self.hightCategoryButtonView.constant = 30
                self.heightViewSearchLogBg.constant =  60 + (self.heightViewlocation.constant ) + 40
            }
            categoryButtonSetup()
            
        }else{
            if addressIsNil == true{
                self.heightViewlocation.constant = 0
                self.hightCategoryButtonView.constant = 0
                self.heightViewSearchLogBg.constant =  0 + (self.heightViewlocation.constant )
            }else{
                self.heightViewlocation.constant = 30
                self.hightCategoryButtonView.constant = 0
                self.heightViewSearchLogBg.constant =  60 + (self.heightViewlocation.constant ) + 0
            }
            
        }
    }
    
    func buttonSelectionCategory(_ btn:UIButton,_ selected:Bool = false){
        if selected{
                       btn.backgroundColor = .blue
                       btn.layer.cornerRadius = 7
                       btn.setTitleColor(UIColor.white, for: .normal)
                       btn.titleLabel?.font = .primaryButtonFont
                       btn.backgroundColor = UIColor.getGradientColor(index: 0).first
                       
                   }else{
                       btn.backgroundColor = .blue
                       btn.layer.cornerRadius = 7
                       btn.setTitleColor(UIColor.getGradientColor(index: 0).first!, for: .normal)
                       btn.titleLabel?.font = .primaryButtonFont
                       btn.backgroundColor = UIColor.white
                   }
    }
    
    @IBAction func categoryButtonAction(_ sender: UIButton) {
        //MARK:Design
        for btn in categoryButton{
            if btn == sender{
              buttonSelectionCategory(btn,true)
            }else{
              buttonSelectionCategory(btn,false)
            }
        }
         
        //MARK:Navigation
        if sender == categoryButton[0]{
            //MARK: Api call with 0
            if typeProduct == 0{
               typeProduct = -1
               buttonSelectionCategory(categoryButton[0],false)
                
            }else{
               typeProduct = 0
            }
           
            searchProduct(search: "" , lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage:1,limit: limit, isFirst: isFirst, withIp: ipAddress)
            
        }else if sender == categoryButton[1]{
            let jobFilterSB = UIStoryboard(name: SearchSB, bundle: nil)
            if let jobFilterVC:SearchJobFilterVC = jobFilterSB.instantiateViewController(withIdentifier: "SearchJobFilterVC") as? SearchJobFilterVC {
                jobFilterVC.delegateFilter = self
                jobFilterVC.productType = 1
                jobFilterVC.title = "Jobs Filters"
                self.navigationController?.pushViewController(jobFilterVC, animated: true)
            }
            
        }else{
            let jobFilterSB = UIStoryboard(name: SearchSB, bundle: nil)
            if let jobFilterVC:SearchJobFilterVC = jobFilterSB.instantiateViewController(withIdentifier: "SearchJobFilterVC") as? SearchJobFilterVC {
                jobFilterVC.delegateFilter = self
                jobFilterVC.productType = 2
                jobFilterVC.title = "Events Filters"
                self.navigationController?.pushViewController(jobFilterVC, animated: true)
            }
        }
        
        
    }
    
    
    
    @objc func btnVideoAction(_ sender: UIButton){
        
        if isVideoPlayed == true {
            
            isVideoPlayed = false
            
        }
        else{
            
            isVideoPlayed = true
            
        }
        
        let indexPath = IndexPath(item: sender.tag, section: 1)
        
        tableViewList.reloadRows(at: [indexPath], with: .fade)
    }
   
   /*
    func updateAvertisementView(){
        
        if advertiseList?.count ?? 0 > 0 {
            
            if offset > 1 {
                heightViewCollView.constant = 0
                maxHeaderHeight = 0
            }else{
                heightViewCollView.constant = 240
                maxHeaderHeight = 240
                collectionViewBanners.reloadData()
            }
            
            
            
            
            // collectionViewBanners.collectionViewLayout.invalidateLayout()
            
            startTimer()
            
        }
        else{
            
            heightViewCollView.constant = 0
            maxHeaderHeight = 0
            
        }
        
    }
    */
    
    
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "callSearch"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "callLocation"), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.parent != nil {
            self.parent!.navigationItem.rightBarButtonItem = UIBarButtonItem()
            if tblVwheaderHight == 0.01{
               UserDefaults.standard.set(0, forKey: "smartSelected")
            }
             
        }
        stopAnimating()
    }
    
    
    func showLocationRequiredPage(){
        weak var weakSelf = self
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable location access to provide you near by search.".localized(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Enable Location Service".localized(), style: .default, handler: {action in
            
            weakSelf?.openAppSettings()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func getCurrentLocation(isUpdateName:Int) {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch locationAuthorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.startUpdatingLocation()
                currentLoc = locationManager.location?.coordinate
                
                if isUpdateName == 1 {
                    
                    updateLocationName()
                    
                }
            }
        case .restricted, .denied:
            
            // showLocationRequiredPage()
            
            print("Access Denied")
        }
    }
    
    func getAddress(for network: Network) -> String? {
        var address: String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr: UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if name == network.rawValue {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
    
    
     func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
          Activity.stopAnimating()
     }
     func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
           Activity.stopAnimating()
     }
       
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if  let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
            //print("locations = \(locValue.latitude) \(locValue.longitude)")
            
            currentLoc = locValue
            
            updateLocationName()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        stopAnimating()
    }
    
    
    func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        currentLoc = coordinate
        
        // pageNo = 1
        
        updateLocationName()
        
        if productList?.count ?? 0 > 0 {
            
            productList?.removeAll()
            self.tableViewList.reloadData()
            
        }
        
        let locA = CLLocation(latitude: appDel.currentLocAppDelegate?.latitude ?? 0.0, longitude: appDel.currentLocAppDelegate?.longitude ?? 0.0)
        
        let locB = CLLocation(latitude: currentLoc?.latitude ?? 0.0, longitude: currentLoc?.longitude ?? 0.0)
        
        let distance = locA.distance(from: locB)
        
        if distance >= 100 || appDel.productListAppDelegate?.count ?? 0 == 0{
            
            isFirst = 0
            
            pageNo = 1
            
            searchProduct(search: "", lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage: pageNo,limit: limit, isFirst: isFirst, withIp: ipAddress)
            
            
        }
        else{
            productList = appDel.productListAppDelegate ?? []
            advertiseList = appDel.advertiseListAppDelegate ?? []
        }
        
    }
    
    func updateLocationName() {
        let geoCoder = GMSGeocoder()
        
        weak var weakSelf = self
        
        geoCoder.reverseGeocodeCoordinate(currentLoc ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), completionHandler: { response, error in
            
            if response != nil {
                let address = response?.results()?.first
                if address != nil {
                    weakSelf?.addressIsNil = false
                    weakSelf?.lblLocationText.attributedText =  NSMutableAttributedString(string:  address?.lines?.joined(separator: ",") ?? "",attributes: self.yourAttributes)
                    
                    weakSelf?.heightViewlocation.constant = 30
                    weakSelf?.hightCategoryButtonView.constant = 0
                    weakSelf?.heightViewSearchLogBg.constant =  60 + (weakSelf?.heightViewlocation.constant ?? 0)
                    
                    weakSelf?.viewLocationText.alpha = 1
                    
                    // weakSelf?.updateAddressViewHeight()
                    
                    // weakSelf?.lblAddress.textColor = .black
                }
            }else{
                weakSelf?.addressIsNil = true
            }
            
        })
    }
    
    
    func getIpAddress() {
        
        var ipWifi = ""
        
        var ipCellular = ""
        
        if let ipv = getAddress(for: .cellular) {
            if ipv.count > 0
            {
                ipCellular = ipv
            }
            
        }
        if let ipW = getAddress(for: .wifi) {
            
            if ipW.count > 0
            {
                ipWifi = ipW
            }
        }
        
        if ipCellular.count > 0
        {
            ipAddress = ipCellular
        }
            //        else if ipWifi.count > 0 {
            //
            //            ipAddress = ipWifi
            //        }
            
        else{
            
            ipAddress = ""
        }
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if   UserDefaults.standard.integer(forKey: "smartSelected") == 0 {
            
             updateNavButton()
        }
       
        //  getCurrentLocation()
        
        let locA = CLLocation(latitude: appDel.currentLocAppDelegate?.latitude ?? 0.0, longitude: appDel.currentLocAppDelegate?.longitude ?? 0.0)
        
        let locB = CLLocation(latitude: currentLoc?.latitude ?? 0.0, longitude: currentLoc?.longitude ?? 0.0)
        
        let distance = locA.distance(from: locB)
        
        if distance >= 100 || appDel.productListAppDelegate?.count ?? 0 == 0{
            
            if UserDefaults.standard.integer(forKey: "selectedTab") == 1
            {
                isFirst = 1
                UserDefaults.standard.set(0, forKey: "selectedTab")
                
                pageNo = 1
                
                searchProduct(search: "", lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage: pageNo,limit: limit, isFirst: isFirst, withIp: ipAddress)
            }
            
            
            
        }
        else{
            
            productList = appDel.productListAppDelegate ?? []
            advertiseList = appDel.advertiseListAppDelegate ?? []
        }
        
    }
    
    
    
    @objc func btnReferProductAction(_ sender: UIButton){
        
        if let model:SearchMainModel = productList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
            let referProductPage:SearchReferProductVC = storyBoard.instantiateViewController(withIdentifier: "SearchReferProductVC") as! SearchReferProductVC
            referProductPage.productCode = model.productCode ?? 0
            referProductPage.productName = model.productName ?? ""
            referProductPage.companyName = model.companyName ?? ""
            referProductPage.productLogo = model.productLogo ?? ""
            referProductPage.sellerCode = model.sellerCode ?? 0
            referProductPage.productType = model.productType ?? 0
            referProductPage.referralNoteToSeller = model.notesForReferralMember ?? ""
            referProductPage.isEnableAppointment = model.enableAppointment ?? 0
            referProductPage.enablePayment = model.enablePayment ?? 0
            referProductPage.isReferNow = 1
            referProductPage.paymentType = model.paymentType
            referProductPage.productType = model.productType ?? 0
            cancelSearch()
            
            if isReferralMember == 1 {
                
                referProductPage.referralMsg = model.referralFormMsg ?? ""
                
            }
            else{
                referProductPage.referralMsg = model.enquiryOrApplyJobFormMsg ?? ""
            }
            navigationController?.pushViewController(referProductPage, animated: true)
            
        }
    }
    
    @objc func btnRegisterProductAction(_ sender: UIButton){
        
        if let model:SearchMainModel = productList?[sender.tag] {
            
            let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
            let referProductPage:SearchReferProductVC = storyBoard.instantiateViewController(withIdentifier: "SearchReferProductVC") as! SearchReferProductVC
            referProductPage.productCode = model.productCode ?? 0
            referProductPage.productName = model.productName ?? ""
            referProductPage.companyName = model.companyName ?? ""
            referProductPage.productLogo = model.productLogo ?? ""
            referProductPage.sellerCode = model.sellerCode ?? 0
            referProductPage.productType = model.productType ?? 0
            referProductPage.referralNoteToSeller = model.notesForReferralMember ?? ""
            referProductPage.isEnableAppointment = model.enableAppointment ?? 0
            referProductPage.enablePayment = model.enablePayment ?? 0
            referProductPage.isRegister = 1
            referProductPage.paymentType = model.paymentType
            referProductPage.productType = model.productType ?? 0
            cancelSearch()
            
            if isReferralMember == 1 {
                
                referProductPage.referralMsg = model.referralFormMsg ?? ""
                
            }
            else{
                referProductPage.referralMsg = model.enquiryOrApplyJobFormMsg ?? ""
            }
            navigationController?.pushViewController(referProductPage, animated: true)
            
        }
    }
    
    @objc func btnLikeAction(_ sender: UIButton){
          
          if checkLogin(){
        
          if let model:SearchMainModel = productList?[sender.tag] {
            
            if model.isLike == 0 {
                model.isLike = 1
                model.likeCount = model.likeCount ?? 0 + 1
            }
            else{
                model.isLike = 0
                model.likeCount = model.likeCount ?? 0 - 1
            }
              
            getLikes(productCode: model.productCode ?? 0, isLike: model.isLike ?? 0, index: sender.tag)
              
          }
        }
          else{
              
              openSignInPage()
          }
          
          
      }
    
    @objc func btnWebAction(sender:UIButton)
    {
        if let model:SearchMainModel = productList?[sender.tag]{
            
            if let strUrl = model.website
            {
                let url: URL?
                if strUrl.hasPrefix("http://") || strUrl.hasPrefix("https://") {
                    url = URL(string: strUrl)
                } else {
                    url = URL(string: "https://" + strUrl)
                }
                if let url = url {
                    
                    let sfViewController = SFSafariViewController(url: url)
                    self.present(sfViewController, animated: true, completion: nil)
                }
            }
            
            cancelSearch()
        }
    }
    
    @objc func btnYouTubeLinkAction(sender:UIButton)
    {
        if let model:SearchMainModel = productList?[sender.tag]{
            
            if let strUrl = model.youtubeLink
            {
                let url: URL?
                if strUrl.hasPrefix("http://") || strUrl.hasPrefix("https://") {
                    url = URL(string: strUrl)
                } else {
                    url = URL(string: "https://" + strUrl)
                }
                if let url = url {
                    
                    let sfViewController = SFSafariViewController(url: url)
                    self.present(sfViewController, animated: true, completion: nil)
                }
            }
            
            cancelSearch()
        }
        
    }
    
    
    @objc func btnSurpriseGiftAction(sender:UIButton)
    {
        
        if let model:SearchMainModel = productList?[sender.tag]{
            
            let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
            
            if let surpriseGiftVC:SearchProductGiftDetailVC = storyBoard.instantiateViewController(withIdentifier: "SearchProductGiftDetailVC") as? SearchProductGiftDetailVC {
                
                surpriseGiftVC.productCode = model.productCode ?? 0
                
                surpriseGiftVC.sellerCode = model.sellerCode ?? 0
                
                self.navigationController?.pushViewController(surpriseGiftVC, animated: true)
                
            }
            
            cancelSearch()
            
        }
    }
    
    @objc func btnLocationAction(_ sender: UIButton){
        
        if let model:SearchMainModel = productList?[sender.tag] {
            
            
            openNaviagationMap(latitude: model.latitude ?? 0.0, longitude: model.longitude ?? 0.0, name: model.branchName ?? "")
            
            cancelSearch()
        }
    }
    
    @objc func btnRatingAction(_ sender: UIButton){
        
        if let model:SearchMainModel = productList?[sender.tag] {
            let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
            if let reviews:SearchRatingVC = storyBoard.instantiateViewController(withIdentifier: "SearchRatingVC") as? SearchRatingVC {
                
                reviews.sellerCode = model.sellerCode ?? 0
                reviews.productCode = model.productCode ?? 0
                
                navigationController?.pushViewController(reviews, animated: true)
                
                cancelSearch()
            }
            
            
        }
    }
    
    @IBAction func btnSellerNotFoundAction(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
        
        if let searchFindVc:SearchFindSellerVC = storyBoard.instantiateViewController(withIdentifier: "SearchFindSellerVC") as? SearchFindSellerVC {
            
            searchFindVc.searchText = searchText
            searchFindVc.delegate = self
            
            cancelSearch()
            
            navigationController?.pushViewController(searchFindVc, animated: true)
            
        }
        
        
    }
    
    @IBAction func btnSelectLocationAction(_ sender: Any) {
        
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        locationPage.lat = currentLoc?.latitude ?? 0.0
        locationPage.lng = currentLoc?.longitude ?? 0.0
        
        locationPage.modalPresentationStyle = .fullScreen
        
        cancelSearch()
        
        navigationController?.present(locationPage, animated: true)
        
    }
    
    @IBAction func btDidSelectCategoryNew(_ sender:UIButton){
        categoryCodeNew = categoryList?[sender.tag].catCode ?? 0
        tblVwheaderHight = 0.01
        updateNavButton()
        pageNo = 1
     
        searchProduct(search: "" , lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage:pageNo,limit: limit, isFirst: isFirst, withIp: ipAddress)
    }
//
//    @objc func scrollAutomatically(_ timer1: Timer) {
//
//
//        if x < advertiseList?.count ?? 0 {
//            let indexPath = IndexPath(item: x, section: 0)
//            collectionViewBanners.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//            x = x + 1
//
//        } else {
//
//            x = 0
//
//            collectionViewBanners.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
//
//        }
//
//    }
//
//
//    func startTimer() {
//
//        timer =  Timer.scheduledTimer(timeInterval: delayTime, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
//
//    }
    
    
    func searchProduct(search:String,lat:Double,long:Double,startPage:Int,limit:Int,isFirst:Int,withIp:String){
        
        
        
        weak var weakSelf = self
        
        if timer != nil {
            timer!.invalidate()
            timer = nil
        }
    
        
        searchText = searchBarProducts.text ?? ""
        
        var params:NSDictionary = [:]
        
        if typeProduct != 2{
        
         params = ["search":search,
                                   "latitude" : lat,
                                   "longitude" : long,
                                   "ip": withIp,
                                   "productType":typeProduct,
                                   "experience":selectedExp,
                                   "jobType" : arrayFilterJobType,
                                    "isFirstTime":isFirst,
                                    "catCode": categoryCodeNew
            
           ]
            
        }
        else{
            
            params = ["search":search,
                      "latitude" : lat,
                      "longitude" : long,
                      "ip": withIp,
                      "productType":typeProduct,
                      "experience":selectedExp,
                      "fromDate" : fromDateEvent,
                      "fromTo": fromToEvent,
                      "jobType" : arrayFilterJobType,
                      "isFirstTime":isFirst,
                      "catCode": categoryCodeNew
                   ]
            
        }
        let encryptedParams = ["data": encryptParams(params: params)]
        
        DispatchQueue.main.async {
                  
                  if isFirst == 1 {
                      weakSelf?.startAnimating()
                  }
                  else{
                      weakSelf?.startAnimatingAfterSubmit()
                  }
              }
        
        let apiClient:APIClient = APIClient()
        
        cancelSearch()
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/productsToReferList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&startPage=\(startPage)&limit=\(limit)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                
                                
                                print(dataObj)
                                
                                let decoder = JSONDecoder()
                                
                                let jsonResponse = try decoder.decode(SearchMainApiModel.self, from: data)
                                
                                if let apiResponse:[SearchMainModel] = jsonResponse.productList {
                                    
                                    weakSelf?.searchBarProducts.resignFirstResponder()
                                    
                                    if weakSelf?.productList?.count ?? 0 > 0
                                    {
                                        
                                        
                                        weakSelf?.productList?.removeAll()
                                        for dict in apiResponse{
                                            
                                            
                                            //                                            if !self.branch.contains(dict.branchCode ?? 0) {
                                            //
                                            //                                                if !self.product.contains(dict.productCode ?? 0) {
                                            
                                            weakSelf?.productList?.append(dict)
                                            //
                                            //                                                    self.branch.insert(dict.branchCode ?? 0)
                                            //                                                    self.product.insert(dict.productCode ?? 0)
                                            //
                                            //                                                }
                                            //                                            }
                                            
                                            print(self.productList?.count ?? 0)
                                        }
                                        
                                        
                                        
                                    }
                                    else{
                                        weakSelf?.productList = apiResponse
                                        
                                        //                                        for dict in apiResponse{
                                        //
                                        //                                            self.branch.insert(dict.branchCode ?? 0)
                                        //                                            self.product.insert(dict.productCode ?? 0)
                                        //
                                        //
                                        //                                        }
                                    }
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.count = apiResponse
                                    
                                }
//                                
//                                if let apiResponse:Int = jsonResponse.count {
//                                                                   
//                                    weakSelf?.count = apiResponse
//                                                        
//                                }
                                
                                if let apiResponse:[AdvertisementModel] = jsonResponse.advertiseList {
                                    
                                    weakSelf?.advertiseList = apiResponse
                                    
                                }
                                
                                if weakSelf?.isFirst == 1{
                                     weakSelf?.headerFooterTitle = (jsonResponse.headerTitle ?? "",jsonResponse.footerTitle ?? "")
                                }
                                
                                if let apiResponse:[ModelCategorySearchList] = jsonResponse.categoryList {
                                    
                                    if  weakSelf?.isFirst == 1{
                                        weakSelf?.categoryList = apiResponse
                                    }
                                    if apiResponse.count > 0 &&  weakSelf?.isFirst == 1{
                                        weakSelf?.tblVwheaderHight = 250.0
                                    } 
                                     
                                }
                                                                                       
                                
                                 weakSelf?.isFirst = 0
                                
                                weakSelf?.stopAnimating()
                                
                                
                                weakSelf?.tableViewList.reloadData()
                                
                          //      weakSelf?.updateAvertisementView()
                                
                                weakSelf?.appDel.currentLocAppDelegate = weakSelf?.currentLoc
                                weakSelf?.appDel.advertiseListAppDelegate = weakSelf?.advertiseList
                                weakSelf?.appDel.productListAppDelegate = weakSelf?.productList
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
            //Show error
            weakSelf?.stopAnimating()
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        weakSelf?.stopAnimating()
        
    }
    
    func getLikes(productCode:Int,isLike:Int,index:Int) {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        
        let params:NSDictionary = ["productCode":productCode,"isLike":isLike]
        
         let encryptedParams = ["data": encryptParams(params: params)]
        
        startAnimatingAfterSubmit()
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveLikeProduct?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataObj:NSDictionary =  jsonObj?.dataObj(){
                        do {
//                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
//                            let decoder = JSONDecoder()
                            print(dataObj)
                            
                             let indexPath = IndexPath(item: index, section: 1)
                            
                            weakSelf?.tableViewList.reloadRows(at: [indexPath], with: .automatic)
                        
                                
                             return
                            
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                            
                        }
                    }
                    
                    let indexPath = IndexPath(item: index, section: 0)
                                               
                    weakSelf?.tableViewList.reloadRows(at: [indexPath], with: .automatic)
            }
          }
            weakSelf?.showErrorMessage(message: message)
            
            
        })
        
    }
    
    
    @objc func callSearchApi(notification: NSNotification) {
        if let isFlag = notification.userInfo?["update"] as? Int {
            
            if isFlag == 1 {
                
                pageNo = 1
                searchProduct(search: "", lat: currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage: pageNo, limit: 10, isFirst: 0, withIp: ipAddress)
                
            }
            
        }
        
    }
    
}
extension SearchMainVC:UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(tblVwheaderHight)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellForHeader") as? TableViewCellForHeader
        cell?.categoryList = categoryList
        cell?.headerFooterTitle = headerFooterTitle
       // cell?.collVwHeader.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if productList?.count ?? 0 > 0{
            
            tableViewList.alpha = 1
            
            lblNoData.alpha = 0
            
            //            viewCollView.alpha = 1
            //
            //            heightViewCollView.constant = 240
            
            return productList?.count ?? 0
            
        }
        else{
            
            tableViewList.alpha = 0
            
            lblNoData.alpha = 1
            
            //            viewCollView.alpha = 0
            //
            //            heightViewCollView.constant = 0
            
            return  0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let model:SearchMainModel = productList?[indexPath.row]{
            
            if model.productType == 0
            {
                
                if let cell:SearchListCell = tableView.dequeueReusableCell(withIdentifier: "SearchListCell", for: indexPath) as? SearchListCell {
                    
                    return getCellForSearchList(cell: cell, indexPath: indexPath)
                    
                }
            }
            
            if model.productType == 1
            {
                
                if let cell:SearchListJobCell = tableView.dequeueReusableCell(withIdentifier: "SearchListJobCell", for: indexPath) as? SearchListJobCell {
                    
                    return getCellForSearchListJob(cell: cell, indexPath: indexPath)
                    
                }
            }
            
            if model.productType == 2 {
                
                if model.isVideo == 0 {
                    
                    if let cell:SearchListImageCell = tableView.dequeueReusableCell(withIdentifier: "SearchListImageCell", for: indexPath) as? SearchListImageCell {
                        
                        return getCellForSearchListImage(cell: cell, indexPath: indexPath)
                        
                    }
                }
                
                if model.isVideo == 1 {
                    
                    if let cell:SearchListVideoCell = tableView.dequeueReusableCell(withIdentifier: "SearchListVideoCell", for: indexPath) as? SearchListVideoCell {
                        
                        return getCellForSearchListVideo(cell: cell, indexPath: indexPath)
                        
                    }
                }
            }
            
        }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:SearchMainModel = productList?[indexPath.row]{
            
            let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
            let newProductPage:SearchProductDetailVC = storyBoard.instantiateViewController(withIdentifier: "SearchProductDetailVC") as! SearchProductDetailVC
            // newProductPage.productList = model
            newProductPage.branchCode = model.branchCode ?? 0
            newProductPage.sellerCode = model.sellerCode ?? 0
            newProductPage.productCode = model.productCode ?? 0
            newProductPage.currentLoc = currentLoc ?? CLLocationCoordinate2DMake(0.0,0.0)
            newProductPage.ipAddress = ipAddress
            cancelSearch()
            navigationController?.pushViewController(newProductPage, animated: true)
            
        }
    }
    
    func getCellForSearchList(cell:SearchListCell,indexPath:IndexPath) -> SearchListCell {
        
        cell.selectionStyle = .none
        
        
        
        if productList?.count ?? 0 > 0 {
            
            if let model:SearchMainModel = productList?[indexPath.row]{
                
                cell.lblProductDes.alpha = 0
                
                let des = model.shortDescription ?? ""
                
                if des.count > 0 {
                    
                    cell.lblProductDes.alpha = 1
                    
                    if des.contains("<") {
                        
                        cell.lblProductDes.attributedText = des.html2AttributedString
                        
                    }
                    else {
                        
                        cell.lblProductDes.text = des
                    }
                }
                
                cell.imgProduct.image = nil
                
                if model.banners?.count ?? 0 > 0 {
                    
                    cell.viewBannerView.alpha = 1
                    cell.arrayBanners = model.banners ?? []
                    cell.modelmain = model
                    cell.delegate = self
                    //                    let dispatchGroup = DispatchGroup()
                    //
                    //                    dispatchGroup.enter()
                    //                    cell.collectionViewBanners.collectionViewLayout.invalidateLayout()
                    //                    dispatchGroup.leave()
                    //
                    //                    dispatchGroup.enter()
                    //                    cell.collectionViewBanners.reloadData()
                    //                    dispatchGroup.leave()
                    //
                    //                    dispatchGroup.enter()
                    
                 //   cell.heightViewCollView.constant = 220
                    //                    dispatchGroup.leave()
                    //
                    //                    dispatchGroup.enter()
                    //                    cell.collectionViewBanners.layoutIfNeeded()
                    //                    dispatchGroup.leave()
                    
                }
                else{
                    cell.viewBannerView.alpha = 0
                    cell.arrayBanners = []
                    cell.heightViewCollView.constant = 0
                }
                
                if cell.timer != nil {
                    cell.timer!.invalidate()
                    cell.timer = nil
                }
                
                if cell.arrayBanners?.count ?? 0 > 0 {
                    
                    cell.startTimer()
                    
                }
                
                if model.productLogo?.count ?? 0 > 0 {
                    
                    cell.imgProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
                    
                }
                else{
                    
                    cell.imgProduct.image = UIImage.init(named: "Logo")
                    
                }
                
                
                // cell.imgViewJob.alpha = 0
                cell.imgViewStar.setImageColor(color: UIColor.btnLinkColor)
                cell.imgViewLocation.setImageColor(color: UIColor.btnLinkColor)
                cell.imgViewReferNow.setImageColor(color: UIColor.btnLinkColor)
                
                cell.lblreferNow.textColor = UIColor.btnLinkColor
                cell.lblRating.textColor = UIColor.btnLinkColor
                cell.lblDistance.textColor = UIColor.btnLinkColor
                
                cell.lblProductName.text = model.productName ?? ""
                cell.lblProductShortDes.text = model.notesForReferralMember ?? ""
                cell.lblProductShortDes.textColor = UIColor.primaryColor
                
                if model.notesForReferralMember?.count ?? 0 > 0 && isReferralMember == 1
                {
                    cell.viewBgProdDesc.isHidden = false
                    cell.topSpaceStackViewPrice.constant = 5
                }
                else
                {
                    cell.viewBgProdDesc.isHidden = true
                    cell.topSpaceStackViewPrice.constant = 0
                }
                ///// ----- New Changes Start
                
                if model.isGift == 1
                {
                    cell.heightOfStackViewGiftWebLink.constant = 40
                    cell.lblGift.text = "Gift".localized()
                    cell.stackViewGiftWebLink.arrangedSubviews[0].isHidden = false
                }
                else
                {
                    cell.stackViewGiftWebLink.arrangedSubviews[0].isHidden = true
                }
                
                if model.website?.count ?? 0 > 0
                {
                    cell.heightOfStackViewGiftWebLink.constant = 40
                    cell.lblWeb.text = "Web".localized()
                    cell.stackViewGiftWebLink.arrangedSubviews[1].isHidden = false
                }
                else
                {
                    cell.stackViewGiftWebLink.arrangedSubviews[1].isHidden = true
                }
                
                if model.youtubeLink?.count ?? 0 > 0
                {
                    cell.heightOfStackViewGiftWebLink.constant = 40
                    cell.lblYouTube.text = "YouTube".localized()
                    cell.stackViewGiftWebLink.arrangedSubviews[2].isHidden = false
                }
                else
                {
                    cell.stackViewGiftWebLink.arrangedSubviews[2].isHidden = true
                }
                
                if model.isGift == 0 && model.website?.count ?? 0 <= 0 && model.youtubeLink?.count ?? 0 <= 0
                {
                    cell.heightOfStackViewGiftWebLink.constant = 0
                    cell.stackViewGiftWebLink.arrangedSubviews[0].isHidden = true
                    cell.stackViewGiftWebLink.arrangedSubviews[1].isHidden = true
                    cell.stackViewGiftWebLink.arrangedSubviews[2].isHidden = true
                }
                
                cell.btnGift.tag = indexPath.row
                cell.btnWeb.tag = indexPath.row
                cell.btnYoutubeLink.tag = indexPath.row
                
                cell.btnGift.addTarget(self, action: #selector(btnSurpriseGiftAction), for: .touchUpInside)
                cell.btnWeb.addTarget(self, action: #selector(btnWebAction), for: .touchUpInside)
                cell.btnYoutubeLink.addTarget(self, action: #selector(btnYouTubeLinkAction), for: .touchUpInside)
                
                
                ///// ----- New Changes Finished
                
                cell.lblPrice.text = "\(model.productPrice ?? "")"
                cell.lblReferralFee.text = "\(model.referralFee ?? "")"
                cell.lblCompanyName.text = model.companyName ?? ""
                
                cell.btnRating.tag = indexPath.row
                
                cell.btnLocation.addTarget(self, action: #selector(btnLocationAction(_:)), for: .touchUpInside)
                
                cell.btnRating.addTarget(self, action: #selector(btnRatingAction(_:)), for: .touchUpInside)
                
                cell.btnLocation.tag = indexPath.row
                
                // cell.lblProductPicsCount.alpha = 0
                
                //cell.viewImageCount.alpha = 0
                
                //            if model.banners.count > 0 {
                //              cell.lblProductPicsCount.alpha = 1
                //              cell.viewImageCount.alpha = 1
                //            }
                
                // cell.lblProductPicsCount.text = "\(model.banners.count)\(" Pics".localized())"
                
                cell.btnReferNow.tag = indexPath.row
                
                cell.btnReferNow.addTarget(self, action: #selector(btnReferProductAction(_:)), for: .touchUpInside)
                
                cell.lblCharityAmnt.text = "\(model.charityValue ?? "")"
                
                cell.lblReferralText.alpha = 0
                
                cell.heightReferalCount.constant = 0
                cell.topSpaceLblReferral.constant = 0
                
                
                if model.avgREAmount?.count ?? 0 > 0 {
                    
                    cell.heightReferalCount.constant = 21
                    cell.topSpaceLblReferral.constant = 8
                    
                    cell.lblReferralText.alpha = 1
                    
                    cell.lblReferralText.textColor = .primaryColor
                    
                    cell.lblReferralText.text = "\(model.avgReferralEarningsText ?? "")\(" ")\(model.avgREAmount ?? "")"
                    
                }
                
                cell.stackViewRatingRefer.arrangedSubviews[0].isHidden = true
                
                if model.distance?.count ?? 0 > 0 {
                    
                    cell.lblDistance.text = "\(model.distance ?? "")"
                    
                    cell.stackViewRatingRefer.arrangedSubviews[0].isHidden = false
                    
                }
                
                if model.rating ?? 0.0 > 0
                {
                    cell.stackViewRatingRefer.arrangedSubviews[1].isHidden = false
                    
                    cell.imgViewStar.alpha = 1
                    if model.rating ?? 0.0 == 1
                    {
                        cell.lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(")")"
                    }
                    else
                    {
                        cell.lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(")")"
                    }
                }
                else if model.rating ?? 0.0 <= 0.0
                {
                    cell.imgViewStar.alpha = 0
                    cell.stackViewRatingRefer.arrangedSubviews[1].isHidden = true
                }
                
                //  cell.lblVerified.text = "Verified".localized()
                
                cell.lblVerified.alpha = 0
                
                cell.imgViewVerified.alpha = 0
                
                cell.rightSpaceLblCompanyName.constant = 10
                
                if model.isVerified == 1 {
                    
                    cell.lblVerified.alpha = 1
                    
                    cell.imgViewVerified.alpha = 1
                    
                    cell.rightSpaceLblCompanyName.constant = 30
                }
                
                //            if model.companyLogo?.count ?? 0 > 0 {
                //                cell.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.companyLogo ?? "")")), completed: nil)
                //            }
                
                cell.lblLoyaltyBonus.text = "\(model.loyaltyValue ?? "")"
                
                cell.stackViewPriceReferral.arrangedSubviews[0].isHidden = true
                cell.stackViewPriceReferral.arrangedSubviews[1].isHidden = true
                cell.stackViewLoyaltyCharity.arrangedSubviews[0].isHidden = true
                cell.stackViewLoyaltyCharity.arrangedSubviews[1].isHidden = true
                cell.stackViewPriceReferral.alpha = 0
                cell.heightStackViewPrice.constant = 0
                cell.stackViewLoyaltyCharity.alpha = 0
                cell.heightStackViewLoyalty.constant = 0
                
                //            if model.productPrice?.count ?? 0 > 0 {
                //
                //                cell.stackViewPriceReferral.alpha = 1
                //                cell.heightStackViewPrice.constant = 70
                //
                //            }
                
                if model.loyaltyValue?.count ?? 0 > 0 || model.charityValue?.count ?? 0 > 0 {
                    
                    cell.stackViewLoyaltyCharity.alpha = 0
                    cell.heightStackViewLoyalty.constant = 00
                    
                }
                
                if model.productPrice?.count ?? 0 > 0 {
                    
                    cell.stackViewPriceReferral.arrangedSubviews[0].isHidden = true
                    
                    cell.stackViewPriceReferral.alpha = 0
                    
                    cell.heightStackViewPrice.constant = 0
                    
                }
                
                
                if model.loyaltyValue?.count ?? 0 > 0 {
                    
                    cell.stackViewLoyaltyCharity.arrangedSubviews[0].isHidden = true
                    
                }
                
                if model.charityValue?.count ?? 0 > 0 {
                    
                    cell.stackViewLoyaltyCharity.arrangedSubviews[1].isHidden = true
                    
                }
                
                print("isReferralMemberisReferralMemberisReferralMember")
                print(isReferralMember)
                
                if model.productType == 1 {
                    //  cell.imgViewJob.alpha = 1
                    cell.imgProduct.image = UIImage.init(named: "jobDefault")
                }
                
                if isReferralMember == 0{
                    
                    if model.productType == 0 {
                        
                        cell.lblreferNow.text = "Send Enquiry".localized()
                        
                        cell.imgViewReferNow.image = UIImage.init(named: "referNowIcon")
                        
                        //  cell.imgViewJob.alpha = 0
                        
                        cell.imgViewReferNow.setImageColor(color: UIColor.btnLinkColor)
                        
                    }
                        
                    else{
                        cell.lblreferNow.text = "Apply Job".localized()
                        
                        cell.imgViewReferNow.image = UIImage.init(named: "applyJob")
                        
                        cell.imgProduct.image = UIImage.init(named: "jobDefault")
                        
                        // cell.imgViewJob.alpha = 1
                        
                        cell.imgViewReferNow.setImageColor(color: UIColor.btnLinkColor)
                    }
                }
                
                if isReferralMember == 1
                {
                    cell.lblreferNow.text = "Refer Now".localized()
                    
                    cell.stackViewPriceReferral.arrangedSubviews[1].isHidden = true
                    
                    if model.referralFee?.count ?? 0 > 0 {
                        
                        cell.stackViewPriceReferral.arrangedSubviews[1].isHidden = true
                        
                        cell.stackViewPriceReferral.alpha = 1
                        
                        cell.heightStackViewPrice.constant = 0
                        
                    }
                    
                }
                
                
            }
            
        }
        return cell
        
    }
    
    func getCellForSearchListImage(cell:SearchListImageCell,indexPath:IndexPath) -> SearchListImageCell {
        
        cell.imgViewLike.setImageColor(color: .btnLinkColor)
                       
        cell.imgViewLocation.setImageColor(color: .btnLinkColor)
        
        if productList?.count ?? 0 > 0 {
            
            if let model:SearchMainModel = productList?[indexPath.row]{
                
                if model.isLike ?? 0 == 1 {
                    
                    cell.imgViewLike.image = UIImage.init(named: "likeFilledIcon")
                     cell.imgViewLike.setImageColor(color: .btnLinkColor)
                 }
                else{
                     cell.imgViewLike.image = UIImage.init(named: "likeIcon")
                     cell.imgViewLike.setImageColor(color: .btnLinkColor)
                }
                
                cell.stackViewRatingLike.arrangedSubviews[1].isHidden = true
                cell.stackViewRatingLike.arrangedSubviews[2].isHidden = true
                
                if model.rating ?? 0.0 > 0
                {
                    cell.stackViewRatingLike.arrangedSubviews[2].isHidden = false
                    
                    if model.rating ?? 0.0 == 1
                    {
                        cell.lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(")")"
                    }
                    else
                    {
                        cell.lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(")")"
                    }
                }
                
                if model.banners?.count ?? 0 > 0 {

                    cell.collectionViewBanners.alpha = 1
                    cell.arrayBanners = model.banners ?? []
                    cell.modelmain = model
                    cell.delegate = self

                    cell.heightViewCollView.constant = 220

                }
                else{
                    cell.collectionViewBanners.alpha = 0
                    cell.arrayBanners = []
                    cell.heightViewCollView.constant = 0
                }
                
                
                if cell.timer != nil {
                    cell.timer!.invalidate()
                    cell.timer = nil
                }
                
                if cell.arrayBanners?.count ?? 0 > 0 {
                    
                    cell.startTimer()
                    
                }
                
                cell.lblProductDes.alpha = 0
                
                let des = model.description ?? ""
                
                if des.count > 0 {
                    
                    cell.lblProductDes.alpha = 1
                    cell.lblProductDes.text = des
                    
                }
                
                cell.lblProductName.text = model.productName ?? ""
                cell.lblEventTime.text = "\(model.displayStartTime ?? "") - \(model.displayEndTime ?? "")"
                
                
                if model.distance?.count ?? 0 > 0 {
                    
                    cell.lblDistance.text = "\(model.distance ?? "")"
                    
                    cell.stackViewRatingLike.arrangedSubviews[1].isHidden = false
                    
                }
                
                cell.imgViewRefer.setImageColor(color: .btnLinkColor)
                
                cell.imgViewLocation.setImageColor(color: .btnLinkColor)
                
                cell.imgViewRegister.setImageColor(color: .btnLinkColor)
                
                //  cell.lblEventTime.text = model.
                //cell.lblProductShortDes.text = model.notesForReferralMember ?? ""
                
                cell.btnRefer.tag = indexPath.row
                             
                cell.btnRegister.tag = indexPath.row
                                            
                cell.btnRefer.addTarget(self, action: #selector(btnReferProductAction(_:)), for: .touchUpInside)
                             
                cell.btnRegister.addTarget(self, action: #selector(btnRegisterProductAction(_:)), for: .touchUpInside)
                
                cell.lblLike.text = "Like".localized()
                
                if model.likeCount ?? 0 > 1{
                    
                    cell.lblLike.text = ("\(model.likeCount ?? 0) \("Likes".localized())")
                    
                }
                
                cell.btnLike.tag = indexPath.row
                cell.btnLike.addTarget(self, action: #selector(btnLikeAction(_:)), for: .touchUpInside)
                
               
                
                cell.btnRating.tag = indexPath.row
                
                cell.btnLocation.addTarget(self, action: #selector(btnLocationAction(_:)), for: .touchUpInside)
                
                cell.btnRating.addTarget(self, action: #selector(btnRatingAction(_:)), for: .touchUpInside)
                
                cell.btnLocation.tag = indexPath.row
                
            }
            
        }
        return cell
    }
    
    func getCellForSearchListVideo(cell:SearchListVideoCell,indexPath:IndexPath) -> SearchListVideoCell {
        
       
        
         if productList?.count ?? 0 > 0 {
            
            if let model:SearchMainModel = productList?[indexPath.row]{
                
                if model.isLike ?? 0 == 1 {
                    
                    cell.imgViewLike.image = UIImage.init(named: "likeFilledIcon")
                    cell.imgViewLike.setImageColor(color: .btnLinkColor)
                }
                else{
                    cell.imgViewLike.image = UIImage.init(named: "likeIcon")
                    cell.imgViewLike.setImageColor(color: .btnLinkColor)
                }
                
                cell.stackViewRatingLike.arrangedSubviews[1].isHidden = true
                cell.stackViewRatingLike.arrangedSubviews[2].isHidden = true
                
                if model.rating ?? 0.0 > 0
                {
                    cell.stackViewRatingLike.arrangedSubviews[2].isHidden = false
                    
                    if model.rating ?? 0.0 == 1
                    {
                        cell.lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(")")"
                    }
                    else
                    {
                        cell.lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(")")"
                    }
                }
                
                if model.banners?.count ?? 0 > 0 {
                    
                    cell.playerView.alpha = 1
                    cell.heightPlayerView.constant = 220
                    
                if let fileName:[String:String] = model.banners?.first{
                    if let imagePath = fileName["cdnPath"] {
                      guard let youtubeURL = URL(string: "https://www.youtube.com/embed/\(imagePath.youtubeID ?? "")") else { return cell }
                      Activity.startAnimating()
                        
                      let webView = WKWebView()
                     
                      Activity.startAnimating()
                      webView.navigationDelegate = self
                      webView.uiDelegate = self
                         
                        
                      webView.frame = cell.playerView.bounds
                      cell.playerView.addSubview(webView)
                        
                      Activity.center = webView.center
                      webView.addSubview(Activity)
                      webView.load(URLRequest(url: youtubeURL))
                        cell.layoutSubviews()
       
                    }
                
                }
//                    let videoLink = model.banners?.first?["cdnPath"] ?? ""
//                    cell.playerView.load(withVideoId: videoLink.youtubeID ?? "")
                }
                else{
                    
                    cell.playerView.alpha = 0
                    cell.heightPlayerView.constant = 0
                }
                
                cell.lblProductDes.alpha = 0
                
                let des = model.description ?? ""
                
                if des.count > 0 {
                    
                    cell.lblProductDes.alpha = 1
                    cell.lblProductDes.text = des
                    
                }
                
                cell.lblProductName.text = model.productName ?? ""
                cell.lblEventTime.text = "\(model.displayStartTime ?? "") - \(model.displayEndTime ?? "")"
                
                
                cell.stackViewRatingLike.arrangedSubviews[1].isHidden = true
                cell.stackViewRatingLike.arrangedSubviews[2].isHidden = true
                
                if model.distance?.count ?? 0 > 0 {
                    
                    cell.lblDistance.text = "\(model.distance ?? "")"
                    
                    cell.stackViewRatingLike.arrangedSubviews[1].isHidden = false
                    
                }
                
                cell.imgViewRefer.setImageColor(color: .btnLinkColor)
                
                 cell.imgViewLike.setImageColor(color: .btnLinkColor)
                
                cell.imgViewLocation.setImageColor(color: .btnLinkColor)
                
                cell.imgViewRegister.setImageColor(color: .btnLinkColor)
                
                cell.btnRefer.tag = indexPath.row
                
                cell.btnRegister.tag = indexPath.row
                               
            cell.btnRefer.addTarget(self, action: #selector(btnReferProductAction(_:)), for: .touchUpInside)
                
            cell.btnRegister.addTarget(self, action: #selector(btnRegisterProductAction(_:)), for: .touchUpInside)
                
                cell.lblLike.text = "Like".localized()
                
                if model.likeCount ?? 0 > 1{
                    
                    cell.lblLike.text = ("\(model.likeCount ?? 0) \("Likes".localized())")
                    
                }
                
//                cell.lblLikes.text = ("\(model.likeCount ?? 0)")
                cell.btnLike.tag = indexPath.row
                cell.btnLike.addTarget(self, action: #selector(btnLikeAction(_:)), for: .touchUpInside)
                
                cell.btnRating.tag = indexPath.row
                
                cell.btnLocation.addTarget(self, action: #selector(btnLocationAction(_:)), for: .touchUpInside)
                
                cell.btnRating.addTarget(self, action: #selector(btnRatingAction(_:)), for: .touchUpInside)
                
                cell.btnLocation.tag = indexPath.row
                
               // cell.imgViewRegister.setImageColor(color: .btnLinkColor)
                
                //  cell.lblEventTime.text = model.
                //cell.lblProductShortDes.text = model.notesForReferralMember ?? ""
                
            }
            
        }
        
        return cell
    }
    
    func getCellForSearchListJob(cell:SearchListJobCell,indexPath:IndexPath) -> SearchListJobCell{
        
               cell.selectionStyle = .none
        
                if productList?.count ?? 0 > 0 {
        //
                    if let model:SearchMainModel = productList?[indexPath.row]{
        //
        //                cell.lblProductDes.alpha = 0
        //
        //                let des = model.shortDescription ?? ""
        //
        //                if des.count > 0 {
        //
        //                    cell.lblProductDes.alpha = 1
        //
        //                    if des.contains("<") {
        //
        //                        cell.lblProductDes.attributedText = des.html2AttributedString
        //
        //                    }
        //                    else {
        //
        //                        cell.lblProductDes.text = des
        //                    }
        //                }
        //
                        cell.imgProduct.image = nil
        //
        //
        //
                        if model.productLogo?.count ?? 0 > 0 {
        
                            cell.imgProduct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
        
                        }
                        else{
        
                           cell.imgProduct.image = UIImage.init(named: "Logo")
        
                        }
        //
        //
                     //   cell.imgViewJob.alpha = 0
                       // cell.imgViewStar.setImageColor(color: UIColor.btnLinkColor)
                        cell.imgViewLocation.setImageColor(color: UIColor.btnLinkColor)
                        cell.imgViewReferNow.setImageColor(color: UIColor.btnLinkColor)
        
//                        cell.lblreferNow.textColor = UIColor.btnLinkColor
//                        cell.lblRating.textColor = UIColor.btnLinkColor
                        cell.lblDistance.textColor = UIColor.btnLinkColor
        //
                        cell.lblProductName.text = model.productName ?? ""
                        cell.lblProductShortDes.text = model.notesForReferralMember ?? ""
                        cell.lblProductShortDes.textColor = UIColor.primaryColor
        //
//                        if model.notesForReferralMember?.count ?? 0 > 0 && isReferralMember == 1
//                        {
//                            cell.viewBgProdDesc.isHidden = false
//                            cell.topSpaceStackViewPrice.constant = 5
//                        }
//                        else
//                        {
//                            cell.viewBgProdDesc.isHidden = true
//                            cell.topSpaceStackViewPrice.constant = 0
//                        }
        //                ///// ----- New Changes Start
        //
//                        if model.isGift == 1
//                        {
//                            cell.heightOfStackViewGiftWebLink.constant = 40
//                           // cell.lblGift.text = "Gift".localized()
//                            cell.stackViewGiftWebLink.arrangedSubviews[0].isHidden = false
//                        }
//                        else
//                        {
//                            cell.stackViewGiftWebLink.arrangedSubviews[0].isHidden = true
//                        }
        //
                        if model.website?.count ?? 0 > 0
                        {
                            cell.heightOfStackViewGiftWebLink.constant = 40
                            cell.lblWeb.text = "Web".localized()
                            cell.stackViewGiftWebLink.arrangedSubviews[0].isHidden = false
                        }
                        else
                        {
                            cell.stackViewGiftWebLink.arrangedSubviews[0].isHidden = true
                        }
        //
                        if model.youtubeLink?.count ?? 0 > 0
                        {
                            cell.heightOfStackViewGiftWebLink.constant = 40
                            cell.lblYouTube.text = "YouTube".localized()
                            cell.stackViewGiftWebLink.arrangedSubviews[1].isHidden = false
                        }
                        else
                        {
                            cell.stackViewGiftWebLink.arrangedSubviews[1].isHidden = true
                        }
        //
                        if model.isGift == 0 && model.website?.count ?? 0 <= 0 && model.youtubeLink?.count ?? 0 <= 0
                        {
                            cell.heightOfStackViewGiftWebLink.constant = 0
                            cell.stackViewGiftWebLink.arrangedSubviews[0].isHidden = true
                            cell.stackViewGiftWebLink.arrangedSubviews[1].isHidden = true
                            //cell.stackViewGiftWebLink.arrangedSubviews[2].isHidden = true
                        }
        //
        //                cell.btnGift.tag = indexPath.row
                        cell.btnWeb.tag = indexPath.row
                        cell.btnYoutubeLink.tag = indexPath.row
        
        //                cell.btnGift.addTarget(self, action: #selector(btnSurpriseGiftAction), for: .touchUpInside)
                        cell.btnWeb.addTarget(self, action: #selector(btnWebAction), for: .touchUpInside)
                        cell.btnYoutubeLink.addTarget(self, action: #selector(btnYouTubeLinkAction), for: .touchUpInside)
        //
        //
        //                ///// ----- New Changes Finished
        //
        //                cell.lblPrice.text = "\(model.productPrice ?? "")"
        //                cell.lblReferralFee.text = "\(model.referralFee ?? "")"
                      cell.lblCompanyName.text = model.companyName ?? ""
        //
        //                cell.btnRating.tag = indexPath.row
        //
                    cell.btnLoc.addTarget(self, action: #selector(btnLocationAction(_:)), for: .touchUpInside)
        //
        //                cell.btnRating.addTarget(self, action: #selector(btnRatingAction(_:)), for: .touchUpInside)
        //
                        cell.btnLoc.tag = indexPath.row
        //
        //                // cell.lblProductPicsCount.alpha = 0
        //
        //                //cell.viewImageCount.alpha = 0
        //
        //                //            if model.banners.count > 0 {
        //                //              cell.lblProductPicsCount.alpha = 1
        //                //              cell.viewImageCount.alpha = 1
        //                //            }
        //
        //                // cell.lblProductPicsCount.text = "\(model.banners.count)\(" Pics".localized())"
        //
                       cell.btnReferNow.tag = indexPath.row
        //
                        cell.btnReferNow.addTarget(self, action: #selector(btnReferProductAction(_:)), for: .touchUpInside)
        //
        //                cell.lblCharityAmnt.text = "\(model.charityValue ?? "")"
        //
        //                cell.lblReferralText.alpha = 0
        //
        //                cell.heightReferalCount.constant = 0
        //                cell.topSpaceLblReferral.constant = 0
        //
        //
        //                if model.avgREAmount?.count ?? 0 > 0 {
        //
        //                    cell.heightReferalCount.constant = 21
        //                    cell.topSpaceLblReferral.constant = 8
        //
        //                    cell.lblReferralText.alpha = 1
        //
        //                    cell.lblReferralText.textColor = .primaryColor
        //
        //                    cell.lblReferralText.text = "\(model.avgReferralEarningsText ?? "")\(" ")\(model.avgREAmount ?? "")"
        //
        //                }
        //
        //                cell.stackViewRatingRefer.arrangedSubviews[0].isHidden = true
        //
                        if model.distance?.count ?? 0 > 0 {

                            cell.lblDistance.text = "\(model.distance ?? "")"

                            cell.stackViewLoactionRefer.arrangedSubviews[0].isHidden = false

                        }
        //
        //                if model.rating ?? 0.0 > 0
        //                {
        //                    cell.stackViewRatingRefer.arrangedSubviews[1].isHidden = false
        //
        //                    cell.imgViewStar.alpha = 1
        //                    if model.rating ?? 0.0 == 1
        //                    {
        //                        cell.lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(")")"
        //                    }
        //                    else
        //                    {
        //                        cell.lblRating.text = "\(model.rating ?? 0.0)\(" (")\(model.totalReviews ?? 0)\(")")"
        //                    }
        //                }
        //                else if model.rating ?? 0.0 <= 0.0
        //                {
        //                    cell.imgViewStar.alpha = 0
        //                    cell.stackViewRatingRefer.arrangedSubviews[1].isHidden = true
        //                }
        //
        //                //  cell.lblVerified.text = "Verified".localized()
        //
        //                cell.lblVerified.alpha = 0
        //
        //                cell.imgViewVerified.alpha = 0
        //
        //                cell.rightSpaceLblCompanyName.constant = 10
        //
        //                if model.isVerified == 1 {
        //
        //                    cell.lblVerified.alpha = 1
        //
        //                    cell.imgViewVerified.alpha = 1
        //
        //                    cell.rightSpaceLblCompanyName.constant = 30
        //                }
        //
        //                //            if model.companyLogo?.count ?? 0 > 0 {
        //                //                cell.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.companyLogo ?? "")")), completed: nil)
        //                //            }
        //
        //                cell.lblLoyaltyBonus.text = "\(model.loyaltyValue ?? "")"
        //
        //                cell.stackViewPriceReferral.arrangedSubviews[0].isHidden = true
        //                cell.stackViewPriceReferral.arrangedSubviews[1].isHidden = true
        //                cell.stackViewLoyaltyCharity.arrangedSubviews[0].isHidden = true
        //                cell.stackViewLoyaltyCharity.arrangedSubviews[1].isHidden = true
        //                cell.stackViewPriceReferral.alpha = 0
        //                cell.heightStackViewPrice.constant = 0
        //                cell.stackViewLoyaltyCharity.alpha = 0
        //                cell.heightStackViewLoyalty.constant = 0
        //
        //                //            if model.productPrice?.count ?? 0 > 0 {
        //                //
        //                //                cell.stackViewPriceReferral.alpha = 1
        //                //                cell.heightStackViewPrice.constant = 70
        //                //
        //                //            }
        //
        //                if model.loyaltyValue?.count ?? 0 > 0 || model.charityValue?.count ?? 0 > 0 {
        //
        //                    cell.stackViewLoyaltyCharity.alpha = 0
        //                    cell.heightStackViewLoyalty.constant = 00
        //
        //                }
        //
        //                if model.productPrice?.count ?? 0 > 0 {
        //
        //                    cell.stackViewPriceReferral.arrangedSubviews[0].isHidden = true
        //
        //                    cell.stackViewPriceReferral.alpha = 0
        //
        //                    cell.heightStackViewPrice.constant = 0
        //
        //                }
        //
        //
        //                if model.loyaltyValue?.count ?? 0 > 0 {
        //
        //                    cell.stackViewLoyaltyCharity.arrangedSubviews[0].isHidden = true
        //
        //                }
        //
        //                if model.charityValue?.count ?? 0 > 0 {
        //
        //                    cell.stackViewLoyaltyCharity.arrangedSubviews[1].isHidden = true
        //
        //                }
        //
        //                print("isReferralMemberisReferralMemberisReferralMember")
        //                print(isReferralMember)
        //
        //                if model.productType == 1 {
        //                    cell.imgViewJob.alpha = 1
        //                    cell.imgProduct.image = UIImage.init(named: "jobDefault")
        //                }
        //
        //                if isReferralMember == 0{
        //
        //                    if model.productType == 0 {
        //
        //                        cell.lblreferNow.text = "Send Enquiry".localized()
        //
        //                        cell.imgViewReferNow.image = UIImage.init(named: "referNowIcon")
        //
        //                        cell.imgViewJob.alpha = 0
        //
        //                        cell.imgViewReferNow.setImageColor(color: UIColor.btnLinkColor)
        //
        //                    }
        //
        //                    else{
        //                        cell.lblreferNow.text = "Apply Job".localized()
        //
        //                        cell.imgViewReferNow.image = UIImage.init(named: "applyJob")
        //
        //                        cell.imgProduct.image = UIImage.init(named: "jobDefault")
        //
        //                        cell.imgViewJob.alpha = 1
        //
        //                        cell.imgViewReferNow.setImageColor(color: UIColor.btnLinkColor)
        //                    }
        //                }
        //
        //                if isReferralMember == 1
        //                {
        //                    cell.lblreferNow.text = "Refer Now".localized()
        //
        //                    cell.stackViewPriceReferral.arrangedSubviews[1].isHidden = true
        //
        //                    if model.referralFee?.count ?? 0 > 0 {
        //
        //                        cell.stackViewPriceReferral.arrangedSubviews[1].isHidden = true
        //
        //                        cell.stackViewPriceReferral.alpha = 1
        //
        //                        cell.heightStackViewPrice.constant = 0
        //
        //                    }
        //                    //                else{
        //                    //
        //                    //                    cell.stackViewPriceReferral.alpha = 0
        //                    //
        //                    //                    cell.heightStackViewPrice.constant = 0
        //                    //
        //                    //                }
        //                }
                        cell.lblExp.text = "\(model.expTo ?? 0) - \(model.expFrom ?? 0)"
                        cell.lblBranch.text = model.branchName ?? ""
                        
      
                    }
        
                }
        return cell
        
        
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
           
           if ((tableViewList.contentOffset.y + tableViewList.frame.size.height) >= tableViewList.contentSize.height)
           {
               if productList?.count ?? 0 < count {
                   if !isDataLoading{
                       isDataLoading = true
                       pageNo = pageNo+1
                       offset = limit * pageNo
                       
                       searchProduct(search: searchBarProducts.text ?? "", lat: currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage: self.pageNo, limit: limit, isFirst: isFirst, withIp: ipAddress)
                       
                   }
                   
               }
               
           }
           
           
//           if !decelerate {
//               self.scrollViewDidStopScrolling()
//           }
           
           
       }
    
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        isDataLoading = false
        
    }
    
    //Pagination
   
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
       // self.scrollViewDidStopScrolling()
        
        
    }
     
     /*
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        defer {
            self.previousScrollViewHeight = scrollView.contentSize.height
            self.previousScrollOffset = scrollView.contentOffset.y
        }
        
        let heightDiff = scrollView.contentSize.height - self.previousScrollViewHeight
        let scrollDiff = (scrollView.contentOffset.y - self.previousScrollOffset)
        
        
        guard heightDiff == 0 else { return }
        
        let absoluteTop: CGFloat = 0;
        let absoluteBottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height;
        
        let isScrollingDown = scrollDiff > 0 && scrollView.contentOffset.y > absoluteTop
        let isScrollingUp = scrollDiff < 0 && scrollView.contentOffset.y < absoluteBottom
        
        if canAnimateHeader(scrollView) {
            
            
            var newHeight = self.heightViewCollView.constant
            if isScrollingDown {
                newHeight = max(self.minHeaderHeight, self.heightViewCollView.constant - abs(scrollDiff))
            } else if isScrollingUp && isRowZeroVisible() {
                newHeight = min(self.maxHeaderHeight, self.heightViewCollView.constant + abs(scrollDiff))
            }
            
            
            if newHeight != self.heightViewCollView.constant {
                self.heightViewCollView.constant = newHeight
                self.updateHeader()
                self.setScrollPosition(self.previousScrollOffset)
            }
        }
        
        
    }
    
    func scrollViewDidStopScrolling() {
        
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let midPoint = self.minHeaderHeight + (range / 2)
        
        if self.heightViewCollView.constant > midPoint {
            self.expandHeader()
        } else {
            self.collapseHeader()
        }
        
        
    }
    
   
    func isRowZeroVisible() -> Bool {
        let indexes = tableViewList.indexPathsForVisibleRows
        for index in indexes ?? [] {
            if index.row == 0 {
                return true
            }
        }
        
        return false
    }
    
    
    func canAnimateHeader(_ scrollView: UIScrollView) -> Bool {
        
        let scrollViewMaxHeight = scrollView.frame.height + self.heightViewCollView.constant - minHeaderHeight
        
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    
    func collapseHeader() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.heightViewCollView.constant = self.minHeaderHeight
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }
    
    func expandHeader() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.heightViewCollView.constant = self.maxHeaderHeight
            self.updateHeader()
            self.view.layoutIfNeeded()
        })
    }
    
    func setScrollPosition(_ position: CGFloat) {
        self.tableViewList.contentOffset = CGPoint(x: self.tableViewList.contentOffset.x, y: position)
    }
    
    func updateHeader() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let openAmount = self.heightViewCollView.constant - self.minHeaderHeight
        let percentage = openAmount / range
        
        self.viewCollView.alpha = percentage
    }
     */
    
}
extension SearchMainVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // if heightViewCollView.constant > 0 {
        
        return CGSize.init(width: collectionViewBanners.frame.size.width, height: 220)
        // }
        
        return CGSize.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        collectionViewBanners.collectionViewLayout.invalidateLayout()
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewBanners {
            
            if advertiseList?.count ?? 0 > 0 {
                
                return advertiseList?.count ?? 0
            }
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewBanners {
            
            if let cell:SearchMainHeaderCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchMainHeaderCell", for: indexPath) as? SearchMainHeaderCell {
                
                return getCellForSearcMainHeader(cell: cell, indexPath: indexPath)
                
            }
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let model:AdvertisementModel = advertiseList?[indexPath.row] {
            
            if model.type == 1 {
                
                let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
                let referProductPage:SearchReferProductVC = storyBoard.instantiateViewController(withIdentifier: "SearchReferProductVC") as! SearchReferProductVC
                referProductPage.productCode = model.productCode ?? 0
                referProductPage.productName = model.productName ?? ""
                referProductPage.companyName = model.sellerCompanyName ?? ""
                referProductPage.productLogo = model.productLogo ?? ""
                referProductPage.sellerCode = model.sellerCode ?? 0
                referProductPage.productType = model.productType ?? 0
                
                if isReferralMember == 1 {
                    
                    referProductPage.referralMsg = model.referralFormMsg ?? ""
                    
                }
                else{
                    
                    referProductPage.referralMsg = model.enquiryOrApplyJobFormMsg ?? ""
                }
                
                cancelSearch()
                navigationController?.pushViewController(referProductPage, animated: true)
                
                
            }
            else{
                let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
                let newProductPage:SearchProductDetailVC = storyBoard.instantiateViewController(withIdentifier: "SearchProductDetailVC") as! SearchProductDetailVC
                
                newProductPage.branchCode = model.branchCode ?? 0
                newProductPage.sellerCode = model.sellerCode ?? 0
                newProductPage.productCode = model.productCode ?? 0
                newProductPage.currentLoc = currentLoc ?? CLLocationCoordinate2DMake(0.0,0.0)
                newProductPage.ipAddress = ipAddress
                navigationController?.pushViewController(newProductPage, animated: true)
                
            }
            
            
        }
        
    }
    
    
    func getCellForSearcMainHeader(cell:SearchMainHeaderCell,indexPath:IndexPath) -> SearchMainHeaderCell {
        
        if advertiseList?.count ?? 0 > 0 {
            
            if let model:AdvertisementModel = advertiseList?[indexPath.row]
            {
                if model.cdnPath?.count ?? 0 > 0
                {
                    if let url:URL = URL.init(string: "\(StorageUrl)\(model.cdnPath ?? "")"){
                        
                        cell.imgViewHeader.sd_setImage(with: url, completed: nil)
                    }
                    
                }
                
                //  delayTime = Double(model.delay ?? 0)
                
            }
        }
        return cell
    }
    
    
    
    
    
}

extension SearchMainVC: UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate {
    
    func searchBar(_ searchBar: UISearchBar,
                   shouldChangeTextIn range: NSRange,
                   replacementText text: String) -> Bool
    {
        didTapDeleteKey = text.isEmpty
        
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) -> Void {
        
        searchBarProducts.showsCancelButton = true
        
        heightViewSellerNotFound.constant = 0
        lblSellerNotFoundTitle.alpha = 0
        
        
        heightViewCollView.constant = 0
        maxHeaderHeight = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
       
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        
        
        cancelSearch()
        
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        cancelSearch()
        
        // if productList?.count == 0 {
        
        //            pageNo = 1
        //
        //             searchProduct(search: searchBar.text ?? "" , lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage:1,limit: limit, isFirst: isFirst, withIp: ipAddress)
        //}
        
    }
    
    @objc func cancelSearch(){
        
        searchBarProducts.showsCancelButton = false
        searchBarProducts.resignFirstResponder()
        
       // heightViewCollView.constant = 240
      //  maxHeaderHeight = 240
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    func animateViewSeller(isPresent:Bool){
        
        if isPresent{
            
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            viewSellerNotFound.layer.add(transition, forKey: nil)
            
        }
        else{
            
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            viewSellerNotFound.layer.add(transition, forKey: nil)
            heightViewSellerNotFound.constant = 0
            lblSellerNotFoundTitle.alpha = 0
            viewSellerNotFound.alpha = 0
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        
        searchBarProducts.resignFirstResponder()
        
        searchBarProducts.showsCancelButton = false
        
        heightViewSellerNotFound.constant = 40
        lblSellerNotFoundTitle.alpha = 1
        viewSellerNotFound.alpha = 1
        
        animateViewSeller(isPresent: true)
        
        //
        //                     UIView.transition(with: viewSellerNotFound,
        //                                       duration: 5.0,
        //                                       options: [.repeat,.autoreverse],
        //                                       animations: {
        //
        //                                         self.viewSellerNotFound.alpha = 0
        //                                       // self.heightViewSellerNotFound.constant = 0
        //                                        self.lblSellerNotFoundTitle.alpha = 0
        //                     },
        //                                       completion: nil)
        
        let seconds = 5.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.animateViewSeller(isPresent: false)
        }
        
        
        productList?.removeAll()
         
        tblVwheaderHight = 0.01
        searchProduct(search: searchBar.text ?? "" , lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage:1,limit: limit, isFirst: isFirst, withIp: ipAddress)
        
        updateNavButton()
       tableViewList.scrollsToTop = true
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if !didTapDeleteKey && searchText.isEmpty {
            
            pageNo = 1
            
            searchProduct(search: searchBar.text ?? "" , lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage:pageNo,limit: limit, isFirst: isFirst, withIp: ipAddress)
            
        }
        
        didTapDeleteKey = false
        
       
        
    }
}

extension SearchMainVC:SearchFindSellerVCDelegate,SearchJobFilterDelegate{
    
    func didCallSearchList(){
        
        cancelSearch()
        
        searchBarProducts.text = ""
        
        heightViewSellerNotFound.constant = 0
        lblSellerNotFoundTitle.alpha = 0
        
        pageNo = 1
        
        searchProduct(search: "" , lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage:pageNo,limit: limit, isFirst: isFirst, withIp: ipAddress)
        
        tableViewList.scrollsToTop = true
        
    }
    
    func filterData(productType:Int?, experience:Int = 0, fromDate:String?, toDate:String?, jobType:[Int]?){
    
        typeProduct = productType ?? -1
        selectedExp = experience
        fromDateEvent = fromDate ?? ""
        fromToEvent = toDate ?? ""
        arrayFilterJobType = jobType ?? []
        
         cancelSearch()
               
        searchBarProducts.text = ""
               
        heightViewSellerNotFound.constant = 0
        lblSellerNotFoundTitle.alpha = 0
        
//        if jobType?.count ?? 0 < 1 && experience == 0{
//           buttonSelectionCategory(categoryButton[1],false)
//        }
        
        if productType == -1 {
           buttonSelectionCategory(categoryButton[1],false)
           buttonSelectionCategory(categoryButton[2],false)
        }
        self.showCategoriesAction(UIButton())
               
        searchProduct(search: "" , lat:  currentLoc?.latitude ?? 0.0, long: currentLoc?.longitude ?? 0.0, startPage:1,limit: limit, isFirst: isFirst, withIp: ipAddress)
               
        tableViewList.scrollsToTop = true
    }
}

extension SearchMainVC:SearchListCellDelegate{
    
    func didSelectedBanner(withModel: SearchMainModel) {
        
        if let model:SearchMainModel = withModel{
            
            let storyBoard = UIStoryboard.init(name: SearchSB, bundle: nil)
            let newProductPage:SearchProductDetailVC = storyBoard.instantiateViewController(withIdentifier: "SearchProductDetailVC") as! SearchProductDetailVC
            // newProductPage.productList = model
            newProductPage.branchCode = model.branchCode ?? 0
            newProductPage.sellerCode = model.sellerCode ?? 0
            newProductPage.productCode = model.productCode ?? 0
            newProductPage.currentLoc = currentLoc ?? CLLocationCoordinate2DMake(0.0,0.0)
            newProductPage.ipAddress = ipAddress
            cancelSearch()
            navigationController?.pushViewController(newProductPage, animated: true)
            
        }
        
    }
    
}
