//
//  SearchFindSellerVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 26/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

protocol SearchFindSellerVCDelegate: class {
    func didCallSearchList()
}
class SearchFindSellerVC: AppBaseViewController,LocationPermissionVCNewDelegate,CLLocationManagerDelegate {
    

    @IBOutlet weak var lblTopTitle: NKLabel!
    
    @IBOutlet weak var txtViewDes: NKTextView!
 
    @IBOutlet weak var heightViewAddress: NSLayoutConstraint!
    
    @IBOutlet weak var lblAddress: NKLabel!
    
    @IBOutlet weak var imgLocation: UIImageView!
    
    var locationManager = CLLocationManager()
    
    var btnName = UIButton()
    
    var currentLoc:CLLocationCoordinate2D?
    
    var searchText:String = ""
    
    weak var delegate:SearchFindSellerVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "iCanRefer".localized()
        
       lblTopTitle.text = "Describe the Products/Services you want us to register".localized()
        
       imgLocation.setImageColor(color: .white)

        getCurrentLocation()
        
        setUpNavItem()
        
        lblAddress.text = "Fetching Location...".localized()
        
        txtViewDes.text = searchText
        
        txtViewDes.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateLocationName()
    }
    
    func setUpNavItem() {
        
        
        btnName.setTitle("Submit -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(callSubmit), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    func updateAddressViewHeight() {
        
        guard let labelText = lblAddress.text else { return }
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        heightViewAddress.constant = height + 30
        
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let size = CGSize(width: view.frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: UIFont.fontForType(textType: 6)]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }
    
    func showLocationRequiredPage(){
        weak var weakSelf = self
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable location access to provide you near by search.".localized(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Enable Location Service".localized(), style: .default, handler: {action in
            
            weakSelf?.openAppSettings()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func getCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch locationAuthorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.startUpdatingLocation()
                currentLoc = locationManager.location?.coordinate
                
                // updateLocationName()
            }
        case .restricted, .denied:
            
          //  showLocationRequiredPage()
            
            print("Access Denied")
            
        }
    }
    
    
    
    //MARK: Location manager
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if  let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
            
            currentLoc = locValue
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        stopAnimating()
    }
    
    func updateLocationName() {
        let geoCoder = GMSGeocoder()
        
        weak var weakSelf = self
        
        geoCoder.reverseGeocodeCoordinate(currentLoc ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), completionHandler: { response, error in
            
            if response != nil {
                let address = response?.results()?.first
                if address != nil {
                    
                    weakSelf?.lblAddress.text = address?.lines?.joined(separator: ",")
                    
                    weakSelf?.updateAddressViewHeight()
                    
                    weakSelf?.lblAddress.textColor = .black
                }
            }
            
        })
    }
    
  func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        
        currentLoc = coordinate
        updateLocationName()
        updateAddressViewHeight()
        
    }
    
    @IBAction func btnSelectLocationAction(_ sender: Any) {
        
        openMap()
        
    }
   
    func openMap() {
        
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        locationPage.lat = currentLoc?.latitude ?? 0.0
        locationPage.lng = currentLoc?.longitude ?? 0.0
        
        locationPage.modalPresentationStyle = .fullScreen
        
        navigationController?.present(locationPage, animated: true)
    }
    
    @IBAction func btnChangeLocationAction(_ sender: Any) {
        
        openMap()
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        callSubmit()
        
    }
    
    @objc func callSubmit(){
        
        if txtViewDes.text.count <= 0 {
            
            txtViewDes.showError(message: "Required".localized())
            
             return
        }
        
        if lblAddress.text?.count ?? 0 <= 0 {
            
            
             return
        }
        
        if currentLoc?.latitude ?? 0.0 <= 0.0 {
            
            
             return
        }
        if currentLoc?.longitude ?? 0.0 <= 0.0 {
            
             return
            
        }
        
        submitUserDetails()
        
        
    }
    
    func submitUserDetails(){

        startAnimatingAfterSubmit()

        let params:NSDictionary = ["searchQuery": searchText,
                                   "description":txtViewDes.text ?? "",
                                   "address":lblAddress.text ?? "",
                                   "latitude":currentLoc?.latitude ?? 0.0,
                                   "longitude":currentLoc?.longitude ?? 0.0
                                   
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self

        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/newSellerRequestSubmit?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in

            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            if let response = response {

                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){

                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){

                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
 
                                weakSelf?.showInfoMessage(message: message)

                                weakSelf?.delegate?.didCallSearchList()
                                weakSelf?.navigationController?.popViewController(animated: true)

                                return


                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
                weakSelf?.showErrorMessage(message: message)
                weakSelf?.dismiss(animated: true, completion: nil)

            }

            //Show error
            //  weakSelf?.showErrorMessage(message: message)

        })


    }
    
}

extension SearchFindSellerVC:UITextViewDelegate{
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView == txtViewDes {
            txtViewDes.showError(message: "")
        }
        
        return true
    }
}
