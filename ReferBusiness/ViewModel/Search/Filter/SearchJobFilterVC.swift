//
//  SearchJobFilterVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 05/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

protocol SearchJobFilterDelegate:class {
    
    func filterData(productType:Int?, experience:Int, fromDate:String?, toDate:String?, jobType:[Int]?)
    
}

class SearchJobFilterVC: AppBaseViewController {
    
    @IBOutlet weak var tableViewExperience:UITableView!
    
    @IBOutlet weak var viewDateSelection:UIView!
    
    @IBOutlet weak var tableViewType:UITableView!
    
    @IBOutlet weak var heightConstraintFirstView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintSecondView: NSLayoutConstraint!
    
    @IBOutlet weak var viewForTblViewFirst: UIView!
    @IBOutlet weak var viewForTblViewSecond: UIView!
    
    var formatedStartDate = ""
    var formatedEndDate = ""
    var isSelected:Int = -1
    
    var productType = -1
    
    weak var delegateFilter:SearchJobFilterDelegate?
    
    var jobTypeList:[JobTypeFilter]?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var experienceList:[ExperienceListModel]?
    
    @IBOutlet weak var txtStartDate: RCTextField!
    
    @IBOutlet weak var txtEndDate: RCTextField!
    
    @IBOutlet weak var tbleVwDate:UITableView!
    
    var arrayFilterList:[Int] = []
    
    var arrayEventTimeFilter = ["Today".localized(),"Tommorow".localized(),"Weekend".localized(),"Date Range".localized()]
    var EventTimeSelectedCellValue = -1
    
    
    
    var selectedDateString :NSMutableAttributedString?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        jobTypeList = appDelegate.jobTypeList
        isSelected = appDelegate.selectedExperience
        formatedStartDate = appDelegate.formatedStartDate
        formatedEndDate = appDelegate.formatedEndDate
        EventTimeSelectedCellValue = appDelegate.EventTimeSelectedCellValue
        
//        tableViewExperience.delegate = self
//        tableViewExperience.dataSource = self
//
        txtEndDate.RCDelegate = self
        fetchFilterList()
        if productType == 2{
           self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Clear...", style: .plain, target: self, action: #selector(rightBarButtonItem))
        }
         
        filterViewsSetup()
        viewDateSelection.center = self.view.center
        viewDateSelection.removeFromSuperview()
        
    }
    @objc func rightBarButtonItem(){
        EventTimeSelectedCellValue = -1
        appDelegate.EventTimeSelectedCellValue = EventTimeSelectedCellValue
        formatedEndDate = ""
        appDelegate.formatedEndDate = formatedEndDate
        
        formatedStartDate = ""
        appDelegate.formatedStartDate = formatedStartDate
        
        delegateFilter?.filterData(productType: -1, experience: -1, fromDate: "", toDate: "", jobType: [Int]())
        self.tbleVwDate.reloadData()
    }
    
    @IBAction func btnApplyFilter(_ sender: UIButton) {
      
        if productType == 1{
            
            var expType = 0
            
            if isSelected > -1 && experienceList?.count ?? 0 > 0{
                
                 expType = experienceList?[isSelected].expType ?? 0
                
            }
            
            arrayFilterList = []
            
            if let intArray:[JobTypeFilter] = jobTypeList{
                
                for model in intArray{
                    
                    if model.isSelected == 1{
                        
                        arrayFilterList.append(model.jobType ?? 0)
                        
                    }
                    
                }
                
            }
            appDelegate.jobTypeList = jobTypeList
            appDelegate.selectedExperience = isSelected
            
            
            
            
            delegateFilter?.filterData(productType: productType, experience: expType, fromDate: "", toDate: "", jobType: arrayFilterList)
            
        }
        
        if productType == 2{
            appDelegate.EventTimeSelectedCellValue = EventTimeSelectedCellValue
            appDelegate.formatedStartDate = formatedStartDate
            appDelegate.formatedEndDate = formatedEndDate
            
            delegateFilter?.filterData(productType: productType, experience: 0, fromDate: formatedStartDate, toDate:  formatedEndDate, jobType: arrayFilterList)
            
        }
        
        navigationController?.popViewController(animated: true)
        
    }
    
   @IBAction func btClearFilter(_ sender:UIButton){
        if sender.tag == 200{
           isSelected = -1
            
            
        }else if sender.tag == 300{
           jobTypeList?.removeAll()
           
        }
         
         
        fetchFilterList()
    }
    
    func tableViewHightMesurments(){
        let screenHight = UIScreen.main.bounds.height - self.topbarHeight
        let tableViewHight = (screenHight - 65) / 2 - 10
        heightConstraintFirstView.constant = tableViewHight
        heightConstraintSecondView.constant = tableViewHight
        
    }
    
    func filterViewsSetup(){
        if productType == 0{
            
            
        }else if productType == 1{
           tableViewHightMesurments()
            tbleVwDate.isHidden = true
                         
        }else if productType == 2{
            viewForTblViewFirst.isHidden = true
            viewForTblViewSecond.isHidden = true
            tbleVwDate.isHidden = false
            
        }
    }
    
    func getDateTimeWithFormat(stringDate:Date) -> String! {
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          dateFormatter.timeZone = TimeZone.current
          dateFormatter.locale = Locale.current
          return dateFormatter.string(from: stringDate)
      }
    
    //MARK: WEB Service Search Filter
    
    
    func fetchFilterList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/searchFilters?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(FilterApiModel.self, from: data)
                                
                                if let apiResponse:[JobTypeFilter] = jsonResponse.jobTypeList {
                                    if weakSelf?.jobTypeList?.count ?? 0 > 0{
                                    }else{
                                       weakSelf?.jobTypeList = apiResponse
                                    }
                                    
                                    
                                }
                                
                                if let apiResponse:[ExperienceListModel] = jsonResponse.experienceList {
                                    
                                    weakSelf?.experienceList = apiResponse
                                    
                                }
                                
                                
                                weakSelf?.tableViewType.reloadData()
                                weakSelf?.tableViewExperience.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
    
    
}

extension SearchJobFilterVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tbleVwDate == tableView{
           return UITableView.automaticDimension
        }
        return 50//UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableViewExperience {
            
            return experienceList?.count ?? 0
        }
        
        if tableView == tableViewType {
            
            return jobTypeList?.count ?? 0
            
        }
        
        if tableView == tbleVwDate{
            return arrayEventTimeFilter.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewExperience {
            
            if let cell:SearchJobFilterCell = tableView.dequeueReusableCell(withIdentifier: "SearchJobFilterCell", for: indexPath) as? SearchJobFilterCell {
                
                return getCellForJobExperience(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if tableView == tableViewType {
            
            if let cell:SearchJobFilterCell = tableView.dequeueReusableCell(withIdentifier: "SearchJobFilterCell", for: indexPath) as? SearchJobFilterCell {
                
                return getCellForJobType(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        if tableView == tbleVwDate {
            
            if let cell:SearchJobFilterCell = tableView.dequeueReusableCell(withIdentifier: "SearchJobFilterCell", for: indexPath) as? SearchJobFilterCell {
                
                return getCellForEventTime(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        
        return UITableViewCell.init()
    }
    
     
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableViewExperience {
            
            isSelected = indexPath.row
            
            tableViewExperience.reloadData()
            
        }
        
        if tableView == tableViewType {
            
            if let model:JobTypeFilter = jobTypeList?[indexPath.row]{
                
                if model.isSelected == 0 {
                    
                    model.isSelected = 1
                    
                }
                else{
                    model.isSelected = 0
                }
                
                tableViewType.reloadData()
                
            }
            
        }
        
        if tableView == tbleVwDate{
            EventTimeSelectedCellValue = indexPath.row
            self.tbleVwDate.reloadData()
            if indexPath.row == arrayEventTimeFilter.count - 1{
                viewDateSelection.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height )
                self.view.addSubview(viewDateSelection)
                viewDateSelection.isHidden = false
              
            }
        }
      
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == tbleVwDate{
            return "Select event date".localized()
        }else{
            return ""
        }
        
          
    }
    
    @IBAction func buttonDismissDatePickerView(_ sender:UIButton){
        viewDateSelection.removeFromSuperview()
         
        let arrtributedStringStartDate = NSMutableAttributedString(string:("   \(txtStartDate.text ?? "")"), attributes: [ NSAttributedString.Key.foregroundColor: UIColor.red ])
        
        let arrtributedStringTo = NSMutableAttributedString(string:" to ", attributes: [ NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font:  UIFont.systemFont(ofSize: 14, weight: .heavy)])
        
        let arrtributedStringEndDate = NSMutableAttributedString(string:(txtEndDate.text ?? ""), attributes: [ NSAttributedString.Key.foregroundColor: UIColor.red ])
        
        arrtributedStringStartDate.append(arrtributedStringTo)
        arrtributedStringStartDate.append(arrtributedStringEndDate)
          
        selectedDateString = arrtributedStringStartDate
        
        formatedStartDate = getDateTimeWithFormat(stringDate: txtStartDate?.selectedDate ?? Date.init())
        formatedEndDate = getDateTimeWithFormat(stringDate: txtEndDate?.selectedDate ?? Date.init())
        tbleVwDate.reloadData()
    }
    
    func getCellForJobExperience(cell:SearchJobFilterCell,indexPath:IndexPath) -> SearchJobFilterCell {
        
        cell.selectionStyle = .none
       
        if let model:ExperienceListModel = experienceList?[indexPath.row]{
            
            cell.lblTitle.text = "\(model.title ?? "")"
            
              if isSelected == indexPath.row {
                      
                 cell.imgViewSelection.image = UIImage.init(imageLiteralResourceName: "radioFilled")
                
            }
            else{
                
                cell.imgViewSelection.image = UIImage.init(imageLiteralResourceName: "radio")
            }
        }
        
        
        return cell
    }
    
    
    func getCellForJobType(cell:SearchJobFilterCell,indexPath:IndexPath) -> SearchJobFilterCell {
        
        cell.selectionStyle = .none
        
        if let model:JobTypeFilter = jobTypeList?[indexPath.row]{
            
            cell.lblTitle.text = "\(model.title ?? "")"
        
            if model.isSelected == 1 {
                
                cell.imgViewSelection.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
                              
            }
            else{
                cell.imgViewSelection.image = UIImage.init(imageLiteralResourceName: "checkBox")
            }
        }
        
        return cell
    }
    
    func getCellForEventTime(cell:SearchJobFilterCell,indexPath:IndexPath) -> SearchJobFilterCell {
        
        cell.selectionStyle = .none
        cell.tintColor = .lightGray
        cell.lblTitle.text = arrayEventTimeFilter[indexPath.row]
        
     //   serverDateTimeString()
         let currentDate = Date().serverDateTimeString()
         print(currentDate)
       
        if  indexPath.row != arrayEventTimeFilter.count - 1{
            
            if EventTimeSelectedCellValue == indexPath.row{
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
            if EventTimeSelectedCellValue == 0{
                
               formatedStartDate = currentDate
               let tommorowzDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
               formatedEndDate = getDateTimeWithFormat(stringDate: tommorowzDate ?? Date())
              
                
            }else if EventTimeSelectedCellValue == 1{
                let tommorowzDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
                formatedStartDate = getDateTimeWithFormat(stringDate: tommorowzDate ?? Date())
                
                let dayAfterTommorowzDate = Calendar.current.date(byAdding: .day, value: 2, to: Date())
                formatedEndDate = getDateTimeWithFormat(stringDate: dayAfterTommorowzDate ?? Date())
              //  Calendar.current.da
            }else if EventTimeSelectedCellValue == 2{
                let x = Calendar.current.nextWeekend(startingAfter: Date())
               // print(getDateTimeWithFormat(stringDate: x?.end ?? Date()))
                print(getDateTimeWithFormat(stringDate: x?.start ?? Date()))
                
            formatedStartDate = getDateTimeWithFormat(stringDate: Calendar.current.date(byAdding: .day, value: -1, to: x?.start ?? Date())!)
            formatedEndDate = getDateTimeWithFormat(stringDate: Calendar.current.date(byAdding: .day, value: 1, to: x?.start ?? Date())!)
  
            }
            
            
        }else if indexPath.row == arrayEventTimeFilter.count - 1{
             cell.accessoryType = .disclosureIndicator
             
            let arrtributedStringDateRange = NSMutableAttributedString(string: arrayEventTimeFilter[indexPath.row], attributes: [ NSAttributedString.Key.foregroundColor: UIColor.black ])
                 
            if EventTimeSelectedCellValue != indexPath.row{
                cell.lblTitle.attributedText = arrtributedStringDateRange
            }else{
                arrtributedStringDateRange.append(selectedDateString ?? NSAttributedString(string: ""))
                cell.lblTitle.attributedText = arrtributedStringDateRange
            }
             
        }else{
            cell.accessoryType = .none
        }
        
        
//       if let model:JobTypeFilter = jobTypeList?[indexPath.row]{
//
//            cell.lblTitle.text = "\(model.title ?? "")"
//
//            if model.isSelected == 1 {
//
//                cell.imgViewSelection.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
//
//            }
//            else{
//                cell.imgViewSelection.image = UIImage.init(imageLiteralResourceName: "checkBox")
//            }
//        }
        
        return cell
    }
    
}

class SearchJobFilterCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle:NKLabel!
    
    @IBOutlet weak var imgViewSelection:UIImageView!
    
}


extension SearchJobFilterVC:RCTextFieldDelegate{
    
    func didChangeDate(textField: RCTextField) {
        
        if textField.tag == 11 {
              
            txtEndDate.dateTimePicker.minimumDate = txtStartDate.selectedDate
        }
        
    }
    
}
