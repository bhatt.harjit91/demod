//
//  ReferralChangeLogVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 20/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ReferralChangeLogVC: UIViewController {
    
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var lblNoData: NKLabel!
    
    var changeLog:[LogModel]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblNoData.text = "No Data Found".localized()
    }
}

extension ReferralChangeLogVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let count = changeLog?.count ?? 0
               if count > 0 {
                   
                   tableViewList.alpha = 1
                   
                   return changeLog?.count ?? 0
                   
               }
               else{
                   
                   tableViewList.alpha = 0
               }
               
               return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            if let cell:ReferralHistoryCell = tableView.dequeueReusableCell(withIdentifier: "ReferralHistoryCell", for: indexPath) as? ReferralHistoryCell {
                
                return getCellForReferralHistoryCell(cell: cell, indexPath: indexPath)
                
            }
        
        return UITableViewCell.init()
    }
    
    func getCellForReferralHistoryCell(cell:ReferralHistoryCell,indexPath:IndexPath) -> ReferralHistoryCell {
        
        cell.selectionStyle = .none
        
        if let model:LogModel = changeLog?[indexPath.row]{
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.crDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblStatus.text = "\(model.oldStageTitle ?? "")\(" to ")\(model.newStageTitle ?? "")\(" by ")\(model.createdBy ?? "")\(" on ")\(" ")\(dateText)"
            
        }
        
        return cell
    }
    
   
}
