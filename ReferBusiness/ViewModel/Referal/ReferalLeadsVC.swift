//
//  ReferalLeadsVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 18/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ReferalLeadsVC: AppBaseViewController,ReferalManagerVCDelegate,ReferralSelectSellerVCDelegate {
    
    @IBOutlet weak var lblCount: NKLabel!
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblNoData: NKLabel!
    
    @IBOutlet weak var btnSeller: NKButton!
    
    @IBOutlet weak var collectionViewFilters: UICollectionView!
    
    var referralLeadList:[ReferalModel]?
    
    var filterMasterList:[FilterReferralModel]?
    
    var sellerCode:Int?
    
    var branchCode:Int = 0
    
    var userCount:Int = 0
    
    var isSelectSellerVisible:Int?
    
    var type:Int?
    
    var stageType:Int?
    
    var strTitle:String?
    
    var progressValue:String = "0"
    
    var isFromHome:Bool = false
    
    private var isExpanded: Bool = false
    
    private var isBtnPressed: Bool = false
    
    var stageList:[StageListMainModel]?
    
    var jobStageList:[StageListMainModel]?
    
    var currency:[CurrencyModel]?
    
    var selectedCurrencyCode : CurrencyModel?
    
    var textFieldIndexPath:IndexPath?
    
    var isUpdated:Bool = false
    
    var selectedRowIndex: NSIndexPath?
    
    var sellerId = [Int]()
    
    var isSelectAll = 0
    
    var selectedFilter = 1
    
    var isDataLoaded:Bool = false
    
    var isFromHomeNew:Bool = false
    
    let underLineAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let underLineButtonAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 4),
        .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let underLineResumeAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.primaryColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var startPage = 1
    
    var limit = 20
    
    var productCode:Int = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if filterMasterList?.count ?? 0 > 0 {
            
            selectedFilter = filterMasterList?.last?.type ?? 0
            
        }
        
        collectionViewFilters.reloadData()
        
        //   navigationItem.title = strTitle ?? "Referral Leads".localized()
        
        navigationItem.title = "Referral Leads".localized()
        
        lblNoData.text = "No Data Found".localized()
        
        btnSeller.alpha = 0
        
        btnSeller.setTitle("Select Company".localized(), for: .normal)
        
        if (isFromHome) {
            
            if progressValue == "0" {
                
                selectedFilter = filterMasterList?.first?.type ?? 0
                
            }
            else  if progressValue == "50" {
                
                selectedFilter = filterMasterList?.first?.type ?? 0
            }
            else  if progressValue == "-1" {
                
                selectedFilter = filterMasterList?.last?.type ?? 0
            }
            
            if filterMasterList?.count ?? 0 > 0 {
                
                for model in filterMasterList ?? []{
                    
                    if progressValue == model.progressValue{
                        
                        selectedFilter = model.type ?? 0
                        
                    }
                    
                }
                
            }
            
            collectionViewFilters.reloadData()
            
            fetchUserListFromHome()
            
        }
        else {
            
            fetchUserList()
            
        }
    }
    
    func didCallApi() {
        
        if (isFromHome) {
            
            fetchUserListFromHome()
            
        }
        else {
            
            fetchUserList()
            
        }
    }
    @IBAction func btnSelectSellerAction(_ sender: UIButton) {
        
        // if let model:ReferalModel = referralLeadList?[indexPath.row] {
        
        let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
        let newProductPage:ReferralSelectSellerVC = storyBoard.instantiateViewController(withIdentifier: "ReferralSelectSellerVC") as! ReferralSelectSellerVC
        newProductPage.delegate = self
        newProductPage.sellerId = sellerId
        
        navigationController?.pushViewController(newProductPage, animated: true)
        
        // }
        
    }
    
    func didSelectedSeller(withSellerCode:[Int]) {
        
        sellerId = withSellerCode
        
        if (isFromHome) {
            
            fetchUserListFromHome()
            
        }
        else {
            
            fetchUserList()
            
        }
        
    }
    
    
    @objc func btnExpandCollapseAction(sender: UIButton){
        
        selectedRowIndex = NSIndexPath(row: sender.tag, section: 0)
        
        if let model:ReferalModel = referralLeadList?[selectedRowIndex!.row] {
            
            if model.isExpand == 1{
                
                model.isExpand = 0
            }
            else{
                
                model.isExpand = 1
            }
            
        }
        
        tableViewList.reloadRows(at: [(selectedRowIndex as! IndexPath)], with: .fade)
        
        
    }
    
    @objc func btnSubmitAction(sender: UIButton){
        
        var selectedRowIndex: NSIndexPath = NSIndexPath(row: sender.tag, section: 0)
        
        let cell: ReferralLeadsNewCell = self.tableViewList.cellForRow(at: selectedRowIndex as IndexPath) as! ReferralLeadsNewCell
        
        if let model:ReferalModel = referralLeadList?[selectedRowIndex.row] {
            
            var dealTxt:Double = 0.0
            
            var currencyId:Int = 0
            
            var currencySymbol:String = ""
            
            var selectedIndex:Int = 0
            
            if let dealAmnt = Double(cell.txtDealValue.text ?? "0.0"){
                
                dealTxt = dealAmnt
            }
            
            for model in currency ?? [] {
                
                if cell.txtDealCurrency.text == model.currencySymbol {
                    
                    currencyId = model.currencyId ?? 0
                    
                    currencySymbol = model.currencySymbol ?? ""
                }
                
            }
            
            if model.referralFeeType == 1 {
                
                if dealTxt <= 0 {
                    cell.txtDealValue.showError(message: "Required".localized())
                    
                    return
                }
                if currencySymbol.count == 0 {
                    
                    cell.txtDealCurrency.showError(message: "Required".localized())
                    
                    return
                }
                
            }
            
            selectedIndex = cell.selectedIndex ?? 0
            
            if cell.selectedIndex == 0 {
                
                selectedIndex = stageList?.first?.stageId ?? 0
                
            }
            
            startAnimatingAfterSubmit()
            
            
            submitStatusDetails(transactionId: model.transactionId ?? 0, sellerCode: model.sellerCode ?? 0, productCode: model.productCode ?? 0, dealValue: dealTxt, dealCurrencyId: currencyId, dealCurrencySymbol: currencySymbol, stageId: selectedIndex, notes: cell.txtViewNotes.text ?? "")
            
            
        }
        
        
        tableViewList.reloadRows(at: [(selectedRowIndex as IndexPath)], with: .fade)
        
        
    }
    
    @objc func btnActions(sender: UIButton){
        
        if let model:ReferalModel = referralLeadList?[sender.tag] {
            
            var titleForCall = "Call to Buyer".localized()
            var titleForMessage = "Message to Buyer".localized()
            
            if model.productType == 1{
                
                titleForMessage = "Message to Jobseeker".localized()
                titleForCall = "Call to Jobseeker".localized()
            }
            
            if model.productType == 2{
                           
                titleForMessage = "Message to Event Member".localized()
                titleForCall = "Call to Event Member".localized()
            }
            
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alert.view.tintColor = UIColor.btnLinkColor
            
            alert.addAction(UIAlertAction(title: titleForCall, style: UIAlertAction.Style.default, handler: { _ in
                
                if model.mobileNumber?.count ?? 0 > 0 {
                    
                    self.initiatePhoneCall(phoneNumber: "\(model.mobileIsd ?? "")\(model.mobileNumber ?? "")")
                    
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: titleForMessage, style: UIAlertAction.Style.default, handler: { _ in
                
                
                //  self.initiateEmail(mailStr:"refer.com")
                
                
                // self.alertSendUpdateLink(selectedTransactionId: model.transactionId ?? 0)
                
                self.showPopUp(selectedTransactionId: model.transactionId ?? 0, title: titleForMessage)
                
                
            }))
            
            if model.productType == 2 {
                
                alert.addAction(UIAlertAction(title: "View Invoice".localized(), style: UIAlertAction.Style.default, handler: { _ in
                              
                              
                              //  self.initiateEmail(mailStr:"refer.com")
                              
                              
                              // self.alertSendUpdateLink(selectedTransactionId: model.transactionId ?? 0)
                              
                    self.openWebView(urlString: model.receiptCdnPath ?? "", title: "Viewer".localized(), subTitle: "")
                              
                              
                          }))
                
                alert.addAction(UIAlertAction(title: "Send Invoice".localized(), style: UIAlertAction.Style.default, handler: { _ in
                              
                              
                              //  self.initiateEmail(mailStr:"refer.com")
                              
                              
                              // self.alertSendUpdateLink(selectedTransactionId: model.transactionId ?? 0)
                              
                    
                    self.sendInvoice(transactionID: model.transactionId ?? 0)
                              
                              
                          }))
                
            }
            
            alert.addAction(UIAlertAction(title: "Cancel".localized(),
                                          style: UIAlertAction.Style.cancel,
                                          handler: {(_: UIAlertAction!) in
                                            
            }))
            
            if let popoverPresentationController = alert.popoverPresentationController {
                popoverPresentationController.sourceView = self.view
                popoverPresentationController.sourceRect = sender.bounds
            }
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    @objc func btnCallAction(sender: UIButton){
        
        if let model:ReferalModel = referralLeadList?[sender.tag] {
            
            if model.mobileNumber?.count ?? 0 > 0 {
                
                self.initiatePhoneCall(phoneNumber: "\(model.mobileIsd ?? "")\(model.mobileNumber ?? "")")
                
            }
        }
        
    }
    
    @objc func btnMailAction(sender: UIButton){
        
        if let model:ReferalModel = referralLeadList?[sender.tag] {
            
            if model.emailId?.count ?? 0 > 0 {
                
                initiateEmail(mailStr: model.emailId ?? "")
                
            }
        }
        
    }
    
    @objc func btnViewResumeAction(_ sender: UIButton){
        
        if let model:ReferalModel = referralLeadList?[sender.tag] {
            
            if model.cvPath?.count ?? 0 > 0 {
                
                openWebView(urlString: model.cvPath ?? "", title: model.cvPath ?? "", subTitle: "")
                
            }
        }
    }
    
    @objc func btnSelectAppointmentAction(sender: UIButton){
        
        if let model:ReferalModel = referralLeadList?[sender.tag] {
           
        let storyBoard = UIStoryboard.init(name: AppointmentSB, bundle: nil)
           
          let appointmentPage:AppointmentViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentViewController") as! AppointmentViewController
           
            appointmentPage.appointmentId = model.appointmentId ?? 0
                
            navigationController?.pushViewController(appointmentPage, animated: true)
                
            
        }
        
    }
    
    func showPopUp(selectedTransactionId:Int,title:String){
        
        let currentWindow: UIWindow? = UIApplication.shared.keyWindow
        
        let message = ReferralMessagePopUp.init(frame: UIScreen.main.bounds)
        
        message.lblNavTitle.text = title
        
        message.btnCancel.setTitle("Cancel".localized(), for: .normal)
        
        message.btnSend.setTitle("Send".localized(), for: .normal)
        
        message.transactionID = selectedTransactionId
        
        message.txtViewMessage.placeholder = "Message".localized()
        
        message.delegate = self
        
        currentWindow?.addSubview(message)
    }
    
    func alertSendUpdateLink(selectedTransactionId:Int) {
        
        let alert = UIAlertController(title: nil, message: "Are you sure to send the message to seller?".localized(), preferredStyle: .alert)
        
        alert.view.tintColor = UIColor.btnLinkColor
        
        alert.addAction(UIAlertAction(title: "No".localized(),
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title: "Yes".localized(), style: UIAlertAction.Style.default, handler: { _ in
            
            // self.sendAlertLink(transactionID: selectedTransactionId, messageText: <#String#>)
            
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func sendAlertLink(transactionID:Int,messageText:String){
        
        let apiClient:APIClient = APIClient()
        
        let params:NSDictionary = ["message":messageText]
        
        var encryptedParams = ["data": encryptParams(params: params)]
        
        weak var weakSelf = self
        startAnimatingAfterSubmit()
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/sendMsgToBuyer?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&transactionId=\(transactionID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            weakSelf?.stopAnimating()
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            if let message:String = dataObj.object(forKey: "message") as? String{
                                
                                weakSelf?.showInfoMessage(message: message)
                                
                            }
                            
                        }
                    }
                }
                
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    func sendInvoice(transactionID:Int){
        
        let apiClient:APIClient = APIClient()
        
        let params:NSDictionary = ["transactionID":transactionID]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        weak var weakSelf = self
        startAnimatingAfterSubmit()
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/sendTransactionInvoice?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            weakSelf?.stopAnimating()
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            if let message:String = dataObj.object(forKey: "message") as? String{
                                
                                weakSelf?.showInfoMessage(message: message)
                                
                            }
                            
                        }
                    }
                }
                
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    func fetchUserList() {
        
        let apiClient:APIClient = APIClient()
        
        var encryptedParams:[String:String]?
        
        if sellerId.count > 0 {
            
            let params:NSDictionary = ["sellerCode": sellerId,"type":selectedFilter]
            
            encryptedParams = ["data": encryptParams(params: params)]
            
        }
        else{
            let params:NSDictionary = ["type":selectedFilter]
            encryptedParams = ["data": encryptParams(params: params)]
        }
        
        // let encryptedParams = ["data": encryptParams(params: params)]
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        
        if !isDataLoaded {
            startAnimating()
            isDataLoaded = true
        }
        else{
            startAnimatingAfterSubmit()
        }
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/referralLeadList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&stage=\(0)&sellerCode=\(sellerCode ?? 0)"), params: encryptedParams! as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ReferalApiModel.self, from: data)
                                
                                if let apiResponse:[ReferalModel] = jsonResponse.referralLeadList {
                                    
                                    weakSelf?.referralLeadList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[StageListMainModel] = jsonResponse.stageList {
                                    
                                    weakSelf?.stageList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[StageListMainModel] = jsonResponse.jobStageList {
                                    
                                    weakSelf?.jobStageList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currency {
                                    
                                    weakSelf?.currency = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.userCount = apiResponse
                                    
                                    weakSelf?.lblCount.text = "\(" Referral Leads ".localized())\("(")\(weakSelf?.userCount ?? 0)\(")")"
                                }
                                
                                
                                if let apiResponse:Int = jsonResponse.isSelectSellerVisible {
                                    
                                    //                                    weakSelf?.isSelectSellerVisible = apiResponse
                                    
                                    if apiResponse == 1 {
                                        
                                        weakSelf?.btnSeller.alpha = 1
                                        
                                    }
                                    else{
                                        
                                        weakSelf?.btnSeller.alpha = 0
                                        
                                    }
                                    
                                    
                                }
                                
                                weakSelf?.tableViewList.reloadData()
                                weakSelf?.tableViewList.scrollsToTop = true
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func fetchUserListFromHome() {
        
        let apiClient:APIClient = APIClient()
        
        var encryptedParams:[String:String]?
        
        var strUrl = ""
        
     if !isFromHomeNew {
        
        if sellerId.count > 0 {
            
            let params:NSDictionary = ["sellerCode": sellerId,"type":selectedFilter,"progressValue":progressValue]
            
            encryptedParams = ["data": encryptParams(params: params)]
            
        }
        else{
            let params:NSDictionary = ["type":selectedFilter,"progressValue":progressValue]
            
            encryptedParams = ["data": encryptParams(params: params)]
        }
        
        strUrl = ("\(ApiBaseUrl)icr/dashboardDetailData?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&stage=\(0)&type=\(type ?? 0)&stageType=\(stageType ?? 0)&startPage=\(startPage)&limit=\(limit)")
        
      }
     else{
        if sellerId.count > 0 {
                  
            let params:NSDictionary = ["sellerCode": sellerId,"type":selectedFilter,"progressValue":Int(progressValue ) ?? 0,"productCode":productCode]
                  
                  encryptedParams = ["data": encryptParams(params: params)]
                  
              }
              else{
            let params:NSDictionary = ["sellerCode":[sellerCode],"type":selectedFilter,"progressValue":Int(progressValue ) ?? 0,"productCode":productCode]
                  
                  encryptedParams = ["data": encryptParams(params: params)]
              }
        
         strUrl = ("\(ApiBaseUrl)icr/dashboardDetailData?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&startPage=\(startPage)&limit=\(limit)")
        }
        
        
        weak var weakSelf = self
        if !isDataLoaded {
            startAnimating()
            isDataLoaded = true
        }
        else{
            startAnimatingAfterSubmit()
        }
        apiClient.PostRequest(urlString: strUrl, params: encryptedParams as NSDictionary?, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ReferalApiModel.self, from: data)
                                
                                if let apiResponse:[ReferalModel] = jsonResponse.referralLeadList {
                                    
                                    weakSelf?.referralLeadList = apiResponse
                                    
                                }
                                if let apiResponse:[StageListMainModel] = jsonResponse.stageList {
                                    
                                    weakSelf?.stageList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[StageListMainModel] = jsonResponse.jobStageList {
                                    
                                    weakSelf?.jobStageList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currency {
                                    
                                    weakSelf?.currency = apiResponse
                                    
                                }
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.userCount = apiResponse
                                    
                                    weakSelf?.lblCount.text = "\(" Referral Leads ".localized())\("(")\(weakSelf?.userCount ?? 0)\(")")"
                                    
                                }
                                
                                if weakSelf?.isFromHome ?? false{
                                    
                                    weakSelf?.tableViewList.reloadData()
                                    
                                    weakSelf?.tableViewList.scrollsToTop = true
                                    
                                }
                                if let apiResponse:Int = jsonResponse.isSelectSellerVisible {
                                    
                                    if apiResponse == 1 {
                                        
                                        weakSelf?.btnSeller.alpha = 1
                                        
                                    }
                                    else{
                                        weakSelf?.btnSeller.alpha = 0
                                    }
                                }
                                
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func submitStatusDetails(transactionId:Int,sellerCode:Int,productCode:Int,dealValue:Double,dealCurrencyId:Int,dealCurrencySymbol:String,stageId:Int,notes:String){
        
        
        startAnimatingAfterSubmit()
        
        // let dealValue = txtDealValue.text ?? "0"
        
        let params:NSDictionary = ["transactionId":transactionId,
                                   "sellerCode": sellerCode ,
                                   "productCode":productCode ,
                                   "dealValue":dealValue,
                                   "dealCurrencyId":dealCurrencyId,
                                   "dealCurrencySymbol":dealCurrencySymbol,
                                   "stageId": stageId,
                                   "notes":notes,
                                   "isSelectAll":isSelectAll
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/updateTransaction?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                // weakSelf?.finalPage(withMessage: message)
                                
                                
                                weakSelf?.stopAnimating()
                                
                                
                                let isUpdated:[String: Int] = ["update": 1]
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callMessage"), object: nil, userInfo: isUpdated)
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
                                //  weakSelf?.navigationController?.popViewController(animated: true)
                                
                                weakSelf?.isUpdated = true
                                
                                weakSelf?.fetchUserList()
                                
                                //  weakSelf?.tableViewList.reloadRows(at: [(self.selectedRowIndex as! IndexPath)], with: .fade)
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                        
                    }
                }
            }
            
            //Show error
            
            weakSelf?.stopAnimating()
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        stopAnimating()
    }
    
    
}

extension ReferalLeadsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: collectionViewFilters.frame.width / 4, height: 40.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return filterMasterList?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
            
            return getCellForReferral(cell: cell, indexPath: indexPath)
            
        }
        
        
        return UICollectionViewCell.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let model:FilterReferralModel = filterMasterList?[indexPath.row] {
            
            selectedFilter = model.type ?? 0
            
            collectionViewFilters.reloadData()
            
            if isFromHome{
                
                fetchUserListFromHome()
                
            }
            else{
                
                fetchUserList()
            }
        }
    }
    
    func getCellForReferral(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:FilterReferralModel = filterMasterList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.title ?? ""
            
            if selectedFilter == model.type {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
}
extension ReferalLeadsVC:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,CurrencySelectorVCDelegate,ReferralLeadsNewCellDelegate{
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        let count = referralLeadList?.count ?? 0
        
        if count > 0 {
            
            tableViewList.alpha = 1
            
            return referralLeadList?.count ?? 0
            
        }
            
        else{
            
            tableViewList.alpha = 0
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let model:ReferalModel = referralLeadList?[indexPath.row] {
            
            if model.workFlowType == 1 {
                
                if let cell:ReferralLeadsNewCell = tableView.dequeueReusableCell(withIdentifier: "ReferralLeadsNewCell", for: indexPath) as? ReferralLeadsNewCell {
                    
                    return getCellForProductList(cell: cell, indexPath: indexPath)
                    
                }
                
            }
            else{
                if let cell:ReferalLeadsCell = tableView.dequeueReusableCell(withIdentifier: "ReferalLeadsCell", for: indexPath) as? ReferalLeadsCell {
                    
                    return getCellForProductListSeller(cell: cell, indexPath: indexPath)
                    
                }
                
            }
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:ReferalModel = referralLeadList?[indexPath.row] {
            
            let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
            let newProductPage:ReferalManagerVC = storyBoard.instantiateViewController(withIdentifier: "ReferalManagerVC") as! ReferalManagerVC
            newProductPage.transactionId = model.transactionId ?? 0
            newProductPage.productCode = model.productCode ?? 0
            newProductPage.sellerCode = model.sellerCode ?? 0
            newProductPage.isSeller = model.isSeller ?? 0
            newProductPage.currency = currency ?? []
            newProductPage.delegate = self
            newProductPage.isEditable = false
            navigationController?.pushViewController(newProductPage, animated: true)
            
        }
        
    }
    
    func getCellForProductList(cell:ReferralLeadsNewCell,indexPath:IndexPath) -> ReferralLeadsNewCell {
        
        cell.selectionStyle = .none
        
        if let model:ReferalModel = referralLeadList?[indexPath.row] {
            
            cell.imgLogo.image = nil
            
            if model.productLogo?.count ?? 0 > 0 {
                
                cell.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
                
            }
            else{
                cell.imgLogo.image = UIImage.init(named: "Logo")
            }
            
            if model.productType == 1 {
                
                cell.imgLogo.image = UIImage.init(named: "jobDefault")
                
            }
            
            if model.productType == 2 {
                           
                cell.imgLogo.image = UIImage.init(named: "eventIcon")
                    
             }
            cell.lblBuyerInfoTitle.text = "Buyer Information".localized()
            
            if model.productType == 1 {
                
                cell.imgLogo.image = UIImage.init(named: "jobDefault")
                cell.lblBuyerInfoTitle.text = "Jobseeker Information".localized()
                
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM hh:mm a"
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.referredDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblTime.text = "\("Referred ".localized())\(" on ".localized())\(dateText)"
            
            if model.referredBy?.count ?? 0 > 0 {
                cell.lblTime.text = "\("Referred by ".localized())\(model.referredBy ?? "")\(" on ".localized())\(dateText)"
            }
            
            cell.lblProductName.text = model.productName
            cell.lblCompanyName.text = model.sellerCompanyName ?? ""
            
            cell.txtDealCurrency.delegate = self
            
            cell.txtDealCurrency.text = model.dealCurrencySymbol ?? ""
            
            if textFieldIndexPath?.row == indexPath.row {
                
                cell.txtDealCurrency.text = selectedCurrencyCode?.currencySymbol
                
            }
            
            if model.dealNotes?.count ?? 0 > 0 {
                
                cell.txtViewNotes.text = model.dealNotes ?? ""
                cell.txtViewNotes.placeholder = ""
                
            }
            
            
            
            cell.txtDealValue.text = "\(model.dealValue ?? 0.0)"
            
            cell.heightExpandableView.constant = 0
            
            cell.viewExpandable.isHidden = true
            
            cell.selectedIndex = model.stageId ?? 0
            
            cell.imgArrow.image = UIImage.init(named: "expandIcon")
            
            cell.imgArrow.setImageColor(color: .btnLinkColor)
            
            cell.btnExpandCollapse.alpha = 0
            cell.imgArrow.alpha = 0
            cell.heightViewExpandCollapse.constant = 0
            cell.viewExpand.alpha = 0
            cell.heightLblNotify.constant = 0
            cell.lblNotifyText.alpha = 0
            
            cell.lblUpdate.attributedText =  NSMutableAttributedString(string:  "\("Update".localized())",attributes: underLineButtonAttributes)
            
            if model.workFlowType == 1 {
                
                cell.btnExpandCollapse.alpha = 1
                cell.imgArrow.alpha = 1
                cell.heightViewExpandCollapse.constant = 40
                cell.viewExpand.alpha = 1
                cell.imgArrow.setImageColor(color: .btnLinkColor)
            }
            
            if model.hideTransForBuyer == 1{
                
                cell.lblNotifyText.text = "Note: Buyer not informed about this mail".localized()
                
                cell.lblNotifyText.alpha = 1
                
                cell.heightLblNotify.constant = 25
            }
            
            
            if model.isExpand == 1 {
                
                cell.heightExpandableView.constant = 290
                cell.viewDealValue.alpha = 0
                cell.heightDealValue.constant = 0
                
                cell.imgArrow.image = UIImage.init(named: "collapseIcon")
                cell.imgArrow.setImageColor(color: .btnLinkColor)
                
                for modelMaster in stageList ?? []{
                    
                    if  cell.selectedIndex == modelMaster.stageId && modelMaster.progressValue ?? 0.0 >= 100 {
                        
                        cell.heightExpandableView.constant = 360
                        cell.heightDealValue.constant = 75
                        cell.viewDealValue.alpha = 1
                        
                    }
                    //                    else{
                    //
                    //                          cell.heightExpandableView.constant = 290
                    //                          cell.viewDealValue.alpha = 0
                    //                          cell.heightDealValue.constant = 0
                    //                    }
                    
                }
                
                cell.viewExpandable.isHidden = false
                
            }
            
            if model.isSeller == 0 {
                
                cell.heightViewExpandCollapse.constant = 0
                cell.viewExpand.alpha = 0
                cell.btnActions.alpha = 0
               // cell.btnChangeSimplified.alpha = 0
            }
            
            cell.btnActions.tag = indexPath.row
            
            cell.stageList = stageList ?? []
            
            if model.productType == 1 {
                
                cell.stageList = jobStageList ?? []
                
            }
            
            
            cell.delegate = self
            
            cell.collectionViewStatus.tag = indexPath.row
            
            cell.collectionViewStatus.reloadData()
            
            cell.btnExpandCollapse.tag = indexPath.row
            
            cell.btnCancel.tag = indexPath.row
            
            cell.btnSubmit.tag = indexPath.row
            
            cell.txtDealCurrency.tag = 11
            
            cell.btnExpandCollapse.addTarget(self, action: #selector(btnExpandCollapseAction(sender:)), for: .touchUpInside)
            
            cell.btnCancel.addTarget(self, action: #selector(btnExpandCollapseAction(sender:)), for: .touchUpInside)
            
            cell.btnSubmit.addTarget(self, action: #selector(btnSubmitAction(sender:)), for: .touchUpInside)
            
            cell.btnActions.addTarget(self, action: #selector(btnActions(sender:)), for: .touchUpInside)
            
            cell.lblPresentStatusTitle.text = "Present Status of Referral".localized()
            
            cell.viewBuyerNumber.alpha = 0
            
            cell.heightViewBuyerNumber.constant = 0
            
            cell.heightViewBuyerInfoBg.constant = 120
            
            cell.heightViewBuyerInfo.constant = 80
            
            cell.lblName.text = model.name ?? ""
            
            cell.btnMail.tag = indexPath.row
            
            cell.btnCall.tag = indexPath.row
            
            
            cell.imgViewCall.setImageColor(color: .btnLinkColor)
            
            cell.imgViewMail.setImageColor(color: .btnLinkColor)
            
            cell.imgViewCV.setImageColor(color: .primaryColor)
            
            cell.imgViewMail.alpha = 0
            
            cell.imgViewCall.alpha = 0
            
            cell.lblMobileNum.alpha = 0
            
            cell.lblEmail.alpha = 0
            
            cell.lblCVText.alpha = 0
            
            cell.imgViewCV.alpha = 0
            
            cell.viewCVText.alpha = 0
            
            cell.heightViewCVText.constant = 0
            
            cell.lblStatus.text = model.stageTitle ?? ""
            
            cell.lblStatus.textColor = UIColor.colorWithHexString(hexString: model.stageColorCode ?? "")
            
            cell.lblStatusTitle.text = "Status".localized()
            
            if model.isSeller == 1 || model.isLeadReferredPartner == 1 {
               
                if model.mobileNumber?.count ?? 0 > 0 {
                
                cell.lblMobileNum.attributedText = NSMutableAttributedString(string:  "\(model.mobileIsd ?? "")\(model.mobileNumber ?? "")",attributes: underLineAttributes)
                
                cell.viewBuyerNumber.alpha = 1
                
                cell.lblMobileNum.alpha = 1
                
                cell.imgViewCall.alpha = 1
                
                cell.heightViewBuyerNumber.constant = 35
                
                cell.heightViewBuyerInfoBg.constant = 140
                
                cell.heightViewBuyerInfo.constant = 110
                
                cell.btnCall.addTarget(self, action: #selector(btnCallAction(sender:)), for: .touchUpInside)
                    
                }
            
            if model.emailId?.count ?? 0 > 0 {
                
                cell.lblEmail.attributedText = NSMutableAttributedString(string: model.emailId ?? "",attributes: underLineAttributes)
                
                cell.lblEmail.alpha = 1
                
                cell.imgViewMail.alpha = 1
                
                cell.viewBuyerNumber.alpha = 1
                
                cell.heightViewBuyerNumber.constant = 35
                
                cell.heightViewBuyerInfoBg.constant = 140
                
                cell.heightViewBuyerInfo.constant = 110
                
                cell.btnMail.addTarget(self, action: #selector(btnMailAction(sender:)), for: .touchUpInside)
                
             }
    
            if model.productType == 1 && model.cvText?.count ?? 0 > 0 {
                
                
                
                cell.lblCVText.attributedText = NSMutableAttributedString(string:  "\(model.cvText ?? "")",attributes: underLineResumeAttributes)
                
                cell.lblCVText.alpha = 1
                
                cell.imgViewCV.alpha = 1
                
                cell.viewCVText.alpha = 1
                
                cell.heightViewCVText.constant = 35
                
                cell.imgViewCV.image = UIImage.init(named: "cvUnSelectIcon")
                
                cell.imgViewCV.setImageColor(color: .primaryColor)
                
                if model.cvPath?.count ?? 0 > 0 {
                    
                    cell.lblCVText.attributedText = NSMutableAttributedString(string:  "\("View resume".localized())",attributes: underLineResumeAttributes)
                    
                    cell.imgViewCV.image = UIImage.init(named: "cvSelectIcon")
                    
                    cell.imgViewCV.setImageColor(color: .primaryColor)
                    
                }
                
                cell.btnViewCV.tag = indexPath.row
                
                cell.btnViewCV.addTarget(self, action: #selector(btnViewResumeAction(_:)), for: .touchUpInside)
                
                if model.mobileNumber?.count ?? 0 > 0 ||  model.emailId?.count ?? 0 > 0 {
                    
                    cell.heightViewBuyerInfoBg.constant = 180
                    cell.heightViewBuyerInfo.constant = 150
                }
                else{
                    
                    cell.heightViewBuyerInfoBg.constant = 140
                    
                    cell.heightViewBuyerInfo.constant = 110
                    
                }
                
            }
            
            }
        }
        
        return cell
    }
    
    func getCellForProductListSeller(cell:ReferalLeadsCell,indexPath:IndexPath) -> ReferalLeadsCell {
        
        cell.selectionStyle = .none
        
        if let model:ReferalModel = referralLeadList?[indexPath.row] {
            
            cell.imgLogo.image = nil
            
            if model.productLogo?.count ?? 0 > 0 {
                
                cell.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
                
            }
            else{
                cell.imgLogo.image = UIImage.init(named: "Logo")
            }
            
            cell.lblBuyerInfoTitle.text = "Buyer Information".localized()
            
            if model.productType == 1 {
                
                cell.imgLogo.image = UIImage.init(named: "jobDefault")
                cell.lblBuyerInfoTitle.text = "Jobseeker Information".localized()
                
            }
            
            if model.productType == 2 {
                                     
               cell.imgLogo.image = UIImage.init(named: "eventIcon")
               cell.lblBuyerInfoTitle.text = "Guest Information".localized()
                
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.referredDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblTime.text = "\("Referred ".localized())\(" on ".localized())\(dateText)"
            
            if model.referredBy?.count ?? 0 > 0 {
                cell.lblTime.text = "\("Referred by ".localized())\(model.referredBy ?? "")\(" on ".localized())\(dateText)"
            }
            
            cell.lblProductName.text = model.productName
            cell.lblCompanyName.text = model.sellerCompanyName ?? ""
            
            cell.lblStatusTitle.text = "Status".localized()
            
            cell.lblStatusValue.text = model.stageTitle ?? ""
            
            cell.lblStatusValue.textColor = UIColor.colorWithHexString(hexString: model.stageColorCode ?? "")
            
            cell.btnActions.tag = indexPath.row
            
            cell.btnMail.tag = indexPath.row
            
            cell.btnCall.tag = indexPath.row
            
            cell.lblName.text = model.name ?? ""
            
            cell.btnActions.addTarget(self, action: #selector(btnActions(sender:)), for: .touchUpInside)
            
            cell.viewBuyerNumber.alpha = 0
            
            cell.heightViewBuyerNumber.constant = 0
            
            cell.heightViewBuyerInfoBg.constant = 120
            
            cell.heightViewBuyerInfo.constant = 80
            
            cell.lblNotifyText.alpha = 0
            
            cell.heightLblNotify.constant = 0
            
            cell.imgViewCall.setImageColor(color: .btnLinkColor)
            
            cell.imgViewMail.setImageColor(color: .btnLinkColor)
            
            cell.imgViewCV.setImageColor(color: .primaryColor)
            
            cell.imgViewMail.alpha = 0
            
            cell.imgViewCall.alpha = 0
            
            cell.lblMobileNum.alpha = 0
            
            cell.lblEmail.alpha = 0
            
            cell.lblCVText.alpha = 0
            
            cell.imgViewCV.alpha = 0
            
            cell.viewCVText.alpha = 0
            
            cell.viewAppointmentTime.alpha = 0
            
            cell.heightViewAppointmentTime.constant = 0
            
            if model.isAppointment == 1 {
              
                cell.viewAppointmentTime.alpha = 1
                           
                cell.heightViewAppointmentTime.constant = 100
                
                var dateAppText = ""
                
                if let date = dateFormatterGet.date(from: model.appointmentDT ?? "") {
                    
                    dateAppText = dateFormatterPrint.string(from: date)
                }
                
                cell.lblDate.text = dateAppText
                
                cell.lblMemberCount.text = "\(model.noOfPeople ?? 0)"
                
               let count = model.resourceCount ?? 0
                
                
                if count > 1{
                  
                    let str1 = NSAttributedString(string: "\(model.resourceTitle ?? "")(\(count - 1) More)", attributes: underLineAttributes)
                                  
                    cell.btnSeeMembers.setAttributedTitle(str1, for: .normal)
                    
                }
                else{
                    
                    let str1 = NSAttributedString(string: "\(model.resourceTitle ?? "")", attributes: underLineAttributes)
                                                     
                    cell.btnSeeMembers.setAttributedTitle(str1, for: .normal)
                }
                
                cell.btnSeeMembers.tag = indexPath.row
                
                cell.btnSeeMembers.addTarget(self, action: #selector(btnSelectAppointmentAction(sender:)), for: .touchUpInside)
            
            }
            
            if model.hideTransForBuyer == 1 {
                
                cell.lblNotifyText.text = "Note: Buyer not informed about this referral".localized()
                
                cell.lblNotifyText.alpha = 1
                
                cell.heightLblNotify.constant = 25
                
            }
            
            cell.btnActions.alpha = 0
            
            if model.isSeller == 1 {
                
                cell.btnActions.alpha = 1
    
            }
            
            if model.isSeller == 1 || model.isLeadReferredPartner == 1{
                
                if model.mobileNumber?.count ?? 0 > 0 {
                    
                    cell.lblMobileNum.attributedText = NSMutableAttributedString(string:  "\(model.mobileIsd ?? "")\(model.mobileNumber ?? "")",attributes: underLineAttributes)
                    
                    cell.viewBuyerNumber.alpha = 1
                    
                    cell.lblMobileNum.alpha = 1
                    
                    cell.imgViewCall.alpha = 1
                    
                    cell.heightViewBuyerNumber.constant = 40
                    
                    cell.heightViewBuyerInfoBg.constant = 140
                    
                    cell.heightViewBuyerInfo.constant = 100
                    
                    cell.btnCall.addTarget(self, action: #selector(btnCallAction(sender:)), for: .touchUpInside)
                    
                }
                
                if model.emailId?.count ?? 0 > 0 {
                    
                    cell.lblEmail.attributedText = NSMutableAttributedString(string: model.emailId ?? "",attributes: underLineAttributes)
                    
                    cell.lblEmail.alpha = 1
                    
                    cell.imgViewMail.alpha = 1
                    
                    cell.viewBuyerNumber.alpha = 1
                    
                    cell.heightViewBuyerNumber.constant = 40
                    
                    cell.heightViewBuyerInfoBg.constant = 140
                    
                    cell.heightViewBuyerInfo.constant = 110
                    
                    cell.btnMail.addTarget(self, action: #selector(btnMailAction(sender:)), for: .touchUpInside)
                    
                }
            }
            
            if model.isSeller == 1 && model.productType == 1 && model.cvText?.count ?? 0 > 0 {
                
                cell.lblCVText.attributedText = NSMutableAttributedString(string:  "\(model.cvText ?? "")",attributes: underLineResumeAttributes)
                
                cell.lblCVText.alpha = 1
                
                cell.imgViewCV.alpha = 1
                
                cell.viewCVText.alpha = 1
                
                cell.heightViewCVText.constant = 35
                
                cell.imgViewCV.image = UIImage.init(named: "cvUnSelectIcon")
                
                cell.imgViewCV.setImageColor(color: .primaryColor)
                
                if model.cvPath?.count ?? 0 > 0 {
                    
                    cell.lblCVText.attributedText = NSMutableAttributedString(string:  "\("View resume".localized())",attributes: underLineResumeAttributes)
                    
                    cell.imgViewCV.image = UIImage.init(named: "cvSelectIcon")
                    
                    cell.imgViewCV.setImageColor(color: .primaryColor)
                    
                }
                
                cell.btnViewCV.tag = indexPath.row
                
                cell.btnViewCV.addTarget(self, action: #selector(btnViewResumeAction(_:)), for: .touchUpInside)
                
                if model.mobileNumber?.count ?? 0 > 0 ||  model.emailId?.count ?? 0 > 0 {
                    
                    cell.heightViewBuyerInfoBg.constant = 180
                    cell.heightViewBuyerInfo.constant = 150
                }
                else{
                    
                    cell.heightViewBuyerInfoBg.constant = 140
                    
                    cell.heightViewBuyerInfo.constant = 110
                    
                }
                
            }
        }
        
        return cell
    }
    
    func didChangedStatus() {
        
        tableViewList.beginUpdates()
        
        tableViewList.endUpdates()
        
    }
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if  textField.tag == 11 {
            
            let pointInTable = textField.convert(textField.bounds.origin, to: self.tableViewList)
            textFieldIndexPath = self.tableViewList.indexPathForRow(at: pointInTable)
            
            openCurrencySelector()
            
            return false
        }
        
        return true
    }
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
        
        tableViewList.reloadData()
        
    }
}

extension ReferralLeadsNewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: collectionViewStatus.frame.width / 4, height: 40.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return stageList?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
            
            return getCellForStatus(cell: cell, indexPath: indexPath)
            
        }
        
        
        return UICollectionViewCell.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let model:StageListMainModel = stageList?[indexPath.row] {
            
            selectedIndex = model.stageId ?? 0
            
            if model.progressValue == 100 {
                
                heightExpandableView.constant = 360
                
                heightDealValue.constant = 75
                
                viewDealValue.alpha = 1
            }
            else{
                
                heightExpandableView.constant = 290
                
                heightDealValue.constant = 0
                
                viewDealValue.alpha = 0
            }
            
            collectionViewStatus.reloadData()
            
            delegate?.didChangedStatus()
            
        }
    }
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:StageListMainModel = stageList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.stageTitle ?? ""
            
            if selectedIndex == model.stageId {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
        }
        return cell
    }
    
}

class ReferalLeadsCell:UITableViewCell {
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblCompanyName: NKLabel!
    
    @IBOutlet weak var lblTime: NKLabel!
    
    @IBOutlet weak var lblStatusValue: NKLabel!
    
    @IBOutlet weak var lblStatusTitle: NKLabel!
    
    @IBOutlet weak var btnActions: UIButton!
    
    @IBOutlet weak var heightViewStatus: NSLayoutConstraint!
    
    @IBOutlet weak var lblNotifyText: NKLabel!
    
    @IBOutlet weak var heightLblNotify: NSLayoutConstraint!
    
    @IBOutlet weak var viewBgBuyerInfo: RCView!
    
    @IBOutlet weak var heightViewBuyerInfoBg: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewBuyerInfo: NSLayoutConstraint!
    
    @IBOutlet weak var viewBuyerName: UIView!
    
    @IBOutlet weak var viewBuyerNumber: UIView!
    
    @IBOutlet weak var heightViewBuyerName: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewBuyerNumber: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewProfile: UIImageView!
    
    @IBOutlet weak var lblName: NKLabel!
    
    @IBOutlet weak var lblMobileNum: NKLabel!
    
    @IBOutlet weak var lblEmail: NKLabel!
    
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var btnMail: UIButton!
    
    @IBOutlet weak var imgViewCall: UIImageView!
    
    @IBOutlet weak var imgViewMail: UIImageView!
    
    @IBOutlet weak var viewCVText: UIView!
    
    @IBOutlet weak var heightViewCVText: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewCV: UIImageView!
    
    @IBOutlet weak var lblCVText: NKLabel!
    
    @IBOutlet weak var btnViewCV:UIButton!
    
    @IBOutlet weak var lblBuyerInfoTitle: NKLabel!
   
    @IBOutlet weak var viewAppointmentTime: UIView!
    
    @IBOutlet weak var heightViewAppointmentTime: NSLayoutConstraint!
    
    @IBOutlet weak var lblDate: NKLabel!
    
    @IBOutlet weak var lblMemberCount: NKLabel!
    
    @IBOutlet weak var btnSeeMembers: UIButton!
    
}

protocol ReferralLeadsNewCellDelegate: class {
    func didChangedStatus()
}

class ReferralLeadsNewCell: UITableViewCell {
    
    @IBOutlet weak var viewBg: RCView!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblCompanyName: NKLabel!
    
    @IBOutlet weak var lblTime: NKLabel!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var viewDealValue: RCView!
    
    @IBOutlet weak var lblDealValueTitle: NKLabel!
    
    @IBOutlet weak var txtDealCurrency: RCTextField!
    
    @IBOutlet weak var txtDealValue: RCTextField!
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
    @IBOutlet weak var heightExpandableView: NSLayoutConstraint!
    
    @IBOutlet weak var viewExpandable: UIView!
    
    @IBOutlet weak var btnExpandCollapse: UIButton!
    
    @IBOutlet weak var btnCancel: NKButton!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var heightDealValue: NSLayoutConstraint!
    
    @IBOutlet weak var btnActions: UIButton!
    
    @IBOutlet weak var imgArrow: UIImageView!
    
    @IBOutlet weak var heightViewExpandCollapse: NSLayoutConstraint!
    
    @IBOutlet weak var viewExpand: UIView!
    
    @IBOutlet weak var lblUpdate: NKLabel!
    
    @IBOutlet weak var viewBgBuyerInfo: RCView!
    
    @IBOutlet weak var heightViewBuyerInfoBg: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewBuyerInfo: NSLayoutConstraint!
    
    @IBOutlet weak var viewBuyerName: UIView!
    
    @IBOutlet weak var viewBuyerNumber: UIView!
    
    @IBOutlet weak var heightViewBuyerName: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewBuyerNumber: NSLayoutConstraint!
    
    @IBOutlet weak var lblName: NKLabel!
    
    @IBOutlet weak var lblMobileNum: NKLabel!
    
    @IBOutlet weak var lblEmail: NKLabel!
    
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var btnMail: UIButton!
    
    @IBOutlet weak var lblPresentStatusTitle: NKLabel!
    
    @IBOutlet weak var lblNotifyText: NKLabel!
    
    @IBOutlet weak var heightLblNotify: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewProfile: UIImageView!
    
    @IBOutlet weak var imgViewCall: UIImageView!
    
    @IBOutlet weak var imgViewMail: UIImageView!
    
    @IBOutlet weak var lblStatusTitle: NKLabel!
    
    @IBOutlet weak var lblStatus: NKLabel!
    
    @IBOutlet weak var viewCVText: UIView!
    
    @IBOutlet weak var heightViewCVText: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewCV: UIImageView!
    
    @IBOutlet weak var lblCVText: NKLabel!
    
    @IBOutlet weak var btnViewCV:UIButton!
    
    @IBOutlet weak var lblBuyerInfoTitle: NKLabel!
    
    var selectedIndex:Int?
    
    var stageList:[StageListMainModel]?
    
    weak var delegate:ReferralLeadsNewCellDelegate?
    
    var progressValue:Int = 0
    
    var selectedStageId:Int = 0
    
    override func awakeFromNib() {
        
        collectionViewStatus.delegate = self
        
        collectionViewStatus.dataSource = self
        
    }
}

extension ReferalLeadsVC:ReferralMessagePopUpDelegate{
    
    func didSendText(transId: Int, text: String) {
        
        sendAlertLink(transactionID: transId, messageText: text)
        
    }
    
}

extension ReferalLeadsVC:ChangeProductVCDelegate{
    
    func didChangeProduct() {
        
        fetchUserListFromHome()
        
    }
    
}
