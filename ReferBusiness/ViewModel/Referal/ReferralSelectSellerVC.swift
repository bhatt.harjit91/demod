//
//  ReferralSelectSellerVC.swift
//  iCanRefer
//
//  Created by TalentMicro on 19/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol ReferralSelectSellerVCDelegate: class {
    
    func didSelectedSeller(withSellerCode:[Int])
}

class ReferralSelectSellerVC: AppBaseViewController {

    @IBOutlet weak var viewSearchBar: RCView!
    
    @IBOutlet weak var searchBarSellers: UISearchBar!
    
    @IBOutlet weak var tableViewSellers: UITableView!
    
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var viewDoneButton: UIView!
    
    @IBOutlet weak var bottomViewDoneBtn: NSLayoutConstraint!
    
    weak var delegate:ReferralSelectSellerVCDelegate?
    
    var selectedIndexPath: NSInteger = -1
    
    var sellerList:[ReferralSellerFilterModel]?
    
    var btnNavDone = UIButton()
    
    var count:Int = 0
    
    var type:Int = 0
    
    var stagetype:Int = 0
    
    var sellerId = [Int]()
    
    var sellerListId = Set<Int>()
    
    var isFirstTime:Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cancelButtonAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.lightGray]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(cancelButtonAttributes, for: .normal)

        
        navigationItem.title = "Select Company".localized()
        
        btnDone.setTitle("Select All".localized(), for: .normal)
        
        btnNavDone.setTitle("Select All".localized(), for: .normal)
        
        
        searchBarSellers.placeholder = "Search Companies...".localized()
        
//        bottomViewDoneBtn.constant = 0
//
//        viewDoneButton.alpha = 0
        
        updateNavBar()
        
      //  btnNavDone.alpha = 0
        
         fetchSellerList(type: type, searchText: "")
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    func updateUI(){
        
//       if sellerId.count == 1 {
//
//        let count = sellerList?.count ?? 0
//
//        for index in 0..<count {
//
//            if sellerList?[index].sellerCode  == sellerId[0]{
//
//                selectedIndexPath  = index
//
//              }
//
//            }
//
//        }
    
         tableViewSellers.reloadData()
    }
    
    func updateNavBar() {
                      
           btnNavDone.setTitle("Select All".localized(), for: .normal)
           btnNavDone.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
           btnNavDone.setTitleColor(UIColor.white, for: .normal)
           btnNavDone.addTarget(self, action: #selector(submit), for: .touchUpInside)
                       
           let rightBarButton = UIBarButtonItem()
           rightBarButton.customView = btnNavDone
           self.navigationItem.rightBarButtonItem = rightBarButton
           
       }
    
 @objc func submit(){
    
    if selectedIndexPath >= 0 {
        
        if let model:ReferralSellerFilterModel = sellerList?[selectedIndexPath]
        {
            
         //  sellerId = model.sellerCode ?? 0
            
            sellerId.removeAll()
            
            sellerId.append(model.sellerCode ?? 0)
            
            delegate?.didSelectedSeller(withSellerCode:sellerId)
            
       }
        
      }
    else{
        
        sellerId.removeAll()
        
        for model in sellerList ?? [] {
            
           
            sellerId.append(model.sellerCode ?? 0)
            
            delegate?.didSelectedSeller(withSellerCode:sellerId)
            
        }
         
      }
    
    navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
                
        submit()

    }
    
    func fetchSellerList(type:Int,searchText:String) {
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        
        if isFirstTime {
           
            startAnimating()
        
        }
        else{
            startAnimatingAfterSubmit()
        }
        
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/referralLeadSellerListFilter?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())&type=\(type)&search=\(searchText)&stagetype=\(stagetype)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
           
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false{
                    
                     if let dataStr:String =  jsonObj?.dataString(){
                        
                    if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                        
                        do {
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            let decoder = JSONDecoder()
                            print(dataObj)
                            
                            let jsonResponse = try decoder.decode(ReferralSellerApiFilterModel.self, from: data)
                            
                           if let apiResponse:[ReferralSellerFilterModel] = jsonResponse.sellerList {
                            
                                weakSelf?.sellerList = apiResponse
                                
                            }
                
                            if let apiResponse:Int = jsonResponse.count {
                                                     
                              weakSelf?.count = apiResponse
                                                      
                             }
                        
                            weakSelf?.stopAnimating()
                            
                            weakSelf?.updateUI()
                            
                           // weakSelf?.tableViewSellers.reloadData()
                            
                            return
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                            
                        }
                      }
                    }
                }
            }
            
            weakSelf?.stopAnimating()
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
}

extension ReferralSelectSellerVC:UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sellerList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:SelectSellerCell = tableView.dequeueReusableCell(withIdentifier: "SelectSellerCell", for: indexPath) as? SelectSellerCell
        {
            
            return getCellForList(cell: cell, indexPath: indexPath)
            
        }
        
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
           tableView.deselectRow(at: indexPath, animated: true)
           
           selectedIndexPath = indexPath.row
        
        btnDone.setTitle("Done".localized(), for: .normal)
                   
        btnNavDone.setTitle("Done".localized(), for: .normal)
                   
        
          tableViewSellers.reloadData()
        
    }
    
    func getCellForList(cell:SelectSellerCell,indexPath:IndexPath) -> SelectSellerCell {
        
        cell.selectionStyle = .none
               
         if indexPath.row == selectedIndexPath {
            
            cell.imgViewCheckBox.alpha = 1
            
            cell.imgViewCheckBox.image = UIImage.init(named: "tickBlue")
            
           // cell.accessoryType = UITableViewCell.AccessoryType.checkmark
            
         }
         else
          {
            cell.imgViewCheckBox.alpha = 0
            
          //  cell.accessoryType = UITableViewCell.AccessoryType.none
            
          }
        
        if let model:ReferralSellerFilterModel = sellerList?[indexPath.row]
        {
            
          if model.sellerCompanyName?.count ?? 0 > 0 {
                
            cell.lblDisplayName.text = model.sellerCompanyName ?? ""
                
           }

            if model.logo?.count ?? 0 > 0 {
                    cell.imgViewReferralConatct.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.logo ?? "")")), completed: nil)
            }
                
         }
        
        return cell
    }
    
}

extension ReferralSelectSellerVC: UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) -> Void {
        
        searchBarSellers.showsCancelButton = true
        
    
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        
        cancelSearch()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        cancelSearch()
        
    }
    
    @objc func cancelSearch(){
       
        searchBarSellers.showsCancelButton = false
        searchBarSellers.resignFirstResponder()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
       searchBarSellers.resignFirstResponder()
        
        
      //  if searchBar.text?.count ?? 0 > 0 {
            
           fetchSellerList(type: type, searchText: searchBar.text ?? "")
        //}
        
    }
    
    
}

class SelectSellerCell: UITableViewCell {
    
    @IBOutlet weak var lblDisplayName: NKLabel!
    
    @IBOutlet weak var imgViewReferralConatct: RCImageView!
    
    @IBOutlet weak var imgViewCheckBox: UIImageView!
    
}
