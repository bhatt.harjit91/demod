//
//  ReferalManagerVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 18/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import Razorpay

protocol ReferalManagerVCDelegate: class {
    func didCallApi()
}
class ReferalManagerVC: SendOTPViewController,ReferralManagerStatusVCDelegate,ReferralPostMessageVCDelegate,CurrencySelectorVCDelegate {
    
    
    ///
    
    ///
    //New Changes..
    
    @IBOutlet weak var viewBgCollectionViewStandard: UIView!
    
    @IBOutlet weak var lblCollectionViewStandardTitle: NKLabel!
    
    @IBOutlet weak var collectionViewStandard: UICollectionView!
    
    @IBOutlet weak var heightOfStandardCollectionViewBG: NSLayoutConstraint!
    
    ///
    
    @IBOutlet weak var viewBgCollectionViewStatus: RCView!
    @IBOutlet weak var heightConstraintOfPostMessage: NSLayoutConstraint!
    
    @IBOutlet weak var heightConstraintOfChangeStatus: NSLayoutConstraint!
    
    @IBOutlet weak var collectionViewAttachment: UICollectionView!
    
    @IBOutlet weak var collectionViewContract: UICollectionView!
    
    @IBOutlet weak var heightViewAttachments: NSLayoutConstraint!
    //
    
    @IBOutlet weak var viewAttachment: RCView!
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var lblProductName: NKLabel!
    
    @IBOutlet weak var lblCompanyName: NKLabel!
    
    @IBOutlet weak var lblPricingInfo: NKLabel!
    
    @IBOutlet weak var lblReferralValue: NKLabel!
    
    @IBOutlet weak var lblGeneralNotes: NKLabel!
    
    
    @IBOutlet weak var lblGeneralNotesTitle: NKLabel!
    
    @IBOutlet weak var lblDealValue: NKLabel!
    
    
    @IBOutlet weak var lblDealValueTitle: NKLabel!
    
    
    @IBOutlet weak var lblPricingInfoTitle: NKLabel!
    
    
    @IBOutlet weak var lblStatus: NKLabel!
    
    @IBOutlet weak var viewStatus: RCView!
    
    @IBOutlet weak var viewSpecialTerms: RCView!
    
    @IBOutlet weak var viewDealValueTitle: RCView!
    
    @IBOutlet weak var viewDealValue: RCView!
    
    
    @IBOutlet weak var lblReferredBy: NKLabel!
    
    @IBOutlet weak var heightPricingInfo: NSLayoutConstraint!
    
    @IBOutlet weak var btnChangeStatus: NKButton!
    
    @IBOutlet weak var viewSpecialTermsTitle: RCView!
    
    @IBOutlet weak var lblNotesToSeller: NKLabel!
    
    @IBOutlet weak var lblNotestToSellerTitle: NKLabel!
    
    @IBOutlet weak var heightViewSpecialTerms: NSLayoutConstraint!
    
    @IBOutlet weak var heightDealValue: NSLayoutConstraint!
    
    @IBOutlet weak var heightValueDeal: NSLayoutConstraint!
    
    @IBOutlet weak var txtViewNotes: NKTextView!
    
    @IBOutlet weak var lblStatusViewTitle: NKLabel!
    
    @IBOutlet weak var lblDealDateTitle: NKLabel!
    
    @IBOutlet weak var lblDealValueNewTitle: NKLabel!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var txtDealCurrencySymbol: RCTextField!
    
    @IBOutlet weak var txtDealValue: RCTextField!
    
    @IBOutlet weak var txtDealDate: RCTextField!
    
    
    @IBOutlet weak var heightViewUpdateDealValue: NSLayoutConstraint!
    
    @IBOutlet weak var viewUpdateDealValue: RCView!
    
    @IBOutlet weak var heightViewDealDate: NSLayoutConstraint!
    
    @IBOutlet weak var viewDealDate: RCView!
    
    @IBOutlet weak var viewContract: RCView!
    
    @IBOutlet weak var heightViewContract: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewAttachContract: UIImageView!
    
    @IBOutlet weak var heightViewEditable: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewUpdateStatus: NSLayoutConstraint!
    
    @IBOutlet weak var btnViewChangeLog: UIButton!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var viewReadOnly: RCView!
    
    @IBOutlet weak var viewPriceInfoTitle: RCView!
    
    @IBOutlet weak var heightViewPriceInfo: NSLayoutConstraint!
    
    @IBOutlet weak var viewEditable: UIView!
    
    @IBOutlet weak var heightViewNotes: NSLayoutConstraint!
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnPrev: UIButton!
    
    @IBOutlet weak var collectionViewSimplifiedStatus: UICollectionView!
    
    @IBOutlet weak var viewSimplifiedStatus: UIView!
    
    @IBOutlet weak var heightViewSimplifiedStatus: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewBgBuyerInfo: RCView!
    
    @IBOutlet weak var heightViewBuyerInfoBg: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewBuyerInfo: NSLayoutConstraint!
    
    @IBOutlet weak var viewBuyerName: UIView!
    
    @IBOutlet weak var viewBuyerNumber: UIView!
    
    @IBOutlet weak var viewNotesToSeller: UIView!
    
    @IBOutlet weak var viewGeneralNotes: UIView!
    
    
    @IBOutlet weak var heightViewBuyerName: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewBuyerNumber: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewNotesToSeller: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewgeneralNotes: NSLayoutConstraint!
    
    @IBOutlet weak var lblName: NKLabel!
    
    @IBOutlet weak var lblMobileNum: NKLabel!
    
    @IBOutlet weak var lblEmail: NKLabel!
    
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var btnMail: UIButton!
    
    @IBOutlet weak var lblNotesToSellerTitle: NKLabel!
    
    @IBOutlet weak var lblNotesValue: NKLabel!
    
    @IBOutlet weak var lblGeneralNotesTitleText: NKLabel!
    
    @IBOutlet weak var lblGeneralNotesValue: NKLabel!
    
    @IBOutlet weak var imgViewCall: UIImageView!
    
    @IBOutlet weak var imgViewMail: UIImageView!
    
    @IBOutlet weak var imgViewProfile: UIImageView!
    
    @IBOutlet weak var viewReferralPayOffBg: RCView!
    
    @IBOutlet weak var heightViewReferralPayOffBg: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewReferralPayOff: NSLayoutConstraint!
    
    @IBOutlet weak var heightTableViewPayoff: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewReferralView: UITableView!
    
    @IBOutlet weak var viewCVText: UIView!
    
    @IBOutlet weak var heightViewCVText: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewCV: UIImageView!
    
    @IBOutlet weak var lblCVText: NKLabel!
    
    @IBOutlet weak var btnViewCV:UIButton!
    
    @IBOutlet weak var lblNotifyText: NKLabel!
    
    @IBOutlet weak var heightLblNotify: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewCompanyName: NSLayoutConstraint!
    
    @IBOutlet weak var lblBuyerInfoTitle: NKLabel!
    
    @IBOutlet weak var lblCopyOfContract: NKLabel!
    
    @IBOutlet weak var btnChangeProduct: NKButton!
    
    @IBOutlet weak var viewPaymentStatus: RCView!
    
    @IBOutlet weak var lblPaymentTypeTitle: NKLabel!
    
    @IBOutlet weak var lblPaymentTypeValue: NKLabel!
    
    @IBOutlet weak var lblAmountTitle: NKLabel!
    
    @IBOutlet weak var lblAmountValue: NKLabel!
    
    @IBOutlet weak var heightViewPaymentStatus: NSLayoutConstraint!
    
    @IBOutlet weak var segmentPaymentStatus: UISegmentedControl!
    
    @IBOutlet weak var viewEventDateTime: UIView!
    
    @IBOutlet weak var heightEventDateTime: NSLayoutConstraint!
    
    @IBOutlet weak var lblEventDateTimeTitle: NKLabel!
    
    @IBOutlet weak var lblEventDateTime: NKLabel!
    
    @IBOutlet weak var btnDeclare: NKButton!
    
    @IBOutlet weak var heightDeclarationView: NSLayoutConstraint!
    
    @IBOutlet weak var viewDeclare: UIView!
    
    @IBOutlet weak var lblViewInvoice: UILabel!
    
    @IBOutlet weak var lblSendInvoice: UILabel!
    
    @IBOutlet weak var viewInvoice: UIView!
    
    @IBOutlet weak var imgViewEyeIcon: UIImageView!
    
    @IBOutlet weak var imgSendInvoice: UIImageView!
    
    @IBOutlet weak var viewAppointmentTime: ScheduledAppointmentView!
    
    @IBOutlet weak var heightViewAppointmentTime: NSLayoutConstraint!
    
    @IBOutlet weak var lblDate: NKLabel!
    
    @IBOutlet weak var lblMemberCount: NKLabel!
    
    @IBOutlet weak var btnSeeMembers: UIButton!
    
    var referralDetailModel:ReferralDetailModel?
    
    var transactionId:Int?
    
    var isEditable:Bool = false
    
    var branchCode:Int?
    
    var sellerCode:Int?
    
    var productCode:Int?
    
    var comingFromArchive:Bool = false
    
    weak var delegate:ReferalManagerVCDelegate?
    
    var selectedCurrencyCode : CurrencyModel?
    
    var currency:[CurrencyModel]?
    
    var selectedIndex:Int = 0
    
    var masterStageList:[StageListMainModel]?
    
    var attachments:[Any] = []
    
    var changeLog:[LogModel]?
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    var isSeller:Int?
    
    let countAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let underLineAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 4),
        .foregroundColor: UIColor.getGradientColor(index: 3).first ?? 0,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    let underLineResumeAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.fontForType(textType: 3),
        .foregroundColor: UIColor.primaryColor,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    var paymentDetails:PaymentModel?
    var billingModel:billingUserModel?
    
    var paymentController = InitialViewController()
    
    private var razorpay:Razorpay?
    
    var paymentStatusCode:Int?
    
    var currencyList:[CurrencyModel]?
    var paymentTypeList:[PaymentTypeListModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewLoad()
    }
    
    func viewLoad(){
        
        btnSubmit.setTitle("Submit".localized(), for: .normal)
        
        btnChangeProduct.addTarget(self, action: #selector(btnChangeAction(_:)), for: .touchUpInside)
        
        viewDealValue.alpha = 0
        viewDealValueTitle.alpha = 0
        //  heightPricingInfo.constant = 180
        
        lblGeneralNotes.alpha = 0
        
        lblGeneralNotesTitle.alpha = 0
        
        lblNotesToSeller.alpha = 0
        
        lblNotestToSellerTitle.alpha = 0
        
        heightDealValue.constant = 0
        
        heightValueDeal.constant = 0
        
        heightViewSpecialTerms.constant = 0
        
        viewSpecialTerms.alpha = 0
        
        navigationItem.title = "Referral Leads".localized()
        
        heightViewAttachments.constant = 0
        
        viewAttachment.alpha = 0
        
        fetchUserDetail()
        
        lblGeneralNotesTitle.text = "General Notes".localized()
        
        lblNotesToSeller.text = "Notes to Seller".localized()
        
        txtDealCurrencySymbol.delegate = self
        
        heightConstraintOfChangeStatus.constant = 0
        
        btnChangeStatus.alpha = 0
        
        imgViewAttachContract.setImageColor(color: .white)
        
        let btnViewChageLogTitle = NSMutableAttributedString(string: "\("View change log".localized())",
            attributes: countAttributes)
        
        btnViewChangeLog.setAttributedTitle(btnViewChageLogTitle, for: .normal)
        
        lblViewInvoice.attributedText = NSAttributedString(string: "View Invoice".localized(), attributes: countAttributes)
        
        lblSendInvoice.attributedText = NSAttributedString(string: "Send Invoice".localized(), attributes: countAttributes)
        
        imgViewEyeIcon.setImageColor(color: .btnLinkColor)
        
        imgSendInvoice.setImageColor(color: .btnLinkColor)
        
        //  heightViewUpdateStatus.constant = 180
        heightViewContract.constant = 0
        heightViewDealDate.constant = 0
        heightViewUpdateDealValue.constant = 40
        txtDealValue.alpha = 0
        viewUpdateDealValue.alpha = 1
        viewDealDate.alpha = 0
        viewContract.alpha = 0
        lblDealValueNewTitle.alpha = 0
        txtDealValue.alpha = 0
        txtDealCurrencySymbol.alpha = 0
        btnSubmit.alpha = 0
        heightConstraintOfPostMessage.constant = 0
        btnPrev.isUserInteractionEnabled = false
        btnNext.isUserInteractionEnabled = false
        collectionViewStatus.isUserInteractionEnabled = false
        collectionViewStandard.isUserInteractionEnabled = false
        txtViewNotes.isUserInteractionEnabled = false
        txtDealValue.isUserInteractionEnabled = false
        txtDealDate.isUserInteractionEnabled = false
        txtDealCurrencySymbol.isUserInteractionEnabled = false
        viewPaymentStatus.alpha = 0
        heightViewPaymentStatus.constant = 0
        
        if isSeller == 1 && isEditable == false {
            btnSubmit.alpha = 1
            heightConstraintOfPostMessage.constant = 44
            btnPrev.isUserInteractionEnabled = true
            btnNext.isUserInteractionEnabled = true
            collectionViewStatus.isUserInteractionEnabled = true
            collectionViewStandard.isUserInteractionEnabled = true
            txtViewNotes.isUserInteractionEnabled = true
            txtDealValue.isUserInteractionEnabled = true
            txtDealDate.isUserInteractionEnabled = true
            txtDealCurrencySymbol.isUserInteractionEnabled = true
            btnCall.addTarget(self, action: #selector(btnCallAction(sender:)), for: .touchUpInside)
            btnMail.addTarget(self, action: #selector(btnMailAction(sender:)), for: .touchUpInside)
        }
    }
    
    @objc func btnCallAction(sender: UIButton){
        
        if let model:ReferralDetailModel = referralDetailModel {
            
            if model.mobileNumber?.count ?? 0 > 0 {
                
                self.initiatePhoneCall(phoneNumber: "\(model.mobileIsd ?? "")\(model.mobileNumber ?? "")")
                
            }
        }
        
    }
    
    @objc func btnMailAction(sender: UIButton){
        
        if let model:ReferralDetailModel = referralDetailModel {
            
            if model.emailId?.count ?? 0 > 0 {
                
                initiateEmail(mailStr: model.emailId ?? "")
                
            }
        }
        
    }
    
    @objc func btnPayAction(sender: UIButton){
        
        // if let model:ReferralDetailModel = referralDetailModel {
        
        getGcDetails()
        
        //}
    }
    
    
    @IBAction func btnDeclareAction(_ sender: Any) {
        
        fetchInvoiceList()
        
    }
    
    func openPaymentDeclarationPage(){
        
        let storyBoard = UIStoryboard.init(name: "Invoices", bundle: nil)
        let transferPaymentPage:TransferPaymentViewController = storyBoard.instantiateViewController(withIdentifier: "TransferPaymentViewController") as! TransferPaymentViewController
        
        transferPaymentPage.currency = currencyList ?? []
        transferPaymentPage.delegate = self
        transferPaymentPage.paymentTypeList = paymentTypeList ?? []
        //transferPaymentPage.sellerCode = sellerCode ?? 0
        transferPaymentPage.transactionId = referralDetailModel?.transactionId ?? 0
        transferPaymentPage.isFromLeads = true
        navigationController?.pushViewController(transferPaymentPage, animated: true)
        
    }
    
    @IBAction func btnPreviousStandardCollectionView(_ sender: Any) {
        
        let visibleItems: NSArray = self.collectionViewStandard.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let prevItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        if prevItem.row < masterStageList?.count ?? 0 && prevItem.row >= 0{
            
            if let modelMain:ReferralDetailModel = referralDetailModel {
                
                if let model:StageListMainModel = masterStageList?[prevItem.row ] {
                    
                    selectedIndex = model.stageId ?? 0
                    
                    if modelMain.productType != 2 {
                        
                        if model.progressValue == 100 {
                            
                            heightViewEditable.constant = 550
                            heightViewUpdateDealValue.constant = 80
                            heightViewContract.constant = 150
                            heightViewDealDate.constant = 80
                            viewUpdateDealValue.alpha = 1
                            viewDealDate.alpha = 1
                            viewContract.alpha = 1
                            lblDealValueNewTitle.alpha = 1
                            txtDealValue.alpha = 1
                            txtDealCurrencySymbol.alpha = 1
                            viewPaymentStatus.alpha = 0
                            heightViewPaymentStatus.constant = 0
                            
                        }
                            
                        else{
                            heightViewEditable.constant = 180
                            heightViewContract.constant = 0
                            heightViewDealDate.constant = 0
                            heightViewUpdateDealValue.constant = 40
                            viewUpdateDealValue.alpha = 1
                            viewDealDate.alpha = 0
                            viewContract.alpha = 0
                            txtDealValue.alpha = 0
                            txtDealCurrencySymbol.alpha = 0
                            lblDealValueNewTitle.alpha = 0
                            viewPaymentStatus.alpha = 0
                            heightViewPaymentStatus.constant = 0
                        }
                    }
                        
                    else{
                        
                        //                        if model.progressValue == 100 {
                        //
                        //                            heightViewEditable.constant = 550
                        //                            heightViewUpdateDealValue.constant = 80
                        //                            heightViewContract.constant = 150
                        //                            heightViewDealDate.constant = 80
                        //                            viewUpdateDealValue.alpha = 1
                        //                            viewDealDate.alpha = 1
                        //                            viewContract.alpha = 1
                        //                            lblDealValueNewTitle.alpha = 1
                        //                            txtDealValue.alpha = 1
                        //                            txtDealCurrencySymbol.alpha = 1
                        //                            viewPaymentStatus.alpha = 0
                        //                            heightViewPaymentStatus.constant = 0
                        //                        }
                        //
                        //                        else{
                        //                            heightViewEditable.constant = 180
                        //                            heightViewContract.constant = 0
                        //                            heightViewDealDate.constant = 0
                        //                            heightViewUpdateDealValue.constant = 40
                        //                            viewUpdateDealValue.alpha = 1
                        //                            viewDealDate.alpha = 0
                        //                            viewContract.alpha = 0
                        //                            txtDealValue.alpha = 0
                        //                            txtDealCurrencySymbol.alpha = 0
                        //                            lblDealValueNewTitle.alpha = 0
                        //                            viewPaymentStatus.alpha = 1
                        //                            heightViewPaymentStatus.constant = 235
                        //                        }
                        
                        heightViewEditable.constant = 410
                        heightViewPaymentStatus.constant = 295
                        heightDeclarationView.constant = 60
                        viewInvoice.alpha = 0
                        viewDeclare.alpha = 1
                        if referralDetailModel?.paymentProgressValue == 100 {
                            
                            heightViewEditable.constant = 350
                            heightViewPaymentStatus.constant = 235
                            heightDeclarationView.constant = 0
                            viewDeclare.alpha = 0
                            if referralDetailModel?.receiptCdnPath?.count ?? 0 > 0 {
                                heightViewEditable.constant = 410
                                heightViewPaymentStatus.constant = 295
                                viewInvoice.alpha = 1
                            }
                        }
                        heightViewContract.constant = 0
                        heightViewDealDate.constant = 0
                        heightViewUpdateDealValue.constant = 40
                        viewUpdateDealValue.alpha = 1
                        viewDealDate.alpha = 0
                        viewContract.alpha = 0
                        txtDealValue.alpha = 0
                        txtDealCurrencySymbol.alpha = 0
                        lblDealValueNewTitle.alpha = 0
                        viewPaymentStatus.alpha = 1
                        // heightViewPaymentStatus.constant = 235
                        
                    }
                    
                }
                
            }
            self.collectionViewStandard.scrollToItem(at: prevItem, at: .right, animated: true)
            
        }
    }
    
    @IBAction func btnNextStandardCollectionView(_ sender: Any) {
        
        let visibleItems: NSArray = self.collectionViewStandard.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        if nextItem.row < masterStageList?.count ?? 0 {
            
            if let modelMain:ReferralDetailModel = referralDetailModel {
                
                if let model:StageListMainModel = masterStageList?[nextItem.row ] {
                    
                    selectedIndex = model.stageId ?? 0
                    
                    if modelMain.productType != 2 {
                        
                        if model.progressValue == 100 {
                            
                            heightViewEditable.constant = 550
                            heightViewUpdateDealValue.constant = 80
                            heightViewContract.constant = 150
                            heightViewDealDate.constant = 80
                            viewUpdateDealValue.alpha = 1
                            viewDealDate.alpha = 1
                            viewContract.alpha = 1
                            lblDealValueNewTitle.alpha = 1
                            txtDealValue.alpha = 1
                            txtDealCurrencySymbol.alpha = 1
                            viewPaymentStatus.alpha = 0
                            heightViewPaymentStatus.constant = 0
                        }
                            
                        else{
                            heightViewEditable.constant = 180
                            heightViewContract.constant = 0
                            heightViewDealDate.constant = 0
                            heightViewUpdateDealValue.constant = 40
                            viewUpdateDealValue.alpha = 1
                            viewDealDate.alpha = 0
                            viewContract.alpha = 0
                            txtDealValue.alpha = 0
                            txtDealCurrencySymbol.alpha = 0
                            lblDealValueNewTitle.alpha = 0
                            viewPaymentStatus.alpha = 0
                            heightViewPaymentStatus.constant = 0
                        }
                        
                    }
                    else{
                        //                        if model.progressValue == 100 {
                        //
                        //                            heightViewEditable.constant = 550
                        //                            heightViewUpdateDealValue.constant = 80
                        //                            heightViewContract.constant = 150
                        //                            heightViewDealDate.constant = 80
                        //                            viewUpdateDealValue.alpha = 1
                        //                            viewDealDate.alpha = 1
                        //                            viewContract.alpha = 1
                        //                            lblDealValueNewTitle.alpha = 1
                        //                            txtDealValue.alpha = 1
                        //                            txtDealCurrencySymbol.alpha = 1
                        //                            viewPaymentStatus.alpha = 0
                        //                            heightViewPaymentStatus.constant = 0
                        //                        }
                        //
                        //                        else{
                        //                            heightViewEditable.constant = 180
                        //                            heightViewContract.constant = 0
                        //                            heightViewDealDate.constant = 0
                        //                            heightViewUpdateDealValue.constant = 40
                        //                            viewUpdateDealValue.alpha = 1
                        //                            viewDealDate.alpha = 0
                        //                            viewContract.alpha = 0
                        //                            txtDealValue.alpha = 0
                        //                            txtDealCurrencySymbol.alpha = 0
                        //                            lblDealValueNewTitle.alpha = 0
                        //                            viewPaymentStatus.alpha = 1
                        //                            heightViewPaymentStatus.constant = 235
                        //
                        //                        }
                        
                        heightViewEditable.constant = 410
                        heightViewPaymentStatus.constant = 295
                        heightDeclarationView.constant = 60
                        viewDeclare.alpha = 1
                        viewInvoice.alpha = 0
                        
                        if referralDetailModel?.paymentProgressValue == 100 {
                            
                            //                            heightViewEditable.constant = 350
                            //                            heightViewPaymentStatus.constant = 235
                            heightDeclarationView.constant = 0
                            viewDeclare.alpha = 0
                            viewInvoice.alpha = 1
                        }
                        heightViewContract.constant = 0
                        heightViewDealDate.constant = 0
                        heightViewUpdateDealValue.constant = 40
                        viewUpdateDealValue.alpha = 1
                        viewDealDate.alpha = 0
                        viewContract.alpha = 0
                        txtDealValue.alpha = 0
                        txtDealCurrencySymbol.alpha = 0
                        lblDealValueNewTitle.alpha = 0
                        viewPaymentStatus.alpha = 1
                        //heightViewPaymentStatus.constant = 235
                        
                    }
                    
                    self.collectionViewStandard.scrollToItem(at: nextItem, at: .left, animated: true)
                }
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isEditable == false {
            
            
            //  heightViewEditable.constant = 0
            //   viewEditable.alpha = 0
            //   btnChangeStatus.setTitle("View Status".localized(), for: .normal)
            
        }
        else {
            //  viewReadOnly.alpha = 0
            //   heightViewPriceInfo.constant = 0
            
            
            
            if comingFromArchive == true
            {
                heightConstraintOfChangeStatus.constant = 0
            }
            
            // btnChangeStatus.setTitle("Change Status".localized(), for: .normal)
            
        }
        
    }
    
    func didUpdatedStatus() {
        
        fetchUserDetail()
        
        delegate?.didCallApi()
        
    }
    
    func didPostedMessage() {
        
        fetchUserDetail()
        delegate?.didCallApi()
    }
    
    @IBAction func btnAttachContract(_ sender: Any) {
        
        btnAttachmentAction()
        
    }
    
    @IBAction func btnViewChangeLog(_ sender: Any) {
        
        if  let changeLogPage:ReferralChangeLogVC = UIStoryboard.init(name: ReferalSB, bundle: nil).instantiateViewController(withIdentifier: "ReferralChangeLogVC") as? ReferralChangeLogVC{
            changeLogPage.changeLog = changeLog
            
            navigationController?.pushViewController(changeLogPage, animated: true)
            
        }
        
    }
    
    @IBAction func btnViewInvoiceAction(_ sender: Any) {
        
        openWebView(urlString: referralDetailModel?.receiptCdnPath ?? "", title: "Viewer".localized(), subTitle: "")
    }
    
    @IBAction func btnSendInvoiceAction(_ sender: Any) {
        
        sendInvoice(transactionID: referralDetailModel?.transactionId ?? 0)
    }
    
    @objc func btnViewResumeAction(_ sender: UIButton){
        
        if let model:ReferralDetailModel = referralDetailModel {
            
            if model.cvPath?.count ?? 0 > 0 {
                
                openWebView(urlString: model.cvPath ?? "", title: model.cvPath ?? "", subTitle: "")
                
            }
        }
    }
    
    func btnAttachmentAction() {
        //  startAnimating()
        
        isAttachment = true
        isDocument = true
        openImagePicker()
        
        weak var weakSelf = self
        
        didUploadHelpDeskImage = {status, urlString, imageName in
            //   weakSelf?.stopAnimating()
            if status {
                print(urlString, imageName)
                var arrayEachImage : [String : String] = [:]
                arrayEachImage["cdnPath"] = urlString
                arrayEachImage["fileName"] = imageName
                weakSelf?.attachments.append(arrayEachImage)
                weakSelf?.collectionViewContract.reloadData()
                self.isAttachment = false
            }
            
        }
        
    }
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachments.count > sender.tag {
            attachments.remove(at: sender.tag)
            
        }
    }
    
    
    func updateUIForReadOnly(){
        
        heightViewEditable.constant = 0
        viewEditable.alpha = 0
        btnSubmit.alpha = 0
        heightConstraintOfPostMessage.constant = 0
        
        var viewHeight:CGFloat = 150.0
        
        if let model:ReferralDetailModel = referralDetailModel {
            
            lblProductName.text = model.productName ?? ""
            lblCompanyName.text = model.sellerCompanyName ?? ""
            
            if model.productLogo?.count ?? 0 > 0 {
                imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = TimeZone.current
            var referredDateText = ""
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.referredDate ?? "") {
                
                referredDateText = dateFormatterPrint.string(from: date)
            }
            
            collectionViewStandard.reloadData()
            
            lblPricingInfo.text = "Pricing Information".localized()
            lblReferralValue.text = "\(model.referralFee ?? "")"
            
            //            lblReferralScheme.text = "Referral Scheme".localized()
            
            heightViewSpecialTerms.constant = 0
            heightViewNotes.constant = 0
            
            if referralDetailModel?.referralNoteToSeller?.count ?? 0 > 0{
                lblNotesToSeller.alpha = 1
                lblNotestToSellerTitle.alpha = 1
                heightViewSpecialTerms.constant = 25
                viewHeight = viewHeight + 40
                lblNotesToSeller.text = referralDetailModel?.referralNoteToSeller ?? ""
                viewSpecialTerms.alpha = 1
            }
            
            if model.dealNotes?.count ?? 0 > 0 {
                
                txtViewNotes.placeholder = ""
                txtViewNotes.text = model.dealNotes ?? ""
                viewHeight = viewHeight + 40
                
            }
            
            //            lblReferredDate.text = referralDetailModel?.referredDate ?? ""
            
            if let date = dateFormatterGet.date(from: model.dealDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            heightDealValue.constant = 0
            
            heightValueDeal.constant = 0
            
            viewDealValue.alpha = 0
            
            if model.dealValue ?? 0.0  > 0.0 {
                
                viewDealValue.alpha = 1
                
                viewDealValueTitle.alpha = 1
                
                lblDealValue.text = "\("Deal Value".localized())\(":")\(model.dealCurrencySymbol ?? "")\(" ")\(model.dealValue ?? 0.0)\(" on ")\(dateText)"
                
                heightDealValue.constant = 25
                
                heightValueDeal.constant = 25
                
                
                viewHeight = viewHeight + 80
                // heightPricingInfo.constant = 240
                
            }
            
            lblStatus.text = model.stageTitle ?? ""
            
            if referralDetailModel?.specialTerms?.count ?? 0 > 0{
                
                lblGeneralNotes.alpha = 1
                
                lblGeneralNotesTitle.alpha = 1
                
                lblGeneralNotes.text = referralDetailModel?.specialTerms ?? ""
                
                
                heightViewNotes.constant = 40
                
                viewHeight = viewHeight + 40
                
            }
            
            transactionId = model.transactionId ?? 0
            
            //            if model.attachmentList?.count ?? 0 > 0 {
            //
            //                heightViewAttachments.constant = 210
            //
            //                viewAttachment.alpha = 1
            //
            //                collectionViewAttachment.reloadData()
            //            }
            
            heightPricingInfo.constant = viewHeight
            
            updateBuyerInfoView()
            updateTopCompanyView()
        }
        
        if comingFromArchive == true
        {
            heightConstraintOfPostMessage.constant = 0
        }
        
    }
    
    @objc func btnSelectAppointmentAction(sender: UIButton){
        
        if let model:ReferralDetailModel = referralDetailModel {
           
        let storyBoard = UIStoryboard.init(name: AppointmentSB, bundle: nil)
           
          let appointmentPage:AppointmentViewController = storyBoard.instantiateViewController(withIdentifier: "AppointmentViewController") as! AppointmentViewController
           
            appointmentPage.appointmentId = model.appointmentId ?? 0
                
            navigationController?.pushViewController(appointmentPage, animated: true)
                
            
        }
        
    }
    
    func updateUIForEditableView(){
        
        updateBuyerInfoView()
        updateTopCompanyView()
        
        viewReadOnly.alpha = 0
        heightPricingInfo.constant = 0
        heightViewPriceInfo.constant = 0
        viewPriceInfoTitle.alpha = 0
        // btnSubmit.alpha = 0
        //  heightConstraintOfPostMessage.constant = 0
        
        if let model:ReferralDetailModel = referralDetailModel {
            
            lblPricingInfo.text = "Pricing Information".localized()
            lblReferralValue.text = "\(model.referralFee ?? "")"
            
            viewAppointmentTime.alpha = 0
            heightViewAppointmentTime.constant = 0
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = TimeZone.current
            
            if model.isAppointment == 1 {
                viewAppointmentTime.alpha = 1
                heightViewAppointmentTime.constant = 100
                
                var dateAppText = ""
                            
                if let date = dateFormatterGet.date(from: model.appointmentDT ?? "") {
                                
                    dateAppText = dateFormatterPrint.string(from: date)
                }
                
                lblDate.text = dateAppText
                
                lblMemberCount.text = "\(model.noOfPeople ?? 0)"
                
                
                let count = model.resourceCount ?? 0
                
                
                if count > 1{
                    
                    let str1 = NSAttributedString(string: "\(model.resourceTitle ?? "")(\(count - 1) More)", attributes: underLineAttributes)
                    
                  btnSeeMembers.setAttributedTitle(str1, for: .normal)
                    
                }
                else{
                    
                    let str1 = NSAttributedString(string: "\(model.resourceTitle ?? "")", attributes: underLineAttributes)
                    
                    btnSeeMembers.setAttributedTitle(str1, for: .normal)
                }
                
                btnSeeMembers.addTarget(self, action: #selector(btnSelectAppointmentAction(sender:)), for: .touchUpInside)
                
            }
            
            
            if model.dealNotes?.count ?? 0 > 0 {
                
                txtViewNotes.placeholder = ""
                txtViewNotes.text = model.dealNotes ?? ""
                
            }
            
            lblPaymentTypeTitle.text = "Payment Type".localized()
            lblAmountTitle.text = "Amount".localized()
            
            if model.productType == 2 {
                
                let str = NSMutableString()
                
                if model.paymentType?.isFreeOfCost == 1 {
                    
                    str.append("Free".localized())
                }
                if model.paymentType?.isFreeOfCost == 1 && (model.paymentType?.isPayonRegistration == 1 || model.paymentType?.isPayAtEvent == 1  ) {
                    
                    str.append(", ".localized())
                    
                }
                if model.paymentType?.isPayonRegistration == 1 {
                    
                    str.append("Pay on registration".localized())
                }
                
                if model.paymentType?.isPayonRegistration == 1 && model.paymentType?.isPayAtEvent == 1 {
                    
                    str.append(", ".localized())
                    
                }
                
                if model.paymentType?.isPayAtEvent == 1 {
                    
                    str.append("Pay at event".localized())
                }
                
                lblPaymentTypeValue.text = str as String
                lblAmountValue.text = "\(model.productPrice ?? "")"
                
                paymentStatusCode = model.paymentStatusCode ?? 0
                
                if paymentStatusCode == 1 {
                    
                    segmentPaymentStatus.selectedSegmentIndex = 0
                    
                }
                else if paymentStatusCode  == 2 {
                    
                    segmentPaymentStatus.selectedSegmentIndex = 1
                    
                }
                else if paymentStatusCode  == 3 {
                    
                    segmentPaymentStatus.selectedSegmentIndex = 2
                    
                }
            }
            
            
            if let date = dateFormatterGet.date(from: model.dealDate ?? "") {
                
                txtDealDate.selectedDate =  date
                
            }
            
            if model.dealValue ?? 0.0  > 0.0 {
                
                txtDealValue.text = "\(model.dealValue ?? 0.0)"
                
                txtDealCurrencySymbol.text = model.dealCurrencySymbol ?? ""
                
            }
            
            for modelMain in currency ?? [] {
                
                if model.dealCurrencyId == modelMain.currencyId ?? 0{
                    
                    selectedCurrencyCode = modelMain
                }
            }
            
            selectedIndex = model.stageId ?? 0
            
            collectionViewStatus.reloadData()
            
            collectionViewStandard.reloadData()
            
            transactionId = model.transactionId ?? 0
            
            if model.attachments?.count ?? 0 > 0 {
                
                attachments = model.attachments ?? []
                
                collectionViewContract.reloadData()
            }
            
            ////--New Standard
            
            if model.workFlowType == 0 {
                
                let count = masterStageList?.count ?? 0
                
                for index in 0..<count {
                    
                    if let model:StageListMainModel = masterStageList?[index] {
                        
                        if let modelMain:ReferralDetailModel = referralDetailModel {
                            
                            if modelMain.productType != 2 {
                                
                                if model.stageId == selectedIndex {
                                    
                                    let indexPath: IndexPath = IndexPath(item: index, section: 0)
                                    
                                    collectionViewStandard.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                                    if model.progressValue == 100 {
                                        
                                        heightViewEditable.constant = 550
                                        heightViewUpdateDealValue.constant = 80
                                        heightViewContract.constant = 150
                                        heightViewDealDate.constant = 80
                                        viewUpdateDealValue.alpha = 1
                                        viewDealDate.alpha = 1
                                        viewContract.alpha = 1
                                        lblDealValueNewTitle.alpha = 1
                                        txtDealValue.alpha = 1
                                        txtDealCurrencySymbol.alpha = 1
                                    }
                                        
                                    else{
                                        heightViewEditable.constant = 350
                                        heightViewContract.constant = 0
                                        heightViewDealDate.constant = 0
                                        heightViewUpdateDealValue.constant = 40
                                        viewUpdateDealValue.alpha = 1
                                        viewDealDate.alpha = 0
                                        viewContract.alpha = 0
                                        txtDealValue.alpha = 0
                                        txtDealCurrencySymbol.alpha = 0
                                        lblDealValueNewTitle.alpha = 0
                                        
                                    }
                                    
                                }
                            }
                            else{
                                //                                heightViewEditable.constant = 350
                                //                                heightViewContract.constant = 0
                                //                                heightViewDealDate.constant = 0
                                //                                heightViewUpdateDealValue.constant = 40
                                //                                viewUpdateDealValue.alpha = 1
                                //                                viewDealDate.alpha = 0
                                //                                viewContract.alpha = 0
                                //                                txtDealValue.alpha = 0
                                //                                txtDealCurrencySymbol.alpha = 0
                                //                                lblDealValueNewTitle.alpha = 0
                                heightViewEditable.constant = 410
                                heightViewPaymentStatus.constant = 295
                                heightDeclarationView.constant = 60
                                viewDeclare.alpha = 1
                                viewInvoice.alpha = 0
                                
                                if referralDetailModel?.paymentProgressValue == 100 {
                                    
                                    //                                    heightViewEditable.constant = 350
                                    //                                    heightViewPaymentStatus.constant = 235
                                    heightDeclarationView.constant = 0
                                    viewDeclare.alpha = 0
                                    viewInvoice.alpha = 1
                                }
                                heightViewContract.constant = 0
                                heightViewDealDate.constant = 0
                                heightViewUpdateDealValue.constant = 40
                                viewUpdateDealValue.alpha = 1
                                viewDealDate.alpha = 0
                                viewContract.alpha = 0
                                txtDealValue.alpha = 0
                                txtDealCurrencySymbol.alpha = 0
                                lblDealValueNewTitle.alpha = 0
                                viewPaymentStatus.alpha = 1
                            }
                        }
                        
                    }
                    
                }
                
                viewBgCollectionViewStandard.isHidden = false
                lblCollectionViewStandardTitle.isHidden = false
                collectionViewStandard.isHidden = false
                heightOfStandardCollectionViewBG.constant = 100
                
                viewBgCollectionViewStatus.isHidden = true
                lblStatusViewTitle.isHidden = true
                collectionViewStatus.isHidden = true
                heightViewUpdateStatus.constant = 0
            }
            else
            {
                viewBgCollectionViewStandard.isHidden = true
                lblCollectionViewStandardTitle.isHidden = true
                collectionViewStandard.isHidden = true
                heightOfStandardCollectionViewBG.constant = 0
                
                viewBgCollectionViewStatus.isHidden = false
                lblStatusViewTitle.isHidden = false
                collectionViewStatus.isHidden = false
                heightViewUpdateStatus.constant = 100
                
            }
            
            lblCopyOfContract.text = "Copy of PO / Contract".localized()
            lblDealValueNewTitle.text = "Deal Value".localized()
            lblDealDateTitle.text = "Deal Date".localized()
            
            
            if model.productType == 1 {
                lblCopyOfContract.text = "Copy of Offer letter".localized()
                lblDealValueNewTitle.text = "Total Salary".localized()
                lblDealDateTitle.text = "Joining Date".localized()
            }
            
            if model.productType == 2 {
                
                lblCopyOfContract.text = "Atachement".localized()
                lblDealValueNewTitle.text = "Event Registration Fee".localized()
                lblDealDateTitle.text = "Event Date".localized()
                
            }
            
        }
        
        if comingFromArchive == true
        {
            heightConstraintOfPostMessage.constant = 0
        }
        
    }
    
    func updateBuyerInfoView(){
        
        if let model:ReferralDetailModel = referralDetailModel {
            
            lblNotesToSellerTitle.text = "Notes to Company".localized()
            lblGeneralNotesTitleText.text = "General Notes".localized()
            
            lblName.text = model.referredTo ?? ""
            
            var estimateheightViewBuyerInfo:CGFloat = 100.0
            
            heightViewBuyerNumber.constant = 0
            heightViewgeneralNotes.constant  = 0
            heightViewNotesToSeller.constant = 0
            heightViewBuyerInfoBg.constant = estimateheightViewBuyerInfo + 40
            heightViewBuyerInfo.constant = estimateheightViewBuyerInfo
            
            viewBuyerNumber.alpha = 0
            viewGeneralNotes.alpha = 0
            viewNotesToSeller.alpha = 0
            imgViewMail.alpha = 0
            imgViewCall.alpha = 0
            lblMobileNum.alpha = 0
            lblEmail.alpha = 0
            imgViewCV.alpha = 0
            viewCVText.alpha = 0
            heightViewCVText.constant = 0
            btnChangeProduct.alpha = 0
            
            viewEventDateTime.alpha = 0
            heightEventDateTime.constant = 0
            
            if model.isSeller == 1 {
                
                btnChangeProduct.alpha = 1
                
            }
            
            if  (model.isSeller == 1 || model.isLeadReferredPartner == 1) && ( model.mobileNumber?.count ?? 0 > 0 ){
                
                lblMobileNum.alpha = 1
                imgViewCall.alpha = 1
                
                lblMobileNum.attributedText = NSMutableAttributedString(string:  "\(model.mobileIsd ?? "")\(model.mobileNumber ?? "")",attributes: countAttributes)
                
            }
            
            if ( model.isSeller == 1 || model.isLeadReferredPartner == 1) && model.emailId?.count ?? 0 > 0 {
                
                lblEmail.alpha = 1
                imgViewMail.alpha = 1
                
                lblEmail.attributedText = NSMutableAttributedString(string: model.emailId ?? "",attributes: countAttributes)
                
            }
            
            if  ((model.isSeller == 1 || model.isLeadReferredPartner == 1 ) && model.mobileNumber?.count ?? 0 > 0 ) || (model.isSeller == 1 && model.emailId?.count ?? 0 > 0) {
                
                viewBuyerNumber.alpha = 1
                
                heightViewBuyerNumber.constant = 35
                
                estimateheightViewBuyerInfo = estimateheightViewBuyerInfo + heightViewBuyerNumber.constant
                
            }
            
            if model.isSeller == 1 && model.productType == 1 && model.cvText?.count ?? 0 > 0 {
                
                lblCVText.attributedText = NSMutableAttributedString(string:  "\(model.cvText ?? "")",attributes: underLineResumeAttributes)
                
                imgViewCV.image = UIImage.init(named: "cvUnSelectIcon")
                
                imgViewCV.setImageColor(color: .primaryColor)
                
                if model.cvPath?.count ?? 0 > 0 {
                    
                    lblCVText.attributedText = NSMutableAttributedString(string:  "\("View resume".localized())",attributes: underLineResumeAttributes)
                    
                    imgViewCV.image = UIImage.init(named: "cvSelectIcon")
                    imgViewCV.setImageColor(color: .primaryColor)
                    
                }
                
                btnViewCV.addTarget(self, action: #selector(btnViewResumeAction(_:)), for: .touchUpInside)
                
                lblCVText.alpha = 1
                
                imgViewCV.alpha = 1
                
                viewCVText.alpha = 1
                
                heightViewCVText.constant = 35
                
                estimateheightViewBuyerInfo = estimateheightViewBuyerInfo +  heightViewCVText.constant
                
            }
            
            lblBuyerInfoTitle.text = "Buyer Information".localized()
            
            if model.productType == 1 {
                
                imgLogo.image = UIImage.init(named: "jobDefault")
                lblBuyerInfoTitle.text = "Jobseeker Information".localized()
                
            }
            
            if model.productType == 2 {
                
                imgLogo.image = UIImage.init(named: "eventIcon")
                lblBuyerInfoTitle.text = "Guest Information".localized()
                
                viewEventDateTime.alpha = 1
                heightEventDateTime.constant = 86
                lblEventDateTime.text = "\(getDateTime(stringDateTime: model.displayStartTime ?? "")) - \(getDateTime(stringDateTime: model.displayEndTime ?? ""))"
            }
            
            
            if referralDetailModel?.specialTerms?.count ?? 0 > 0{
                
                
                viewGeneralNotes.alpha = 1
                
                lblGeneralNotesValue.text = referralDetailModel?.specialTerms ?? ""
                
                guard let labelText = lblGeneralNotesValue.text else { return }
                
                let height = estimatedHeightOfLabel(text: labelText)
                
                
                heightViewgeneralNotes.constant  = 60 + height
                
                estimateheightViewBuyerInfo = estimateheightViewBuyerInfo +  heightViewgeneralNotes.constant
                
            }
            
            if referralDetailModel?.referralNoteToSeller?.count ?? 0 > 0{
                
                viewNotesToSeller.alpha = 1
                
                lblNotesValue.text = referralDetailModel?.referralNoteToSeller ?? ""
                
                guard let labelText = lblNotesValue.text else { return }
                
                let height = estimatedHeightOfLabel(text: labelText)
                
                heightViewNotesToSeller.constant = height + 70
                
                estimateheightViewBuyerInfo = estimateheightViewBuyerInfo + heightViewNotesToSeller.constant
                
            }
            
            heightViewBuyerInfoBg.constant = estimateheightViewBuyerInfo + 40
            heightViewBuyerInfo.constant = estimateheightViewBuyerInfo
            
        }
    }
    
    func getDateTime(stringDateTime:String) -> String {
        
        var dateStartText = ""
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM HH:mm a"
        dateFormatterPrint.timeZone = .current
        
        if let date = dateFormatterGet.date(from: stringDateTime) {
            
            dateStartText = dateFormatterPrint.string(from: date)
            
        }
        
        return dateStartText
    }
    
    func updateReferralPayoutView(){
        
        if let model:ReferralDetailModel = referralDetailModel {
            
            if model.referralPayoutDetails?.count ?? 0 > 0 {
                
                DispatchQueue.main.async(execute: {
                    
                    self.viewReferralPayOffBg.alpha = 1
                    
                    let heightReferral =  model.referralPayoutDetails?.count ?? 0
                    
                    self.heightTableViewPayoff.constant = CGFloat(heightReferral * 50)
                    
                    self.heightViewReferralPayOff.constant =  self.heightTableViewPayoff.constant + 16
                    
                    self.heightViewReferralPayOffBg.constant = self.heightViewReferralPayOff.constant + 60
                    
                    self.tableViewReferralView.reloadData()
                })
                
            }
            else{
                self.viewReferralPayOffBg.alpha = 0
                self.heightViewReferralPayOffBg.constant = 0
            }
            
        }
        
    }
    
    func updateTopCompanyView(){
        
        if let model:ReferralDetailModel = referralDetailModel {
            
            lblProductName.text = model.productName ?? ""
            lblCompanyName.text = model.sellerCompanyName ?? ""
            
            heightLblNotify.constant = 0
            lblNotifyText.alpha = 0
            heightViewCompanyName.constant = 120
            
            if model.productLogo?.count ?? 0 > 0 {
                imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
            }
            
            if model.productType == 1 {
                
                imgLogo.image = UIImage.init(named: "jobDefault")
                
            }
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = TimeZone.current
            var referredDateText = ""
            
            if let date = dateFormatterGet.date(from: model.referredDate ?? "") {
                
                referredDateText = dateFormatterPrint.string(from: date)
            }
            
            lblReferredBy.text = "\("Referred".localized())\(model.referredBy ?? "")\(" on ".localized())\(referredDateText)"
            
            if model.referredBy?.count ?? 0 > 0 {
                lblReferredBy.text = "\("Referred by ".localized())\(model.referredBy ?? "")\(" on ".localized())\(referredDateText)"
            }
            
            if model.hideTransForBuyer == 1{
                
                lblNotifyText.text = "Note: Buyer not informed about this mail".localized()
                
                lblNotifyText.alpha = 1
                
                heightLblNotify.constant = 25
                
                heightViewCompanyName.constant = 150
                
            }
        }
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let size = CGSize(width: view.frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: UIFont.fontForType(textType: 3)]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
        
    }
    
    func openPaymentPage()
    {
        if paymentDetails != nil {
            
            if billingModel?.billing_address?.count ?? 0 > 0{
                
                if paymentDetails?.billingDetails != nil{
                    
                    if (paymentDetails?.currencyTitle?.caseInsensitiveCompare("AED") == ComparisonResult.orderedSame) {
                        
                        openCCAvenuePayment()
                    }
                    if (paymentDetails?.currencyTitle?.caseInsensitiveCompare("INR") == ComparisonResult.orderedSame) {
                        
                        openRazorpay()
                        
                    }
                    else
                    {
                        openPayPal()
                        // self.paymentContext?.presentPaymentOptionsViewController()
                    }
                }
                else{
                    print("billing_address is empty")
                    
                }
            }
        }
    }
    
    func openRazorpay()
    {
        let amount = paymentDetails?.amount ?? 0.0
        let options: [String:Any] = [
            "amount" : amount * 100, //mandatory in paise like:- 1000 paise ==  10 rs
            "currency" : paymentDetails?.currencyTitle ?? "",
            "order_id":paymentDetails?.razorpay_order_id ?? "",
            "receipt": paymentDetails?.order_id ?? "",
            "prefill": [
                "contact": billingModel?.billing_tel ?? "",
                "email": billingModel?.billing_email ?? ""
            ]
        ]
        print("options::\(options)")
        razorpay?.open(options)
    }
    
    func openCCAvenuePayment()
    {
        
        
        //        let storyBoard = UIStoryboard.init(name: "Invoices", bundle: nil)
        //        let sellerPage:CCWebViewController = storyBoard.instantiateViewController(withIdentifier: "CCWebViewController") as! CCWebViewController
        //
        //        sellerPage.paymentDetails = paymentDetails
        //
        //        navigationController?.pushViewController(sellerPage, animated: true)
        
        
        //print("\("PayemetModel::::::::::::::")\(String(describing: paymentDetails?.billingDetails))\(String(describing: paymentDetails))")
        paymentController = InitialViewController.init(orderId: String(paymentDetails?.order_id ?? ""), merchantId: paymentDetails?.merchantId!, accessCode: paymentDetails?.accessCode!, custId: String(paymentDetails?.customerId ?? 0), amount: String(paymentDetails?.amount ?? 0.0), currency: paymentDetails?.currencyTitle!, rsaKeyUrl: paymentDetails?.rsaKeyUrl!, redirectUrl: paymentDetails?.redirectUrl!, cancelUrl: paymentDetails?.cancelUrl!, showAddress: "N", billingName: billingModel?.billing_name!, billingAddress: billingModel?.billing_address!, billingCity: billingModel?.billing_city!, billingState: billingModel?.billing_state!, billingCountry: billingModel?.billing_country!, billingTel: billingModel?.billing_tel!, billingEmail: billingModel?.billing_email!, deliveryName: paymentDetails?.delivery_name!, deliveryAddress: paymentDetails?.delivery_address!, deliveryCity: paymentDetails?.delivery_city!, deliveryState: paymentDetails?.delivery_state!, deliveryCountry: paymentDetails?.delivery_country!, deliveryTel: paymentDetails?.delivery_tel!, promoCode: "", merchant_param1: "Param 1", merchant_param2: "Param 2", merchant_param3: "Param 3", merchant_param4: "Param 4", merchant_param5: "Param 5", useCCPromo: "N")
        
        
        paymentController.delegate = self
        
        present(paymentController, animated: true, completion: nil)
        
    }
    
    func openPayPal() {
        
        guard let url = URL(string: paymentDetails?.paymentUrl ?? "") else { return }
        UIApplication.shared.open(url)
        
    }
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
        
        txtDealCurrencySymbol.text = selectedCurrencyCode?.currencySymbol ?? ""
        
    }
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
    @objc func btnChangeAction(_ sender: UIButton) {
        
        if let transactionModel:ReferralDetailModel = referralDetailModel {
            
            let storyBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
            if  let changeProduct:ChangeProductVC = storyBoard.instantiateViewController(withIdentifier: "ChangeProductVC") as? ChangeProductVC {
                
                changeProduct.sellerCode = transactionModel.sellerCode ?? 0
                changeProduct.productCode = transactionModel.productCode ?? 0
                changeProduct.productType = transactionModel.productType ?? 0
                changeProduct.transactionId = transactionModel.transactionId ?? 0
                changeProduct.delegate = self
                navigationController?.pushViewController(changeProduct, animated: true)
                
            }
        }
        
    }
    
    func finalPage(withMessage:String?) {
        
        if  let finalPage:ReferralSuccessVC = UIStoryboard.init(name: SearchSB, bundle: nil).instantiateViewController(withIdentifier: "ReferralSuccessVC") as? ReferralSuccessVC{
            finalPage.message = withMessage
            navigationController?.present(finalPage, animated: true, completion: nil)
            navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    
    func submitStatusDetails(){
        
        if selectedIndex == 0 {
            
            selectedIndex = masterStageList?.first?.stageId ?? 0
            
        }
        
        startAnimatingAfterSubmit()
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MMM dd,yyyy hh:mm a"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        var dateText = ""
        
        var dateServer = ""
        
        if let myString:String = dateFormatterGet.string(from: txtDealDate.selectedDate ) {
            
            dateServer = myString
        }
        
        if let date = dateFormatterGet.date(from:dateServer) {
            
            dateText = dateFormatterPrint.string(from: date)
        }
        
        var dealValue = 0.0
        
        if let dealAmnt = Double(txtDealValue.text ?? "0.0"){
            
            dealValue = dealAmnt
        }
        
        
        
        let params:NSDictionary = ["transactionId":transactionId ?? 0,
                                   "sellerCode": sellerCode ?? 0,
                                   "productCode":productCode ?? 0,
                                   "dealValue":dealValue,
                                   "dealCurrencyId":selectedCurrencyCode?.currencyId ?? 0,
                                   "dealCurrencySymbol":selectedCurrencyCode?.currencySymbol ?? "",
                                   "dealDate":dateText,
                                   "stageId": selectedIndex,
                                   "outputAttachments":attachments,
                                   "notes":txtViewNotes.text ?? "",
                                   "paymentStatusCode":paymentStatusCode ?? 0
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/updateTransaction?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                
                                // weakSelf?.finalPage(withMessage: message)
                                
                                
                                weakSelf?.stopAnimating()
                                
                                weakSelf?.delegate?.didCallApi()
                                
                                let isUpdated:[String: Int] = ["update": 1]
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callMessage"), object: nil, userInfo: isUpdated)
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callHome"), object: nil, userInfo: isUpdated)
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                        
                    }
                }
            }
            
            //Show error
            
            weakSelf?.stopAnimating()
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        stopAnimating()
    }
    
    func fetchUserDetail() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/referralLeadDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&transactionId=\(transactionId ?? 0)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ReferralDetailApiModel.self, from: data)
                                
                                if let apiResponse:ReferralDetailModel = jsonResponse.referralLeadDetails {
                                    
                                    weakSelf?.referralDetailModel = apiResponse
                                    
                                }
                                
                                if let apiResponse:[StageListMainModel] = jsonResponse.masterStageList {
                                    
                                    weakSelf?.masterStageList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[LogModel] = jsonResponse.changeLog {
                                    
                                    weakSelf?.changeLog  = apiResponse
                                    
                                }
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currency {
                                    
                                    weakSelf?.currency = apiResponse
                                    
                                }
                                
                                //                                if weakSelf?.isEditable == true {
                                //
                                //                                  //  weakSelf?.updateUIForReadOnly()
                                //                                }
                                //                                else{
                                weakSelf?.updateUIForEditableView()
                                //  }
                                
                                weakSelf?.updateReferralPayoutView()
                                
                                // weakSelf?.tableViewReferralView.reloadData()
                                
                                let isUpdated:[String: Int] = ["update": 1]
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callMessage"), object: nil, userInfo: isUpdated)
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func getGcDetails() {
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        
        //        if isFirst == true
        //        {
        //            isFirst = false
        //            startAnimating()
        //        }
        //        else
        //        {
        startAnimatingAfterSubmit()
        //  }
        
        let params:NSDictionary = ["sellerCode":sellerCode ?? 0]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/gcdetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            DispatchQueue.main.async
                                {
                                    weakSelf?.stopAnimating()
                            }
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(PaymentAPIModel.self, from: data)
                                
                                if let paymentResponse:PaymentModel = jsonResponse.details {
                                    weakSelf?.paymentDetails = paymentResponse
                                    weakSelf?.razorpay = Razorpay.initWithKey(weakSelf?.paymentDetails?.accessCode ?? "", andDelegate: self)
                                    weakSelf?.billingModel = weakSelf?.paymentDetails?.billingDetails
                                    
                                    weakSelf?.openPaymentPage()
                                }
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            //Show error
            DispatchQueue.main.async
                {
                    weakSelf?.stopAnimating()
            }
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    func fetchInvoiceList() {
        
        let apiClient:APIClient = APIClient()
        
        weak var weakSelf = self
        
        startAnimatingAfterSubmit()
        
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/paymentDeclarationMasterData?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ReferralPaymentMasterApiModel.self, from: data)
                                
                                
                                if let apiResponse:[CurrencyModel] = jsonResponse.currencyList {
                                    
                                    weakSelf?.currencyList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[PaymentTypeListModel] = jsonResponse.paymentTypeList {
                                    
                                    weakSelf?.paymentTypeList = apiResponse
                                    
                                }
                                
                                
                                DispatchQueue.main.async
                                    {
                                        weakSelf?.stopAnimating()
                                        weakSelf?.openPaymentDeclarationPage()
                                }
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            DispatchQueue.main.async
                {
                    weakSelf?.stopAnimating()
            }
            
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    func sendInvoice(transactionID:Int){
        
        let apiClient:APIClient = APIClient()
        
        let params:NSDictionary = ["transactionID":transactionID]
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        weak var weakSelf = self
        startAnimatingAfterSubmit()
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/sendTransactionInvoice?lngId=\(Localize.currentLanguageID)&token=\(getCurrentUserToken())"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            weakSelf?.stopAnimating()
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            if let message:String = dataObj.object(forKey: "message") as? String{
                                
                                weakSelf?.showInfoMessage(message: message)
                                
                            }
                            
                        }
                    }
                }
                
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
    
    
    @IBAction func btnChangeStatusAction(_ sender: Any) {
        let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
        let referralStatusPage:ReferralManagerStatusVC = storyBoard.instantiateViewController(withIdentifier: "ReferralManagerStatusVC") as! ReferralManagerStatusVC
        referralStatusPage.transactionId = transactionId ?? 0
        referralStatusPage.delegate = self
        referralStatusPage.sellerCode = sellerCode ?? 0
        referralStatusPage.productCode = productCode ?? 0
        referralStatusPage.referralDetailModel = referralDetailModel
        referralStatusPage.isEditable = isEditable
        
        navigationController?.pushViewController(referralStatusPage, animated: true)
    }
    
    @IBAction func btnPostMessage(_ sender: Any) {
        
        if referralDetailModel?.productType != 2 {
            
            if masterStageList?[selectedIndex].progressValue ?? 0.0 >= 100.0 {
                
                if txtDealCurrencySymbol.text?.count == 0 {
                    
                    showErrorMessage(message: "Enter Deal Currency".localized())
                    
                    return
                }
                
                if txtDealValue.text?.count == 0
                {
                    showErrorMessage(message: "Enter Deal Value".localized())
                    
                    return
                }
                
            }
            
            if attachments.count == 0 {
                
                showErrorMessage(message: "Please upload purchase proof".localized())
                
                return
                
            }
            
            submitStatusDetails()
            
        }
        else{
            
            submitStatusDetails()
            
        }
        
    }
    
    @IBAction func segmentPaymentAction(_ sender: UISegmentedControl) {
        
        switch segmentPaymentStatus.selectedSegmentIndex {
        case 0:
            paymentStatusCode = 1
            break
        case 1:
            paymentStatusCode = 2
            break
        case 2:
            paymentStatusCode = 3
            break
        default:
            print("")
        }
    }
}

extension ReferalManagerVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus {
            
            return CGSize.init(width: collectionViewStatus.frame.width / 4, height: 40.0)
            
        }
        
        if collectionView == collectionViewStandard
            
        {
            return CGSize.init(width: collectionViewStandard.frame.width, height: collectionViewStandard.frame.height)
            
        }
        return CGSize.init(width: 100.0, height: 100.0)
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewStatus {
            
            
            return masterStageList?.count ?? 0
            
        }
        
        if collectionView == collectionViewStandard {
            
            
            return masterStageList?.count ?? 0
            
        }
        
        if collectionView == collectionViewContract {
            
            
            return attachments.count
            
        }
        
        if collectionView == collectionViewAttachment{
            
            
            if referralDetailModel?.attachmentList?.count ?? 0 > 0 {
                
                return referralDetailModel?.attachmentList?.count ?? 0
            }
            
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewStatus {
            
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForStatus(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        
        if collectionView == collectionViewStandard {
            
            if let cell:ReferralStandardStatusCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReferralStandardStatusCollectionCell", for: indexPath) as? ReferralStandardStatusCollectionCell {
                
                return getCellForStandardStatus(cell: cell, indexPath: indexPath)
                
            }
            
        }
        
        
        if collectionView == collectionViewContract{
            
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForContract(cell: cell, indexPath: indexPath)
                
            }
        }
        
        if collectionView == collectionViewAttachment {
            
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachment(cell: cell, indexPath: indexPath)
                
            }
        }
        
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewStatus {
            
            if let model:StageListMainModel = masterStageList?[indexPath.row] {
                
                selectedIndex = model.stageId ?? 0
                
                if selectedIndex == model.stageId && model.progressValue == 100 {
                    
                    heightViewEditable.constant = 550
                    heightViewUpdateDealValue.constant = 80
                    heightViewContract.constant = 150
                    heightViewDealDate.constant = 80
                    viewUpdateDealValue.alpha = 1
                    viewDealDate.alpha = 1
                    viewContract.alpha = 1
                    txtDealValue.alpha = 1
                    
                    txtDealCurrencySymbol.alpha = 1
                    lblDealValueNewTitle.alpha = 1
                    
                }
                else{
                    heightViewEditable.constant = 180
                    heightViewContract.constant = 0
                    heightViewDealDate.constant = 0
                    heightViewUpdateDealValue.constant = 40
                    viewUpdateDealValue.alpha = 1
                    viewDealDate.alpha = 0
                    viewContract.alpha = 0
                    txtDealValue.alpha = 0
                    txtDealCurrencySymbol.alpha = 0
                    lblDealValueNewTitle.alpha = 0
                }
                
                collectionViewStatus.reloadData()
                
                
            }
            
        }
        
        if collectionView == collectionViewAttachment {
            
            
            if let fileName = referralDetailModel?.attachmentList?[indexPath.row] {
                
                if let imagePath = fileName["cdnPath"] {
                    
                    if let filePath = fileName["fileName"] {
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            let sb:UIStoryboard = UIStoryboard.init(name: ImageViewerSB, bundle: nil)
                            let imageViewer:ImageViewerVC = sb.instantiateViewController(withIdentifier: "ImageViewerVC") as! ImageViewerVC
                            imageViewer.strImg = imagePath
                            navigationController?.pushViewController(imageViewer, animated: true)
                            
                        }
                        else{
                            
                            openWebView(urlString:imagePath, title: filePath, subTitle: "")
                        }
                        
                    }
                }
                
            }
            
        }
    }
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.alpha = 0
        
        
        if let model = referralDetailModel?.attachmentList?[indexPath.row]
        {
            if let imagePath = model["cdnPath"]
            {
                if let filePath = model["fileName"] {
                    
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            cell.imgView.sd_setImage(with: url, completed: nil)
                            
                        }
                        else{
                            cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                        }
                        
                    }
                }
                
            }
        }
        
        return cell
    }
    
    func getCellForContract(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        
        if let model:[String:String] = attachments[indexPath.row] as! [String:String]
        {
            if let imagePath:String = model["cdnPath"] as? String
            {
                if let filePath:String  = model["fileName"] as? String {
                    
                    if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                        
                        let strFileType = filePath.fileExtension()
                        
                        if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                        {
                            
                            cell.imgView.sd_setImage(with: url, completed: nil)
                            
                        }
                        else{
                            cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                        }
                        
                    }
                }
                
            }
        }
        
        return cell
    }
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:StageListMainModel = masterStageList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.stageTitle ?? ""
            
            if selectedIndex == model.stageId {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
            
            if selectedIndex == model.stageId && model.progressValue == 100 {
                
                heightViewEditable.constant = 550
                heightViewUpdateDealValue.constant = 80
                heightViewContract.constant = 150
                heightViewDealDate.constant = 80
                viewUpdateDealValue.alpha = 1
                viewDealDate.alpha = 1
                viewContract.alpha = 1
                lblDealValueNewTitle.alpha = 1
                txtDealValue.alpha = 1
                txtDealCurrencySymbol.alpha = 1
            }
            
            
        }
        return cell
    }
    
    
    
    func getCellForStandardStatus(cell:ReferralStandardStatusCollectionCell,indexPath:IndexPath) -> ReferralStandardStatusCollectionCell {
        
        if let model:StageListMainModel = masterStageList?[indexPath.row] {
            
            cell.lblStandardStatus.text = model.stageTitle ?? ""
        }
        return cell
    }
    
}

extension ReferalManagerVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return referralDetailModel?.referralPayoutDetails?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:ReferralPayOffTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ReferralPayOffTableViewCell", for: indexPath) as? ReferralPayOffTableViewCell {
            
            return getCellForReferralPayOff(cell: cell, indexPath: indexPath)
        }
        
        return UITableViewCell.init()
        
    }
    
    func getCellForReferralPayOff(cell:ReferralPayOffTableViewCell,indexPath:IndexPath) -> ReferralPayOffTableViewCell {
        
        if let model:ReferralPayOutModel = referralDetailModel?.referralPayoutDetails?[indexPath.row]{
            
            cell.lblTitle.text = model.title ?? ""
            
            cell.lblAmount.text = model.amount ?? ""
            
        }
        
        return cell
    }
    
    
    
}

extension ReferalManagerVC:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDealCurrencySymbol  {
            
            openCurrencySelector()
            
            return false
        }
        
        return true
    }
}

extension ReferalManagerVC:ChangeProductVCDelegate{
    
    func didChangeProduct() {
        
        fetchUserDetail()
        
        delegate?.didCallApi()
        
        
        
    }
    
}


class ReferralStandardStatusCollectionCell:UICollectionViewCell{
    
    @IBOutlet weak var lblStandardStatus: NKLabel!
    
}

class ReferralPayOffTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: NKLabel!
    @IBOutlet weak var lblAmount: NKLabel!
}

extension ReferalManagerVC:InitialViewControllerDelegate
{
    func getResponse(_ responseDict_: NSMutableDictionary!) {
        
        let orderNum = "\(responseDict_["order_id"] ?? "")"
        let orderStatus = "\(responseDict_["order_status"] ?? "")"
        let orderTrackId = "\(responseDict_["tracking_id"] ?? "")"
        
        let msg = "Your order # \(orderNum) is \(orderStatus)\n\nPayment Reference Number : \(orderTrackId)\n\nYou may use this number for any future communications.";
        
        let alert = UIAlertController(title: "Payment Status".localized(), message: msg, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Done".localized(), style: UIAlertAction.Style.default, handler: nil));
        
        self.present(alert, animated: true, completion: nil);
    }
}

extension ReferalManagerVC:RazorpayPaymentCompletionProtocol
{
    func onPaymentSuccess(_ payment_id: String) {
        
        saveRazorPaymentGatewayResponse(payment_id: payment_id)
        
        let alert = UIAlertController(title: "Paid".localized(), message: "Your payment was successful. The payment ID is \(payment_id)".localized(), preferredStyle: .alert)
        let action = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        
        fetchUserDetail()
        
        // submitReferral()
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        let alert = UIAlertController(title: "Error".localized(), message: "Your payment failed due to an error.\nCode: \(code)\nDescription: \(str)".localized(), preferredStyle: .alert)
        let action = UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func saveRazorPaymentGatewayResponse(payment_id: String)
    {
        
        weak var weakSelf = self
        
        DispatchQueue.main.async {
            
            weakSelf?.startAnimatingAfterSubmit()
        }
        
        let params:NSDictionary = ["order_id":paymentDetails?.order_id ?? "",
                                   "razorpay_order_id" : paymentDetails?.razorpay_order_id ?? "",
                                   "razorpay_payment_id" : payment_id
        ]
        
        print("params::\(params)")
        
        let encryptedParams = ["data": encryptParams(params: params)]
        
        let apiClient:APIClient = APIClient()
        
        
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveGatewayPaymentResponse?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                weakSelf?.stopAnimating()
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    
                    print("response :: \(response)")
                    print("jsonObj:: \(String(describing: jsonObj))")
                    //............
                }
            }
            
            //Show error
            weakSelf?.stopAnimating()
            weakSelf?.showErrorMessage(message: message)
            
        })
    }
    
}

extension ReferalManagerVC:TransferPaymentVCDelegate{
    
    func refreshInvoicePage(message: String) {
        
        fetchUserDetail()
        
    }
    
}

