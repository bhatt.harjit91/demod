//
//  ReferralManagerStatusVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 18/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol ReferralManagerStatusVCDelegate: class {
    func didUpdatedStatus()
}
class ReferralManagerStatusVC: SendOTPViewController,CurrencySelectorVCDelegate {
    
     @IBOutlet weak var txtViewNotes: NKTextView!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var lblStatusViewTitle: NKLabel!
    
    @IBOutlet weak var lblDealValueTitle: NKLabel!
    
    @IBOutlet weak var lblDealDateTitle: NKLabel!
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var btnPrev: UIButton!
    
    @IBOutlet weak var collectionViewStatus: UICollectionView!
    
    @IBOutlet weak var txtDealCurrencySymbol: RCTextField!
    
    @IBOutlet weak var txtDealValue: RCTextField!
    
    @IBOutlet weak var txtDealDate: RCTextField!
    
    @IBOutlet weak var collectionViewAttachments: UICollectionView!
   
    @IBOutlet weak var btnAttachment: UIButton!
    
    @IBOutlet weak var lblAttachmentTitle: NKLabel!
    
    @IBOutlet weak var viewMessageListTitle: RCView!
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var heightViewMessageList: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewAttachment: UIImageView!
    
    var transactionId:Int = 0
    
    var referralDetailModel:ReferralDetailModel?
    
    var masterStageList:[StageListMainModel]?
    
    var changeLog:[LogModel]?
    
    var messageHistory:[MessageHistoryModel]?
    
    var attachments:[Any] = []
    
    var selectedIndex:Int = 0
    
    var sellerCode:Int?
    
    var productCode:Int?
    
    var selectedCurrencyCode : CurrencyModel?
    
    var currency:[CurrencyModel]?
    
    weak var delegate:ReferralManagerStatusVCDelegate?
    
    var isEditable:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // tableViewList.estimatedRowHeight = 100
        
        navigationItem.title = "Referral Manager".localized()
        
        imgViewAttachment.setImageColor(color: .white)
        
        btnNext.addTarget(self, action: #selector(btnNextAction(_:)), for: .touchUpInside)
        
        btnPrev.addTarget(self, action: #selector(btnPreviousAction(_:)), for: .touchUpInside)
        
        btnAttachment.addTarget(self, action: #selector(btnAttachmentAction(_:)), for: .touchUpInside)
        
        txtDealCurrencySymbol.delegate = self
        
        if let currentCurrency =  getLocalCurrencyCode(){
            
            print("\("CurrencyCode::::::::")\(currentCurrency)")
            
            txtDealCurrencySymbol.text = currentCurrency
            
           // txtDealCurrencySymbol.isUserInteractionEnabled = false
            
        }
        
        txtDealDate.dateTimePicker.minimumDate = Date.init()
        
        btnSubmit.addTarget(self, action: #selector(submitAction(sender:)), for: .touchUpInside)
        
        fetchReferralDetailsStatus()
        
        if  isEditable ?? false {
            btnSubmit.isUserInteractionEnabled = false
        }
        else{
             btnSubmit.isUserInteractionEnabled = true
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        fetchReferralDetailsStatus()
//    }
    
    //Methods
    
    func updateUI(){
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy hh:mm a"
        dateFormatterPrint.timeZone = TimeZone.current
        
        var referredDateText = ""
        
        var dateText = ""
        
       
        
        if let model:ReferralDetailModel = referralDetailModel {
            
            if let date = dateFormatterGet.date(from: model.dealDate ?? "") {
                
                referredDateText = dateFormatterPrint.string(from: date)
            }
            
            if let date = dateFormatterPrint.date(from: referredDateText) {
                
                txtDealDate.selectedDate = date
            }
            
           attachments = model.attachments ?? []
            
         // txtDealDate.text = model.dealDate ?? ""
            
          txtDealValue.text = "\(model.dealValue ?? 0)"
            
          txtViewNotes.text = model.buyerComments ?? ""
            
          txtDealCurrencySymbol.text = model.dealCurrencySymbol ?? ""
          
          selectedIndex = model.stageId ?? 0
            
            let count = masterStageList?.count ?? 0
            
            for index in 0..<count {
              
                if let model:StageListMainModel = masterStageList?[index] {
                    
                    if model.stageId == selectedIndex {
                        
                        let indexPath: IndexPath = IndexPath(item: index, section: 0)
                        
                        collectionViewStatus.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                    }
                
                
                }
                
            }
            
            
         // collectionViewStatus.reloadData()
            
        }
        
        if changeLog?.count ?? 0 > 0 {
            
            viewMessageListTitle.alpha = 1
            
            DispatchQueue.main.async(execute: {
                
                var frame = self.tableViewList.frame
                frame.size.height = self.tableViewList.contentSize.height
                self.tableViewList.frame = frame
                
                self.heightViewMessageList.constant = self.tableViewList.contentSize.height + 40
                
            })
    
            
        }
        else{
            
            viewMessageListTitle.alpha = 0
            heightViewMessageList.constant = 0
            
        }
        
    }
    
    
    
    //Mark:- Actions

    @objc func submitAction(sender: UIButton){
        
        submitStatusDetails()
        
    }
    
     @objc func btnAttachmentAction(_ sender: Any) {
      //  startAnimating()
        
        isAttachment = true
        isDocument = true
        openImagePicker()
        
        weak var weakSelf = self
                
        didUploadHelpDeskImage = {status, urlString, imageName in
         //   weakSelf?.stopAnimating()
            if status {
                print(urlString, imageName)
                var arrayEachImage : [String : String] = [:]
                arrayEachImage["cdnPath"] = urlString
                arrayEachImage["fileName"] = imageName
                weakSelf?.attachments.append(arrayEachImage)
                weakSelf?.collectionViewAttachments.reloadData()
                self.isAttachment = false
            }
            
        }
        
    }
    
    @objc func deleteImage(_ sender: NKButton) {
        if attachments.count > sender.tag {
            attachments.remove(at: sender.tag)
           
        }
    }
    
    @objc func btnPreviousAction(_ sender: UIButton) {
        
        let visibleItems: NSArray = self.collectionViewStatus.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let prevItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        if prevItem.row < masterStageList?.count ?? 0 && prevItem.row >= 0{
            
        if let model:StageListMainModel = masterStageList?[prevItem.row ] {
                
            selectedIndex = model.stageId ?? 0
                
        }
            self.collectionViewStatus.scrollToItem(at: prevItem, at: .right, animated: true)
            
        }
    }
    
    
    @objc func btnNextAction(_ sender: UIButton) {
        
        let visibleItems: NSArray = self.collectionViewStatus.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        if nextItem.row < masterStageList?.count ?? 0 {
            if let model:StageListMainModel = masterStageList?[nextItem.row ] {
                
                selectedIndex = model.stageId ?? 0
                
            }
            self.collectionViewStatus.scrollToItem(at: nextItem, at: .left, animated: true)
            
        }
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.collectionViewStatus.contentOffset.y ,width : self.collectionViewStatus.frame.width,height : self.collectionViewStatus.frame.height)
        self.collectionViewStatus.scrollRectToVisible(frame, animated: true)

    }
    
    func scrollCollection() {
        for cell in collectionViewStatus.visibleCells {
            let indexPath = collectionViewStatus.indexPath(for: cell)
            
            if let model:StageListMainModel = masterStageList?[indexPath?.row ?? 0] {
                
                selectedIndex = model.stageId ?? 0
                
            }
        }
    }
    
    func didFinishSelectingCurrency(selectedCurrency: CurrencyModel) {
        
        selectedCurrencyCode = selectedCurrency
    
        txtDealCurrencySymbol.text = selectedCurrencyCode?.currencySymbol ?? ""
    
    }
    
    func openCurrencySelector() {
        
        let storyBoard = UIStoryboard.init(name: CurrencySelectorSB, bundle: nil)
        let countrySelector:CurrencySelectorVC = storyBoard.instantiateViewController(withIdentifier: "CurrencySelectorVC") as! CurrencySelectorVC
        countrySelector.selectedCurrency = selectedCurrencyCode
        countrySelector.currencyList = currency ?? []
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
        
    }
    
    func finalPage(withMessage:String?) {
        
        if  let finalPage:ReferralSuccessVC = UIStoryboard.init(name: SearchSB, bundle: nil).instantiateViewController(withIdentifier: "ReferralSuccessVC") as? ReferralSuccessVC{
            finalPage.message = withMessage
            navigationController?.present(finalPage, animated: true, completion: nil)
            navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    
    func submitStatusDetails(){
        
        if selectedIndex == 0 {
            
            selectedIndex = masterStageList?.first?.stageId ?? 0
            
        }
        
        startAnimatingAfterSubmit()
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MMM dd,yyyy hh:mm a"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        var dateText = ""
        
        var dateServer = ""
        
        if let myString:String = dateFormatterGet.string(from: txtDealDate.selectedDate ) {
            
            dateServer = myString
        }
        
        if let date = dateFormatterGet.date(from:dateServer) {
            
            dateText = dateFormatterPrint.string(from: date)
        }
        
        let dealValue = txtDealValue.text ?? "0"
        
        let params:NSDictionary = ["transactionId":transactionId,
                                   "sellerCode": sellerCode ?? 0,
                                   "productCode":productCode ?? 0,
                                   "dealValue":dealValue,
                                   "dealCurrencyId":selectedCurrencyCode?.currencyId ?? 0,
                                   "dealCurrencySymbol":selectedCurrencyCode?.currencySymbol ?? "",
                                   "dealDate":dateText,
                                   "stageId": selectedIndex,
                                   "outputAttachments":attachments,
                                   "notes":txtViewNotes.text ?? ""
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/updateTransaction?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            //let decoder = JSONDecoder()
                            let decoder = JSONDecoder()
                            let dateFormat:DateFormatter = DateFormatter.init()
                            dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                            decoder.dateDecodingStrategy = .formatted(dateFormat)
                          
                            // weakSelf?.finalPage(withMessage: message)
                            
                            
                            weakSelf?.stopAnimating()
                            
                            weakSelf?.delegate?.didUpdatedStatus()
                                
                        let isUpdated:[String: Int] = ["update": 1]
                                
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callMessage"), object: nil, userInfo: isUpdated)
                            
                         weakSelf?.navigationController?.popViewController(animated: true)
                            
                            return
                            
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                        }
                      }
                        
                    }
                }
            }
            
            //Show error
            
              weakSelf?.stopAnimating()
              weakSelf?.showErrorMessage(message: message)
            
        })
        
          stopAnimating()
    }
    
    func fetchReferralDetailsStatus() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/referralLeadDetailsUpdatePage?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&transactionId=\(transactionId)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ReferralDetailUpdateApiModel.self, from: data)
                                
//
//                                if let apiResponse:ReferralDetailModel = jsonResponse.referralLeadDetails {
//
//                                    weakSelf?.referralDetailModel = apiResponse
//
//                                }
                                
                                if let apiResponse:[LogModel] = jsonResponse.changeLog {
                                    
                                    weakSelf?.changeLog = apiResponse
                                    
                                }
                                
                                if let apiResponse:[MessageHistoryModel] = jsonResponse.messageHistory {
                                    
                                    weakSelf?.messageHistory = apiResponse
                                    
                                }
                                
                                if let apiResponse:[StageListMainModel] = jsonResponse.masterStageList {
                                    
                                    weakSelf?.masterStageList = apiResponse
                                    
                                }
                                if let apiResponse:[CurrencyModel] = jsonResponse.currency {
                                    
                                    weakSelf?.currency = apiResponse
                                    
                                }
                                
                               weakSelf?.tableViewList.reloadData()
                                
                               weakSelf?.collectionViewStatus.reloadData()
                                
                               weakSelf?.collectionViewAttachments.reloadData()
                               
                              weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    

}
extension ReferralManagerStatusVC:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDealCurrencySymbol  {
            
            openCurrencySelector()
            
            return false
        }
        
        return true
    }
}

extension ReferralManagerStatusVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
          return changeLog?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            if let cell:ReferralHistoryCell = tableView.dequeueReusableCell(withIdentifier: "ReferralHistoryCell", for: indexPath) as? ReferralHistoryCell {
                
                return getCellForReferralHistoryCell(cell: cell, indexPath: indexPath)
                
            }
        
        return UITableViewCell.init()
    }
    
    func getCellForReferralHistoryCell(cell:ReferralHistoryCell,indexPath:IndexPath) -> ReferralHistoryCell {
        
        cell.selectionStyle = .none
        
        if let model:LogModel = changeLog?[indexPath.row]{
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.crDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblStatus.text = "\(model.oldStageTitle ?? "")\(" to ")\(model.newStageTitle ?? "")\(" by ")\(model.createdBy ?? "")\(" on ")\(" ")\(dateText)"
            
        }
        
        return cell
    }
    
   
}


extension ReferralManagerStatusVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewStatus
            
        {
            return CGSize.init(width: collectionViewStatus.frame.width, height: collectionViewStatus.frame.height)
            
        }
        
        if collectionView == collectionViewAttachments
            
        {
            return CGSize.init(width: 100, height: 100)
            
        }
        
        return CGSize.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewStatus {
        
              return masterStageList?.count ?? 0
            
        }
        if collectionView == collectionViewAttachments {
            
            return attachments.count 
            
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewAttachments
        {
            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
                
                return getCellForAttachment(cell: cell, indexPath: indexPath)
                
            }
        }
        else {
       
            if let cell:ReferralStatusCollectionCell = collectionViewStatus.dequeueReusableCell(withReuseIdentifier: "ReferralStatusCollectionCell", for: indexPath) as? ReferralStatusCollectionCell {
            
            return getCellForReferralStatusCell(cell: cell, indexPath: indexPath)
            
        
            }
      }
        return UICollectionViewCell.init()
    }
    
    func getCellForReferralStatusCell(cell:ReferralStatusCollectionCell,indexPath:IndexPath) -> ReferralStatusCollectionCell {
        
        if let model:StageListMainModel = masterStageList?[indexPath.row]{
            
            cell.lblStatus.text = model.stageTitle ?? ""
            
            
        }
        
        return cell
    }
    
    func getCellForAttachment(cell:AttachmentPictureCell,indexPath:IndexPath) -> AttachmentPictureCell {
        
        cell.btnDelete.tag = indexPath.row
    //    cell.btnDelete .addTarget(self, action: #selector(deleteImage(_:)), for: .touchUpInside)
        if let fileName = attachments[indexPath.row] as? [String : String]
        {
            if let imagePath = fileName["cdnPath"]
            {
               if let filePath = fileName["fileName"] {
                  
                if let url:URL = URL.init(string: "\(StorageUrl)\(imagePath)"){
                  
                   let strFileType = filePath.fileExtension()

                   if(strFileType.caseInsensitiveCompare("jpg") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("png") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("gif") == ComparisonResult.orderedSame || strFileType.caseInsensitiveCompare("tiff") == ComparisonResult.orderedSame)
                       {
                  
                    cell.imgView.sd_setImage(with: url, completed: nil)
                      
                  }
                   else{
                      cell.imgView.image = UIImage.init(imageLiteralResourceName: "document")
                  }
                  
                }
            }

            }
        }
        return cell
    }
    
}


class ReferralHistoryCell:UITableViewCell{
    
    @IBOutlet weak var lblStatus: NKLabel!
    
    
}


class ReferralStatusCollectionCell:UICollectionViewCell{
    
    @IBOutlet weak var lblStatus: NKLabel!
    
    
}
