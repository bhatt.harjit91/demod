//
//  ReferralPostMessageVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 22/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol ReferralPostMessageVCDelegate: class {
    func didPostedMessage()
}

class ReferralPostMessageVC: AppBaseViewController {

    var messageHistory:[MessageHistoryModel]?
    
    var replyTypes:[ReplyTypeModel]?
    
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var heightOfViewMessages: NSLayoutConstraint!
    @IBOutlet weak var viewRespond: RCView!
    
    @IBOutlet weak var heightViewRespond: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewMessageList: UITableView!
    
    @IBOutlet weak var heightViewMessageHistory: NSLayoutConstraint!
    
    @IBOutlet weak var viewMessagesTitle: RCView!
    
    @IBOutlet weak var viewMessagesList: RCView!
    
    @IBOutlet weak var txtViewMessage: NKTextView!
    
    @IBOutlet weak var heightScrollView: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfSubmitBtnConstraint: NSLayoutConstraint!
    
    weak var delegate:ReferralPostMessageVCDelegate?
    
    var transactionId:Int = 0
    var comingFromArchive:Bool = false
    
    var replyTypeInt:[Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewList.estimatedRowHeight = 50
        
        viewMessagesList.alpha = 0
        viewMessagesTitle.alpha = 0
        
        navigationItem.title = "Messages".localized()
        
        btnSubmit.setTitle("Submit".localized(), for: .normal)
        
        txtViewMessage.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        fetchMessageList()
       
    }
    
    func updateUI() {
        
        if messageHistory?.count ?? 0 > 0 {
            
            DispatchQueue.main.async(execute: {
                //This code will run in the main thread:
                var frame = self.tableViewMessageList.frame
                frame.size.height = self.tableViewMessageList.contentSize.height
                self.tableViewMessageList.frame = frame
                
                self.heightViewMessageHistory.constant = self.tableViewMessageList.contentSize.height + 40
                
            })
            
            viewMessagesList.alpha = 1
            viewMessagesTitle.alpha = 1
        }
        
        if replyTypes?.count ?? 0 > 0 {
            
            replyTypes?.first?.isSelected = 1
            
            DispatchQueue.main.async(execute: {
              
                var secframe = self.tableViewList.frame
                secframe.size.height = self.tableViewList.contentSize.height
                self.tableViewList.frame = secframe
                self.heightViewRespond.constant = self.tableViewList.contentSize.height + 40
            })
        
        }
        
        DispatchQueue.main.async(execute: {
            
            if self.comingFromArchive == true {
                
                self.heightViewRespond.constant = 0.0
                self.viewRespond.isHidden = true
                self.heightOfViewMessages.constant = 0.0
                self.viewMessage.isHidden = true
                self.btnSubmit.isHidden = true
                self.btnSubmit.isUserInteractionEnabled = false
                self.heightOfSubmitBtnConstraint.constant = 0.0
            }
        })
    }
    
    func upadteNavBar() {
        
        let btnName = UIButton()
        btnName.setTitle("Submit -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(postMessage), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    @IBAction func submitMessageAction(_ sender: Any) {
        
        postMessage()
        
    }
    
    
    @objc func postMessage(){
        
        
        for model in replyTypes ?? []{
                   
            if model.isSelected == 1 {
                   
               replyTypeInt.append(model.replyType ?? 0)
                       
             }
          }
        
        if txtViewMessage.text.count == 0{
            
            txtViewMessage.showError(message: "Required")
        }
        
        if txtViewMessage.text.count > 0 && replyTypeInt.count > 0 {
         
             submitMessage()
            
        }
        else{
            replyTypeInt.removeAll()
        }
        
    }
    
    func fetchMessageList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/messageHistory?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&transactionId=\(transactionId)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(MessageHistoryListApiModel.self, from: data)
                                
                                if let apiResponse:[MessageHistoryModel] = jsonResponse.messageHistory {
                                    
                                    weakSelf?.messageHistory = apiResponse
                                    
                                }
                                
                                if let apiResponse:[ReplyTypeModel] = jsonResponse.replyTypes {
                                    
                                     weakSelf?.replyTypes = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                 //   weakSelf?.masterStageList = apiResponse
                                    
                                }
                                
                                if weakSelf?.comingFromArchive == true {
                                    
                                    weakSelf?.tableViewMessageList.reloadData()
                                    
                                    weakSelf?.updateUI()
                                }
                                else
                                {
                                    weakSelf?.tableViewList.reloadData()
                                    weakSelf?.tableViewMessageList.reloadData()
                                    
                                    weakSelf?.updateUI()
                                }

                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    func submitMessage(){
        
        startAnimating()
        
        let params:NSDictionary = ["transactionId":transactionId,
                                   "message":txtViewMessage.text ?? "",
                                   "replyTypes":replyTypeInt
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/composeMessage?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                            let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                            //let decoder = JSONDecoder()
                            let decoder = JSONDecoder()
                            let dateFormat:DateFormatter = DateFormatter.init()
                            dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                            decoder.dateDecodingStrategy = .formatted(dateFormat)
                            
                              weakSelf?.stopAnimating()
                                
                                let isUpdated:[String: Int] = ["update": 1]

                                                                                                           
                                                                                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callMessage"), object: nil, userInfo: isUpdated)
                            
                             weakSelf?.delegate?.didPostedMessage()
                                
                            weakSelf?.navigationController?.popViewController(animated: true)
                            
                            return
                            
                        } catch {
                            print(error)
                            message = failedToConnectMessage.localized()
                        }
                       }
                        
                    }
                }
            }
            
              weakSelf?.stopAnimating()
            
            //Show error
            //  weakSelf?.showErrorMessage(message: message)
            
        })
        
          weakSelf?.stopAnimating()
    }
    
    
}

extension ReferralPostMessageVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tableViewList {
        
            return replyTypes?.count ?? 0
            
        }
        if tableView == tableViewMessageList {
            
            return messageHistory?.count ?? 0
            
        }
       
         return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableViewList {
            
            if let model:ReplyTypeModel = replyTypes?[indexPath.row]{
                
                if model.isSelected == 0 {
                    
                    model.isSelected = 1
                    
                }
                else{
                    model.isSelected = 0
                }
                
                 tableViewList.reloadData()
               // model.isSelected = indexPath.row
                
            }
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
    if tableView == tableViewList {
        
            if let cell:ReferralTypeCell = tableView.dequeueReusableCell(withIdentifier: "ReferralTypeCell", for: indexPath) as? ReferralTypeCell {
                
                return getCellForReferralType(cell: cell, indexPath: indexPath)
                
            }
        }
     
    if tableView == tableViewMessageList {
            
            if let cell:ReferralMessageListCell = tableView.dequeueReusableCell(withIdentifier: "ReferralMessageListCell", for: indexPath) as? ReferralMessageListCell {
            
             return getCellForReferralMessageList(cell: cell, indexPath: indexPath)
            
            }
            
        }
       
        return UITableViewCell.init()
    }
    
    func getCellForReferralType(cell:ReferralTypeCell,indexPath:IndexPath) -> ReferralTypeCell {
        
        cell.selectionStyle = .none
        
        if let model:ReplyTypeModel = replyTypes?[indexPath.row]{
            
            cell.lblTitle.text = model.title ?? ""
            
            if model.isSelected == 0 {
                
                cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBox")
                
            }
            else{
                cell.imgViewCheckBox.image = UIImage.init(imageLiteralResourceName: "checkBoxFilled")
            }
        }
        
        return cell
    }
    
    func getCellForReferralMessageList(cell: ReferralMessageListCell,indexPath:IndexPath) ->  ReferralMessageListCell {
        
        if let model:MessageHistoryModel =  messageHistory?[indexPath.row] {
            
            cell.lblTitle.text = model.title ?? ""
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatterGet.timeZone = TimeZone(abbreviation: "UTC")
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MMM-yyyy hh:mm a"
            dateFormatterPrint.timeZone = .current
            
            var dateText = ""
            
            if let date = dateFormatterGet.date(from: model.crDate ?? "") {
                
                dateText = dateFormatterPrint.string(from: date)
            }
            
            cell.lblTime.text = dateText
            
            cell.lblMessage.text = model.message ?? ""
            
        }
        
        return cell
    }
    
    
   
}

class ReferralTypeCell: UITableViewCell {
    
    @IBOutlet weak var imgViewCheckBox: UIImageView!
    
    @IBOutlet weak var lblTitle: NKLabel!
    
}

class ReferralMessageListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: NKLabel!
    
    @IBOutlet weak var lblMessage: NKLabel!
    
    @IBOutlet weak var lblTime: NKLabel!
    
}


extension ReferralPostMessageVC:UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
      
        if textView == txtViewMessage{
            
           txtViewMessage.showError(message: "")
            
        }
        
        return true
    }
    
}
