//
//  ReferralLeadsHomeVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 12/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

class ReferralLeadsHomeVC: AppBaseViewController {

    
    @IBOutlet weak var lblCount: NKLabel!
    
    @IBOutlet weak var tableViewList: UITableView!
    
    var referralLeadList:[ReferalModel]?
    
    var type:Int?
    
    var stageType:Int?
    
    var userCount:Int = 0
    
    var strTitle:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = strTitle.localized()
        
    }


    override func viewWillAppear(_ animated: Bool) {
        
        fetchUserList()
        
    }
    
    func fetchUserList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/dashboardDetailData?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&stage=\(0)&type=\(type ?? 0)&stageType=\(stageType ?? 0)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(ReferalApiModel.self, from: data)
                                
                                if let apiResponse:[ReferalModel] = jsonResponse.referralLeadList {
                                    
                                    weakSelf?.referralLeadList = apiResponse
                                    
                                }
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.userCount = apiResponse
                                    
                                    weakSelf?.lblCount.text = ("\("This seller has ")\(weakSelf?.userCount ?? 0)\(" users")")
                                }
                                
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
}
extension ReferralLeadsHomeVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return referralLeadList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:ReferalLeadsCell = tableView.dequeueReusableCell(withIdentifier: "ReferalLeadsCell", for: indexPath) as? ReferalLeadsCell {
            
            return getCellForProductList(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:ReferalModel = referralLeadList?[indexPath.row] {
            
            let storyBoard = UIStoryboard.init(name: ReferalSB, bundle: nil)
            let newProductPage:ReferalManagerVC = storyBoard.instantiateViewController(withIdentifier: "ReferalManagerVC") as! ReferalManagerVC
            newProductPage.transactionId = model.transactionId ?? 0
            navigationController?.pushViewController(newProductPage, animated: true)
            
        }
        
    }
    
    func getCellForProductList(cell:ReferalLeadsCell,indexPath:IndexPath) -> ReferalLeadsCell {
        
        cell.selectionStyle = .none
        
        if let model:ReferalModel = referralLeadList?[indexPath.row] {
            
            if model.productLogo?.count ?? 0 > 0 {
                cell.imgLogo.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(model.productLogo ?? "")")), completed: nil)
            }
            cell.lblTime.text = model.referredDate ?? ""
            cell.lblProductName.text = model.productName
            cell.lblCompanyName.text = model.sellerCompanyName ?? ""
//            cell.lblReferralFee.text = "Referral".localized()
//            cell.lblReferralValue.alpha = 0
//            if model.referralFee?.count ?? 0 > 0 {
//            cell.lblReferralValue.text = "\(model.referralFee ?? "")"
//                cell.lblReferralValue.alpha = 1
//            }
        
            cell.lblStatusTitle.text = "Status".localized()
            cell.lblStatusValue.text = model.stageTitle ?? ""
        }
        
        return cell
    }
}


