//
//  ReferralMessagePopUp.swift
//  iCanRefer
//
//  Created by TalentMicro on 04/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

protocol ReferralMessagePopUpDelegate: class {
    func didSendText(transId:Int,text:String)
}

class ReferralMessagePopUp: UIView {

     let kCONTENT_XIB_NAME = "ReferralMessagePopUp"
        
    @IBOutlet var contentView: UIView!
       
    @IBOutlet weak var btnCancel: NKButton!
    
     @IBOutlet weak var btnSend: NKButton!
    
     @IBOutlet weak var txtViewMessage: NKTextView!
    
    @IBOutlet weak var lblNavTitle: NKLabel!
    
    var transactionID:Int = 0
    
    weak var delegate:ReferralMessagePopUpDelegate?
    
    override init(frame: CGRect) {
            super.init(frame: frame)
        
            commonInit()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonInit()
        }
        
        func commonInit() {
            Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
            contentView.fixInView(self)
        }

        
        @IBAction func btnCancelAction(_ sender: Any) {
            
                self.removeFromSuperview()
            
        }
    
    @IBAction func btnSendAction(_ sender: Any){
        
        if txtViewMessage.text.count > 0 {
            
            self.delegate?.didSendText(transId: transactionID, text: txtViewMessage.text ?? "")
            
            self.removeFromSuperview()
        }
        
    }

}
