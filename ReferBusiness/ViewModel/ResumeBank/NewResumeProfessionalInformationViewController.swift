//
//  NewResumeProfessionalInformationViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 25/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class NewResumeProfessionalInformationViewController: AppBaseViewController {
//MARK:Labels for Localization
    
    @IBOutlet weak var labelPresentEmployer: NKLabel!
    @IBOutlet weak var labelNoticePeriod: NKLabel!
    @IBOutlet weak var labelPresentSalary: NKLabel!
    @IBOutlet weak var labelExpectedSalary: NKLabel!
    
    //MARK:Text feilds
    @IBOutlet weak var txtPresentEmployer: RCTextField!
    @IBOutlet weak var txtNoticePeriod: RCTextField!
    @IBOutlet weak var txtNoticePeriodUnit: RCTextField!
    @IBOutlet weak var txtPresentSalaryCurrency: RCTextField!
    @IBOutlet weak var txtPresentSalary: RCTextField!
    @IBOutlet weak var txtPresentSalaryUnit: RCTextField!
    @IBOutlet weak var txtExpectedSalaryCurrency: RCTextField!
    @IBOutlet weak var txtExpectedSalary: RCTextField!
    @IBOutlet weak var txtExpectedSalaryUnit: RCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.tabBarController?.title = "Professional"
    }
     
}
 
 
