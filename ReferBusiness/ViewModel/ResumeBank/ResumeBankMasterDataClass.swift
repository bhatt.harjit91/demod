//
//  ResumeBankMasterDataClass.swift
//  iCanRefer
//
//  Created by Hirecraft on 27/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class ResumeBankMasterDataClass: UIViewController {
     var resumeMasterData:ResumeMasterDataModel?
     
      //MARK: webService
    
         func searchPartner(completionHandler: @escaping (ResumeMasterDataModel?, String?) -> Void){
             
            let params:NSDictionary = [:]
 
             let encryptedParams = ["data": encryptParams(params: params)]
             let apiClient:APIClient = APIClient()
             weak var weakSelf = self
             apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/resumeDetailsWithMaster?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
                 
                 var message:String = failedToConnectMessage.localized()
                 if let serverMessage = jsonObj?.message(){
                     message = (serverMessage.count>0) ? serverMessage : message
                 }
                 
               //  weakSelf?.stopAnimating()
//
//                serialQueue.enter()
                 
                 if let response = response {
                     
                    if  self.validateStatus(response: response) {
                         if let dataStr:String =  jsonObj?.dataString(){
                             
                             if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                                 do {
                                     let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                     print(dataObj)
                                     //let decoder = JSONDecoder()
                                     let decoder = JSONDecoder()
                                     let dateFormat:DateFormatter = DateFormatter.init()
                                     dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                     //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                     decoder.dateDecodingStrategy = .formatted(dateFormat)
                                      let serialQueue = DispatchGroup()
                                      serialQueue.enter()
                                    
                                    do{
                                       self.resumeMasterData = try decoder.decode(ResumeMasterDataModel.self, from: data)
                                    }catch{
                                        print("\(error)xxcxcxcxccxcxcxcxcxcxcxcxcxccxcxcxcxcxcxccxcxcxcxcxcxccxcxcxcxc")
                                    }
                                    
                                    serialQueue.leave()
                                    serialQueue.enter()
                                    completionHandler(weakSelf?.resumeMasterData, nil)
                                    serialQueue.leave()
                                     
                                 } catch {
                                     print(error)
                                     message = failedToConnectMessage.localized()
                                     completionHandler(nil, message)
                                 }
                             }
                         }
                     }else{
                        
                         completionHandler(nil, message)
                    }
                 }
                  
                 //Show error
               //   completionHandler(weakSelf?.resumeMasterData, message)
                 //weakSelf?.showErrorMessage(message: message)
                 
             })
              
         }
}
