
//
//  CVManagerSkill&EducationViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 25/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class CVManagerSkillAndEducationViewController: AppBaseViewController {
    @IBOutlet weak var tblVwEducation:UITableView!
    @IBOutlet weak var tblVwSkills:UITableView!
    @IBOutlet weak var heightConstraintTblVwEducation: NSLayoutConstraint!
    @IBOutlet var btMinusPlus: [UIButton]!
    
    @IBOutlet weak var txtExperience: RCTextField!
    @IBOutlet weak var txtVwCertificationOrAcheivements: NKTextView!
    
    @IBOutlet weak var heightConstraintSkillTblVw: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        heightConstraintTblVwEducation.constant = 40 + 96 + 96
        heightConstraintSkillTblVw.constant = 40 + 46 + 46
        roundCorner(btMinusPlus)
         
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.tabBarController?.title = "Skills & Education"
    }
    
    func roundCorner(_ vw:[UIButton]){
           for btn in vw{
               btn.layer.cornerRadius = btn.frame.height / 2
           }
    }
    
    @IBAction func btMinusExperience(_ sender: UIButton) {
           if Int(txtExperience.text ?? "") ?? 0 > 0{
               txtExperience.text =  String((Int(txtExperience.text ?? "") ?? 0) - 1)
           }
       }
       @IBAction func btPlusExperience(_ sender: UIButton) {
           txtExperience.text =  String((Int(txtExperience.text ?? "") ?? 0) + 1)
       }
    
    @IBAction func btAttachment(_ sender: UIButton) {
        if sender.tag == 111{
            //Education
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewSkillSetViewController") as? NewSkillSetViewController
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
     ////END//////
}

extension CVManagerSkillAndEducationViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblVwEducation{
            return 94
        }else{
            return 46
        }
       
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CVManagerSkillAndEducationHeaderTblVwCell") as? CVManagerSkillAndEducationHeaderTblVwCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblVwEducation{
             let cell = tableView.dequeueReusableCell(withIdentifier: "CVManagerSkillAndEducationTblVwCell", for: indexPath) as? CVManagerSkillAndEducationTblVwCell
             return cell!
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedSkillsCell", for: indexPath) as? SelectedSkillsCell
             return cell!
        }
       
    }
    
    
}

class CVManagerSkillAndEducationTblVwCell: UITableViewCell {
    @IBOutlet weak var txtStudy:RCTextField!
    @IBOutlet weak var txtStudyStream:RCTextField!
    @IBOutlet weak var txtStudyInstitute:RCTextField!
    @IBOutlet weak var txtPercentage:RCTextField!
    @IBOutlet weak var txtStudyCompletionYear:RCTextField!
}
class CVManagerSkillAndEducationHeaderTblVwCell: UITableViewCell {
    @IBOutlet weak var lblTitle: NKLabel!
}
