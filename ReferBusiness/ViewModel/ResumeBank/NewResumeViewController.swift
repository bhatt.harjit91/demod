//
//  NewResumeViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 24/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation
class NewResumeViewController: AppBaseViewController,CountryCodeSelectViewControllerDelegate, CLLocationManagerDelegate {
    //MARK:- Constraint height
    @IBOutlet weak var heightContraintAlternativeNo: NSLayoutConstraint!
    @IBOutlet weak var heightContraintAffirmativeAction: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintMainView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintCollVwBgJobType: NSLayoutConstraint!
    
    
    
    //MARK:- textFeilds
    @IBOutlet weak var txtFirstName: RCTextField!
    @IBOutlet weak var txtLastName: RCTextField!
    @IBOutlet weak var txtMobileNo: RCTextField!
    @IBOutlet weak var txtWhatsAppNo: RCTextField!
    @IBOutlet weak var txtEmailId: RCTextField!
    @IBOutlet weak var txtAlternativeNo: RCTextField!
    @IBOutlet weak var txtBirthday: RCTextField!
    @IBOutlet weak var txtInterestedAfter: RCTextField!
    @IBOutlet weak var txtMobileCountryCode: RCTextField!
    @IBOutlet weak var txtWhatsAppCountryCode: RCTextField!
    @IBOutlet weak var txtAltNoCountryCode: RCTextField!
    
    //MARK:- labels
    
    @IBOutlet weak var lblCVAttachment: NKLabel!
    @IBOutlet weak var lblMobileNO: NKLabel!
    @IBOutlet weak var lblWhatsAppNo: NKLabel!
    @IBOutlet weak var lblAlternateNo: NKLabel!
    @IBOutlet weak var lblAffirmativeAction: NKLabel!
    @IBOutlet weak var lblIfJobSeekerManageCalls: NKLabel!
    @IBOutlet weak var lblGender: NKLabel!
    @IBOutlet weak var lblBirthDate: NKLabel!
    @IBOutlet weak var lblStatus: NKLabel!
    @IBOutlet weak var lblInterestedAfter: NKLabel!
    @IBOutlet weak var lblPreferedJobTypes: NKLabel!
    @IBOutlet weak var lblPresentLocation: NKLabel!
    @IBOutlet weak var lblPreferedLocationsForJob: NKLabel!
    @IBOutlet weak var lblPreferedLocationSelectLocation: NKLabel!
    
    //MARK:- CollectionView
    @IBOutlet weak var collVwStatus: UICollectionView!
    @IBOutlet weak var collVwPreferedJobTypes: UICollectionView!
    @IBOutlet weak var collVWGender:UICollectionView!
    //////
    @IBOutlet weak var imgVwProfilePic: UIImageView!
    @IBOutlet weak var imgVwAttachPic: UIImageView!
    
    @IBOutlet weak var imbVwCheckBoxAlternativeNo: UIImageView!
    @IBOutlet weak var imgVwCheckBoxAffirmativeAction: UIImageView!
    
    @IBOutlet var ShadowView: [UIView]!
    @IBOutlet var imgVwLocationIcon: [UIImageView]!
    
    
    //MARK: ImagePicker
    typealias didFinishUploadingImage = (Bool,String) -> Void
    var didUploadImage:didFinishUploadingImage?
    
    typealias didFinishPickingImage = (UIImage) -> Void
    var didPickImage:didFinishPickingImage?
    
    var imagePicker = UIImagePickerController()
    
    //MARK:CountryCode
    var selectedCountryCode : CountryCodeModel?
    var selectedCountryCodeTxtFldTag = -1
    
    //MARK:Data
    var resumeMasterData:ResumeMasterDataModel?
     
        class CellSelected {
            static var gender = -1
            static var status = -1
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
        txtMobileCountryCode.delegate = self
        txtWhatsAppCountryCode.delegate = self
        txtAltNoCountryCode.delegate = self
        
        txtMobileCountryCode.tag = 101
        txtWhatsAppCountryCode.tag = 102
        txtAltNoCountryCode.tag = 103
        
        self.startAnimating()
        loadDataForUI()
        // Do any additional setup after loading the view.
    }
    
    deinit {
        print("deinit")
    }
    func loadDataForUI(){
        ResumeBankMasterDataClass().searchPartner { (data, message) in
            weak var weakSelf = self
            self.stopAnimating()
            if data != nil{
                weakSelf?.resumeMasterData = data
                weakSelf?.updateUIdata()
            }else{
                weakSelf?.showErrorMessage(message: message ?? "")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.title = "Personel"
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewSkillSetViewController") as? NewSkillSetViewController
          self.navigationController?.pushViewController(vc!, animated: false)
        
        for vw in ShadowView{
            //vw.layer.borderColor = UIColor.lightGray.cgColor
            vw.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
            vw.layer.borderWidth = 1
        }
    }
    func updateUIdata(){
        collVwStatus.reloadData()
        collVWGender.reloadData()
        collVwPreferedJobTypes.reloadData()
        let newHeight : CGFloat = self.collVwPreferedJobTypes.collectionViewLayout.collectionViewContentSize.height
        heightConstraintCollVwBgJobType.constant = newHeight
        collVwPreferedJobTypes.reloadData()
         
    }
     
    
    func updateUI(){
        self.imgVwAttachPic.setImageColor(color: .white)
        self.imgVwLocationIcon[0].setImageColor(color: .white)
        self.imgVwLocationIcon[1].setImageColor(color: .white)
        btnAlternativeNo(UIButton())
        btnAffirmativeAction(UIButton())
    }
    //MARK:CountryCode
    func didFinishSelectingCountryCode(selectedCountry: CountryCodeModel) {
        selectedCountryCode = selectedCountry
        if selectedCountryCodeTxtFldTag == txtMobileCountryCode.tag{
            txtMobileCountryCode.text = selectedCountry.dial_code
        }else if selectedCountryCodeTxtFldTag == txtWhatsAppCountryCode.tag{
            txtWhatsAppCountryCode.text = selectedCountry.dial_code
        }else{
            txtAltNoCountryCode.text = selectedCountry.dial_code
        }
        
    }
    func openCountrySelector() {
        let storyBoard = UIStoryboard.init(name: CountryCodeSB, bundle: nil)
        let countrySelector:CountryCodeSelectViewController = storyBoard.instantiateViewController(withIdentifier: "CountryCodeSelectViewController") as! CountryCodeSelectViewController
        countrySelector.selectedCountryCode = selectedCountryCode
        countrySelector.delegate = self
        navigationController?.pushViewController(countrySelector, animated: true)
    }
    
    //MARK: ButtonActions
    
    @IBAction func btPresentLocation(_ sender: UIButton) {
        openMap()
    }
    @IBAction func btPreferedLocationForJob(_ sender: UIButton) {
    }
    
    @IBAction func btnImagePicker(_ sender: UIButton) {
        openImagePicker()
    }
    
    @IBAction func btnAlternativeNo(_ sender: UIButton) {
        
        if heightContraintAlternativeNo.constant == 0{
            heightContraintAlternativeNo.constant = 55
            heightConstraintMainView.constant =  heightConstraintMainView.constant + heightContraintAlternativeNo.constant
            txtAlternativeNo.superview!.superview!.alpha = 1
            imbVwCheckBoxAlternativeNo.image = UIImage(named: "checkBoxFilled")
        }else{
            heightConstraintMainView.constant =  heightConstraintMainView.constant + heightContraintAlternativeNo.constant
            heightContraintAlternativeNo.constant = 0
            txtAlternativeNo.superview!.superview!.alpha = 0
            imbVwCheckBoxAlternativeNo.image = UIImage(named: "checkBox")
            
        }
        
    }
    @IBAction func btnAffirmativeAction(_ sender: UIButton) {
        if heightContraintAffirmativeAction.constant == 0{
            heightContraintAffirmativeAction.constant = 126
            lblGender.superview?.alpha = 1
            imgVwCheckBoxAffirmativeAction.image = UIImage(named: "checkBoxFilled")
            
        }else{
            heightContraintAffirmativeAction.constant = 0
            lblGender.superview?.alpha = 0
            imgVwCheckBoxAffirmativeAction.image = UIImage(named: "checkBox")
        }
        
    }
    @IBAction func btnPresentLocation(_ sender: UIButton) {
    }
    
    @IBAction func btnPreferedLocationForJob(_ sender: UIButton) {
    }
    @IBAction func btAttachCV(_ sender: UIButton) {
        openImportDocumentPicker()
    }
    
}


extension NewResumeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collVwStatus{
            return CGSize(width: (( resumeMasterData?.resumeStatusList?[indexPath.row].title)?.size(withAttributes: nil).width ?? 0) + 30   , height: 40.0)
        }
        if collectionView == collVwPreferedJobTypes{
              return CGSize(width: (( resumeMasterData?.jobTypeList?[indexPath.row].title)?.size(withAttributes: nil).width ?? 0) + 30   , height: 40.0)
        }
        if collectionView == collVWGender{
              return CGSize(width: (( resumeMasterData?.genderList?[indexPath.row].title)?.size(withAttributes: nil).width ?? 0) + 30   , height: 40.0)
        }
        return CGSize.init(width: collVwStatus.frame.width / 4, height: 40.0)
        
        // return CGSize.init()
    }
     

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collVwStatus{
            return resumeMasterData?.resumeStatusList?.count ?? 0
        }
        if collectionView == collVWGender{
            return resumeMasterData?.genderList?.count ?? 0
        }
        if collectionView == collVwPreferedJobTypes{
            return resumeMasterData?.jobTypeList?.count ?? 0
        }
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collVwPreferedJobTypes {
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                return getCellForPreferedJobTypes(cell: cell, indexPath: indexPath)
            }
        }
        if collectionView == collVWGender {
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                return getCellForGender(cell: cell, indexPath: indexPath)
            }
        }
        if collectionView == collVwStatus  {
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                return getCellForStatus(cell: cell, indexPath: indexPath)
            }
        }
        return UICollectionViewCell.init()
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collVwStatus {
               CellSelected.status = indexPath.row
            
             if resumeMasterData?.resumeStatusList?[indexPath.row].selectedCell == 1 {
                resumeMasterData?.resumeStatusList?[indexPath.row].selectedCell = 0
             } else{
                 resumeMasterData?.resumeStatusList?[indexPath.row].selectedCell = 1
             }
                     
            collVwStatus.reloadData()
        }
        if collectionView == collVWGender{
              CellSelected.gender = indexPath.row
            
            if resumeMasterData?.genderList?[indexPath.row].selectedCell == 1 {
               resumeMasterData?.genderList?[indexPath.row].selectedCell = 0
            } else{
                resumeMasterData?.genderList?[indexPath.row].selectedCell = 1
            }
            
            collVWGender.reloadData()
        }
        if collectionView == collVwPreferedJobTypes{
            
            if resumeMasterData?.jobTypeList?[indexPath.row].selectedCell == 1 {
              resumeMasterData?.jobTypeList?[indexPath.row].selectedCell = 0
            } else{
                resumeMasterData?.jobTypeList?[indexPath.row].selectedCell = 1
            }
            
            collVwPreferedJobTypes.reloadData()
        }
        
    }
    func getCellForPreferedJobTypes(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        cell.viewCellBackground.backgroundColor = .white
        cell.lblTitle.textColor = .primaryColor
        cell.lblTitle.text = resumeMasterData?.jobTypeList?[indexPath.row].title ?? ""
        
        if  resumeMasterData?.jobTypeList?[indexPath.row].selectedCell == 1 {
            cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
            cell.lblTitle.textColor = .white
        } else{
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
        }
        
        return cell
    }
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        cell.viewCellBackground.backgroundColor = .white
        cell.lblTitle.textColor = .primaryColor
        cell.lblTitle.text = resumeMasterData?.resumeStatusList?[indexPath.row].title ?? ""
        
        if  CellSelected.status == indexPath.row {
            cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
            cell.lblTitle.textColor = .white
        } else{
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
        }
        
        return cell
    }
    
    
    func getCellForGender(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        cell.viewCellBackground.backgroundColor = .white
        cell.lblTitle.textColor = .primaryColor
        cell.lblTitle.text = resumeMasterData?.genderList?[indexPath.row].title
        
        if CellSelected.gender == indexPath.row{
          
             cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
             cell.lblTitle.textColor = .white
        } else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
        }
         
        
        return cell
    }
    /////END/////
}

extension NewResumeViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    func openImagePicker() {
        weak var weakSelf = self
        let actionSheet:UIAlertController = UIAlertController.init(title: "Select".localized(), message: "", preferredStyle: .actionSheet)
        actionSheet.view.tintColor = UIColor.btnLinkColor
        actionSheet.addAction(UIAlertAction.init(title: "Camera".localized(), style: .default, handler: {action in
            weakSelf?.checkForCamera()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Photo Library".localized(), style: .default, handler: {action in
            weakSelf?.checkForPhotos()
        }))
        
        
        actionSheet.addAction(UIAlertAction.init(title: "Cancel".localized(), style: .default, handler: {action in
            if let closure =  weakSelf?.didUploadImage {
                closure(false,"")
            }
            
        }))
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            present(actionSheet, animated: true, completion: nil)
            break
            
        case .pad:
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            present(actionSheet, animated: true, completion: nil)
            break
            
        case .unspecified:
            break
        case .tv:
            break
        case .carPlay:
            break
        }
    }
    
    
    func checkForCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(.camera)) {
            openCamera()
        } else {
            let actionController: UIAlertController = UIAlertController(title: "Camera is not available".localized(),message: "", preferredStyle: .alert)
            actionController.view.tintColor = UIColor.btnLinkColor
            let cancelAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .cancel) { action -> Void  in
            }
            actionController.addAction(cancelAction)
            self.present(actionController, animated: true, completion: nil)
        }
    }
    func checkForPhotos(){
        if(UIImagePickerController.isSourceTypeAvailable(.photoLibrary)) {
            openPhotLibrary()
        } else {
            let actionController: UIAlertController = UIAlertController(title: "Photos are not available".localized(),message: "", preferredStyle: .alert)
            actionController.view.tintColor = UIColor.btnLinkColor
            let cancelAction: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .cancel) { action -> Void  in
            }
            actionController.addAction(cancelAction)
            self.present(actionController, animated: true, completion: nil)
        }
    }
    
    func  openCamera() {
        weak var weakSelf = self
        checkCameraPermisson(completionHander: {status in
            if status{
                weakSelf?.openImagePIcker(type: 0)
            }
        })
    }
    
    func openPhotLibrary() {
        weak var weakSelf = self
        checkPhotoLibraryPermission(completionHander: {status in
            if status{
                weakSelf?.openImagePIcker(type: 1)
            }
        })
    }
    
    func openImagePIcker(type:Int) {
        imagePicker = UIImagePickerController.init()
        imagePicker.sourceType = type == 1 ? .photoLibrary : .camera
        //imagePicker.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor : UIColor.white]
        imagePicker.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
        present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let tempImage:UIImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            imgVwProfilePic.image = tempImage
            if let closure =  didPickImage {
                closure(tempImage)
            }
            
            imagePicker.dismiss(animated: true, completion: nil)
        }
    }
}
extension NewResumeViewController: UIDocumentPickerDelegate{
    
    func openImportDocumentPicker(){
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: ["public.text", "com.apple.iwork.pages.pages", "public.data"], in: .import)
        
        documentPicker.delegate = self
        
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance(whenContainedInInstancesOf: [UIDocumentBrowserViewController.self]).tintColor = nil
        }
        present(documentPicker, animated: true, completion: nil)
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let cico = url as URL
        print(cico)
        print(url)
        
        print("url.lastPathComponent \(url.lastPathComponent)")
        
        print("url.pathExtension \(url.pathExtension)")
        
        // print(url.pathExtension)
        
        do {
            // inUrl is the document's URL
            let data = try Data(contentsOf: url) // Getting file data here
            
            //   uploadDocument(docData: data, mimeType: mimeTypeForPath(url: url), fileName: url.lastPathComponent)
            
            print(data)
        } catch {
            print("document loading error")
        }
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        print(" cancelled by user")
        dismiss(animated: true, completion: nil)
        
    }
    
}
extension NewResumeViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  textField == txtMobileCountryCode || textField == txtWhatsAppCountryCode  || textField == txtAltNoCountryCode{
            selectedCountryCodeTxtFldTag = textField.tag
            openCountrySelector()
            return false
        }
        
        return true
    }
}

extension NewResumeViewController: LocationPermissionVCNewDelegate{
    func openMap() {
        
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        //  locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        // locationPage.lat = currentLoc?.latitude ?? 0.0
        //  locationPage.lng = currentLoc?.longitude ?? 0.0
        locationPage.modalPresentationStyle = .fullScreen
        navigationController?.present(locationPage, animated: true)
    }
    
    func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        
        //   currentLoc = coordinate
        // updateLocationName()
        // updateLocationViewHeight()
        
    }
}
