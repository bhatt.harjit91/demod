//
//  NewSkillSetViewController.swift
//  iCanRefer
//
//  Created by Hirecraft on 25/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import UIKit

class NewSkillSetViewController: AppBaseViewController {
    @IBOutlet weak var labelSkillCategory: NKLabel!
    @IBOutlet weak var labelSkillLevel: NKLabel!
    @IBOutlet weak var labelExperience: NKLabel!
    @IBOutlet weak var labelSkill: NKLabel!
    @IBOutlet weak var labelYears: NKLabel!
    @IBOutlet weak var btAddSkill: NKButton!
   // @IBOutlet weak var labelSelectedSkills: NKLabel!
    @IBOutlet weak var tblVwExpList: UITableView!
    
    @IBOutlet weak var txtExperience: RCTextField!
    @IBOutlet weak var vwStarSkillLevel: CosmosView!
    @IBOutlet weak var txtAddASkill: RCTextField!
    @IBOutlet weak var txtSkillCategory: RCTextField!
    
     var tableViewFrame = CGRect()
     
 //   @IBOutlet  var btnPlusMinus: [UIButton]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableViewFrame = self.tblVwExpList.frame
        
        tblVwExpList.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        tblVwExpList.layer.borderWidth = 1
        tblVwExpList.layer.cornerRadius = 5
        tblVwExpList.clipsToBounds = true
        self.scrollViewDidScroll(tblVwExpList)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
          self.tableViewFrame = self.tblVwExpList.frame
       // roundCorner(btnPlusMinus)
    }
//    func roundCorner(_ vw:[UIButton]){
//        for btn in vw{
//            btn.layer.cornerRadius = btn.frame.height / 2
//        }
//    }
    
//    @IBAction func btMinusExperience(_ sender: UIButton) {
//        if Int(txtExperience.text ?? "") ?? 0 > 0{
//            txtExperience.text =  String((Int(txtExperience.text ?? "") ?? 0) - 1)
//        }
//    }
//    @IBAction func btPlusExperience(_ sender: UIButton) {
//        txtExperience.text =  String((Int(txtExperience.text ?? "") ?? 0) + 1)
//    }
//
    
}
extension NewSkillSetViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectedSkillHeader")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedSkillsCell", for: indexPath) as? SelectedSkillsCell
        cell?.layoutIfNeeded()
        return cell!
    }
   
}

extension NewSkillSetViewController: UIScrollViewDelegate{
    //MARK:- ScrollView Delegates
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.bringSubviewToFront(tblVwExpList)
        
        UIView.animate( withDuration: 0.5, delay: 0, options: [.curveEaseInOut] ,animations:  {
            if scrollView.contentOffset.y > 0{
                if scrollView.contentOffset.y > 0.5 && self.tblVwExpList.frame.height == self.tableViewFrame.height{
                    self.tblVwExpList.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                    self.tblVwExpList.isScrollEnabled = false
                }
                let height =  UIScreen.main.bounds.height - (self.navigationController?.navigationBar.frame.height ?? 70)!
                self.tblVwExpList.frame = CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.height ?? 70)! + 20, width: UIScreen.main.bounds.width, height: height + 20)
                
            }
        },completion: { bool in
            self.tblVwExpList.isScrollEnabled = true
            
        })
        
        UIView.animate( withDuration: 0.3, delay: 0, options: [.curveEaseIn] ,animations:  {
            if scrollView.contentOffset.y  < 0 && self.tableViewFrame.height > 0{
                self.tblVwExpList.frame = self.tableViewFrame
            }
        })
        
       // print(tableView.contentOffset.y)
    }
}

class SelectedSkillsCell: UITableViewCell {
    @IBOutlet weak var ratingVw: CosmosView!
    @IBOutlet weak var lblExperience: RCTextField!
    @IBOutlet weak var labelExperience: NKLabel!
    @IBOutlet weak var btCross: UIButton!
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var lblSubTitle: NKLabel!
    @IBOutlet weak var lblYearsExperience: NKLabel!
    
     
    
    override func layoutIfNeeded() {
        vwBg.layer.cornerRadius = 6.0
        vwBg.layer.borderWidth = 0.5
        vwBg.layer.borderColor = UIColor.lightGray.cgColor
        vwBg.layer.masksToBounds = true
         
//        vwBg.layer.shadowColor = UIColor.black.cgColor
//        vwBg.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
//        vwBg.layer.shadowRadius = 1
//        vwBg.layer.shadowOpacity = 0.5
//        vwBg.layer.masksToBounds = false
//        vwBg.layer.shadowPath = UIBezierPath(roundedRect: vwBg.bounds, cornerRadius:  vwBg.layer.cornerRadius ).cgPath
    }
    
}

class selectedSkillHeader:UITableViewCell{
    
}
