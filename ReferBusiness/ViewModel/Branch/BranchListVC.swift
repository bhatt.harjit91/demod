//
//  BranchListVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 10/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

protocol BranchListVCDelegate: class {
    func didAddedNewBranch()
    func didSelectedBranch(withModel:BranchModel)
    func didSelectedPreferredBranch(withModel:BranchModel)
}

extension BranchListVCDelegate{
    
    func didSelectedBranch(withModel:BranchModel) {
        
    }
    
    func didSelectedPreferredBranch(withModel:BranchModel){
        
    }
    
}

class BranchListVC: AppBaseViewController,NewBranchVCDelegate {
    
    @IBOutlet weak var tableViewList: UITableView!
    
    @IBOutlet weak var lblBranchCount: NKLabel!
    
    @IBOutlet weak var lblNoData: NKLabel!
    
    var branchList:[BranchModel]?
    
    var branchCount:Int = 0
    
    var sellerCode:Int = 0
    
    var colorCodes:ColorModel?
    
    weak var delegate: BranchListVCDelegate?
    
    weak var delegate2: BranchListVCDelegate?
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    var isFromPopUp:Int = 0
    
    var appointmentTemplateList:[BranchAppointmentModel]?
    
    var timeZoneList:[BranchTimeModel]?
    
    var btnName = UIButton()
    
    var jobLocationTypeList:[BranchLocationModel]?
    
    var isFromJob:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        navigationItem.title = "Branches".localized()
        
        lblNoData.text = "No Data Found".localized()
        
        
        btnName.alpha = 0
        
        if isAdmin == 1 || isPartnerForCompany == 1 {
            
            btnName.alpha = 1
        }
        
        fetchBranchList()
    }
    
    func didAddedBranch() {
        
        fetchBranchList()
        
        delegate?.didAddedNewBranch()
        
    }
    
    func setRightBarButtonItem(){
        //  btnName.setImage(UIImage(named: "walletIcon"), for: .normal)
        btnName.setTitle("New -->".localized(), for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(newBranchAction(sender:)), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    
    @objc func newBranchAction(sender: UIButton){
        
        openBranchCreationPage()
        
    }
    
    func openBranchCreationPage(){
        
        let storyBoard = UIStoryboard.init(name: BranchSB, bundle: nil)
        let newBranchPage:NewBranchVC = storyBoard.instantiateViewController(withIdentifier: "NewBranchVC") as! NewBranchVC
        newBranchPage.sellerCode = sellerCode
        newBranchPage.branchList = branchList
        newBranchPage.delegate = self
        newBranchPage.fromPopUp = isFromPopUp
        newBranchPage.appointmentTemplateList = appointmentTemplateList
        newBranchPage.timeZoneList = timeZoneList
        newBranchPage.jobLocationTypeList = jobLocationTypeList
        newBranchPage.isFromJob = isFromJob
        navigationController?.pushViewController(newBranchPage, animated: true)
        
    }
    
    func fetchBranchList() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/branchList?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            weakSelf?.setRightBarButtonItem()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(BranchApiModel.self, from: data)
                                
                                if let apiResponse:[BranchModel] = jsonResponse.branchList {
                                    
                                    weakSelf?.branchList = apiResponse
                                    
                                    if weakSelf?.branchList?.count ?? 0 == 0{
                                        
                                        if weakSelf?.isAdmin == 1 || weakSelf?.isPartnerForCompany == 1 {
                                            
                                            weakSelf?.openBranchCreationPage()
                                            
                                        }
                                    }
                                    
                                }
                                
                                if let apiResponse:[BranchAppointmentModel] = jsonResponse.appointmentTemplateList {
                                    
                                    weakSelf?.appointmentTemplateList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[BranchTimeModel] = jsonResponse.timeZoneList {
                                    
                                    weakSelf?.timeZoneList = apiResponse
                                    
                                }
                                
                                if let apiResponse:[BranchLocationModel] = jsonResponse.jobLocationTypeList {
                                    
                                    weakSelf?.jobLocationTypeList = apiResponse
                                    
                                }
                                
                                if let apiResponse:Int = jsonResponse.count {
                                    
                                    weakSelf?.branchCount = apiResponse
                                    
                                    weakSelf?.lblBranchCount.text = ("\("This seller has ".localized())\(weakSelf?.branchCount ?? 0)\(" Branches")")
                                }
                                
                                
                                weakSelf?.tableViewList.reloadData()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
}

extension BranchListVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = branchList?.count ?? 0
        
        if count > 0 {
            
            tableViewList.alpha = 1
            
            return branchList?.count ?? 0
            
        }
            
        else{
            
            tableViewList.alpha = 0
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:BranchListCell = tableView.dequeueReusableCell(withIdentifier: "BranchListCell", for: indexPath) as? BranchListCell {
            
            return getCellForBranchList(cell: cell, indexPath: indexPath)
            
        }
        
        return UITableViewCell.init()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let model:BranchModel = branchList?[indexPath.row]
        {
            if isFromJob == 1 {
              
                delegate?.didSelectedBranch(withModel:model)
                
                navigationController?.popViewController(animated: true)
            }
            else if isFromJob == 2 {
                
                delegate2?.didSelectedPreferredBranch(withModel: model)
                
                navigationController?.popViewController(animated: true)
            }
                
            else {
                let storyBoard = UIStoryboard.init(name: BranchSB, bundle: nil)
                let newBranchPage:NewBranchVC = storyBoard.instantiateViewController(withIdentifier: "NewBranchVC") as! NewBranchVC
                newBranchPage.sellerCode = model.sellerCode ?? 0
                newBranchPage.branchCode = model.branchCode ?? 0
                newBranchPage.branchList = branchList
                newBranchPage.isDetail = true
                newBranchPage.delegate = self
                newBranchPage.isAdmin = isAdmin
                newBranchPage.isPartnerForCompany = isPartnerForCompany
                newBranchPage.appointmentTemplateList = appointmentTemplateList
                newBranchPage.timeZoneList = timeZoneList
                newBranchPage.jobLocationTypeList = jobLocationTypeList
                
                navigationController?.pushViewController(newBranchPage, animated: true)
                
            }
        }
        
    }
    
    func getCellForBranchList(cell:BranchListCell,indexPath:IndexPath) -> BranchListCell {
        
        cell.selectionStyle = .none
        
        if let model:BranchModel = branchList?[indexPath.row]
        {
            cell.lblBranchNameText.text = model.branchName ?? ""
            cell.lblStatusText.text = model.statusTitle ?? ""
            cell.lblAddressText.text = model.address
            cell.lblType.text = model.jobLocation ?? ""
            cell.viewStatus.alpha = 0
            
            if model.status == 1 {
                
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.activeColor ?? "")
                
                cell.viewStatus.alpha = 1
                
            }
                
            else if model.status == 2 {
                
                cell.viewStatus.backgroundColor = UIColor.colorWithHexString(hexString: colorCodes?.inactiveColor ?? "")
                
                cell.viewStatus.alpha = 1
                
            }
            
        }
        
        return cell
    }
    
    
}

class BranchListCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: RCView!
    
    @IBOutlet weak var lblBranchName: NKLabel!
    
    @IBOutlet weak var lblAddress: NKLabel!
    
    
    @IBOutlet weak var lblBranchNameText: NKLabel!
    
    @IBOutlet weak var lblAddressText: NKLabel!
    
    @IBOutlet weak var lblStatusText: NKLabel!
    
    @IBOutlet weak var viewStatus: RCView!
    
    @IBOutlet weak var lblType: NKLabel!
    
}
