//
//  NewBranchVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 14/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

protocol NewBranchVCDelegate: class {
    func didAddedBranch()
    
}

class NewBranchVC: AppBaseViewController,LocationPermissionVCNewDelegate,CLLocationManagerDelegate {
    
    
    @IBOutlet weak var txtBranchName: RCTextField!
    
    @IBOutlet weak var lblLocation: NKLabel!
    
    @IBOutlet weak var segmentStatus: UISegmentedControl!
    
    @IBOutlet weak var imgViewLoc: UIImageView!
    
    @IBOutlet weak var txtBillingBranch: RCTextField!
    
    @IBOutlet weak var btnSubmit: NKButton!
    
    @IBOutlet weak var heightViewLocation: NSLayoutConstraint!
    
    @IBOutlet weak var lblAddressTitle: NKLabel!
    
    @IBOutlet weak var txtAddressLine1: RCTextField!
    
    @IBOutlet weak var txtCountry: RCTextField!
    
    @IBOutlet weak var txtState: RCTextField!
    
    @IBOutlet weak var txtCity: RCTextField!
    
    @IBOutlet weak var txtPostalCode: RCTextField!
    
    @IBOutlet weak var txtAddressLine2: RCTextField!
    
    @IBOutlet weak var txtAppointmentTemplate: RCTextField!
    
    @IBOutlet weak var txtTimeZone: RCTextField!
    
    @IBOutlet weak var viewTimeZone: RCView!
    
    @IBOutlet weak var heightViewTimeZone: NSLayoutConstraint!
    
    @IBOutlet weak var viewAppointment: RCView!
    
    @IBOutlet weak var heightViewAppointment: NSLayoutConstraint!
    
    @IBOutlet weak var viewBillingBranch: RCView!
    
    @IBOutlet weak var heightViewBillingBranch: NSLayoutConstraint!
    
    @IBOutlet weak var collectionViewType: UICollectionView!
    
    var currentLoc:CLLocationCoordinate2D?
    
    var sellerCode:Int = 0
    
    var branchCode:Int = 0
    
    var billingAddressBranchCode:Int = 0
    
    var status:Int = 1
    
    var branchDetail:BranchDetailModel?
    
    var locationManager = CLLocationManager()
    
    var pickerOptions:[String] = []
    
    var appointmentOptions:[String] = []
    
    var timezone:[String] = []
    
    var branchList:[BranchModel]?
    
    var selectedBranchCode:Int?
    
    var isDetail:Bool = false
    
    weak var delegate:NewBranchVCDelegate?
    
    var btnName = UIButton()
    
    var fromPopUp:Int = 0
    
    var isAdmin:Int?
    
    var isPartnerForCompany:Int?
    
    var appointmentTemplateList:[BranchAppointmentModel]?
    
    var timeZoneList:[BranchTimeModel]?
    
    var appointmentTemplateCode:Int?
    
    var timeZoneId:Int?
    
    var isFromJob:Int = 0
    
    var jobLocationTypeList:[BranchLocationModel]?
    
    var selectedIndex:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentStatus.setSegmentStyle()
        
        getCurrentLocation()
        
        btnSubmit.setTitle("Submit".localized(), for: .normal)
        
        txtBillingBranch.tintColor = UIColor.clear
        
        lblAddressTitle.text = "Address".localized()
        
        ////
        
        let starAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.fontForType(textType: 6),
            .foregroundColor: UIColor.getGradientColor(index: 1).first ?? 0]
        
        let textAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.fontForType(textType: 6),
            .foregroundColor: UIColor.lightGray]
        
        
        let star = NSAttributedString(string:"*" , attributes: starAttributes)
        
        let starTexr = NSMutableAttributedString(string: "Select location".localized() , attributes: textAttributes)
        
        starTexr.append(star)
        
        lblLocation.attributedText = starTexr
        
        ///
        
        lblLocation.textColor = .primaryColor
        
        navigationItem.title = "Branch".localized()
        
        imgViewLoc.setImageColor(color: UIColor.white)
        
        txtBranchName.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Branch Name".localized())
        
        txtAddressLine1.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Address line 1".localized())
        
         txtCountry.attributedPlaceholder = addPlaceHolderWithText(placeholder: "Country".localized())
        
         txtState.attributedPlaceholder = addPlaceHolderWithText(placeholder: "State".localized())
        
         txtCity.attributedPlaceholder = addPlaceHolderWithText(placeholder: "City".localized())
        
        //  btnName.setImage(UIImage(named: "walletIcon"), for: .normal)
        btnName.setTitle("Submit -->", for: .normal)
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.setTitleColor(UIColor.white, for: .normal)
        btnName.addTarget(self, action: #selector(callSubmit), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        pickerOptions.append("<-- This Branch -->".localized())
        
        if branchList?.count ?? 0 > 0 {
            
            for model in branchList ?? []{
                
                if model.branchCode != branchCode {
                    
                    pickerOptions.append(model.branchName ?? "")
                    
                }
                
            }
            
        }
        
        txtBillingBranch.pickerOptions = pickerOptions
        txtBillingBranch.dropDownPicker?.reloadAllComponents()
        
        txtBillingBranch.selectedOption = pickerOptions.first ?? ""
        
        txtBranchName.delegate = self
        
        txtAddressLine1.delegate = self
        
        txtCountry.delegate = self
        
        txtCity.delegate = self
        
        txtState.delegate = self
        
        billingAddressBranchCode = branchCode
        
        
        if isDetail == true {
            
            fetchBranchDetails()
            
            btnSubmit.setTitle("Update".localized(), for: .normal)
            btnName.setTitle("Update -->".localized(), for: .normal)
            
            btnName.alpha = 0
            btnSubmit.alpha = 0
            
            if isAdmin == 1 || isPartnerForCompany == 1 {
                
                btnName.alpha = 1
                btnSubmit.alpha = 1
            }
        }
        
        if appointmentTemplateList?.count ?? 0 > 0 {
                   
                   for model in appointmentTemplateList ?? []{
                       
                       appointmentOptions.append(model.title ?? "")
                       
                   }
                   
                   txtAppointmentTemplate.pickerOptions = appointmentOptions
                   txtAppointmentTemplate.dropDownPicker?.reloadAllComponents()
                   
                 //  txtCategory.selectedOption = categoryOptions.first ?? ""
                   
               }
        
        if timeZoneList?.count ?? 0 > 0 {
                          
                          for model in timeZoneList ?? []{
                              
                              timezone.append(model.tzName ?? "")
                              
                          }
                          
                          txtTimeZone.pickerOptions = timezone
                          txtTimeZone.dropDownPicker?.reloadAllComponents()
                          
                        //  txtCategory.selectedOption = categoryOptions.first ?? ""
                          
                      }
        
        if isFromJob == 1 || isFromJob == 2 {
            
            viewTimeZone.alpha = 0
               
            heightViewTimeZone.constant = 0
               
           // viewAppointment.aplha = 0
               
            heightViewAppointment.constant = 0
               
            viewBillingBranch.alpha = 0
               
            heightViewBillingBranch.constant = 0
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
    }
    
    
    func updateUI(){
        
        if branchDetail != nil {
            
            segmentStatus.setSegmentStyle()
            
            txtBranchName.text = branchDetail?.branchName ?? ""
            
            txtAddressLine1.text = branchDetail?.addressLine1 ?? ""
            
            txtAddressLine2.text = branchDetail?.addressLine2 ?? ""
            
            txtCountry.text = branchDetail?.country ?? ""
            
            txtState.text = branchDetail?.state ?? ""
            
            txtCity.text = branchDetail?.city ?? ""
            
            txtPostalCode.text = branchDetail?.postalCode ?? ""
            
            lblLocation.text = "Location Selected".localized()
            
            lblLocation.textColor = .btnLinkColor
            
            if branchDetail?.status == 1 {
                
                segmentStatus.selectedSegmentIndex = 0
                
                status = 1
            }
            else{
                
                segmentStatus.selectedSegmentIndex = 1
                status = 2
            }
            
            currentLoc?.latitude = branchDetail?.latitude ?? 0.0
            currentLoc?.longitude = branchDetail?.longitude ?? 0.0
            //updateLocationName()
            //  updateLocationViewHeight()
            
            billingAddressBranchCode = branchDetail?.billingAddressBranchCode ?? 0
            
            var index = 0
            
            let count = pickerOptions.count ?? 0
            
            appointmentTemplateCode = branchDetail?.appointmentTemplateCode ?? 0
            
            timeZoneId = branchDetail?.timeZoneId ?? 0
            
            for index in 0..<count {
                
                for model in branchList ?? []{
                    
                    if billingAddressBranchCode == branchCode {
                        
                        txtBillingBranch.selectedOption = pickerOptions[0]
                        
                    }
                    
                    if model.branchCode == billingAddressBranchCode {
                        
                        if let modelStr:String = pickerOptions[index]{
                            
                            if modelStr == model.branchName {
                                
                                txtBillingBranch.selectedOption = pickerOptions[index]
                                
                            }
                            
                        }
                    }
                }
            }
            
            if appointmentTemplateCode ?? 0 > 0 {
                
                for model in appointmentTemplateList ?? [] {
                    
                    if model.appointmentTemplateCode == appointmentTemplateCode {
                        
                        txtAppointmentTemplate.selectedOption = model.title ?? ""
                        
                    }
                    
                }
            }
            
            if timeZoneId ?? 0 > 0 {
                           
                           for model in timeZoneList ?? [] {
                               
                               if model.timeZoneId == timeZoneId {
                                   
                                txtTimeZone.selectedOption = model.tzName ?? ""
                                   
                               }
                               
                           }
                       }
            
            selectedIndex = branchDetail?.jobLocationType ?? 0
            
            collectionViewType.reloadData()
        }
        
    }
    
    func updateLocationViewHeight() {
        
        guard let labelText = lblLocation.text else { return }
        
        let height = estimatedHeightOfLabel(text: labelText)
        
        heightViewLocation.constant = height + 30
        
    }
    
    func estimatedHeightOfLabel(text: String) -> CGFloat {
        
        let size = CGSize(width: view.frame.width - 16, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: UIFont.fontForType(textType: 6)]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }
    
    
    func showLocationRequiredPage(){
        weak var weakSelf = self
        let alert:UIAlertController = UIAlertController.init(title: "Access Required.".localized(), message: "Please enable location access to provide you near by search.".localized(), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "Enable Location Service".localized(), style: .default, handler: {action in
            
            weakSelf?.openAppSettings()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func getCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        switch locationAuthorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .authorizedWhenInUse, .authorizedAlways:
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.startUpdatingLocation()
                currentLoc = locationManager.location?.coordinate
                
                // updateLocationName()
            }
        case .restricted, .denied:
            
            //  showLocationRequiredPage()
            
            print("Access Denied")
            
        }
    }
    
    
    
    //MARK: Location manager
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if  let locValue:CLLocationCoordinate2D = manager.location?.coordinate {
            //print("locations = \(locValue.latitude) \(locValue.longitude)")
            
            currentLoc = locValue
            
        }
        // updateLocationName()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        stopAnimating()
    }
    
    @IBAction func btnSelectLocationAction(_ sender: Any) {
        
        openMap()
        
    }
    
    @IBAction func segmentStatusAction(_ sender: UISegmentedControl) {
        
        segmentStatus.setSegmentStyle()
        
        if sender.selectedSegmentIndex == 0 {
            
            status = 1
        }
        else{
            status = 2
        }
        
    }
    
    func openMap() {
        
        let locationPage = LocationNewViewController(nibName: "LocationPicker", bundle: nil)
        
        locationPage.delegate = self
        locationPage.currentLocation = currentLoc
        locationPage.isCancelable = true
        locationPage.lat = currentLoc?.latitude ?? 0.0
        locationPage.lng = currentLoc?.longitude ?? 0.0
        locationPage.modalPresentationStyle = .fullScreen
        navigationController?.present(locationPage, animated: true)
    }
    
    @IBAction func btnChangeLocationAction(_ sender: Any) {
        
        openMap()
    }
    
    func didFinishSelectingLocation(coordinate: CLLocationCoordinate2D) {
        
        currentLoc = coordinate
        updateLocationName()
        // updateLocationViewHeight()
        
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        callSubmit()
        
    }
    
    @objc func callSubmit(){
        
        txtBillingBranch.resignFirstResponder()
        txtBranchName.resignFirstResponder()
        
        if txtBranchName.text?.count ?? 0 == 0 {
            
            txtBranchName.showError(message: "Required".localized())
            
            return
            
        }
        else if lblLocation.text ?? "" == "\("Select Location".localized())*" {
            
            showErrorMessage(message: "Location Required".localized())
            
            return
            
        }
        else if txtAddressLine1.text?.count == 0 {
            
            txtAddressLine1.showError(message: "Required".localized())
            
            return
            
        }
        else if txtCountry.text?.count == 0 {
                       
            txtCountry.showError(message: "Required".localized())
                       
             return
                       
         }
        else if txtState.text?.count == 0 {
                          
            txtState.showError(message: "Required".localized())
                          
            return
                          
         }
       else if txtCity.text?.count == 0 {
                                     
            txtCity.showError(message: "Required".localized())
                                     
                return
                                     
          }
            
        else
        {
            billingCode()
            submitBranchDetails()
        }
    }
    
    func billingCode()  {
        
        billingAddressBranchCode = branchCode
        
        for model in branchList ?? [] {
            
            if model.branchName == txtBillingBranch.selectedOption {
                
                billingAddressBranchCode = model.branchCode ?? 0
                
            }
            
        }
    }
    
    
    func updateLocationName() {
        
        let geoCoder = GMSGeocoder()
        
        weak var weakSelf = self
        
        geoCoder.reverseGeocodeCoordinate(currentLoc ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), completionHandler: { response, error in
            
            if response != nil {
                let address = response?.results()?.first
                if address != nil {
                    
                    weakSelf?.lblLocation.text = "Location Selected".localized()
                    
                    //                    weakSelf?.txtAddressLine1.text = address?.addressLine1()
                    //
                    //                    weakSelf?.txtAddressLine2.text = address?.addressLine2()
                    
                    if weakSelf?.txtCountry.text?.count ?? 0 == 0 || weakSelf?.txtState.text?.count ?? 0 == 0 || weakSelf?.txtCity.text?.count ?? 0 == 0 ||  weakSelf?.txtPostalCode.text?.count ?? 0 == 0 {
                        
                        weakSelf?.txtCountry.text = address?.country ?? ""
                        
                        weakSelf?.txtState.text = address?.administrativeArea ?? ""
                        
                        weakSelf?.txtCity.text = address?.locality
                        
                        weakSelf?.txtPostalCode.text = address?.postalCode ?? ""
                        
                    }
                    
                    //  weakSelf?.updateLocationViewHeight()
                    
                    
                    
                    weakSelf?.lblLocation.textColor = .btnLinkColor
                }
            }
            
        })
    }
    
    func submitAppointmentCodeAndTimeZone(){
      
        if appointmentTemplateList?.count ?? 0 > 0 {
                          
           for model in appointmentTemplateList ?? []{
                              
             if model.title == txtAppointmentTemplate.selectedOption {
                                  
                appointmentTemplateCode = model.appointmentTemplateCode ?? 0
                        
                }
                              
              }
                    
          }
        
        if timeZoneList?.count ?? 0 > 0 {
                                 
                  for model in timeZoneList ?? []{
                                     
                    if model.tzName == txtTimeZone.selectedOption {
                                         
                       timeZoneId = model.timeZoneId ?? 0
                               
                       }
                                     
                     }
                           
                 }
    }
    
    func submitBranchDetails(){
        
        startAnimatingAfterSubmit()
        
        submitAppointmentCodeAndTimeZone()
        
        txtBillingBranch.resignFirstResponder()
        txtBranchName.resignFirstResponder()
        
        let params:NSDictionary = ["sellerCode": sellerCode,
                                   "branchCode":branchCode,
                                   "branchName" : txtBranchName.text ?? "",
                                   "addressLine1" :txtAddressLine1.text ?? "",
                                   "addressLine2" :txtAddressLine2.text ?? "",
                                   "latitude" : currentLoc?.latitude ?? 0.0,
                                   "longitude" : currentLoc?.longitude ?? 0.0,
                                   "notes" : "",
                                   "status" :status,
                                   "country":txtCountry.text ?? "",
                                   "state":txtState.text ?? "",
                                   "city":txtCity.text ?? "",
                                   "postalCode":txtPostalCode.text ?? "",
                                "billingAddressBranchCode":billingAddressBranchCode,
                                "appointmentTemplateCode":appointmentTemplateCode ?? 0,
                                "timeZoneId":timeZoneId ?? 0,
                                "jobLocationType":jobLocationTypeList?[selectedIndex].jobLocationType ?? 0
            
        ]
        let encryptedParams = ["data": encryptParams(params: params)]
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.PostRequest(urlString: ("\(ApiBaseUrl)icr/saveSellerBranch?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)"), params: encryptedParams as NSDictionary, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            
            weakSelf?.stopAnimating()
            
            if let response = response {
                
                if  weakSelf?.validateStatus(response: response) ?? false {
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            do {
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                //let decoder = JSONDecoder()
                                let decoder = JSONDecoder()
                                let dateFormat:DateFormatter = DateFormatter.init()
                                dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                //dateFormat.timeZone = TimeZone.init(secondsFromGMT: 0)
                                decoder.dateDecodingStrategy = .formatted(dateFormat)
                                //                            let apiResponse = try decoder.decode(CategoryListModel.self, from: data)
                                //                            weakSelf?.categoryNewList = apiResponse.categoriesList ?? []
                                //                            weakSelf?.searchCategoryList = apiResponse.categoriesList ?? []
                                //
                                //                            DispatchQueue.main.async {
                                //
                                //                                weakSelf?.collectionViewCategory.reloadData()
                                
                                
                                //                            }
                                
                                UserDefaults.standard.set(0, forKey: "isBranch")
                                
                                weakSelf?.delegate?.didAddedBranch()
                                weakSelf?.navigationController?.popViewController(animated: true)
                                
                                return
                                
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                            }
                        }
                    }
                }
            }
            
            //Show error
            weakSelf?.showErrorMessage(message: message)
            
        })
        
        
    }
    
    func fetchBranchDetails() {
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/branchDetails?token=\(getCurrentUserToken())&lngId=\(Localize.currentLanguageID)&sellerCode=\(sellerCode)&branchCode=\(branchCode)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    
                    if let dataStr:String =  jsonObj?.dataString(){
                        
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                
                                let data = try JSONSerialization.data(withJSONObject: dataObj as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let decoder = JSONDecoder()
                                print(dataObj)
                                
                                let jsonResponse = try decoder.decode(BranchDetailApiModel.self, from: data)
                                
                                if let apiResponse:BranchDetailModel = jsonResponse.sellerBranchDetails {
                                    
                                    weakSelf?.branchDetail = apiResponse
                                    
                                }
                                
                                weakSelf?.updateUI()
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
}

extension NewBranchVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtBranchName{
            txtBranchName.showError(message: "")
        }
        
        if textField == txtAddressLine1{
            txtAddressLine1.showError(message: "".localized())
        }
        
        if textField == txtCountry{
            txtCountry.showError(message: "".localized())
        }
        
        if textField == txtState{
            txtState.showError(message: "".localized())
        }
        if textField == txtCity{
            txtCity.showError(message: "".localized())
        }
        
        return true
        
    }
}


extension NewBranchVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewType
            
        {
            return CGSize.init(width: collectionViewType.frame.width / 3, height: 40.0)
            
        }
        
        
        return CGSize.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == collectionViewType{
            
            if jobLocationTypeList?.count ?? 0 > 0 {
                
                return jobLocationTypeList?.count ?? 0
            }
            
        }
        
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        if collectionView == collectionViewAttachment
//        {
//            if let cell:AttachmentPictureCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentPictureCell", for: indexPath) as? AttachmentPictureCell {
//
//                return getCellForAttachment(cell: cell, indexPath: indexPath)
//
//            }
//        }
//        else{
            
            if let cell:StatusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusCell", for: indexPath) as? StatusCell {
                
                return getCellForStatus(cell: cell, indexPath: indexPath)
                
            }
            
      //  }
        return UICollectionViewCell.init()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == collectionViewType
            
        {
            selectedIndex = indexPath.row
            
            collectionViewType.reloadData()
            
        }
   
    }
    
    
    func getCellForStatus(cell:StatusCell,indexPath:IndexPath) -> StatusCell {
        
        if let model:BranchLocationModel = jobLocationTypeList?[indexPath.row] {
            
            cell.viewCellBackground.backgroundColor = .white
            cell.lblTitle.textColor = .primaryColor
            cell.lblTitle.text = model.title ?? ""
            
            if selectedIndex == indexPath.row {
                
                cell.viewCellBackground.backgroundColor = UIColor.getGradientColor(index: 0).first
                cell.lblTitle.textColor = .white
                
            }
            else{
                cell.viewCellBackground.backgroundColor = .white
                cell.lblTitle.textColor = UIColor.getGradientColor(index: 0).first
                
            }
        
            
        }
        return cell
    }
    
}
