//
//  AppLaunchVC.swift
//  ICanRefer
//
//  Created by TalentMicro on 03/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit

let versionCode = 14     //test 14,A //// live 15,B

class AppLaunchVC: AppBaseViewController {

    @IBOutlet weak var imgLogo: UIImageView!
    var versionStatus = -1
    var versionMessage = ""
    
    var socialMediaList:[SocialMediaModel] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        UIApplication.shared.delegate?.setSemantics()
        
        imgLogo.isHidden = true
        imgLogo.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        
       
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        imgLogo.isHidden = false
        weak var weakSelf = self
        if versionStatus == -1 {
            UIView.animate(withDuration: 1.0) {
                weakSelf?.imgLogo.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                weakSelf?.checkVersionCode()
            }
        }
        else if versionStatus > 0 {
            showAlert()
        }
//        else {
//
//            openHomePage()
//        }
        
    }
    
//    func checkLanguageSelection() {
//        let currentLanguage = Localize.currentLanguage
//        if currentLanguage.count>0 {
//            //Go to home page
//            openHomePage()
//        }
//        else{
//            //Show Language selector
//            openLanguageSelector()
//        }
//    }
    
    func checkLoginDone() {
        
        let currentLanguage = Localize.currentLanguage
        
        if currentLanguage.count > 0 {
            
         if checkLogin() {
            
            var configVersionNo:Int = 0
            var alertNo:Int = 0
            let token = getCurrentUserToken()
            
            configVersionNo = UserDefaults.standard.integer(forKey: "configVersionNo")
            alertNo = UserDefaults.standard.integer(forKey: "alertNo")

            getUserDetailsNew(token: token, alertNo: alertNo, configVersionNo: configVersionNo)

         }
        else{
            
           openloginPage()
            
        }
     }
            
    else{
            openLanguageSelector()
            
        }
        
    }
    
    
    func getUserDetailsNew(token:String,alertNo:Int,configVersionNo:Int) {
        
        UserDefaults.standard.set(1, forKey: "firstTime")
        
        let apiClient:APIClient = APIClient()
        //lblCartEmpty.text = "Loading...".localized()
        
        weak var weakSelf = self
        weakSelf?.startAnimating()
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/getUserDetailsNew?token=\(token)&lngId=\(Localize.currentLanguageID)&configVersionNo=\(configVersionNo)&alertNo=\(alertNo)"), params: nil, completionHandler:{response, jsonObj in
            
            var message:String = failedToConnectMessage.localized()
            if let serverMessage = jsonObj?.message(){
                message = (serverMessage.count>0) ? serverMessage : message
            }
            weakSelf?.stopAnimating()
            //weakSelf?.lblCartEmpty.text = ""
            if let response = response {
                if  weakSelf?.validateStatus(response: response) ?? false{
                    if let dataStr:String =  jsonObj?.dataString(){
                        if let dataObj:NSDictionary =  weakSelf?.decryptData(encryptedString: dataStr){
                            
                            do {
                                print(dataObj)
                                
                                let isFetch:Int =  dataObj.object(forKey: "isFetch") as? Int ?? 0
                                
                                let configVersionNo:Int =  dataObj.object(forKey: "configVersionNo") as? Int ?? 0
                                
                                print("configVersionNoconfigVersionNoconfigVersionNoconfigVersionNo")
                                print(configVersionNo)
                                print(dataObj.object(forKey: "config") ?? [:])
                                UserDefaults.standard.set(configVersionNo, forKey: "configVersionNo")
                                
                                if isFetch == 1
                                {
                                    UserDefaults.standard.set(dataObj.object(forKey: "config"), forKey: "config")
                                    
                                    UserDefaults.standard.set(dataObj.object(forKey: "bannerList"), forKey: "bannerList")

                                    
                                    if let userDetails:NSDictionary = dataObj.object(forKey: "userDetails") as? NSDictionary{
                                        
                                        if let mutableData:NSMutableDictionary = userDetails.mutableCopy() as? NSMutableDictionary{
                                            
                                            mutableData.removeObject(forKey: "token")
                                            
                                            if  weakSelf?.saveUserDetails(userDetail: mutableData.copy() as! NSDictionary) ?? false{
                                                weakSelf?.navigationController?.dismiss(animated: true, completion: nil)
                                                
                                                weakSelf?.openHomePage()
                                                
                                                return
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    weakSelf?.openHomePage()
                                    return
                                }
                                
                                return
                            } catch {
                                print(error)
                                message = failedToConnectMessage.localized()
                                
                            }
                        }
                    }
                }
            }
            
            weakSelf?.showErrorMessage(message: message)
            
        })
        
    }
    
    
    func openloginPage() {
        
        let storyBoardL = UIStoryboard.init(name: MainSB, bundle: nil)
        present(storyBoardL.instantiateInitialViewController()!, animated: true, completion: nil)
    }
    
    
    func openLanguageSelector() {
        let storyBoard = UIStoryboard.init(name: LanguageSelectorSB, bundle: nil)
        
        present(storyBoard.instantiateInitialViewController()!, animated: true, completion: nil)
    }
    
    func openHomePage() {
        
        let defaults = UserDefaults.standard
        
        if  defaults.bool(forKey: "isShowBanner")
        {
            let HomeStoryBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
            let homePage:UINavigationController = HomeStoryBoard.instantiateInitialViewController() as! UINavigationController
            homePage.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
            
            view.window?.rootViewController = homePage
        }
        else
        {
    
        if let arrayBanner:[NSDictionary] = UserDefaults.standard.value(forKey: "bannerList") as? [NSDictionary] {
            
                if arrayBanner.count > 0 {
                    
                    showBanners(attachmentArray:arrayBanner)
                    
                }
                else{
                    let HomeStoryBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
                    let homePage:UINavigationController = HomeStoryBoard.instantiateInitialViewController() as! UINavigationController
                    homePage.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
                    
                    view.window?.rootViewController = homePage
            }
                
            }
            

        }
    
    }
    
    func checkVersionCode(){
        let apiClient:APIClient = APIClient()
        weak var weakSelf = self
        apiClient.GetRequest(urlString: ("\(ApiBaseUrl)icr/version/A?versionCode=\(versionCode)&lngId=\(Localize.currentLanguageID)&isIOS=\(1)"), params: nil, completionHandler:{response, jsonObj in
            //let statusCode = response?.statusCode
            //var message:String = failedToConnectMessage.localized()
            
            if let selfObj = weakSelf {
                if (jsonObj?.status() ?? false) {
                    //Save userdata
                    if let dataObj:NSDictionary = jsonObj?.dataObj() {
                        if let status = dataObj.object(forKey: "versionStatus") as? Int {
                            selfObj.versionStatus = status
                        }
                        
                        if let message = dataObj.object(forKey: "versionMessage") as? String{
                            selfObj.versionMessage = message
                        }
                        
                        if let socialMediaList = dataObj.object(forKey: "socialMediaList") as? [SocialMediaModel]{
                            selfObj.socialMediaList = socialMediaList
                        }
                        
                        
                        if selfObj.versionStatus > 0 {
                            selfObj.showAlert()
                            return
                        }
                    }
                }
                
            }
            
            weakSelf?.checkLoginDone()
            
        })
    }
    
    func showAlert(){
        let alert:UIAlertController = UIAlertController.init(title: "Update Available".localized(), message: versionMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Update".localized(), style: .default, handler: { (action) in
            self.OpenAppLink()
            self.showAlert()
        }))
        
        if versionStatus == 1 {
            alert.addAction(UIAlertAction.init(title: "Skip".localized(), style: .default, handler: { (action) in
                
                self.checkLoginDone()
                
            }))
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func OpenAppLink() -> Void {
      let appUrl = "https://itunes.apple.com/us/app/iCanRefer/id1488210455?mt=8"
      if let callurl = URL.init(string: appUrl) {
      UIApplication.shared.openURL(callurl)
        UIControl().sendAction(Selector("suspend"), to: UIApplication.shared, for: nil)
      }
    }
    
    
    //MARK: IBActions
    @IBAction func clickedUpdateButton(_ sender: NKButton) {
        self.OpenAppLink()
    }
    
}
