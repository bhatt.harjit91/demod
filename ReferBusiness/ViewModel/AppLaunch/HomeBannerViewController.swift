//
//  HomeBannerViewController.swift
//  iCanRefer
//
//  Created by TalentMicro on 04/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

//
//  HomeBannerViewController.swift
//  NearKart
//
//  Created by TalentMicro on 24/09/19.
//  Copyright © 2019 RaviKiran B. All rights reserved.
//

import UIKit

class HomeBannerViewController: UIViewController {
    
    var bannerSliderView: HomeBannerView = HomeBannerView()
    
    @IBOutlet weak var btnNext: NKButton!
    
    
    @IBOutlet weak var collectionViewBanner: UICollectionView!
    
    var images = [String]()
    
    var x:Int = 0
    
    var timer : Timer?
    
    var delayTime:Double = 8.0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnNext.addTarget(self, action: #selector(nextBtnAction), for: .touchUpInside)
        
        collectionViewBanner.reloadData()
        
        startTimer()
        
        UserDefaults.standard.set(1, forKey: "isShowBanner")
        
        // Do any additional setup after loading the view.
    }
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        let count:Int =  images.count
        
        x = (x + 1) % count
        
        let indexPath = IndexPath(item: x, section: 0)
        
        if x == count - 1 {
            
            openHomePage()
            
        }
        else{
            
            collectionViewBanner.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if timer != nil {
            timer!.invalidate()
            timer = nil
        }
    }
    
    func startTimer() {
        
        timer =  Timer.scheduledTimer(timeInterval: delayTime, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        
        
    }
    
    @objc func nextBtnAction(){
        
        
        let count:Int =  images.count
        
        x = (x + 1) % count
        
        let indexPath = IndexPath(item: x, section: 0)
        
        if x == count - 1 {
            
            openHomePage()
            
        }
        else{
            
            collectionViewBanner.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
        }
        
    }
    
    func openHomePage() {
        
        let HomeStoryBoard = UIStoryboard.init(name: HomeSB, bundle: nil)
        
        let homePage:UINavigationController = HomeStoryBoard.instantiateInitialViewController() as! UINavigationController
        
        homePage.navigationBar.barTintColor = UIColor.getGradientColor(index: 1).first
        
        view.window?.rootViewController = homePage
    }
    
    
}

extension HomeBannerViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return images.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: collectionViewBanner.frame.width, height: collectionViewBanner.frame.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:BannerCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionCell", for: indexPath) as! BannerCollectionCell
        
        return getCellForBanner(cell: cell, indexPath: indexPath)
        
        
    }
    
    func getCellForBanner(cell:BannerCollectionCell,indexPath:IndexPath) -> BannerCollectionCell {
        
        let model:String = images[indexPath.row]
        
        if model.count > 0 {
            
            let trimmedString = model.trimmingCharacters(in: .whitespacesAndNewlines)
            
            cell.imgViewHeader.sd_setImage(with: URL.init(string: String("\(StorageUrl)\(trimmedString)")), completed: nil)
            
        }
        
        
        
        return cell
        
    }
}

class BannerCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgViewHeader: UIImageView!
    
}

