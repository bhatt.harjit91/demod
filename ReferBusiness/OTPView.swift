//
//  OTPView.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import UIKit



protocol OTPViewDelegate {
    func didChangeOTP(otpText:String)
}



@IBDesignable
class OTPView: UIView, UIKeyInput {
    var hasText: Bool = true
    var otpLabel:UILabel?
    var digits:Int = 4
    var keyboardType: UIKeyboardType = .numberPad
    
    @IBInspectable var textContentType: UITextContentType?
    
    
    var otpText = ""
    var delegate:OTPViewDelegate?
    var isEnabled:Bool = true{
        didSet{
            otpLabel?.textColor = isEnabled ? UIColor.black : UIColor.gray
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        updateLabel(text: otpText)
        
    }
    func updateLabel(text:String) {
        if #available(iOS 12, *){
            textContentType = .oneTimeCode
        }
        var tempText = text
        if otpLabel == nil{
            otpLabel = UILabel()
            otpLabel?.layer.frame = CGRect(origin: CGPoint.zero, size:  frame.size)
            otpLabel?.textAlignment = .center
            addSubview(otpLabel!)
        }
        otpLabel?.layer.frame = CGRect(origin: CGPoint.zero, size:  frame.size)
        otpLabel?.textAlignment = .center
        if tempText.count<digits {
            var index = tempText.count
            while index < digits{
                tempText.append("_")
                index = index+1
            }
        }
        let attributedString = NSMutableAttributedString(string: tempText)
        attributedString.addAttribute(NSAttributedString.Key.kern, value: 10, range: NSRange(location: 0, length: attributedString.length))
        
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.OTPFont, range: NSRange(location: 0, length: attributedString.length))
        otpLabel?.attributedText = attributedString
        otpLabel?.textColor = isEnabled ? UIColor.black : UIColor.gray
        layoutIfNeeded()
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    func insertText(_ text: String) {
        ////print("\(text)")
        if isEnabled{
            if otpText.count<digits {
                otpText = "\(otpText)\(text)"
                updateLabel(text: otpText)
                callOTPChanged()
            }
        }
        
        
    }
    
    func deleteBackward() {
        ////print("backwward clicked")
        if isEnabled{
            otpText = String(otpText.dropLast())
            updateLabel(text: otpText)
            callOTPChanged()
        }
        
    }
    
    func callOTPChanged() {
        delegate?.didChangeOTP(otpText: otpText)
    }
    
    override var canBecomeFirstResponder: Bool{
        if digits>0 {
            updateLabel(text: otpText)
            return true
        }
        return false
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    
    
    
    
}
