//
//  CustomerModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 10/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class CustomerModel: Codable {
    
    var sellerCode:Int?
    var customerName:String?
    var description:String?
    var website:String?
    var logo:String?
    var logoName:String?
    var customerCode:Int?
    var status:Int?
    var lngId:Int?
    var crUserId:Int?
    var crDate:String?
    var luUserId:Int?
    var luDate:String?
    var statusTitle:String?
    
    private enum CodingKeys:String,CodingKey{
        
           case sellerCode
           case customerName
           case description
           case website
           case logo
           case logoName
           case customerCode
           case status
           case lngId
           case crUserId
           case crDate
           case luUserId
           case luDate
           case statusTitle
        
        
    }

    
}

class CustomerApiModel: Codable {
    
    var sellerCustomerList:[CustomerModel]?
    var statusList:[SellerStatusModel]?
    var count:Int?
}

class CustomerDetailApiModel: Codable {
    
    var sellerCustomerDetails:CustomerModel?

}
