//
//  ResumeBankModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 26/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import Foundation

class ResumeStatusModel:Codable{
    var selectedCell:Int?
    var resumeStatusCode:Int?
    var title:String?
    
}

class GenderModel:Codable{
    var selectedCell:Int? = 0
    var genderCode:Int?
    var title:String?
    
}

class EducationModel:Codable{
    
    var educationCode:Int?
    var title:String?
    var noSpecialization:Int?
    
}

class SpecializationModel:Codable{
    
    var specializationCode:Int?
    var title:String?
    
}

class SkillCategoryModel:Codable{
    
    var skillCatCode:Int?
    var title:String?
    
}

class SkillListModel:Codable{
    
    var skillCode:Int?
    var title:String?
    
}

class ScaleDurationModel:Codable{
    
    var scaleDurationId:Int?
    var name:String?
    var isDefault:Int?
    
    
}

class JobTypeModel:Codable{
    var selectedCell:Int? = 0
    var jobType:Int?
    var title:String?
    
}

class MonthModel:Codable{
    
    var month:Int?
    var title:String?
    
}

class YearModel:Codable{
    
    var year:Int?
    
}

class ResumeDetailsModel:Codable{
    
}


class CountryListModel:Codable{
    var code:String?
    var countryId:Int?
    var countryName:String?
    var dial_code:String?
    var flagImage:String?
    var mobileNumberLength:Int?
    var name:String?
    var zipPinCodeLength:Int?
}


////////////BY Harjit//////////
//class ResumeMasterDataModelClass: Codable{
//
//    var data:ResumeMasterDataModel?
//}

class ResumeMasterDataModel: Codable{
    
    var countryList:[CountryListModel]?
    var currencyList:[CurrencyModel]?
    var educationList:[EducationModel]?
    var genderList:[GenderModel]?
    var jobTypeList:[JobTypeModel]?
    var monthList:[MonthModel]?
    var resumeDetails:ResumeDetailsModel?
    var resumeStatusList:[ResumeStatusModel]?
    var scaleDurationList:[ScaleDurationModel]?
    var skillCategoryList:[SkillCategoryModel]?
    var skillList:[SkillListModel]?
    var specializationList:[SpecializationModel]?
    var yearList:[YearModel]?
    
}
