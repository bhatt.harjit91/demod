//
//  AgreementModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class SellerAgreementModel: Codable {
    
    var agreementId:Int?
    var agreements:[AttachmentMainModel]?
    var status:Int?
    var sellerCode:Int?
    var crDate:String?
    var crUserId:Int?
    var createdBy:String?
    var luDate:String?
    var luUserId:Int?
    var updatedBy:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case agreementId
        case agreements
        case status
        case sellerCode
        case crDate
        case crUserId
        case createdBy
        case luDate
        case luUserId
        case updatedBy
        
    }
      
        public func encode(to encoder: Encoder) throws {
              
               var container = encoder.container(keyedBy: CodingKeys.self)
               try container.encode(agreementId, forKey: .agreementId)
               try container.encode(agreements, forKey: .agreements)
               try container.encode(status, forKey: .status)
               try container.encode(sellerCode, forKey: .sellerCode)
               try container.encode(crDate, forKey: .crDate)
               try container.encode(crUserId, forKey: .crUserId)
               try container.encode(createdBy, forKey: .createdBy)
               try container.encode(luDate, forKey: .luDate)
               try container.encode(luUserId, forKey: .luUserId)
               try container.encode(updatedBy, forKey: .updatedBy)
           }
    
           required public init(from decoder: Decoder) throws {
              
               let values = try decoder.container(keyedBy: CodingKeys.self)
               agreementId = try values.decodeIfPresent(Int.self, forKey: .agreementId)
            agreements = try values.decodeIfPresent([AttachmentMainModel].self, forKey: .agreements)
               status = try values.decodeIfPresent(Int.self, forKey: .status)
               sellerCode = try values.decodeIfPresent(Int.self, forKey: .sellerCode)
               crDate = try values.decodeIfPresent(String.self, forKey: .crDate)
               crUserId = try values.decodeIfPresent(Int.self, forKey: .crUserId)
               luUserId = try values.decodeIfPresent(Int.self, forKey: .luUserId)
               createdBy = try values.decodeIfPresent(String.self, forKey: .createdBy)
               luDate = try values.decodeIfPresent(String.self, forKey: .luDate)
               updatedBy = try values.decodeIfPresent(String.self, forKey: .updatedBy)
           }
}

class SellerAgreementApiModel: Codable {
    
    var aggreementHistory:[SellerAgreementModel]?
    
    private enum CodingKeys:String,CodingKey{
        
        case aggreementHistory
        
    }
    
       public func encode(to encoder: Encoder) throws {
          
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(aggreementHistory, forKey: .aggreementHistory)
        
       }
       
       required public init(from decoder: Decoder) throws {

           let values = try decoder.container(keyedBy: CodingKeys.self)
        aggreementHistory = try values.decodeIfPresent([SellerAgreementModel].self, forKey: .aggreementHistory)
        
       }
    
}

class  InvitePartnerApiModel: Codable {
    
    var smSharingText:String?
    var smSharingImage:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case smSharingText
        case smSharingImage
    }
}

