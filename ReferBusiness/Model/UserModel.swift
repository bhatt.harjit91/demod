//
//  UserModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 12/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class SellerUserModel: Codable {
  
    var sellerUserId:Int?
    var displayName:String?
    var status:Int?
    var isBuyer:Int?
    var isSeller:Int?
    var isReferralMember:Int?
    var emailId:String?
    var statusTitle:String?

    private enum CodingKeys:String,CodingKey{
        case sellerUserId
        case displayName
        case isBuyer
        case isSeller
        case status
        case isReferralMember
        case emailId
        case statusTitle
    }
}

class SellerUserDetailModel: Codable {
   
    var sellerUserId:Int?
    var displayName:String?
    var firstName:String?
    var lastName:String?
    var status:Int?
    var isBuyer:Int?
    var isSeller:Int?
    var isReferralMember:Int?
    var emailId:String?
    var statusTitle:String?
    
    var crDate:String?
    var crUserId:Int?
    var jobTitle:String?
    var luDate:String?
    var luUserId:Int?
    var mobileIsd:String?
    var mobileNumber:String?
    var noteToReferralMember:String?
    var phoneIsd:String?
    var phoneNumber:String?
    var userId:String?
    
    var accessType:Int?
    var isAdmin:Int?
    var officialMailId:String?

    var isBusinessUser:Int?
    var businessEmailId:String?
    var isHRUser:Int?
    var hrEmailId:String?

    
    private enum CodingKeys:String,CodingKey{
        case sellerUserId
        case displayName
        case firstName
        case lastName
        case isBuyer
        case isSeller
        case status
        case isReferralMember
        case emailId
        case statusTitle
        case crDate
        case crUserId
        case jobTitle
        case luDate
        case luUserId
        case mobileIsd
        case mobileNumber
        case noteToReferralMember
        case phoneIsd
        case phoneNumber
        case userId
        case accessType
        case isAdmin
        case officialMailId
        
        case isBusinessUser
        case businessEmailId
        case isHRUser
        case hrEmailId
    }
}

class SellerUserDetailApiModel: Codable {
    
    var sellerUserDetails:SellerUserDetailModel?
    
}

class SellerUserApiModel: Codable {
    
    var sellerUserList:[SellerUserModel]?
    var count:Int?
    
}

class ReferBuyerUserModel: Codable {
   
    var userId:Int?
    var displayName:String?
    var firstName:String?
    var lastName:String?
    var mobileIsd:String?
    var mobileNumber:String?
    var emailId:String?
    var jobTitle:String?
    var organization:String?
    var city:String?
    var about:String?
    var latitude:Double?
    var longitude:Double?
    var isBuyer:Int?
    var isReferralMember:Int?
    var isSeller:Int?
    var profilePicture:String?
    var sellerUserId:Int?
    
    private enum CodingKeys:String,CodingKey{
        
        case userId
        case displayName
        case firstName
        case lastName
        case isBuyer
        case isSeller
        case emailId
        case jobTitle
        case organization
        case city
        case about
        case latitude
        case longitude
        case isReferralMember
        case profilePicture
        case sellerUserId
        
    }
}

class ReferBuyerUserApiModel: Codable {
    
    var userDetails:[ReferBuyerUserModel]?
    var userPresent:Int?
    
}
