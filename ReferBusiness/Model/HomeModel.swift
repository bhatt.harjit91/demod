//
//  HomeModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 29/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class BannerModel: Codable {
    var id:Int?
    var title:String?
    var cdnPath:String?
    var description:String?
   
    private enum CodingKeys:String,CodingKey{
        case id
        case title
        case cdnPath
        case description
    }
}

class HomeTransactionModel: Codable {
    var footerText:String?
    var transactionId:Int?
    var actualMessage:String?
    var rmAnonymous:Int?
    var productCode:Int?
    var productName:String?
    var productLogo:String?
    var description:String?
    var rating:Double?
    var banners:[[String:String]]?
    var crUserId:Int?
    var toUserId:Int?
    var referredTo:String?
    var referredBy:String?
    var referredDate:String?
    var stageId:Int?
    var stage:String?
    var sellerCompanyName:String?
    var sellerCode:Int?
    var isBuyer:Int?
    var isReferralMember:Int?
    var isSeller:Int?
    var isListingPartner:Int?
    var isNotified:Int?
    var responseCount:Int?
    var unreadResponseCount:Int?
    var currencyTitle:String?
    
    var loyaltyAmount:String?
    var loyaltyCurrencyId:Int?
    var loyaltyType:Int?
    var loyaltyValue:Int?
    var productType:Int?
    
    var isAppointment:Int?
    var appointmentId:Int?
    var appointmentDT:String?
    var noOfPeople:Int?
    var bookingStatusTitle:String?
    var resourceTitle:String?
    var resourceCount:Int?
    var registrationId:String?
    
    private enum CodingKeys:String,CodingKey{
        case footerText
        case transactionId
        case actualMessage
        case rmAnonymous
        case productCode
        case productName
        case productLogo
        case description
        case rating
        case banners
        case crUserId
        case toUserId
        case referredTo
        case referredBy
        case referredDate
        case stageId
        case stage
        case sellerCompanyName
        case sellerCode
        case isBuyer
        case isReferralMember
        case isSeller
        case isListingPartner
        case isNotified
        case responseCount
        case unreadResponseCount
        case currencyTitle
        case loyaltyAmount
        case loyaltyCurrencyId
        case loyaltyType
        case loyaltyValue
        case productType
        case isAppointment
        case appointmentDT
        case noOfPeople
        case bookingStatusTitle
        case resourceTitle
        case resourceCount
        case registrationId
        case appointmentId
    }
}


class BuyerModel: Codable {
    
    var topText:String?
    var bottomText:String?
    var loyaltyBonus:Double?
    var currencyTitle:String?
    var currencyId:Int?
    var transactionList:[HomeTransactionModel]?
    var isClaimActive:Int?
    
    private enum CodingKeys:String,CodingKey{

        case topText
        case bottomText
        case loyaltyBonus
        case currencyTitle
        case currencyId
        case transactionList
        case isClaimActive
    }
}


class ReferalMemberStatusModel: Codable {
    
    var lostCount:Int?
    var lostAmount:Double?
    var earnedCount:Int?
    var earnedAmount:Double?
    var pipelineCount:Int?
    var pipelineAmount:Double?
    
    private enum CodingKeys:String,CodingKey{
        
        case lostCount
        case lostAmount
        case earnedCount
        case earnedAmount
        case pipelineCount
        case pipelineAmount
        
    }
    
}

class SellerTransactionStatusModel: Codable {
  
    var lostCount:Int?
    var lostAmount:Double?
    var earnedCount:Int?
    var earnedAmount:Double?
    var pipelineCount:Int?
    var pipelineAmount:Double?

    private enum CodingKeys:String,CodingKey{
        
        case lostCount
        case lostAmount
        case earnedCount
        case earnedAmount
        case pipelineCount
        case pipelineAmount
        
    }
}

class SellerDetailsModel: Codable {
    
    var referralLeadList:ReferalModel?
    
}

class ReferralMemberModel: Codable {
    
    var currencyTitle:String?
    var currencyId:Int?
    var totalReferralCount:Int?
    var totalEarnings:Double?
    var totalSellerCount:Int?
    var referralStatus:ReferalMemberStatusModel?
    var sellerTransactionStatus:SellerTransactionStatusModel?
    var isClaimActive:Int?
    var referralClaimAmount:Double?
    var referralCurrencyId:Int?
    var referralCurrencyTitle:String?
    var myPartnerCount:Int?
    
    private enum CodingKeys:String,CodingKey{
        case currencyTitle
        case currencyId
        case totalReferralCount
        case totalEarnings
        case totalSellerCount
        case referralStatus
        case sellerTransactionStatus
        case isClaimActive
        case referralClaimAmount
        case referralCurrencyId
        case referralCurrencyTitle
        case myPartnerCount
    }
}



class HomeApiModel: Codable {
    
    var isBuyer:Int?
    var isReferralMember:Int?
    var isSeller:Int?
    var buyerDetails:BuyerModel?
    var referralMemberDetails:ReferralMemberModel?
    var banners:[BannerModel]?
    var sellerDetails:HomeSellerDetailApiModel?
    var memberShipStatusObj:MemberShipStatusObj?
    var membershipId:String?
    var isLocateReferralPartner:Int?
    var isManageSeller:Int?
    var isAccountStatement:Int?
    var claimData:ClaimDataModel?
    var filterMasterList:[FilterReferralModel]?
    var isActivePartner:Int?
    var isRequestForTraining:Int?
    var isRequestForActivation:Int?
    var requestMessageText:String?
    var isManagePartners:Int?
    
    var referralLeadLabelText:String?
    var listingCompanyLeadLabelText:String?
    var companySectionLabelText:String?
    
    var sellerWiseLeadList:[HomeSellerLeadModel]?
    
    private enum CodingKeys:String,CodingKey{
        
        case banners = "banners"
        case isBuyer = "isBuyer"
        case isReferralMember = "isReferralMember"
        case isSeller = "isSeller"
        case buyerDetails = "buyerDetails"
        case referralMemberDetails = "referralMemberDetails"
        case sellerDetails = "sellerDetails"
        case memberShipStatusObj = "memberShipStatusObj"
        case membershipId = "membershipId"
        case isLocateReferralPartner = "isLocateReferralPartner"
        case isManageSeller = "isManageSeller"
        case isAccountStatement = "isAccountStatement"
        case claimData = "claimData"
        case filterMasterList = "filterMasterList"
        case isActivePartner = "isActivePartner"
        case isRequestForTraining = "isRequestForTraining"
        case isRequestForActivation = "isRequestForActivation"
        case requestMessageText = "requestMessageText"
        case isManagePartners = "isManagePartners"
        case referralLeadLabelText
        case listingCompanyLeadLabelText
        case companySectionLabelText
        case sellerWiseLeadList
    }
}

class HomeSellerDetailApiModel: Codable {
    
    var referralLeadList:[HomeSellerDetailModel]?
    var productLeadStatus:[HomeSellerDetailModel]?
    var jobLeadStatus:[HomeSellerDetailModel]?
    var sellerCode:Int?
    var productStatusLabelText:String?
    var jobStatusLabelText:String?

    
    private enum CodingKeys:String,CodingKey{
        
        case referralLeadList
        case productLeadStatus
        case jobLeadStatus
        case sellerCode
        case productStatusLabelText
        case jobStatusLabelText
    }
}

class HomeSellerDetailModel: Codable {
 
    var title:String?
    var id:Int?
    var count:Int?
    var progressValue:Int?
    
    private enum CodingKeys:String,CodingKey{
        
        case title
        case id
        case count
        case progressValue
    }
    
}

class HomeSellerReferralModel: Codable {
   var newCount:Int?
   var newAmount:String?
   var closedCount:Int?
   var closedAmount:String?
   var droppedCount:Int?
   var droppedAmount:String?
   var inProcessCount:Int?
   var inProcessAmount:String?

}

class MemberShipStatusObj: Codable {
    
    var expiresIn:Int?
    var expiryDate:String
    var isRenewMemberShip:Int?
    var memberShipType:Double?
    
    private enum CodingKeys:String,CodingKey{
        
        case expiresIn = "expiresIn"
        case expiryDate = "expiryDate"
        case isRenewMemberShip = "isRenewMemberShip"
        case memberShipType = "memberShipType"
        
    }
}

class ClaimDataModel: Codable {
    
    var totalClaimAmount:Double?
    var currencyTitle:String?
    var currencyId:Int?
    var minClaimAmount:Double?
    var claimAlertTextMessage:String?
    var claimAlertTextMessageTwo:String?
    var isClaimActive:Int?
    private enum CodingKeys:String,CodingKey{
        
        case totalClaimAmount = "totalClaimAmount"
        case currencyTitle = "currencyTitle"
        case currencyId = "currencyId"
        case minClaimAmount = "minClaimAmount"
        case claimAlertTextMessage = "claimAlertTextMessage"
        case isClaimActive = "isClaimActive"
        case claimAlertTextMessageTwo = "claimAlertTextMessageTwo"
    }
}

class FilterReferralModel: Codable {
    
    var title:String?
    var type:Int?
    var progressValue:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case title
        case type
        case progressValue
    }
       
}

class HomeSellerLeadModel:Codable{
    
       var eventColorCode:String?
       var eventCount:Int?
       var eventRegisteredCount:Int?
       var eventType:Int?
       var jobColorCode:String?
       var jobCount:Int?
       var jobRegisteredResumeCount:Int?
       var jobType:Int?
       var logo:String?
       var referralLeadLabelText:String?
       var sellerCode:Int?
       var sellerCompanyName:String?
       var sellerMemberId:String?
       var referralLeads:[HomeReferralLeadsModel]?
}

class HomeReferralLeadsModel:Codable{
    var count:Int?
    var progressValue:Int?
    var stageColorCode:String?
    var title:String?
    var eventRegisteredCount:Int?
    var jobRegisteredResumeCount:Int?
}
