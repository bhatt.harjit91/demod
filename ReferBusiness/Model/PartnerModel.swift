//
//  PartnerModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 16/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import Foundation

class PartnerModel: Codable {
    
    var userId:Int?
    var profilePicture:String?
    var displayName:String?
    var latitude:Double?
    var longitude:Double?
    var distance:String?
    var isReferralMember:Int?
    var partnerId:String?
    var isSelected:Int = 0

    private enum CodingKeys:String,CodingKey{
        case userId
        case profilePicture
        case displayName
        case latitude
        case longitude
        case distance
        case isReferralMember
        case partnerId
    }
    
}

extension PartnerModel:Equatable{
    static func == (lhs: PartnerModel, rhs: PartnerModel) -> Bool {
        
        return lhs.userId == rhs.userId
        
    }
}

class PartnerListApiModel: Codable {
    
    var partnerList:[PartnerModel]?
    var count:Int?
}
