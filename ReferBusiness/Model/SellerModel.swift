//
//  SellerModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 12/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class SellerModel: Codable {
    var sellerCode:Int?
    var lngId:Int?
    var sellerCompanyName:String?
    var logo:String?
    var sellerName:String?
    var branchCount:Int?
    var productCount:Int?
    var referralLeads:Int?
    var userCount:Int?
    var sellerMemberId:String?
    var testimonialCount:Int?
    var customerCount:Int?
    var giftCount:Int?
    var jobsCount:Int?
    var billingCount:Int?
    var isAdmin:Int?
    var isPartnerForCompany:Int?
    var companyType:Int?
    var eventCount:Int?
    
    private enum CodingKeys:String,CodingKey{
        case sellerCode
        case lngId
        case sellerCompanyName
        case logo
        case sellerName
        case branchCount
        case productCount
        case referralLeads
        case userCount
        case sellerMemberId
        case testimonialCount
        case customerCount
        case giftCount
        case jobsCount
        case billingCount
        case isAdmin
        case isPartnerForCompany
        case companyType
        case eventCount
    }

}

class ColorModel: Codable {
    
    var activeColor:String?
    var blacklistedColor:String?
    var branchCountColor:String?
    var inactiveColor:String?
    var productCountColor:String?
    var referralLeadsColor:String?
    var userCountColor:String?
    var customerCountColor:String?
    var testimonialCountColor:String?
    var giftCountColor:String?
    var jobsCountColor:String?
    var billingCountColor:String?
    var eventCountColor:String?
    
    private enum CodingKeys:String,CodingKey{
        case activeColor
        case blacklistedColor
        case branchCountColor
        case inactiveColor
        case productCountColor
        case referralLeadsColor
        case userCountColor
        case customerCountColor
        case testimonialCountColor
        case giftCountColor
        case jobsCountColor
        case billingCountColor
        case eventCountColor
    }

}

class SellerApiModel: Codable{
    
    var sellerList:[SellerModel]?
    var count:Int?
    var statusList:[SellerStatusModel]?
    var colorCodes:ColorModel?
    var billingCycle:[BillingCycleModel]?
    var attachmentTypes:[AttachmentTypeModel]?
    var categoryList:[SellerCategoryModel]?
    var currency:[CurrencyModel]?
    var timePeriodList:[TimePeriodModel]?
    var accessUserTypes:[AccessUserTypesModel]?
    var masterDetails:SellerProductBranchPopUpModel?
    var companyTypes:[CompanyTypesModel]?
    
    private enum CodingKeys:String,CodingKey{
        
        case sellerList = "sellerList"
        case count = "count"
        case statusList = "statusList"
        case colorCodes = "colorCodes"
        case billingCycle = "billingCycle"
        case attachmentTypes = "attachmentTypes"
        case categoryList = "categoryList"
        case currency = "currency"
        case timePeriodList = "timePeriodList"
        case accessUserTypes = "accessUserTypes"
        case masterDetails = "masterDetails"
        case companyTypes = "companyTypes"
    }
    
}

class SellerStatusModel: Codable {
    var status:Int?
    var statusTitle:String?
}

class SellerDetailModel: Codable {
    var sellerCode:Int?
    var lngId:Int?
    var sellerCompanyName:String?
    var logo:String?
    var firstName:String?
    var lastName:String?
    var jobTitle:String?
    var mobileIsd:String?
    var mobileNumber:String?
    var phoneIsd:String?
    var phoneNumber:String?
    var notes:String?
    var taxNumber:String?
    var status:Int?
    var statusTitle:String?
    var crDate:String?
    var luDate:String?
    var amcCurrencyId:Int?
    var amcCurrencySymbol:String?
    var amcType:Int?
    var amcValue:Int?
    var attachmentList:[AttachmentBannerTypeModel]?
    var billingType:Int?
    var emailId:String?
    var sellerProfile:String?
    var sellerType:Int?
    var tagLine:String?
    var website:String?
    var registeredAddress:String?
    var youtubeLink:String?
    var categoryCode:Int?
    var workFlowType:Int?
    var moveTransAfterTP:Int?
    var moveTransAfterTPType:Int?
    var enableAppointmentTimeInRN:Int?
    var cpDays:Int?
    var cpHours:Int?
    var companyThumbnailLogo:String?
    var lockAmcField:Int?
    var amcFieldAlertMessage:String?
    var companyType:Int?
    
    private enum CodingKeys:String,CodingKey{
        case sellerCode
        case lngId
        case sellerCompanyName
        case logo
        case firstName
        case lastName
        case jobTitle
        case mobileIsd
        case mobileNumber
        case phoneIsd
        case phoneNumber
        case notes
        case taxNumber
        case status
        case statusTitle
        case crDate
        case luDate
        case amcCurrencyId
        case amcCurrencySymbol
        case amcType
        case amcValue
        case attachmentList
        case billingType
        case emailId
        case sellerProfile
        case sellerType
        case tagLine
        case website
        case registeredAddress
        case youtubeLink
        case categoryCode
        case workFlowType
        case moveTransAfterTP
        case moveTransAfterTPType
        case enableAppointmentTimeInRN
        case cpDays
        case cpHours
        case companyThumbnailLogo
        case lockAmcField
        case amcFieldAlertMessage
        case companyType
    }
}

class BillingCycleModel: Codable {
   
    var billingType:Int?
    var title:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case billingType
        case title
    }
    

}

class SellerCategoryModel: Codable {
   
    var categoryCode:Int?
    var currencyId:Int?
    var minFixedAmount:Double?
    var minPercentage:Double?
    var title:String?
    var defaultCPDays:Int?
    var defaultCPHours:Int?
    
    private enum CodingKeys:String,CodingKey{
           
            case categoryCode
            case currencyId
            case minFixedAmount
            case minPercentage
            case title
            case defaultCPDays
            case defaultCPHours
       }
    
//    override init() {
//           super.init()
//       }
//    public override func encode(to encoder: Encoder) throws {
//           try super.encode(to: encoder)
//           var container = encoder.container(keyedBy: CodingKeys.self)
//           try container.encode(categoryCode, forKey: .categoryCode)
//           try container.encode(currencyId, forKey: .currencyId)
//           try container.encode(minFixedAmount, forKey: .minFixedAmount)
//           try container.encode(minPercentage, forKey: .minPercentage)
//           try container.encode(title, forKey: .title)
//           try container.encode(defaultCPDays, forKey: .defaultCPDays)
//           try container.encode(defaultCPHours, forKey: .defaultCPHours)
//       }
//       required public init(from decoder: Decoder) throws {
//           try super.init(from: decoder)
//           let values = try decoder.container(keyedBy: CodingKeys.self)
//           categoryCode = try values.decodeIfPresent(Int.self, forKey: .categoryCode)
//           currencyId = try values.decodeIfPresent(Int.self, forKey: .currencyId)
//           minFixedAmount = try values.decodeIfPresent(Double.self, forKey: .minFixedAmount)
//           minPercentage = try values.decodeIfPresent(Double.self, forKey: .minPercentage)
//           title = try values.decodeIfPresent(String.self, forKey: .title)
//           defaultCPDays = try values.decodeIfPresent(Int.self, forKey: .defaultCPDays)
//           defaultCPHours = try values.decodeIfPresent(Int.self, forKey: .defaultCPHours)
//       }
}

class SellerDetailApiModel: Codable {
    
    var sellerCompanyDetails:SellerDetailModel?
    
}

class NewSellerUserModel: Codable {
    
    var requestId:Int?
    var searchQuery:String?
    var description:String?
    var address:String?
    var latitude:Double?
    var longitude:Double?
    var status:Int?
    var crUserId:Int?
    var createdBy:String?
    var luUserId:Int?
    var updatedBy:String?
    var crDate:String?
    var luDate:String?
    var assignedTo:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case requestId
        case searchQuery
        case description
        case address
        case latitude
        case longitude
        case status
        case crUserId
        case createdBy
        case luUserId
        case updatedBy
        case crDate
        case luDate
        case assignedTo
    }

}

class NewSellerUserApiModel: Codable {
    
    var sellerRequestList:[NewSellerUserModel]?
    var count:Int?
    
}

class NewSellerUserHistoryModel: Codable {
    
    var requestId:Int?
    var searchQuery:String?
    var description:String?
    var address:String?
    var latitude:Double?
    var longitude:Double?
    var status:Int?
    var crUserId:Int?
    var createdBy:String?
    var luUserId:Int?
    var updatedBy:String?
    var crDate:String?
    var luDate:String?
    
    var sellerCode:Int?
    var sellerCompanyName:String?
    var companyLogo:String?
    var sellerMemberId:String?

    
    private enum CodingKeys:String,CodingKey{
        
        case requestId
        case searchQuery
        case description
        case address
        case latitude
        case longitude
        case status
        case crUserId
        case createdBy
        case luUserId
        case updatedBy
        case crDate
        case luDate
        case sellerCode
        case sellerCompanyName
        case companyLogo
        case sellerMemberId
    }
    
}

class NewSellerUserDetailApiModel: Codable {
    
    var sellerRequestDetails:NewSellerUserModel?
    var sellerRequestHistory:[NewSellerUserHistoryModel]?
    var count:Int?
    
}

class FindSellerDetailsModel: Codable {
    
    var sellerCode:Int?
    var companyLogo:String?
    var sellerCompanyName:String?
    var sellerMemberId:String?
    var sellerUserId:Int?
    
    private enum CodingKeys:String,CodingKey{
        
        case sellerCode
        case companyLogo
        case sellerCompanyName
        case sellerMemberId
        case sellerUserId
        
    }
    
}

class FindSellerApiModel: Codable {
    
    var sellerPresent:Int?
    var sellerDetails:FindSellerDetailsModel?
    
}

class TimePeriodModel: Codable {
    var timeCode:Int?
    var title:String?

}

class AccessUserTypesModel:Codable{
  var accessType:Int?
  var title:String?
}

class SellerProductBranchPopUpModel:Codable{
  var newBranchMessage:String?
  var newBranchNote:String?
  var newProductMessage:String?
  var newProductNote:String?
    
    private enum CodingKeys:String,CodingKey{
        case newBranchMessage
        case newBranchNote
        case newProductMessage
        case newProductNote
    }
}

class CompanyTypesModel:Codable{
  var companyType:Int?
  var title:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case companyType
        case title
        
    }
}
