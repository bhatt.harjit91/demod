//
//  RatingModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 10/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class RatingMasterModel: Codable {
   
    var ratingTypeId:Int?
    var ratingName:String?
    var rating:Double?
    
    private enum CodingKeys:String,CodingKey{
        
        case ratingTypeId
        case ratingName
        case rating
        
    }

}

class PostRatingModel : Codable
{
    var ratingType:Int?
    var userRating:Double?
}

class RatingDetailsModel: Codable {
    
    var sellerCode:Int?
    var transactionId:Int?
    var productCode:Int?
    var comments:String?
    var responseMessage:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case sellerCode
        case transactionId
        case productCode
        case comments
        case responseMessage
        
    }
    
}

class RatingHistoryModel: Codable {
    
    var reviewId:Int?
    var rating:Double?
    var sellercode:Int?
    var productCode:Int?
    var comments:String?
    var responseMessage:String?
    var userId:Int?
    var createdby:String?
    var luUserid:String?
    var createdDate:String?
    var responseDate:String?
    var updatedBy:String?
    
    private enum CodingKeys:String,CodingKey{
       
        case reviewId
        case rating
        case sellercode
        case productCode
        case comments
        case responseMessage
        case userId
        case createdby
        case luUserid
        case createdDate
        case responseDate
        case updatedBy
    }

}
