//
//  ReferalModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 18/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class ReferalModel: Codable {
    
    var actualMessage:String?
    var sellerCode:Int?
    var transactionId:Int?
    var productCode:Int?
    var referredDate:String?
    var stageId:Int?
    var productName:String?
    var productLogo:String?
    var priceType:Int?
    var productCurrencyId:Int?
    var productCurrencySymbol:String?
    var referralFeeType:Int?
    var referralCurrencyId:Int?
    var referralCurrencySymbol:String?
    var referralFee:String?
    var sellerCompanyName:String?
    
    var stageTitle:String?
    var referredBy:String?
    var referredTo:String?
    var isExpand:Int?
    
    var name:String?
    var mobileIsd:String?
    var mobileNumber:String?
    var emailId:String?
    
    var workFlowType:Int?
    
    var dealValue:Double?
    var dealCurrencyId:Int?
    var dealCurrencySymbol:String?
    var dealDate:String?
    var dealNotes:String?
    var isSeller:Int?
    
    var hideTransForBuyer:Int?
    var stageColorCode:String?
    var cvText:String?
    var cvPath:String?
    var productType:Int?
    
    var isLeadReferredPartner:Int?
    var receiptCdnPath:String?
    
    var isAppointment:Int?
    
    var appointmentId:Int?
    var appointmentDT:String?
    var noOfPeople:Int?
    var bookingStatusTitle:String?
    var resourceTitle:String?
    var resourceCount:Int?
    var registrationId:String?

    
    private enum CodingKeys:String,CodingKey{
        
        case actualMessage
        case sellerCode
        case transactionId
        case productCode
        case referredDate
        case stageId
        case productName
        case productLogo
        case priceType
        case productCurrencyId
        case productCurrencySymbol
        case referralFeeType
        case referralCurrencyId
        case referralCurrencySymbol
        case referralFee
        case sellerCompanyName
        case referredTo
        case stageTitle
        case referredBy
        case name
        case mobileIsd
        case mobileNumber
        case emailId
        case workFlowType
        case dealValue
        case dealCurrencyId
        case dealCurrencySymbol
        case dealNotes
        case isSeller
        case hideTransForBuyer
        case stageColorCode
        case cvText
        case productType
        case cvPath
        case isLeadReferredPartner
        case receiptCdnPath
        case isAppointment
        case appointmentDT
        case noOfPeople
        case bookingStatusTitle
        case resourceTitle
        case resourceCount
        case registrationId
        case appointmentId
    }
}
class ReferralDetailModel:Codable{
    
    var sellerCode:Int?
    var transactionId:Int?
    var productCode:Int?
    var referredDate:String?
    var stageId:Int?
    var productName:String?
    var productLogo:String?
    var priceType:Int?
    var productCurrencyId:Int?
    var productCurrencySymbol:String?
    var referralFeeType:Int?
    var referralCurrencyId:Int?
    var referralCurrencySymbol:String?
    var referralFee:String?
    var sellerCompanyName:String?
    var referralNoteToSeller:String?
    var specialTerms:String?
    var dealValue:Double?
    var dealCurrencyId:Int?
    var dealCurrencySymbol:String?
    var dealDate:String?
    var dealNotes:String?
    var buyerComments:String?
    var stageTitle:String?
    var referredBy:String?
    var attachments:[[String:String]]?
    var attachmentList:[[String:String]]?
    var workFlowType:Int?
    
    var referredTo:String?
    var mobileIsd:String?
    var mobileNumber:String?
    var emailId:String?
    
    var hideTransForBuyer:Int?
    var isSeller:Int?
    
    var cvText:String?
    var cvPath:String?
    var referralPayoutDetails:[ReferralPayOutModel]?
    var productType:Int?
    
    var isLeadReferredPartner:Int?
    
    var paymentProgressValue:Int?
    var paymentStatusCode:Int?
    var paymentStatusTitle:String?
    
    var receiptCdnPath:String?
    
    var  displayEndTime:String?
    var  displayStartTime:String?
    
    var paymentType:EventPaymentTypeModel?
    
    var productPrice:String?
    
    var isAppointment:Int?
    
    var appointmentId:Int?
    var appointmentDT:String?
    var noOfPeople:Int?
    var bookingStatusTitle:String?
    var resourceTitle:String?
    var resourceCount:Int?
    var registrationId:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case sellerCode
        case transactionId
        case productCode
        case referredDate
        case stageId
        case productName
        case productLogo
        case priceType
        case productCurrencyId
        case productCurrencySymbol
        case referralFeeType
        case referralCurrencyId
        case referralCurrencySymbol
        case referralFee
        case sellerCompanyName
        case referralNoteToSeller
        case specialTerms
        case dealValue
        case dealCurrencyId
        case dealCurrencySymbol
        case dealDate
        case dealNotes
        case buyerComments
        case stageTitle
        case referredBy
        case attachments
        case attachmentList
        case workFlowType
        case referredTo
        case mobileIsd
        case mobileNumber
        case emailId
        case hideTransForBuyer
        case cvText
        case cvPath
        case referralPayoutDetails
        case productType
        case isSeller
        case isLeadReferredPartner
        case paymentProgressValue
        case paymentStatusCode
        case paymentStatusTitle
        case receiptCdnPath
        case displayEndTime
        case displayStartTime
        case paymentType
        case productPrice
        case isAppointment
        case appointmentDT
        case noOfPeople
        case bookingStatusTitle
        case resourceTitle
        case resourceCount
        case registrationId
        case appointmentId
    }
}

class LogModel: Codable {
    
    var oldStageId:Int?
    var oldStageTitle:String?
    var newStageId:Int?
    var newStageTitle:String?
    var changeLogJson:String?
    var crUserId:Int?
    var createdBy:String?
    var crDate:String?
    
    private enum CodingKeys:String,CodingKey{
        case oldStageId
        case oldStageTitle
        case newStageId
        case newStageTitle
        case changeLogJson
        case crUserId
        case createdBy
        case crDate
    }
}

class StageListMainModel: Codable {
    
    var stageId :Int?
    var stageTitle:String?
    var progressValue:Double?
    
    private enum CodingKeys:String,CodingKey{
        case stageId
        case stageTitle
        case progressValue
    }
}

class ReplyTypeModel: Codable {
    
    var replyType :Int?
    var title:String?
    var isDefault:Int?
    var isSelected:Int = 0
    
    private enum CodingKeys:String,CodingKey{
        case replyType
        case title
        case isDefault
    }
}


class ReferalApiModel: Codable {
    
    var referralLeadList:[ReferalModel]?
    var stageList:[StageListMainModel]?
    var jobStageList:[StageListMainModel]?
    var currency:[CurrencyModel]?
    var count:Int?
    var isSelectSellerVisible:Int?
    
}

class ReferralDetailApiModel:Codable{
    
    var referralLeadDetails:ReferralDetailModel?
    var currency:[CurrencyModel]?
    var changeLog:[LogModel]?
    var masterStageList:[StageListMainModel]?
    
}

class ReferralDetailUpdateApiModel:Codable{
    
    var referralLeadDetails:ReferralDetailModel?
    var changeLog:[LogModel]?
    var messageHistory:[MessageHistoryModel]?
    var masterStageList:[StageListMainModel]?
    var currency:[CurrencyModel]?
    
    private enum CodingKeys:String,CodingKey{
        
        case referralLeadDetails
        case changeLog
        case messageHistory
        case masterStageList
        case currency
    }
}

class ReferralSellerFilterModel:Codable {
    
    var sellerCode:Int?
    var sellerCompanyName:String?
    var logo:String?
    var isSelected:Int?

    private enum CodingKeys:String,CodingKey{
        
        case sellerCode
        case sellerCompanyName
        case logo
        
    }
    
}

class ReferralSellerApiFilterModel: Codable {
    
    var sellerList:[ReferralSellerFilterModel]?
    var count:Int?
    
}

class ReferralPayOutModel: Codable {
    
    var title:String?
    var amount:String?

}
