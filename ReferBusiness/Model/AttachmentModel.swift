//
//  AttachmentModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class AttachmentTypeModel: Codable {
    
    var attachmentType:Int?
    var title:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case attachmentType
        case title
    }
}

class AttachmentBannerTypeModel: Codable {
    
    var attachmentType:Int?
    var cdnPath:String?
    var fileName:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case attachmentType
        case cdnPath
        case fileName
    }

}

class AttachmentMainModel: Codable {
    
    var cdnPath:String?
    var fileName:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case cdnPath
        case fileName
    }

}


