//
//  PaymentModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 09/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class BenificiaryAccountDetailModel: Codable {
    
    var claimAmount:Double?
    var currencyId:Int?
    var taxNumber:String?
    var currencySymbol:String?
    var isFirstTimeUser:Int?
    var beneficiaryName:String?
    var bankName:String?
    var accountNumber:String?
    var ifscCode:String?
    var beneficiaryComments:String?
    var topText:String?
    var bottomText:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case claimAmount
        case currencyId
        case taxNumber
        case currencySymbol
        case isFirstTimeUser
        case beneficiaryName
        case bankName
        case accountNumber
        case ifscCode
        case beneficiaryComments
        case topText
        case bottomText
        
    }

}

class BenificiaryAccountDetailApiModel: Codable {
    
    var beneficiaryDetails:BenificiaryAccountDetailModel?
    
}

class PaymentModel: Codable {
    
    var merchantId:String?
    var accessCode:String?
    var currencyTitle:String?
    var amount:Double?
    var customerId:Int?
    var rsaKeyUrl:String?
    var redirectUrl:String?
    var cancelUrl:String?
    var order_id:String?
    var encryptionKey:String?
    var countryId:Int?
    var currencyId:Int?
    var bankFixedAmount:Double?
    var bankPercentage:Double?
    var delivery_name:String?
    var delivery_address:String?
    var delivery_city:String?
    var delivery_state:String?
    var delivery_zip:String?
    var delivery_country:String?
    var delivery_tel:String?
    var bankCharges:Double?
    var rsaPublicKey:String?
    var billingDetails:billingUserModel?
    var razorpay_order_id:String?
    var merchantName:String?
    var sellerCompanyName:String?
    var paymentUrl:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case merchantId = "merchantId"
        case accessCode = "accessCode"
        case currencyTitle = "currencyTitle"
        case amount = "amount"
        case customerId = "customerId"
        case rsaKeyUrl = "rsaKeyUrl"
        case redirectUrl = "redirectUrl"
        case cancelUrl = "cancelUrl"
        case order_id = "order_id"
        case encryptionKey = "encryptionKey"
        case countryId = "countryId"
        case currencyId = "currencyId"
        case bankFixedAmount = "bankFixedAmount"
        case bankPercentage = "bankPercentage"
        case delivery_name = "delivery_name"
        case delivery_address = "delivery_address"
        case delivery_city = "delivery_city"
        case delivery_state = "delivery_state"
        case delivery_zip = "delivery_zip"
        case delivery_country = "delivery_country"
        case delivery_tel = "delivery_tel"
        case bankCharges = "bankCharges"
        case rsaPublicKey = "rsaPublicKey"
        case billingDetails = "billingDetails"
        case razorpay_order_id = "razorpay_order_id"
        case merchantName = "merchantName"
        case sellerCompanyName = "sellerCompanyName"
        case paymentUrl = "paymentUrl"
    }
}

class PaymentAPIModel:Codable {
    
    var details:PaymentModel?
    
}

class ReferralPaymentMasterApiModel:Codable{
    
    var currencyList:[CurrencyModel]?
    var paymentTypeList:[PaymentTypeListModel]?
    
}
