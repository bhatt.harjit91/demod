//
//  SearchListFilterModel.swift
//  iCanRefer
//
//  Created by Hirecraft on 07/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import Foundation

class JobTypeFilter: Codable {
    
    var jobType:Int?
    var title:String?
    var isSelected:Int?
    
    private enum CodingKeys:String,CodingKey{
        case jobType
        case title
        
    }
}

class ExperienceListModel: Codable {
    var expType:Int?
    var title:String?
    
    private enum CodingKeys:String,CodingKey{
        case expType
        case title
        
    }
}

class FilterApiModel: Codable {
    
    var jobTypeList:[JobTypeFilter]?
    var experienceList:[ExperienceListModel]?
    
    private enum CodingKeys:String,CodingKey{
           case jobTypeList
           case experienceList
           
       }
}
