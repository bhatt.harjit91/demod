//
//  MessageModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 25/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class MessageHistoryModel: Codable {
    
    var messageId:Int?
    var fromUserId:Int?
    var createdBy:String?
    var message:String?
    var crDate:String?
    var isBuyer:Int?
    var isReferralMember:Int?
    var isSeller:Int?
    var receiverId:Int?
    var receiverName:String?
    var title:String?
    
    private enum CodingKeys:String,CodingKey{
        case messageId
        case fromUserId
        case createdBy
        case message
        case crDate
        case isBuyer
        case isReferralMember
        case isSeller
        case receiverId
        case receiverName
        case title
    }
}
class MessageHistoryListApiModel:Codable{
    
    var messageHistory:[MessageHistoryModel]?
    var count:Int?
    var replyTypes:[ReplyTypeModel]?
    
    private enum CodingKeys:String,CodingKey{
        
        case messageHistory
        case replyTypes
        case count
    }
}

class MessageListModel:Codable{
    
    var transactionId:Int?
    var rmAnonymous:Int?
    var productCode:Int?
    var crUserId:Int?
    var toUserId:Int?
    var referredTo:String?
    var referredBy:String?
    var referredDate:String?
    var actualMessage:String?
    var stageId:Int?
    var stage:String?
    var productName:String?
    var productLogo:String?
    var sellerCompanyName:String?
    var sellerCode:Int?
    var isBuyer:Int?
    var isReferralMember:Int?
    var isSeller:Int?
    var isNotified:Int?
    var responseCount:Int?
    var unreadResponseCount:Int?
    var isListingPartner:Int?
    var title:String?
    var isVideo:Int?
    
    private enum CodingKeys:String,CodingKey{
        
        case transactionId
        case rmAnonymous
        case productCode
        case crUserId
        case toUserId
        case referredTo
        case referredBy
        case referredDate
        case actualMessage
        case stageId
        case stage
        case productName
        case productLogo
        case sellerCompanyName
        case sellerCode
        case isBuyer
        case isReferralMember
        case isSeller
        case isNotified
        case responseCount
        case unreadResponseCount
        case isListingPartner
        case title
        case isVideo
    }
}

class MessageListApiModel:Codable{
    
    var transactionList:[MessageListModel]?
    var count:Int?
    
    private enum CodingKeys:String,CodingKey{
        
        case transactionList
        case count
    }
    
}



class NextTransactionListApiModel:Codable{
    
    var nextTransaction:MessageListModel?
    var count:Int?
    
    private enum CodingKeys:String,CodingKey{
        
        case nextTransaction
        case count
    }
    
}
