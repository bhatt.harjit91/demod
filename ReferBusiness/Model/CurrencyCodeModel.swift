//
//  CurrencyCodeModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class CurrencyDetailModel:Codable {
    
    var currencyId:Int?
    var currencySymbol:String?
    var minimumSellerAnnualAmount:Double?
    
    private enum CodingKeys:String,CodingKey{
        
        case currencyId = "currencyId"
        case currencySymbol = "currencySymbol"
        case minimumSellerAnnualAmount = "minimumSellerAnnualAmount"
        
    }
    
}
class CurrencyDetailApiModel:Codable {
    
    var currencyDetail:CurrencyDetailModel?
}
