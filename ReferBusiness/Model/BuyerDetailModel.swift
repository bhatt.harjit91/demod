//
//  BuyerDetailModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 10/11/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class TransactionBuyerDetailApiModel: Codable {
    
    var masterStageList:[BuyerStageListModel]?
    var transactionDetails:TransactionBuyerDetailModel?
    var reviewDetails:TransactionBuyerRatingModel?
    var currencyList:[CurrencyModel]?
    
    private enum CodingKeys:String,CodingKey{
        
        case masterStageList
        case transactionDetails
        case reviewDetails
        case currencyList
    }
    
}



class BuyerStageListModel:Codable{
    
    var stageId:Int?
    var stageTitle:String?
    var progressValue:Int?
    var type:Int?

    private enum CodingKeys:String,CodingKey{
        
        case stageId
        case stageTitle
        case progressValue
        case type
    }
    
}

class TransactionBuyerDetailModel: Codable {
    
    var transactionId:Int?
    var rmAnonymous:Int?
    var actualMessage:String?
    var isVerified:Int?
    var companyLogo:String?
    var rating:Double?
    var productCode:Int?
    var crUserId:Int?
    var toUserId:Int?
    var referredTo:String?
    var referredBy:String?
    var referredDate:String?
    var productName:String?
    var description:String?
    var productLogo:String?
    var sellerCompanyName:String?
    var sellerCode:Int?
    var isBuyer:Int?
    var isReferralMember:Int?
    var isSeller:Int?
    var banners:[[String:String]]?
    var buyerAttachments:[[String:String]]?
    var buyerComments:String?
    var loyaltyValue:Int?
    var loyaltyCurrencyId:Int?
    var loyaltyType:Int?
    var buyerStageId:Int?
    var stageTitle:String?
    var buyerPaidAmount:Double?
    var buyerPaidCurrencyId:Int?
    var buyerPaidCurrencySymbol:String?
    var specialTerms:String?
    var referralNoteToSeller:String?
    var productType:Int?
    var isVideo:Int?
    var receiptCdnPath:String?
    
    var isAppointment:Int?
    var appointmentId:Int?
    var appointmentDT:String?
    var noOfPeople:Int?
    var bookingStatusTitle:String?
    var resourceTitle:String?
    var resourceCount:Int?
    var registrationId:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case transactionId
        case rmAnonymous
        case actualMessage
        case isVerified
        case companyLogo
        case rating
        case productCode
        case crUserId
        case toUserId
        case referredTo
        case referredBy
        case referredDate
        case productName
        case description
        case productLogo
        case sellerCompanyName
        case sellerCode
        case isBuyer
        case isReferralMember
        case isSeller
        case banners
        case buyerAttachments
        case buyerComments
        case loyaltyValue
        case loyaltyCurrencyId
        case loyaltyType
        case buyerStageId
        case stageTitle
        case buyerPaidAmount
        case buyerPaidCurrencyId
        case buyerPaidCurrencySymbol
        case specialTerms
        case referralNoteToSeller
        case productType
        case isVideo
        case receiptCdnPath
        case isAppointment
        case appointmentDT
        case noOfPeople
        case bookingStatusTitle
        case resourceTitle
        case resourceCount
        case registrationId
        case appointmentId
    }
}

class TransactionBuyerRatingModel:Codable{
    
    var reviewId:Int?
    var sellerCode:Int?
    var transactionId:Int?
    var productCode:Int?
    var comments:String?
    var rating:Double?
    var responseMessage:String?
    var ratingdetails:[RatingMasterModel]?
    var crDate:String?
    var luDate:String?
    var createdby:String?
    var updatedby:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case reviewId
        case sellerCode
        case transactionId
        case productCode
        case comments
        case rating
        case responseMessage
        case ratingdetails
        case crDate
        case luDate
        case createdby
        case updatedby
        
    }
    
}
