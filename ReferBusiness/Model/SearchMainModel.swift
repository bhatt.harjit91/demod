//
//  SearchMainModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 23/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class SearchMainModel:Codable{
    
    var sellerCode:Int?
    var productCode:Int?
    var address:String?
    var priceType:Int?
    var productPrice:String?
    var priceGuideline:String?
    var referralFeeType:Int?
    var referralFee:String?
    var charityType:Int?
    var charityValue:String?
    var branchCode:Int?
    var companyName:String?
    var distance:String?
    var keywords:String?
    var lngId:Int?
    var productName:String?
    var description:String?
    var productLogo:String?
    var banners:[[String:String]]?
    var notesForReferralMember:String?
    var status:Int?
    var statusTitle:String?
    var crDate:String?
    var luDate:String?
    var rating:Double?
    var referredCount:Int?
    var productCurrencySymbol:String?
    var referralCurrencySymbol:String?
    var charityCurrencySymbol:String?
    var latitude:Double?
    var longitude:Double?
    var branchName:String?
    var isVerified:Int?
    var loyaltyType:Int?
    var loyaltyValue:String?
    var loyaltyCurrencyId:Int?
    var sellerProfile:String?
    var tagLine:String?
    var totalReviews:Int?
    var companyLogo:String?
    var isGift:Int?
    var website:String?
    var youtubeLink:String?
    var avgReferralEarningsText:String?
    var avgREAmount:String?
    var productType:Int?
    var referralFormMsg:String?
    var enquiryOrApplyJobFormMsg:String?
    var prodThumbnailLogo:String?
    var companyType:Int?
    var shortDescription:String?
    var enableAppointment:Int?
    var isVideo:Int?
    var displayEndTime:String?
    var displayStartTime:String?
    var expFrom:Int?
    var expTo:Int?
    var jobType:String?
    var enablePayment:Int?
    
    var isLike:Int?
    var likeCount:Int?
    
    var paymentType:EventPaymentTypeModel?
    
    private enum CodingKeys:String,CodingKey{
        
        case sellerCode
        case productCode
        case address
        case priceType
        case productPrice
        case priceGuideline
        case referralFeeType
        case referralFee
        case charityType
        case charityValue
        case branchCode
        case companyName
        case distance
        case keywords
        case lngId
        case productName
        case description
        case productLogo
        case banners
        case notesForReferralMember
        case status
        case statusTitle
        case crDate
        case luDate
        case rating
        case referredCount
        case productCurrencySymbol
        case referralCurrencySymbol
        case charityCurrencySymbol
        case latitude
        case longitude
        case branchName
        case isVerified
        case loyaltyType
        case loyaltyValue
        case loyaltyCurrencyId
        case sellerProfile
        case tagLine
        case totalReviews
        case companyLogo
        case isGift
        case website
        case youtubeLink
        case avgReferralEarningsText
        case avgREAmount
        case productType
        case referralFormMsg
        case enquiryOrApplyJobFormMsg
        case prodThumbnailLogo
        case companyType
        case shortDescription
        case enableAppointment
        case isVideo
        case displayEndTime
        case displayStartTime
        case expFrom
        case expTo
        case jobType
        case enablePayment
        case isLike
        case likeCount
        case paymentType
    }
    
    public func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(sellerCode, forKey: .sellerCode)
        try container.encode(productCode, forKey: .productCode)
        try container.encode(address, forKey: .address)
        try container.encode(priceType, forKey: .priceType)
        try container.encode(productPrice, forKey: .productPrice)
        try container.encode(priceGuideline, forKey: .priceGuideline)
        try container.encode(referralFeeType, forKey: .referralFeeType)
        try container.encode(referralFee, forKey: .referralFee)
        try container.encode(charityType, forKey: .charityType)
        try container.encode(charityValue, forKey: .charityValue)
        try container.encode(branchCode, forKey: .branchCode)
        try container.encode(companyName, forKey: .companyName)
        try container.encode(distance, forKey: .distance)
        try container.encode(keywords, forKey: .keywords)
        try container.encode(lngId, forKey: .lngId)
        try container.encode(productName, forKey: .productName)
        try container.encode(description, forKey: .description)
        try container.encode(productLogo, forKey: .productLogo)
        try container.encode(banners, forKey: .banners)
        try container.encode(notesForReferralMember, forKey: .notesForReferralMember)
        try container.encode(status, forKey: .status)
        try container.encode(statusTitle, forKey: .statusTitle)
        try container.encode(crDate, forKey: .crDate)
        try container.encode(luDate, forKey: .luDate)
        try container.encode(rating, forKey: .rating)
        try container.encode(referredCount, forKey: .referredCount)
        try container.encode(productCurrencySymbol, forKey: .productCurrencySymbol)
        try container.encode(referralCurrencySymbol, forKey: .referralCurrencySymbol)
        try container.encode(charityCurrencySymbol, forKey: .charityCurrencySymbol)
        try container.encode(latitude, forKey: .latitude)
        try container.encode(longitude, forKey: .longitude)
        try container.encode(branchName, forKey: .branchName)
        try container.encode(isVerified, forKey: .isVerified)
        try container.encode(loyaltyType, forKey: .loyaltyType)
        try container.encode(loyaltyValue, forKey: .loyaltyValue)
        try container.encode(loyaltyCurrencyId, forKey: .loyaltyCurrencyId)
        try container.encode(sellerProfile, forKey: .sellerProfile)
        try container.encode(tagLine, forKey: .tagLine)
        try container.encode(totalReviews, forKey: .totalReviews)
        try container.encode(companyLogo, forKey: .companyLogo)
        try container.encode(isGift, forKey: .isGift)
        try container.encode(website, forKey: .website)
        try container.encode(youtubeLink, forKey: .youtubeLink)
        try container.encode(avgReferralEarningsText, forKey: .avgReferralEarningsText)
        try container.encode(avgREAmount, forKey: .avgREAmount)
        try container.encode(productType, forKey: .productType)
        try container.encode(referralFormMsg, forKey: .referralFormMsg)
        try container.encode(enquiryOrApplyJobFormMsg, forKey: .enquiryOrApplyJobFormMsg)
        try container.encode(prodThumbnailLogo, forKey: .prodThumbnailLogo)
        try container.encode(companyType, forKey: .companyType)
        try container.encode(shortDescription, forKey: .shortDescription)
        try container.encode(enableAppointment, forKey: .enableAppointment)
        try container.encode(isVideo, forKey: .isVideo)
        
        try container.encode(displayEndTime, forKey: .displayEndTime)
        try container.encode(displayStartTime, forKey: .displayStartTime)
        try container.encode(expFrom, forKey: .expFrom)
        try container.encode(expTo, forKey: .expTo)
        try container.encode(jobType, forKey: .jobType)
        try container.encode(enablePayment, forKey: .enablePayment)
        try container.encode(isLike, forKey:.isLike)
        try container.encode(likeCount, forKey:.likeCount)
        try container.encode(paymentType, forKey: .paymentType)
        
    }
    
    required public init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sellerCode = try values.decodeIfPresent(Int.self, forKey: .sellerCode)
        productCode = try values.decodeIfPresent(Int.self, forKey: .productCode)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        priceType = try values.decodeIfPresent(Int.self, forKey: .priceType)
        productPrice = try values.decodeIfPresent(String.self, forKey: .productPrice)
        priceGuideline = try values.decodeIfPresent(String.self, forKey: .priceGuideline)
        referralFeeType = try values.decodeIfPresent(Int.self, forKey: .referralFeeType)
        referralFee = try values.decodeIfPresent(String.self, forKey: .referralFee)
        charityType = try values.decodeIfPresent(Int.self, forKey: .charityType)
        charityValue = try values.decodeIfPresent(String.self, forKey: .charityValue)
        
        branchCode = try values.decodeIfPresent(Int.self, forKey: .branchCode)
        companyName = try values.decodeIfPresent(String.self, forKey: .companyName)
        distance = try values.decodeIfPresent(String.self, forKey: .distance)
        keywords = try values.decodeIfPresent(String.self, forKey: .keywords)
        lngId = try values.decodeIfPresent(Int.self, forKey: .lngId)
        productName = try values.decodeIfPresent(String.self, forKey: .productName)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        productLogo = try values.decodeIfPresent(String.self, forKey: .productLogo)
        banners = try values.decodeIfPresent([[String:String]].self, forKey: .banners)
        notesForReferralMember = try values.decodeIfPresent(String.self, forKey: .notesForReferralMember)
        
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        statusTitle = try values.decodeIfPresent(String.self, forKey: .statusTitle)
        crDate = try values.decodeIfPresent(String.self, forKey: .crDate)
        luDate = try values.decodeIfPresent(String.self, forKey: .luDate)
        rating = try values.decodeIfPresent(Double.self, forKey: .rating)
        referredCount = try values.decodeIfPresent(Int.self, forKey: .referredCount)
        productCurrencySymbol = try values.decodeIfPresent(String.self, forKey: .productCurrencySymbol)
        referralCurrencySymbol = try values.decodeIfPresent(String.self, forKey: .referralCurrencySymbol)
        charityCurrencySymbol = try values.decodeIfPresent(String.self, forKey: .charityCurrencySymbol)
        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
        
        branchName = try values.decodeIfPresent(String.self, forKey: .branchName)
        isVerified = try values.decodeIfPresent(Int.self, forKey: .isVerified)
        loyaltyType = try values.decodeIfPresent(Int.self, forKey: .loyaltyType)
        loyaltyValue = try values.decodeIfPresent(String.self, forKey: .loyaltyValue)
        loyaltyCurrencyId = try values.decodeIfPresent(Int.self, forKey: .loyaltyCurrencyId)
        sellerProfile = try values.decodeIfPresent(String.self, forKey: .sellerProfile)
        tagLine = try values.decodeIfPresent(String.self, forKey: .tagLine)
        totalReviews = try values.decodeIfPresent(Int.self, forKey: .totalReviews)
        companyLogo = try values.decodeIfPresent(String.self, forKey: .companyLogo)
        isGift = try values.decodeIfPresent(Int.self, forKey: .isGift)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        youtubeLink = try values.decodeIfPresent(String.self, forKey: .youtubeLink)
        avgReferralEarningsText = try values.decodeIfPresent(String.self, forKey: .avgReferralEarningsText)
        avgREAmount = try values.decodeIfPresent(String.self, forKey: .avgREAmount)
        productType = try values.decodeIfPresent(Int.self, forKey: .productType)
        referralFormMsg = try values.decodeIfPresent(String.self, forKey: .referralFormMsg)
        enquiryOrApplyJobFormMsg = try values.decodeIfPresent(String.self, forKey: .enquiryOrApplyJobFormMsg)
        prodThumbnailLogo = try values.decodeIfPresent(String.self, forKey: .prodThumbnailLogo)
        companyType = try values.decodeIfPresent(Int.self, forKey: .companyType)
        shortDescription = try values.decodeIfPresent(String.self, forKey: .shortDescription)
        enableAppointment = try values.decodeIfPresent(Int.self, forKey: .enableAppointment)
        isVideo = try values.decodeIfPresent(Int.self, forKey: .isVideo)
        
        displayEndTime = try values.decodeIfPresent(String.self, forKey: .displayEndTime)
        displayStartTime = try values.decodeIfPresent(String.self, forKey: .displayStartTime)
        expFrom = try values.decodeIfPresent(Int.self, forKey: .expFrom)
        expTo = try values.decodeIfPresent(Int.self, forKey: .expTo)
        jobType = try values.decodeIfPresent(String.self, forKey: .jobType)
        
        enablePayment = try values.decodeIfPresent(Int.self, forKey: .enablePayment)
        
        isLike = try values.decodeIfPresent(Int.self, forKey: .isLike)
        likeCount = try values.decodeIfPresent(Int.self, forKey: .likeCount)
        paymentType = try values.decodeIfPresent(EventPaymentTypeModel.self, forKey: .paymentType)
    }
    
}


class SearchMainApiModel:Codable{
    
    var productList:[SearchMainModel]?
    var count:Int?
    var advertiseList:[AdvertisementModel]?
    
    var headerTitle:String?
    var footerTitle:String?
    var categoryList:[ModelCategorySearchList]?
    
    private enum CodingKeys:String,CodingKey{
        
        case productList
        case count
        case advertiseList
        case headerTitle
        case footerTitle
        case categoryList
        
    }
    
    public func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(productList, forKey: .productList)
        try container.encode(count, forKey: .count)
        try container.encode(advertiseList, forKey: .advertiseList)
        
        try container.encode(headerTitle, forKey: .headerTitle)
        try container.encode(footerTitle, forKey: .footerTitle)
        try container.encode(categoryList, forKey: .categoryList)
        
    }
    
    required public init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        productList = try values.decodeIfPresent([SearchMainModel].self, forKey: .productList)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
        advertiseList = try values.decodeIfPresent([AdvertisementModel].self, forKey: .advertiseList)
        
         headerTitle = try values.decodeIfPresent(String.self, forKey: .headerTitle)
         footerTitle = try values.decodeIfPresent(String.self, forKey: .footerTitle)
         categoryList = try values.decodeIfPresent([ModelCategorySearchList].self, forKey: .categoryList)
    }
}

class AverageRatingModel: Codable {
    
    var avgRating:Double?
    var totalReviews:Double?
    
}

class SearchRatingApiModel: Codable {
    
    var avgReviewData:AverageRatingModel?
    var recentReviews:[RatingHistoryModel]?
    var avgReviewDetailedData:[RatingMasterModel]?
    var masterReviewTypes:[RatingMasterModel]?
}

class SearchDetailModel:Codable{
    
    var sellerCode:Int?
    var productCode:Int?
    var address:String?
    var priceType:Int?
    var productPrice:String?
    var priceGuideline:String?
    var referralFeeType:Int?
    var referralFee:String?
    var charityType:Int?
    var charityValue:String?
    var branchCode:Int?
    var companyName:String?
    var distance:String?
    var keywords:String?
    var lngId:Int?
    var productName:String?
    var description:String?
    var productLogo:String?
    var banners:[[String:String]]?
    var notesForReferralMember:String?
    var status:Int?
    var statusTitle:String?
    var crDate:String?
    var luDate:String?
    var rating:Double?
    var referredCount:Int?
    var productCurrencySymbol:String?
    var referralCurrencySymbol:String?
    var charityCurrencySymbol:String?
    var latitude:Double?
    var longitude:Double?
    var branchName:String?
    var isVerified:Int?
    var loyaltyType:Int?
    var loyaltyValue:String?
    var loyaltyCurrencyId:Int?
    var sellerProfile:String?
    var tagLine:String?
    var totalReviews:Int?
    var companyLogo:String?
    var isGift:Int?
    var website:String?
    var youtubeLink:String?
    
    var distanceType:Int?
    var productCurrencyId:Int?
    var referralCurrencyId:Int?
    var charityCurrencyId:Int?
    
    var loyaltyCurrencySymbol:String?
    var customerCount:Int?
    var testimonialCount:Int?
    var attachmentList:[[String:String]]?
    var productType:Int?
    
    var referralFormMsg:String?
    var enquiryOrApplyJobFormMsg:String?
    var productCodeText:String?
    
    var companyType:Int?
    var shortDescription:String?
    
    var enableAppointment:Int?
    var workingStatus:Int?
    var workingStatusText:String?
    
    var isVideo:Int?
    var enablePayment:Int?
    
    var paymentType:EventPaymentTypeModel?
    
    private enum CodingKeys:String,CodingKey{
        
        case sellerCode
        case productCode
        case address
        case priceType
        case productPrice
        case priceGuideline
        case referralFeeType
        case referralFee
        case charityType
        case charityValue
        case branchCode
        case companyName
        case distance
        case keywords
        case lngId
        case productName
        case description
        case productLogo
        case banners 
        case notesForReferralMember
        case status
        case statusTitle
        case crDate
        case luDate
        case rating
        case referredCount
        case productCurrencySymbol
        case referralCurrencySymbol
        case charityCurrencySymbol
        case latitude
        case longitude
        case branchName
        case isVerified
        case loyaltyType
        case loyaltyValue
        case loyaltyCurrencyId
        case sellerProfile
        case tagLine
        case totalReviews
        case companyLogo
        case isGift
        case website
        case youtubeLink
        case distanceType
        case productCurrencyId
        case referralCurrencyId
        case charityCurrencyId
        case loyaltyCurrencySymbol
        case customerCount
        case testimonialCount
        case attachmentList
        case productType
        case referralFormMsg
        case enquiryOrApplyJobFormMsg
        case productCodeText
        case companyType
        case shortDescription
        case enableAppointment
        case workingStatus
        case workingStatusText
        case isVideo
        case enablePayment
        case paymentType
    }
    
    public func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(sellerCode, forKey: .sellerCode)
        try container.encode(productCode, forKey: .productCode)
        try container.encode(address, forKey: .address)
        try container.encode(priceType, forKey: .priceType)
        try container.encode(productPrice, forKey: .productPrice)
        try container.encode(priceGuideline, forKey: .priceGuideline)
        try container.encode(referralFeeType, forKey: .referralFeeType)
        try container.encode(referralFee, forKey: .referralFee)
        try container.encode(charityType, forKey: .charityType)
        try container.encode(charityValue, forKey: .charityValue)
        try container.encode(branchCode, forKey: .branchCode)
        try container.encode(companyName, forKey: .companyName)
        try container.encode(distance, forKey: .distance)
        try container.encode(keywords, forKey: .keywords)
        try container.encode(lngId, forKey: .lngId)
        try container.encode(productName, forKey: .productName)
        try container.encode(description, forKey: .description)
        try container.encode(productLogo, forKey: .productLogo)
        try container.encode(banners, forKey: .banners)
        try container.encode(notesForReferralMember, forKey: .notesForReferralMember)
        try container.encode(status, forKey: .status)
        try container.encode(statusTitle, forKey: .statusTitle)
        try container.encode(crDate, forKey: .crDate)
        try container.encode(luDate, forKey: .luDate)
        try container.encode(rating, forKey: .rating)
        try container.encode(referredCount, forKey: .referredCount)
        try container.encode(productCurrencySymbol, forKey: .productCurrencySymbol)
        try container.encode(referralCurrencySymbol, forKey: .referralCurrencySymbol)
        try container.encode(charityCurrencySymbol, forKey: .charityCurrencySymbol)
        try container.encode(latitude, forKey: .latitude)
        try container.encode(longitude, forKey: .longitude)
        try container.encode(branchName, forKey: .branchName)
        try container.encode(isVerified, forKey: .isVerified)
        try container.encode(loyaltyType, forKey: .loyaltyType)
        try container.encode(loyaltyValue, forKey: .loyaltyValue)
        try container.encode(loyaltyCurrencyId, forKey: .loyaltyCurrencyId)
        try container.encode(sellerProfile, forKey: .sellerProfile)
        try container.encode(tagLine, forKey: .tagLine)
        try container.encode(totalReviews, forKey: .totalReviews)
        try container.encode(companyLogo, forKey: .companyLogo)
        try container.encode(isGift, forKey: .isGift)
        try container.encode(website, forKey: .website)
        try container.encode(youtubeLink, forKey: .youtubeLink)
        try container.encode(distanceType, forKey: .distanceType)
        try container.encode(productCurrencyId, forKey: .productCurrencyId)
        try container.encode(referralCurrencyId, forKey: .referralCurrencyId)
        try container.encode(charityCurrencyId, forKey: .charityCurrencyId)
        try container.encode(loyaltyCurrencySymbol, forKey: .loyaltyCurrencySymbol)
        try container.encode(customerCount, forKey: .customerCount)
        try container.encode(testimonialCount, forKey: .testimonialCount)
        try container.encode(attachmentList, forKey: .attachmentList)
        try container.encode(productType, forKey: .productType)
        try container.encode(referralFormMsg, forKey: .referralFormMsg)
        try container.encode(enquiryOrApplyJobFormMsg, forKey: .enquiryOrApplyJobFormMsg)
        try container.encode(productCodeText, forKey: .productCodeText)
        try container.encode(companyType, forKey: .companyType)
        try container.encode(shortDescription, forKey: .shortDescription)
        try container.encode(enableAppointment, forKey: .enableAppointment)
        try container.encode(workingStatus, forKey: .workingStatus)
        try container.encode(workingStatusText, forKey: .workingStatusText)
        try container.encode(isVideo, forKey: .isVideo)
        try container.encode(enablePayment, forKey: .enablePayment)
        try container.encode(paymentType, forKey: .paymentType)
    }
    required public init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sellerCode = try values.decodeIfPresent(Int.self, forKey: .sellerCode)
        productCode = try values.decodeIfPresent(Int.self, forKey: .productCode)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        priceType = try values.decodeIfPresent(Int.self, forKey: .priceType)
        productPrice = try values.decodeIfPresent(String.self, forKey: .productPrice)
        priceGuideline = try values.decodeIfPresent(String.self, forKey: .priceGuideline)
        referralFeeType = try values.decodeIfPresent(Int.self, forKey: .referralFeeType)
        referralFee = try values.decodeIfPresent(String.self, forKey: .referralFee)
        charityType = try values.decodeIfPresent(Int.self, forKey: .charityType)
        charityValue = try values.decodeIfPresent(String.self, forKey: .charityValue)
        
        branchCode = try values.decodeIfPresent(Int.self, forKey: .branchCode)
        companyName = try values.decodeIfPresent(String.self, forKey: .companyName)
        distance = try values.decodeIfPresent(String.self, forKey: .distance)
        keywords = try values.decodeIfPresent(String.self, forKey: .keywords)
        lngId = try values.decodeIfPresent(Int.self, forKey: .lngId)
        productName = try values.decodeIfPresent(String.self, forKey: .productName)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        productLogo = try values.decodeIfPresent(String.self, forKey: .productLogo)
        banners = try values.decodeIfPresent([[String:String]].self, forKey: .banners)
        notesForReferralMember = try values.decodeIfPresent(String.self, forKey: .notesForReferralMember)
        
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        statusTitle = try values.decodeIfPresent(String.self, forKey: .statusTitle)
        crDate = try values.decodeIfPresent(String.self, forKey: .crDate)
        luDate = try values.decodeIfPresent(String.self, forKey: .luDate)
        rating = try values.decodeIfPresent(Double.self, forKey: .rating)
        referredCount = try values.decodeIfPresent(Int.self, forKey: .referredCount)
        productCurrencySymbol = try values.decodeIfPresent(String.self, forKey: .productCurrencySymbol)
        referralCurrencySymbol = try values.decodeIfPresent(String.self, forKey: .referralCurrencySymbol)
        charityCurrencySymbol = try values.decodeIfPresent(String.self, forKey: .charityCurrencySymbol)
        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
        
        branchName = try values.decodeIfPresent(String.self, forKey: .branchName)
        isVerified = try values.decodeIfPresent(Int.self, forKey: .isVerified)
        loyaltyType = try values.decodeIfPresent(Int.self, forKey: .loyaltyType)
        loyaltyValue = try values.decodeIfPresent(String.self, forKey: .loyaltyValue)
        loyaltyCurrencyId = try values.decodeIfPresent(Int.self, forKey: .loyaltyCurrencyId)
        sellerProfile = try values.decodeIfPresent(String.self, forKey: .sellerProfile)
        tagLine = try values.decodeIfPresent(String.self, forKey: .tagLine)
        totalReviews = try values.decodeIfPresent(Int.self, forKey: .totalReviews)
        companyLogo = try values.decodeIfPresent(String.self, forKey: .companyLogo)
        isGift = try values.decodeIfPresent(Int.self, forKey: .isGift)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        youtubeLink = try values.decodeIfPresent(String.self, forKey: .youtubeLink)
        distanceType = try values.decodeIfPresent(Int.self, forKey: .distanceType)
        productCurrencyId = try values.decodeIfPresent(Int.self, forKey: .productCurrencyId)
        referralCurrencyId = try values.decodeIfPresent(Int.self, forKey: .referralCurrencyId)
        charityCurrencyId = try values.decodeIfPresent(Int.self, forKey: .charityCurrencyId)
        loyaltyCurrencySymbol = try values.decodeIfPresent(String.self, forKey: .loyaltyCurrencySymbol)
        testimonialCount = try values.decodeIfPresent(Int.self, forKey: .testimonialCount)
        customerCount = try values.decodeIfPresent(Int.self, forKey: .customerCount)
        attachmentList = try values.decodeIfPresent([[String:String]].self, forKey: .attachmentList)
        productType = try values.decodeIfPresent(Int.self, forKey: .productType)
        
        referralFormMsg = try values.decodeIfPresent(String.self, forKey: .referralFormMsg)
        enquiryOrApplyJobFormMsg = try values.decodeIfPresent(String.self, forKey: .enquiryOrApplyJobFormMsg)
        productCodeText = try values.decodeIfPresent(String.self, forKey: .productCodeText)
        companyType = try values.decodeIfPresent(Int.self, forKey: .companyType)
        shortDescription = try values.decodeIfPresent(String.self, forKey: .shortDescription)
        
        enableAppointment = try values.decodeIfPresent(Int.self, forKey: .enableAppointment)
        workingStatus = try values.decodeIfPresent(Int.self, forKey: .workingStatus)
        workingStatusText = try values.decodeIfPresent(String.self, forKey: .workingStatusText)
        isVideo = try values.decodeIfPresent(Int.self, forKey: .isVideo)
        enablePayment = try values.decodeIfPresent(Int.self, forKey: .enablePayment)
        paymentType = try values.decodeIfPresent(EventPaymentTypeModel.self, forKey: .paymentType)
    }
    
    
}

class SearchDetailApiModel:Codable {
    
    var productDetails:SearchDetailModel?
    
    
    private enum CodingKeys:String,CodingKey{
        
        case productDetails
        
    }
    
    public func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(productDetails, forKey: .productDetails)
        
    }
    
    required public init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        productDetails = try values.decodeIfPresent(SearchDetailModel.self, forKey: .productDetails)
        
    }
}


class AdvertisementModel: Codable {
    var appBannerCode:Int?
    var title:String?
    var cdnPath:String?
    var isVideo:Int?
    var delay:Int?
    var thumbnail:String?
    var branchCode:Int?
    var productCode:Int?
    var sellerCompanyName:String?
    var sellerCompanyLogo:String?
    var sellerCode:Int?
    var productName:String?
    var productLogo:String?
    var productType:Int?
    var referralFormMsg:String?
    var enquiryOrApplyJobFormMsg:String?
    var type:Int?
    
    private enum CodingKeys:String,CodingKey{
        case appBannerCode
        case title
        case cdnPath
        case isVideo
        case delay
        case thumbnail
        case branchCode
        case productCode
        case sellerCompanyName
        case sellerCompanyLogo
        case sellerCode
        case productName
        case productLogo
        case productType
        case referralFormMsg
        case enquiryOrApplyJobFormMsg
        case type
        
    }
}

struct ModelCategorySearchList:Codable {
    var catCode:Int?
    var title:String?
    var imagePath:String?
}
