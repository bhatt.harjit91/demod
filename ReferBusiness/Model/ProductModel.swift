//
//  ProductModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 12/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class ProductModel: Codable {
    
    var sellerCode:Int?
    var lngId:Int?
    var productName:String?
    var productCode:Int?
    var notes:String?
    var status:Int?
    var statusTitle:String?
    var productLogo:String?
    var notesForReferralMember:String?
    var crDate:String?
    var luDate:String?
    var thumbnailPath:String?
    var transactionCount:Int?
    
    private enum CodingKeys:String,CodingKey{
        case sellerCode
        case lngId
        case productName
        case productCode
        case notes
        case status
        case statusTitle
        case notesForReferralMember
        case crDate
        case luDate
        case productLogo
        case thumbnailPath
        case transactionCount
    }

}

class ProductDetailModel: Codable {
    var sellerCode:Int?
    var lngId:Int?
    var productCode:Int?
    var productName:String?
    var description:String?
    var productLogo:String?
    var prodThumbnailLogo:String?
    var banners:[[String:String]] = [[:]]
    var priceType:Int?
    var productCurrencyId:Int?
    var productCurrencySymbol:String?
    var minPrice:Double?
    var maxPrice:Double?
    var priceGuideline:String?
    var referralFeeType:Int?
    var referralCurrencyId:Int?
    var referralCurrencySymbol:String?
    var referralFee:Int?
    var notesForReferralMember:String?
    var status:Int?
    var responseType:Int?
    var responseTypeTitle:String?
    var crDate:String?
    var crUserId:Int?
    var luDate:String?
    var luUserId:Int?
    
    var branchCode:[Int]?
    var charityType:Int?
    var charityCurrencyId:Int?
    var charityValue:Double?
    var keywords:String?
    var gifts:[Int]?
    
    var loyaltyValue:Double?
    var loyaltyCurrencyId:Int?
    var loyaltyType:Int?
    var loyaltyCurrencySymbol:String?
    
    var isCharityOffer:Int?
    var isLoyaltyBonus:Int?
    var isPriceType:Int?
    var isReferralFeeType:Int?
    
    var charityCurrencySymbol:String?
    
    var pricingGuideline:String?
    
    var attachmentList:[AttachmentBannerTypeModel]?
    
    var showAttachmentsToBuyer:Int?
    var avgRECurrencyId:Int?
    var avgRECurrencySymbol:String?
    var avgREAmount:Double?
    
    var tagLine:String?
    
    var referralFormMsg:String?
    var enquiryOrApplyJobFormMsg:String?
    var productCodeText:String?

     var shortDescription:String?
    
    var enableAppointment:Int?
    
    var buttonTemplateCode:Int?
    var buttonType:Int?
    var taxTemplateCode:Int?
    var paymentCollectType:Int?
    var cancellationTemplateCode:Int?
    var isVideo:Int?
    
    var displayStartTime:String?
    var displayEndTime:String?
    var visibleUpto:String?
    var jobType:Int?
    
    var expFrom:Int?
    var expTo:Int?
    
    var paymentType:EventPaymentTypeModel?
    
    private enum CodingKeys:String,CodingKey{
        case sellerCode
        case lngId
        case productCode
        case productName
        case description
        case productLogo
        case prodThumbnailLogo
        case banners = "banners"
        case priceType
        case productCurrencyId
        case productCurrencySymbol
        case minPrice
        case maxPrice
        case priceGuideline
        case referralFeeType
        case referralCurrencyId
        case referralCurrencySymbol
        case referralFee
        case notesForReferralMember
        case status
        case responseType
        case responseTypeTitle
        case crDate
        case crUserId
        case luDate
        case luUserId
        case branchCode
        case charityType
        case charityCurrencyId
        case charityValue
        case keywords
        case loyaltyValue
        case loyaltyCurrencyId
        case loyaltyType
        case loyaltyCurrencySymbol
        case isCharityOffer
        case isLoyaltyBonus
        case isPriceType
        case isReferralFeeType
        case charityCurrencySymbol
        case pricingGuideline
        case attachmentList
        case gifts
        case showAttachmentsToBuyer
        case avgRECurrencyId
        case avgRECurrencySymbol
        case avgREAmount
        case tagLine
        case referralFormMsg
        case enquiryOrApplyJobFormMsg
        case productCodeText
        case shortDescription
        case enableAppointment
        case buttonTemplateCode
        case buttonType
        case taxTemplateCode
        case paymentCollectType
        case cancellationTemplateCode
        case isVideo
        case displayStartTime
        case displayEndTime
        case visibleUpto
        case jobType
        case expFrom
        case expTo
        case paymentType
    }

}

class EventPaymentTypeModel: Codable {
    
    var currencySymbol:String?
    var extraCurrencyId:Int?
    var extraFee:Double?
    var isFreeOfCost:Int?
    var isPayAtEvent:Int?
    var isPayonRegistration:Int?
    
    private enum CodingKeys:String,CodingKey{
        
           case currencySymbol
           case extraCurrencyId
           case extraFee
           case isFreeOfCost
           case isPayAtEvent
           case isPayonRegistration
    }
}

class ProductDetailApiModel: Codable {
    
    var productDetails:ProductDetailModel?
    
    private enum CodingKeys:String,CodingKey{
        
        case productDetails = "productDetails"
        
    }
    
}

class ResponseTypeModel: Codable {
   
    var responseType:Int?
    var title: String?
    
}

class CurrencyModel: Codable {
   
    var currencyId:Int?
    var currencySymbol:String?
    var symbol:String?
    var conversionRate:Double?
    var defaultAMCAmount:Double?
    var minAMCAmount:Double?

}

class ProductBranchModel: Codable {
    
    var branchCode:Int?
    var branchName:String?
    var isSelected:Int = 0
    
    private enum CodingKeys:String,CodingKey{
        
        case branchCode
        case branchName
        
    }
}

class ProductGiftModel: Codable {
    
    var giftCode:Int?
    var description:String?
    var imagePath:String?
    var title:String?
    var isSelected:Int = 0
    
    private enum CodingKeys:String,CodingKey{
        
        case giftCode
        case description
        case imagePath
        case title
        
    }
}

class ButtonTypeMasterModel: Codable {
   
    var buttonType:Int?
    var title:String?

    private enum CodingKeys:String,CodingKey{
           
           case buttonType
           case title
           
       }
}

class ButtonTemplateMasterModel: Codable {
   
    var buttonTemplateCode:Int?
    var title:String?

    private enum CodingKeys:String,CodingKey{
           
           case buttonTemplateCode
           case title
           
       }
}

class PaymentCollectTypeMasterModel: Codable {
   
    var paymentCollectType:Int?
    var title:String?

    private enum CodingKeys:String,CodingKey{
           
           case paymentCollectType
           case title
           
       }
}

class CancellationTemplateMasterModel: Codable {
   
    var paymentCollectType:Int?
    var title:String?

    private enum CodingKeys:String,CodingKey{
           
           case paymentCollectType
           case title
           
       }
}

class TaxTemplateMasterModel: Codable {
   
    var paymentCollectType:Int?
    var title:String?

    private enum CodingKeys:String,CodingKey{
           
           case paymentCollectType
           case title
           
       }
}

class ProductApiModel: Codable {
    
    var attachmentTypes:[AttachmentTypeModel]?
    var productList:[ProductModel]?
    var count:Int?
    var responseTypes:[ResponseTypeModel]?
    var currency:[CurrencyModel]?
    var branchList:[ProductBranchModel]?
    var giftList:[ProductGiftModel]?
    var buttonTypeList:[ButtonTypeMasterModel]?
    var buttonTemplateList:[ButtonTemplateMasterModel]?
    var paymentCollectTypeList:[PaymentCollectTypeMasterModel]?
    var cancellationTemplateList:[CancellationTemplateMasterModel]?
    var taxTemplateList:[TaxTemplateMasterModel]?
    var jobTypeList:[JobTypeFilter]?
    
    private enum CodingKeys:String,CodingKey{
        case attachmentTypes
        case productList
        case count
        case responseTypes
        case currency
        case branchList
        case giftList
        case buttonTypeList
        case buttonTemplateList
        case paymentCollectTypeList
        case cancellationTemplateList
        case taxTemplateList
        case jobTypeList
    }
}

class ProductLogModel:Codable{
    
    var productCode:Int?
    var productPrice:String?
    var referralFee:String?
    var charityValue:String?
    var loyaltyValue:String?
    var updatedDate:String?
    var updatedBy:String?
    
    private enum CodingKeys:String,CodingKey{
        case productCode
        case productPrice
        case referralFee
        case charityValue
        case loyaltyValue
        case updatedDate
        case updatedBy
    }
}

class ChangeProductModel: Codable {
  
    var sellerCode:Int?
       var lngId:Int?
       var productName:String?
       var productCode:Int?
       var status:Int?
       var statusTitle:String?
       var productLogo:String?
       var notesForReferralMember:String?
       var crDate:String?
       var luDate:String?
       var prodThumbnailLogo:String?
       var productCodeText:String?

       
       private enum CodingKeys:String,CodingKey{
           case sellerCode
           case lngId
           case productName
           case productCode
           case status
           case statusTitle
           case notesForReferralMember
           case crDate
           case luDate
           case productLogo
           case prodThumbnailLogo
           case productCodeText
       }
    
    
}

class ChangeModelApiModel: Codable {
    var productList:[ChangeProductModel]?
}
