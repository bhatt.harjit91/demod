//
//  TestimonialModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 10/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class TestimonialModel: Codable {
    
    var testimonialCode:Int?
    var sellerCode:Int?
    var description:String?
    var createdBy:String?
    var status:Int?
    var statusTitle:String?
    var lngId:Int?
    var crUserId:Int?
    var crDate:String?
    var luUserId:Int?
    var updatedBy:String?
    var luDate:String?
    var profilePicture:String?
    var ppFileName:String?
    var userProfile:String?

    
    private enum CodingKeys:String,CodingKey {
        
        case testimonialCode
        case sellerCode
        case description
        case createdBy
        case status
        case statusTitle
        case lngId
        case crUserId
        case crDate
        case luUserId
        case updatedBy
        case luDate
        case profilePicture
        case ppFileName
        case userProfile
    }
    

}

class TestimonialApiModel: Codable {
    
    var sellerTestimonialList:[TestimonialModel]?
    var statusList:[SellerStatusModel]?
    var count:Int?
    
}

class TestimonialDetailApiModel: Codable {
    
    var sellerTestimonialDetails:TestimonialModel?
    
}
