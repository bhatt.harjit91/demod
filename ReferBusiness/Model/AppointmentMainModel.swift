//
//  AppointmentMainModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 31/01/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import Foundation

class AppointmentModel: Codable {
    
    var dayType:Int?
    var dayName:String?
    var isHoliday:Int?
    var holidayText:String?
    var workingTime:[AppointmentTimeModel]?
    
    private enum CodingKeys:String,CodingKey{
        case dayType
        case dayName
        case isHoliday
        case holidayText
        case workingTime
    }

}

class AppointmentTimeModel: Codable {
    
    var startTime:String?
    var endTime:String?
    
}

class AppointmentApiModel: Codable {
    
    var workingDays:[AppointmentModel]?

}

class AppointmentSlotModel: Codable {
    
    var status:Int?
    var slotTime:String?
    var availablePax:Int?
    var slotId:Int?
    
}

class AppointmentSlotMainModel: Codable {
     var dayType:Int?
     var dayTypeName:Int?
     var slots:[AppointmentSlotModel]?
}

class AppointmentSlotApiModel: Codable {
    
    var slotList:[AppointmentSlotModel]?
    
}

class ResourceTypeModel:Codable{
      
           var rsCode:Int?
           var code :String?
           var title:String?
           var description:String?
           var pax:Int?
           var imagePath:String?
           var isSelected:Int = 0
    
    private enum CodingKeys:String,CodingKey{
                  case rsCode
                  case code
                  case title
                  case description
                  case pax
                  case imagePath
    }

}

class ResourceTypeApiModel:Codable{
    
    var resourceList:[ResourceTypeModel]?
    var count:Int?
    var note:String?
}
