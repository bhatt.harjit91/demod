//
//  ReferralPartnerModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class ReferalBenifitModel: Codable {
    
    var textCode:Int?
    var title:String?
    var description:String?

    private enum CodingKeys:String,CodingKey {
    
       case textCode
       case title
       case description
    
   }
}

class ReferralMembershipMaster: Codable {
    
    var memberShipType:Int?
    var title:String?
    var currencySymbol:String?
    var fee:String?
    var period:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case memberShipType
        case title
        case currencySymbol
        case fee
        case period
        
    }

}

class ReferralMemberShipFeatureModel: Codable {
    
    var title:String?
    var basicString:String?
    var silverString:String?
    var goldString:String?

    
    private enum CodingKeys:String,CodingKey{
        
        case title
        case basicString
        case silverString
        case goldString
        
    }
    
}
class ReferalPartnerMainApiModel: Codable {
    
    var referralBenefitHeading:String?
    
    var referralBenefitList:[ReferalBenifitModel]?
    
    var termsAndConditions:String?
    
    var referralMemberPaymentPageMessage:String?
    
    var paymentGatewayPage:String?
    
    var referralMemberShipMaster:[ReferralMembershipMaster]?
    
    var referralMemberShipFeatureList:[ReferralMemberShipFeatureModel]?
  
    private enum CodingKeys:String,CodingKey{
        
        case referralBenefitHeading
        case referralBenefitList
        case termsAndConditions
        case referralMemberPaymentPageMessage
        case paymentGatewayPage
        case referralMemberShipMaster
        case referralMemberShipFeatureList
    }
    
    
}

class ReferralMemberList: Codable {
    
    var referralUserId:Int?
    var displayName:String?
    var latitude:Double?
    var longitude:Double?
    var distance:String?
    var isReferralMember:Int?
    var emailId:String?
    
    private enum CodingKeys:String,CodingKey{
        
        case referralUserId
        case displayName
        case latitude
        case longitude
        case distance
        case isReferralMember
        case emailId
    }

}

class LocateReferralApiMaster: Codable {
    
    var title:String?
    var stepOne:String?
    var stepTwo:String?
    var placeHolder:String?
    var referralMemberList:[ReferralMemberList]?
    var count:Int?
    
    private enum CodingKeys:String,CodingKey{
        
        case title
        case stepOne
        case stepTwo
        case placeHolder
        case referralMemberList
        case count
    }
    
}

class GetLocateReferralApiModel: Codable {
    
    var referredUserList:[GetLocateReferralModel]?
    
    
    
    private enum CodingKeys:String,CodingKey{
        
        case referredUserList
        
    }
    
}

class GetLocateReferralModel: Codable {
    
    var displayName:String?
    var profilePicture:String?
    var referredUserId:Int?
    
    private enum CodingKeys:String,CodingKey{
        
        case displayName
        case profilePicture
        case referredUserId
        
    }
    
}
