//
//  JobManagerModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 27/02/20.
//  Copyright © 2020 TalentMicro. All rights reserved.
//

import Foundation

class JobPercentageModel:Codable{
    var code:Int?
    var title:String?

}

class JobDetailApiModel:Codable{
    
    var educationList:[EducationModel]?
    var specializationList:[SpecializationModel]?
    var skillCategoryList:[SkillCategoryModel]?
    var skillList:[SkillListModel]?
    var scaleDurationList:[ScaleDurationModel]?
    var jobTypeList:[JobTypeFilter]?
    var countryList:[CountryListModel]?
    var currencyList:[CurrencyModel]?
    var percentageList:[PercentageModel]?
    var yearList:[YearModel]?
    
}

class JobSkillDetailModel:Codable{
         var skillCatCode:Int?
         var skillCode:Int?
         var minSkillLevel:Int?
         var maxSkillLevel:Int?
         var expFrom:Int?
         var expTo:Int?
         var skillTitle:String?
         var skillCatTitle:String?
}

class EducationDetailModel:Codable{
         var educationCode:Int?
         var specializationCode:Int?
         var percentageCode:Int?
         var yopFrom:Int?
         var yopTo:Int?
         var educationTitle:String?
         var specilaizationTitle:String?
         var percentageTitle:String?
}

  class PercentageModel:Codable{
         var code:Int?
         var title:String?
  }

class AssesmentModel:Codable{
    
   var templateCode:Int?
   var title:String?
   var cutOff:Int?
   var status:Int?
   var crDate:String?
   var luDate:String?

}

class AssesmentApiModel: Codable {
    var assessmentList:[AssesmentModel]?
}
