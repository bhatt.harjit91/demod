//
//  CountryCodeModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

struct CountryCodeModel: Codable {
    let code: String
    let name: String
    let dial_code: String
    let digits: Int
}


struct CountryCodeList: Codable {
    
    let countryList:[CountryCodeModel]
    
}

class SocialMediaModel: Codable {
    
    var socialMediaType:Int?
    var socialMediaName:String?
    var logo:String?
}
