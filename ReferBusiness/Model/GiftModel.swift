//
//  GiftModel.swift
//  iCanRefer
//
//  Created by TalentMicro on 11/12/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class GiftModel: Codable {
    
  var giftCode:Int?
  var sellerCode:Int?
  var title:String?
  var description:String?
  var imagePath:String?
  var imageFileName:String?
  var scheduleText:String?
  var displayFor:Int?
  var status:Int?
  var statusTitle:String?
  var lngId:Int?
  var crUserId:Int?
  var createdBy:String?
  var crDate:String?
  var luUserId:Int?
  var updatedBy:String?
  var luDate:String?
    var giftSchedule:[[String:String]]?
  var giftTimings:[[String:String]]?
    
    private enum CodingKeys:String,CodingKey{
        case giftCode
        case sellerCode
        case title
        case description
        case imagePath
        case imageFileName
        case scheduleText
        case displayFor
        case status
        case statusTitle
        case lngId
        case crUserId
        case createdBy
        case crDate
        case luUserId
        case updatedBy
        case luDate
        case giftSchedule
        case giftTimings
    
    }
    
 }

class GiftScheduleModel: Codable {
    
    var endDate:String?
    var startDate:String?
    
}

class GiftApiModel: Codable {
    
    var sellerGiftlList:[GiftModel]?
    var count:Int?
    var statusList:[SellerStatusModel]?
}

class GiftDetailApiModel: Codable {
    
    var sellerGiftDetails:GiftModel?
    
}

class SearchGiftDetailModel: Codable {
    
    var giftcode:Int?
    var title:String?
    var description:String?
    var imagePath:String?
    var imageFileName:String?
    var scheduleText:String?
    
    private enum CodingKeys:String,CodingKey{
        case giftcode
        case title
        case description
        case imagePath
        case imageFileName
        case scheduleText
        
        
    }

    
}

class SearchGiftDetailApiModel: Codable {
    
    var giftData:SearchGiftDetailModel?
    
}
