//
//  BranchModel.swift
//  ICanRefer
//
//  Created by TalentMicro on 12/10/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

class BranchModel: Codable {
    var branchCode:Int?
    var sellerCode:Int?
    var lngId:Int?
    var branchName:String?
    var address:String?
    var latitude:Double?
    var longitude:Double?
    var notes:String?
    var status:Int?
    var statusTitle:String?
    var jobLocationType:Int?
    var jobLocation:String?
    
    private enum CodingKeys:String,CodingKey{
        case branchCode
        case sellerCode
        case lngId
        case branchName
        case address
        case latitude
        case longitude
        case notes
        case status
        case statusTitle
        case jobLocationType
        case jobLocation
    }
}

class BranchDetailModel: Codable {
  
    var sellerCode:Int?
    var branchName:String?
    var branchCode:Int?
    var lngId:Int?
    var addressLine1:String?
    var addressLine2:String?
    var latitude:Double?
    var longitude: Double?
    var notes:String?
    var status:Int?
    var crDate:String?
    var luDate:String?
    var statusTitle:String?
    var billingAddressBranchCode:Int?
    var country:String?
    var state:String?
    var city:String?
    var postalCode:String?
    var appointmentTemplateCode:Int?
    var timeZoneId:Int?
    var jobLocationType:Int?
    
    private enum CodingKeys:String,CodingKey{
        case sellerCode
        case branchName
        case branchCode
        case lngId
        case addressLine1
        case addressLine2
        case latitude
        case longitude
        case notes
        case status
        case crDate
        case luDate
        case statusTitle
        case billingAddressBranchCode
        case country
        case state
        case city
        case postalCode
        case appointmentTemplateCode
        case timeZoneId
        case jobLocationType
    }

}

class BranchDetailApiModel: Codable {
    
    var sellerBranchDetails:BranchDetailModel?
}

class BranchApiModel: Codable {
    
    var branchList:[BranchModel]?
    var appointmentTemplateList:[BranchAppointmentModel]?
    var timeZoneList:[BranchTimeModel]?
    var jobLocationTypeList:[BranchLocationModel]?
    var count:Int?
    
}

class BranchAppointmentModel: Codable {
    var title:String?
    var appointmentTemplateCode:Int?
}

class BranchTimeModel: Codable {
    
    var countryCode:String?
    var timeZoneId:Int?
    var tzName:String?
    var utcOffset:String?
    
    private enum CodingKeys:String,CodingKey{
      
        case countryCode
        case timeZoneId
        case tzName
        case utcOffset
        
    }
}

class BranchLocationModel: Codable {
    
    var jobLocationType:Int?
    var title:String?
    
    private enum CodingKeys:String,CodingKey{
         
           case jobLocationType
           case title
           
       }
}
