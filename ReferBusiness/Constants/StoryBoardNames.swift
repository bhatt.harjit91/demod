//
//  StoryBoardNames.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation

let LanguageSelectorSB = "LanguageSelector"
let CountryCodeSB = "CountryCodeSelector"
let CurrencySelectorSB = "CurrencySelectorSB"
let LoginSB = "Login"
let ProfilePageSB = "ProfilePage"
let MainSB = "Main"
let HomeSB = "Home"
let LaunchSB = "AppLaunch"
let SellerSB = "SellerSB"
let BranchSB = "BranchSB"
let ProductSB = "ProductSB"
let UserSB = "UserSB"
let ReferalSB = "ReferalSB"
let MessageSB = "MessageSB"
let SearchSB = "SearchSB"
let PaymentSB = "PaymentSB"
let CustomerSB = "CustomerSB"
let TestimonialSB = "TestimonialSB"
let ImageViewerSB = "ImageViewerSB"
let GiftSB = "GiftSB"
let JobSB = "JobSB"
let PartnerSB = "PartnerSB"
let EventsSB = "Events"
let ResumeBankSB = "ResumeBankSB"
let AppointmentSB = "AppointmentSB"
let JobManagerSB = "JobManagerSB"
