//
//  AppConstants.swift
//  ICanRefer
//
//  Created by TalentMicro on 30/09/19.
//  Copyright © 2019 TalentMicro. All rights reserved.
//

import Foundation
 
let isDebug = true

let LiveUrl = "https://www.icanrefer.com/api/"

let TestUrl = "http://23.236.49.140:1001/api/"


let ApiBaseUrl = isDebug ? TestUrl : LiveUrl

let StorageUrl = "https://storage.googleapis.com/ezeone/icanrefer/"

let TermsURL = "icr_termsAndConditions_20191227.html"
let PrivacyURL = "icr_privacy_20191227.html"
let AboutusUrl = "icr_aboutUs_2019.html"
let HelpUrl = "icr_contactUs_2019.html"

let clientId = "77tn2ar7gq6lgv"
let redirectUri = "https://github.com/tonyli508/LinkedinSwift"
